<?php

define('BASE_DIR', __DIR__ . '/../');
require BASE_DIR . 'vendor/autoload.php';

use core\cache\FakeCache;
use core\config\Config;
use Nette\Utils\Finder;

require BASE_DIR . 'constants/default.php';
require BASE_DIR . 'functions/default.php';

$config     = new Config('config/env.neon', new FakeCache());
$seeds      = [];
$migrations = [];

/** @var SplFileInfo $directory */
foreach (Finder::findDirectories()->in(__DIR__ . '/../extensions') as $directory) {
    $seeds[]      = __DIR__ . '/../extensions/' . $directory->getFilename() . '/config/db/seeds';
    $migrations[] = __DIR__ . '/../extensions/' . $directory->getFilename() . '/config/db/migrations';
}

return
    [
        'paths' => [
            'migrations' => [__DIR__ . '/db/migrations', ...$migrations],
            'seeds'      => [__DIR__ . '/db/seeds', ...$seeds],
        ],
        'environments' => [
            'default_migration_table' => 'phinxlog',
            'default_environment'     => $config->find('ENVIRONNEMENT'),
            'production'              => [
                'adapter'    => 'mysql',
                'connection' => new PDO('mysql:' . $config->find('MYSQL.SERVER') . ';dbname=' . $config->find('MYSQL.DATABASE'), $config->find('MYSQL.USER'), $config->find('MYSQL.PASSWORD'), [
                    \PDO::MYSQL_ATTR_INIT_COMMAND       => 'SET NAMES utf8mb4',
                    \PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true,
                    \PDO::ATTR_PREFETCH                 => true,
                    \PDO::ATTR_PERSISTENT               => true
                ]),
                'name' => $config->find('MYSQL.DATABASE'),
            ],
            'development' => [
                'adapter' => 'sqlite',
                'name'    => BASE_DIR . 'config/db/' . $config->find('MYSQL.DATABASE') . '.sqlite3',
                'suffix'  => '.sqlite3',
                'cache'   => 'shared'
            ],
        ],
        'version_order' => 'creation'
    ];
