<?php /**
 * Class CreateActivitie637bc97b9c1efTable
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
final class CreateActivitie637bc97b9c1efTable extends Phinx\Migration\AbstractMigration
{
    /**
     * Change Method.\r\n
     *
     *  Write your reversible migrations using this method.
     *
     *  More information on writing migrations is available here:
     *  https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     *  Remember to call "create()" or "update()" and NOT "save()" when working
     *  with the Table class.
     */
    public function change(): void
    {
        $table = $this->table('activitie', ['id' => 'idactivitie',  'primary_key' => 'idactivitie']);
                $columns = ($table->exists()) ? $table->getColumns() : [];
                $properties = array (
          0 => 'idactivitie',
          1 => 'created_at',
          2 => 'updated_at',
        );
                $columnNames = [];
                foreach($columns as $column){
                    $columnNames[$column->getName()] = true;
                    if (!in_array($column->getName(), $properties)) {
                        $table->removeColumn($column->getName());
                    }
                }
                if(!isset($columnNames['idcontext'])){
                    $table->addColumn('idcontext', 'integer', ['null' => true, 'length' => 11]);
                }else{
                    $table->changeColumn('idcontext', 'integer', ['null' => true, 'length' => 11]);
                }
                if(!$table->exists() || !$table->hasIndexByName('idx_room_reference_activitie')){
                    $table->addIndex('room_reference', [ 'type' => 'fulltext',  'name' =>'idx_room_reference_activitie']);
                }
                if(!isset($columnNames['room_reference'])){
                    $table->addColumn('room_reference', 'string', [ 'default' => 'NULL', 'null' => true]);
                }else{
                    $table->changeColumn('room_reference', 'string', [ 'default' => 'NULL', 'null' => true]);
                }
                if(!$table->exists() || !$table->hasIndexByName('idx_place_reference_activitie')){
                    $table->addIndex('place_reference', [ 'type' => 'fulltext',  'name' =>'idx_place_reference_activitie']);
                }
                if(!isset($columnNames['place_reference'])){
                    $table->addColumn('place_reference', 'string', [ 'default' => 'NULL', 'null' => true]);
                }else{
                    $table->changeColumn('place_reference', 'string', [ 'default' => 'NULL', 'null' => true]);
                }
                if(!isset($columnNames['name'])){
                    $table->addColumn('name', 'string', [ 'default' => '', 'null' => false]);
                }else{
                    $table->changeColumn('name', 'string', [ 'default' => '', 'null' => false]);
                }
                if(!isset($columnNames['idactivitie_type'])){
                    $table->addColumn('idactivitie_type', 'integer', ['null' => true, 'length' => 11]);
                }else{
                    $table->changeColumn('idactivitie_type', 'integer', ['null' => true, 'length' => 11]);
                }
                if(!isset($columnNames['visual'])){
                    $table->addColumn('visual', 'string', [ 'default' => 'NULL', 'null' => true]);
                }else{
                    $table->changeColumn('visual', 'string', [ 'default' => 'NULL', 'null' => true]);
                }
                if(!isset($columnNames['description'])){
                    $table->addColumn('description', 'text', [ 'default' => 'NULL', 'null' => true]);
                }else{
                    $table->changeColumn('description', 'text', [ 'default' => 'NULL', 'null' => true]);
                }
                if(!$table->exists() || !$table->hasIndexByName('idx_tags_activitie')){
                    $table->addIndex('tags', [ 'type' => 'fulltext',  'name' =>'idx_tags_activitie']);
                }
                if(!isset($columnNames['tags'])){
                    $table->addColumn('tags', 'string', [ 'default' => 'NULL', 'null' => true]);
                }else{
                    $table->changeColumn('tags', 'string', [ 'default' => 'NULL', 'null' => true]);
                }
                if(!isset($columnNames['flag_delete'])){
                    $table->addColumn('flag_delete', 'boolean', [ 'default' => '0', 'null' => true, 'length' => 1]);
                }else{
                    $table->changeColumn('flag_delete', 'boolean', [ 'default' => '0', 'null' => true, 'length' => 1]);
                }
                if(!$table->exists()){
                    $table->addTimestamps();
                }
                if($table->exists()) {
                    $table->update();
                } else {
                    $table->create();
                }
    }
}
