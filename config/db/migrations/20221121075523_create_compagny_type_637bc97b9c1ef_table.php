<?php /**
 * Class CreateCompagnyType637bc97b9c1efTable
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
final class CreateCompagnyType637bc97b9c1efTable extends Phinx\Migration\AbstractMigration
{
    /**
     * Change Method.\r\n
     *
     *  Write your reversible migrations using this method.
     *
     *  More information on writing migrations is available here:
     *  https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     *  Remember to call "create()" or "update()" and NOT "save()" when working
     *  with the Table class.
     */
    public function change(): void
    {
        $table = $this->table('compagny_type', ['id' => 'idcompagny_type',  'primary_key' => 'idcompagny_type']);
                $columns = ($table->exists()) ? $table->getColumns() : [];
                $properties = array (
          0 => 'idcompagny_type',
          1 => 'created_at',
          2 => 'updated_at',
        );
                $columnNames = [];
                foreach($columns as $column){
                    $columnNames[$column->getName()] = true;
                    if (!in_array($column->getName(), $properties)) {
                        $table->removeColumn($column->getName());
                    }
                }
                if(!isset($columnNames['idcontext'])){
                    $table->addColumn('idcontext', 'integer', ['null' => true, 'length' => 11]);
                }else{
                    $table->changeColumn('idcontext', 'integer', ['null' => true, 'length' => 11]);
                }
                if(!isset($columnNames['name'])){
                    $table->addColumn('name', 'string', [ 'default' => '', 'null' => false]);
                }else{
                    $table->changeColumn('name', 'string', [ 'default' => '', 'null' => false]);
                }
                if(!isset($columnNames['property'])){
                    $table->addColumn('property', 'string', [ 'default' => 'NULL', 'null' => true]);
                }else{
                    $table->changeColumn('property', 'string', [ 'default' => 'NULL', 'null' => true]);
                }
                if(!isset($columnNames['related_namespace'])){
                    $table->addColumn('related_namespace', 'string', [ 'default' => 'NULL', 'null' => true]);
                }else{
                    $table->changeColumn('related_namespace', 'string', [ 'default' => 'NULL', 'null' => true]);
                }
                if(!isset($columnNames['flag_delete'])){
                    $table->addColumn('flag_delete', 'boolean', [ 'default' => '0', 'null' => true, 'length' => 1]);
                }else{
                    $table->changeColumn('flag_delete', 'boolean', [ 'default' => '0', 'null' => true, 'length' => 1]);
                }
                if(!$table->exists()){
                    $table->addTimestamps();
                }
                if($table->exists()) {
                    $table->update();
                } else {
                    $table->create();
                }
    }
}
