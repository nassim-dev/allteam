<?php /**
 * Class CreatePlaceOpeningTimeLink637bc97b9c1efLinkTable
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
final class CreatePlaceOpeningTimeLink637bc97b9c1efLinkTable extends Phinx\Migration\AbstractMigration
{
    /**
     * Change Method.\r\n
     *
     *  Write your reversible migrations using this method.
     *
     *  More information on writing migrations is available here:
     *  https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     *  Remember to call "create()" or "update()" and NOT "save()" when working
     *  with the Table class.
     */
    public function change(): void
    {
        $table = $this->table('place_opening_time_link', ['id' => 'idplace_opening_time_link',  'primary_key' => 'idplace_opening_time_link']);
                $columns = ($table->exists()) ? $table->getColumns() : [];
                $properties = array (
          0 => 'idplace_opening_time_link',
          1 => 'created_at',
          2 => 'updated_at',
        );
                $columnNames = [];
                foreach($columns as $column){
                    $columnNames[$column->getName()] = true;
                    if (!in_array($column->getName(), $properties)) {
                        $table->removeColumn($column->getName());
                    }
                }
                if(!isset($columnNames['idplace_opening_time'])){
                    $table->addColumn('idplace_opening_time', 'integer', [ 'default' => 0, 'null' => true]);
                }else{
                    $table->changeColumn('idplace_opening_time', 'integer', [ 'default' => 0, 'null' => true]);
                }
                if(!isset($columnNames['idplace'])){
                    $table->addColumn('idplace', 'integer', [ 'default' => 1, 'null' => false, 'length' => 11]);
                }else{
                    $table->changeColumn('idplace', 'integer', [ 'default' => 1, 'null' => false, 'length' => 11]);
                }
                if(!$table->exists()){
                    $table->addTimestamps();
                }
                if(!$table->exists() || !$table->hasIndexByName(idx_idplace_idplace_opening_time_place_opening_time)){
                    $table->addIndex(array (
          0 => 'idplace',
          1 => 'idplace_opening_time',
        ), array (
          'unique' => true,
          'name' => 'idx_idplace_idplace_opening_time_place_opening_time',
        ));
                }
                if($table->exists()) {
                    $table->update();
                } else {
                    $table->create();
                }
    }
}
