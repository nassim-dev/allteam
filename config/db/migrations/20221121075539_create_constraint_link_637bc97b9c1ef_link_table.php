<?php /**
 * Class CreateConstraintLink637bc97b9c1efLinkTable
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
final class CreateConstraintLink637bc97b9c1efLinkTable extends Phinx\Migration\AbstractMigration
{
    /**
     * Change Method.\r\n
     *
     *  Write your reversible migrations using this method.
     *
     *  More information on writing migrations is available here:
     *  https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     *  Remember to call "create()" or "update()" and NOT "save()" when working
     *  with the Table class.
     */
    public function change(): void
    {
        $table = $this->table('constraint_link', ['id' => 'idconstraint_link',  'primary_key' => 'idconstraint_link']);
                $columns = ($table->exists()) ? $table->getColumns() : [];
                $properties = array (
          0 => 'idconstraint_link',
          1 => 'created_at',
          2 => 'updated_at',
        );
                $columnNames = [];
                foreach($columns as $column){
                    $columnNames[$column->getName()] = true;
                    if (!in_array($column->getName(), $properties)) {
                        $table->removeColumn($column->getName());
                    }
                }
                if(!isset($columnNames['idconstraint'])){
                    $table->addColumn('idconstraint', 'integer', [ 'default' => 0, 'null' => true, 'length' => 11]);
                }else{
                    $table->changeColumn('idconstraint', 'integer', [ 'default' => 0, 'null' => true, 'length' => 11]);
                }
                if(!isset($columnNames['idconstraint_schema'])){
                    $table->addColumn('idconstraint_schema', 'integer', [ 'default' => 1, 'null' => false, 'length' => 11]);
                }else{
                    $table->changeColumn('idconstraint_schema', 'integer', [ 'default' => 1, 'null' => false, 'length' => 11]);
                }
                if(!$table->exists()){
                    $table->addTimestamps();
                }
                if(!$table->exists() || !$table->hasIndexByName(idx_idconstraint_schema_idconstraint_constraint)){
                    $table->addIndex(array (
          0 => 'idconstraint_schema',
          1 => 'idconstraint',
        ), array (
          'unique' => true,
          'name' => 'idx_idconstraint_schema_idconstraint_constraint',
        ));
                }
                if($table->exists()) {
                    $table->update();
                } else {
                    $table->create();
                }
    }
}
