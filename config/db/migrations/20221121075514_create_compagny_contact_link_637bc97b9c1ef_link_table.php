<?php /**
 * Class CreateCompagnyContactLink637bc97b9c1efLinkTable
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
final class CreateCompagnyContactLink637bc97b9c1efLinkTable extends Phinx\Migration\AbstractMigration
{
    /**
     * Change Method.\r\n
     *
     *  Write your reversible migrations using this method.
     *
     *  More information on writing migrations is available here:
     *  https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     *  Remember to call "create()" or "update()" and NOT "save()" when working
     *  with the Table class.
     */
    public function change(): void
    {
        $table = $this->table('compagny_contact_link', ['id' => 'idcompagny_contact_link',  'primary_key' => 'idcompagny_contact_link']);
                $columns = ($table->exists()) ? $table->getColumns() : [];
                $properties = array (
          0 => 'idcompagny_contact_link',
          1 => 'created_at',
          2 => 'updated_at',
        );
                $columnNames = [];
                foreach($columns as $column){
                    $columnNames[$column->getName()] = true;
                    if (!in_array($column->getName(), $properties)) {
                        $table->removeColumn($column->getName());
                    }
                }
                if(!isset($columnNames['idcompagny_contact'])){
                    $table->addColumn('idcompagny_contact', 'integer', [ 'default' => 0, 'null' => true]);
                }else{
                    $table->changeColumn('idcompagny_contact', 'integer', [ 'default' => 0, 'null' => true]);
                }
                if(!isset($columnNames['idcompagny'])){
                    $table->addColumn('idcompagny', 'integer', [ 'default' => 1, 'null' => false, 'length' => 11]);
                }else{
                    $table->changeColumn('idcompagny', 'integer', [ 'default' => 1, 'null' => false, 'length' => 11]);
                }
                if(!$table->exists()){
                    $table->addTimestamps();
                }
                if(!$table->exists() || !$table->hasIndexByName(idx_idcompagny_idcompagny_contact_compagny_contact)){
                    $table->addIndex(array (
          0 => 'idcompagny',
          1 => 'idcompagny_contact',
        ), array (
          'unique' => true,
          'name' => 'idx_idcompagny_idcompagny_contact_compagny_contact',
        ));
                }
                if($table->exists()) {
                    $table->update();
                } else {
                    $table->create();
                }
    }
}
