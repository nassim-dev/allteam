<?php /**
 * Class CreateContactTagsList637bc97b9c1efTable
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
final class CreateContactTagsList637bc97b9c1efTable extends Phinx\Migration\AbstractMigration
{
    /**
     * Change Method.\r\n
     *
     *  Write your reversible migrations using this method.
     *
     *  More information on writing migrations is available here:
     *  https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     *  Remember to call "create()" or "update()" and NOT "save()" when working
     *  with the Table class.
     */
    public function change(): void
    {
        $table = $this->table('contact_tags_list', ['id' => 'idcontact_tags_list',  'primary_key' => 'idcontact_tags_list']);
                $columns = ($table->exists()) ? $table->getColumns() : [];
                $properties = array (
          0 => 'idcontact_tags_list',
          1 => 'created_at',
          2 => 'updated_at',
        );
                $columnNames = [];
                foreach($columns as $column){
                    $columnNames[$column->getName()] = true;
                    if (!in_array($column->getName(), $properties)) {
                        $table->removeColumn($column->getName());
                    }
                }
                if(!isset($columnNames['key'])){
                    $table->addColumn('key', 'string', [ 'default' => 'NULL', 'null' => true, 'length' => 255]);
                }else{
                    $table->changeColumn('key', 'string', [ 'default' => 'NULL', 'null' => true, 'length' => 255]);
                }
                if(!isset($columnNames['value'])){
                    $table->addColumn('value', 'string', [ 'default' => 'NULL', 'null' => true, 'length' => 255]);
                }else{
                    $table->changeColumn('value', 'string', [ 'default' => 'NULL', 'null' => true, 'length' => 255]);
                }
                if(!$table->exists()){
                    $table->addTimestamps();
                }
                if($table->exists()) {
                    $table->update();
                } else {
                    $table->create();
                }
    }
}
