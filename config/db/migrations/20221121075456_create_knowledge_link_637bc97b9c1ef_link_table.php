<?php /**
 * Class CreateKnowledgeLink637bc97b9c1efLinkTable
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
final class CreateKnowledgeLink637bc97b9c1efLinkTable extends Phinx\Migration\AbstractMigration
{
    /**
     * Change Method.\r\n
     *
     *  Write your reversible migrations using this method.
     *
     *  More information on writing migrations is available here:
     *  https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     *  Remember to call "create()" or "update()" and NOT "save()" when working
     *  with the Table class.
     */
    public function change(): void
    {
        $table = $this->table('knowledge_link', ['id' => 'idknowledge_link',  'primary_key' => 'idknowledge_link']);
                $columns = ($table->exists()) ? $table->getColumns() : [];
                $properties = array (
          0 => 'idknowledge_link',
          1 => 'created_at',
          2 => 'updated_at',
        );
                $columnNames = [];
                foreach($columns as $column){
                    $columnNames[$column->getName()] = true;
                    if (!in_array($column->getName(), $properties)) {
                        $table->removeColumn($column->getName());
                    }
                }
                if(!isset($columnNames['idknowledge'])){
                    $table->addColumn('idknowledge', 'integer', [ 'default' => 0, 'null' => true, 'length' => 11]);
                }else{
                    $table->changeColumn('idknowledge', 'integer', [ 'default' => 0, 'null' => true, 'length' => 11]);
                }
                if(!isset($columnNames['idcontact'])){
                    $table->addColumn('idcontact', 'integer', [ 'default' => 1, 'null' => false, 'length' => 11]);
                }else{
                    $table->changeColumn('idcontact', 'integer', [ 'default' => 1, 'null' => false, 'length' => 11]);
                }
                if(!$table->exists()){
                    $table->addTimestamps();
                }
                if(!$table->exists() || !$table->hasIndexByName(idx_idcontact_idknowledge_knowledge)){
                    $table->addIndex(array (
          0 => 'idcontact',
          1 => 'idknowledge',
        ), array (
          'unique' => true,
          'name' => 'idx_idcontact_idknowledge_knowledge',
        ));
                }
                if($table->exists()) {
                    $table->update();
                } else {
                    $table->create();
                }
    }
}
