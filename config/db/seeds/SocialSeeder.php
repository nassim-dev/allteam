<?php /**
 * Class SocialSeeder
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class SocialSeeder extends Phinx\Seed\AbstractSeed
{
    public function getDependencies()
    {
        return array (
          0 => 'ContextSeeder',
        );
    }

    public function run(): void
    {
        $data = [array (
          'idcontext' => '',
          'date_birth' => '',
          'place_birth' => 'NULL',
          'flag_delete' => '0',
        )];
                $table = $this->table('social');
                $table->insert($data)
                    ->saveData();
                //$table"->truncate();
    }
}
