<?php /**
 * Class GdprSeeder
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class GdprSeeder extends Phinx\Seed\AbstractSeed
{
    public function getDependencies()
    {
        return array (
          0 => 'ContextSeeder',
          1 => 'UserSeeder',
        );
    }

    public function run(): void
    {
        $data = [array (
          'idcontext' => '',
          'iduser' => 1,
          'database_preferences' => 'NULL',
          'mailing_preferences' => 'NULL',
          'flag_delete' => '0',
        )];
                $table = $this->table('gdpr');
                $table->insert($data)
                    ->saveData();
                //$table"->truncate();
    }
}
