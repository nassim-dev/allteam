<?php /**
 * Class AddressSeeder
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class AddressSeeder extends Phinx\Seed\AbstractSeed
{
    public function getDependencies()
    {
        return array (
          0 => 'ContextSeeder',
        );
    }

    public function run(): void
    {
        $data = [array (
          'idcontext' => '',
          'name' => '',
          'street' => 'NULL',
          'city' => 'NULL',
          'zipcode' => 0,
          'country' => 'NULL',
          'geopoint' => 'NULL',
          'flag_delete' => '0',
        )];
                $table = $this->table('address');
                $table->insert($data)
                    ->saveData();
                //$table"->truncate();
    }
}
