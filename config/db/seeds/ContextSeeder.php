<?php /**
 * Class ContextSeeder
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class ContextSeeder extends Phinx\Seed\AbstractSeed
{
    public function getDependencies()
    {
        return array (
          0 => 'UserSeeder',
        );
    }

    public function run(): void
    {
        $data = [array (
          'iduser' => '',
          'name' => 'NULL',
          'status' => 'NULL',
          'flag_delete' => '0',
        )];
                $table = $this->table('context');
                $table->insert($data)
                    ->saveData();
                //$table"->truncate();
    }
}
