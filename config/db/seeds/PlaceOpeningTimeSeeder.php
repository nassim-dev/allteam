<?php /**
 * Class PlaceOpeningTimeSeeder
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class PlaceOpeningTimeSeeder extends Phinx\Seed\AbstractSeed
{
    public function getDependencies()
    {
        return array (
          0 => 'ContextSeeder',
        );
    }

    public function run(): void
    {
        $data = [array (
          'idcontext' => '',
          'hours' => '',
          'flag_delete' => '0',
        )];
                $table = $this->table('place_opening_time');
                $table->insert($data)
                    ->saveData();
                //$table"->truncate();
    }
}
