<?php /**
 * Class ContactSeeder
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class ContactSeeder extends Phinx\Seed\AbstractSeed
{
    public function getDependencies()
    {
        return array (
          0 => 'ContextSeeder',
          1 => 'UserSeeder',
          2 => 'SocialSeeder',
          3 => 'HiringSeeder',
        );
    }

    public function run(): void
    {
        $data = [array (
          'idcontext' => '',
          'iduser' => '',
          'idsocial' => '',
          'idhiring' => '',
          'photo' => 'NULL',
          'civility' => 'NULL',
          'pseudo' => '',
          'name' => 'NULL',
          'first_name' => 'NULL',
          'phone' => 'NULL',
          'email' => 'NULL',
          'tags' => 'NULL',
          'flag_delete' => '0',
        )];
                $table = $this->table('contact');
                $table->insert($data)
                    ->saveData();
                //$table"->truncate();
    }
}
