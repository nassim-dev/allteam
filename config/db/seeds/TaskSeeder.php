<?php /**
 * Class TaskSeeder
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class TaskSeeder extends Phinx\Seed\AbstractSeed
{
    public function getDependencies()
    {
        return array (
          0 => 'ContextSeeder',
          1 => 'OperationSeeder',
          2 => 'EventSeeder',
          3 => 'PlaceSeeder',
          4 => 'RoomSeeder',
        );
    }

    public function run(): void
    {
        $data = [array (
          'idcontext' => '',
          'name' => '',
          'idoperation' => '',
          'idevent' => '',
          'idplace' => '',
          'idroom' => '',
          'visual' => 'NULL',
          'lock' => '0',
          'status' => 'NULL',
          'description' => 'NULL',
          'schedule' => '',
          'tags' => 'NULL',
          'flag_delete' => '0',
        )];
                $table = $this->table('task');
                $table->insert($data)
                    ->saveData();
                //$table"->truncate();
    }
}
