<?php /**
 * Class PlaceTypeSeeder
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class PlaceTypeSeeder extends Phinx\Seed\AbstractSeed
{
    public function getDependencies()
    {
        return array (
          0 => 'ContextSeeder',
        );
    }

    public function run(): void
    {
        $data = [array (
          'idcontext' => '',
          'name' => '',
          'property' => 'NULL',
          'related_namespace' => 'NULL',
          'flag_delete' => '0',
        )];
                $table = $this->table('place_type');
                $table->insert($data)
                    ->saveData();
                //$table"->truncate();
    }
}
