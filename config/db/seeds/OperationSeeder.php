<?php /**
 * Class OperationSeeder
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class OperationSeeder extends Phinx\Seed\AbstractSeed
{
    public function getDependencies()
    {
        return array (
          0 => 'ContextSeeder',
          1 => 'OperationSeeder',
        );
    }

    public function run(): void
    {
        $data = [array (
          'idcontext' => '',
          'name' => '',
          'duration' => '',
          'room_reference' => 'NULL',
          'place_reference' => 'NULL',
          'visual' => 'NULL',
          'description' => 'NULL',
          'flag_delete' => '0',
          'idoperation_parent' => '',
        )];
                $table = $this->table('operation');
                $table->insert($data)
                    ->saveData();
                //$table"->truncate();
    }
}
