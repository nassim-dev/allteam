<?php /**
 * Class RoomSeeder
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class RoomSeeder extends Phinx\Seed\AbstractSeed
{
    public function getDependencies()
    {
        return array (
          0 => 'PlaceSeeder',
          1 => 'ContextSeeder',
          2 => 'Room_typeSeeder',
        );
    }

    public function run(): void
    {
        $data = [array (
          'idplace' => '',
          'idcontext' => '',
          'name' => '',
          'visual' => 'NULL',
          'description' => 'NULL',
          'tags' => 'NULL',
          'idroom_type' => '',
          'flag_delete' => '0',
        )];
                $table = $this->table('room');
                $table->insert($data)
                    ->saveData();
                //$table"->truncate();
    }
}
