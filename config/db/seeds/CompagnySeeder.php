<?php /**
 * Class CompagnySeeder
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class CompagnySeeder extends Phinx\Seed\AbstractSeed
{
    public function getDependencies()
    {
        return array (
          0 => 'ContextSeeder',
          1 => 'AddressSeeder',
          2 => 'Compagny_typeSeeder',
        );
    }

    public function run(): void
    {
        $data = [array (
          'idcontext' => '',
          'idaddress' => '',
          'idcompagny_type' => '',
          'name' => '',
          'visual' => 'NULL',
          'description' => 'NULL',
          'tags' => 'NULL',
          'flag_delete' => '0',
        )];
                $table = $this->table('compagny');
                $table->insert($data)
                    ->saveData();
                //$table"->truncate();
    }
}
