<?php /**
 * Class RoomEscapeSeeder
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class RoomEscapeSeeder extends Phinx\Seed\AbstractSeed
{
    public function getDependencies()
    {
        return array (
          0 => 'RoomSeeder',
          1 => 'ContextSeeder',
        );
    }

    public function run(): void
    {
        $data = [array (
          'idroom' => '',
          'idcontext' => '',
          'size' => 1,
          'flag_delete' => '0',
        )];
                $table = $this->table('room_escape');
                $table->insert($data)
                    ->saveData();
                //$table"->truncate();
    }
}
