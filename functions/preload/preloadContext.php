<?php

define('CONTEXT', (getenv('CONTEXT') !== '') ? getenv('CONTEXT') : 'CLI');
define('BASE_DIR', __DIR__ . '/../../');
set_time_limit(0);

require BASE_DIR . 'vendor/autoload.php';

use core\app\ApplicationException;
use core\app\ApplicationServiceInterface;
use core\app\Bootstrap;
use core\cache\RedisCache;
use core\DI\DependenciesInjector;
use core\logger\Log;
use core\tasks\AsyncJob;
use core\tasks\DbMigrationsTask;

$filename = BASE_DIR . 'tmp/cache/' . CONTEXT;
if (file_exists($filename)) {
    unlink($filename);
}

try {
    $bootstrap = new Bootstrap(
        applicationClass:ApplicationServiceInterface::class,
        container:['class' => DependenciesInjector::class, 'cache' => RedisCache::class],
        configClass:Config::class,
    );

    $bootstrap->startup(
        [
            'CONTEXT' => CONTEXT,
            'envFile' => 'config/env.neon'
        ]
    );

    require BASE_DIR . 'constants/default.php';
    require BASE_DIR . 'functions/default.php';


    /** @var ApplicationServiceInterface $app */
    $app = $bootstrap->buildApplication();

    $app->boot() //Startup actions
        ->process(); //Render view
    $app->shutdown(); //Execute shutdown callbacks


    if (CONTEXT === 'CLI') {
        /** @var AsyncJob $task */
        $task = $bootstrap->getDi()->singleton(AsyncJob::class, ['id' => '_bootExtensions']);
        $task->setCallable(DbMigrationsTask::class)
            ->save();
    }
} catch (ApplicationException $exception) {
    Log::dump(
        [
            'data'    => $exception->getMessage(),
            'logfile' => APP_LOGFILE
        ]
    );

    touch($filename);
    exit();
}

Log::dump(
    [
        'data'    => CONTEXT . ' loaded !',
        'logfile' => APP_LOGFILE
    ]
);

touch($filename);
exit();
