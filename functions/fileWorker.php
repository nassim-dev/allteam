<?php

/**
 * Worker process
 *
 * @category  Process
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
const CONTEXT = 'CLI';
define('BASE_DIR', __DIR__ . '/../');
set_time_limit(0);
require BASE_DIR . 'vendor/autoload.php';
require BASE_DIR . 'functions/default.php';
require BASE_DIR . 'constants/default.php';
require BASE_DIR . 'functions/cliFunctions.php';

use app\CommandLine;
use core\logger\LogServiceInterface;
use core\tasks\TaskException;
use core\workers\FileWorker;

/**
 * @return FileWorker
 */
function startWorker(): FileWorker
{
    /** @var CommandLine $cli */
    $cli = getCliEnvironement();
    /**
     * @var LogServiceInterface $logger
     */
    $logger = $cli->getServiceProvider()->getDi()->singleton(LogServiceInterface::class);


    try {
        /** @var FileWorker $worker */
        $worker = $cli->getServiceProvider()->getDi()->singleton(FileWorker::class);
        $logger->dump(
            [
                'data'    => 'Start fileworker PID ' . getmypid(),
                'logfile' => ASYNC_LOGFILE
            ]
        );
        while (true) {
            $worker->tick();
        }
    } catch (TaskException $e) {
        $logger->dump(
            [
                'data'    => $e->getMessage(),
                'logfile' => ASYNC_LOGFILE
            ]
        );
    }

    return $worker;
}

/**
 * @param FileWorker $worker
 *
 * @return void
 */
function onShutdownWorker(FileWorker $worker)
{
    /** @var CommandLine $cli */
    $cli = getCliEnvironement();

    /**
     * @var LogServiceInterface $logger
     */
    $logger = $cli->getServiceProvider()->getDi()->singleton(LogServiceInterface::class);

    $logger->dump(
        [
            'data'    => 'FileWorker PID ' . getmypid() . ' is shutting down, wait for pending tasks...',
            'logfile' => ASYNC_LOGFILE
        ]
    );

    while (!$worker->empty || $worker->isRunning) {
        usleep(1000);
    }

    unset($worker);

    $logger->dump(
        [
            'data'    => 'FileWorker PID ' . getmypid() . ' has stoped',
            'logfile' => ASYNC_LOGFILE
        ]
    );
}

$worker = startWorker();
register_shutdown_function('onShutdownWorker', $worker);
