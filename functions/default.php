<?php

use core\services\serializer\DescriptionInterface;

/**
 * Default functions
 * @category Function
 *
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
if (!function_exists('apache_request_headers')) {
    /**
     * @return array
     */
    function apache_request_headers()
    {
        $headers = [];
        foreach ($_SERVER as $key => $value) {
            if (substr($key, 0, 5) === 'HTTP_') {
                $headers[str_replace(' ', '-', ucwords(str_replace('_', ' ', strtolower(substr($key, 5)))))] = $value;
            }
        }

        return $headers;
    }
}
function setGettextLocale()
{
    $availableLocales = [
        'fr' => 'fr_FR',
        'es' => 'es_ES',
        'en' => 'en_US'
    ];

    //Get Session language
    $language = $_SESSION['__CONTAINERS__']['CONTEXT']['LANGUAGE'] ?? 'fr';
    if (is_subclass_of($language, DescriptionInterface::class)) {
        $language = $language->getDatas();
    }
    $locale = $availableLocales[$language] ?? $availableLocales['en'];

    //Set Gettext
    setlocale(LC_ALL, $locale);
    bindtextdomain('core', __DIR__ . '/../locales');
    bind_textdomain_codeset('core', 'UTF-8');
    textdomain('core');
}

setGettextLocale();

//Redirect if language is specified in url
//TODO move into a controller
$requestUri = $_SERVER['REQUEST_URI'] ?? null;
if ('/en' === $requestUri || '/fr' === $requestUri) {
    $_SESSION['__CONTAINERS__']['CONTEXT']['LANGUAGE'] = str_replace('//', '', $requestUri);
    header('Location:/');
}
