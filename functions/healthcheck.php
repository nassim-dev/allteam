<?php

define('BASE_DIR', __DIR__ . '/../');

$contexts = ['APP', 'API', 'CLI', 'HOOK'];
foreach ($contexts as $context) {
    $filename = BASE_DIR . 'tmp/cache/' . $context;
    if (!file_exists($filename)) {
        exit(1);
    }
}

exit(0);
