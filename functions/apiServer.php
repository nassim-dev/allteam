<?php

/**
 * Launch the api server
 * @category proccess
 *
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
set_time_limit(0);
const CONTEXT = 'API';
define('BASE_DIR', __DIR__ . '/../');

require BASE_DIR . 'vendor/autoload.php';
require BASE_DIR . 'functions/default.php';
require BASE_DIR . 'constants/default.php';
require BASE_DIR . 'functions/cliFunctions.php';


use core\app\ApiException;
use core\app\ApplicationServiceInterface;
use core\app\Bootstrap;
use core\cache\RedisCache;
use core\config\Config;
use core\DI\DependenciesInjector;
use core\events\EventManagerInterface;
use core\logger\LogServiceInterface;
use core\routing\DispatcherServiceInterface;
use core\secure\authentification\AuthServiceInterface;
use Swoole\Http\Request;
use Swoole\Http\Response;
use Swoole\Http\Server;

$server = null;

$bootstrap = new Bootstrap(
    applicationClass:ApplicationServiceInterface::class,
    container:['class' => DependenciesInjector::class, 'cache' => RedisCache::class],
    configClass:Config::class,
);

$bootstrap->startup(
    [
        'CONTEXT' => CONTEXT,
        'envFile' => 'config/env.neon'
    ]
);

$api = $bootstrap->buildApplication();

/**
 * Start api server
 *
 * @return void
 */
function startApi(ApplicationServiceInterface $api, ?Server $server)
{
    /**
     * @var LogServiceInterface $logger
     */
    $logger = $api->getServiceProvider()->getDi()->singleton(LogServiceInterface::class);

    try {
        /**
         * @var LogServiceInterface $logger
         */
        $logger = $api->getServiceProvider()->getDi()->singleton(LogServiceInterface::class);

        // Create a new server on this host and port, turn off SSL
        $server = new Server('localhost', 8080);
        $server->set(
            [
                'worker_num'          => 4,      // The number of worker processes to start
                'task_worker_num'     => 4,  // The amount of task workers to start
                'backlog'             => 128,       // TCP backlog connection number
                'open_http2_protocol' => true, // Enable HTTP2 protocol
            ]
        );

        /** @var EventManagerInterface $em */
        $em = $api->getServiceProvider()->getDi()->singleton(EventManagerInterface::class);
        $em->emit('API_PRE_BOOT');

        touch(BASE_DIR . 'tmp/cache/' . CONTEXT);

        $em->emit('API_POST_BOOT');

        // The main HTTP server request callback event, entry point for all incoming HTTP requests
        $server->on(
            'Request',
            function (Request $request, Response $response) use ($api, $logger) {
                $di = $api->getServiceProvider()->getDi();

                $logger->dump(
                    [
                        'data'    => 'Handle request : ' . $request->server['request_uri'],
                        'logfile' => API_LOGFILE
                    ]
                );

                /** @var DispatcherServiceInterface $dispatcher */
                $dispatcher = $di->singleton(DispatcherServiceInterface::class);

                /** @var AuthServiceInterface $auth */
                $auth = $di->singleton(AuthServiceInterface::class);

                $dispatcher->setRequest($request);
                $router = $dispatcher->dispatch();
                $router->run($auth, $api);

                /*$logger->dump(
                    [
                        'data'    => 'Response : ' . $api->process()->json(),
                        'logfile' => API_LOGFILE
                    ]
                );


                $logger->dump(
                    [
                        'data'    => 'Session : ' . json_encode($_SESSION, JSON_PRETTY_PRINT),
                        'logfile' => API_LOGFILE
                    ]
                );*/

                $appResponse = $api->process();
                foreach ($appResponse->getHeaders() as $name => $value) {
                    $exploded = explode(':', $value);
                    $response->setHeader($name, end($exploded));
                }

                $response->setStatusCode($appResponse->getCode());
                if ($appResponse->getFile() !== null) {
                    $response->sendfile($appResponse->getFile()->getRealPath());
                } else {
                    $response->end($appResponse->json());
                }
            }
        );

        $server->on(
            'Task',
            function (Server $server, $task_id, $reactorId, $data) use ($logger) {
                $logger->dump(
                    [
                        'data'    => 'Task Worker Process received data',
                        'logfile' => API_LOGFILE
                    ]
                );
                $logger->dump(
                    [
                        'data'    => '#' . $server->worker_id . ' onTask: [PID=' . $server->worker_pid . ']: task_id=' . $task_id . ', data_len=' . strlen($data),
                        'logfile' => API_LOGFILE
                    ]
                );

                $server->finish($data);
            }
        );

        // Triggered when new worker processes starts
        $server->on('WorkerStart', function ($server, $workerId) use ($logger) {
            $logger->dump(
                [
                    'data'    => "Worker started: $workerId",
                    'logfile' => API_LOGFILE
                ]
            );
        });

        // Triggered when the HTTP Server starts, connections are accepted after this callback is executed
        $server->on('Start', function () use ($logger) {
            $logger->dump(
                [
                    'data'    => 'Api server is listenening at http://127.0.0.1:8080 (public endpoint -> https://' . env('WEB_DOMAIN') . '/api)',
                    'logfile' => API_LOGFILE
                ]
            );
        });


        // Triggered when the server is shutting down
        $server->on('Shutdown', function () use ($logger) {
            $logger->dump(
                [
                    'data'    => 'Api server shutdown...',
                    'logfile' => API_LOGFILE
                ]
            );
        });

        // Triggered when worker processes are being stopped
        $server->on('WorkerStop', function ($server, $workerId) use ($logger) {
            $logger->dump(
                [
                    'data'    => "Worker stopped: $workerId",
                    'logfile' => API_LOGFILE
                ]
            );
        });

        $server->start();
    } catch (ApiException $e) {
        $logger->dump(
            [
                'data'    => $e->getMessage(),
                'logfile' => API_LOGFILE
            ]
        );
    }
}

function onShutdownApi(ApplicationServiceInterface $api, ?Server $server = null)
{
    $server?->shutdown();
    $server = null;
    startApi($api, $server);
}

register_shutdown_function('onShutdownApi', $api, $server);

startApi($api, $server);
