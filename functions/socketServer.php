<?php

set_time_limit(0);
/**
 * Launch the socket server
 * @category proccess
 *
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
const CONTEXT = 'CLI';
define('BASE_DIR', __DIR__ . '/../');

require BASE_DIR . 'vendor/autoload.php';
require BASE_DIR . 'functions/default.php';
require BASE_DIR . 'constants/default.php';
require BASE_DIR . 'functions/cliFunctions.php';

use app\CommandLine;
use core\config\Conf;
use core\logger\LogServiceInterface;
use core\logger\PsrLogAdapter;
use core\transport\websocket\SocketException;
use React\EventLoop\Loop;
use Thruway\Logging\Logger;
use Thruway\Peer\Router;
use Thruway\Transport\RatchetTransportProvider;

/**
 * Start socket server
 *
 * @return void
 */
function startSocket()
{
    /** @var CommandLine $cli */
    $cli = getCliEnvironement();

    /**
     * @var LogServiceInterface $logger
     */
    $logger = $cli->getServiceProvider()->getDi()->singleton(LogServiceInterface::class);

    Logger::set(new PsrLogAdapter($logger, WEBSOCKET_LOGFILE));

    try {
        $loop   = Loop::get();
        $router = new Router($loop);

        $transportProvider = new RatchetTransportProvider(Conf::find('SOCKET.WEBSOCKET_LOCAL_BIND'), Conf::find('SOCKET.WEBSOCKET_PORT'));

        $router->addTransportProvider($transportProvider);

        $router->start();
    } catch (SocketException $e) {
        $logger->dump(
            [
                'data'    => $e->getMessage(),
                'logfile' => WEBSOCKET_LOGFILE
            ]
        );
    }
}

function onShutdownSocket()
{
    startSocket();
}

register_shutdown_function('onShutdownSocket');

startSocket();
