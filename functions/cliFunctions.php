<?php

use core\app\ApplicationServiceInterface;
use core\app\Bootstrap;
use core\cache\RedisCache;
use core\DI\DependenciesInjector;

/**
 * @return ApplicationServiceInterface
 */
function getCliEnvironement(): ApplicationServiceInterface
{
    $bootstrap = new Bootstrap(
        applicationClass:ApplicationServiceInterface::class,
        container:['class' => DependenciesInjector::class, 'cache' => RedisCache::class],
        configClass:Config::class,
    );

    $bootstrap->startup(
        [
            'CONTEXT' => CONTEXT,
            'envFile' => 'config/env.neon'
        ]
    );

    $_SERVER['REQUEST_SCHEME'] = $bootstrap->getConfig()->find('PHP.HTTP_PROTOCOL');
    $_SERVER['SERVER_NAME']    = $bootstrap->getConfig()->find('PHP.SERVER_NAME');
    $_SERVER['HTTP_HOST']      = $bootstrap->getConfig()->find('PHP.SERVER_NAME');

    /** @var ApplicationServiceInterface $app */
    $app = $bootstrap->buildApplication();

    $app->boot() //Startup actions
        ->process(); //Render view

    return $app; //Execute shutdown callbacks
}
