<?php

/**
 * Worker process
 *
 * @category  Process
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
const CONTEXT = 'CLI';
define('BASE_DIR', __DIR__ . '/../');
set_time_limit(0);
require BASE_DIR . 'vendor/autoload.php';
require BASE_DIR . 'functions/default.php';
require BASE_DIR . 'constants/default.php';
require BASE_DIR . 'functions/cliFunctions.php';

use app\CommandLine;
use core\logger\LogServiceInterface;
use core\tasks\TaskException;
use core\workers\WorkerInterface;

/**
 * @return WorkerInterface
 */
function startWorker(): WorkerInterface
{
    /** @var CommandLine $cli */
    $cli = getCliEnvironement();
    /**
     * @var LogServiceInterface $logger
     */
    $logger = $cli->getServiceProvider()->getDi()->singleton(LogServiceInterface::class);


    try {
        /** @var WorkerInterface $worker */
        $worker = $cli->getServiceProvider()->getDi()->singleton(WorkerInterface::class);
        $logger->dump(
            [
                'data'    => 'Start worker PID ' . getmypid(),
                'logfile' => ASYNC_LOGFILE
            ]
        );
        while (true) {
            $worker->tick();
        }
    } catch (TaskException $e) {
        $logger->dump(
            [
                'data'    => $e->getMessage(),
                'logfile' => ASYNC_LOGFILE
            ]
        );
    }

    return $worker;
}

/**
 * @param WorkerInterface $worker
 *
 * @return void
 */
function onShutdownWorker(WorkerInterface $worker)
{
    /** @var CommandLine $cli */
    $cli = getCliEnvironement();

    /**
     * @var LogServiceInterface $logger
     */
    $logger = $cli->getServiceProvider()->getDi()->singleton(LogServiceInterface::class);

    $logger->dump(
        [
            'data'    => 'Worker PID ' . getmypid() . ' is shutting down, wait for pending tasks...',
            'logfile' => ASYNC_LOGFILE
        ]
    );

    while (!$worker->empty || $worker->isRunning) {
        usleep(1000);
    }

    unset($worker);

    $logger->dump(
        [
            'data'    => 'Worker PID ' . getmypid() . ' has stoped',
            'logfile' => ASYNC_LOGFILE
        ]
    );
}

$worker = startWorker();
register_shutdown_function('onShutdownWorker', $worker);
