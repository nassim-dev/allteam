<?php

/**
 * Launch the socket server
 * @category proccess
 *
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
set_time_limit(0);
const CONTEXT = 'CLI';
define('BASE_DIR', __DIR__ . '/../');

require BASE_DIR . 'vendor/autoload.php';
require BASE_DIR . 'functions/default.php';
require BASE_DIR . 'constants/default.php';
require BASE_DIR . 'functions/cliFunctions.php';

use app\CommandLine;
use core\config\Conf;
use core\logger\LogServiceInterface;
use core\logger\PsrLogAdapter;
use core\messages\queue\MessageQueueInterface;
use core\transport\websocket\Publisher;
use core\transport\websocket\SocketClient;
use core\transport\websocket\SocketException;
use Thruway\ClientSession;
use Thruway\Logging\Logger;
use Thruway\Message\PublishMessage;
use Thruway\Peer\Client;
use Thruway\Transport\PawlTransportProvider;

/**
 * Start socket server
 *
 * @return void
 */
function startSocketClient()
{
    /** @var CommandLine $cli */
    $cli = getCliEnvironement();

    /**
     * @var LogServiceInterface $logger
     */
    $logger = $cli->getServiceProvider()->getDi()->singleton(LogServiceInterface::class);

    /** @var MessageQueueInterface $queue */
    $queue = $cli->getServiceProvider()->getDi()->singleton(MessageQueueInterface::class);

    Logger::set(new PsrLogAdapter($logger, WEBSOCKET_LOGFILE));

    try {
        $client = new Client('default.domain');
        //$client->addRole(new Publisher($logger));

        $client->addTransportProvider(new PawlTransportProvider('ws://' . Conf::find('SOCKET.WEBSOCKET_LOCAL_BIND') . ':' . Conf::find('SOCKET.WEBSOCKET_PORT')));


        $client->on('open', function (ClientSession $session) use ($queue, $logger, $client) {
            $logger->dump(
                [
                    'data'    => 'Start backoffice client identified by [' . $client->getRealm() . ']',
                    'logfile' => WEBSOCKET_LOGFILE
                ]
            );

            $session->getLoop()->addPeriodicTimer(0.1, function () use ($session, $queue) {
                $message = $queue->pull([SocketClient::class]);
                if ($message !== null) {
                    /** @var PublishMessage $message */
                    $session->sendMessage($message);
                }
            });
        });

        $client->start();
    } catch (SocketException $e) {
        $logger->dump(
            [
                'data'    => $e->getMessage(),
                'logfile' => WEBSOCKET_LOGFILE
            ]
        );
    }
}

function onShutdownSocketClient()
{
    startSocketClient();
}

register_shutdown_function('onShutdownSocketClient');

startSocketClient();
