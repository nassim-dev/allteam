<?php

opcache_reset();
set_time_limit(0);
const CONTEXT = 'CLI';
const SIGTERM = 15;

define('BASE_DIR', __DIR__ . '/../');

require BASE_DIR . 'vendor/autoload.php';
require BASE_DIR . 'functions/default.php';
require BASE_DIR . 'constants/default.php';

use core\cache\RedisCache;
use core\config\Config;
use core\logger\FileLogger;
use core\logger\LoggerManager;
use core\logger\LogServiceInterface;
use core\utils\Pipeline;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

$workers     = new SplObjectStorage();
$loaders     = new SplObjectStorage();
$fileWorkers = new SplObjectStorage();

$logger = new LoggerManager();
$logger->registerLogger(new FileLogger());

try {
    $cache = new RedisCache(...RedisCache::defaultConfig());
    $cache->clearAll();

    $logger->dump(
        [
            'data'    => 'Cache cleared !',
            'logfile' => APP_LOGFILE
        ]
    );
} catch (ProcessFailedException $e) {
    $logger->dump(
        [
            'data'    => $e->getMessage(),
            'logfile' => APP_LOGFILE
        ]
    );
}


function preloadContext(string $context, LogServiceInterface $logger, SplObjectStorage $loaders)
{
    $logger->dump(
        [
            'data'    => "Preload $context context...",
            'logfile' => APP_LOGFILE
        ]
    );

    try {
        $process = new Process(
            [
                '/usr/local/bin/php',
                '--define',
                'memory_limit=128M',
                BASE_DIR . 'functions/preload/preloadContext.php'
            ]
        );
        $process->setTimeout(3600);
        $process->start(null, ['CONTEXT' => $context]);
        $loaders->attach($process, $process->getPid());

        $time = time();
        while ($process->isRunning()) {
            sleep(1);
        }

        $logger->dump(
            [
                'data'    => $process->getCommandLine() . ' complete in : ' . time() - $time . 's',
                'logfile' => APP_LOGFILE
            ]
        );

        $loaders->detach($process);
        unset($process);
    } catch (ProcessFailedException $e) {
        $logger->dump(
            [
                'data'    => $e->getMessage(),
                'logfile' => APP_LOGFILE
            ]
        );
    }
}
/**
 * @param array $workersPids
 *
 * @return void
 */
function startWorkerManager(SplObjectStorage $workers, SplObjectStorage $fileWorkers, LogServiceInterface $logger)
{
    $cache  = new RedisCache(...RedisCache::defaultConfig());
    $config = new Config('config/env.neon', $cache);

    $maxWorkers = $config->find('WORKERS.MAX_WORKERS');

    $logger->dump(
        [
            'data'    => 'Worker Manager PID ' . getmypid() . ' is started with limit : ' . $maxWorkers . ' worker(s) simultaneous',
            'logfile' => WORKER_LOGFILE
        ]
    );


    while (true) {
        if ($workers->count() < $maxWorkers) {
            try {
                $process = new Process(
                    [
                        '/usr/local/bin/php',
                        '--define',
                        'memory_limit=128M',
                        BASE_DIR . 'functions/backgroundWorker.php'
                    ]
                );
                $workers->attach($process);
                $process->start();
            } catch (ProcessFailedException $e) {
                $logger->dump(
                    [
                        'data'    => $e->getMessage(),
                        'logfile' => WORKER_LOGFILE
                    ]
                );
            }


            $logger->dump(
                [
                    'data'    => 'New worker attached identified by PID ' . $process->getPid() . ' added !',
                    'logfile' => WORKER_LOGFILE
                ]
            );

            if ($process->getOutput() !== '') {
                $logger->dump(
                    [
                        'data'    => $process->getOutput(),
                        'logfile' => WORKER_LOGFILE
                    ]
                );
            }
        }
        usleep(rand(5, 10000));
        sleep(1);
        /** @var Process $process  */
        foreach ($workers as $process) {
            if ($process->isTerminated()) {
                $logger->dump(
                    [
                        'data'    => $process->getErrorOutput(),
                        'logfile' => WORKER_LOGFILE
                    ]
                );
                $process->stop(5, SIGTERM);
                $workers->detach($process);
                unset($process);
            }
        }
        if ($fileWorkers->count() < 1) {
            try {
                $process = new Process(
                    [
                        '/usr/local/bin/php',
                        '--define',
                        'memory_limit=128M',
                        BASE_DIR . 'functions/fileWorker.php'
                    ]
                );
                $fileWorkers->attach($process);
                $process->start();
            } catch (ProcessFailedException $e) {
                $logger->dump(
                    [
                        'data'    => $e->getMessage(),
                        'logfile' => WORKER_LOGFILE
                    ]
                );
            }


            $logger->dump(
                [
                    'data'    => 'New fileworker attached identified by PID ' . $process->getPid() . ' added !',
                    'logfile' => WORKER_LOGFILE
                ]
            );

            if ($process->getOutput() !== '') {
                $logger->dump(
                    [
                        'data'    => $process->getOutput(),
                        'logfile' => WORKER_LOGFILE
                    ]
                );
            }
        }

        /** @var Process $process  */
        foreach ($fileWorkers as $process) {
            if ($process->isTerminated()) {
                $logger->dump(
                    [
                        'data'    => $process->getErrorOutput(),
                        'logfile' => WORKER_LOGFILE
                    ]
                );
                $process->stop(5, SIGTERM);
                $fileWorkers->detach($process);
                unset($process);
            }
        }
    }
}

/**
 * @param array $workersPids
 *
 * @return void
 */
function onShutdownWorkerManager(SplObjectStorage $workers, SplObjectStorage $fileWorkers)
{
    $logger = new LoggerManager();
    $logger->registerLogger(new FileLogger());

    $logger->dump(
        [
            'data'    => 'Worker Manager PID ' . getmypid() . ' is off',
            'logfile' => WORKER_LOGFILE
        ]
    );

    /** @var Process $process  */
    foreach ($workers as $process) {
        $process->stop(5, SIGTERM);
        $workers->detach($process);
        unset($process);
    }

    startWorkerManager($workers, $fileWorkers, $logger);
}

$pipeline = new Pipeline();

$pipeline->pipe(function ($logger, $loaders) {
    preloadContext('CLI', $logger, $loaders);
}, $logger, $loaders)
    ->pipe(function ($logger, $loaders) {
        preloadContext('APP', $logger, $loaders);
    }, $logger, $loaders)
    ->pipe(function ($logger, $loaders) {
        preloadContext('API', $logger, $loaders);
    }, $logger, $loaders)
    ->pipe(function ($logger, $loaders) {
        preloadContext('HOOK', $logger, $loaders);
    }, $logger, $loaders)
    ->pipe(function ($workers, $fileWorkers, $logger) {
        startWorkerManager($workers, $fileWorkers, $logger);
    }, $workers, $fileWorkers, $logger)
    ->execute();

register_shutdown_function('onShutdownWorkerManager', $workers, $fileWorkers);
