# AllTeam

---

<img alt="Logo" align="right" src="/public/images/logo-color-01-notitle.png" width="20%" />

- Contributors: Nassim Ourami <nassim.ourami@mailo.com>
- Requires at least: PHP 8
- Actual version tag: 0.2
- https://gitlab.com/Tricksoft/allteam

---

<a href='https://ind.ie/ethical-design'><img style='margin-left: auto; margin-right: auto;' alt='We practice Ethical Design' src='https://img.shields.io/badge/Ethical_Design-_▲_❤_-blue.svg'></a> <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a>

## Contents

- [History](#history)
- [Description](#description)
- [Todo list](#todo-list)
- [User documentation](#user-documentation)
- [Installation](#installation)
- [Contribution guide](#contribution-guide)
- [Developper guide](#developper-guide)

  - [Coding style](#coding-style)
  - [Configuration](#configuration)
  - [Bootstraping](#bootstraping)
  - [Application](#application)
  - [Dependencies injection](#dependencies-injection)
  - [Facades & Providers](#facades--providers)
  - [Authentification & Security](#authentification--security)
  - [Async jobs](#async-jobs)
  - [Workers](#workers)
  - [Cron jobs](#cron-jobs)
  - [API](#api)
  - [Websocket server](#websocket-server)
  - [Logger](#logger)
  - [Php Session](#php-session)
  - [Notification](#notification)
  - [Routing](#routing)
  - [Cache](#cache)
  - [Html elements](#html-elements)
  - [Database](#database)
  - [Object storage](#object-storage)
  - [Serializer & Encryption](#serializer)
  - [Utils](#utils)
  - [Extensions](#extensions)
  - [Frontend](#frontend)

    - [Configuration](#configuration-manager)
    - [Main application](#main-application)
    - [Events](#events-listeners)
    - [Ajax](#ajax)
    - [Websocket](#websocket)
    - [Store](#store)
    - [Database](#database--localstorage)
    - [Machine state](#machine-state)
    - [Components](#components)

  - [Translations](#translations)
  - [Unit tests](#unit-tests)

- [File structure](#file-structure)
- [Commit rules](#commit-rules)
- [Code of conduct](#code-of-conduct)
- [Stack used](#stack-used)
- [Recommanded tools to manage project](#recommanded-tools-to-manage-project)
- [Frequently Asked Questions](#frequently-asked-questions)
- [Screenshots](#screenshots)
- [Changelog](#changelog)

---

### History

I was managing an event space in Paris and i needed a tool to simplify our event production processes (management of deadlines and technical needs).

So i decided to push the development of a custom tool to make it collaborative and complete! We had been looking for a complete open source solution, and at the time had not found a satisfactory tool.
The positive feedback from my collaborators and the time savings from the first version used only internally, confirmed that it was necessary to offer our solution to the public.

<figure class="video_container">
    <video width="100%" controls="true" allowfullscreen="true" poster="public/images/cover.png"">
    <source src="public/videos/video-allteam-event-project-manager.mp4" type="video/mp4">
    </video>
</figure>

As a technical director in multiple shows, i was the first user of this tool.

The first project (Tricks'oft) has been completely rewritten to make it more modular and to allow end users to contribute easily to the development of specific features.

This repo **is not a working app** but it's the base code that i'm actualy rewritting. It's a long job...
This project was an opportunity for me to learn different languages, in particular PHP and Javascript. I started without framework, you'll find in this repository many pieces that could one day be used to create one. So yes, I have reinvented the wheel for my training =)

For now, i use some pices of Symphony and Nette Framework. And i plan to migrate everything one of them.
I hope a version 1.0 will be available soon.

---

### Description

Allteam is a versatile digital tool that allows to structure the management of any type of event project in order to gain in productivity.

It is presented as a platform allowing the different teams to use a common tool in order to avoid any loss of information to involve all your collaborators in a collective production dynamic.

---

### Todo list

- [ ] Clean and refactor code
- [x] Fix documentation menu
- [ ] Create a marketplace system
- [ ] Build an offline app
- [ ] Write unit tests
- [ ] Update documentation

---

### User documentation

See documentation website <https://doc.allteam.io/>

---

### Installation

With docker

```shell
sudo apt-get install docker-ce
sudo docker run -d -p 8080:80 allteam
```

Now you can access at <http://localhost:8080/> from your host system.

or with docker-compose (See [here](#build-docker-containers-with-docker-compose))

---

### Development

---

#### Get source code

```shell
git clone https://gitlab.com/Tricksoft/allteam.git
cd allteam
```

---

#### Deploy environnement packagers & tools (npm, yarn, gulp)

```shell
sudo tools/build-dev
```

---

#### Get dependencies

```shell
yarn
tools/bin/composer install
```

---

#### Build docker containers with docker-compose

```shell
sudo apt-get install docker-ce docker-compose -y
```

## You should modify stack config in **[tools/docker/config/config.env](tools/docker/config/config.env)** file then execute the command below

---

##### Traefik

Generate a new couple user/password with this command, and put the result in **TRAEFIK_USER_CREDENCIAL**

```shell
echo $(htpasswd -nb <USER> <PASSWORD>) | sed -e s/\\$/\\$\\$/g
#then replace double # by simple #
```

You could define a traefik resolver :

- empty : self signed certificate
- letsencrypt

To configure the default resolver, see in **[tools/docker/traefik/config/traefik.yml](tools/docker/traefik/config/traefik.yml)**

Dont forget to define your domains :

- **DOMAIN_NAME** (required)
- **TRAEFIK_DOMAIN** (required)
- **WEB_DOMAIN** (required)
- **WEBSOCKET_DOMAIN** (required)
- MYSQL_DOMAIN
- PHPMYADMIN_DOMAIN
- SONAR_DOMAIN
- PHPMEMCACHEADMIN_DOMAIN
- PHPREDISADMIN_DOMAIN
- MAILCATCHER_DOMAIN
- MONGOEXPRESS_DOMAIN
- CADVISOR_DOMAIN
- PROMETHEUS_DOMAIN
- GRAFANA_DOMAIN
- POSTHOG_DOMAIN
- FATHOM_DOMAIN
- PORTAINER_DOMAIN
- KIBANA_DOMAIN
- METABASE_DOMAIN

---

##### POSTHOG

**SECRET_KEY** must be defined with the result of the following command

```shell
openssl rand -hex 32
```

---

##### FATHOM

**FATHOM_SECRET** must be defined with the result of the following command

```shell
openssl rand -hex 32
```

---

##### PORTAINER

**PORTAINER_CREDENCIAL** must be defined with the result of the following command

```shell
htpasswd -nb -B "you-username" "your-password" | cut -d ":" -f 2
```

---

```shell
sudo docker network create allteam
sudo docker network create logs
sudo tools/docker/start --build --dev --monitoring --ssl --clean
```

You could register the necessary subdomains

```shell
sudo cat tools/docker/config/hosts >> /etc/hosts
```

Now you can access at <https://allteam.localhost/> from your host system.

You can use theses arguments :

| Argument     | Description                                        |
| ------------ | -------------------------------------------------- |
| --ssl        | Generate ssl certificates for localhost sudo       |
| --clean      | Clean unused image and container :warning:         |
| --mount-s3   | Mount S3 Storage into the stack (in php container) |
| --upgrade    | Used in production only                            |
| --build      | Build the default stack                            |
| --stats      | Start stats tools                                  |
| --dev        | Start development tools                            |
| --debug      | Start debug tools                                  |
| --monitoring | Start monitoring tools                             |
| --restart    | Restart php services                               |

To stop the stack, use this command

```shell
sudo tools/docker/stop
```

You can use theses arguments

!!!Note: :warning: It will remove all your stoped containers and related image :warning:

| Argument          | Description                       |
| ----------------- | --------------------------------- |
| --clean-images    | Clean unused images :warning:     |
| --clean-container | Clean unused containers :warning: |
| --clean-all       | Clean all :warning:               |
| --main            | Stop Main tools                   |
| --dev             | Stop Developement tools           |
| --stats           | Stop Stats tools                  |
| --monitoring      | Stop Monitoring tools             |
| --all             | Stop All containers               |

##### Bundle assets for development (assets are build from src to public/dist)

```shell
yarn build-dev
```

##### Bundle assets for production (assets are build from src to public/dist)

```shell
yarn build
```

##### Launch vitejs server with HMR to bundle on change

```shell
yarn dev
```

##### Launch live server to preview result

```shell
yarn serve
```

##### Launch development environnment

```shell
sudo tools/launch-dev
```

You can use theses arguments :

| Argument     | Description                                                                                                             |
| ------------ | ----------------------------------------------------------------------------------------------------------------------- |
| --ssl        | Generate ssl certificates for localhost sudo                                                                            |
| --clean      | Clean unused image and container :warning:                                                                              |
| --mount-s3   | Mount S3 Storage into the stack (in php container)                                                                      |
| --upgrade    | Used in production only                                                                                                 |
| --build      | Build the default stack                                                                                                 |
| --stats      | Start stats environnement (fathom, posthog, metabase)                                                                   |
| --dev        | Start development environnement (blackfire, mongo-express, phpmyadmin, php-redis-admin, mailcatcher, phpmemcachedadmin) |
| --debug      | Start debug stack (sonarqube, blackfire)                                                                                |
| --monitoring | Start monitoring stack (prometheus, grafana, cadvisor, filebeat, elasticsearch, logstash, kibana)                       |
| --restart    | Restart php services                                                                                                    |

!!!Note: :warning: --clean, --cleanAll --clean-images, --clean-container options will remove all your stoped containers and related image :warning:

Now you can access at <https://allteam.localhost/> from your host system.

---

#### Docker-compose description

| File                          | Usage               |
| ----------------------------- | ------------------- |
| docker-compose.yml            | Required containers |
| docker-compose-dev.yml        | Developpement tools |
| docker-compose-monitoring.yml | Monitoring tools    |
| docker-compose-stats.yml      | Statistics tools    |
| docker-compose-debug.yml      | Debugging tools     |

---

### Contribution guide

---

### Developper guide

_When we specifie an interface in the current document, consider the main implementation_
_All facade could be used as regular classes ([See Facades & Providers](#facades--providers))_

---

#### Console

The console tool provides some usefull command stored in **./console**

```shell
./tools/bin/console command
```

| Command                    | Usage                       |
| -------------------------- | --------------------------- |
| app:build-extensions       | Build extensions manifest   |
| app:build-proxy            | Regenerate proxy classes    |
| app:create-entitie         | Create a new entitie        |
| app:generate-kahlan-tests  | Generate kahlan specs       |
| app:generate-phpunit-tests | Generate phpunit tests      |
| app:initialize             | Generate multiples entities |
| app:migrate-database       | Execute database migrations |
| app:phpcs                  | Fix php style               |
| app:rector                 | Fix php rules               |
| app:rollback-database      | Rollback database           |
| app:seed-database          | Execute database seeders    |
| app:unit-test              | Execute unit test           |
| app:update-facades         | Update facade phpdoc        |

---

#### Coding style

In order to format you code, you could use this command

```shell
./tools/rector/vendor/bin/rector process --dry-run
```

If you are ok with the changes, you can applies them

```shell
./tools/rector/vendor/bin/rector process
```

---

#### Configuration

You could find default implementations in **[config/config\_{context}.neon](config/config_app.neon)**.
This file is loaded by **[core\DI\DependencieInjectorInterface](core/DI/DependencieInjectorInterface.php)**

The config could be accessed by **[core\config\Config](core/config/Config.php)** class.

Imagine a **[config/env.neon](config/env.neon)** like this

```neon
ENVIRONNEMENT: "%env:ENVIRONNEMENT"
VERSION: "%env:VERSION"
WORKERS:
    MAX_WORKERS: 2
    MAX_TASK_RETRY: 3
```

You could access individual value like this :

```php
$config = core\DI\DI::singleton(Config::class);
$config->find("ENVIRONNEMENT");
$config->find("WORKERS.MAX_WORKERS");
```

The default environnement variables are stored in **[config/env.neon](config/env.neon)** and **extensions/extensionName/config/env.neon**
The special shortcut \*\*%env:VARIABLE_NAME\*\* is used to get host environnement variables in config file.

All environnement variables for containers are defined in **[tools/docker/config/config.env](tools/docker/config/config.env)**

---

#### Bootstraping

The **[core\app\BootstrapInterface](core/app/BootstrapInterface.php)** class is used to build the main **[core\app\ApplicationServiceInterface](core/app/ApplicationServiceInterface.php)**
It is called on all application endpoints :

- /public/index.php : Frontend endpoint
- /public/api/index.php : Api endpoint
- /public/hooks/index.php : Hooks endpoint

And in every backend scrips (cron, socket-server, background jobs)
Here an example :

```php
$bootstrap = new Bootstrap(
    applicationClass:ApplicationServiceInterface::class,
    container:['class' => DependenciesInjector::class, 'cache' => FileCache::class],
    configClass:Config::class,
    eventListeners:[],
    middlewares: []
);

$bootstrap->startup(
    [
        'context'  => CONTEXT,
        'env_file' => 'config/env.neon'
    ]
);

/** @var ApplicationServiceInterface $app */
$app = $bootstrap->buildApplication();

$app->boot() //Startup actions
    ->process(); //Render view

$app->shutdown(); //Then execute shutdown callbacks
```

It will create :

- **[core\DI\DependencieInjectorInterface](core/DI/DependencieInjectorInterface.php)**
- **[core\cache\CacheServiceInterface](core/cache/CacheServiceInterface.php)**

It starts :

- **[core\config\Config](core/config/Config.php)** and initialize configurations
- **[core\events\EventManagerInterface](core/events/EventManagerInterface.php)** and register classes listeners

If middleswares are provied :

- **[core\middleware\MiddlewareDispatcher](core/middleware/MiddlewareDispatcher.php)** will execute all piped middlewarre

Then the **[core\app\ApplicationServiceInterface](core/app/ApplicationServiceInterface.php)** is launched.

---

#### Application

There is 3 implementations of **[core\app\ApplicationServiceInterface](core/app/ApplicationServiceInterface.php)**, depending on the context :

- **[app\Api](app/Api.php)** for API context
- **[app\App](app/App.php)** default context
- **[app\CommandLine](app/CommandLine.php)** for command line context

You could create new implementations, that must extends **[core\app\AbstractApplication](core/app/AbstractApplication.php)** if you do not want to rewrite default methods.

##### Application workflow

###### core\app\ApplicationServiceInterface::build()

1. Trigger **{CONTEXT}\_PRE_BUILD** event
2. Register main application listeners
3. Register main application controllers, then extensions controllers
4. Build and boot extensions (Register extensions controllers, listeners, factories, entities, widgets...)
5. Register main application controllers
6. Trigger **{CONTEXT}\_POST_BUILD** event

###### core\app\ApplicationServiceInterface::boot()

Main application startup (see exemple in core\app\ApplicationServiceInterface implementations)
Usually :

1. Trigger **{CONTEXT}\_PRE_BOOT** event
2. Get http request, and prepare http response
3. Get **[core\routing\DispatcherServiceInterface](core/routing/DispatcherServiceInterface.php)**, find the **[core\controller\ControllerInterface](core/controller/ControllerInterface.php)** and build the routes **[core\routing\Route](core/routing/Route.php)**
4. Authentification & security (check if user can be routed to controller's method) with **[core\secure\authentification\AuthServiceInterface](core/secure/authentification/AuthServiceInterface.php)**
5. Get **[core\routing\RouterServiceInterface](core/routing/RouterServiceInterface.php)** and execute target method
6. Execute specifics actions (build and assign widgets to view for example)
7. Trigger **{CONTEXT}\_POST_BOOT** event

###### core\app\ApplicationServiceInterface::render()

1. Trigger **{CONTEXT}\_POST_RENDER** event
2. Render the **[core\view\ViewServiceInterface](core/view/ViewServiceInterface.php)** or return the response.
3. Trigger **{CONTEXT}\_POST_RENDER** event

---

#### Dependencies injection

In order to prenvent duplicated services, object creation is always managed by **[core\DI\DependencieInjectorInterface](core/DI/DependencieInjectorInterface.php)** that provide some useful methods to build your component in a smart way.
This component will use specific factories to build object.
Basically, you can use one these methods to have autowiring working (check inside interface for more information) :

```php
core\DI\DI::make(string $class, array $parameters = []);
core\DI\DI::singleton(string $class, array $parameters = []);
core\DI\DI::call($object, string $methodName, array $parameters = []);
core\DI\DI::callInside($object, string $methodName, array $parameters = []);
core\DI\DI::callable(callable $callable, array $parameters = [])
```

Each builded object will have to properties added :

- di : [Weak reference](https://www.php.net/manual/en/class.weakreference.php) to DependencieInjectorInterface object
- diWrapper : a closure used for "call" and "callInside" methods

To configure build mode, you could use a special parameter like this :

```php
core\DI\DI::singleton(AuthServiceInterface::class, [
    core\DI\Context::__BUILD_MODE__ => [
            'main'         => core\DI\Context::SINGLETON, //or core\DI\Context::UNIQUE
            'dependencies' => core\DI\Context::SINGLETON //or core\DI\Context::UNIQUE
    ]
];

```

Interfaces mapping are stored under **[config/config\_{CONTEXT}.neon](config/config_app.neon)**, but if you want a specific build config for a method, you can use InjectionAttribute.

```php
#[InjectionAttribute(ViewServiceInterface::class, build:InjectionAttribute::UNIQUE)]
public function getHtml(?ViewServiceInterface $view = null, ?string $template = null): string
{
}
```

or a specific implementation

```php
#[InjectionAttribute(ViewServiceInterface::class, implementation:LatteView::class)]
public function getHtml(?ViewServiceInterface $view = null, ?string $template = null): string
{
}
```

!!!Note: You could always build your object with the **"new"** keyword, but it's not encouraged exept if : you don't need to retrieve an object, or you don't want to use singleton services, or you don't need autowiring.

##### Rebuild proxy classes

Dependencie injector container build class proxy on demand for each loaded class. It's possible to rebuild all classes with this command

```shell
docker exec -it php php -f /var/www/html/tools/bin/console app:build-proxy
```

!!! Note: app:build-proxy must be called inside the php container, because redis and memcached are needed

---

#### Facades & Providers

Each service has a facade and a provider as trait.

For example, you could use for **[core\secure\authentification\AuthServiceInterface](core/secure/authentification/AuthServiceInterface.php)**
**The facade**

```php
core\secure\authentification\Auth::getUser();
```

**The service provided by dependencie injection**

```php
class MyClass {
    public function __construct(private AuthServiceInterface $auth){

    }

    public function myMethod(){
        $this->auth->getUser();
    }
}
```

**The service provided by provider trait**

```php
class MyClass {

    use core\secure\authentification\AuthProvider;

    public function myMethod(){
        $this->getAuth()->getUser();
    }
}
```

---

#### Authentification & Security

**[core\secure\authentification\AuthServiceInterface](core/secure/authentification/AuthServiceInterface.php)** default implementation : **[core\secure\authentification\AuthService](core/secure/authentification/AuthService.php)**
It's the entrypoint that manage authentification; it will use some other components :

- **[core\secure\authentification\components\AuthComponentInterface](core/secure/authentification/components/AuthComponentInterface.php)** : to acces to users storage and check password validity
- **[core\secure\permissions\PermissionServiceInterface](core/secure/permissions/PermissionServiceInterface.php)** : to manage and check permissions
- **[core\secure\context\ContextInterface](core/secure/context/ContextInterface.php)** : represents a context element (a specific instance)
- **[core\secure\roles\RoleInterface](core/secure/roles/RoleInterface.php)** : represents role componenent (containing multiples permissions)
- **[core\secure\permissions\PermissionInterface](core/secure/permissions/PermissionInterface.php)** : represents permission component
- **[core\secure\user\UserInterface](core/secure/user/UserInterface.php)** : represents user component (with roles and permissions)
- **[core\secure\user\UserTrait](core/secure/user/UserTrait.php)** could be useful to implementing a custom class;
- **[core\secure\component\RessourceInterface](core/secure/component/RessourceInterface.php)** : ressource to be checked (could be a route, an html widget and more...)
- **[core\secure\csrf\CsrfServiceInterface](core/secure/csrf/CsrfServiceInterface.php)** : manage csrf tokens

Permissions, roles, users, contexts, have their entity class in order to be saved in a database.

Each class implementing **[core\secure\components\RessourceInterface](core/secure/components/RessourceInterface.php)** is able to be checked. The facilitator **[core\secure\components\RessourceTrait](core/secure/components/RessourceTrait.php)** could be used to implementing the main methods.

Let's look this example :

```php
$form = new Form();
$form->getPermissionSet()->addRequiredCondition(core\secure\permissions\PermissionInterface::class);

$auth = core\DI\DI::singleton(core\secure\authentification\AuthServiceInterface::class);
$auth->getPermissionManager()->allow(core\secure\permissions\PermissionInterface::class, $auth->getUser()); //Add new permission
$auth->getPermissionManager()->deny(core\secure\permissions\PermissionInterface::class, $auth->getUser()); //Remove permission

if($auth->isAllowed($form, $auth->getUser())){
    //Do some actions
}

//Then you could persist permissions, but it's automaticly called after AuthServiceInterface->__destruct()
$auth->getPermissionManager()->persist($auth->getUser());

```

---

#### Async jobs

Job task are custom class implementing **[core\tasks\TaskInterface](core/tasks/TaskInterface.php)**
To create a new async task, create a **[core\tasks\AsyncJob](core/tasks/AsyncJob.php)**

```php

$task = core\DI\DI::make(core\tasks\AsyncJob::class, ['id' => 'identifier']);
$task->setCallable(core\tasks\TaskInterface::class); //Use a real class
$task->save(); //call async
$task->run(); //or call sync
```

##### Workers

A specific php container named **worker** is running in background, at launch it start a process from **[functions/workerManager.php](functions/workerManager.php)**.
Each worker is launched in a specific process from **[functions/backgroundWorker.php](functions/backgroundWorker.php)**

A worker need to implements **[core\workers\WorkerInterface](core/workers/WorkerInterface.php)**

By default there is 2 running workers :

- **[core\workers\Worker](core/workers/Worker.php)** to running async job
- **[core\workers\FileWorker](core/workers/FileWorker.php)** to execute file operation

FileWorker is used to aync file sync, like modifiying config files :

```php
//It will modify the [SECURITY][JSRPC][KEY] config element
Conf::saveInFile(BASE_DIR . 'config/env.neon', 'SECURITY.JSONRPC.KEY', 'value'));
```

##### Cron jobs

Actually all actions are registred inside 5 php files with an explicit name :

- cron/cron.daily.php
- cron/cron.hourly.php
- cron/cron.min.php
- cron/cron.monthly.php
- cron/cron.weekly.php

The cron system is launched by the main php container named **php**; its rules are defined by **[tools/docker/php/cron/crontab](tools/docker/php/cron/crontab)**

Each classe in **app/cron** or **extensions/extensionName/cron** wich implement **[core\tasks\CronInterface](core/tasks/CronInterface.php)** will be execute following the explicicit method name. For example :

```php
class MyCronJob implements CronInterface
{
    public function everyMinute(){
        //Do something
    }

    public function everyHour(){
        //Do something
    }

    public function everyDay(){
        //Do something
    }

    public function everyWeek(){
       //Do something
    }

    public function everyMonth(){
        //Do something
    }
}
```

---

#### API

The api context use a custom implementation of **[core\app\ApplicationServiceInterface](core/app/ApplicationServiceInterface.php)** wich is **[app\Api](app/Api.php)**

Like the main application, it will try to find the matching Controller by parsing all **[core\routing\RouteAttribute](core/routing/RouteAttribute.php)**, wich are parsed once on application build.

For example, this method inside a Controller will be parsed:

```php
#[\core\routing\RouteAttribute(
    url: '/api/categorie/copy/:idcategorie',
    method: 'POST',
    permission: ['create:categorie'],
    encoding: \false,
    visible: \false
)]
public function copyAction(){}

```

Every base api controller extending from [app\api\controller\ApiControllerBase](app/api/controller/ApiControllerBase.php) will permit to access these endpoints.

---

#### Websocket server

The websocket server is served by a php container named **socket**

You can interact with **[core\transport\websocket\WebsocketServiceInterface](core/transport/websocket/WebsocketServiceInterface.php)**

```php
$client = core\DI\DI::singleton(core\transport\websocket\WebsocketServiceInterface::class);
$client->sendToContext(array $message, string $type, ?int $idcontext = null);
$client->sendToPage(array $message, string $type, ?string $page = null);
$client->sendToUser(array $message, string $type, ?int $iduser = null);
```

Context, Iduser, and Page are 3 differents channels that every user subscribe.

or directly from the frontend with [See websocket](#websocket)

The default endoint is **wss://websocket.localhost**

---

#### Logger

To manage logs use **[core\logger\LogServiceInterface](core/logger/LogServiceInterface.php)** then register a custom **[core\logger\LoggerInterface](core/logger/LoggerInterface.php)**
The main implementation **[core\logger\LoggerManager](core/logger/LoggerManager.php)** could use multiple logger :

- **[core\logger\ConsoleLogger](core/logger/ConsoleLogger.php)**
- **[core\logger\FileLogger](core/logger/FileLogger.php)**
- **[core\logger\MemoryLogger](core/logger/MemoryLogger.php)**
- **[core\logger\PsrLogAdapter](core/logger/PsrLogAdapter.php)**
- **[core\logger\TracyLogger](core/logger/TracyLogger.php)**

```php
$logger = core\DI\DI::singleton(LogServiceInterface::class);
$logger->registerLogger(LoggerInterface::class); //Use a real implementation
```

---

#### Events

You could define some callback to be executed on specific event.

First define a class in **app\event** or **extensions\extensionName\event** extending **[core\events\AbstractListener](core/events/AbstractListener.php)**
Then define your first listener method with an **[core\events\ListenerAttribute](core/events/ListenerAttribute.php)**

```php
#[\core\events\ListenerAttribute(event: 'MY_EVENT')]
public function defaultListener(array &$args): array
{
    return $args;
}
```

To execute the action, trig the event with the **[core\event\EventManagerInterface](core/events/EventManagerInterface.php)**

```php
$eventManager = core\DI\DI::singleton(EventManagerInterface::class);
$eventManager->emit('MY_EVENT', ["param" => "value"]);
```

---

#### Php Session

To use $\_SESSION, prefer usage of **[core\session\SessionInterface](core/session/SessionInterface.php)**. It provides containerization feature.

```php
$session = core\DI\DI::singleton(SessionInterface::class);

$session->set("key", "hello"); //Save in main container
$session->get("key"); //Return "hello"

$container = $session->getContainer("myContainer"); //Return a core\session\SessionContainer;
$container->set("key", "world"); //Save in myContainer container
$session->set("myContainer.key", "world"); //Do the same action

$session->get("key"); //return "hello"
$session->get("myContainer.key"); //return "world"
$container->get("key"); //return "world"
```

---

#### Notification

To send notification to user use **[core\notifications\NotificationServiceInterface](core/notifications/NotificationServiceInterface.php)** and the main implementation **[core\notifications\Notificator](core/notifications/Notificator.php)**

```php
$notifier = core\DI\DI::singleton(NotificationServiceInterface::class);
$notifier->attachTransport(TransportServiceInterface::class);
$notifier->notify("My message", "danger", "/error"); //Third parameter will force client redirection
```

By default, **[core\transport\websocket\WebsocketServiceInterface](core/transport/websocket/WebsocketServiceInterface.php)** is the only one transport method.

---

#### Routing

Every step describes are already executed **[core\app\ApplicationServiceInterface](core/app/ApplicationServiceInterface.php)**

##### Disptacher

The **[core\routing\DispatcherServiceInterface](core/routing/DispatcherServiceInterface.php)** load only the candidates routes parsing the request.

How it works ? Each namespace is associated to a base path :

```php
$dispatcher = core\DI\DI::singleton(DispatcherServiceInterface::class);
$dispatcher->registerNamespace('app\controller', '/');

```

You could also manualy append a controller

```php
$dispatcher = core\DI\DI::singleton(DispatcherServiceInterface::class);
$namespace = $dispatcher->registerController(ControllerInterface::class, "/my_endpoint");
```

Then when a request in intercepted it try to find the related controller from base path.
It could easyly find the matching controller, because all url are dynamicly generated like this :

| namespace                           | endpoint          |
| ----------------------------------- | ----------------- |
| app\controller                      | /                 |
| app\api\controller                  | /api              |
| extension\accounting\controller     | /e/accounting     |
| extension\accounting\apî\controller | /api/e/accounting |

So **/e/accounting** will load only the routes defined in **extension\accounting\controller\AccountingController** and the dispatcher register them in the router with its private methods **\_registerRoutes**

##### Router

With a set of routes provided by the **[core\routing\DispatcherServiceInterface](core/routing/DispatcherServiceInterface.php)**, the **[core\routing\RouterServiceInterface](core/routing/RouterServiceInterface.php)** will :
1- Parse the url to find the matching route
2- Check the permissions
3- Then execute the related method if allowed
4- If an error appends, it returs the related $\_defaultControllers

You could add a custom **[core\routing\Route](core/routing/Route.php)**, the callable is autowiring

```php
$router = core\DI\DI::singleton(RouterServiceInterface::class);
$router->addRoute('GET', "/my_endpoint", function(ControllerServiceInterface $controllerFactory) {
    $controller = $controllerFactory->create(ControllerInterface::class, ["param" => "value"]);
    DI::call($controller, "indexAction"); //It's a method named indexAction
}, ["read:permission"]); //The last parameter is a permission
```

or like this

```php
$route = new Route('GET', "/my_endpoint", fn() => return "Hello world";);
$router->addRoute($route);
```

But be careful with circular error, this will probably won't work

```php
$router = core\DI\DI::singleton(RouterServiceInterface::class);
$router->addRoute('GET', "/my_endpoint", function(RouterServiceInterface $router) {

});
```

##### RouteAttribute

**[core\routing\RouteAttribute](core/routing/RouteAttribute.php)** are used to associate routes to controllers methods. For example :

```php
#[\core\routing\RouteAttribute(
    url: '/account',//Endpoint - Note : The endpoint is transformed inside extension to /e/account
    method: 'GET', //Http method
    permission: ['read:account'], //Defaut permissions
    encoding: false, // True if parmeters must be encoder in url
    displayName: 'account')] //Display name in menu
public function indexAction() //The method must be public
{
    parent::indexAction();
}
```

##### Link

**[core\routing\Link](core/routing/Link.php)** are useful to replace hard coded url. For example :

```php
$link = new Link(ControllerInterface::class, "indexAction");
$link->getEndpoint(); // Will return the endpoint defined in the RouteAttribute
```

---

#### Cache

The **[core\cache\CacheServiceInterface](core/cache/CacheServiceInterface.php)** describe all cache implementations. There is 4 available implementations :

- **[core\cache\MemcachedCache](core/cache/MemcachedCache.php)**
- **[core\cache\FileCache](core/cache/FileCache.php)**
- **[core\cache\FakeCache](core/cache/FakeCache.php)**
- **[core\cache\RedisCache](core/cache/RedisCache.php)**

These are the main methods :

```php
$cache = core\DI\DI::singleton(CacheServiceInterface::class);
$cache->clearAll();
$cache->close();
$cache->get(array|int|string $key);
$cache->hasKey(array|int|string $key): bool;
$cache->remove(array|int|string $key);
$cache->set(array|int|string $key, $values);
$cache->replace(string $key, $value);
$cache->queued(string $key, $values);
$cache->setMulti(array $properties): bool;
$cache->getCachePrefix(): ?string;
$cache->invalidate(string $key);
$cache->getMulti(array $keys);
```

Also, many classes use **[core\cache\CacheUpdaterTrait](core/cache/CacheUpdaterTrait.php)** wich provides 2 methods to use cache in objects.

```php
class MyClass
{
    use CacheUpdaterTrait;

    private array $_prop1 = [];
    private ?string $_prop2 = null;

    public function __construct(private CacheServiceInterface $cache) { //We must define a cache propertie
        //We retrieve _prop1 and _prop2 from cache
        $this->_fetchPropsFromCache(['_prop1', '_prop2']);
    }

    public function __destruct()
    {   //We save _prop1 and _prop2 in cache
        $this->_cacheMultiplesProps(['_prop1', '_prop2']);
    }
}
```

---

#### Controllers

Each controller must implements from **[core\controller\ControllerInterface](core/controller/ControllerInterface.php)**. There is a base controller defined for each context.

| Context | Base controller                                                                      |
| ------- | ------------------------------------------------------------------------------------ |
| App     | **[app\controller\ControllerBase](app/controller/ControllerBase.php)**               |
| Api     | **[app\api\controller\ApiControllerBase](app/api/controller/ApiControllerBase.php)** |

These controller are build by a factory **[core\controller\ControllerServiceInterface](core/controller/ControllerServiceInterface.php)**

```php
$controllerFactory = core\DI\DI::singleton(ControllerServiceInterface::class);
$controller = $controllerFactory->create(ControllerInterface::class, ["param" => "value"]);
```

By convention methods with associated **[core\routing\RouteAttribute](core/routing/RouteAttribute.php)** must ending with **Action**

---

#### Views

**[core\view\ViewServiceInterface](core/view/ViewServiceInterface.php)** permit to assign **[core\html\HtmlElementInterface](core/html/HtmlElementInterface.php)** to transform them in real html.
By default the application will call **renderView** in **[core\controller\ControllerInterface](core/controller/ControllerInterface.php)** and each component will be assigned to the view.

```php
$view = core\DI\DI::singleton(ViewServiceInterface::class);
$datatable = new Datatable()
$view->assignWidget($datatable); // It must be an HtmlElementInterface
$view->assign("param", "Hello world!");
```

Then :
1 - Load a **[templates/layout/layout.\*.html](templates/layout/layout.private.html)**
2 - Load page template **[templates/pages/page.\*.html](templates/pages/page.private.html)**
3 - Load different component templates **templates/components/\*/\*.html**

Inside template you could retrieve different elements :

```html
{$param}
<!-- Will display "Hello world! -->

{foreach $HTML_COMPONENTS['datatables'] as $element} {if !$element->hasParent()}
<!-- Load associated file template -->
{include $element->getHtmlTemplate(), element => $element} {/if} {/foreach}
```

See more about latte syntax : [Nette Latte](https://latte.nette.org/)

Each component has its own template **templates/component/component_anme/Component_name.html** but could be define your own like this

```php
$form = core\DI\DI::singleton(Form::class);
$form->setHtmlTemplate(Form::getBaseTemplateDir().'form/my.custom.template.html');
// templates/component/form/my.custom.template.html will be loaded
```

---

#### Html elements

There is one [custom element](https://developer.mozilla.org/en-US/docs/Web/Web_Components) defined for each **core\html** component that use **[core\html\javascript\WithJavacript](core/html/javascript/WithJavascript.php)** trait. For javascript frontend explanations[See javascript components](#components)

##### Available html elements

| Php class                                                                                          | Html tag                                                                         |
| -------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------- |
| [core\html\button\Button](core/html/button/Button.php)                                             | [t-button](assets/js/components/button/Button.js)                                |
| [core\html\calendar\Calendar](core/html/calendar/Calendar.php)                                     | [t-calendar](assets/js/components/calendar/Calendar.js)                          |
| [core\html\chart\Chart](core/html/chart/Chart.php)                                                 | [t-chart](assets/js/components/chart/Chart.js)                                   |
| [core\html\diagram\Diagram](core/html/diagram/Diagram.php)                                         | [t-diagram](assets/js/components/diagram/Diagram.js)                             |
| [core\html\form\Form](core/html/form/Form.php)                                                     | [t-form](assets/js/components/form/Form.js)                                      |
| [core\html\gantt\Gantt](core/html/gantt/Gantt.php)                                                 | [t-gantt](assets/js/components/gantt/Gantt.js)                                   |
| [core\html\tour\BootstrapTour](core/html/tour/BootstrapTour.php)                                   | [t-tour](assets/js/components/tour/Tour.js)                                      |
| [core\html\tables\pillsTable\PillsTable](core/html/tables/pillsTable/PillsTable.php)               | [t-pillstable](assets/js/components/tables\pillsTable/PillsTable.js)             |
| [core\html\tables\pillsTable\PillsTableElement](core/html/tables/pillsTable/PillsTableElement.php) | [t-pillsTableelement](assets/js/components/tables\pillsTable/PillsTable.js)      |
| [core\html\form\Stepper](core/html/form/Stepper.php)                                               | [t-stepper](assets/js/components/form/Stepper.js)                                |
| [core\html\label\Label](core/html/label/Label.php)                                                 | [t-label ](assets/js/components/label/Label.js)                                  |
| [core\html\menu\Menu](core/html/menu/Menu.php)                                                     | [t-menu ](assets/js/components/menu/Menu.js)                                     |
| [core\html\modal\Modal](core/html/modal/Modal.php)                                                 | [t-modal ](assets/js/components/modal/Modal.js)                                  |
| [core\html\timeline\Timeline](core/html/timeline/Timeline.php)                                     | [t-timeline ](assets/js/components/timeline/Timeline.js)                         |
| [core\html\progressBar\ProgressBar](core/html/progressBar/ProgressBar.php)                         | [t-progress ](assets/js/components/progressBar/ProgressBar.js)                   |
| [core\html\treeview\Treeview](core/html/treeview/Treeview.php)                                     | [t-treeview ](assets/js/components/treeview/Treeview.js)                         |
| [core\html\tables\datatable\Datatable](core/html/tables/datatable/Datatable.php)                   | [t-datatable ](assets/js/components/tables/datatable/Datatable.js)               |
| [core\html\form\elements\AdressElement](core/html/form/elements/AddressElement.php)                | [i-address ](assets/js/components/form/elements/AddressElement.js)               |
| [core\html\form\elements\ButtonElement](core/html/form/elements/ButtonElement.php)                 | [i-button ](assets/js/components/form/elements/ButtonElement.js)                 |
| [core\html\form\elements\CguElement](core/html/form/elements/CguElement.php)                       | [i-cgu ](assets/js/components/form/elements/CguElement.js)                       |
| [core\html\form\elements\CheckboxElement](core/html/form/elements/CheckboxElement.php)             | [i-checkbox ](assets/js/components/form/elements/CheckboxElement.js)             |
| [core\html\form\elements\ColorpickerElement](core/html/form/elements/ColorpickerElement.php)       | [i-colorpicker ](assets/js/components/form/elements/ColorpickerElement.js)       |
| [core\html\form\elements\DatatableElement](core/html/form/elements/DatatableElement.php)           | [i-datatable ](assets/js/components/form/elements/DatatableElement.js)           |
| [core\html\form\elements\DateElement](core/html/form/elements/DateElement.php)                     | [i-date ](assets/js/components/form/elements/DateElement.js)                     |
| [core\html\form\elements\DaterangeElement](core/html/form/elements/DaterangeElement.php)           | [i-daterange ](assets/js/components/form/elements/DaterangeElement.js)           |
| [core\html\form\elements\DatetimeElement](core/html/form/elements/DatetimeElement.php)             | [i-datetime ](assets/js/components/form/elements/DatetimeElement.js)             |
| [core\html\form\elements\DatetimerangeElement](core/html/form/elements/DatetimerangeElement.php)   | [i-datetimerange ](assets/js/components/form/elements/DatetimerangeElement.js)   |
| [core\html\form\elements\EmailElement](core/html/form/elements/EmailElement.php)                   | [i-email ](assets/js/components/form/elements/EmailElement.js)                   |
| [core\html\form\elements\FileinputElement](core/html/form/elements/FileinputElement.php)           | [i-fileinput ](assets/js/components/form/elements/FileinputElement.js)           |
| [core\html\form\elements\CalendarElement](core/html/form/elements/CalendarElement.php)             | [i-calendar ](assets/js/components/form/elements/CalendarElement.js)             |
| [core\html\form\elements\HiddenElement](core/html/form/elements/HiddenElement.php)                 | [i-hidden ](assets/js/components/form/elements/HiddenElement.js)                 |
| [core\html\form\elements\MentionableElement](core/html/form/elements/MentionableElement.php)       | [i-mentionable ](assets/js/components/form/elements/MentionableElement.js)       |
| [core\html\form\elements\MultilistElement](core/html/form/elements/MultilistElement.php)           | [i-multilist ](assets/js/components/form/elements/MultilistElement.js)           |
| [core\html\form\elements\MultipleemailElement](core/html/form/elements/MultipleemailElement.php)   | [i-multipleemail ](assets/js/components/form/elements/MultipleemailElement.js)   |
| [core\html\form\elements\MultiselectElement](core/html/form/elements/MultiselectElement.php)       | [i-multiselect ](assets/js/components/form/elements/MultiselectElement.js)       |
| [core\html\form\elements\NumberElement](core/html/form/elements/NumberElement.php)                 | [i-number ](assets/js/components/form/elements/NumberElement.js)                 |
| [core\html\form\elements\PasswordElement](core/html/form/elements/PasswordElement.php)             | [i-password ](assets/js/components/form/elements/PasswordElement.js)             |
| [core\html\form\elements\PlurimultilistElement](core/html/form/elements/PlurimultilistElement.php) | [i-plurimultilist ](assets/js/components/form/elements/PlurimultilistElement.js) |
| [core\html\form\elements\SelectElement](core/html/form/elements/SelectElement.php)                 | [i-select](assets/js/components/form/elements/SelectElement.js)                  |
| [core\html\form\elements\SignatureElement](core/html/form/elements/SignatureElement.php)           | [i-signature](assets/js/components/form/elements/SignatureElement.js)            |
| [core\html\form\elements\SliderElement](core/html/form/elements/SliderElement.php)                 | [i-slider](assets/js/components/form/elements/SliderElement.js)                  |
| [core\html\form\elements\StaticElement](core/html/form/elements/StaticElement.php)                 | [i-static](assets/js/components/form/elements/StaticElement.js)                  |
| [core\html\form\elements\SwitchElement](core/html/form/elements/SwitchElement.php)                 | [i-switch](assets/js/components/form/elements/SwitchElement.js)                  |
| [core\html\form\elements\TextareaElement](core/html/form/elements/TextareaElement.php)             | [i-textarea](assets/js/components/form/elements/TextareaElement.js)              |
| [core\html\form\elements\RichtextElement](core/html/form/elements/RichtextElement.php)             | [i-richtext](assets/js/components/form/elements/RichtextElement.js)              |
| [core\html\form\elements\TextElement](core/html/form/elements/TextElement.php)                     | [i-text](assets/js/components/form/elements/TextElement.js)                      |
| [core\html\form\elements\TimeElement](core/html/form/elements/TimeElement.php)                     | [i-time](assets/js/components/form/elements/TimeElement.js)                      |
| [core\html\form\elements\TimerangeElement](core/html/form/elements/TimerangeElement.php)           | [i-timerange](assets/js/components/form/elements/TimerangeElement.jss)           |
| [core\html\form\elements\TagElement](core/html/form/elements/TagElement.php)                       | [i-tag](assets/js/components/form/elements/TagElement.js)                        |

**Here an example with a basic form**

```php
$commandFactory = core\DI\DI::singleton(CommandFactory::class);
$formFactory = core\DI\DI::singleton(FormFactory::class);
$form = $formFactory->create("MyId", [
            'name'   => 'create_log',
            'method' => 'POST',
            'action' => '#'
        ]); //Will return a Form instance

//Set value for submit button
$form->setValue(\_('Save'));

//Define submit event
$form->on('submit')
    ->command($commandFactory->create(
        \core\html\form\command\SubmitCommand::class,
        [
            'target'   => $form,
            'method'   => 'POST',
            'endpoint' => '/api/log/create'
        ]
    ));

$form->setTitle(\_('Add a log'));

//Define html teplate
$form->setHtmlTemplate(self::getDefaultTemplateDir() . 'form/form.template.log.html');

//Append a field
/** @var \core\html\form\elements\MultiselectElement $field */
$field = $form->add(\core\html\form\elements\MultiselectElement::class);
$field->setName('idcontext')
    ->setLabel(_('Context'))
    ->btn('context');
```

Let's look at **[core\html\modal\Modal](core/html/modal/Modal.php)** getAllowedChilds() method

```php
/**
 * List of allowed childs elements
 * could be a class, an abstract class or an interface
 */
protected static function getAllowedChilds(): array
{
    return [
        HtmlElement::class,
        Datatable::class,
        Form::class,
        PillsTable::class,
        Calendar::class,
        Accordion::class,
        TableStriped::class,
        Treeview::class
    ];
}
```

So you can append this form to a modal element.

```php
$modalFactory = core\DI\DI::singleton(ModalFactory::class);
$modalFactory->createThen("MyModal", function (Modal $modal) use ($form){
    $modal->appendChild($form);
});

//or simply
$modal = $modalFactory->create("MyModal");
$modal->appendChild($form);
```

_See in each php class wich methods are available_

##### Html elements factories

Each element has its own factory extending from **[core\html\HtmlElementFactory](core/html/HtmlElementFactory.php)** and implementing **[core\factory\FactoryInterface](core/factory/FactoryInterface.php)**

Theses factories store instance with unique identifier.

```php
$formFactory = core\DI\DI::singleton(FormFactory::class);
$formA = $formFactory->create("MyId"); //Will return a Form instance
$formB = $formFactory->create("MyId"); //Will return the same instance stored in $formA
```

Because the factories check the permissions, this useful method permit to execute some code only if the element is build

```php
// You could execute a function just if the element is build
$formA =$formFactory->createThen('MyId', function (Form $form) use ($modalFactory) {
    //Do some actions
}

//The callable is autowiring, so you could write
$formA =$formFactory->createThen('MyId', function (Form $form, ModalFactory $modalFactory) {
    $modalFactory->create("id")->appendChild($form);
}
```

##### Widgets

Widget are a set of configured html elements, like datatables, forms, modals.
They are defined in **app\widgets** or **extensions\ExtensionNamespace\widgets** namespace, and are usualy called by their related factroy.

Example :

```php
$formFactory = core\DI\DI::singleton(FormFactory::class);
$formA = $formFactory->createFromWidget("Type", ['method' => 'edit']); //Will call TypeFormWidget->edit() and return the Form object
$formB = $formFactory->createFromWidget("Type", ['method' => 'edit']); //Will return the same instance stored in $formA
$formC = $formFactory->createFromWidget("Type", ['method' => 'edit', "param1" => "value"]); //Will return a new instance
```

##### Assets

We use [ViteJs](https://vitejs.dev) to manage assets.
All files are stored in **assets** directory.
For extensions developpement, you could store specific assets inside **extensions/ExtensionName/assets** and then use this methods in Php controllers

- With facade : **core\html\Asset::method()**
- With service : **core\html\Assets->method()**

```php
addCss(string $file, bool $inBody = true)
addCssInline(string $style, bool $inBody = true)
addJs(string $file, bool $inBody = true)
addJsInline(string $script, bool $inBody = true)
```

In production all files are served from **public** directory wich is also the default endpoint rewrited by [Traefik](https://traefik.io/).

- allteam.localhost -> /public
- allteam.localhost/api -> /public/api

##### Html layout and templates

Each component extends from **[templates/layout/layout.component.html](templates/layout/layout.component.html)**
This template provide a base structure (with bootstrap card) for each components.

Here an example with all available blocks in use :

```html
{layout '/templates/layout/layout.component.html'}
<!-- {$element->getViewReference()} :: {$element->getIdAttribute()} Start -->

<!-- $attributes represents the main container's html attributes -->
{var $attributes = ["id" => 'parent-'. $element->getIdAttribute()]}

<!-- Define a top image -->
{var $topImage = '/public/images/logo-color-01-notitle.png'} {var $imgCss =
'custom-css-classe-1 custom-css-class-2'}

<!-- Define a top icon -->
{var $icon = 'fas fa-lock'}

<!--Custom card header css -->
{var $headerCss = 'custom-css-classe-1 custom-css-class-2'}
<!-- Custom card header content -->
{block cardHeader}My custom content{/block}

<!-- Custom card header previous button in left corner
By default if $element->getParent()->isA('Modal') the button will be showed -->
{block cardPrevious}My custom content{/block}

<!-- Assign a tool bar in the header in right corner -->
{var $toolbar = $element->getActionBar()}
<!-- Or define a custom content for tool bar -->
{block cardToolbar}My custom content{/block}
<!--Custom card toolbar css -->
{var $toolbarCss = 'custom-css-classe-1 custom-css-class-2'}

<!-- Custom card spinner content -->
{block cardSpinner}My custom content{/block}
<!-- Or just enable the default spinner -->
{var $enableSpinner = true}

<!-- Define a card-body -->
{block cardBody}My Custom content{/block}
<!-- Custom card-body css -->
{var $bodyCss = 'custom-css-classe-1 custom-css-class-2'}

<!-- Define an icon in card-body -->
{var $subIcon = 'fas fa-lock'}

<!-- Define a card-title in card body -->
{block cardTitle}My Custom content{/block}
<!-- Custom card-title css -->
{var $titleCss = 'custom-css-classe-1 custom-css-class-2'}

<!-- Define a card-subtitle in card body -->
{block cardSubtitle}My Custom content{/block}
<!-- Custom card-subtitle css -->{var $subtitleCss = 'custom-css-classe-1
custom-css-class-2'}

<!-- Define a card-text in card body -->
{block cardText}My Custom content{/block}
<!-- Custom card-text css -->
{var $textCss = 'custom-css-classe-1 custom-css-class-2'}

<!-- Define a custom link  in body -->
{var $link = $element->getLink()[1]} {var $href = $element->getLink()[0]}

<!-- Define a card-footer -->
{var $footerCss = 'custom-css-classe-1 custom-css-class-2'}
<!-- Custom card-footer -->
{block cardFooter}My Custom content{/block}

<!-- {$element->getViewReference()} :: {$element->getIdAttribute()} End -->
```

##### Forms

Here is an example of form with default layout **[templates/components/form/Form.html](templates/components/form/Form.html)**

```html
{layout '/templates/form/Form.html'}
<!-- {$element->getViewReference()} :: {$element->getIdAttribute()} Start -->

<!-- $attributes represents the main container's html attributes -->
{var $attributes = ["id" => 'parent-'. $element->getIdAttribute()]}

<!-- Define a top image -->
{var $topImage = '/public/images/logo-color-01-notitle.png'} {var $imgCss =
'custom-css-classe-1 custom-css-class-2'}

<!-- Define a top icon -->
{var $icon = 'fas fa-lock'}

<!-- Custom form open tag  -->
{block formOpenTag}
<form>
  {/block}

  <!--Custom card header css -->
  {var $headerCss = 'custom-css-classe-1 custom-css-class-2'}
  <!-- Custom form title -->
  {block formTitle}My custom content{/block}

  <!-- Custom swicher -->
  {block formSwitch}{/block}

  <!-- Custom card header previous button in left corner
By default if $element->getParent()->isA('Modal') the button will be showed -->
  {block cardPrevious}My custom content{/block}

  <!-- Custom submit button in the header in right corner -->
  {block formSubmitButton}<button></button>{/block}

  <!-- Custom card spinner content -->
  {block cardSpinner}My custom content{/block}
  <!-- Or just enable the default spinner -->
  {var $enableSpinner = true}

  <!-- Define a form top message  -->
  {block formTopMessage}My Custom content{/block}

  <!-- Custom csrf field -->
  {block formCsrf}{/block}

  <!-- Custom fields -->
  {block formFields}{/block}

  <!-- Custom captcha component -->
  {block formCaptcha}{/block}

  <!-- Custom bottom message -->
  {block formBottomMessage}

  <!-- Custom form-footer  -->
  {block formFooter}My Custom content{/block}

  <!-- Custom close tag -->
  {block formCloseTag}
</form>
{/block}

<!-- {$element->getViewReference()} :: {$element->getIdAttribute()} End -->
```

---

#### Database

It's encouraged to use command line to generate new entitie classes (with associated controller, widgets...) [See Extensions](#extensions)

##### Repository & Entities

Here is a minimal entitie extending **[core\entities\EntitieAbstract](core/entities/EntitieAbstract.php)**. Only public properties are real databse column.

```php
#[\core\entities\EntitieAttribute(tableName: 'user')]
class User extends \core\entities\EntitieAbstract
{
    public ?int $iduser = null;
    private ?array $idcontext_s = null;
    public ?string $email = null;
    public ?string $password = null;
    public ?\Nette\Utils\DateTime $created_at = null;
    public ?\Nette\Utils\DateTime $updated_at = null;
    private array $searchFields = ['email'];

    public function getSearchFields(): array
    {
        return $this->searchFields;
    }

    public function __construct(array $data, ?\core\database\mysql\lib\nette\CustomNetteSelection $selection)
    {
        parent::__construct($data, $selection);
    }

    public function getRequiredField(): string|null|array
    {
        return 'name';
    }
}
```

CRUD is manage by **[core\database\RepositoryInterface](core/database/RepositoryInterface.php)** wich are always builded by **[core\database\RepositoryFactoryInterface](core/database/RepositoryFactoryInterface.php)** to prevent duplicate instance.

```php
$repositoryFactory = core\DI\DI::singleton(RepositoryFactoryInterface::class);
$repository = $repositoryFactory->table("user"); // Returns RepositoryInterface
$repository->getById(1); // Return User with iduser = 1
```

Database connection is managed by **[core\database\DatabaseManagerInterface](core/database/DatabaseManagerInterface.php)**
You could access to the ORM to build complexe requests. See [Nette Database](https://doc.nette.org/en/database)

```php
$databaseManager = core\DI\DI::singleton(DatabaseManagerInterface::class);
$databaseManager->getOrm("user")
    ->where(["iduser > ?" => 1, "email != ?" => "allteam@allteam.io"]);
```

##### Migrations & Seeders

[Phinx](https://book.cakephp.org/phinx/0/en/index.html) is used to manage migrations and seeders.
To create migration or seeder use your php docker container (because the environnement variables are not present in host)

```shell
docker exec -it php php -f /var/www/html/vendor/bin/phinx {command} --configuration /var/www/html/config/phinx.php
```

Usual commands are :

Execute pending migrations

```shell
docker exec -it php php -f /var/www/html/vendor/bin/phinx migrate --configuration /var/www/html/config/phinx.php
```

Rollback to initial state

```shell
docker exec -it php php -f /var/www/html/vendor/bin/phinx rollback -t 0 --configuration /var/www/html/config/phinx.php
```

Execute seeders

```shell
docker exec -it php php -f /var/www/html/vendor/bin/phinx seed:run --configuration /var/www/html/config/phinx.php
```

[Complete list of available commands](https://book.cakephp.org/phinx/0/en/commands.html)
Migrations and seeders are stored inside config/db, and inside each extension's config/db directory.

---

#### Object storage

Let's look inside **[core\services\storage\ObjectStorageInterface](core/services/storage/ObjectStorageInterface.php)**. By default, the **[core\services\storage\FilesystemStorage](core/services/storage/FilesystemStorage.php)** is used.

It provides some usefull methods to manipulate **[core\services\storage\FileItemInterface](core/services/storage/FileItemInterface.php)** :

```php
public function copy(string $source, string $destination): ?FileItemInterface;
public function delete(string $source);
public function get(string $source): ?FileItemInterface;
public function has(string $source): bool;
public function move(FileItemInterface|string $source, string $destination): ?FileItemInterface;
public function setVisibility(string $source, string $visibility): ?FileItemInterface;
public function update(string $source, string $filename, string $visibility = self::_PUBLIC): ?FileItemInterface;
public function upload(string $source, string $destination, string $visibility = self::_PUBLIC)
```

---

#### Serializer

[core\services\serializer\SerializerServiceInterface](core/services/serializer/SerializerServiceInterface.php) will transform object into [core\services\serializer\DescriptionInterface](core/services/serializer/DescriptionInterface.php) in order to be serialized or unserialized by a [core\services\serializer\SerializerInterface](core/services/serializer/SerializerInterface.php).

The defautl [core\services\serializer\DefaultSerializer](core/services/serializer/DefaultSerializer.php) is a wrapper for buildin functions.

#### Utils

[core\utils\Utils](core/utils/Utils.php) is used to achieve many little useful things

**Encrypt and decrypt a string**

```php
$myString = 'My super message to encode'
$myEncodedString = Utils::encrypt($myString);
$myDecodedString = Utils::decrypt($myEncodedString);
```

**Transform an object into an hash, depending on its parameters values**

```php
//You could also transform an object into an hash, depending on its parameters values
$hash = Utils::hash($myString); //with string, int, boolean, null...
$hash = Utils::hash(['pro'=>  "val"]); //but also with array
$hash = Utils::hash(new MyClass()); //and with object
$hash = Utils::hash(["a"=> new MyClass(), "b" => false, "c" => ["d", "e"]); //or with a set of mixed vars
```

---

#### Extensions

You could find command shortcut to generate single entitie or a set of entites.

This command will use a config file to generate multiple entities with associated classes (controllers, models, listeners, migrations, widgets, api, routes...)

The file used is **[config/init.neon](config/init.neon)**.

```shell
./tools/bin/console app:initialize
```

Let's explain the possible values

```neon
app: #this will be defined in namespace app
    gdpr: #this is the entitie name, an indexKey is always defined 'idgdpr'
        idcontext:
            name: "idcontext" #the propertie name
            required: true #default false
            isNullable: true #default false
            isSearcheable: true #default false
            type: "one-to-many"
            length: 11 #field size
extensions\suvey: #this will be defined in namespace extensions\suvey
    survey_item:
        idsurvey_item_parent:
            targetTable: "survey_item" #specific option for "orderable"
            name: "idsurvey_item_parent"
            isNullable: true
            type: "orderable"
        idgdpr: #it will append a child form reffering to entitie
            name: "idgdpr"
            isForm: true #could also be an array with columns names
            type: "one-to-one"
            isNullable: true

```

This types are defined in **[core\services\generators\Helper](core/services/generators/Helper.php)**

| type          | Mysql column type | Entitie property type                                      | Form field type                                                          | Datatable type |
| ------------- | ----------------- | ---------------------------------------------------------- | ------------------------------------------------------------------------ | -------------- |
| text          | string            | string                                                     | [TextElement](core/html/form/elements/TextElement.php)                   | text           |
| richtext      | text              | string                                                     | [RichtextElement](core/html/form/elements/RichtextElement.php)           | richtext       |
| switch        | boolean           | bool                                                       | [SwitchElement](core/html/form/elements/SwitchElement.php)               | checkbox       |
| checkbox      | boolean           | bool                                                       | [CheckboxElement](core/html/form/elements/CheckboxElement.php)           | checkbox       |
| date          | date              | \Nette\Utils\DateTime                                      | [DateElement](core/html/form/elements/DateElement.php)                   | date           |
| integer       | integer           | int                                                        | [NumberElement](core/html/form/elements/NumberElement.php)               | number         |
| datetime      | datetime          | \Nette\Utils\DateTime                                      | [DatetimeElement](core/html/form/elements/DatetimeElement.php)           | datetime       |
| time          | time              | \Nette\Utils\DateTime                                      | [TimeElement](core/html/form/elements/TimeElement.php)                   | time           |
| daterange     | date              | \Nette\Utils\DateTime                                      | [DaterangeElement](core/html/form/elements/DaterangeElement.php)         | daterange      |
| timerange     | time              | \Nette\Utils\DateTime                                      | [TimerangeElement](core/html/form/elements/TimerangeElement.php)         | timerange      |
| datetimerange | datetime          | \Nette\Utils\DateTime                                      | [DatetimerangeElement](core/html/form/elements/DatetimerangeElement.php) | datetimerange  |
| dateinterval  | timeint           | \DateInterval                                              | [TimeElement](core/html/form/elements/TimeElement.php)                   | time           |
| number        | decimal           | int                                                        | [NumberElement](core/html/form/elements/NumberElement.php)               | nulber         |
| currency      | decimal           | int                                                        | [NumberElement](core/html/form/elements/NumberElement.php)               | number         |
| phone         | string            | string                                                     | [TextElement](core/html/form/elements/TextElement.php)                   | phone          |
| password      | string            | string                                                     | [PasswordElement](core/html/form/elements/PasswordElement.php)           | password       |
| email         | string            | string                                                     | [EmailElement](core/html/form/elements/EmailElement.php)                 | email          |
| file          | string            | [FileType](core/database/types/FileType.php)               | [FileinputElement](core/html/form/elements/FileinputElement.php)         | fileinput      |
| tag           | string            | [ArrayStringType](core/database/types/ArrayStringType.php) | [TagElement](core/html/form/elements/TagElement.php)                     | autocomplet    |
| status        | string            | string                                                     | [SelectElement](core/html/form/elements/SelectElement.php)               | select         |

Special fields will define foreign keys, link tables, or add filelds in entities and other tables

| type         | Mysql column type | Entitie property type | Form field type                                                      | Datatable type |
| ------------ | ----------------- | --------------------- | -------------------------------------------------------------------- | -------------- |
| orderable    | integer           | int                   | [SelectElement](core/html/form/elements/SelectElement.php)           | select         |
| one-to-one   | integer           | int                   | [SelectElement](core/html/form/elements/SelectElement.php)           | select         |
| one-to-many  | integer           | array                 | [MultiselectElement](core/html/form/elements/MultiselectElement.php) | multiselect    |
| many-to-one  | integer           | int                   | [SelectElement](core/html/form/elements/SelectElement.php)           | select         |
| many-to-many | integer           | array                 | [MultiselectElement](core/html/form/elements/MultiselectElement.php) | multiselect    |

This command will generate a single entitie with associated classes (controllers, models, listeners...)

```shell
./tools/bin/console app:create-entitie
```

Changes are saved in **[config/latest_config_succeed.neon](config/latest_config_succeed.neon)** After you must call database migration ([See Migrations & Seeders](#migrations--seeders))

This command will compile the manifest file for class mapping inside each extensions

```shell
./tools/bin/console app:build-extensions
```

Each extensions must have this structure :

| Folder                | Description                                                |
| --------------------- | ---------------------------------------------------------- |
| assets\css            | **Store specific css files**                               |
| assets\js             | **Store specific js files**                                |
| command               | **Store global CommandInterface classes**                  |
| config                | **Store specifics configurations**                         |
| controller\api        | **Api controllers**                                        |
| controller\app        | **App controllers**                                        |
| cron                  | **Class implementing \core\tasks\CronInterface**           |
| entities              | **Database entities, command, decorator and repositories** |
| events                | **Events listeners**                                       |
| locales               | **Specific translations**                                  |
| model                 | **Database models**                                        |
| SkeletonExtension.php | **Main class**                                             |
| tasks                 | **Async task**                                             |
| templates             | **Html templates**                                         |
| widgets               | **Html widgets**                                           |

An extension must extend from **[core\extensions\AbstractExtension](core/extensions/AbstractExtension.php)** and implement **[core\extensions\ExtensionInterface](core/extensions/ExtensionInterface.php)** and then define these 4 methods :

```php
public function boot(){}
public function build(){}
public function destroy(){}
public function init(\core\app\ApplicationServiceInterface $app)
{
    parent::init($app);
}
```

Optionnally you could define some actions for installation an uninstallation process :

```php
public function install(){}
public function uninstall(){}
```

!!! Note: Migrations are always executed by the [core\extensions\ExtensionInstallerServiceInterface](core/extensions/ExtensionInstallerServiceInterface.php) and the[core\extensions\ExtensionUninstallerServiceInterface](core/extensions/ExtensionUninstallerServiceInterface.php)

And must have an **[core\extensions\ExtensionConfig](core/extensions/ExtensionConfig.php)** :

```php
#[\core\extensions\ExtensionConfig(
    name: 'Mail',
    description: 'Describe the extension',
    depends: [core\extensions\ExtensionInterface::class, core\extensions\ExtensionInterface::class],
    icon: 'fa-x-ray')
]
```

---

#### Frontend

The entrypoint is [assets/index.js](assets/index.js)

Each component implements these methods :

```ts
mount(): Promise
unmount(): this
reload(): this

```

##### Configuration manager

[ConfigurationManager](assets/js/ConfigurationManager.js) is used to parse config and affect properties
!!! Note: TODO

##### Main application

The main application is an intance off [App](assets/js/App.js) could be accessed everytwhere beacause :

- it's attached to window object
- it's usualy passed as dependencie in contructors

You could pass some config at build.

```js
const app = new App(
  {}, //Define some config
  (success) => {}, //Exectute action on construct
  (error) => {
    app.exceptiond.addException(error, app);
  }, //Exeption handler
  () => {
    return true;
  } //Exectute actions on destroy
);

app.on('app.mount', () => {
  //Execute actions
});

//Then mount component
app.mount();
```

Then you could call other component using its getters.

```js
app.transition; // Return Highway.Core
app.configd; // Return ConfigurationManager
app.datad; // Return DatasourcesManager
app.command; // Return CommandManager
app.exceptiond; // Return ExceptionManager
app.componentd; // Return ComponentManager
app.stored; // Return Store
app.machined; //return MachineManager
app.socketd; // return WebsocketManager
app.ajaxd; // Return AjaxManager
app.storaged; // return LocalStorageManager
app.databased; //return DatabaseManager
app.windowd; //return WindowsManager
app.comd; //return InstantMessaging
```

##### Events listeners

Most of classes extends from [Events](assets/js/Events.js) and permit to acces to [EventManager](assets/js/EventManager.js) instance.

It provides some event capacities to object :

```js
component.on(event, handler, (selector = null), (parameters = [])); //Attach listener
component.off(event, (selector = null)); //Remove listeners for event
component.dispatch(event, (parameters = [])); //Dispatch event
```

But if not, you could always access to it with [App](assets/js/App.js) instance

```js
app.eventd.on(event, handler, (selector = null), (parameters = [])); //Attach listener
app.eventd.off(event, (selector = null)); //Remove listeners for event
app.eventd.dispatch(event, (parameters = [])); //Dispatch event
```

##### Ajax

[AjaxManager](assets/js/AjaxManager.js) manage ajax request. Every request must be done with one of this method.

```ts
app.ajaxd.call(request, parameters = {}) :Promise
app.ajaxd.get(url) :Promise
app.ajaxd.post(url, datas = []) :Promise
app.ajaxd.delete(url, datas = []) :Promise
app.ajaxd.put(url, datas = []) :Promise
app.ajaxd.queue(request);
```

The **queue** method is used when client is offline.

##### Websocket

[WebsocketManager](assets/js/WebsocketManager.js) manage websocket communications. It manage too json web tokens.

```js
app.socketd.publish(channel, data);
app.socketd.subscribe(topic);
app.socketd.unsubscribe(topic);
```

Socket channels and config are stored in DOM and retrieve at mount.

```js
this.channel = config.channel ?? document.querySelector('#CHANNEL').value;
this.mainchannel =
  config.mainchannel ?? document.querySelector('#MAINCHANNEL').value;
this.endpoint = document.querySelector('#URLSOCKET').value;
this.pagechannel =
  config.pagechannel ?? document.querySelector('#PAGECHANNEL').value;
```

##### Store

You could use the [Store](assets/js/Store.js) in order to facilitate component data transfert. For example

```js
//Define new store variable
app.stored.set('propertie', 'Hello');

//Default assign
let myObject1 = {
  propertie: app.stored.get('propertie', myObject1),
};

//Assign with a custom handler
let myObject2 = {
  propertie: app.stored.get('propertie', myObject2, (event) => {
    myObject2.propertie = e.detail.newValue + ' world';
  }),
};

console.log(myObject1.propertie); // Return "Hello"
console.log(myObject2.propertie); // Return "Hello world"
console.log(app.stored.get('propertie')); // Return "Hello"
```

##### Database & localstorage

###### [DatabaseManager](assets/js/DatabaseManager.js)

This component is a local [indexDB](https://developer.mozilla.org/fr/docs/Web/API/IndexedDB_API) that register widgets datas that need an ajax request. It permit to work offline, and to sync local datatabase with a distant one (in our case Mysql Database behind api).

```js
//Define new store variable
app.databased.initStores({
  tableName: '++id, name, age, *tags',
  secondTable: 'id, prop1',
});

//Then you could use your table like this
let repository = app.databased.getStore('tableName');
repository.add({ name: 'John', age: 33 }); //Adding one row
repository.bulkAdd([
  { name: 'Skat', age: 43 },
  { name: 'Paf', age: 12 },
]); //or multiples
```

See [Dexie.js](https://dexie.org/docs/API-Reference#quick-reference) for more infos.

Here is schema syntax :

| Syntax | Description                  |
| ------ | ---------------------------- |
| ++     | Auto-incremented primary key |
| &      | Unique                       |
| \*     | Multi-entry index            |
| [A+B]  | Compound index               |

Each php entitie have 2 associated files in **assets/js/schema** or **extensions/extensionName/assets/js/schema** :

- EntitieName.js : the javascript implementation of the entitie
- EntitieName.json : the default schema

The entitie class could be mapped to the database

```js
import EntitieName from './schema/EntitieName.js';
app.databased.db.entitieName.mapToClass(EntitieName);
```

###### [LocalStorageManager](assets/js/LocalStorageManager.js)

Just a warper to [localStorage](https://developer.mozilla.org/fr/docs/Web/API/Window/localStorage) if available.

```js
app.storaged.set('myKey', 'Hello'); // Set new myKey
app.storaged.get('myKey'); // Return a stored "Hello"
app.storaged.remove('myKey'); // Return a stored "Hello"
```

###### [DatasourcesManager](assets/js/DatasourcesManager.js)

Manage datasources components.

##### Machine state

[MachineManage](assets/js/machine/MachineManager.js) is a factory for [Machine](assets/js/machine/Machine.js). You could define a logic like this :

```js
//Its a customElement extending from one of the 3 HtmlElement class
let myComponent;

//Components with the same tagName will use the same machine
let machine = app.machined.createMachine(myComponent, 'tagName', {param:"value"}));

let state1 = machine.addState(
    "state-1",
    autoswitch = false, //Auto swich to next transision if true
    action = () => { console.log("Hello !")},
    onReload = null
);

let state2 = machine.addState(
    "state-2",
    autoswitch = false,
    action = () => { console.log("world !")},
    onReload = () => { console.log("Reloading !")}
);

state1.addTransition("state-2",
    onTransition = () => {},
    validation = () => {
      return true;
    },
    onError = () => {}
);

myComponent.goto("state-1"); //Print "Hello" in console
myComponent.goto("state-2"); //Print "world !" in console
myComponent.goto("state-1"); //Error because there is no registred transistion

state2.addTransition("state-1",
    onTransition = () => {},
    validation = () => {
      return true;
    },
    onError = () => {}
);

myComponent.goto("state-1"); //Print "Hello" in console

```

##### Components

There are 3 parents javascript classes :

- **[HtmlFormElement](assets/js/components/HtmlElement.js)** is used to extends HTMLFormElement (only forms)
- **[HtmlInputElement](assets/js/components/HtmlInputElement.js)** is used to extends HTMLInputElement (all inputs)
- **[HtmlElement](assets/js/components/HtmlElement.js)** is used to extends HTMLElement (other components, like tables, calendars...)

When an element is connected to the dom, it follows the [custom element](https://developer.mozilla.org/en-US/docs/Web/Web_Components) decorated lifecycle :

1. Execute component **constructor** and parse **.component-config**
2. Execute component **connectedCallback** : register component in [ComponentManager](assets/js/ComponentManager.js) and trigger event
3. Go to **initializedState**
4. Register listener to deferred **mountState** or directly go to **mountState**
5. Components states are manages by **[Machine](assets/js/machine/Machine.js)** instance (through **[MachineManage](assets/js/machine/MachineManager.js)**)
6. Each state is a method defined in the component itself (example: **mount(component, parameters)** for **mountState**)
7. Each state have a transition method to go to the next state (ex: mounttransition to go to mountState)
8. During transition, component are locked through **lock(component, parameters)** method
9. After operation the lock is released through **unlock(component, parameters)** method

Then it follows this logic (defined in the default **[Machine](assets/js/machine/Machine.js)**)

```js
this.addState('initialized').addTransition('mount').addTransition('unmount');
this.addState('mount', 'idle').addTransition('idle');
this.addState('idle').addTransition('processing').addTransition('unmount');
this.addState('processing').addTransition('error').addTransition('success');
this.addState('error', 'rendering').addTransition('rendering');
this.addState('success', 'rendering').addTransition('rendering');
this.addState('rendering', 'idle').addTransition('idle');
this.addState('unmount');
```

Each state triggers an event define like this : **action.{actionName}.{componentId}**
Example : for **#form_test_form** component on **mount** -> **action.mount.form_test_form** is triggered.

You can define a validation method for each state's transition.
example : **validationmount(component, parameters)** for transition **mountState**

---

#### Translations

You could create new translation with [Poedit](https://poedit.net/).

Translation are stored in **locales** directory, under a subfolder named locale.

For each translation, you must create this structure :

Example for locale fr_FR

- /locales/fr_FR/LC_MESSAGE/fr_FR.po
- /locales/fr_FR/LC_MESSAGE/fr_FR.mo

In extensions, you must use **dgettext** instead of **gettext** because each extension have it's own domain.
For example, **AdminExtension** is associated to **Admin** domain

Inside main application (app or core namespace), the domain is set to **core**

---

#### Unit tests

##### PHP

Test are located in **tests** folder (that is also a php namespace).

You could launch tests with one of these command :

```shell
vendor/bin/phpunit --testsuite EXTENSIONS
vendor/bin/phpunit --testsuite APP
vendor/bin/phpunit --testsuite CORE
```

##### Javascript

!!! Note: TODO

---

### File structure

---

#### namespace app

| Namespace      | Description                          |
| -------------- | ------------------------------------ |
| api\controller | **Api controllers**                  |
| controller     | **App controllers**                  |
| data           | **Sets of datas reusable**           |
| entities       | **Database entities & Repositories** |
| event          | **Events listeners**                 |
| services       | **Various cross-sectional services** |
| model          | **Database models**                  |
| tasks          | **Async task**                       |
| templates      | **Html templates**                   |
| widgets        | **Html widgets**                     |

---

#### namespace core

| Namespace     | Description                                                 |
| ------------- | ----------------------------------------------------------- |
| api           | **Api base controller and interfaces**                      |
| app           | **Application base and interfaces**                         |
| cache         | **Cache interface and implementations**                     |
| config        | **Config parser class**                                     |
| controller    | **Base controller and interfaces**                          |
| database      | **Database managment**                                      |
| DI            | **Dependencie injection**                                   |
| entities      | **Base entities**                                           |
| events        | **Base listeners**                                          |
| extensions    | **Base extensions**                                         |
| facade        | **Default facades**                                         |
| factory       | **Base factorie(**                                          |
| html          | **Html components**                                         |
| logger        | **Logger interface and implementations**                    |
| mail          | **Mail interface and implementations**                      |
| messages      | **Http request and response interface, and implementation** |
| model         | **Base model**                                              |
| notifications | **Notification service interface and implementations**      |
| providers     | **Default providers**                                       |
| routing       | **Routing classes : router, dispatcher, routes**            |
| secure        | **Security components : authentification, csrf, ACL...**    |
| session       | **Php session interface and implementations**               |
| states        | **Base state object**                                       |
| tasks         | **Async tasks interface and implementations**               |
| utils         | **Various tools**                                           |
| view          | **View interface and implementations**                      |
| transport     | **Transport protocols implementations**                     |
| workers       | **Background worker interface and implementations**         |

---

#### Other folders

| Folder        | Description                               |
| ------------- | ----------------------------------------- |
| assets        | **Javascript and css files**              |
| config        | **Application configuration files**       |
| console       | **Php cli commands**                      |
| constants     | **Applications constants**                |
| cron          | **Default cron scripts**                  |
| extensions    | **Extensions directory**                  |
| functions     | **Various function**                      |
| locales       | **Translations**                          |
| logs          | **Errors and phh logs**                   |
| nodes_modules | **Javascript dependencies**               |
| patches       | **Composer patches**                      |
| public        | **Default server public directory**       |
| templates     | **Base html templates**                   |
| tmp           | **Temporary files**                       |
| tools         | **Docker and other server tools**         |
| updates       | **Updates to execute**                    |
| uploads       | **Upload directory if filesystem is use** |
| vendor        | **Composer dependencies**                 |

---

### Commit rules

| Prefix   | Usage                      |
| -------- | -------------------------- |
| Fix :    | Minor fixes                |
| Refact : | Update an existing feature |
| Format : | Fix code standards         |
| Feat :   | Add new feature            |
| Doc :    | Add documentation          |

---

### Code of conduct

!!! Note: TODO

---

### Stack used

---

#### Container manager

- [docker](https://docs.docker.com/engine/install/) : >= 20.10.5
- [docker-compose](https://docs.docker.com/compose/install/) : >= 1.28.5
- [portainer](https://documentation.portainer.io/) Container manager

---

#### Main

- [traefik](https://doc.traefik.io/traefik/) Proxy - Router
- [httpd](https://httpd.apache.org/docs/2.4/fr/programs/httpd.html) Web server
- php containers
  - main php-fpm
  - worker
  - socket
- [mysql](https://www.mysql.com/fr/) Main database
- [memcached](https://memcached.org/) Php Session storage & Cache storage
- [redis](https://redis.io/documentation) Worker queue storage
- [mongodb](https://www.mongodb.com/)
- [ExpessJs](https://expressjs.com/)
- [Python](https://www.python.org/downloads/release/python-340/) For Pulp solver

---

#### Javascript libraries

- [Fullcalendar](https://fullcalendar.io/)
- [Highway](https://highway.js.org/)
- [Hammer.js](https://hammerjs.github.io/)
- [Fontawesome](https://fontawesome.com/start)
- [Selectize](https://selectize.dev/)
- [Autobahn-browser](https://github.com/crossbario/autobahn-js-browser)
- [Bootstrap 5](https://getbootstrap.com/docs/5.0/getting-started/introduction/)
- [Dexie.js](https://dexie.org/)
- [Excel Editor for VUE 3](https://github.com/cscan/vue3-excel-editor)

---

#### PHP libraries

- [Tracy](https://tracy.nette.org/en/)
- [Nette Database](https://doc.nette.org/en/database)
- [Nette Latte](https://latte.nette.org/)
- [Nette Caching](https://doc.nette.org/en/caching)
- [Nette Finder](https://doc.nette.org/en/finder)
- [Nette Neon](https://doc.nette.org/en/neon)
- [Nette Php generator](https://doc.nette.org/en/php-generator)
- [Nette Safe stream](https://doc.nette.org/en/safe-stream)
- [Predis](https://github.com/predis/predis)
- [Snappy](https://github.com/knplabs/snappy)
- [Phpmailer](https://github.com/PHPMailer/PHPMailer)
- [Thruway](https://github.com/voryx/Thruway)
- [Symfony Process](https://symfony.com/doc/current/components/process.html)
- [Phpoffice](https://github.com/PHPOffice)
- [Clockwork](https://underground.works/clockwork/)

---

#### Python libraries

- [Pulp](https://github.com/coin-or/pulp)

---

#### Stats & analytics

- [fathom](https://usefathom.com/) Gooogle analitycs replacement
- [posthog](https://posthog.com/docs) Gooogle analitycs replacement
- [metabase](https://www.metabase.com/docs/latest/) Database intelligence dashboard

---

#### Development environnement

- [blackfire](https://blackfire.io/docs/introduction) Php performance manager
- [mongo-express](https://github.com/mongo-express/mongo-express)
- [phpmyadmin](https://www.phpmyadmin.net/)
- [php-redis-admin](https://github.com/faktiva/php-redis-admin)
- [mailcatcher](https://mailcatcher.me/)
- [phpmemcachedadmin](https://github.com/elijaa/phpmemcachedadmin)
- [sonarqube](https://www.sonarqube.org/)

---

#### Monitoring

- [prometheus](https://prometheus.io/)
- [grafana](https://grafana.com/)
- [cadvisor](https://github.com/google/cadvisor)
- [filebeat](https://www.elastic.co/fr/beats/filebeat)
- [elasticsearch](elastic.co/fr/elasticsearch/)
- [logstash](https://www.elastic.co/fr/logstash/)
- [kibana](https://www.elastic.co/fr/kibana/)

---

### Recommanded tools to manage project

- [Vscodium](https://vscodium.com/)
  - [Intelephense](https://marketplace.visualstudio.com/items?itemName=bmewburn.vscode-intelephense-client)
  - [Php Phan](https://marketplace.visualstudio.com/items?itemName=TysonAndre.php-phan)
  - [Php mess Detector](https://marketplace.visualstudio.com/items?itemName=ecodes.vscode-phpmd)
  - [Eslint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
  - [Phpcs fixer](https://github.com/junstyle/vscode-php-cs-fixer)
  - [Prettier](https://github.com/prettier/prettier-vscode)
  - [Phpcs](https://github.com/shevaua/vscode-phpcs)
  - [Nette Latte Neon](https://github.com/kasik96/VS-Latte)
- [Vitejs](https://vitejs.dev/)
- [Yarn](https://yarnpkg.com/)
- [Composer](https://getcomposer.org/)

---

### Frequently Asked Questions

**[The dev server doesn't serve my assets](The dev server doesn't serve my assets)**
Because the certificate is self-signate, you must add an exception. Open https://localhost:3000 in your browser to achieve this.

!!! Note: TO BE COMPLETED

---

### Screenshots

!!! Note: TODO

---

### Changelog

---

#### 0.1

- Rewriting all code
