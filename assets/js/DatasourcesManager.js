import Datasource from './data/Datasource';
import Events from './Events';
import App from './App';
/**
 * @var {array} defaultConfig
 */
const defaultConfig = [];
/**
 *
 *  Class DatasourcesManager
 *
 * Represent  Base DatasourcesManager
 *
 * @category Represent html websocketDatasourcesManager
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */

export default class DatasourcesManager extends Events {
  /**
   *
   * @param {App} app
   * @param {object} config
   */
  constructor(app, config = []) {
    super();
    /**
     * @param {App} app
     */
    this.app = app;

    /**
     * Default config components
     * @property {array} config
     */
    this.config = config;

    /**
     * Datasources
     * @property {array<Datasource>}
     */
    this._datasources = {};
  }

  mount() {
    this.app.configd
      .parseConfig(defaultConfig, this.config, this)
      .then((parsedConfig) => {
        this.config = parsedConfig;
      });
  }

  /**
   * Get a datasource
   * @param {string|Datasource} endpoint

   * @returns {Datasource}
   */
  getDatasource(endpoint) {
    if (endpoint instanceof Datasource) {
      return endpoint;
    }
    let url =
      typeof endpoint === 'object'
        ? endpoint.method + ':' + endpoint.endpoint
        : endpoint;

    if (typeof this._datasources[url] === 'undefined') {
      let elements = url.split(':');
      this._datasources[url] = new Datasource(elements[1]);
      this._datasources[url].method = elements[0];
    }

    if (typeof endpoint.parameters != 'undefined') {
      this._datasources[url].parameters = endpoint.parameters;
    }
    return this._datasources[url];
  }

  /**
   * Listen to an endpoint
   *
   * @param {string|Datasource} endpoint
   * @param {function} handler
   */
  attach(endpoint, handler) {
    const datasource =
      endpoint instanceof Datasource ? endpoint : this.getDatasource(endpoint);

    this.on(datasource.getEventName(), handler);
    //this.app.eventd.attach(this.app, datasource.getEventName(), handler);
  }

  /**
   * Stop listening an endpoint
   *
   * @param {string|Datasource} endpoint
   * @param {function|null} handler
   */
  detach(endpoint, handler = null) {
    const datasource =
      endpoint instanceof Datasource ? endpoint : this.getDatasource(endpoint);
    //this.app.eventd.detach(this.app, datasource.getEventName(), handler);
    this.off(datasource.getEventName(), handler);
  }

  /**
   * Watch a datasource and get endpoint as string
   *
   * @param {string|Datasource} endpoint
   * @param {function} handler
   * @returns string
   */
  stringWatch(endpoint, handler) {
    this.attach(endpoint, handler);
    return endpoint;
  }

  /**
   * Watch a datasource and get endpoint as function
   *
   * @param {string|Datasource} endpoint
   * @param {function} handler
   * @returns function
   */
  handlerWatch(endpoint, handler) {
    this.attach(endpoint, handler);
    return () => {
      this.fetch(endpoint);
    };
  }

  /**
   * Update datasource with optionnal datas
   * @param {string|Datasource} endpoint
   * @param {object} datas
   */
  update(endpoint, datas = []) {
    const datasource =
      endpoint instanceof Datasource ? endpoint : this.getDatasource(endpoint);
    datasource.update(datas);
    this.dispatch(datasource.getEventName(), datas);
    //this.app.dispatch(datasource.getEventName(), datas);
  }

  /**
   * Fetch endpoint
   *
   * @param {string|Datasource} endpoint
   * @param {object} parameters
   *
   * @returns {Promise}
   */
  fetch(endpoint, parameters = []) {
    let datasource = this.getDatasource(endpoint);
    return this.app.configd
      .parseConfig(parameters, datasource.parameters, datasource)
      .then(() => {
        this.app.ajaxd.call(datasource).then((result) => {
          this.update(endpoint, result);
        });
      });
  }

  /**
   * Unmount component
   */
  unmount() {
    this._datasources = [];
  }

  /**
   * Reload component
   */
  reload() {
    return this;
  }
}
