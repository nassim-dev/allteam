import App from './App';
/**
 *
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */

/**
 * Check if item is an object
 * @param {*} item
 *
 * @returns {boolean}
 */
const isObject = function (item) {
  return item && typeof item === 'object' && !Array.isArray(item);
};

/**
 * Deep clone of object
 * @param {*} obj
 */
const clone = function (obj) {
  let copy;

  // Handle the 3 simple types, and null or undefined
  if (obj == null || 'object' !== typeof obj) return obj;

  // Handle Date
  if (obj instanceof Date) {
    copy = new Date();
    copy.setTime(obj.getTime());
    return copy;
  }

  // Handle Array
  if (obj instanceof Array) {
    copy = [];
    for (let i = 0, len = obj.length; i < len; i++) {
      copy[i] = clone(obj[i]);
    }
    return copy;
  }

  // Handle Object
  if (obj instanceof Object) {
    copy = {};
    for (const attr in obj) {
      if (obj.hasOwnProperty(attr)) copy[attr] = clone(obj[attr]);
    }
    return copy;
  }

  throw new Error("Unable to copy obj! Its type isn't supported.");
};

/**
 * Slugify a string
 * @param {string} str
 */
const slugify = function (str) {
  str = str.replace(/^\s+|\s+$/g, '');

  // Make the string lowercase
  str = str.toLowerCase();

  // Remove accents, swap ñ for n, etc
  const from =
    'ÁÄÂÀÃÅČÇĆĎÉĚËÈÊẼĔȆÍÌÎÏŇÑÓÖÒÔÕØŘŔŠŤÚŮÜÙÛÝŸŽáäâàãåčçćďéěëèêẽĕȇíìîïňñóöòôõøðřŕšťúůüùûýÿžþÞĐđßÆa·/_,:;';
  const to =
    'AAAAAACCCDEEEEEEEEIIIINNOOOOOORRSTUUUUUYYZaaaaaacccdeeeeeeeeiiiinnooooooorrstuuuuuyyzbBDdBAa------';
  for (let i = 0, l = from.length; i < l; i++) {
    str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
  }

  // Remove invalid chars
  str = str
    .replace(/[^a-z0-9 -]/g, '')
    // Collapse whitespace and replace by -
    .replace(/\s+/g, '-')
    // Collapse dashes
    .replace(/-+/g, '-')
    .replace(/^-+/, '') // Trim - from start of text
    .replace(/-+$/, ''); // Trim - from end of text

  return str;
};

/**
 * Extend object from another object
 * @param {*} child
 * @param {*} parent
 */
const extend = function (child, parent) {
  let Surrogate = function () {};

  Surrogate.prototype = parent.prototype;
  child.prototype = new Surrogate();
};

/**
 * Use propertie from another object
 * @param {*} child
 * @param {*} trait
 */
const use = function (child, trait) {
  for (const propertie in trait) {
    Object.defineProperties(child, { propertie: clone(trait[propertie]) });
  }
};

export { clone, slugify, extend, use, isObject };
