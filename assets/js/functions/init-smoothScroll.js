/**
 * @category Represent html CommandManager
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 */
let navbar = document.getElementById('navbar-header');

function scrollTo(element) {
  let headerOffset = navbar ? 50 : 0;
  let elementPosition = element.offsetTop;
  let offsetPosition = elementPosition - headerOffset;

  window.scrollTo({
    top: offsetPosition,
    behavior: 'smooth',
  });
}

function initSmoothScroll() {
  const innerLinks = document.querySelectorAll(
    "a[data-scroll*='#'],button[data-scroll*='#']"
  );

  innerLinks.forEach((a) => {
    a.addEventListener('click', (e) => {
      let ancre = e.target.tagName == 'A' ? e.target : null;

      if (ancre == null && e.target.tagName == 'SPAN') {
        ancre = e.target.parentNode.tagName == 'A' ? e.target.parentNode : null;
      }
      if (ancre == null) {
        return;
      }

      let element = document.getElementById(
        ancre.dataset.scroll.split('#').pop()
      );

      scrollTo(element);
    });
  });
}

export { scrollTo, initSmoothScroll };
