import App from './App';
import Events from './Events';

/**
 *
 *  Class Store
 *
 * Represent  Base Store
 *
 * @category Represent html websocketStore
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 */

export default class Store extends Events {
  /**
   *
   * @param {App} app
   */
  constructor(app) {
    super();
    /**
     * @property {App} app
     */
    this.app = app;

    /**
     * @property {Proxy}
     */
    this.proxy = {};

    /**
     * All stored states
     * @property {object}
     */
    this.store = {};

    /**
     * @property {Set}
     */
    this.subscriptions = {};
  }

  /**
   * Subscribe to a state
   *
   * @param {*} receiver
   * @param {string} prop
   * @param {function|null} handler
   */
  subscribe(receiver, prop, handler = null) {
    if (typeof this.subscriptions[prop] === 'undefined') {
      this.subscriptions[prop] = new Set();
    }
    if (!this.subscriptions[prop].has(receiver)) {
      let event = 'state.change.' + prop;
      this.on(event, (e) => {
        if (handler != null) {
          handler(e, receiver);
        } else {
          receiver[prop] = e.detail.newValue;
        }
      });
      this.subscriptions[prop].add(receiver);
    }
  }

  /**
   * unsubscribe to a state
   *
   * @param {*} receiver
   * @param {string} prop
   */
  unsubscribe(receiver, prop) {
    if (typeof this.subscriptions[prop] === 'undefined') {
      this.subscriptions[prop] = new Set();
    }
    if (this.subscriptions[prop].has(receiver)) {
      this.subscriptions[prop].delete(receiver);
    }
  }

  /**
   * Get a state
   *
   * @param {string} prop
   * @param {*} receiver
   * @param {function|null} handler
   * @returns
   */
  get(prop, receiver, handler = null) {
    this.subscribe(receiver, prop, handler);
    return this.proxy[prop];
  }

  /**
   * Set a state
   *
   * @param {string} prop
   * @param {*} value
   * @param {*} receiver
   * @param {function|null} handler
   */
  set(prop, value, receiver, handler = null) {
    this.subscribe(receiver, prop, handler);
    this.proxy[prop] = value;
  }

  /**
   * Mount component
   */
  mount() {
    this.proxy = new Proxy(this.store, {
      set: (target, prop, value, receiver) => {
        this.dispatch('state.change.' + prop, {
          key: prop,
          oldValue:
            this.store[prop] != null
              ? this.store[prop].valueOf()
              : this.store[prop],
          updated_at: new Date(),
          newValue: value,
        });

        this.store[prop] = value;
        return true;
      },
      get: (target, prop, receiver) => {
        return this.store[prop] ?? null;
      },
    });
    return this;
  }

  /**
   * Unmount component
   */
  unmount() {
    this.subscriptions = {};
    return this;
  }

  /**
   * Reload component
   */
  reload() {
    return this;
  }
}
