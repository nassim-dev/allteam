import Events from './Events';
import ComponentManager from './ComponentManager';
import ExceptionManager from './ExceptionManager';
import AjaxManager from './AjaxManager';
import LocalStorageManager from './LocalStorageManager';
import MachineManager from './machine/MachineManager';
import WebsocketManager from './WebsocketManager';
import ConfigurationManager from './ConfigurationManager';
import WindowsManager from './WindowsManager';
import InstantMessaging from './InstantMessaging';
import CommandManager from './CommandManager';
import DatasourcesManager from './DatasourcesManager';
import Store from './Store';
import Highway from '@dogstudio/highway';
import DatabaseManager from './DatabaseManager';

const appInit = {
  hashListener: function () {
    //Close modal on previous button click
    window.onhashchange = function (event) {
      let oldHash = event.oldURL.split('#');
      let newHash = event.newURL.split('#');

      if (typeof oldHash[1] != 'undefined' && oldHash[1] != '') {
        let $oldModal = app.componentd.get('modal_' + oldHash[1]);
        if ($oldModal != null) {
          $oldModal.triggerHide();
        }
      }

      if (typeof newHash[1] != 'undefined' && newHash[1] != '') {
        let $newModal = app.componentd.get('modal_' + newHash[1]);
        if ($newModal != null) {
          $newModal.triggerShow();
        }
      }
    };
  },
  //Show initial modal if exists
  openInitialModal: function () {
    if (window.location.hash != '') {
      let initialHash = window.location.hash.replace('#', '');

      if (initialHash != '') {
        setTimeout(() => {
          let $initialModal = app.componentd.get('modal_' + initialHash);
          if ($initialModal != null) {
            $initialModal.triggerShow();
          }
        }, 100);
      }
    }
  },
  //Show menu aside
  asideMenuTrigger: function () {
    document.body.removeAttribute('data-kt-aside-minimize');
  },
  /**
   * Get environnement : prod || dev
   */
  getEnv: function () {
    let env = document.getElementById('ENVIRONNEMENT');
    if (env instanceof HTMLElement) {
      let environnement = env.value;
      env.parentElement.removeChild(env);
      return environnement;
    }
    return 'production';
  },
};
window.appInit = appInit;

/**
 * @var {array} defaultConfig
 */
const defaultConfig = [];
/**
 *
 *  Class App
 *
 * Represent main application
 *
 * @category Represent main application
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */
export default class App extends Events {
  /**
   *
   * @param {object} config
   * @param {function} onConstruct
   * @param {function} onError
   * @param {function} onDestroy
   */
  constructor(config, onConstruct, onError, onDestroy) {
    super();

    this.appInit = appInit;
    window.app = this; //Attach instance to window
    /**
     * Callback exectued after creation
     * @property {function} onConstruct
     */
    this.onConstruct = onConstruct;
    /**
     * Callback exectued when an error occur
     * @property {function} onError
     */
    this.onError =
      typeof onError === 'function'
        ? onError
        : (error, context) => {
            this.exceptiond.addException(error, context);
          };

    /**
     * Callback exectued after destruction
     * @property {function} onDestroy
     */
    this.onDestroy = onDestroy;

    /**
     * Configuration
     * @property {array} config
     */
    this.config = config;

    /**
     * Manipulate configurations
     * @property {ConfigurationManager} _configd
     */
    this._configd = null;

    /**
     * Manipulate datasources updated state
     * @property {DatasourcesManager} _datad
     */
    this._datad = null;

    /**
     * Execute actions
     * @property {CommandManager} _command
     */
    this._command = null;

    /**
     * Manage exception && notifications
     * @property {ExceptionManager} exceptiond
     */
    this._exceptiond = null;

    /**
     * Factory for new component
     * @property {ComponentManager} componentd
     */
    this._componentd = null;

    /**
     * State Machine
     * @property {MachineManager} machined
     */
    this._machined = null;

    /**
     * Interfacing with websocket
     * @property {WebsocketManager} socketd
     */
    this._socketd = null;

    /**
     * Interfacing with ajax
     * @property {AjaxManager} ajaxd
     */
    this._ajaxd = null;

    /**
     * Interfacing with localstorage
     * @property {LocalStorageManager} storaged
     */
    this._storaged = null;

    /**
     * Interfacing with window
     * @property {WindowsManager} windowd
     */
    this._windowd = null;

    /**
     * Instant messaging module
     * @property {InstantMessaging} comd
     */
    this._comd = null;

    /**
     * Instant messaging module
     * @property {Store} store
     */
    this._stored = null;

    /**
     * Interfacing with indexedDB
     * @property {DatabaseManager} _databased
     */
    this._databased = null;

    /**
     * Registred modules
     * @property {array} modules
     */
    this.modules = [];

    /**
     * @property {Highway.Core} _transition
     */
    this._transition = null;

    /**
     * Actual Csrf token
     */
    this.CSRF = null;

    /**
     * Actual Jwt token
     */
    this.JWT = null;
  }

  /**
   *
   * @param {string} elementId
   * @returns {object}
   */
  getToken(elementId) {
    if (this[elementId] === null) {
      let $token = document.querySelector('#' + elementId);
      this[elementId] = {
        name: $token?.name,
        value: $token?.value,
      };

      $token?.parentElement?.removeChild($token);
    }

    return this[elementId];
  }

  /**
   * Mount component && assign default modules to properties
   */
  mount() {
    this.configd
      .parseConfig(defaultConfig, this.config, this)
      .then((parsedConfig) => {
        this.config = parsedConfig;
        if (typeof this.onConstruct === 'function') {
          this.onConstruct(this);
        }
      })
      .then(() => {
        this.getToken('CSRF');
        this.getToken('JWT');
        this.dispatch('app.mount');
      })
      .then(() => {
        this.componentd.initModeSwitchListener();
      })
      .catch((error) => {
        if (typeof this.onError === 'function') {
          this.onError(error, this);
        }
      });
    return this;
  }

  switchPage() {
    //this.componentd.setMode('async');
    this.CSRF = null;
    this.JWT = null;
    return this;
  }

  ready(callbackFunction) {
    if (document.readyState === 'complete') {
      callbackFunction();
    } else {
      let listener = () => {
        if (document.readyState === 'complete') {
          callbackFunction();
          document.removeEventListener('readystatechange', listener);
        }
      };
      document.addEventListener('readystatechange', listener);
    }
  }

  /**
   * Unmount component
   */
  unmount() {
    new Promise((resolve) => {
      this.modules.forEach((module) => {
        module.unmount();
      }, this);

      resolve(true);
    })
      .then(() => {
        if (typeof this.onDestroy === 'function') {
          this.onDestroy();
        }
      })
      .catch((error) => {
        if (typeof this.onError === 'function') {
          this.onError(error, this);
        }
      });
    return this;
  }

  /**
   * Reload component
   */
  reload() {
    this.command.reload();
    this.componentd.reload();
    this.ajaxd.reload();
    this.socketd.reload();

    return this;
  }

  // Getters

  /**
   * Transition
   *
   * @returns {Highway.Core}
   */
  get transition() {
    if (this._transition == null) {
      this._transition = new Highway.Core(this.config.transitionConfig);
    }
    return this._transition;
  }

  /**
   * Manipulate configurations
   *
   * @returns {ConfigurationManager}
   */
  get configd() {
    if (this._configd == null) {
      this._configd = new ConfigurationManager(this);
      this._configd.mount();
    }
    return this._configd;
  }

  /**
   * Manipulate datasources updated state
   *
   * @returns {DatasourcesManager} _datad
   */
  get datad() {
    if (this._datad == null) {
      this._datad = new DatasourcesManager(this);
      this._datad.mount();
    }
    return this._datad;
  }

  /**
   * Execute actions
   *
   * @returns {CommandManager}
   */
  get command() {
    if (this._command == null) {
      this._command = new CommandManager(this);
      this._command.mount();
    }
    return this._command;
  }

  /**
   * Manage exception && notifications
   *
   * @returns {ExceptionManager} exceptiond
   */
  get exceptiond() {
    if (this._exceptiond == null) {
      this._exceptiond = new ExceptionManager(this, {
        showLogs: this.config.environnement === 'development',
      });
      this._exceptiond.mount();
    }
    return this._exceptiond;
  }

  /**
   * Factory for new component
   *
   * @returns {ComponentManager} componentd
   */
  get componentd() {
    if (this._componentd == null) {
      this._componentd = new ComponentManager(this);
      this._componentd.mount();
    }
    return this._componentd;
  }
  /**
   * Factory for new store
   *
   * @returns {Store} stored
   */
  get stored() {
    if (this._stored == null) {
      this._stored = new Store(this);
      this._stored.mount();
    }
    return this._stored;
  }

  /**
   * State Machine
   *
   * @returns {MachineManager} machined
   */
  get machined() {
    if (this._machined == null) {
      this._machined = new MachineManager(this);
      this._machined.mount();
    }
    return this._machined;
  }

  /**
   * Interfacing with websocket
   *
   * @returns {WebsocketManager} socketd
   */
  get socketd() {
    if (this._socketd == null) {
      this._socketd = new WebsocketManager(this);
      this._socketd.mount();
    }
    return this._socketd;
  }

  /**
   * Interfacing with ajax
   *
   * @returns {AjaxManager} ajaxd
   */
  get ajaxd() {
    if (this._ajaxd == null) {
      this._ajaxd = new AjaxManager(this);
      this._ajaxd.mount();
    }
    return this._ajaxd;
  }

  /**
   * Interfacing with localstorage
   *
   * @returns {LocalStorageManager} storaged
   */
  get storaged() {
    if (this._storaged == null) {
      this._storaged = new LocalStorageManager(this);
      this._storaged.mount();
    }
    return this._storaged;
  }

  /**
   * Interfacing with indexedDB
   *
   * @returns {DatabaseManager} databased
   */
  get databased() {
    if (this._databased == null) {
      this._databased = new DatabaseManager(this, {
        method: 'POST',
        endpoint: '/api/v2/sync',
      });
      this._databased.mount();
    }
    return this._databased;
  }

  /**
   * Interfacing with window
   *
   * @returns {WindowsManager} windowd
   */
  get windowd() {
    if (this._windowd == null) {
      this._windowd = new WindowsManager(this);
      this._windowd.mount();
    }
    return this._windowd;
  }

  /**
   * Instant messaging module
   *
   * @returns {InstantMessaging} comd
   */
  get comd() {
    if (this._comd == null) {
      this._comd = new InstantMessaging(this);
      this._comd.mount();
    }
    return this._comd;
  }
}
