import HtmlFormElement from './components/HtmlFormElement';
import HtmlInputElement from './components/HtmlInputElement';
import HtmlElement from './components/HtmlElement';
import Events from './Events';
import App from './App';
import { string } from 'prop-types';

/**
 * @var {array} defaultConfig
 */
const defaultConfig = [];
/**
 *
 *  Class ComponentManager
 *
 * Represent  object ComponentManager
 *
 * @category Represent object Componentmanager
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */
export default class ComponentManager extends Events {
  /**
   * @param {App} app
   * @param {object} config
   */
  constructor(app, config = []) {
    super();
    /**
     * Main application
     * @property {App} app
     */
    this.app = app;

    /**
     * Default config components
     * @property {array} config
     */
    this.config = config;

    /**
     * Instanciated components
     *
     * @property {Array<HtmlElement|HtmlFormElement|HtmlInputElement>} components
     */
    this.components = [];

    this.mode = 'sync';

    this.stack = [];

    this.listeners = [];
  }

  /**
   * Set mode : sync or async
   * @param {string} mode
   */
  setMode(mode) {
    this.mode = mode;
    this.app.eventd.dispatch('componentd.switchto.' + mode, this);
  }

  initModeSwitchListener() {
    this.app.eventd.attach(this, 'componentd.switchto.sync', () => {
      while (this.stack.length != 0) {
        this.register(this.stack.pop());
      }
    });
  }

  /**
   * Reload component
   */
  reload() {
    return this;
  }

  hideFields(selector) {
    let fields = document.querySelectorAll(selector);

    fields.forEach((field) => {
      let component = this.get(field.id);
      if (component != null) {
        component.hide();
      }
    }, this);
  }

  /**
   * Execute setters if provided
   * @param {object} setters
   */
  executeSetters(setters) {
    for (let field in setters) {
      for (let i in this.components) {
        if (i.includes(field)) {
          this.components[i].setValue(setters[field]);
        }
      }
    }
  }

  /**
   * Mount component
   */
  mount() {
    this.app.configd
      .parseConfig(defaultConfig, this.config, this)
      .then((config) => {
        this.config = config;
      });

    return this;
  }

  /**
   * Unmount component
   */
  unmount() {
    new Promise((resolve) => {
      this.components.forEach((component) => {
        component.goto('unmount');
      });
      resolve(true);
    }).then(
      (succes) => {
        return true;
      },
      (error) => {
        return true;
      }
    );
    return this;
  }

  /**
   * Return true if elementy is registred
   *
   * @param {string} key
   * @returns {boolean}
   */
  isRegistred(key) {
    return typeof this.components[key] !== 'undefined';
  }

  /**
   * Register component
   *
   * @param {HtmlElement|HtmlFormElement|HtmlInputElement} component
   */
  register(component) {
    if (this.mode == 'async') {
      this.stack.push(component);
      return;
    }

    if (!this.isRegistred(component.id)) {
      let listener = () => {
        this.components[component.id] = component;
        this.initializeComponent(component);
      };
      if (document.readyState === 'complete') {
        listener();
        return;
      }
      this.app.ready(listener);
    }
  }

  /**
   * Deregister component
   *
   * @param {HtmlElement|HtmlFormElement|HtmlInputElement} component
   */
  deregister(component) {
    component.goto('unmount');
    if (this.isRegistred(component.id)) {
      component.machine.deregister(component);
      delete this.components[component.id];
    }
  }

  /**
   * Return a registred component
   * @param {string} key

   * @returns {HtmlElement|HtmlFormElement|HtmlInputElement|null}
   */
  get(key) {
    if (typeof key != 'string') {
      key = key[0];
    }
    if (this.isRegistred(key)) {
      return this.components[key];
    }

    this.app.exceptiond.addException('Innexisting component', key);

    return null;
  }

  /**
   * Initialize new component
   *
   * @param {HtmlElement|HtmlFormElement|HtmlInputElement} component
   */
  initializeComponent(component) {
    if (
      typeof component.config.buildOn == 'undefined' ||
      !component.config.buildOn
    ) {
      this.build(component);
      return;
    }

    /**
     * @var {HtmlElement|HtmlFormElement|HtmlInputElement} $selector
     */

    const $selector = document.querySelector(
      component.config.buildOn.parameters.selector
    );

    if ($selector === null) {
      this.build(component);
      return;
    }

    if (component.config.buildOn.value == 'onViewPort') {
      //this.app.exceptiond.addException(
      //  component.id + ' will be constructed onViewPort event',
      //  component
      //);
      this.app.eventd.os.on(
        component.config.buildOn.parameters.event ?? 'enter',
        $selector,
        () => {
          this.build(component);
        }
      );
      return;
    }

    /**
     * Deffered mounting
     */
    if (component.config.buildOn.value == 'deferred') {
      //this.app.exceptiond.addException(
      //  component.id +
      //    ' will be constructed on ' +
      //    component.config.buildOn.parameters.event +
      //    ' event',
      //  $selector.id
      //);
      $selector.addEventListener(
        component.config.buildOn.parameters.event,
        () => {
          this.build(component);
        },
        {
          once: true,
        }
      );
    }
  }

  /**
   *
   * @param {HtmlElement|HtmlFormElement|HtmlInputElement} component
   */
  build(component) {
    if (component != null && component.is('initialized')) {
      component.goto('mount');
    }
  }
}
