import Events from './Events';
import App from './App';
import Dexie from 'dexie';
import 'dexie-observable';
import 'dexie-syncable';
import Datasource from './data/Datasource';

Dexie.Syncable.registerSyncProtocol('CustomAjax', {
  sync: function (
    context,
    url,
    options,
    baseRevision,
    syncedRevision,
    changes,
    partial,
    applyRemoteChanges,
    onChangesAccepted,
    onSuccess,
    onError
  ) {
    /// <param name="context" type="IPersistedContext"></param>
    /// <param name="url" type="String"></param>
    /// <param name="changes" type="Array" elementType="IDatabaseChange"></param>
    /// <param name="applyRemoteChanges" value="function (changes, lastRevision, partial, clear) {}"></param>
    /// <param name="onSuccess" value="function (continuation) {}"></param>
    /// <param name="onError" value="function (error, again) {}"></param>

    var POLL_INTERVAL = 10000; // Poll every 10th second

    // In this example, the server expects the following JSON format of the request:
    //  {
    //      [clientIdentity: unique value representing the client identity. If omitted, server will return a new client identity in its response that we should apply in next sync call.]
    //      baseRevision: baseRevision,
    //      partial: partial,
    //      changes: changes,
    //      syncedRevision: syncedRevision
    //  }
    //  To keep the sample simple, we assume the server has the exact same specification of how changes are structured.
    //  In real world, you would have to pre-process the changes array to fit the server specification.
    //  However, this example shows how to deal with ajax to fulfil the API.
    var request = {
      clientIdentity: context.clientIdentity || null,
      baseRevision: baseRevision,
      partial: partial,
      changes: changes,
      syncedRevision: syncedRevision,
    };
    window.app.ajaxd.post(url, request).then((data) => {
      // In this example, the server response has the following format:
      //{
      //    success: true / false,
      //    errorMessage: "",
      //    changes: changes,
      //    currentRevision: revisionOfLastChange,
      //    needsResync: false, // Flag telling that server doesn't have given syncedRevision or of other reason wants client to resync. ATTENTION: this flag is currently ignored by Dexie.Syncable
      //    partial: true / false, // The server sent only a part of the changes it has for us. On next resync it will send more based on the clientIdentity
      //    [clientIdentity: unique value representing the client identity. Only provided if we did not supply a valid clientIdentity in the request.]
      //}
      if (!data.success) {
        onError(data.errorMessage, Infinity); // Infinity = Don't try again. We would continue getting this error.
      } else {
        if ('clientIdentity' in data) {
          context.clientIdentity = data.clientIdentity;
          // Make sure we save the clientIdentity sent by the server before we try to resync.
          // If saving fails we wouldn't be able to do a partial synchronization
          context
            .save()
            .then(() => {
              // Since we got success, we also know that server accepted our changes:
              onChangesAccepted();
              // Convert the response format to the Dexie.Syncable.Remote.SyncProtocolAPI specification:
              applyRemoteChanges(
                data.changes,
                data.currentRevision,
                data.partial,
                data.needsResync
              );
              onSuccess({ again: POLL_INTERVAL });
            })
            .catch((e) => {
              // We didn't manage to save the clientIdentity stop synchronization
              onError(e, Infinity);
            });
        } else {
          // Since we got success, we also know that server accepted our changes:
          onChangesAccepted();
          // Convert the response format to the Dexie.Syncable.Remote.SyncProtocolAPI specification:
          applyRemoteChanges(
            data.changes,
            data.currentRevision,
            data.partial,
            data.needsResync
          );
          onSuccess({ again: POLL_INTERVAL });
        }
      }
    });
  },
});

/**
 * @var {array} defaultConfig
 */
const defaultConfig = [];

/**
 *
 *  Class DatabaseManager
 *
 * Represent  Base DatabaseManager
 *
 * @category Represent html websocketDatabaseManager
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */
export default class DatabaseManager extends Events {
  /**
   *
   * @param {App} app
   * @param {object} config
   */
  constructor(app, config = {}) {
    super();
    /**
     * @property {App} app
     */
    this.app = app;

    /**
     * Default config components
     * @property {object} config
     */
    this.config = config;

    /**
     * Db
     * @property {Dexie|null} db
     */

    this.db = null;

    /**
     * Is indexedDB activated on client side
     */
    this.isAvailable = true;

    /**
     * List of updated store on client side
     */
    this.updatedStore = [];

    /**
     * Last database update
     */
    this.lastUpdate = new Date();
  }

  /**
   * Mount component
   */
  mount() {
    this.app.configd
      .parseConfig(defaultConfig, this.config, this)
      .then((parsedConfig) => {
        this.config = parsedConfig;
        if (!window.indexedDB) {
          this.app.exceptiond.addException(
            'Unable to activate indexedDB',
            this
          );
          this.isAvailable = false;
        }
      })
      .then(() => {
        this.db = new Dexie('allteam');
        this.db.open();
        this.db.syncable.connect('CustomAjax', this.config.endpoint);
        //window.addEventListener('online', this._syncDatabase);
      });
    return this;
  }

  /**
   *
   * @param {Datasource} datasource
   * @returns {Promise}
   */
  parseDatasource(datasource) {
    let store = this.getStore(datasource.ressource);
    let date = new Date();
    return new Promise((resolve) => {
      if (typeof store == 'undefined') {
        let schema = {};
        schema[datasource.ressource] = 'id';
        this.initStores(schema);
        store = this.getStore(datasource.ressource);
      }
      resolve(store);
      datasource.parameters.map((item) => {
        item.updated_at = date;
      });
    }).then(() => {
      this.updatedStore.push({
        store: store,
        datasource: datasource,
      });

      switch (datasource.method) {
        case 'PUT':
          store?.bulkPut(datasource.parameters);
        case 'GET':
          store?.bulkGet(datasource.parameters);
        case 'POST':
          store?.bulkAdd(datasource.parameters);
        case 'DELETE':
          store?.bulkDelete(datasource.parameters);
        default:
          this.app.exceptiond.addException(
            'Invalid method : ' + datasource.method,
            this
          );
      }
    });
  }

  /**
   * Sync Database with backoffice
   * @param {Event} event
   */
  async _syncDatabase(event) {
    while (this.updatedStore.length != 0) {
      let element = this.updatedStore.pop();
      this.app.ajaxd.call(
        element.datasource,
        await element.store
          ?.where('updated_at')
          .above(this.lastUpdate)
          .toArray()
      );
    }
  }
  /**
   * Init database schema
   * @param {object} schema
   */
  initStores(schema) {
    this.db?.version(1).stores(schema);
  }
  /**
   *
   * @param {string} store
   * @param {object} items
   */
  updateStore(store, items) {
    this.getStore(store)?.bulkPut(items);
  }

  /**
   *
   * @param {string} store
   * @returns
   */
  getStore(store) {
    return this.db?.table(store);
  }

  /**
   * Unmount component
   */
  unmount() {
    this.db?.close();
    //window.removeEventListener('online', this._syncDatabase);
    return this;
  }

  /**
   * Reload component
   */
  reload() {
    return this;
  }
}
