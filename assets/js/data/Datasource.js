/**
 *
 *  Class Datasource
 *
 * Represent  Base Datasource
 *
 * @category Represent html websocketDatasource
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */
export default class Datasource {
  /**
   *
   * @param {string} endpoint
   */
  constructor(endpoint) {
    /**
     * Datasource listeners
     * @property {array<HTMLElement>} listeners
     */
    this.listeners = [];

    /**
     * Api version
     * @property {string} apiVersion
     */
    this.apiVersion = null;

    /**
     * Api ressource
     * @property {string} ressource
     */
    this.ressource = null;

    /**
     * Endpoint parameters
     * @property {object} parameters
     */
    this.parameters = [];

    /**
     * Api endpoint
     * @property {string} endpoint
     */
    this.endpoint = endpoint;

    /**
     * Last datas
     * @property {object} datas
     */
    this.datas = {};

    /**
     * Last updated
     * @property {Date} lastUpdated
     */
    this.lastUpdated = new Date();

    /**
     * Http method
     */
    this.method = null;

    this.parseEndpoint(endpoint);
  }

  /**
   * Return endpoint components
   * @param {string} endpoint
   * /apiVersion/ressource/parameter/parameter/parameter
   *
   * @returns {object}
   */
  parseEndpoint(endpoint) {
    // Remove first && last slash
    if (endpoint.charAt(0) === '/') {
      endpoint = endpoint.substr(1);
    }
    if (endpoint.charAt(endpoint.length - 1) === '/') {
      endpoint = endpoint.substr(0, endpoint.length - 1);
    }

    const endpointParts = endpoint.split('/');

    this.apiVersion = endpointParts.shift();
    this.ressource = endpointParts.shift();
    this.parameters = endpointParts;
  }

  /**
   * Update object with new datas
   * @param {object} datas
   */
  update(datas = []) {
    this.datas = datas;
    this.lastUpdated = new Date();
  }

  /**
   * Return event name
   *
   * @returns {string}
   */
  getEventName() {
    return this.endpoint;
  }
}
