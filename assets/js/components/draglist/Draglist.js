import HtmlElement from '../HtmlElement';
import { DataSet } from 'vis-data';
import { KTBlockUI } from '../../vendor/components/blockui';
import utils from '../CustomElementUtils';
//import '../../../../node_modules/jkanban/dist/jkanban.js';
//import '../../../../node_modules/jkanban/dist/jkanban.css';

const privateMethods = {
  initFromDom: (instance) => {
    let name;
    let category;
    let childs;
    for (let i in instance.config.childs) {
      name = instance.config.childs[i].split('_');
      category = document.querySelector(instance.config.childs[i]);
      childs = new DataSet();

      for (let j in category.children) {
        if (typeof category.children[j].dataset != 'undefined') {
          childs.add({
            id: category.children[j].dataset.uuid,
            content: category.children[j],
          });
        }
      }
      instance.categories[name[1]] = {
        element: category,
        childs: childs,
      };
    }
  },
};
/**
 *
 *  Class Draglist
 *
 * Represent html draglist
 *
 * @category Represent html draglist
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 * @param {object} config
 */
export default class Draglist extends HtmlElement {
  constructor() {
    super();

    this.categories = {};
    this.childs = {};

    this.blockUi = new KTBlockUI(this.parentElement);
    if (!this.blockUi.isBlocked()) {
      this.blockUi.block();
    }
  }

  /**
   * Initialize component
   *
   * @returns  {Promise}
   */
  mount() {
    return super
      .mount()
      .then(() => {
        privateMethods.initFromDom(this);
        //this.wrapper = new jKanban();
      })
      .then(() => {
        utils.loadDatas(this);
      })
      .then(() => {
        //if (this.blockUi.isBlocked()) {
        this.blockUi.release();
        //}
      });
  }

  /**
   * Add new item
   * @param {object} properties
   * @param {number|string} idcategory
   *
   */
  addElement({ id, category, html }, idcategory) {
    if (typeof this.categories[idcategory] == 'undefined') {
      return;
    }
    let oldItem = this.categories[idcategory].childs.get(id);
    if (oldItem) {
      oldItem.content.remove();
      this.categories[idcategory].childs.remove(id);
    }

    let newItem = document.createElement('div');
    newItem.innerHTML = html;

    this.categories[idcategory].element.appendChild(newItem);

    let item = {
      id: id,
      content: newItem,
    };

    this.categories[idcategory].childs.update(item);
  }

  addCategory(properties) {
    if (typeof this.categories[properties.id] === 'undefined') {
      this.categories[properties.id].properties = properties;
      this.categories[properties.id].childs = new DataSet(properties);
    }
  }

  /**
   * Destroy component
   */
  unmount() {
    super.unmount();
    return this;
  }

  /**
   * Reload component
   */
  reload() {
    super.reload();

    utils.loadDatas(this);
    return this;
  }
}
