import HtmlElement from '../HtmlElement';

let commandLoaded = false;
/**
 *
 *  Class Chart
 *
 * Represent html chart
 *
 * @category Represent html chart
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 * @param {object} config
 */
export default class Chart extends HtmlElement {
  constructor() {
    super();
    if (!commandLoaded) {
      commandLoaded = true;
      this.command.addCommand('RenderCommand', async () => {
        return import('../../command/components/chart/RenderCommand');
      });
    }
  }

  /**
   * Initialize component
   *
   * @returns {Chart}
   */
  mount() {
    return super.mount();
  }

  /**
   * Destroy component
   */
  unmount() {
    super.unmount();
  }

  /**
   * Reload component
   */
  reload() {
    return super.reload();
  }
}
