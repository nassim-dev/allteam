import GanttPluggin from 'frappe-gantt';
import HtmlElement from '../HtmlElement';
import { DataSet } from 'vis-data';

const privateMethods = {
  initChangeViewButtons: (instance) => {
    $('.btn-gantt-chart').on('click', function () {
      let $btn = $(this);
      var mode = $btn.text();

      instance.change_view_mode(mode);
      $btn.parent().parent().find('button').removeClass('active');
      $btn.addClass('active');
    });
  },
  onClick: (task, event, instance) => {},
  onBarClick: (task) => {},
  onViewChange: (mode) => {},
  onDateChange: (task, start, end) => {},
  onProgressChange: (task, progress) => {},
  onDateAdded: (start, end, property, propertyIdx, xPosition, yPosition) => {},
};

/**
 *
 *  Class Gantt
 *
 * Represent html gantt
 *
 * @category Represent html gantt
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 */
export default class Gantt extends HtmlElement {
  constructor() {
    super();

    /** @property GanttPluggin wrapper */
    this.wrapper = document.querySelector(this.id + '_wrapper');
    this.items = new DataSet();
  }

  /**
   * Initialize component
   *
   * @returns  {Promise}
   */
  mount() {
    return super.mount().then(() => {
      this.wrapper = new GanttPluggin(this.wrapper, this.items, this.config);
    });
  }

  /**
   * Destroy component
   */
  unmount() {
    super.unmount();
    return this;
  }

  /**
   * Reload component
   */
  reload() {
    super.reload();
    this.wrapper.refresh(this.items);
    return this;
  }

  getDefaultConfig() {
    return {
      on_click: (task) => {
        privateMethods.onClick(task, this.wrapper);
      },
      on_date_change: (task, start, end) => {
        privateMethods.onDateChange(task, start, end);
      },
      on_progress_change: (task, progress) => {
        privateMethods.onProgressChange(task, progress);
      },
      on_view_change: (mode) => {
        privateMethods.onViewChange(mode);
      },

      on_date_added: (
        start,
        end,
        property,
        propertyIdx,
        xPosition,
        yPosition
      ) => {
        // executed with double click on column
        privateMethods.onDateAdded(
          start,
          end,
          property,
          propertyIdx,
          xPosition,
          yPosition
        );
      },
      custom_click_on_bar: (task) => {
        // only works if popup_trigger is not set to "click"
        privateMethods.onBarClick(task);
      },
      header_height: 50,
      column_width: 30,
      step: 24,
      view_modes: ['Quarter Day', 'Half Day', 'Day', 'Week', 'Month'],
      popup_trigger: 'mouseover',
      bar_height: 20,
      bar_corner_radius: 3,
      arrow_curve: 5,
      padding: 18,
      view_mode: 'Day',
      date_format: 'YYYY-MM-DD',
      custom_popup_html: null,
      start_date: this.config.initialStart ?? new Date(),
      end_date: this.config.initialEnd ?? new Date(),
      show_label: true,
      animations_active: false,
      init_scroll_position: 0,
    };
  }
}
