import HtmlElement from './HtmlElement';
import HtmlFormElement from './HtmlFormElement';
import HtmlInputElement from './HtmlInputElement';

const utils = {
  /**
   *
   * @param {HtmlElement|HtmlInputElement|HtmlFormElement} instance
   */
  parseHtmlConfig: (instance) => {
    let initialConfig = instance.querySelector('.component-config');

    if (initialConfig != null) {
      let config = initialConfig.innerHTML.trim();
      if (config.length > 0) {
        instance.config = Object.assign(instance.config, JSON.parse(config));
      }
      instance.removeChild(initialConfig);
    }
  },
  saveDatas: (instance, properties) => {
    if (typeof instance.config.endpoints !== 'undefined') {
      let config = instance.config.endpoints;

      if (typeof config.updateData == 'undefined') {
        return;
      }

      let datasource = instance.app.datad.getDatasource(config.updateData);
      instance.app.ajaxd.call(datasource, properties ?? {}).then((response) => {
        if (typeof response.content == 'undefined') {
          return;
        }
        instance.initFromState(response.content);
      });
    }
  },
  loadDatas: (instance, properties) => {
    if (typeof instance.config.endpoints !== 'undefined') {
      let config = instance.config.endpoints;

      if (typeof config.readData == 'undefined') {
        return;
      }

      let datasource = instance.app.datad.getDatasource(config.readData);

      if (typeof properties != 'undefined') {
        datasource.parameters = properties;
      }

      instance.app.datad.handlerWatch(datasource, (e) => {
        instance.initFromState(e.detail.content);
      });

      instance.app.ajaxd.call(datasource).then((response) => {
        if (typeof response.content == 'undefined') {
          return;
        }
        instance.initFromState(response.content);
      });
    }
  },
  /**
   *
   * @param {HtmlElement|HtmlInputElement|HtmlFormElement} instance
   */
  _appHandler: (instance) => {
    return (event) => {
      instance.app.componentd.register(instance);
    };
  },
  /**
   * Invoked each time the custom element is disconnected
   *
   * @param {HtmlElement|HtmlInputElement|HtmlFormElement} instance
   */
  disconnectedCallback: (instance) => {
    instance.removeEventListener('app.mount', utils._appHandler(instance));
    for (var event in instance.listeners) {
      for (var i in instance.listeners[event]) {
        instance.removeEventListener(event, instance.listeners[event][i]);
      }
    }
    instance.app.componentd.deregister(instance);
  },

  /**
   * Invoked each time the custom element is appended
   *
   * @param {HtmlElement|HtmlInputElement|HtmlFormElement} instance
   */
  connectedCallback: (instance) => {
    if (typeof instance.app === 'undefined') {
      instance.addEventListener('app.mount', utils._appHandler(instance));
      return;
    }
    instance.app.componentd.register(instance);
  },

  /**
   * Initialize component
   * @param {HtmlElement|HtmlInputElement|HtmlFormElement} instance
   * @returns {Promise}
   */
  mount: (instance) => {
    return instance.app.configd
      .parseConfig(instance.config, instance.getDefaultConfig(), instance)
      .then((parsedConfig) => {
        instance.config = parsedConfig;
        if (
          instance.wrapper !== null &&
          typeof instance.wrapper !== 'undefined'
        ) {
          if (typeof instance.wrapper.addEventListener === 'function') {
            instance.bindedEvents.forEach((event) => {
              instance.wrapper.addEventListener(event, this);
            });
          }
        }
      });
  },
  /**
   * Destroy component
   *
   * @param {HtmlElement|HtmlInputElement|HtmlFormElement} instance
   */
  unmount: (instance) => {
    if (instance.wrapper === null) {
      return;
    }
    instance.bindedEvents.forEach((event) => {
      if (
        typeof instance.wrapper !== 'undefined' &&
        typeof instance.wrapper.removeEventListener === 'function'
      ) {
        instance.wrapper.removeEventListener(event, instance);
      }
    });
  },
  /**
   * Add event resize listener if enabled in config
   * @param {object} listener
   * @param {HtmlElement|HtmlInputElement|HtmlFormElement} instance
   *
   */
  addListener: (listener, instance) => {
    let handler = (event) => {
      event.preventDefault();
      instance.app.exceptiond.addException(
        'Event triggered in component',
        event
      );
      //listener.info = info;

      if (typeof listener.callback.addPopup !== 'undefined') {
        instance.app.windowd.createPopup(listener.callback.addPopup, instance);
        return;
      }
      let commandName = listener.callback.split('/');
      commandName = commandName.pop();
      if (!instance.command.commandExist(commandName)) {
        instance.command.addCommand(commandName, async () => {
          return import(`../command/${listener.callback}.js`);
        });
      }
      instance.command.execute(instance, commandName, listener);
    };
    instance.addEventListener(listener.event, handler);
    if (typeof instance.listeners[listener.event] == 'undefined') {
      instance.listeners[listener.event] = [];
    }
    instance.listeners[listener.event].push(handler);
  },
  /**
   * Attach event listener
   *
   * @param {HtmlElement|HtmlInputElement|HtmlFormElement} instance
   * @param {string} event event to watch
   * @param {function} handler function or command name to exectute
   * @param {string|null} selector string selector
   * @param {array} parameters handler specific parameters
   */ on: (instance, event, handler, selector = null, parameters = []) => {
    instance.addEventListener(
      event,
      utils._getHandler(instance, handler, parameters)
    );
    if (typeof instance.listeners[event] == 'undefined') {
      instance.listeners[event] = [];
    }
    instance.listeners[event].push(
      utils._getHandler(instance, handler, parameters)
    );
  },

  /**
   * Remove all events listeners for this type of event
   *
   * @param {HtmlElement|HtmlInputElement|HtmlFormElement} instance
   * @param {string} event
   * @param {string|null} selector string selector
   */
  off: (instance, event, selector = null) => {
    for (var i in instance.listeners[event]) {
      instance.removeEventListener(event, instance.listeners[event][i]);
    }
  },

  /**
   * Get function handler
   *
   * @param {HtmlElement|HtmlInputElement|HtmlFormElement} instance
   * @param {string|function} callback
   * @param {array} parameters
   */
  _getHandler: (instance, callback, parameters) => {
    /**
     * @var {Event} event
     */
    return (event) => {
      event.preventDefault();
      instance.app.exceptiond.addException(
        'Event triggered in component',
        event
      );
      if (instance.command.commandExist(callback)) {
        return instance.command.execute(instance, callback, parameters);
      }
    };
  },

  /**
   * Set/Get datas attributes
   *
   * @param {HtmlElement|HtmlInputElement|HtmlFormElement} instance
   * @param {string} key
   * @param {*} value
   *
   * @returns {string|null|object}
   */
  datas: (instance, key = null, value = null) => {
    if (typeof key == null) {
      return instance.dataset;
    }
    if (value !== null && key !== null) {
      instance.dataset[key] = value;
      return null;
    }

    return typeof instance.dataset[key] !== 'undefined'
      ? instance.dataset[key]
      : null;
  },
};

export default utils;
