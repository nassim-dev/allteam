import HtmlFormElement from '../HtmlFormElement';

/**
 *
 *  Class Stepper
 *
 * Represent html stepper
 *
 * @category Represent html stepper
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 * @param {object} config
 */
export default class Stepper extends HtmlFormElement {
  constructor() {
    super();

    /**
     * Real Dom events to bind to this object
     * @property {array} bindedEvents
     * @protected
     */
    this.bindedEvents = this.config.bindedEvents
      ? [...this.config.bindedEvents, 'submit']
      : ['submit'];
  }

  /**
   * Initialize component
   *
   * @returns {Stepper}
   */
  mount() {
    return super.mount();
  }

  /**
   * Destroy component
   */
  unmount() {
    super.unmount();
    return this;
  }
  /**
   * Reload component
   */
  reload() {
    super.reload();
    return this;
  }
}
