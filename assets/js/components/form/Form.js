import HtmlFormElement from '../HtmlFormElement';
import { KTBlockUI } from '../../vendor/components/blockui';
let commandLoaded = false;
/**
 *
 *  Class Form
 *
 * Represent html form
 *
 * @category Represent html form
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 * @param {object} config
 */
export default class Form extends HtmlFormElement {
  constructor() {
    super();
    /**
     * Default api endpoint
     * @property {string} endpoint
     */
    this.endpoint = this.attributes.endpoint ?? null;

    /**
     * Api parameters provided dynamically
     * @param {array} endpointDynamicParameters
     */
    this.endpointDynamicParameters = [];

    /**
     * Real Dom events to bind to this object
     * @property {array} bindedEvents
     * @protected
     */
    this.bindedEvents = this.config.bindedEvents
      ? [...this.config.bindedEvents, 'submit']
      : ['submit'];

    this.$submitButton = this.querySelector('button[type="submit"]');

    this.blockUi = new KTBlockUI(this);
    if (!commandLoaded) {
      commandLoaded = true;
      this.command
        .addCommand('CopyCommand', async () => {
          return import('../../command/components/form/CopyCommand');
        })
        .addCommand('LockCommand', async () => {
          return import('../../command/components/form/LockCommand');
        })
        .addCommand('SubmitCommand', async () => {
          return import('../../command/components/form/SubmitCommand');
        })
        .addCommand('SubmitStepCommand', async () => {
          return import('../../command/components/form/SubmitStepCommand');
        })
        .addCommand('PopulateCommand', async () => {
          return import('../../command/components/form/PopulateCommand');
        })
        .addCommand('UnlockCommand', async () => {
          return import('../../command/components/form/UnlockCommand');
        });
    }
  }

  /**
   * Initialize component
   *
   * @returns {Form}
   */
  mount() {
    return super.mount().then(() => {
      if (typeof this.config.listeners != 'undefined') {
        for (let i in this.config.listeners) {
          this.addListener(this.config.listeners[i]);
        }
      }
    });
  }

  clear() {
    this.reset();
    let field;
    for (var i in this.config.childs) {
      field = this.app.componentd.get(this.config.childs[i].replace('#', ''));
      if (field != null) {
        field.clear();
      }
    }
  }

  /**
   * Destroy component
   */
  unmount() {
    super.unmount();
    return this;
  }

  /**
   * Start button icon animation
   */
  startIconAnimation() {
    if (this.$submitButton instanceof HTMLElement) {
      this.$submitButton.disabled = true;
      this.$submitButton.setAttribute('data-kt-indicator', 'on');
    }
    if (!this.blockUi.isBlocked()) {
      this.blockUi.block();
    }
    //this.stopIconAnimation();
  }
  /**
   * Stop button icon animation
   */
  stopIconAnimation() {
    if (this.$submitButton instanceof HTMLElement) {
      this.$submitButton.disabled = false;
      this.$submitButton.removeAttribute('data-kt-indicator');
    }
    if (this.blockUi.isBlocked()) {
      this.blockUi.release();
    }
  }
  /**
   * Method to execute for locking component
   */
  lock() {}

  /**
   * Method to execute for unlocking component
   */
  unlock() {}

  processing() {
    this.startIconAnimation();
  }
  idle() {
    this.stopIconAnimation();
    setTimeout(() => {
      if (this.blockUi.isBlocked()) {
        this.blockUi.release();
      }
    }, 10000);
  }

  /**
   * Reload component
   */
  reload() {
    super.reload();
    return this;
  }

  populate(datas) {
    let field;
    for (let i in this.config.childs) {
      field = this.app.componentd.get(this.config.childs[i].replace('#', ''));
      if (field != null) {
        if (typeof datas[field.config.name] != 'undefined') {
          field.setValue(datas[field.config.name]);
        }
      }
    }
  }
  /**
   * Return form datas
   *
   * @returns {FormData}
   */
  getDatas() {
    let datas = new FormData(this);
    let field;
    for (let i in this.config.childs) {
      field = this.app.componentd.get(this.config.childs[i].replace('#', ''));
      if (field != null) {
        datas.set(field.config.name, field.getValue());
      }
    }
    datas.append(this.id, 'ok');
    return datas;
  }
}
