import FormElement from './FormElement';

import flatpickr from 'flatpickr';
import '../../../../../node_modules/flatpickr/dist/themes/airbnb.css';

/**
 *
 *  Class DateElement
 *
 * Represent html dateElement
 *
 * @category Represent html dateElement
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 * @param {object} config
 */
export default class DateElement extends FormElement {
  constructor() {
    super();
    this.wrapper = null;
  }

  /**
   * Initialize component
   *
   * @returns {Promise}
   */
  mount() {
    return super.mount().then(() => {
      this.config.onChange = (selectedDates, dateStr, instance) => {
        const event = new CustomEvent('change', {
          composed: true,
          bubbles: true,
          cancelable: true,
          detail: { selectedDates },
        });
        this.dispatchEvent(event);
      };
      this.wrapper = flatpickr(
        document.getElementById(this.id + '_input'),
        this.config
      );
    });
  }

  /**
   * Destroy component
   */
  unmount() {
    super.unmount();
    return this;
  }

  /**
   * Reload component
   */
  reload() {
    super.reload();
    return this;
  }

  /**
   * Get default config
   *
   * @returns {object};
   */
  getDefaultConfig() {
    return {
      disableMobile: true,
      allowInput: true,
      enableTime: false,
      dateFormat: 'd/m/Y',
      closeOnSelect: true,
    };
  }
  getValue() {
    return this.wrapper.selectedDates;
  }
  setValue(value, format = null) {
    this.wrapper.setDate(value, false, format ?? this.config.dateFormat);
  }
}
