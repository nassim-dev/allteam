import FormElement from './FormElement';

import '../../../../../node_modules/@selectize/selectize/dist/js/selectize';

import '../../../../../node_modules/@selectize/selectize/dist/css/selectize.css';
import '../../../../../node_modules/@selectize/selectize/dist/css/selectize.bootstrap5.css';
import Handlebars from 'handlebars/dist/handlebars.js';
let templatesJsField = {};
let templatesHandle = {
  'base.handlebars': `
        <div class='row'>
        {{#if title}}
          <div class='col'>
            {{{title}}}
          </div>
        {{/if}}
        {{#if actionbar}}
          <div class='col'>
            {{{actionbar}}}
          </div>
        {{/if}}
      </div>`,
};
//$.fn.select2.defaults.set('theme', 'bootstrap5');

const privateMethods = {
  defineSelectingListener: (element) => {
    element.$wrapperJquery.on('select2:selecting', (e) => {
      let args = e.params.args;
      console.log(e);
      if (typeof args.originalEvent == 'undefined') {
        return element.$wrapperJquery.trigger({
          type: 'select2:select',
          params: {
            data: args.data,
          },
          data: args.data,
        });
      }
      let clickElement = args.originalEvent.target;

      if (
        !clickElement.classList.contains('select2-selection__choice__remove') &&
        (clickElement.nodeName === 'svg' ||
          clickElement.nodeName === 'SVG' ||
          clickElement.nodeName === 'span' ||
          clickElement.nodeName === 'SPAN' ||
          clickElement.nodeName === 'button' ||
          clickElement.nodeName === 'BUTTON' ||
          clickElement.nodeName === 'path' ||
          clickElement.nodeName === 'PATH' ||
          clickElement.nodeName === 'rect' ||
          clickElement.nodeName === 'RECT' ||
          clickElement.nodeName === 'i' ||
          clickElement.nodeName === 'I')
      ) {
        e.preventDefault();
        return false; // stops propogation (so select2:selecting never fires)
      }
    });
  },
  defineUnSelectingListener: (element) => {
    element.$wrapperJquery.on('select2:unselecting', (e) => {
      let args = e.params.args;
      let clickElement = args.originalEvent.target;

      if (
        !clickElement.classList.contains('select2-selection__choice__remove') &&
        (clickElement.nodeName === 'svg' ||
          clickElement.nodeName === 'SVG' ||
          clickElement.nodeName === 'span' ||
          clickElement.nodeName === 'SPAN' ||
          clickElement.nodeName === 'button' ||
          clickElement.nodeName === 'BUTTON' ||
          clickElement.nodeName === 'path' ||
          clickElement.nodeName === 'PATH' ||
          clickElement.nodeName === 'rect' ||
          clickElement.nodeName === 'RECT' ||
          clickElement.nodeName === 'i' ||
          clickElement.nodeName === 'I')
      ) {
        e.preventDefault();
        return false; // stops propogation (so select2:selecting never fires)
      }
    });
  },
  defineInlineListerner: (element) => {
    if (typeof element.config.endpoints.createData !== 'undefined') {
      element.$wrapperJquery.on('change', (e) => {
        let isNew = $(this).find('[data-select2-tag="true"]');
        if (isNew.length && $.inArray(isNew.val(), $(this).val()) !== -1) {
          isNew.replaceWith(
            '<option selected value="' +
              isNew.val() +
              '">' +
              isNew.val() +
              '</option>'
          );
          //modalTag.mainObject.modal("show");
        }
      });

      element.$wrapperJquery.on('select2:select', (e) => {
        if (typeof e.params.data == 'undefined') {
          return;
        }
        if (e.params.data.isNew) {
          // append the new option element prenamently:
          $(this)
            .find('[value="' + e.params.data.id + '"]')
            .replaceWith(
              '<option selected value="' +
                e.params.data.id +
                '">' +
                e.params.data.text +
                '</option>'
            );
          //modalTag.mainObject.modal("show");
        }
      });
    }
  },
  defineButtonsListeners: (element) => {
    let $buttonAdd = document.querySelector('#' + element.id + '_btn_add');

    if (
      typeof $buttonAdd != 'undefined' &&
      typeof $buttonAdd != null &&
      $buttonAdd != null
    ) {
      element.$wrapperJquery.on('select2:open', (e) => {
        //let $container = $("#" + element.id + "_div");
        let $searchfield = $('#main').find('.select2-search__field');

        if (typeof $searchfield != 'undefined') {
          if ($searchfield.length != 0) {
            let $buttonAddParent = $buttonAdd.parentElement;
            $buttonAddParent.removeChild($buttonAdd);
            $searchfield.after($buttonAdd);

            $(this).off(e);
          }
        }
      });
    }

    let $buttonValidate = document.querySelector(
      '#' + element.id + '_btn_validate'
    );
    if (
      typeof $buttonValidate != 'undefined' &&
      typeof $buttonValidate != null &&
      $buttonValidate != null
    ) {
      element.$wrapperJquery.on('select2:open', (e) => {
        //let $container = $("#" + element.id + "_div");
        let $searchfield = $('#main').find('.select2-search__field');

        if (typeof $searchfield != 'undefined') {
          if ($searchfield.length != 0) {
            let $buttonValidateParent = $buttonValidate.parentElement;
            $buttonValidateParent.removeChild($buttonValidate);
            $searchfield.after($buttonValidate);
            $('#' + element.id + '_btn_validate').on('click', () => {
              element.hide();
              element.$wrapperJquery.trigger('select2:select');
            });
            $(this).off(e);
          }
        }
      });
    }
  },
};
/**
 *
 *  Class SelectElement
 *
 * Represent html selectElement
 *
 * @category Represent html selectElement
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 * @param {object} config
 */
export default class SelectElement extends FormElement {
  constructor() {
    super();
    this.wrapper = document.getElementById(this.id + '_input');
    this.$wrapperJquery = $('#' + this.id + '_input');
  }

  hide() {
    if (this.is('idle')) {
      this.$wrapperJquery.selectize('close');
    }
  }
  /**
   * Initialize component
   *
   * @returns {Promise}
   */
  mount() {
    return super
      .mount()
      .then(() => {
        this.$wrapperJquery.selectize(this.config);
      })
      .then(() => {
        privateMethods.defineSelectingListener(this);
        privateMethods.defineUnSelectingListener(this);
        privateMethods.defineInlineListerner(this);
        //privateMethods.defineButtonsListeners(this);
        let container = this.querySelector('.select2-selection');
        container.setAttribute('id', this.getAttribute('id') + '_container');
        container.classList.add('form-select');
      });
  }

  /**
   * Destroy component
   */
  unmount() {
    super.unmount();
    this.$wrapperJquery.off('select2:open');
    this.$wrapperJquery.off('select2:unselecting');
    this.$wrapperJquery.off('change');
    this.$wrapperJquery.off('select2:selecting');
    return this;
  }

  /**
   * Reload component
   */
  reload() {
    super.reload();
    return this;
  }

  getDefaultConfig() {
    let config = {
      createOnBlur: true,
      showAddOptionOnCreate: true,
      hideSelected: true,
      preload: 'focus',
      selectOnTab: true,
    };
    this.defineAjaxConfig(config);
    //this.defineTemplate(config);
    return config;
  }

  /**
   * Define ajax config if available
   * @param {object} config
   * @returns
   */
  defineAjaxConfig(config) {
    if (typeof this.config.endpoints.readData !== 'undefined') {
      config.load = (query, callback) => {
        return this.app.ajaxd
          .post(this.config.endpoints.readData.endpoint, {
            q: query ?? null,
            p: 1,
          })
          .then((data) => {
            callback(data);
          });
      };
    }
    if (typeof this.config.endpoints.createData !== 'undefined') {
      config.create = (input) => {
        let term = $.trim(input);

        if (term === '') {
          return null;
        }

        return this.app.ajaxd
          .post(this.config.endpoints.createData.endpoint, { tag: term })
          .then((response) => {
            if (
              typeof response !== 'undefined' &&
              typeof response.data != 'undefined' &&
              typeof response.data.id != 'undefined'
            ) {
              return {
                value: response.data.id,
                text: response.data.tag,
                newTag: true,
              };
            }
            return null;
          });
      };
    }
    return config;
  }

  /**
   * Define custom template if available
   * @param {object} config
   */
  defineTemplate(config) {
    if (
      typeof this.config.selectTemplate !== 'undefined' &&
      this.config.selectTemplate != null &&
      typeof templatesHandle[this.config.selectTemplate] != 'undefined'
    ) {
      if (typeof templatesJsField[this.config.selectTemplate] == 'undefined') {
        this.compile = Handlebars.compile(
          templatesHandle[this.config.selectTemplate]
        );
        templatesJsField[this.config.selectTemplate] = Handlebars.compile(
          templatesHandle[this.config.selectTemplate]
        );
      } else {
        this.compile = templatesJsField[this.config.selectTemplate];
      }

      config.templateResult = (repo) => {
        return this.formatRepo(repo);
      };
      config.templateSelection = (repo) => {
        return this.formatRepoSelection(repo);
      };
      config.escapeMarkup = (markup) => {
        return markup;
      };
    }
    return config;
  }

  formatRepo(repo) {
    if (repo.loading) {
      return $("<i class='fa fa-sync fa-spin'></i>");
    }
    let markup = this.compile({
      title: repo.text,
      actionbar: repo.actionbar,
    });

    return $(markup);
  }

  formatRepoSelection(repo) {
    return repo.text;
  }

  setValue(value) {
    console.log(this.$wrapperJquery);
    this.$wrapperJquery.val(value);
    this.$wrapperJquery.trigger('change');
  }

  getValue() {
    return this.$wrapperJquery.select2('data');
  }
}
