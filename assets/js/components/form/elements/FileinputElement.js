import FormElement from './FormElement';
import Swal from 'sweetalert2';
import 'sweetalert2/dist/sweetalert2.css';

//import 'jquery';
import '../../../../../node_modules/bootstrap-fileinput/css/fileinput.css';
import '../../../../../node_modules/bootstrap-fileinput/js/plugins/piexif';
import '../../../../../node_modules/bootstrap-fileinput/js/fileinput';

let localesLoaded = false;
const utils = {
  loadLocales: (locale) => {
    if (!localesLoaded) {
      import(
        `../../../../../node_modules/bootstrap-fileinput/js/locales/${locale}.js`
      ).then(() => {
        localesLoaded = true;
      });
    }
  },
  FIfooterTemplate: (fieldName) => {
    return (
      '<div class="file-thumbnail-footer" style ="height:94px">\n' +
      '   <div style="margin:5px 0">\n' +
      '<span class="kv-input kv-new badge hoverable badge-success ">{caption}</span>' +
      //'       <input disabled class="kv-input kv-new form-control input-sm text-center" value="{caption}" >\n' +
      '       <input type="hidden" name="' +
      fieldName +
      '[]" value="{KEY}">\n' +
      '   </div>\n' +
      '   {size}\n ' +
      '{progress} \n' +
      '{actions}\n' +
      '</div>'
    );
  },
  FImodalTemplate: () => {
    return (
      '<div class="modal fade" role="dialog">\n' +
      '  <div class="modal-dialog">\n' +
      '       <div class="col-lg-12">\n' +
      '          <div class="panel panel-default">\n' +
      '                <div class="modal-content">\n' +
      '                  <div class="modal-header">\n' +
      '                       <div class="kv-zoom-actions pull-right">{toggleheader}{fullscreen}{borderless}{close}</div>\n' +
      '                          <h3 class="modal-title">{heading} <small><span class="kv-zoom-title"></span></small></h3>\n' +
      '                    </div>\n' +
      '                   <div class="modal-body">\n' +
      '                         <div class="floating-buttons"></div>\n' +
      '                         <div class="kv-zoom-body file-zoom-content"></div>\n' +
      '{prev} {next}\n' +
      '                       </div>\n' +
      '                   </div>\n' +
      '               </div>\n' +
      '           </div>\n' +
      '       </div>\n' +
      '   </div>\n' +
      '</div>\n'
    );
  },
  FIzoomTemplate: (endpoint) => {
    return (
      '<a class="btn btn-light-success btn-sm view-attachement" data-router-disabled href="' +
      endpoint +
      '" target="_blank "><i class="fas fa-eye" aria-hidden="true "></i></a>'
    );
  },
  FImain: () => {
    return (
      '{preview}\n' +
      '<div class="kv-upload-progress kv-hidden"></div><div class="clearfix"></div>\n' +
      '<div class="input-group {class}">\n' +
      //'     {caption}\n' +
      '     <div class="input-group-btn input-group-append">\n' +
      '      {remove}\n' +
      '      {cancel}\n' +
      '      {upload}\n' +
      //'      {browse}\n' +
      '    </div>\n' +
      '</div>'
    );
  },
  FImain2: () => {
    return (
      '{preview}\n<div class="kv-upload-progress kv-hidden pt-4"></div>\n<div class="clearfix"></div>\n' +
      '{remove}\n{cancel}\n{upload}\n'
    );
  },
};
/**
 *
 *  Class FileinputElement
 *
 * Represent html fileinputElement
 *
 * @category Represent html fileinputElement
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 * @param {object} config
 */
export default class FileinputElement extends FormElement {
  constructor() {
    super();
    this.wrapper = document.getElementById(this.id + '_input');
    this.$wrapperJquery = $('#' + this.id + '_input');
  }

  /**
   * Initialize component
   *
   * @returns {Promise}
   */
  mount() {
    //utils.loadLocales(this.app.config.lang);
    return super
      .mount()
      .then(() => {
        this.$wrapperJquery.fileinput(this.config);
      })
      .then(() => {
        this.$wrapperJquery.on('filebeforedelete', function (e) {
          return Swal.fire({
            title: 'Voulez-vous supprimer ce fichier ?',
            text: 'Cette action est définitive!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Oui, supprimer!',
          }).then((result) => {
            return !result.isConfirmed;
          });
        });

        this.$wrapperJquery.on('fileuploaded', function (event, data) {
          if (typeof data.response.initialPreview == 'undefined') {
            $(this).fileinput('clear').fileinput('unlock');
          }
        });

        this.$wrapperJquery.on('filebatchselected', function () {
          $(this).fileinput('upload');
        });
      });
  }

  /**
   * Destroy component
   */
  unmount() {
    super.unmount();
    this.$wrapperJquery.off('filebeforedelete');
    this.$wrapperJquery.off('fileuploaded');
    this.$wrapperJquery.off('filebatchselected');
    return this;
  }

  clear() {
    this.$wrapperJquery.fileinput('clear');
    this.$wrapperJquery.fileinput('destroy').fileinput(this.config);
  }

  /**
   * Reload component
   */
  reload() {
    super.reload();
    return this;
  }

  getDefaultConfig() {
    return {
      minFileCount: 1,
      maxFileCount: 10,
      language: this.app.config.lang,
      uploadAsync: true,
      showUpload: false,
      showRemove: false,
      showPreview: true,
      fileActionSettings: {
        removeIcon: '<i class="fas fa-trash"></i>',
        removeClass:
          'btn-kv btn btn-sm btn-outline btn-outline-dashed btn-outline-secondary',
        zoomIcon: '<i class="fas fa-zoom-in"></i>',
        zoomClass:
          'btn-kv btn btn-sm btn-outline btn-outline-dashed btn-outline-secondary',
        uploadIcon: '<i class="fas fa-upload"></i>',
        uploadClass:
          'btn-kv btn btn-sm btn-outline btn-outline-dashed btn-outline-secondary',
        browseClass:
          'btn-kv btn btn-sm btn-outline btn-outline-dashed btn-outline-primary',
        browseIcon: '<i class="fas fa-folder2-open"></i>',
        cancelClass:
          'btn-kv btn btn-sm btn-outline btn-outline-dashed btn-outline-danger',
        cancelIcon: '<i class="fas fa-slash"></i>',
        uploadRetryIcon: '<i class="fas fa-fa-refresh"></i>',
      },
      uploadUrl: this.config.uploadEndpoint ?? '/api/utils/file/upload',
      deleteUrl: this.config.deleteEndpoint ?? '/api/utils/file/delete',
      theme: 'fa',
      overwriteInitial: false,
      browseOnZoneClick: true,
      layoutTemplates: {
        footer: utils.FIfooterTemplate(this.wrapper.getAttribute('data-name')),
        modal: utils.FImodalTemplate(),
        actionZoom: utils.FIzoomTemplate(
          this.config.fetchEndpoint ?? '/api/utils/file/fetch/{KEY}'
        ),
        main1: utils.FImain(),
        main2: utils.FImain2(),
      },
      previewFileIcon: '<i class="fas fa-5x fa-file text-primary"></i>',
      allowedPreviewTypes: null,
      preferIconicPreview: true, // this will force thumbnails to display icons for following file extensions
      previewFileIconSettings: {
        // configure your icon file extensions
        doc: '<i class="fas fa-5x fa-file-word text-primary"></i>',
        xls: '<i class="fas fa-5x fa-file-excel text-success"></i>',
        ppt: '<i class="fas fa-5x fa-file-powerpoint text-danger"></i>',
        pdf: '<i class="fas fa-5x fa-file-pdf text-danger"></i>',
        zip: '<i class="fas fa-5x fa-file-archive text-muted"></i>',
        htm: '<i class="fas fa-5x fa-file-code text-info"></i>',
        txt: '<i class="fas fa-5x fa-file-text text-info"></i>',
        mov: '<i class="fas fa-5x fa-file-movie text-warning"></i>',
        mp3: '<i class="fas fa-5x fa-file-audio text-warning"></i>',
        jpg: '<i class="fas fa-5x fa-file-photo text-danger"></i>',
        gif: '<i class="fas fa-5x fa-file-photo text-muted"></i>',
        png: '<i class="fas fa-5x fa-file-photo text-primary"></i>',
      },
      previewFileExtSettings: {
        doc: function (ext) {
          return ext.match(/(doc|docx|odt)$/i);
        },
        xls: function (ext) {
          return ext.match(/(xls|xlsx|ods)$/i);
        },
        ppt: function (ext) {
          return ext.match(/(ppt|pptx|odp)$/i);
        },
        pdf: function (ext) {
          return ext.match(/(pdf)$/i);
        },
        jpg: function (ext) {
          return ext.match(/(jpg|jpeg)$/i);
        },
        png: function (ext) {
          return ext.match(/(png)$/i);
        },
        gif: function (ext) {
          return ext.match(/(gif)$/i);
        },
        zip: function (ext) {
          return ext.match(/(zip|rar|tar|gzip|gz|7z)$/i);
        },
        htm: function (ext) {
          return ext.match(/(htm|html)$/i);
        },
        txt: function (ext) {
          return ext.match(/(txt|ini|csv|java|php|js|css)$/i);
        },
        mov: function (ext) {
          return ext.match(/(avi|mpg|mkv|mov|mp4|3gp|webm|wmv)$/i);
        },
        mp3: function (ext) {
          return ext.match(/(mp3|wav)$/i);
        },
      },
      previewThumbTags: {
        '{KEY}': '',
      },
    };
  }

  setValue(value) {
    this.config.initialPreviewThumbTags = value.initialPreviewThumbTags;
    this.config.initialPreview = value.initialPreview;
    this.config.initialPreviewConfig = value.initialPreviewConfig;
    this.$wrapperJquery.fileinput('destroy').fileinput(this.config);
  }
}
