import FormElement from './FormElement';
//import 'jquery';

//import { Popover } from 'bootstrap';
const utils = {
  // Turn 3 digit hex color into 6 digit version.
  normalizeColor: (color) => {
    if (!color) {
      return null;
    }
    return color.replace(
      /^#([a-f\d])([a-f\d])([a-f\d])$/i,
      (m, r, g, b) => '#' + r + r + g + g + b + b
    );
  },

  // Check hex color brightness.
  isColorDark: (hex) => {
    if (!hex) {
      return null;
    }

    // Parse color.
    let result = /^#([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(
      utils.normalizeColor(hex)
    );
    if (!result) {
      return null;
    }

    // Manage foreground color based on background brightness
    // https://www.w3.org/TR/AERT/#color-contrast
    result = Math.round(
      (parseInt(result[1], 16) * 299 +
        parseInt(result[2], 16) * 587 +
        parseInt(result[3], 16) * 114) /
        1000
    );
    return result <= 125;
  },

  getColorLabel: ($el, color) => {
    let colors = $el.config.colors || null;

    if (colors && color) {
      for (let line in colors) {
        for (let col in colors[line]) {
          if (col === color) {
            return colors[line][col];
          }
        }
      }
    }

    return null;
  },
  intitalizeListeners: ($element, $contents) => {
    $contents.addEventListener('click', (e) => {
      $element.setValue(utils.normalizeColor(e.target.dataset.color));
      if ($element.popover != null) {
        $element.popover.hide();
      }
    });
  },
};
/**
 *
 *  Class ColorpickerElement
 *
 * Represent html colorpickerElement
 *
 * @category Represent html colorpickerElement
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @contributor https://github.com/bgaze/bootstrap-color-palette
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 *
 *
 * @param {object} config
 */
export default class ColorpickerElement extends FormElement {
  constructor() {
    super();
    this.wrapper = document.getElementById(this.id + '_input');
    this.$wrapperJquery = $('#' + this.id + '_input');
    this.popover = null;
  }

  /**
   * Initialize component
   *
   * @returns {Promise}
   */
  mount() {
    return super.mount().then(() => {
      this.initialize(this.config);
    });
  }

  /**
   * Destroy component
   */
  unmount() {
    super.unmount();
    return this;
  }

  /**
   * Reload component
   */
  reload() {
    super.reload();
    return this;
  }

  initialize(settings) {
    if (!settings.colors.length) {
      return;
    }

    // Override reserved popover configuration.
    settings = $.extend(settings, {
      html: true,
      sanitize: false,
      trigger: 'manual',
      content: function () {
        return settings.body(settings.colors);
      },
    });

    // Init popover.

    this.popover = new bootstrap.Popover(this, settings);

    this.addEventListener('shown.bs.popover', () => {
      let contents = document.getElementById(this.id + '_content');
      utils.intitalizeListeners(this, contents);
      let color = this.getValue();
      contents.querySelectorAll('[data-color]').forEach((item) => {
        item.classList.remove('bcp-active');
      });
      if (color && color !== '') {
        contents
          .querySelector(`[data-color="${color}"]`)
          .classList.add('bcp-active');
      }
    });

    // Open popover on element click.
    this.addEventListener('click', () => {
      this.popover.toggle();
    });

    // Init default color.
    this.setValue(utils.normalizeColor(settings.color));

    // Keep chaining.
    return this;
  }

  getDefaultConfig() {
    return {
      // Colors list.
      colors: [
        {
          '#000000': 'Black',
          '#434343': 'Dark grey 4',
          '#666666': 'Dark grey 3',
          '#999999': 'Dark grey 2',
          '#b7b7b7': 'Dark grey 1',
          '#cccccc': 'grey',
          '#d9d9d9': 'Light grey 1',
          '#efefef': 'Light grey 2',
          '#f3f3f3': 'Light grey 3',
          '#ffffff': 'White',
        },
        {
          '#980000': 'Red fruits',
          '#ff0000': 'Red',
          '#ff9900': 'Orange',
          '#ffff00': 'Yellow',
          '#00ff00': 'Green',
          '#00ffff': 'Cyan',
          '#4a86e8': 'Blueberry',
          '#0000ff': 'Blue',
          '#9900ff': 'Purple',
          '#ff00ff': 'Magenta',
        },
        {
          '#e6b8af': 'Light red fruits 3',
          '#f4cccc': 'Light red 3',
          '#fce5cd': 'Light orange 3',
          '#fff2cc': 'Light yellow 3',
          '#d9ead3': 'Light green 3',
          '#d0e0e3': 'Light cyan 3',
          '#c9daf8': 'Light blueberry 3',
          '#cfe2f3': 'Light blue 3',
          '#d9d2e9': 'Light purple 3',
          '#ead1dc': 'Light magenta 3',
        },
        {
          '#dd7e6b': 'Light red fruits 2',
          '#ea9999': 'Light red 2',
          '#f9cb9c': 'Light orange 2',
          '#ffe599': 'Light yellow 2',
          '#b6d7a8': 'Light green 2',
          '#a2c4c9': 'Light cyan 2',
          '#a4c2f4': 'Light blueberry 2',
          '#9fc5e8': 'Light blue 2',
          '#b4a7d6': 'Light purple 2',
          '#d5a6bd': 'Light magenta 2',
        },
        {
          '#cc4125': 'Light red fruits 1',
          '#e06666': 'Light red 1',
          '#f6b26b': 'Light orange 1',
          '#ffd966': 'Light yellow 1',
          '#93c47d': 'Light green 1',
          '#76a5af': 'Light cyan 1',
          '#6d9eeb': 'Light blueberry 1',
          '#6fa8dc': 'Light blue 1',
          '#8e7cc3': 'Light purple 1',
          '#c27ba0': 'Light magenta 1',
        },
        {
          '#a61c00': 'Dark red fruits 1',
          '#cc0000': 'Dark red 1',
          '#e69138': 'Dark orange 1',
          '#f1c232': 'Dark yellow 1',
          '#6aa84f': 'Dark green 1',
          '#45818e': 'Dark cyan 1',
          '#3c78d8': 'Dark blueberry 1',
          '#3d85c6': 'Dark blue 1',
          '#674ea7': 'Dark purple 1',
          '#a64d79': 'Dark magenta 1',
        },
        {
          '#85200c': 'Dark red fruits 2',
          '#990000': 'Dark red 2',
          '#b45f06': 'Dark orange 2',
          '#bf9000': 'Dark yellow 2',
          '#38761d': 'Dark green 2',
          '#134f5c': 'Dark cyan 2',
          '#1155cc': 'Dark blueberry 2',
          '#0b5394': 'Dark blue 2',
          '#351c75': 'Dark purple 2',
          '#741b47': 'Dark magenta 2',
        },
        {
          '#5b0f00': 'Dark red fruits 3',
          '#660000': 'Dark red 3',
          '#783f04': 'Dark orange 3',
          '#7f6000': 'Dark yellow 3',
          '#274e13': 'Dark green 3',
          '#0c343d': 'Dark cyan 3',
          '#1c4587': 'Dark blueberry 3',
          '#073763': 'Dark blue 3',
          '#20124d': 'Dark purple 3',
          '#4c1130': 'Dark magenta 3',
        },
      ],
      // Template.
      template:
        '<div class="popover bcp-popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>',
      // Build popover content.
      body: (colors) => {
        let content = colors
          .map((row) => {
            let tmp = '';

            for (let color in row) {
              let cl = utils.isColorDark(color) ? 'bcp-dark' : 'bcp-light';
              tmp += `<div class="bcp-color ${cl}" style="background-color: ${color};" data-color="${color}" title="${row[color]}"></div>`;
            }

            return `<div class="bcp-row">${tmp}</div>`;
          })
          .join('');

        return `<div class="bcp-content" id="${this.id}_content">${content}</div>`;
      },
    };
  }

  getValue() {
    return this.wrapper.value;
  }
  setValue(value) {
    this.wrapper.style.color = utils.isColorDark(value) ? '#FFFFFF' : '#000000';
    this.wrapper.style.backgroundColor = value;
    this.wrapper.value = value;
  }
}
