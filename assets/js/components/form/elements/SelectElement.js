import FormElement from './FormElement';

import TomSelect from '../../../../../node_modules/tom-select/dist/esm/tom-select.complete';
import '../../../../../node_modules/tom-select/dist/css/tom-select.css';
import '../../../../../node_modules/tom-select/dist/css/tom-select.bootstrap5.css';
import Handlebars from 'handlebars/dist/handlebars.js';

let templatesJsField = {};
let templatesHandle = {
  'base.handlebars': `
        <div class='row'>
        {{#if title}}
          <div class='col'>
            {{{title}}}
          </div>
        {{/if}}
        {{#if actionbar}}
          <div class='col'>
            {{{actionbar}}}
          </div>
        {{/if}}
      </div>`,
};
/**
 *
 *  Class SelectElement
 *
 * Represent html selectElement
 *
 * @category Represent html selectElement
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 * @param {object} config
 */
export default class SelectElement extends FormElement {
  constructor() {
    super();
    this.wrapper = document.getElementById(this.id + '_input');
    this.$wrapperJquery = $('#' + this.id + '_input');
  }

  hide() {
    if (this.is('idle')) {
      //this.$wrapperJquery.selectize('close');
    }
  }
  /**
   * Initialize component
   *
   * @returns {Promise}
   */
  mount() {
    return super.mount().then(() => {
      this.wrapper = new TomSelect('#' + this.id + '_input', this.config);
    });
  }

  /**
   * Destroy component
   */
  unmount() {
    super.unmount();
    return this;
  }

  /**
   * Reload component
   */
  reload() {
    super.reload();
    return this;
  }

  getDefaultConfig() {
    let config = {
      createOnBlur: true,
      showAddOptionOnCreate: true,
      hideSelected: true,
      preload: 'focus',
      selectOnTab: true,
    };
    this.defineAjaxConfig(config);
    //this.defineTemplate(config);
    return config;
  }

  /**
   * Define ajax config if available
   * @param {object} config
   * @returns
   */
  defineAjaxConfig(config) {
    if (typeof this.config.endpoints.readData !== 'undefined') {
      let datasource = this.app.datad.getDatasource(
        this.config.endpoints.readData
      );

      config.load = (query, callback) => {
        return this.app.ajaxd
          .call(datasource, {
            q: query ?? null,
            p: 1,
          })
          .then((response) => {
            if (typeof response !== 'undefined') {
              this.app.stored.set(
                datasource.ressource,
                response.content,
                this,
                (e) => {
                  this.wrapper.load((callback) => {
                    callback(response.content);
                  });
                }
              );
              callback(response.content);
            } else {
              callback({});
            }
          });
      };
    }
    if (typeof this.config.endpoints.createData !== 'undefined') {
      config.create = (input, callback) => {
        let term = /*$.trim(input); */ input.toString().trim();

        if (term === '') {
          return null;
        }

        return this.app.ajaxd
          .call(
            this.app.datad.getDatasource(this.config.endpoints.createData),
            { tag: term }
          )
          .then((response) => {
            if (
              typeof response !== 'undefined' &&
              typeof response.data != 'undefined' &&
              typeof response.data.id != 'undefined'
            ) {
              callback({
                value: response.data.id,
                text: response.data.tag,
              });
            }
          });
      };
    }
    return config;
  }

  /**
   * Define custom template if available
   * @param {object} config
   */
  defineTemplate(config) {
    if (
      typeof this.config.selectTemplate !== 'undefined' &&
      this.config.selectTemplate != null &&
      typeof templatesHandle[this.config.selectTemplate] != 'undefined'
    ) {
      if (typeof templatesJsField[this.config.selectTemplate] == 'undefined') {
        this.compile = Handlebars.compile(
          templatesHandle[this.config.selectTemplate]
        );
        templatesJsField[this.config.selectTemplate] = Handlebars.compile(
          templatesHandle[this.config.selectTemplate]
        );
      } else {
        this.compile = templatesJsField[this.config.selectTemplate];
      }

      config.templateResult = (repo) => {
        return this.formatRepo(repo);
      };
      config.templateSelection = (repo) => {
        return this.formatRepoSelection(repo);
      };
      config.escapeMarkup = (markup) => {
        return markup;
      };
    }
    return config;
  }

  formatRepo(repo) {
    if (repo.loading) {
      return $("<i class='fa fa-sync fa-spin'></i>");
    }
    let markup = this.compile({
      title: repo.text,
      actionbar: repo.actionbar,
    });

    return $(markup);
  }

  formatRepoSelection(repo) {
    return repo.text;
  }

  setValue(value) {
    this.wrapper.setValue(value);
  }

  getValue() {
    return this.wrapper.getValue();
  }
}
