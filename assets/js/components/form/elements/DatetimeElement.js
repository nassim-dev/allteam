import FormElement from './FormElement';

/**
 *
 *  Class DatetimeElement
 *
 * Represent html datetimeElement
 *
 * @category Represent html datetimeElement
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 * @param {object} config
 */
export default class DatetimeElement extends FormElement {
  constructor() {
    super();
  }

  /**
   * Initialize component
   *
   * @returns {Promise}
   */
  mount() {
    return super.mount().then(() => {
      this.config.onChange = (selectedDates, dateStr, instance) => {
        const event = new CustomEvent('change', {
          composed: true,
          bubbles: true,
          cancelable: true,
          detail: { selectedDates },
        });
        this.dispatchEvent(event);
      };
      this.wrapper = flatpickr(
        document.getElementById(this.id + '_input'),
        this.config
      );
    });
  }

  /**
   * Destroy component
   */
  unmount() {
    super.unmount();
    return this;
  }

  /**
   * Reload component
   */
  reload() {
    super.reload();
    return this;
  }

  /**
   * Get default config
   *
   * @returns {object};
   */
  getDefaultConfig() {
    return {
      allowInput: true,
      enableTime: true,
      dateFormat: 'd/m/Y H:i',
      time_24hr: true,
      closeOnSelect: true,
      disableMobile: true,
    };
  }
  getValue() {
    return this.wrapper.selectedDates;
  }
  setValue(value, format = null) {
    this.wrapper.setDate(value, false, format ?? this.config.dateFormat);
  }
}
