import HtmlInputElement from '../../HtmlInputElement';
import Inputmask from '../../../../../node_modules/inputmask/lib/inputmask.js';

let commandLoaded = false;
/**
 *
 *  Class FormElement
 *
 * Represent html formElement
 *
 * @category Represent html formElement
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 * @param {object} config
 */
export default class FormElement extends HtmlInputElement {
  constructor() {
    super();

    /**
     * Real Dom events to bind to this object
     * @property {array} bindedEvents
     * @protected
     */
    this.bindedEvents = this.config.bindedEvents
      ? [...this.config.bindedEvents, 'submit']
      : ['submit'];
    if (!commandLoaded) {
      commandLoaded = true;
      this.command
        .addCommand('DisableCommand', async () => {
          return import(
            '../../../command/components/form/elements/DisableCommand'
          );
        })
        .addCommand('EnableCommand', async () => {
          return import(
            '../../../command/components/form/elements/EnableCommand'
          );
        })
        .addCommand('FillFieldCommand', async () => {
          return import(
            '../../../command/components/form/elements/FillFieldCommand'
          );
        })
        .addCommand('SelectFilterCommand', async () => {
          return import(
            '../../../command/components/form/elements/SelectFilterCommand'
          );
        })
        .addCommand('WatchChangeCommand', async () => {
          return import(
            '../../../command/components/form/elements/WatchChangeCommand'
          );
        });
    }

    this.wrapper = document.getElementById(this.id + '_input');
  }

  /**
   * Initialize component
   *
   * @returns {Promise}
   */
  mount() {
    return super
      .mount()
      .then(() => {
        Inputmask().mask(this.querySelectorAll('input'));
      })
      .then(() => {
        if (this.wrapper != null && this.datas('save')) {
          this.wrapper.addEventListener('change', () => {
            //let parent = this.app.componentd.get(this.config.parent);
            //
            //if (parent != null) {
            //parent.dispatchEvent(new SubmitEvent('submit'));
            //}
          });
        }
      });
  }

  /**
   * Destroy component
   */
  unmount() {
    super.unmount();
    //return this;
  }

  /**
   * Reload component
   */
  reload() {
    super.reload();
    //return this;
  }

  getValue() {
    return this.wrapper.value;
  }
  setValue(value) {
    this.wrapper.value = value;
  }
}
