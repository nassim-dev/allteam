//import 'jquery';
let utils = {
  /**
   * A Javascript module to loadeding/refreshing options of a select2 list box using ajax based on selection of another select2 list box.
   *
   * @url : https://gist.github.com/ajaxray/187e7c9a00666a7ffff52a8a69b8bf31
   * @author : Anis Uddin Ahmad <anis.programmer@gmail.com>
   *
   * Live demo - https://codepen.io/ajaxray/full/oBPbQe/
   * w: http://ajaxray.com | t: @ajaxray
   */
  Select2Cascade: (parent, child, url, select2Options) => {
    let afterActions = [];

    // Register functions to be called after cascading data loading done
    this.then = function (callback) {
      afterActions.push(callback);
      return this;
    };

    parent.on('change', (e) => {
      var key = $(this).val();

      if (Array.isArray(key)) {
        key = key.toString();
      }

      child.mainObject.empty();

      $.getJSON(url.replace(':parentId:', key), function (response) {
        var newOptions = [];

        $.each(response.items, function () {
          newOptions.push(new Option(this.text, this.id));
        });
        child.mainObject.append(newOptions).trigger('change');

        afterActions.forEach(function (callback) {
          callback(parent, child.mainObject, response.items);
        });
      });
    });
  },
};

export default utils;
