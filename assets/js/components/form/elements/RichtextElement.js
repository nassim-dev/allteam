import FormElement from './FormElement';
import '../../../../../node_modules/quill/dist/quill.snow.css';
import Quill from 'quill';

/**
 *
 *  Class RichtextElement
 *
 * Represent html richtextElement
 *
 * @category Represent html richtextElement
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 * @param {object} config
 */

export default class RichtextElement extends FormElement {
  constructor() {
    super();
  }

  /**
   * Initialize component
   *
   * @returns {Promise}
   */
  mount() {
    return super.mount().then(() => {
      this.wrapper = new Quill('#' + this.id + '_wrapper', this.config);
    });
  }

  /**
   * Destroy component
   */
  unmount() {
    super.unmount();
    return this;
  }

  /**
   * Reload component
   */
  reload() {
    super.reload();
    return this;
  }

  getDefaultConfig() {
    return {
      theme: 'snow',
      modules: {
        history: {
          delay: 2000,
          maxStack: 500,
          userOnly: true,
        },
        toolbar: [
          ['bold', 'italic', 'underline', 'strike'], // toggled buttons
          ['blockquote'],

          [{ header: 1 }, { header: 2 }], // custom button values
          [{ list: 'ordered' }, { list: 'bullet' }],
          [{ script: 'sub' }, { script: 'super' }], // superscript/subscript
          [{ indent: '-1' }, { indent: '+1' }], // outdent/indent
          [{ header: [1, 2, 3, 4, 5, 6, false] }],

          [{ color: [] }, { background: [] }], // dropdown with defaults from theme
          [{ font: [] }],
          [{ align: [] }],

          ['clean'], // remove formatting button
        ],
      },
      placeholder: 'Type your text here...',
    };
  }
}
