import FormElement from './FormElement';
import L from 'leaflet';
import 'leaflet/dist/leaflet.css';
import { OpenStreetMapProvider } from 'leaflet-geosearch';

const provider = new OpenStreetMapProvider();
let timer,
  timeoutVal = 500;
const privateMethods = {
  reverseSearch: async (latLng) => {
    //let url = `https://photon.komoot.io/reverse?lat=${latLng.lat}&lon=${latLng.lng}`;
    let url = `https://api-adresse.data.gouv.fr/reverse/?lat=${latLng.lat}&lon=${latLng.lng}`;

    return fetch(url)
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
        return null;
      })
      .then((response) => {
        if (response === null) {
          return '';
        }
        let properties = response.features[0].properties;

        let address = properties.housenumber ?? '';
        address += properties.street ?? properties.name ?? '';
        address += ' ' + properties.postcode ?? '';
        address += ' ' + properties.city ?? '';
        address += ' ' + properties.country ?? '';

        return address;
      });
  },
  searchTemplate: (element) => {
    let child = document.createElement('div');
    child.innerHTML = `<div class="fw-bold" >
    <span class="fs-6 text-gray-800 me-2" data-x="${element.x}" data-y="${element.y}", data-label="${element.label}">${element.label}</span>
</div>`;
    child.classList.add(
      'd-flex',
      'align-items-center',
      'p-3',
      'rounded-3',
      'border-hover',
      'border',
      'border-dashed',
      'border-gray-300',
      'cursor-pointer',
      'mb-1'
    );

    return child;
  },
  updateView: (instance) => {
    if (instance.latLng != null) {
      instance.wrapper.flyTo(instance.latLng, 15);
      if (instance.marker != null) {
        instance.marker.remove();
      }
      instance.marker = L.marker(instance.latLng).addTo(instance.wrapper);
    }
  },
  initClickMap: (instance) => {
    instance.wrapper.on('click', async (e) => {
      instance.latLng = L.latLng(e.latlng.lat, e.latlng.lng);
      instance.searchInput.value = await privateMethods.reverseSearch(
        instance.latLng
      );
      instance.searchContainer.style.display = 'none';
      instance.reload();
    });
  },
  initSearchField: (instance) => {
    instance.searchInput.addEventListener('keypress', () => {
      window.clearTimeout(timer);
    });
    instance.searchInput.addEventListener('keyup', async (event) => {
      window.clearTimeout(timer); // prevent errant multiple timeouts from being generated
      timer = window.setTimeout(async () => {
        event.preventDefault();

        const results = await provider.search({
          query: instance.searchInput.value,
        });

        instance.searchContainer.innerHTML = '';

        let container = document.createElement('div');
        container.classList.add('mh-300px', 'scroll-y', 'me-n5', 'pe-5');

        let child;
        for (var i in results) {
          child = privateMethods.searchTemplate(results[i]);
          child.addEventListener('click', (e) => {
            instance.latLng = L.latLng(e.target.dataset.y, e.target.dataset.x);
            instance.searchContainer.style.display = 'none';
            instance.reload();
            instance.searchInput.value = e.target.dataset.label;
          });
          container.appendChild(child);
        }

        instance.searchContainer.appendChild(container);
        instance.searchContainer.style.display = 'block';
      }, timeoutVal);
    });
  },
};

/**
 *
 *  Class AddressElement
 *
 * Represent html addressElement
 *
 * @category Represent html addressElement
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 * @param {object} config
 */
export default class AddressElement extends FormElement {
  constructor() {
    super();

    this.inputLatlng = document.getElementById(this.id + '-latlng');
    this.searchInput = document.getElementById(this.id + '_input');
    this.searchContainer = document.getElementById(this.id + '-search');
    this.marker = null;
    this.latLng = {
      lat: 0,
      lng: 0,
    };
  }

  /**
   * Initialize component
   *
   * @returns {Promise}
   */
  mount() {
    return super.mount().then(() => {
      this.wrapper = L.map(this.id + '-map', {
        scrollWheelZoom: true,
      }).setView([46.298076, 2.011321], 13);
      L.control.scale().addTo(this.wrapper);
      L.tileLayer('//{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
        minZoom: 4,
        maxZoom: 20,
        attribution:
          '&copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
      }).addTo(this.wrapper);

      privateMethods.initSearchField(this);
      privateMethods.initClickMap(this);

      if (this.config.value) {
        this.latLng = L.latLng(this.config.value.y, this.config.value.x);
        privateMethods.updateView(this);
      }
    });
  }

  /**
   * Destroy component
   */
  unmount() {
    super.unmount();
    this.wrapper.remove();
    return this;
  }

  /**
   * Reload component
   */
  reload() {
    super.reload();
    privateMethods.updateView(this);
    this.inputLatlng.value = this.latLng.toString();
    return this;
  }

  getValue() {
    return {
      x: this.latLng.lat,
      y: this.latLng.lng,
      label: this.searchInput.value,
    };
  }
  setValue(value, format = null) {
    this.latLng = L.latLng(value.y, value.x);
    privateMethods.updateView(this);
    this.searchInput = value.label;
  }
}
