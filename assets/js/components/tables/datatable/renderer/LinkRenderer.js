/**
 *
 *  Class LinkRenderer
 *
 * Represent html linkRendLinkRenderer
 *
 * @category Represent html linkRendLinkRenderer
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */
export default class LinkRenderer {
  constructor(props) {
    this.el = document.createElement('a');
    this.el.classList.add('btn', 'btn-sm', 'btn-light-primary');
    this.el.innerHTML = String(props.value);
    this.render(props);
  }

  getElement() {
    return this.el;
  }

  render(props) {
    // formattedValue should be set to textContent
    this.el.textContent = String(props.formattedValue);
  }
}
