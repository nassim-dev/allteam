/**
 *
 *  Class LabelRenderer
 *
 * Represent html linkRendLabelRenderer
 *
 * @category Represent html linkRendLabelRenderer
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */
export default class LabelRenderer {
  constructor(props) {
    this.el = document.createElement('span');
    this.el.classList.add('badge', props.value.color ?? 'badge-primary');
    this.el.innerHTML = String(props.value.value);
    this.render(props);
  }

  getElement() {
    return this.el;
  }

  render(props) {
    // formattedValue should be set to textContent
    this.el.textContent = String(props.value.value);
  }
}
