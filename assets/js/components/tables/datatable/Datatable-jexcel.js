import HtmlElement from '../../HtmlElement';
import jspreadsheet from 'jspreadsheet-ce';
import 'jsuites/dist/jsuites.css';
import 'jspreadsheet-ce/dist/jspreadsheet.css';
//import 'jspreadsheet-ce/dist/jspreadsheet.datatables.css';
//import 'jspreadsheet-ce/dist/jspreadsheet.theme.css';

//import 'jquery';
import { KTBlockUI } from '../../../vendor/components/blockui';
import jsuites from 'jsuites';

function decorate(fn) {
  return function wrapper(...args) {
    return fn(window.app.ajaxd.ajaxDecorator(args[0]));
  };
}

jsuites.ajax.send = decorate(jsuites.ajax.send);
const editors = {};

const privateMethods = {
  getRowConfig: (instance, config) => {
    if (typeof instance.config.endpoints.modifyData !== 'undefined') {
      config.persistance = instance.config.endpoints.modifyData.endpoint;
      return;
    }

    if (typeof instance.config.endpoints.readData !== 'undefined') {
      let datasource = instance.app.datad.getDatasource(
        instance.config.endpoints.readData
      );
      config.url = (() => {
        instance.app.datad.stringWatch(datasource, () => {
          instance.wrapper.setData(e.detail.data);
          instance.wrapper.updateTable();
        });
      })();
      //instance.config.endpoints.readData.endpoint;
      return;
    }

    privateMethods.getRows(instance, config);
  },
  getContextMenu: (obj, x, y, e) => {
    var items = [];
    if (typeof obj == 'undefined') {
      return items;
    }
    if (y == null) {
      // Insert a new column
      if (obj.options.allowInsertColumn == true) {
        items.push({
          title: obj.options.text.insertANewColumnBefore,
          onclick: function () {
            obj.insertColumn(1, parseInt(x), 1);
          },
        });
      }

      if (obj.options.allowInsertColumn == true) {
        items.push({
          title: obj.options.text.insertANewColumnAfter,
          onclick: function () {
            obj.insertColumn(1, parseInt(x), 0);
          },
        });
      }

      // Delete a column
      if (obj.options.allowDeleteColumn == true) {
        items.push({
          title: obj.options.text.deleteSelectedColumns,
          onclick: function () {
            obj.deleteColumn(
              obj.getSelectedColumns().length ? undefined : parseInt(x)
            );
          },
        });
      }

      // Rename column
      if (obj.options.allowRenameColumn == true) {
        items.push({
          title: obj.options.text.renameThisColumn,
          onclick: function () {
            obj.setHeader(x);
          },
        });
      }

      // Sorting
      if (obj.options.columnSorting == true) {
        // Line
        items.push({ type: 'line' });

        items.push({
          title: obj.options.text.orderAscending,
          onclick: function () {
            obj.orderBy(x, 0);
          },
        });
        items.push({
          title: obj.options.text.orderDescending,
          onclick: function () {
            obj.orderBy(x, 1);
          },
        });
      }
    } else {
      // Insert new row
      if (obj.options.allowInsertRow == true) {
        items.push({
          title: obj.options.text.insertANewRowBefore,
          onclick: function () {
            obj.insertRow(1, parseInt(y), 1);
          },
        });

        items.push({
          title: obj.options.text.insertANewRowAfter,
          onclick: function () {
            obj.insertRow(1, parseInt(y));
          },
        });
      }

      items.push({
        title: 'Edit all line',
        onclick: function () {},
      });

      items.push({
        title: 'Duplicate all line',
        onclick: function () {},
      });

      if (obj.options.allowDeleteRow == true) {
        items.push({
          title: obj.options.text.deleteSelectedRows,
          onclick: function () {
            obj.deleteRow(
              obj.getSelectedRows().length ? undefined : parseInt(y)
            );
          },
        });
      }

      if (x) {
        if (obj.options.allowComments == true) {
          items.push({ type: 'line' });

          var title = obj.records[y][x].getAttribute('title') || '';

          items.push({
            title: title
              ? obj.options.text.editComments
              : obj.options.text.addComments,
            onclick: function () {
              var comment = prompt(obj.options.text.comments, title);
              if (comment) {
                obj.setComments([x, y], comment);
              }
            },
          });

          if (title) {
            items.push({
              title: obj.options.text.clearComments,
              onclick: function () {
                obj.setComments([x, y], '');
              },
            });
          }
        }
      }
    }

    // Line
    items.push({ type: 'line' });

    // Copy
    items.push({
      title: obj.options.text.copy,
      shortcut: 'Ctrl + C',
      onclick: function () {
        obj.copy(true);
      },
    });

    // Paste
    if (navigator && navigator.clipboard) {
      items.push({
        title: obj.options.text.paste,
        shortcut: 'Ctrl + V',
        onclick: function () {
          if (obj.selectedCell) {
            navigator.clipboard.readText().then(function (text) {
              if (text) {
                jspreadsheet.current.paste(
                  obj.selectedCell[0],
                  obj.selectedCell[1],
                  text
                );
              }
            });
          }
        },
      });
    }

    // Save
    if (obj.options.allowExport) {
      items.push({
        title: obj.options.text.saveAs,
        shortcut: 'Ctrl + S',
        onclick: function () {
          obj.download();
        },
      });
    }

    return items;
  },

  getRows: (instance, config) => {
    if (typeof instance.config.data != undefined) {
      config.data = instance.config.data;
      return;
    }

    if (instance.childNodes.length > 0) {
      //todo implements if rows are defined in html
    }
  },

  populateWithLocalStorage: (instance) => {
    instance.wrapper.setData(localStorage.getItem(instance.id));
  },

  initializeCalendarSync: (instance, calendarId, field) => {
    instance.wrapper.on('select', function (e, instance, type, indexes) {
      if (type === 'row') {
        let value = instance.wrapper.rows(indexes).data().pluck(field);

        if (typeof value != 'undefined') {
          let calendar = instance.app.componentd.get(calendarId);
          if (typeof calendar != null) {
            //calendar.goToDate(moment(value[0], 'DD/MM/YYYY'));
          }
        }
      }
    });
  },
  updatewWidth(instance) {
    let clientWidth = instance.$wrapper.clientWidth - 50;
    let rows = instance.config.columns.length;
    rows = rows === 0 ? 1 : rows;
    let columnWidth = clientWidth / rows;
    for (let i in instance.config.columns) {
      instance.wrapper.setWidth(i, columnWidth);
    }
  },
};

let commandLoaded = false;
/**
 *
 *  Class Datatable
 *
 * Represent html datatable
 *
 * @category Represent html datatable
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 * @param {object} config
 */
export default class Datatable extends HtmlElement {
  constructor() {
    super();

    /**
     * Progress Bar element
     * @property {HTMLElement} progressBar
     */
    this.progressBar = document.getElementById(`spinner-${this.id}`);

    /**
     * Component title
     */
    this.tableTitle = document.getElementById(this.id + '_title');

    /**
     * Datatable object
     * @property {Grid} wrapper
     */
    this.wrapper = null;

    /**
     * Editor instance
     * @property {Editor} editor
     */
    this.editor = null;

    this.blockUi = new KTBlockUI(this.parentElement);
    if (!this.blockUi.isBlocked()) {
      this.blockUi.block();
    }
    if (!commandLoaded) {
      commandLoaded = true;
      this.command
        .addCommand('DisableCommand', async () => {
          return import(
            '../../../command/components/tables/datatable/AddFilterCommand'
          );
        })
        .addCommand('EnableCommand', async () => {
          return import(
            '../../../command/components/tables/datatable/AddRowCommand'
          );
        })
        .addCommand('FillFieldCommand', async () => {
          return import(
            '../../../command/components/tables/datatable/DeleteRowCommand'
          );
        })
        .addCommand('SelectFilterCommand', async () => {
          return import(
            '../../../command/components/tables/datatable/DetachCommand'
          );
        })
        .addCommand('SelectFilterCommand', async () => {
          return import(
            '../../../command/components/tables/datatable/RenderCommand'
          );
        })
        .addCommand('SelectFilterCommand', async () => {
          return import(
            '../../../command/components/tables/datatable/ReorderRowCommand'
          );
        })
        .addCommand('SelectFilterCommand', async () => {
          return import(
            '../../../command/components/tables/datatable/UpdateRowCommand'
          );
        });
    }
  }

  /**
   * Initialize component
   *
   * @returns  {Promise}
   */
  mount() {
    return super
      .mount()
      .then(() => {
        if (this.config.localStorage) {
          this.config.data = privateMethods.populateWithLocalStorage(this);
        }
      })
      .then(() => {
        this.$wrapper = document.getElementById(this.id + '_wrapper');
        this.wrapper = jspreadsheet(this.$wrapper, this.config);
      })
      .then(() => {
        //privateMethods.updatewWidth(this);
        this.resizeEvent = window.addEventListener('resize', () => {
          privateMethods.updatewWidth(this);
        });

        if (typeof this.config.listeners != 'undefined') {
          this.config.listeners.forEach((listener) => {
            this.addListener(listener);
          });
        }

        //this.app.datad.attach(
        //  'POST:' + this.config.endpoints.modifyData.endpoint,
        //  (e) => {
        //    this.wrapper.setData(e.detail.data);
        //    this.wrapper.updateTable();
        //  }
        //);
      })
      .then(() => {
        if (this.blockUi.isBlocked()) {
          this.blockUi.release();
        }
      });
  }

  idle() {
    super.idle();
    //privateMethods.updatewWidth(this);
  }

  /**
   * Get default config
   *
   * @returns {object};
   */
  getDefaultConfig() {
    let modifyData = this.app.datad.getDatasource(
      this.config.endpoints.modifyData
    );
    let readData = this.app.datad.getDatasource(this.config.endpoints.readData);
    let config = {
      toolbar: [
        {
          type: 'i',
          content: 'undo',
          icon: 'fa-undo',
          onclick: () => {
            this.wrapper.undo();
          },
        },
        {
          type: 'i',
          content: 'redo',
          icon: 'fa-redo',
          onclick: () => {
            this.wrapper.redo();
          },
        },
        {
          type: 'i',
          content: 'save',
          icon: 'fa-download',
          onclick: () => {
            this.wrapper.download();
          },
        },
      ],
      copyCompatibility: true,
      columns: this.config.columns,
      allowExport: true,
      includeHeadersOnDownload: true,
      columnSorting: true,
      columnDrag: true,
      columnResize: true,
      rowResize: true,
      rowDrag: true,
      editable: true,
      allowInsertRow: true,
      allowManualInsertRow: true,
      allowInsertColumn: false,
      allowManualInsertColumn: false,
      allowDeleteRow: true,
      allowRenameColumn: false,
      allowDeleteColumn: false,
      allowComments: false,
      selectionCopy: false,
      pagination: 50,
      search: true,
      filters: true,
      tableHeight: '100%',
      tableWidth: '100%',
      onafterchanges: () => {
        //console.log(this.wrapper.prepareJson(this.wrapper.getData()));
        this.app.ajaxd.call(
          modifyData,
          this.wrapper.prepareJson(this.wrapper.getData())
        );
      },
      // defaultColWidth: '100',
      minSpareRows: 2,
      //lazyLoading: true,
      url: readData.endpoint,
      method: readData.method,
      persistence: modifyData.endpoint,
      data: [],
      loadingSpin: true,
      tableOverflow: true,
      contextMenu: privateMethods.getContextMenu(),
    };
    //privateMethods.getRowConfig(this, config);
    return config;
  }

  /**
   * Destroy component
   */
  unmount() {
    super.unmount();
    if (typeof this.resizeEvent != 'undefined') {
      window.removeEventListener('resize', this.resizeEvent);
    }
    if (typeof this.wrapper != null) {
      this.wrapper.destroy();
    }
  }

  /**
   * Reload component
   */
  reload() {
    super.reload();

    if (typeof this.wrapper != null) {
      new Promise((resolve) => {
        this.blockUi.block();
        this.app.datad.handlerWatch(
          'POST:' + this.config.endpoints.readData.endpoint,
          (e) => {
            this.wrapper.setData(e.detail.data);
            this.wrapper.updateTable();
          }
        );
        /*this.app.ajaxd
          .post(this.config.endpoints.readData.endpoint)
          .then((response) => {
            if (typeof response != 'undefined') {
              this.wrapper.setData(response.data);
              this.wrapper.updateTable();
            }
          });*/

        resolve(true);
      }).then(() => {
        this.blockUi.release();
      });
    }
  }
}
