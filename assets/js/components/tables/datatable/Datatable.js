import HtmlElement from '../../HtmlElement';
import { KTBlockUI } from '../../../vendor/components/blockui';
import { createApp } from 'vue';
import VueExcelEditor from 'vue3-excel-editor';

const editors = {};

const privateMethods = {
  getRowConfig: (instance, config) => {
    if (typeof instance.config.endpoints.modifyData !== 'undefined') {
      config.persistance = instance.config.endpoints.modifyData.endpoint;
      return;
    }

    if (typeof instance.config.endpoints.readData !== 'undefined') {
      let datasource = instance.app.datad.getDatasource(
        instance.config.endpoints.readData
      );
      config.url = (() => {
        instance.app.datad.stringWatch(datasource, () => {
          instance.wrapper.setData(e.detail.data);
          instance.wrapper.updateTable();
        });
      })();
      //instance.config.endpoints.readData.endpoint;
      return;
    }

    privateMethods.getRows(instance, config);
  },

  getRows: (instance, config) => {
    if (typeof instance.config.data != undefined) {
      config.data = instance.config.data;
      return;
    }

    if (instance.childNodes.length > 0) {
      //todo implements if rows are defined in html
    }
  },

  populateWithLocalStorage: (instance) => {
    instance.wrapper.setData(localStorage.getItem(instance.id));
  },

  initializeCalendarSync: (instance, calendarId, field) => {
    instance.wrapper.on('select', function (e, instance, type, indexes) {
      if (type === 'row') {
        let value = instance.wrapper.rows(indexes).data().pluck(field);

        if (typeof value != 'undefined') {
          let calendar = instance.app.componentd.get(calendarId);
          if (typeof calendar != null) {
            //calendar.goToDate(moment(value[0], 'DD/MM/YYYY'));
          }
        }
      }
    });
  },
  updatewWidth(instance) {
    let clientWidth = instance.$wrapper.clientWidth - 50;
    let rows = instance.config.columns.length;
    rows = rows === 0 ? 1 : rows;
    let columnWidth = clientWidth / rows;
    for (let i in instance.config.columns) {
      instance.wrapper.setWidth(i, columnWidth);
    }
  },
};

let commandLoaded = false;

/**
 *
 *  Class Datatable
 *
 * Represent html datatable
 *
 * @category Represent html datatable
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 * @param {object} config
 */
export default class Datatable extends HtmlElement {
  constructor() {
    super();

    /**
     * Progress Bar element
     * @property {HTMLElement} progressBar
     */
    this.progressBar = document.getElementById(`spinner-${this.id}`);

    /**
     * Component title
     */
    this.tableTitle = document.getElementById(this.id + '_title');

    /**
     * Datatable object
     * @property {Grid} wrapper
     */
    this.wrapper = null;

    /**
     * Editor instance
     * @property {Editor} editor
     */
    this.editor = null;

    this.blockUi = new KTBlockUI(this.parentElement);
    if (!this.blockUi.isBlocked()) {
      this.blockUi.block();
    }
    if (!commandLoaded) {
      commandLoaded = true;
      this.command
        .addCommand('DisableCommand', async () => {
          return import(
            '../../../command/components/tables/datatable/AddFilterCommand'
          );
        })
        .addCommand('EnableCommand', async () => {
          return import(
            '../../../command/components/tables/datatable/AddRowCommand'
          );
        })
        .addCommand('FillFieldCommand', async () => {
          return import(
            '../../../command/components/tables/datatable/DeleteRowCommand'
          );
        })
        .addCommand('SelectFilterCommand', async () => {
          return import(
            '../../../command/components/tables/datatable/DetachCommand'
          );
        })
        .addCommand('SelectFilterCommand', async () => {
          return import(
            '../../../command/components/tables/datatable/RenderCommand'
          );
        })
        .addCommand('SelectFilterCommand', async () => {
          return import(
            '../../../command/components/tables/datatable/ReorderRowCommand'
          );
        })
        .addCommand('SelectFilterCommand', async () => {
          return import(
            '../../../command/components/tables/datatable/UpdateRowCommand'
          );
        });
    }
  }

  /**
   * Initialize component
   *
   * @returns  {Promise}
   */
  mount() {
    return super
      .mount()
      .then(() => {
        if (this.config.localStorage) {
          this.config.data = privateMethods.populateWithLocalStorage(this);
        }

        let modifyData = this.app.datad.getDatasource(
          this.config.endpoints.modifyData ?? ''
        );
        let readData = this.app.datad.getDatasource(
          this.config.endpoints.readData ?? ''
        );
        let createData = this.app.datad.getDatasource(
          this.config.endpoints.createData ?? ''
        );
        let updateData = this.app.datad.getDatasource(
          this.config.endpoints.updateData ?? ''
        );
        let deleteData = this.app.datad.getDatasource(
          this.config.endpoints.deleteData ?? ''
        );

        this.vue = createApp({
          methods: {
            exportAsExcel() {
              const format = 'xlsx';
              const exportSelectedOnly = true;
              const filename = 'test';
              this.$refs.grid.exportTable(format, exportSelectedOnly, filename);
            },
            exportAsCsv() {
              const format = 'csv';
              const exportSelectedOnly = true;
              const filename = 'test';
              this.$refs.grid.exportTable(format, exportSelectedOnly, filename);
            },
            onChange: (records) => {
              records = records.map((rec) => [
                'hset',
                rec.keys.join(),
                rec.name,
                rec.newVal,
              ]);
              this.app.datad.fetch(modifyData, { datas: records });
            },
            onDelete: (records) => {
              records = records.map((rec) => ['del', rec.keys.join()]);
              this.app.datad.fetch(deleteData, { datas: records });
            },
            onUpdate: (records) => {
              records = records.map((rec) => [
                'hset',
                rec.keys.join(),
                rec.name,
                rec.newVal,
              ]);
              this.app.datad.fetch(updateData, { datas: records });
            },
            onSelect: (records) => {
              console.log(records);
            },
            getOptions: () => {},
          },
          data: () => {
            return this.app.datad.fetch(readData).then((data) => {
              return {
                data: data.data,
              };
            });
          },
        });
        this.vue.use(VueExcelEditor);
      })
      .then(() => {
        this.vue.mount('#' + this.id + '_vuapp');
        this.component = console.log(this.vue);
      })
      .then(() => {
        if (this.blockUi.isBlocked()) {
          this.blockUi.release();
        }
      });
  }

  idle() {
    super.idle();
    //privateMethods.updatewWidth(this);
  }

  /**
   * Get default config
   *
   * @returns {object};
   */
  getDefaultConfig() {
    return {};
    let modifyData = this.app.datad.getDatasource(
      this.config.endpoints.modifyData
    );
    let readData = this.app.datad.getDatasource(this.config.endpoints.readData);
    let config = {
      toolbar: [
        {
          type: 'i',
          content: 'undo',
          icon: 'fa-undo',
          onclick: () => {
            this.wrapper.undo();
          },
        },
        {
          type: 'i',
          content: 'redo',
          icon: 'fa-redo',
          onclick: () => {
            this.wrapper.redo();
          },
        },
        {
          type: 'i',
          content: 'save',
          icon: 'fa-download',
          onclick: () => {
            this.wrapper.download();
          },
        },
      ],
      copyCompatibility: true,
      columns: this.config.columns,
      allowExport: true,
      includeHeadersOnDownload: true,
      columnSorting: true,
      columnDrag: true,
      columnResize: true,
      rowResize: true,
      rowDrag: true,
      editable: true,
      allowInsertRow: true,
      allowManualInsertRow: true,
      allowInsertColumn: false,
      allowManualInsertColumn: false,
      allowDeleteRow: true,
      allowRenameColumn: false,
      allowDeleteColumn: false,
      allowComments: false,
      selectionCopy: false,
      pagination: 50,
      search: true,
      filters: true,
      tableHeight: '100%',
      tableWidth: '100%',
      onafterchanges: () => {
        //console.log(this.wrapper.prepareJson(this.wrapper.getData()));
        this.app.ajaxd.call(
          modifyData,
          this.wrapper.prepareJson(this.wrapper.getData())
        );
      },
      // defaultColWidth: '100',
      minSpareRows: 2,
      //lazyLoading: true,
      url: readData.endpoint,
      method: readData.method,
      persistence: modifyData.endpoint,
      data: [],
      loadingSpin: true,
      tableOverflow: true,
      contextMenu: privateMethods.getContextMenu(),
    };
    //privateMethods.getRowConfig(this, config);
    return config;
  }

  /**
   * Destroy component
   */
  unmount() {
    super.unmount();
    if (typeof this.resizeEvent != 'undefined') {
      window.removeEventListener('resize', this.resizeEvent);
    }
    if (this.wrapper != null) {
      this.wrapper.destroy();
    }
  }

  /**
   * Reload component
   */
  reload() {
    super.reload();

    if (typeof this.wrapper != null) {
      new Promise((resolve) => {
        this.blockUi.block();
        this.app.datad.handlerWatch(
          'POST:' + this.config.endpoints.readData.endpoint,
          (e) => {
            this.wrapper.setData(e.detail.data);
            this.wrapper.updateTable();
          }
        );
        /*this.app.ajaxd
          .post(this.config.endpoints.readData.endpoint)
          .then((response) => {
            if (typeof response != 'undefined') {
              this.wrapper.setData(response.data);
              this.wrapper.updateTable();
            }
          });*/

        resolve(true);
      }).then(() => {
        this.blockUi.release();
      });
    }
  }
}
