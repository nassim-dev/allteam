import 'tui-grid/dist/tui-grid.css';
import 'tui-pagination/dist/tui-pagination.css';
import Grid from 'tui-grid';
import HtmlElement from '../../HtmlElement';
import LabelRenderer from './renderer/LabelRenderer';
import LinkRenderer from './renderer/LinkRenderer';
import moment from 'moment';
//import 'jquery';
import { KTBlockUI } from '../../../vendor/components/blockui';

const renderers = {
  LinkRenderer: LinkRenderer,
  LabelRenderer: LabelRenderer,
};

const editors = {};

const privateMethods = {
  getRowConfig: (instance, config) => {
    let apiConfig = { api: {} };
    let isAjaxSourced = false;

    if (typeof instance.config.endpoints.readData !== 'undefined') {
      apiConfig.api.readData = {};

      apiConfig.api.readData = instance.config.endpoints.readData;
      apiConfig.api.readData.url = instance.config.endpoints.readData.endpoint;
      isAjaxSourced = true;
    }
    if (typeof instance.config.endpoints.createData !== 'undefined') {
      apiConfig.api.createData = {};

      apiConfig.api.createData = instance.config.endpoints.createData;
      apiConfig.api.createData.url =
        instance.config.endpoints.createData.endpoint;
      isAjaxSourced = true;
    }
    if (typeof instance.config.endpoints.updateData !== 'undefined') {
      apiConfig.api.updateData = {};

      apiConfig.api.updateData = instance.config.endpoints.updateData;
      apiConfig.api.updateData.url =
        instance.config.endpoints.updateData.endpoint;
      isAjaxSourced = true;
    }
    if (typeof instance.config.endpoints.deleteData !== 'undefined') {
      apiConfig.api.deleteData = {};

      apiConfig.api.deleteData = instance.config.endpoints.deleteData;
      apiConfig.api.deleteData.url =
        instance.config.endpoints.deleteData.endpoint;
      isAjaxSourced = true;
    }
    if (typeof instance.config.endpoints.modifyData !== 'undefined') {
      apiConfig.api.modifyData = {};

      apiConfig.api.modifyData = instance.config.endpoints.modifyData;
      apiConfig.api.modifyData.url =
        instance.config.endpoints.modifyData.endpoint;
      isAjaxSourced = true;
    }
    if (isAjaxSourced) {
      config.data = apiConfig;

      return;
    }

    privateMethods.getRows(instance, config);
  },

  getRows: (instance, config) => {
    if (typeof instance.config.childs != undefined) {
      config.data = instance.config.childs;
      return;
    }

    if (instance.childNodes.length > 0) {
      //todo implements if rows are defined in html
    }
  },

  populateWithLocalStorage: (instance) => {
    instance.app.exceptiond.addException(
      'populateWithLocalStorage triggered',
      instance
    );
  },
  renderDeleteButton: (instance) => {
    if (instance.config.rowHeaders[0] === 'checkbox') {
      instance.wrapper.on('check', (ev) => {
        //instance.app.stored.push(instance.id + '-selected', {ev.rowId : ev.rowId});
      });
      instance.wrapper.on('uncheck', (ev) => {
        //instance.app.stored.remove(instance.id + '-selected', ev.rowId);
      });
    }
  },

  initializeCalendarSync: (instance, calendarId, field) => {
    instance.wrapper.on('select', function (e, instance, type, indexes) {
      if (type === 'row') {
        let value = instance.wrapper.rows(indexes).data().pluck(field);

        if (typeof value != 'undefined') {
          let calendar = instance.app.componentd.ge(calendarId);
          if (typeof calendar != null) {
            calendar.goToDate(moment(value[0], 'DD/MM/YYYY'));
          }
        }
      }
    });
  },

  initializeLocalStorage: (instance) => {
    instance.config.ajax = null;
    instance.config.initComplete = () => {
      instance.initTriggered = true;
      //instance.config.parentContainer.show();
    };
  },
  parseColumnCustomCallback: (instance) => {
    for (var i in instance.config.columns) {
      //Renderer for rows
      if (typeof instance.config.columns[i].renderer === 'string') {
        instance.config.columns[i].renderer =
          renderers[instance.config.columns[i].renderer];
      }

      //Editor for rows
      if (
        typeof instance.config.columns[i].editor !== 'undefined' &&
        typeof editors[instance.config.columns[i].editor.type] !== 'undefined'
      ) {
        instance.config.columns[i].editor.type =
          editors[instance.config.columns[i].editor.type];
      }
    }
  },
};

let commandLoaded = false;
/**
 *
 *  Class Datatable
 *
 * Represent html datatable
 *
 * @category Represent html datatable
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 * @param {object} config
 */
export default class Datatable extends HtmlElement {
  constructor() {
    super();

    /**
     * Progress Bar element
     * @property {HTMLElement} progressBar
     */
    this.progressBar = document.getElementById(`spinner-${this.id}`);

    /**
     * Component title
     */
    this.tableTitle = document.getElementById(this.id + '_title');

    /**
     * Datatable object
     * @property {Grid} wrapper
     */
    this.wrapper = null;

    /**
     * Editor instance
     * @property {Editor} editor
     */
    this.editor = null;

    this.blockUi = new KTBlockUI(this.parentElement);
    if (!this.blockUi.isBlocked()) {
      this.blockUi.block();
    }
    if (!commandLoaded) {
      commandLoaded = true;
      this.command
        .addCommand('DisableCommand', async () => {
          return import(
            '../../../command/components/tables/datatable/AddFilterCommand'
          );
        })
        .addCommand('EnableCommand', async () => {
          return import(
            '../../../command/components/tables/datatable/AddRowCommand'
          );
        })
        .addCommand('FillFieldCommand', async () => {
          return import(
            '../../../command/components/tables/datatable/DeleteRowCommand'
          );
        })
        .addCommand('SelectFilterCommand', async () => {
          return import(
            '../../../command/components/tables/datatable/DetachCommand'
          );
        })
        .addCommand('SelectFilterCommand', async () => {
          return import(
            '../../../command/components/tables/datatable/RenderCommand'
          );
        })
        .addCommand('SelectFilterCommand', async () => {
          return import(
            '../../../command/components/tables/datatable/ReorderRowCommand'
          );
        })
        .addCommand('SelectFilterCommand', async () => {
          return import(
            '../../../command/components/tables/datatable/UpdateRowCommand'
          );
        });
    }
  }

  /**
   * Initialize component
   *
   * @returns  {Promise}
   */
  mount() {
    return super
      .mount()
      .then(() => {
        if (this.config.localStorage) {
          this.config.event = privateMethods.populateWithLocalStorage(this);
        }
      })
      .then(() => {
        Grid.applyTheme('clean');
        //console.log(this.config);
        this.wrapper = new Grid(this.config);
      })
      .then(() => {
        if (typeof this.config.listeners != 'undefined') {
          this.config.listeners.forEach((listener) => {
            this.addListener(listener);
          });
        }
      })
      .then(() => {
        privateMethods.renderDeleteButton(this);
      })
      .then(() => {
        if (this.blockUi.isBlocked()) {
          this.blockUi.release();
        }
      });
  }

  /**
   * Get default config
   *
   * @returns {object};
   */
  getDefaultConfig() {
    let config = {
      el: document.getElementById(this.id + '_wrapper'),
      //scrollX: false,
      scrollY: false,
      rowHeight: 50,
      usageStatistics: false,
      showDummyRows: true,
      keyColumnName: 'rowId',
      pageOptions: {
        useClient: true,
        perPage: 20,
        template: {
          page: '<a href="#" class="m-1 btn btn-sm btn-light-primary">{{page}}</a>',
          currentPage:
            '<strong class="m-1 btn btn-sm btn-primary">{{page}}</strong>',
          moveButton:
            '<a href="#" class="m-1 btn btn-sm btn-light-primary  custom-class-{{type}}">' +
            '<i class="tui-ico-{{type}}">{{type}}</i>' +
            '</a>',
          disabledMoveButton:
            '<span class="m-1 btn btn-sm btn-light-primary disabled custom-class-{{type}}">' +
            '<span class="tui-ico-{{type}}">{{type}}</span>' +
            '</span>',
          moreButton:
            '<a href="#" class="m-1 btn btn-sm btn-light-primary custom-class-{{type}}">' +
            '<i class="fas fa-ellipsis-h"></i>' +
            '</a>',
        },
      },
      rowHeaders: ['checkbox'],
      columnOptions: {
        resizable: true,
        frozenCount: this.config.columns[0].name == 'tableactions' ? 1 : 0,
      },
      selectionUnit: 'row',
      minBodyHeight: 30,
      draggable: this.config.draggable ?? true,
    };
    privateMethods.getRowConfig(this, config);
    privateMethods.parseColumnCustomCallback(this);

    return config;
  }

  /**
   * Destroy component
   */
  unmount() {
    super.unmount();
    if (typeof this.wrapper != null) {
      this.wrapper.destroy();
    }
  }

  /**
   * Reload component
   */
  reload() {
    super.reload();

    if (typeof this.wrapper != null) {
      new Promise((resolve) => {
        this.blockUi.block();
        this.wrapper.readData();
        this.wrapper.refreshLayout();
        resolve();
      }).then(() => {
        this.blockUi.release();
      });
    }
  }
}
