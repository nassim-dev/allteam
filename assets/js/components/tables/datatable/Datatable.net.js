import 'jszip';
import 'pdfmake';
import 'datatables.net-bs5/css/dataTables.bootstrap5.css';
import 'datatables.net-responsive-bs5/css/responsive.bootstrap5.css';
import 'datatables.net-keytable-bs5/css/keyTable.bootstrap5.css';
import 'datatables.net-select-bs5/css/select.bootstrap5.css';
import 'datatables.net-rowreorder-bs5/css/rowReorder.bootstrap5.css';
import * from 'datatables.net'
(async () => {
  import('datatables.net').then(() => {
    import('datatables.net-bs5');

    import('datatables.net-buttons').then(() => {
      import('datatables.net-buttons-bs5');
      import(
        '../../../../../node_modules/datatables.net-buttons/js/buttons.html5'
      );
      import(
        '../../../../../node_modules/datatables.net-buttons/js/buttons.print'
      );
    });

    //import ('datatables.net-colreorder-bs5');
    //import ('datatables.net-colreorder-bs5/css/colReorder.bootstrap5.css');
    //import ('datatables.net-fixedcolumns-bs5');
    //import ('datatables.net-fixedcolumns-bs5/css/fixedColumns.bootstrap5.css');
    //import ('datatables.net-fixedheader-bs5');
    //import ('datatables.net-fixedheader-bs5/css/fixedHeader.bootstrap5.css');
    //import ('datatables.net-rowgroup-bs5');
    //import ('datatables.net-rowgroup-bs5/css/rowGroup.bootstrap5.css');
    import('datatables.net-rowreorder-bs5');
    import('datatables.net-responsive').then(() => {
      import('datatables.net-responsive-bs5');
    });
    //import ('datatables.net-scroller-bs5');
    //import ('datatables.net-scroller-bs5/css/scroller.bootstrap5.css');
    import('datatables.net-select').then(() => {
      import('datatables.net-select-bs5');
    });
    import('datatables.net-keytable-bs5');
    //import ('datatables.net-searchpanes');
    //import ('datatables.net-searchpanes-bs5');
    //import ('datatables.net-searchpanes-bs5/css/searchPanes.bootstrap5.css');
    import('datatables.net-editor').then(() => {
      import('datatables.net-editor-bs5');
    });
  });
})();

import HtmlElement from '../../HtmlElement';

const datatableLanguage = {
  sProcessing: "<i class='fas fa-sync fa-spin'></i>",
  sSearch: 'Rechercher&nbsp;:',
  sLengthMenu: '_MENU_',
  sInfo: "_START_ <i class='fas fa-arrow-right'></i> _END_ / _TOTAL_",
  sInfoEmpty: '0 &agrave; 0 sur 0',
  sInfoFiltered: '(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)',
  sInfoPostFix: '',
  sLoadingRecords: 'Chargement en cours...',
  sZeroRecords: 'Aucun &eacute;l&eacute;ment &agrave; afficher',
  sEmptyTable: 'Aucune donn&eacute;e disponible dans le tableau',
  oPaginate: {
    sFirst: "<i class='fas fa-fast-backward'></i>",
    sPrevious: "<i class='fas fa-angle-left'></i>",
    sNext: "<i class='fas fa-angle-right'></i>",
    sLast: "<i class='fas fa-fast-forward'></i>",
  },
  oAria: {
    sSortAscending: ': activer pour trier la colonne par ordre croissant',
    sSortDescending:
      ': activer pour trier la colonne par ordre d&eacute;croissant',
  },
  select: {
    rows: {
      _: '%d lignes séléctionnées',
      0: '',
      1: '1 ligne séléctionnée',
    },
  },
  deleteMessage:
    'Vous allez supprimer %d élément(s). Voulez-vous confirmer cette action ?',
};

/**
 *
 *  Class Datatable
 *
 * Represent html datatable
 *
 * @category Represent html datatable
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 * @param {object} config
 */
export default class Datatable extends HtmlElement {
  constructor() {
    super();

    /**
     * Default api endpoint
     * @param {array} endpoint
     */
    this.endpoint = this.datas('endpoint');

    /**
     * Api parameters provided dynamically
     * @param {array} endpointDynamicParameters
     */
    this.endpointDynamicParameters = [];

    /**
     * Progress Bar element
     * @property {HTMLElement} progressBar
     */
    this.progressBar = document.getElementById(`spinner-${this.id}`);

    /**
     * Component title
     */
    this.tableTitle = document.getElementById(this.id + '_title');

    /**
     * Datatable object
     * @property {DataTable} wrapper
     */
    this.wrapper = null;

    /**
     * Editor instance
     * @property {Editor} editor
     */
    this.editor = null;
  }

  /**
   * Initialize component
   *
   * @returns  {Promise}
   */
  mount() {
    new Promise((resolve) => {
      super.mount();
      resolve(this.config);
    })
      .then(() => {
        if (this.config.resources) {
          this.config.resources = this.getResourcesWithAjax;
        }
        if (this.config.localStorage) {
          this.config.event = this.populateWithLocalStorage;
        }
        if (this.config.customHeader || this.config.noDateHeader) {
          this.config.columnHeaderHtml = this.generateHeaderForCell;
        }
      })
      .then(() => {
        //this.wrapper = new $.fn.dataTable.Api(`#` + this.id + '_wrapper');
        this.wrapper = $(`#` + this.id + '_wrapper').DataTable(this.config);
        //this.wrapper = new DataTable(`#` + this.id + '_wrapper', this.config);
        this.wrapper.on('column-visibility.dt', () => {
          if (typeof this.wrapper.columns != 'undefined') {
            this.wrapper.columns.adjust();
          }
        });
      })
      .then(() => {
        //this.wrapper.searchPanes
        //  .container()
        //  .appendTo(this.wrapper.table().container());
      })
      .then(() => {
        if (typeof this.config.listeners != 'undefined') {
          this.config.listeners.forEach((listener) => {
            this.addListener(listener);
          });
        }
      });

    return this;
  }

  /**
   * Get default config
   *
   * @returns {object};
   */
  getDefaultConfig() {
    return {
      dom:
        "<'#header-" +
        this.id +
        ".d-flex align-items-center justify-content-between pb-2'<'p-2'p><'p-2'B><'p-2'f><'p-2'r><'d-flex align-items-center'<'pb-4 px-2'i><'p-2'l>>>t<'#footer-" +
        this.id +
        "'>",
      //searchPanes: {
      //  cascadePanes: true,
      //  viewTotal: true,
      //},
      hideEmptyCols: [-1],
      pagingType: 'full_numbers',
      lengthMenu: [
        [10, 25, 50, 100],
        [10, 25, 50, 100],
      ],
      scrollCollapse: true,
      scrollY: '100vh',
      scrollX: true,
      deferRender: true,
      //scroller: true,
      //processing: true
      columnDefs: [
        {
          orderable: false,
          className: 'select-checkbox',
          targets: 0,
        },
      ],
      select: {
        style: 'os',
        blurable: true,
      },
      buttons: this._getCustomButtons(),
      stateSave: true,
      language: datatableLanguage,
      ajax: this._getAjaxConfig(),
      drawCallback: this._drawCallbackCommand,
      initComplete: this._initCompleteCommand(),
    };
  }

  /**
   * Destroy component
   */
  unmount() {
    super.unmount();
    this.wrapper.destroy();
  }

  /**
   * Reload component
   */
  reload() {
    super.reload();
    this.wrapper.ajax.reload(null, false);
    this.wrapper.columns.adjust();
  }

  getResourcesWithAjax() {
    this.app.exceptiond.addException('getResourcesWithAjax triggered', this);
  }
  populateWithLocalStorage() {
    this.app.exceptiond.addException(
      'populateWithLocalStorage triggered',
      this
    );
  }
  generateHeaderForCell() {
    this.app.exceptiond.addException('generateHeaderForCell triggered', this);
  }

  _getCustomButtons() {
    return [
      //{
      //  text: '<i class="fas fa-search"></i>',
      //  tag: 'span',
      //  titleAttr: datatableLanguage.sSearch,
      //  className: ' btn-light-primary',
      //  action: () => {
      //    let element = this.querySelector(
      //      'tfoot input[type=search]:visible:first'
      //    );
      //    if (element) {
      //      element.focus();
      //      scrollTo(element);
      //    }
      //  },
      //},
      {
        extend: 'selected',
        text: '<i class="fas fa-trash"></i>',
        titleAttr: 'Supprimer',
        tag: 'span',
        className: ' btn-light-danger',
        action: function (e, instance, button, config) {
          let rows = instance.rows({
            selected: true,
          });
          let count = rows.indexes().length;
          let ids = rows.ids().toArray();
          let datatableId = this.id;
          let apiRessource = datatableId.split('_');
          apiRessource = apiRessource[apiRessource.length - 1];

          this.addPopup({
            title: 'Confirmation',
            content: datatableLanguage.deleteMessage,
            callbackYes: () => {
              if (this.config.apiAction.callback != false) {
                this.app.ajaxd
                  .delete('/api/' + apiRessource, { ids: ids })
                  .then((response) => {
                    this.reloadDatas();
                    this.deleteRows(ids);
                  });
              }
            },
            callbackNo: () => {},
          });
        },
      },

      {
        align: 'button-left',
        autoclose: true,
        extend: 'collection',
        text: '<i class="fas fa-cog"></i>',
        className: ' btn-light-primary',
        buttons: this._getCustomButtonsDropdown(),
      },
    ];
  }

  _getAjaxConfig() {
    return false;
    return {
      url: this.endpoint,
      type: 'POST',
      data: this.endpointDynamicParameters,
      dataType: 'json',
      cache: false,
      contentType: 'application/json',
      dataSrc: (response) => {
        if (typeof response == 'undefined') {
          return {};
        }
        if (typeof response.title != 'undefined') {
          this.tableTitle.innerHTML = response.title;
        }
        if (typeof response.data == 'string') {
          return {};
        }
        return response.data;
      },
      complete: () => {},
      beforeSend: (jqXHR, settings) => {
        this.app.ajaxd.decorate(settings.data);
      },
    };
  }
  _drawCallbackCommand() {
    return () => {
      /**
       * Show inline form after init, to prevent datatable html process
       */
      let $inlineForm = document.querySelector(
        '#inline-form-div-' + this.id + ' tr'
      );

      if (
        typeof $inlineForm != 'undefined' &&
        typeof $inlineForm != null &&
        $inlineForm != null
      ) {
        let $inlineParent = $inlineForm.parentElement;
        $inlineParent.removeChild($inlineForm);
        let $thead = $('<thead></thead>');
        $thead.append($inlineForm);
        $('#' + this.id + '_wrapper thead:nth-child(1)').after($thead);
        $inlineForm.style = '';
      }
    };
  }

  _initCompleteCommand() {
    return () => {
      this.initTriggered = true;
      //this.config.parentContainer.show();

      /*let el = $('#sticky-' + this.id + this.jsTableId);
      let footer = $('#footer-' + this.id + this.jsTableId);
      if (typeof el != 'undefined' && typeof footer != 'undefined') {
        //console.log(1, $('#navbar-header').height() + 10);
        el.css({
          position: 'sticky',
          top: $('#navbar-header').height() + 10,
          'z-index': 450,
          padding: '10px',
        });

        //let stickyTop = el.offset().top; // returns number
        let footerTop = footer.offset().top; // returns number
        let stickyHeight = el.height();
        let limit = footerTop - stickyHeight - 20;
        $(window).scroll(() => {
          // scroll event
          let windowTop = $(window).scrollTop(); // returns number

          //console.log([stickyHeight, stickyTop, footerTop, windowTop, limit]);
          if (limit <= windowTop) {
            if (el.css('position') != 'fixed' && footerTop != 0) {
              el.css({
                position: 'fixed',
                top: footerTop,
              });
            }
          } else {
            if (el.css('position') != 'sticky') {
              //console.log(2,$('#navbar-header').height() + 10);
              el.css({
                position: 'sticky',
                top: $('#navbar-header').height() + 10,
                'z-index': 450,
                padding: '10px',
              });
            }
          }
        });
      }*/
    };
  }
  _getCustomButtonsDropdown() {
    return [
      {
        extend: 'excelHtml5',
        text: '<i class="fas fa-file-excel"></i> Export XLS',
        titleAttr: 'Excel',
        filename: this.tableTitle.innerHTML + ' - ' + new Date().toDateString(),
        tag: 'a',
        className: 'btn  dropdown-item',
      },
      {
        extend: 'pdfHtml5',
        text: '<i class="fas fa-file-pdf"></i>Export PDF',
        titleAttr: 'PDF',
        filename: this.tableTitle.innerHTML + ' - ' + new Date().toDateString(),
        tag: 'a',
        className: ' dropdown-item',
      },
      {
        text: '<i class="fas spin-on-click fa-sync"></i>Reload',
        titleAttr: 'Recharger',
        tag: 'a',
        className: 'btn  dropdown-item',
        action: (e, instance, node, config) => {
          if (this.clipTimerId) {
            clearTimeout(this.clipTimerId);
          }
          this.clipTimerId = setTimeout(() => {
            $('.spin-on-click').addClass('fa-spin');

            this.progressBar.show();
            if (this.config.apiAction.callback != false) {
              instance.ajax.reload(() => {}, false);
            }
            setTimeout(() => {
              //jsConfigObj.refresh();
              $('.spin-on-click').removeClass('fa-spin');
              this.progressBar.hide();
            }, 1500);
            this.wrapper.columns.adjust();
          }, 500);
        },
      },
      /*{
                extend: "columnsToggle",
                columns: 1,
                columnText:  () => {
                  return '<i class="fas fa-cogs"></i>';
                },
                titleAttr: "Actions",
                tag: 'a',
                className: ' dropdown-item'
              },*/
      /*{
                extend: 'colvis',
                columns: ':gt(0)',
                text: '<i class="fas fa-eye"></i>'
              },*/
    ];
  }

  initializeCalendarSync(calendarId, field) {
    this.wrapper.on('select', function (e, instance, type, indexes) {
      if (type === 'row') {
        let value = this.wrapper.rows(indexes).data().pluck(field);

        if (typeof value != 'undefined') {
          let calendar = this.app.componentd.ge(calendarId);
          if (typeof calendar != null) {
            calendar.goToDate(moment(value[0], 'DD/MM/YYYY'));
          }
        }
      }
    });
  }

  initializeLocalStorage() {
    this.config.ajax = null;
    this.config.initComplete = () => {
      this.initTriggered = true;
      //this.config.parentContainer.show();
    };
  }

  initializeEditor() {
    if (this.editor != null) {
      this.app.exceptiond.addException(
        'Editor is already defined for ' + this.id,
        this.editor
      );
      return;
    }

    /*let fields = [];
    this.config.editor.fields.each(function (d) {
      d.compare = function (a, b) {
        return false;
      };
      fields.push(d);
    });*/

    this.editor = new $.fn.dataTable.Editor({
      ajax: {
        url:
          '/api/' +
          this.config.editor.ajax.ressource +
          '//' +
          this.config.editor.ajax.action,
        type: 'PUT',
      },
      table: this.config.editor.table,
      fields: this.config.editor.fields,
      formOptions: {
        inline: {
          submit: 'changed',
        },
      },
    });

    this.wrapper.on(
      'click',
      'tbody td:not(:nth-child(1)):not(:nth-child(2))',
      () => {
        this.editor.inline(this, {
          onBlur: 'submit',
          onReturn: 'submit',
          scope: 'cell',
          onsubmit: (settings, original) => {
            if (original.revert == removeTags($('input', this).val())) {
              original.reset();
              this.editor.close();
              return true;
            }
          },
        });
      }
    );

    this.wrapper.on('click', '.btn-editor-submit', (e) => {
      setTimeout(() => {
        this.editor.submit().close();
        return true;
      }, 10);
    });

    this.config.keys = {
      columns: ':not(:nth-child(1)):not(:nth-child(2))',
      editor: this.editor,
    };
    this.config.autoFill = {
      columns: ':not(:nth-child(1)):not(:nth-child(2))',
      editor: this.editor,
    };
  }
}
