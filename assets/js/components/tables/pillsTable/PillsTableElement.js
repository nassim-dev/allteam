import HtmlElement from '../../HtmlElement';

/**
 *
 *  Class PillsTableElement
 *
 * Represent html pillsTableElement
 *
 * @category Represent html pillsTableElement
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 * @param {object} config
 */
export default class PillsTableElement extends HtmlElement {
  constructor() {
    super();
  }

  /**
   * Initialize component
   *
   * @returns  {Promise}
   */
  mount() {
    return super.mount().then(() => {
      let tabEl = document.querySelector(
        'a[data-bs-target="#' + this.id + '"]'
      );
      tabEl?.addEventListener('shown.bs.tab', (event) => {
        this.dispatchEvent(new CustomEvent('shown.bs.tab'));
      });
    });
  }

  /**
   * Destroy component
   */
  unmount() {
    super.unmount();
    return this;
  }

  /**
   * Reload component
   */
  reload() {
    super.reload();

    return this;
  }
}
