import HtmlElement from '../HtmlElement';
import { scrollTo } from '../../functions/init-smoothScroll';
import * as bootstrap from '../../../../node_modules/bootstrap/dist/js/bootstrap.esm.js';

let commandLoaded = false;
/**
 *
 *  Class Modal
 *
 * Represent html modal
 *
 * @category Represent html modal
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 * @param {object} config
 */
export default class Modal extends HtmlElement {
  constructor() {
    super();

    this.wrapper = new bootstrap.Modal(this);
    this.addEventListener(
      'shown.bs.modal',
      this.handlersRepository('shownBsHandler')
    );

    this.addEventListener(
      'hide.bs.modal',
      this.handlersRepository('hideBsHandler')
    );
    this.addEventListener(
      'hidden.bs.modal',
      this.handlersRepository('hiddenBsHandler')
    );
    if (!commandLoaded) {
      commandLoaded = true;
      this.command
        .addCommand('ModalHideCommand', async () => {
          return import('../../command/components/modal/ModalHideCommand');
        })
        .addCommand('ModalShowCommand', async () => {
          return import('../../command/components/modal/ModalShowCommand');
        })
        .addCommand('OpenPopupCommand', async () => {
          return import('../../command/components/modal/OpenPopupCommand');
        });
    }

    /**
     * True if a confirm must be triggered befor modal close
     */
    this.autosave = false;
  }
  /**
   * Initialize component
   *
   * @returns  {Promise}
   */
  mount() {
    return super.mount();
  }

  /**
   * Destroy component
   */
  unmount() {
    super.unmount();

    this.removeEventListener(
      'shown.bs.modal',
      this.handlersRepository('shownBsHandler')
    );

    this.removeEventListener(
      'hide.bs.modal',
      this.handlersRepository('hideBsHandler')
    );
    this.removeEventListener(
      'hidden.bs.modal',
      this.handlersRepository('hiddenBsHandler')
    );

    return this;
  }

  /**
   * Reload component
   */
  reload() {
    super.reload();
    this.wrapper.handleUpdate();
    return this;
  }

  triggerShow() {
    if (this.is('initialized')) {
      this.goto('mount');
    }
    this.app.componentd.hideFields('i-select');
    this.app.componentd.hideFields('i-multilist');
    this.app.componentd.hideFields('i-plurimultilist');
    this.app.componentd.hideFields('i-multiselect');

    this.wrapper.show(this);
  }

  triggerHide() {
    if (this.is('initialized')) {
      this.goto('mount');
    }
    this.wrapper.hide(this);
  }

  /**
   *
   * @param {string} name
   * @returns
   */
  handlersRepository(name) {
    switch (name) {
      case 'shownBsHandler':
        return () => {
          scrollTo('#header');
          this.classList.replace(
            'animate__bounceOutLeft',
            'animate__bounceInLeft'
          );
          window.location.hash = '#' + this.id.replace('modal_', '');
        };

      case 'hiddenBsHandler':
        return () => {
          this.style.display = 'block';
          this.classList.replace(
            'animate__bounceInLeft',
            'animate__bounceOutLeft'
          );
        };

      case 'hideBsHandler':
        return () => {
          setTimeout(() => {
            this.style.display = 'none';
          }, 600);
        };

      default:
        return () => {};
    }
  }
}
