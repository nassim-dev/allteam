import HtmlElement from '../HtmlElement';

/**
 *
 *  Class ProgressBar
 *
 * Represent html progressBar
 *
 * @category Represent html progressBar
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 */
export default class ProgressBar extends HtmlElement {
  constructor() {
    super();
  }

  /**
   * Initialize component
   *
   * @returns  {Promise}
   */
  mount() {
    return super.mount();
  }

  /**
   * Destroy component
   */
  unmount() {
    super.unmount();
  }

  /**
   * Reload component
   */
  reload() {
    super.reload();
  }
}
