import HtmlElement from '../HtmlElement';
let commandLoaded = false;
/**
 *
 *  Class Diagram
 *
 * Represent html diagram
 *
 * @category Represent html diagram
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 * @param {object} config
 */
export default class Diagram extends HtmlElement {
  constructor() {
    super();
    if (!commandLoaded) {
      commandLoaded = true;
      this.command.addCommand('RenderCommand', async () => {
        return import('../../command/components/diagram/RenderCommand');
      });
    }
  }

  /**
   * Initialize component
   *
   * @returns  {Promise}
   */
  mount() {
    return super.mount();
  }

  /**
   * Destroy component
   */
  unmount() {
    super.unmount();
  }

  /**
   * Reload component
   */
  reload() {
    return super.reload();
  }
}
