import HtmlElement from '../HtmlElement';

let commandLoaded = false;
/**
 *
 *  Class Button
 *
 * Represent html button
 *
 * @category html_component
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 * @param {object} config
 */
export default class Button extends HtmlElement {
  constructor() {
    super();

    /**
     * Real Dom events to bind to this object
     * @property {array} bindedEvents
     * @protected
     */
    this.bindedEvents = this.config.bindedEvents
      ? [...this.config.bindedEvents, 'click']
      : ['click'];

    /**
     * Default action to execute
     * @property {string} action
     */
    this.action = this.config.action || 'DefaultCommand';
    if (!commandLoaded) {
      commandLoaded = true;
      this.command
        .addCommand('ActionConfirmCommand', async () => {
          return import('../../command/components/button/ActionConfirmCommand');
        })
        .addCommand('DeleteCommand', async () => {
          return import('../../command/components/button/DeleteCommand');
        })
        .addCommand('GeneratePdfCommand', async () => {
          return import('../../command/components/button/GeneratePdfCommand');
        })
        .addCommand('LogoutCommand', async () => {
          return import('../../command/components/button/LogoutCommand');
        })
        .addCommand('PopulateCommand', async () => {
          return import('../../command/components/form/PopulateCommand');
        })
        .addCommand('RedirectCommand', async () => {
          return import('../../command/components/button/RedirectCommand');
        })
        .addCommand('SendMailCommand', async () => {
          return import('../../command/components/button/SendMailCommand');
        })
        .addCommand('ModalShowCommand', async () => {
          return import('../../command/components/modal/ModalShowCommand');
        });
    }
  }

  /**
   * Initialize component
   *
   * @returns  {Promise}
   */
  mount() {
    return super.mount().then(() => {
      this.startIconAnimation();
      if (typeof this.config.listeners != 'undefined') {
        for (let i in this.config.listeners) {
          this.addListener(this.config.listeners[i]);
        }
      }
    });
  }

  /**
   * Idle state action
   */
  idle() {
    this.unlock();
  }

  /**
   * Processing state action
   * @param {object} parameters
   */
  processing(parameters = []) {
    this.lock();
    this.command.execute(this, this.action, parameters);
  }

  /**
   * Start button icon animation
   */
  startIconAnimation() {
    this.disabled = true;
    this.setAttribute('data-kt-indicator', 'on');
  }

  /**
   * Stop button icon animation
   */
  stopIconAnimation() {
    this.disabled = false;
    this.removeAttribute('data-kt-indicator');
  }

  /**
   * Method to execute for locking component
   */
  lock() {
    this.startIconAnimation();
  }

  /**
   * Method to execute for unlocking component
   */
  unlock() {
    this.stopIconAnimation();
  }

  /**
   * Destroy component
   */
  unmount() {
    super.unmount();
    return this;
  }

  /**
   * Reload component
   */
  reload() {
    super.reload();
    return this;
  }
}
