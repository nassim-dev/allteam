import HtmlElement from '../HtmlElement';

/**
 *
 *  Class Treeview
 *
 * Represent html treeview
 *
 * @category Represent html treeview
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 * @param {object} config
 */

export default class Treeview extends HtmlElement {
  constructor() {
    super();
  }

  /**
   * Initialize component
   *
   * @returns  {Promise}
   */
  mount() {
    return super.mount();
  }

  /**
   * Destroy component
   */
  unmount() {
    super.unmount();
  }

  /**
   * Reload component
   */
  reload() {
    return super.reload();
  }
}
