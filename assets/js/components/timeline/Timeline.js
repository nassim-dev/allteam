import HtmlElement from '../HtmlElement';
import { Timeline as TimelineLib } from 'vis-timeline/esnext';
import { DataSet } from 'vis-data';
import 'vis-timeline/dist/vis-timeline-graph2d.css';
import moment from 'moment';
import { KTBlockUI } from '../../vendor/components/blockui';

const privateMethods = {
  initDateSelector: (instance) => {
    if (typeof instance.config.dateSelector === 'undefined') {
      return;
    }

    let selector = instance.app.componentd.get(
      instance.config.dateSelector.parameters.id
    );
    if (selector === null) {
      return;
    }
    selector.addEventListener('change', (event) => {
      if (typeof event.detail === 'undefined' || event.detail === null) {
        return;
      }
      if (typeof event.detail.selectedDates === 'undefined') {
        return;
      }
      if (event.detail.selectedDates.length != 2) {
        return;
      }

      let start = moment(event.detail.selectedDates[0]);
      let end = moment(event.detail.selectedDates[1]);

      instance.wrapper.setWindow(start, end);
    });
  },
  initTodaySelector: (instance) => {
    if (typeof instance.config.todaySelector === 'undefined') {
      return;
    }

    let selector = instance.app.componentd.get(
      instance.config.todaySelector.parameters.id
    );
    if (selector === null) {
      return;
    }
    selector.addEventListener('click', (event) => {
      let today = moment();
      instance.wrapper.moveTo(today);
    });
  },
  initFitSelector: (instance) => {
    if (typeof instance.config.fitSelector === 'undefined') {
      return;
    }

    let selector = instance.app.componentd.get(
      instance.config.fitSelector.parameters.id
    );
    if (selector === null) {
      return;
    }
    selector.addEventListener('click', (event) => {
      instance.wrapper.fit();
    });
  },
  loadDatas: (instance, properties) => {
    if (instance.config.api != false) {
      let config = instance.config.api;
      if (typeof config.readData == 'undefined') {
        return;
      }

      instance.app.datad.handlerWatch(
        instance.app.datad.getDatasource(config.readData),
        (e) => {
          if (typeof e.detail.data.items != 'undefined') {
            for (var i in e.detail.data.items) {
              instance.addElement(e.detail.data.items[i]);
            }
          }
          if (typeof e.detail.data.groups != 'undefined') {
            for (var i in e.detail.data.groups) {
              instance.addElement(e.detail.data.groups[i]);
            }
          }
        }
      );

      instance.app.ajaxd
        .call(
          instance.app.datad.getDatasource(config.readData),
          properties ?? {}
        )
        .then((response) => {
          if (typeof response.data == 'undefined') {
            return;
          }
          if (typeof response.data.items != 'undefined') {
            for (var i in response.data.items) {
              instance.addElement(response.data.items[i]);
            }
          }
          if (typeof response.data.groups != 'undefined') {
            for (var i in response.data.groups) {
              instance.addElement(response.data.groups[i]);
            }
          }
        });
    }
  },
  postData: (instance, datas) => {
    if (instance.config.api != false) {
      let config = instance.config.api;
      if (typeof config.createData == 'undefined') {
        return;
      }

      instance.app.ajaxd
        .call(instance.app.datad.getDatasource(config.createData), datas ?? {})
        .then((response) => {
          //if (typeof response.data == 'undefined') {
          //  return;
          //}
          //if (typeof response.data.items != 'undefined') {
          //  for (var i in response.data.items) {
          //    instance.addElement(response.data.items[i]);
          //  }
          //}
          //if (typeof response.data.groups != 'undefined') {
          //  for (var i in response.data.groups) {
          //    instance.addElement(response.data.groups[i]);
          //  }
          //}
        });
    }
  },
  pushDatas: (instance, datas) => {
    if (instance.config.api != false) {
      let config = instance.config.api;
      if (typeof config.upateData == 'undefined') {
        return;
      }

      instance.app.ajaxd
        .call(instance.app.datad.getDatasource(config.upateData), datas ?? {})
        .then((response) => {
          //if (typeof response.data == 'undefined') {
          //  return;
          //}
          //if (typeof response.data.items != 'undefined') {
          //  for (var i in response.data.items) {
          //    instance.addElement(response.data.items[i]);
          //  }
          //}
          //if (typeof response.data.groups != 'undefined') {
          //  for (var i in response.data.groups) {
          //    instance.addElement(response.data.groups[i]);
          //  }
          //}
        });
    }
  },
  initAjaxSources: (instance) => {
    instance.wrapper.on('rangechanged', (properties) => {
      privateMethods.loadDatas(instance, properties);
    });
  },
  getRowConfig: (instance) => {
    let apiConfig = { api: {} };
    let isAjaxSourced = false;
    instance.config.api = false;
    if (typeof instance.config.endpoints.readData !== 'undefined') {
      apiConfig.readData = instance.config.endpoints.readData;
      isAjaxSourced = true;
    }
    if (typeof instance.config.endpoints.createData !== 'undefined') {
      apiConfig.createData = instance.config.endpoints.createData;
      isAjaxSourced = true;
    }
    if (typeof instance.config.endpoints.updateData !== 'undefined') {
      apiConfig.updateData = instance.config.endpoints.updateData;
      isAjaxSourced = true;
    }
    if (typeof instance.config.endpoints.deleteData !== 'undefined') {
      apiConfig.deleteData = instance.config.endpoints.deleteData;
      isAjaxSourced = true;
    }
    if (typeof instance.config.endpoints.modifyData !== 'undefined') {
      apiConfig.modifyData = instance.config.endpoints.modifyData;
      isAjaxSourced = true;
    }
    if (isAjaxSourced) {
      instance.config.api = apiConfig;

      privateMethods.initAjaxSources(instance);
    }
  },
};
/**
 *
 *  Class Timeline
 *
 * Represent html timeline
 *
 * @category Represent html timeline
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 * @param {object} config
 */
export default class Timeline extends HtmlElement {
  constructor() {
    super();
    this.blockUi = new KTBlockUI(this.parentElement);
    if (!this.blockUi.isBlocked()) {
      this.blockUi.block();
    }
    this.wrapper = document.getElementById(this.id + '_wrapper');
    this.items = new DataSet();

    this.groups = new DataSet();
  }

  /**
   * Initialize component
   *
   * @returns {Promise}
   */
  mount() {
    return super
      .mount()
      .then(() => {
        this.wrapper = new TimelineLib(
          this.wrapper,
          this.items,
          this.groups,
          this.getDefaultConfig()
        );
        privateMethods.initDateSelector(this);
        privateMethods.initTodaySelector(this);
        privateMethods.initFitSelector(this);
      })
      .then(() => {
        if (typeof this.config.groups != 'undefined') {
          for (var i in this.config.groups) {
            this.addCategory(this.config.groups[i]);
          }
        }
        if (typeof this.config.items != 'undefined') {
          for (var i in this.config.items) {
            this.addElement(this.config.items[i]);
          }
        }
        privateMethods.getRowConfig(this);
      })
      .then(() => {
        privateMethods.loadDatas(this);
      })
      .then(() => {
        if (this.blockUi.isBlocked()) {
          this.blockUi.release();
        }
      });
  }

  /**
   * Add new item
   *
   * @param {object} properties
   * @param {number|string} idcategory
   */
  addElement(properties, idcategory) {
    let { id, content, start, end, group, type, className, style } = properties;

    if (!moment.isMoment(start)) {
      start = moment(start);
    }
    if (null != end && !moment.isMoment(end)) {
      end = moment(end);
    }
    let item = {
      id: id,
      content: content,
      start: start,
    };

    if (end != null) {
      item.end = end;
    }
    if (type != null) {
      item.type = type;
    }
    if (group != null) {
      item.group = group;
    }
    if (className != null) {
      item.className = className;
    }
    if (style != null) {
      item.style = style;
    }

    if (this.items.get(id)) {
      privateMethods.pushDatas(item);
      this.items.update(item);
    } else {
      privateMethods.postData(item);
      this.items.add(item);
    }
  }

  /**
   * Add a group
   *
   *  @param {object} propertie
   */
  addCategory(propertie) {
    let { id, content, className, style, order, properties } = propertie;
    let group = { id: id, content: content };
    if (className != null) {
      group.className = className;
    }
    if (style != null) {
      group.style = style;
    }
    if (order != null) {
      group.order = order;
    }
    if (properties != null) {
      group.properties = properties;
    }
    this.groups.update(group);
  }

  /**
   * Destroy component
   */
  unmount() {
    super.unmount();
    return this;
  }

  getDefaultConfig() {
    let config = {
      stack: true,
      maxHeight: 640,
      verticalScroll: true,
      zoomKey: 'ctrlKey',
      editable:
        typeof this.config.editable != 'undefined'
          ? this.config.editable
          : false,
    };
    return config;
  }

  /**
   * Reload component
   */
  reload() {
    super.reload();
    privateMethods.loadDatas(this);
    this.wrapper.redraw();
    return this;
  }
}
