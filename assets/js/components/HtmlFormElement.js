import utils from './CustomElementUtils';
import CommandManager from '../CommandManager';
import Machine from '../machine/Machine';
import State from '../machine/State';
import App from '../App';
/**
 *
 *  Class HtmlFormElement
 *
 * Represent  Base HtmlFormElement
 *
 * @category Represent html htmlFormElement
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */
export default class HtmlFormElement extends HTMLFormElement {
  constructor() {
    super();

    /**
     * Component config
     *
     * @property {object} config
     */
    this.config = this.datas() ?? [];
    utils.parseHtmlConfig(this);

    /**
     * Command manager with specific actions
     * @property {CommandManager} command
     */
    this._command = null;
    /**
     * State machine
     * @property {Machine}
     */
    this._machine = null;

    /**
     * Dom events to bind to object
     * @property {array} bindedEvents
     * @protected
     */
    this.bindedEvents = this.config.bindedEvents ?? [];

    /**
     * Wrapper for object
     * @property {HTMLElement} wrapper
     */
    this.wrapper = this;

    /**
     * Event listeners
     * @property {array} listeners
     */
    this.listeners = [];
  }

  // Lifecycle callbacks for custom element implementation

  /**
   * Invoked each time the custom element is disconnected
   */
  disconnectedCallback() {
    utils.disconnectedCallback(this);
  }

  /**
   * Invoked each time one of the custom element's attributes is added
   */
  adoptedCallback() {
    this.reload();
  }

  /**
   * Invoked each time the custom element is appended
   */
  connectedCallback() {
    utils.connectedCallback(this);
  }

  /**
   * Invoked each time one of the custom element's attributes is added
   */
  attributeChangedCallback(attrName, oldValue, newValue) {
    if (newValue === oldValue) {
      return;
    }
  }

  /**
   * Get attributes to notice change
   */
  static get observedAttributes() {
    return [];
  }

  /**
   * Initialize component
   */
  mount() {
    return utils.mount(this);
  }

  /**
   * Add event resize listener if enabled in config
   * @param {object} listener
   *
   */
  addListener(listener) {
    utils.addListener(listener, this);
  }

  idle() {
    //this.app.exceptiond.addException('idle : implement some logic', this);
  }
  processing() {
    //this.app.exceptiond.addException('processing : implement some logic', this);
  }
  error() {
    //this.app.exceptiond.addException('error : implement some logic', this);
  }
  success() {
    //this.app.exceptiond.addException('success : implement some logic', this);
  }
  rendering() {
    //this.app.exceptiond.addException('rendering : implement some logic', this);
  }
  lock() {
    //this.app.exceptiond.addException('lock : implement some logic', this);
  }
  unlock() {
    //this.app.exceptiond.addException('unlock : implement some logic', this);
  }
  /**
   * Destroy component
   */
  unmount() {
    utils.unmount(this);
  }

  /**
   *
   * @param {Event} event
   */
  handleEvent(event) {}

  /**
   * Reload component
   */
  reload() {}

  /**
   * Check if the key is allowed for this object's configuration
   *
   * @param {string} key

   * @returns {boolean}
   */
  isBuildinKey(key) {
    return false;
  }

  /**
   * Attach event listener
   *
   * @param {string} event event to watch
   * @param {function} handler function or command name to exectute
   * @param {string|null} selector string selector
   * @param {array} parameters handler specific parameters
   */
  on(event, handler, selector = null, parameters = []) {
    utils.on(this, event, handler, selector, parameters);
  }

  /**
   * Remove all events listeners for this type of event
   * @param {string} event
   * @param {string|null} selector string selector
   */
  off(event, selector = null) {
    utils.off(this, event, selector);
  }

  /**
   * Set/Get datas attributes
   *
   * @param {string} key
   * @param {*} value
   *
   * @returns {string|null|object}
   */
  datas(key = null, value = null) {
    return utils.datas(this, key, value);
  }

  /**
   * Return true if component is in state
   *
   * @param {string} state
   * @returns {boolean}
   */
  is(state) {
    return this.machine.getState(this).name === state;
  }

  /**
   * Return  component's state
   *
   * @returns {State}
   */
  getState() {
    return this.machine.getState(this);
  }

  /**
   * Go to specific state if available
   *
   * Available states : load, mount, idle, processing, error, success, rendering, unmount
   * @param {string} state
   * @param {object} parameters
   */
  goto(state, parameters = []) {
    this.machine.execute(this, state, parameters);
    return this;
  }

  // Getters

  /**
   * Get CommandManager (specific or global)
   * @returns {CommandManager}
   */
  get command() {
    return (this._command =
      this._command ?? this.app.command.createManager(this.tagName));
  }

  /**
   * State machine
   *
   * @returns {Machine}
   */
  get machine() {
    return (this._machine =
      this._machine ?? this.app.machined.createMachine(this, this.tagName));
  }

  /**
   * Interfacing main application
   *
   * @returns {App} app
   */
  get app() {
    /**
     * @property {App} app
     */
    return window.app;
  }

  /**
   * Get default config
   *
   * @returns {object};
   */
  getDefaultConfig() {
    return {};
  }
}
