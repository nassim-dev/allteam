import HtmlElement from '../HtmlElement';
import flowy from './vendor/flowy-engine';
import { KTBlockUI } from '../../vendor/components/blockui';
import utils from '../CustomElementUtils';
//import flowy from './vendor/flowy-engine';
import './vendor/flowy-engine.css';

const privateMethods = {};
/**
 *
 *  Class Flow
 *
 * Represent html flow
 *
 * @category Represent html flow
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 * @param {object} config
 */
export default class Flow extends HtmlElement {
  constructor() {
    super();

    this.categories = {};
    this.childs = {};

    this.blockUi = new KTBlockUI(this.parentElement);
    if (!this.blockUi.isBlocked()) {
      this.blockUi.block();
    }

    this.$wrapper = document.getElementById(this.id + '_wrapper');

    this.wrapper = null;
  }

  /**
   * Initialize component
   *
   * @returns  {Promise}
   */
  mount() {
    return super
      .mount()
      .then(() => {
        this.wrapper = flowy(
          this.$wrapper,
          (block) => {
            return this.onGrab(block);
          },
          () => {
            return this.onRelease();
          },
          (block, first, parent) => {
            return this.onSnap(block, first, parent);
          },
          (block, parent) => {
            return this.onRearrange(block, parent);
          },
          this.config.spacing_x,
          this.config.spacing_y
        );
      })
      .then(() => {
        utils.loadDatas(this);
      })
      .then(() => {
        //if (this.blockUi.isBlocked()) {
        this.blockUi.release();
        //}
      });
  }

  onGrab(block) {
    return true;
  }
  onRelease() {
    this.saveState();
  }
  onSnap(block, first, parent) {
    /* var blockin = block.querySelector('.blockin');
    blockin.parentNode.removeChild(blockin);

    block.innerHTML += `<div class="card card-flush shadow-sm blockin">
    <div class="card-header">
      <h3 class="card-title">Title</h3>
      <div class="card-toolbar">
        <button type="button" class="btn btn-sm btn-light">Action</button>
      </div>
    </div>
    <div class="card-body">
      Triggers when somebody performs a specified action
    </div>
  </div>`;
    */
    return true;
  }
  onRearrange(block, parent) {
    this.saveState();
    //return true;
  }

  /**
   * Destroy component
   */
  unmount() {
    super.unmount();
    if (typeof this.wrapper != null) {
      //this.wrapper.deleteBlocks();
    }
    this.saveState();
    return this;
  }

  getDefaultConfig() {
    return {
      spacing_x: 40,
      spacing_y: 100,
    };
  }

  initFromState(state) {
    this.wrapper.import(state);
  }

  saveState() {
    //utils.saveDatas(this, this.wrapper.output());
  }

  /**
   * Reload component
   */
  reload() {
    super.reload();
    utils.loadDatas(this);
    return this;
  }
}
