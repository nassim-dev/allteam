//Define custom elements
import Button from './button/Button.js';
customElements.define('t-button', Button);

import Calendar from './calendar/Calendar.js';
customElements.define('t-calendar', Calendar);

import Chart from './chart/Chart.js';
customElements.define('t-chart', Chart);

import Diagram from './diagram/Diagram.js';
customElements.define('t-diagram', Diagram);

import Form from './form/Form.js';
customElements.define('t-form', Form, { extends: 'form' });

import Stepper from './form/Stepper.js';
customElements.define('t-stepper', Stepper);

import Label from './label/Label.js';
customElements.define('t-label', Label);

import Draglist from './draglist/Draglist.js';
customElements.define('t-draglist', Draglist);

import Flow from './flow/Flow.js';
customElements.define('t-flow', Flow);

import Menu from './menu/Menu.js';
customElements.define('t-menu', Menu);

import Modal from './modal/Modal.js';
customElements.define('t-modal', Modal);

import ProgressBar from './progressBar/ProgressBar.js';
customElements.define('t-progress', ProgressBar);

import Gantt from './gantt/Gantt.js';
customElements.define('t-gantt', Gantt);

import BootstrapTour from './tour/BootstrapTour.js';
customElements.define('t-tour', BootstrapTour);

import Treeview from './treeview/Treeview.js';
customElements.define('t-treeview', Treeview);

import Timeline from './timeline/Timeline.js';
customElements.define('t-timeline', Timeline);

import Datatable from './tables/datatable/Datatable.js';
customElements.define('t-datatable', Datatable);

import PillsTable from './tables/pillsTable/PillsTable.js';
customElements.define('t-pillstable', PillsTable);

import PillsTableElement from './tables/pillsTable/PillsTableElement.js';
customElements.define('t-pillstableelement', PillsTableElement);

import AddressElement from './form/elements/AddressElement.js';
customElements.define('i-address', AddressElement, {});

import ButtonElement from './form/elements/ButtonElement.js';
customElements.define('i-button', ButtonElement, {});

import CguElement from './form/elements/CguElement.js';
customElements.define('i-cgu', CguElement, {});

import CheckboxElement from './form/elements/CheckboxElement.js';
customElements.define('i-checkbox', CheckboxElement, {});

import DatatableElementElement from './form/elements/DatatableElement.js';
customElements.define('i-datatable', DatatableElementElement, {});

import DateElementElement from './form/elements/DateElement.js';
customElements.define('i-date', DateElementElement, {});

import DaterangeElement from './form/elements/DaterangeElement.js';
customElements.define('i-daterange', DaterangeElement, {});

import DatetimeElement from './form/elements/DatetimeElement.js';
customElements.define('i-datetime', DatetimeElement, {});

import DatetimerangeElement from './form/elements/DatetimerangeElement.js';
customElements.define('i-datetimerange', DatetimerangeElement, {});

import EmailElement from './form/elements/EmailElement.js';
customElements.define('i-email', EmailElement, {});

import FileinputElement from './form/elements/FileinputElement.js';
customElements.define('i-fileinput', FileinputElement, {});

import CalendarElementElement from './form/elements/CalendarElement.js';
customElements.define('i-calendar', CalendarElementElement, {});

import HiddenElement from './form/elements/HiddenElement.js';
customElements.define('i-hidden', HiddenElement, {});

import MentionableElement from './form/elements/MentionableElement.js';
customElements.define('i-mentionable', MentionableElement, {});

import NumberElement from './form/elements/NumberElement.js';
customElements.define('i-number', NumberElement, {});

import PasswordElement from './form/elements/PasswordElement.js';
customElements.define('i-password', PasswordElement, {});

import SignatureElement from './form/elements/SignatureElement.js';
customElements.define('i-signature', SignatureElement, {});

import SliderElement from './form/elements/SliderElement.js';
customElements.define('i-slider', SliderElement, {});

import StaticElement from './form/elements/StaticElement.js';
customElements.define('i-static', StaticElement, {});

import SwitchElement from './form/elements/SwitchElement.js';
customElements.define('i-switch', SwitchElement, {});

import TextareaElement from './form/elements/TextareaElement.js';
customElements.define('i-textarea', TextareaElement, {});

import TextElement from './form/elements/TextElement.js';
customElements.define('i-text', TextElement, {});

import TagElement from './form/elements/TagElement.js';
customElements.define('i-tag', TagElement, {});

import TimeElement from './form/elements/TimeElement.js';
customElements.define('i-time', TimeElement, {});

import TimerangeElement from './form/elements/TimerangeElement.js';
customElements.define('i-timerange', TimerangeElement, {});

import PlurimultilistElement from './form/elements/PlurimultilistElement.js';
customElements.define('i-plurimultilist', PlurimultilistElement, {});

import SelectElement from './form/elements/SelectElement.js';
customElements.define('i-select', SelectElement, {});

import RichtextElement from './form/elements/RichtextElement.js';
customElements.define('i-richtext', RichtextElement, {});

import MultilistElement from './form/elements/MultilistElement.js';
customElements.define('i-multilist', MultilistElement, {});

import MultipleemailElement from './form/elements/MultipleemailElement.js';
customElements.define('i-multipleemail', MultipleemailElement, {});

import MultiselectElement from './form/elements/MultiselectElement.js';
customElements.define('i-multiselect', MultiselectElement, {});

import ColorpickerElement from './form/elements/ColorpickerElement.js';
customElements.define('i-colorpicker', ColorpickerElement, {});
