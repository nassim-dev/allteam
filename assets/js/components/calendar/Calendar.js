//import 'jquery';
import '@fullcalendar/core/vdom';
import { Calendar as Fullcalendar } from '@fullcalendar/core';
//import * as frLocale from '@fullcalendar/core/locales/fr';
import moment from 'moment';
import bootstrapPlugin from '@fullcalendar/bootstrap';
//import rrulePlugin from '@fullcalendar/rrule';
import interactionPlugin from '@fullcalendar/interaction';
import daygridPlugin from '@fullcalendar/daygrid';
import timegridPlugin from '@fullcalendar/timegrid';
import adaptivePlugin from '@fullcalendar/adaptive';
import resourcecommonPlugin from '@fullcalendar/resource-common';
import resourcedaygridPlugin from '@fullcalendar/resource-daygrid';
import resourcetimegridPlugin from '@fullcalendar/resource-timegrid';
import resourcetimelinePlugin from '@fullcalendar/resource-timeline';
import listPlugin from '@fullcalendar/list';
import HtmlElement from '../HtmlElement';
import { KTBlockUI } from '../../vendor/components/blockui';

bootstrapPlugin.themeClasses.bootstrap.prototype.classes = {
  root: 'fc-theme-bootstrap',
  table:
    'table table-rounded table-striped border table-row-dashed table-row-gray-300 ',
  tableCellShaded: 'table-active',
  buttonGroup: 'btn-group',
  button: 'btn   btn-light-primary',
  buttonActive: 'active',
  popover: 'popover',
  popoverHeader: 'popover-header',
  popoverContent: 'popover-body',
};
bootstrapPlugin.themeClasses.bootstrap.prototype.baseIconClass = 'fas';

//To fix https://github.com/fullcalendar/fullcalendar/issues/5544
window.FontAwesome.config.autoReplaceSvg = 'nest';

let commandLoaded = false;
/**
 *
 *  Class Calendar
 *
 * Represent html calendar
 *
 * @category Represent html calendar
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */
export default class Calendar extends HtmlElement {
  constructor() {
    super();

    /**
     * Progress Bar element
     * @property {HTMLElement} progressBar
     */
    this.progressBar = document.querySelector(`#spinner-${this.id}`);

    /**
     * Fullcalendar object
     * @property {Fullcalendar} wrapper
     */
    this.wrapper = document.getElementById(this.id + '_wrapper');
    this.blockUi = new KTBlockUI(this.parentElement);
    if (!this.blockUi.isBlocked()) {
      this.blockUi.block();
    }
    if (!commandLoaded) {
      commandLoaded = true;
      this.command
        .addCommand('DetachCommand', async () => {
          return import('../../command/components/calendar/DetachCommand');
        })
        .addCommand('GotoDateCommand', async () => {
          return import('../../command/components/calendar/GotoDateCommand');
        })
        .addCommand('RenderCommand', async () => {
          return import('../../command/components/calendar/RenderCommand');
        });
    }
  }

  /**
   * Get default config
   *
   * @returns {object};
   */
  getDefaultConfig() {
    return {
      headerToolbar: {
        start: 'prev,next,today',
        center: 'title',
        end: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek',
      },
      plugins: [
        bootstrapPlugin,
        //rrulePlugin,
        interactionPlugin,
        daygridPlugin,
        timegridPlugin,
        adaptivePlugin,
        listPlugin,
        resourcecommonPlugin,
        resourcedaygridPlugin,
        resourcetimegridPlugin,
        resourcetimelinePlugin,
      ],
      //locales: [frLocale],
      locale: 'fr',
      themeSystem: 'bootstrap',
      stickyHeaderDates: true,
      stickyFooterScrollbar: true,
      expandRows: false,
      initialView: 'dayGridMonth',
      schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
      progressiveEventRendering: false,
      navLinks: true,
      selectable: false,
      nowIndicator: true,
      handleWindowResize: true,
      slotDuration: '00:05:00',
      events: () => {
        return this.populateWithAjax;
      },
    };
  }

  /**
   * Initialize component
   *
   * @returns {Promise}
   */
  mount() {
    return super
      .mount()
      .then(() => {
        if (this.config.resources) {
          this.config.resources = () => {
            return this.getResourcesWithAjax;
          };
        }
        if (this.config.localStorage) {
          this.config.event = () => {
            return this.populateWithLocalStorage;
          };
        }
        if (this.config.customHeader || this.config.noDateHeader) {
          this.config.columnHeaderHtml = () => {
            return this.generateHeaderForCell;
          };
        }
      })
      .then(() => {
        this.wrapper = new Fullcalendar(this.wrapper, this.config);
      })
      .then(() => {
        if (this.config.gotoDate) {
          let defaultDate =
            this.app.storaged.get(`${this.id}-defaultDate`) ||
            this.config.gotoDate;
          this.goToDate(defaultDate);
        }
        this.wrapper.on('eventDragStart', () => {
          $('.fc-time-grid-event').popover('disable');
        });
        this.wrapper.on('eventDragStop', () => {
          $('.fc-time-grid-event').popover('enable');
        });
        this.wrapper.render();
      })
      .then(() => {
        if (typeof this.config.listeners != 'undefined') {
          for (let i in this.config.listeners) {
            this.addListener(this.config.listeners[i]);
          }
        }
      })
      .then(() => {
        this.parentElement.classList.toggle('hide');
        if (this.blockUi.isBlocked()) {
          this.blockUi.release();
        }
      });

    return this;
  }
  /**
   * Switch calendar to specific date
   */
  goToDate(date) {
    this.wrapper.gotoDate(!moment.isMoment(date) ? moment(date, 'X') : date);
  }

  /**
   * Destroy component
   */
  unmount() {
    super.unmount();
    if (this.is('mount')) {
      this.wrapper.destroy();
    }
  }

  /**
   * Reload component
   */
  reload() {
    super.reload();
    this.wrapper.refetchEvents();
  }

  /**
   * Create new button on toolbar
   * @param {object} button
   */
  addCustomButon(button) {
    this.config.customButtons = {
      myCustomButton: {
        text: button.name,
        click: () => {
          this.command
            .addCommand(button.commandName, button.commandFile)
            .execute(this, button.commandName, button);
        },
      },
    };
  }

  /**
   * Generate custom cell header
   *
   * @param {*} cell
   *
   * @returns {string}
   */
  generateHeaderForCell(cell) {
    const today = moment();
    if (cell.dayOfYear() === today.dayOfYear()) {
      return '<b>J</b>';
    }
    const i = cell.dayOfYear() - today.dayOfYear();
    return `<i> J${i}</i>`;
  }

  /**
   * Function for fetching resources from ajax
   * @param {object} info
   * @param {function} successCallback
   * @param {function} failureCallback
   */
  getResourcesWithAjax(info, successCallback, failureCallback) {
    let objects = [];

    /**
     * @var {object} parameters
     */

    const parameters = this.endpointDynamicParameters;
    parameters.start = info.start;
    parameters.end = info.end;

    this.config.resources
      .forEach((endpoint) => {
        this.app.ajaxd
          .call(this.app.datad.getDatasource(endpoint), parameters)
          .then((response) => {
            objects = objects.concat(response);
          });
      })
      .then(() => {
        successCallback(objects);
      })
      .catch((error) => {
        failureCallback(error);
      });
  }

  /**
   * Function for fetching datas with localstorage
   * @param {object} info
   * @param {function} successCallback
   * @param {function} failureCallback
   */
  populateWithLocalStorage(info, successCallback, failureCallback) {
    const datas = this.app.storaged.get(this.id);
    datas.map((element) => {
      element.start = moment(element.start, 'DD/MM/YYYY hh:mm');
      element.end = moment(element.end, 'DD/MM/YYYY hh:mm');
    });
    successCallback(datas);
  }

  /**
   * Function for fetching datas with ajax
   * @param {object} info
   * @param {function} successCallback
   * @param {function} failureCallback
   */
  populateWithAjax(info, successCallback, failureCallback) {
    const id = `${this.id}-defaultDate`;
    let objects = [];
    try {
      setTimeout(() => {
        this.app.storaged.set(id, moment(info.startStr).format('X'));
      }, 5000);
    } catch (error) {
      this.app.exceptiond.addException(error, this);
    }

    // todo move in command class
    if (typeof this.config.linkedDateFields !== 'undefined') {
      /**
       * @var {FormElement} fieldStart
       */
      const fieldStart = this.app.componentd.get(
        this.config.linkedDateFields.parameters.start
      );
      /**
       * @var {FormElement} fieldEnd
       */
      const fieldEnd = this.app.componentd.get(
        this.config.linkedDateFields.parameters.end
      );

      if (typeof fieldStart !== 'undefined') {
        fieldStart.setValue(info.startStr);
      }
      if (typeof fieldEnd !== 'undefined') {
        fieldStart.setValue(info.endStr);
      }
    }
    // todo move in command class

    /**
     * @var {object} parameters
     */
    const parameters = this.endpointDynamicParameters;
    parameters.start = info.start;
    parameters.end = info.end;

    this.endpoint
      .forEach((endpoint) => {
        this.app.ajaxd
          .call(this.app.datad.getDatasource(endpoint), parameters)
          .then((response) => {
            objects = objects.concat(response);
          });
      })
      .then(() => {
        successCallback(objects);
      })
      .catch((error) => {
        failureCallback(error);
      });
  }

  /**
   * Add event resize listener if enabled in config
   * @param {object} listener
   *
   */
  addListener(listener) {
    this.wrapper.on(listener.event, (info) => {
      listener.info = info;

      if (typeof listener.callback.addPopup !== 'undefined') {
        this.app.windowd.createPopup(listener.callback.addPopup, this);
        return;
      }
      this.command
        .addCommand(listener.commandFile)
        .execute(this, listener.commandName, listener);
    });
  }
}
