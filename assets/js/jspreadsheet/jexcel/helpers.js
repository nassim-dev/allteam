// Helpers

let helpers = {};

/**
 * Get carret position for one element
 */
helpers.getCaretIndex = function (e) {
  if (this.config.root) {
    let d = this.config.root;
  } else {
    let d = window;
  }
  let pos = 0;
  let s = d.getSelection();
  if (s) {
    if (s.rangeCount !== 0) {
      let r = s.getRangeAt(0);
      let p = r.cloneRange();
      p.selectNodeContents(e);
      p.setEnd(r.endContainer, r.endOffset);
      pos = p.toString().length;
    }
  }
  return pos;
};

/**
 * Invert keys and values
 */
helpers.invert = function (o) {
  let d = [];
  let k = Object.keys(o);
  for (let i = 0; i < k.length; i++) {
    d[o[k[i]]] = k[i];
  }
  return d;
};

/**
 * Get letter based on a number
 *
 * @param integer i
 * @return string letter
 */
helpers.getColumnName = function (i) {
  let letter = '';
  if (i > 701) {
    letter += String.fromCharCode(64 + parseInt(i / 676));
    letter += String.fromCharCode(64 + parseInt((i % 676) / 26));
  } else if (i > 25) {
    letter += String.fromCharCode(64 + parseInt(i / 26));
  }
  letter += String.fromCharCode(65 + (i % 26));

  return letter;
};

/**
 * Get column name from coords
 */
helpers.getColumnNameFromCoords = function (x, y) {
  return helpers.getColumnName(parseInt(x)) + (parseInt(y) + 1);
};

helpers.getCoordsFromColumnName = function (columnName) {
  // Get the letters
  let t = /^[a-zA-Z]+/.exec(columnName);

  if (t) {
    // Base 26 calculation
    let code = 0;
    for (let i = 0; i < t[0].length; i++) {
      code +=
        parseInt(t[0].charCodeAt(i) - 64) * Math.pow(26, t[0].length - 1 - i);
    }
    code--;
    // Make sure jspreadsheet starts on zero
    if (code < 0) {
      code = 0;
    }

    // Number
    let number = parseInt(/[0-9]+$/.exec(columnName)) || null;
    if (number > 0) {
      number--;
    }

    return [code, number];
  }
};

/**
 * Extract json configuration from a TABLE DOM tag
 */
helpers.createFromTable = function () {};

/**
 * Helper injectArray
 */
helpers.injectArray = function (o, idx, arr) {
  return o.slice(0, idx).concat(arr).concat(o.slice(idx));
};

/**
 * Parse CSV string to JS array
 */
helpers.parseCSV = function (str, delimiter) {
  // user-supplied delimeter or default comma
  delimiter = delimiter || ',';

  // Final data
  let col = 0;
  let row = 0;
  let num = 0;
  let data = [[]];
  let limit = 0;
  let flag = null;
  let inside = false;
  let closed = false;

  // Go over all chars
  for (let i = 0; i < str.length; i++) {
    // Create new row
    if (!data[row]) {
      data[row] = [];
    }
    // Create new column
    if (!data[row][col]) {
      data[row][col] = '';
    }

    // Ignore
    if (str[i] == '\r') {
      continue;
    }

    // New row
    if (
      (str[i] == '\n' || str[i] == delimiter) &&
      (inside == false || closed == true || !flag)
    ) {
      // Restart flags
      flag = null;
      inside = false;
      closed = false;

      if (data[row][col][0] == '"') {
        let val = data[row][col].trim();
        if (val[val.length - 1] == '"') {
          data[row][col] = val.substr(1, val.length - 2);
        }
      }

      // Go to the next cell
      if (str[i] == '\n') {
        // New line
        col = 0;
        row++;
      } else {
        // New column
        col++;
        if (col > limit) {
          // Keep the reference of max column
          limit = col;
        }
      }
    } else {
      // Inside quotes
      if (str[i] == '"') {
        inside = !inside;
      }

      if (flag === null) {
        flag = inside;
        if (flag == true) {
          continue;
        }
      } else if (flag === true && !closed) {
        if (str[i] == '"') {
          if (str[i + 1] == '"') {
            inside = true;
            data[row][col] += str[i];
            i++;
          } else {
            closed = true;
          }
          continue;
        }
      }

      data[row][col] += str[i];
    }
  }

  // Make sure a square matrix is generated
  for (let j = 0; j < data.length; j++) {
    for (let i = 0; i <= limit; i++) {
      if (data[j][i] === undefined) {
        data[j][i] = '';
      }
    }
  }

  return data;
};

export default helpers;
