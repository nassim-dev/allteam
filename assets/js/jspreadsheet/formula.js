import met from './met';
import helpers from './jexcel/helpers';

for (let i = 0; i < Object.keys(met).length; i++) {
  let methods = met[Object.keys(met)[i]];
  let keys = Object.keys(methods);
  for (let j = 0; j < keys.length; j++) {
    if (!methods[keys[j]]) {
      window[keys[j]] = function () {
        return keys[j] + 'Not implemented';
      };
    } else if (
      typeof methods[keys[j]] == 'function' ||
      typeof methods[keys[j]] == 'object'
    ) {
      window[keys[j]] = methods[keys[j]];
      window[keys[j]].toString = function () {
        return '#ERROR';
      };

      if (typeof methods[keys[j]] == 'object') {
        let tmp = Object.keys(methods[keys[j]]);
        for (let z = 0; z < tmp.length; z++) {
          window[keys[j]][tmp[z]].toString = function () {
            return '#ERROR';
          };
        }
      }
    } else {
      window[keys[j]] = function () {
        return keys[j] + 'Not implemented';
      };
    }
  }
}

/**
 * Instance execution helpers
 */
let x = null;
let y = null;
let instance = null;

window.TABLE = function () {
  return instance;
};
window.COLUMN = window.COL = function () {
  return parseInt(x) + 1;
};
window.ROW = function () {
  return parseInt(y) + 1;
};
window.CELL = function () {
  return formula.getColumnNameFromCoords(x, y);
};
window.VALUE = function (col, row, processed) {
  return instance.getValueFromCoords(
    parseInt(col) - 1,
    parseInt(row) - 1,
    processed
  );
};
window.THISROWCELL = function (col) {
  return instance.getValueFromCoords(parseInt(col) - 1, parseInt(y));
};

// Secure formula
let secureFormula = function (oldValue, runtime) {
  let newValue = '';
  let inside = 0;

  let special = ['=', '!', '>', '<'];

  for (let i = 0; i < oldValue.length; i++) {
    if (oldValue[i] == '"') {
      if (inside == 0) {
        inside = 1;
      } else {
        inside = 0;
      }
    }

    if (inside == 1) {
      newValue += oldValue[i];
    } else {
      newValue += oldValue[i].toUpperCase();

      if (runtime == true) {
        if (
          i > 0 &&
          oldValue[i] == '=' &&
          special.indexOf(oldValue[i - 1]) == -1 &&
          special.indexOf(oldValue[i + 1]) == -1
        ) {
          newValue += '=';
        }
      }
    }
  }

  // Adapt to JS
  newValue = newValue.replace(/\^/g, '**');
  newValue = newValue.replace(/\<\>/g, '!=');
  newValue = newValue.replace(/\&/g, '+');
  newValue = newValue.replace(/\$/g, '');

  return newValue;
};

// Convert range tokens
let tokensUpdate = function (tokens, e) {
  for (let index = 0; index < tokens.length; index++) {
    let f = formula.getTokensFromRange(tokens[index]);
    e = e.replace(tokens[index], '[' + f.join(',') + ']');
  }
  return e;
};

let formula = function (expression, variables, i, j, obj) {
  // Global helpers
  instance = obj;
  x = i;
  y = j;
  // String
  let s = '';
  let keys = Object.keys(variables);
  if (keys.length) {
    for (let n = 0; n < keys.length; n++) {
      if (keys[n].indexOf('.') == -1 && keys[n].indexOf('!') == -1) {
        s += 'let ' + keys[n] + ' = ' + variables[keys[n]] + ';\n';
      } else {
        s += keys[n] + ' = ' + variables[keys[n]] + ';\n';
      }
    }
  }
  // Remove $
  expression = expression.replace(/\$/g, '');
  // Replace ! per dot
  expression = expression.replace(/\!/g, '.');
  // Adapt to JS
  expression = secureFormula(expression, true);
  // Update range
  let tokens = expression.match(
    /([A-Z]+[0-9]*\.)?(\$?[A-Z]+\$?[0-9]+):(\$?[A-Z]+\$?[0-9]+)?/g
  );
  if (tokens && tokens.length) {
    expression = tokensUpdate(tokens, expression);
  }

  // Calculate
  return new Function(s + '; return ' + expression)();
};

/**
 * Get letter based on a number
 * @param {number} i
 * @return {string}
 */
let getColumnName = function (i) {
  let letter = '';
  if (i > 701) {
    letter += String.fromCharCode(64 + parseInt(i / 676));
    letter += String.fromCharCode(64 + parseInt((i % 676) / 26));
  } else if (i > 25) {
    letter += String.fromCharCode(64 + parseInt(i / 26));
  }
  letter += String.fromCharCode(65 + (i % 26));

  return letter;
};

/**
 * Get column name from coords
 */
formula.getColumnNameFromCoords = function (x, y) {
  return getColumnName(parseInt(x)) + (parseInt(y) + 1);
};

formula.getCoordsFromColumnName = function (columnName) {
  // Get the letters
  let t = /^[a-zA-Z]+/.exec(columnName);

  if (t) {
    // Base 26 calculation
    let code = 0;
    for (let i = 0; i < t[0].length; i++) {
      code +=
        parseInt(t[0].charCodeAt(i) - 64) * Math.pow(26, t[0].length - 1 - i);
    }
    code--;
    // Make sure jspreadsheet starts on zero
    if (code < 0) {
      code = 0;
    }

    // Number
    let number = parseInt(/[0-9]+$/.exec(columnName)) || null;
    if (number > 0) {
      number--;
    }

    return [code, number];
  }
};

formula.getRangeFromTokens = function (tokens) {
  tokens = tokens.filter(function (v) {
    return v != '#REF!';
  });

  let d = '';
  let t = '';
  for (let i = 0; i < tokens.length; i++) {
    if (tokens[i].indexOf('.') >= 0) {
      d = '.';
    } else if (tokens[i].indexOf('!') >= 0) {
      d = '!';
    }
    if (d) {
      t = tokens[i].split(d);
      tokens[i] = t[1];
      t = t[0] + d;
    }
  }

  tokens.sort(function (a, b) {
    let t1 = helpers.getCoordsFromColumnName(a);
    let t2 = helpers.getCoordsFromColumnName(b);
    if (t1[1] > t2[1]) {
      return 1;
    } else if (t1[1] < t2[1]) {
      return -1;
    } else {
      if (t1[0] > t2[0]) {
        return 1;
      } else if (t1[0] < t2[0]) {
        return -1;
      } else {
        return 0;
      }
    }
  });

  if (!tokens.length) {
    return '#REF!';
  } else {
    return t + (tokens[0] + ':' + tokens[tokens.length - 1]);
  }
};

formula.getTokensFromRange = function (range) {
  if (range.indexOf('.') > 0) {
    let t = range.split('.');
    range = t[1];
    t = t[0] + '.';
  } else if (range.indexOf('!') > 0) {
    let t = range.split('!');
    range = t[1];
    t = t[0] + '!';
  } else {
    let t = '';
  }

  range = range.split(':');
  let e1 = formula.getCoordsFromColumnName(range[0]);
  let e2 = formula.getCoordsFromColumnName(range[1]);

  if (e1[0] <= e2[0]) {
    let x1 = e1[0];
    let x2 = e2[0];
  } else {
    let x1 = e2[0];
    let x2 = e1[0];
  }

  if (e1[1] === null && e2[1] == null) {
    let y1 = null;
    let y2 = null;

    let k = Object.keys(vars);
    for (let i = 0; i < k.length; i++) {
      let tmp = formula.getCoordsFromColumnName(k[i]);
      if (tmp[0] === e1[0]) {
        if (y1 === null || tmp[1] < y1) {
          y1 = tmp[1];
        }
      }
      if (tmp[0] === e2[0]) {
        if (y2 === null || tmp[1] > y2) {
          y2 = tmp[1];
        }
      }
    }
  } else {
    if (e1[1] <= e2[1]) {
      let y1 = e1[1];
      let y2 = e2[1];
    } else {
      let y1 = e2[1];
      let y2 = e1[1];
    }
  }

  let f = [];
  for (let j = y1; j <= y2; j++) {
    let line = [];
    for (let i = x1; i <= x2; i++) {
      line.push(t + formula.getColumnNameFromCoords(i, j));
    }
    f.push(line);
  }

  return f;
};

formula.setFormula = function (o) {
  let k = Object.keys(o);
  for (let i = 0; i < k.length; i++) {
    if (typeof o[k[i]] == 'function') {
      window[k[i]] = o[k[i]];
    }
  }
};

export default formula;
