import utils from '../utils';
import error from '../error';
import math from './math';
import misc from './misc';

let stats = {};

let SQRT2PI = 2.5066282746310002;

stats.AVEDEV = null;

stats.AVERAGE = function () {
  let range = utils.numbers(utils.flatten(arguments));
  let n = range.length;
  let sum = 0;
  let count = 0;
  for (let i = 0; i < n; i++) {
    sum += range[i];
    count += 1;
  }
  return sum / count;
};

stats.AVERAGEA = function () {
  let range = utils.flatten(arguments);
  let n = range.length;
  let sum = 0;
  let count = 0;
  for (let i = 0; i < n; i++) {
    let el = range[i];
    if (typeof el === 'number') {
      sum += el;
    }
    if (el === true) {
      sum++;
    }
    if (el !== null) {
      count++;
    }
  }
  return sum / count;
};

stats.AVERAGEIF = function (range, criteria, average_range) {
  average_range = average_range || range;
  range = utils.flatten(range);
  average_range = utils.parseNumberArray(utils.flatten(average_range));
  if (average_range instanceof Error) {
    return average_range;
  }
  let average_count = 0;
  let result = 0;
  for (let i = 0; i < range.length; i++) {
    if (eval(range[i] + criteria)) {
      // jshint ignore:line
      result += average_range[i];
      average_count++;
    }
  }
  return result / average_count;
};

stats.AVERAGEIFS = null;

stats.COUNT = function () {
  return utils.numbers(utils.flatten(arguments)).length;
};

stats.COUNTA = function () {
  let range = utils.flatten(arguments);
  return range.length - stats.COUNTBLANK(range);
};

stats.COUNTIN = function (range, value) {
  let result = 0;
  for (let i = 0; i < range.length; i++) {
    if (range[i] === value) {
      result++;
    }
  }
  return result;
};

stats.COUNTBLANK = function () {
  let range = utils.flatten(arguments);
  let blanks = 0;
  let element;
  for (let i = 0; i < range.length; i++) {
    element = range[i];
    if (element === null || element === '') {
      blanks++;
    }
  }
  return blanks;
};

stats.COUNTIF = function () {
  let args = utils.argsToArray(arguments);
  let criteria = args.pop();
  let range = utils.flatten(args);
  if (!/[<>=!]/.test(criteria)) {
    criteria = '=="' + criteria + '"';
  }
  let matches = 0;
  for (let i = 0; i < range.length; i++) {
    if (typeof range[i] !== 'string') {
      if (eval(range[i] + criteria)) {
        // jshint ignore:line
        matches++;
      }
    } else {
      if (eval('"' + range[i] + '"' + criteria)) {
        // jshint ignore:line
        matches++;
      }
    }
  }
  return matches;
};

stats.COUNTIFS = function () {
  let args = utils.argsToArray(arguments);
  let results = new Array(utils.flatten(args[0]).length);
  for (let i = 0; i < results.length; i++) {
    results[i] = true;
  }
  for (let i = 0; i < args.length; i += 2) {
    let range = utils.flatten(args[i]);
    let criteria = args[i + 1];
    if (!/[<>=!]/.test(criteria)) {
      criteria = '=="' + criteria + '"';
    }
    for (let j = 0; j < range.length; j++) {
      if (typeof range[j] !== 'string') {
        results[j] = results[j] && eval(range[j] + criteria); // jshint ignore:line
      } else {
        results[j] = results[j] && eval('"' + range[j] + '"' + criteria); // jshint ignore:line
      }
    }
  }
  let result = 0;
  for (let i = 0; i < results.length; i++) {
    if (results[i]) {
      result++;
    }
  }
  return result;
};

stats.COUNTUNIQUE = function () {
  return misc.UNIQUE.apply(null, utils.flatten(arguments)).length;
};

stats.FISHER = function (x) {
  x = utils.parseNumber(x);
  if (x instanceof Error) {
    return x;
  }
  return Math.log((1 + x) / (1 - x)) / 2;
};

stats.FISHERINV = function (y) {
  y = utils.parseNumber(y);
  if (y instanceof Error) {
    return y;
  }
  let e2y = Math.exp(2 * y);
  return (e2y - 1) / (e2y + 1);
};

stats.FREQUENCY = function (data, bins) {
  data = utils.parseNumberArray(utils.flatten(data));
  bins = utils.parseNumberArray(utils.flatten(bins));
  if (utils.anyIsError(data, bins)) {
    return error.value;
  }
  let n = data.length;
  let b = bins.length;
  let r = [];
  for (let i = 0; i <= b; i++) {
    r[i] = 0;
    for (let j = 0; j < n; j++) {
      if (i === 0) {
        if (data[j] <= bins[0]) {
          r[0] += 1;
        }
      } else if (i < b) {
        if (data[j] > bins[i - 1] && data[j] <= bins[i]) {
          r[i] += 1;
        }
      } else if (i === b) {
        if (data[j] > bins[b - 1]) {
          r[b] += 1;
        }
      }
    }
  }
  return r;
};

stats.LARGE = function (range, k) {
  range = utils.parseNumberArray(utils.flatten(range));
  k = utils.parseNumber(k);
  if (utils.anyIsError(range, k)) {
    return range;
  }
  return range.sort(function (a, b) {
    return b - a;
  })[k - 1];
};

stats.MAX = function () {
  let range = utils.numbers(utils.flatten(arguments));
  return range.length === 0 ? 0 : Math.max.apply(Math, range);
};

stats.MAXA = function () {
  let range = utils.arrayValuesToNumbers(utils.flatten(arguments));
  return range.length === 0 ? 0 : Math.max.apply(Math, range);
};

stats.MIN = function () {
  let range = utils.numbers(utils.flatten(arguments));
  return range.length === 0 ? 0 : Math.min.apply(Math, range);
};

stats.MINA = function () {
  let range = utils.arrayValuesToNumbers(utils.flatten(arguments));
  return range.length === 0 ? 0 : Math.min.apply(Math, range);
};

stats.MODE = {};

stats.MODE.MULT = function () {
  // Credits: Roönaän
  let range = utils.parseNumberArray(utils.flatten(arguments));
  if (range instanceof Error) {
    return range;
  }
  let n = range.length;
  let count = {};
  let maxItems = [];
  let max = 0;
  let currentItem;

  for (let i = 0; i < n; i++) {
    currentItem = range[i];
    count[currentItem] = count[currentItem] ? count[currentItem] + 1 : 1;
    if (count[currentItem] > max) {
      max = count[currentItem];
      maxItems = [];
    }
    if (count[currentItem] === max) {
      maxItems[maxItems.length] = currentItem;
    }
  }
  return maxItems;
};

stats.MODE.SNGL = function () {
  let range = utils.parseNumberArray(utils.flatten(arguments));
  if (range instanceof Error) {
    return range;
  }
  return stats.MODE.MULT(range).sort(function (a, b) {
    return a - b;
  })[0];
};

stats.PERCENTILE = {};

stats.PERCENTILE.EXC = function (array, k) {
  array = utils.parseNumberArray(utils.flatten(array));
  k = utils.parseNumber(k);
  if (utils.anyIsError(array, k)) {
    return error.value;
  }
  array = array.sort(function (a, b) {
    {
      return a - b;
    }
  });
  let n = array.length;
  if (k < 1 / (n + 1) || k > 1 - 1 / (n + 1)) {
    return error.num;
  }
  let l = k * (n + 1) - 1;
  let fl = Math.floor(l);
  return utils.cleanFloat(
    l === fl ? array[l] : array[fl] + (l - fl) * (array[fl + 1] - array[fl])
  );
};

stats.PERCENTILE.INC = function (array, k) {
  array = utils.parseNumberArray(utils.flatten(array));
  k = utils.parseNumber(k);
  if (utils.anyIsError(array, k)) {
    return error.value;
  }
  array = array.sort(function (a, b) {
    return a - b;
  });
  let n = array.length;
  let l = k * (n - 1);
  let fl = Math.floor(l);
  return utils.cleanFloat(
    l === fl ? array[l] : array[fl] + (l - fl) * (array[fl + 1] - array[fl])
  );
};

stats.PERCENTRANK = {};

stats.PERCENTRANK.EXC = function (array, x, significance) {
  significance = significance === undefined ? 3 : significance;
  array = utils.parseNumberArray(utils.flatten(array));
  x = utils.parseNumber(x);
  significance = utils.parseNumber(significance);
  if (utils.anyIsError(array, x, significance)) {
    return error.value;
  }
  array = array.sort(function (a, b) {
    return a - b;
  });
  let uniques = misc.UNIQUE.apply(null, array);
  let n = array.length;
  let m = uniques.length;
  let power = Math.pow(10, significance);
  let result = 0;
  let match = false;
  let i = 0;
  while (!match && i < m) {
    if (x === uniques[i]) {
      result = (array.indexOf(uniques[i]) + 1) / (n + 1);
      match = true;
    } else if (x >= uniques[i] && (x < uniques[i + 1] || i === m - 1)) {
      result =
        (array.indexOf(uniques[i]) +
          1 +
          (x - uniques[i]) / (uniques[i + 1] - uniques[i])) /
        (n + 1);
      match = true;
    }
    i++;
  }
  return Math.floor(result * power) / power;
};

stats.PERCENTRANK.INC = function (array, x, significance) {
  significance = significance === undefined ? 3 : significance;
  array = utils.parseNumberArray(utils.flatten(array));
  x = utils.parseNumber(x);
  significance = utils.parseNumber(significance);
  if (utils.anyIsError(array, x, significance)) {
    return error.value;
  }
  array = array.sort(function (a, b) {
    return a - b;
  });
  let uniques = misc.UNIQUE.apply(null, array);
  let n = array.length;
  let m = uniques.length;
  let power = Math.pow(10, significance);
  let result = 0;
  let match = false;
  let i = 0;
  while (!match && i < m) {
    if (x === uniques[i]) {
      result = array.indexOf(uniques[i]) / (n - 1);
      match = true;
    } else if (x >= uniques[i] && (x < uniques[i + 1] || i === m - 1)) {
      result =
        (array.indexOf(uniques[i]) +
          (x - uniques[i]) / (uniques[i + 1] - uniques[i])) /
        (n - 1);
      match = true;
    }
    i++;
  }
  return Math.floor(result * power) / power;
};

stats.PERMUT = function (number, number_chosen) {
  number = utils.parseNumber(number);
  number_chosen = utils.parseNumber(number_chosen);
  if (utils.anyIsError(number, number_chosen)) {
    return error.value;
  }
  return math.FACT(number) / math.FACT(number - number_chosen);
};

stats.PERMUTATIONA = function (number, number_chosen) {
  number = utils.parseNumber(number);
  number_chosen = utils.parseNumber(number_chosen);
  if (utils.anyIsError(number, number_chosen)) {
    return error.value;
  }
  return Math.pow(number, number_chosen);
};

stats.PHI = function (x) {
  x = utils.parseNumber(x);
  if (x instanceof Error) {
    return error.value;
  }
  return Math.exp(-0.5 * x * x) / SQRT2PI;
};

stats.PROB = function (range, probability, lower, upper) {
  if (lower === undefined) {
    return 0;
  }
  upper = upper === undefined ? lower : upper;

  range = utils.parseNumberArray(utils.flatten(range));
  probability = utils.parseNumberArray(utils.flatten(probability));
  lower = utils.parseNumber(lower);
  upper = utils.parseNumber(upper);
  if (utils.anyIsError(range, probability, lower, upper)) {
    return error.value;
  }

  if (lower === upper) {
    return range.indexOf(lower) >= 0 ? probability[range.indexOf(lower)] : 0;
  }

  let sorted = range.sort(function (a, b) {
    return a - b;
  });
  let n = sorted.length;
  let result = 0;
  for (let i = 0; i < n; i++) {
    if (sorted[i] >= lower && sorted[i] <= upper) {
      result += probability[range.indexOf(sorted[i])];
    }
  }
  return result;
};

stats.QUARTILE = {};

stats.QUARTILE.EXC = function (range, quart) {
  range = utils.parseNumberArray(utils.flatten(range));
  quart = utils.parseNumber(quart);
  if (utils.anyIsError(range, quart)) {
    return error.value;
  }
  switch (quart) {
    case 1:
      return stats.PERCENTILE.EXC(range, 0.25);
    case 2:
      return stats.PERCENTILE.EXC(range, 0.5);
    case 3:
      return stats.PERCENTILE.EXC(range, 0.75);
    default:
      return error.num;
  }
};

stats.QUARTILE.INC = function (range, quart) {
  range = utils.parseNumberArray(utils.flatten(range));
  quart = utils.parseNumber(quart);
  if (utils.anyIsError(range, quart)) {
    return error.value;
  }
  switch (quart) {
    case 1:
      return stats.PERCENTILE.INC(range, 0.25);
    case 2:
      return stats.PERCENTILE.INC(range, 0.5);
    case 3:
      return stats.PERCENTILE.INC(range, 0.75);
    default:
      return error.num;
  }
};

stats.RANK = {};

stats.RANK.AVG = function (number, range, order) {
  number = utils.parseNumber(number);
  range = utils.parseNumberArray(utils.flatten(range));
  if (utils.anyIsError(number, range)) {
    return error.value;
  }
  range = utils.flatten(range);
  order = order || false;
  let sort = order
    ? function (a, b) {
        return a - b;
      }
    : function (a, b) {
        return b - a;
      };
  range = range.sort(sort);

  let length = range.length;
  let count = 0;
  for (let i = 0; i < length; i++) {
    if (range[i] === number) {
      count++;
    }
  }

  return count > 1
    ? (2 * range.indexOf(number) + count + 1) / 2
    : range.indexOf(number) + 1;
};

stats.RANK.EQ = function (number, range, order) {
  number = utils.parseNumber(number);
  range = utils.parseNumberArray(utils.flatten(range));
  if (utils.anyIsError(number, range)) {
    return error.value;
  }
  order = order || false;
  let sort = order
    ? function (a, b) {
        return a - b;
      }
    : function (a, b) {
        return b - a;
      };
  range = range.sort(sort);
  return range.indexOf(number) + 1;
};

stats.RSQ = function (data_x, data_y) {
  // no need to flatten here, PEARSON will take care of that
  data_x = utils.parseNumberArray(utils.flatten(data_x));
  data_y = utils.parseNumberArray(utils.flatten(data_y));
  if (utils.anyIsError(data_x, data_y)) {
    return error.value;
  }
  return Math.pow(stats.PEARSON(data_x, data_y), 2);
};

stats.SMALL = function (range, k) {
  range = utils.parseNumberArray(utils.flatten(range));
  k = utils.parseNumber(k);
  if (utils.anyIsError(range, k)) {
    return range;
  }
  return range.sort(function (a, b) {
    return a - b;
  })[k - 1];
};

stats.STANDARDIZE = function (x, mean, sd) {
  x = utils.parseNumber(x);
  mean = utils.parseNumber(mean);
  sd = utils.parseNumber(sd);
  if (utils.anyIsError(x, mean, sd)) {
    return error.value;
  }
  return (x - mean) / sd;
};

stats.STDEV = {};

stats.STDEV.P = function () {
  let v = stats.VAR.P.apply(this, arguments);
  return Math.sqrt(v);
};

stats.STDEV.S = function () {
  let v = stats.VAR.S.apply(this, arguments);
  return Math.sqrt(v);
};

stats.STDEVA = function () {
  let v = stats.VARA.apply(this, arguments);
  return Math.sqrt(v);
};

stats.STDEVPA = function () {
  let v = stats.VARPA.apply(this, arguments);
  return Math.sqrt(v);
};

stats.VAR = {};

stats.VAR.P = function () {
  let range = utils.numbers(utils.flatten(arguments));
  let n = range.length;
  let sigma = 0;
  let mean = stats.AVERAGE(range);
  for (let i = 0; i < n; i++) {
    sigma += Math.pow(range[i] - mean, 2);
  }
  return sigma / n;
};

stats.VAR.S = function () {
  let range = utils.numbers(utils.flatten(arguments));
  let n = range.length;
  let sigma = 0;
  let mean = stats.AVERAGE(range);
  for (let i = 0; i < n; i++) {
    sigma += Math.pow(range[i] - mean, 2);
  }
  return sigma / (n - 1);
};

stats.VARA = function () {
  let range = utils.flatten(arguments);
  let n = range.length;
  let sigma = 0;
  let count = 0;
  let mean = stats.AVERAGEA(range);
  for (let i = 0; i < n; i++) {
    let el = range[i];
    if (typeof el === 'number') {
      sigma += Math.pow(el - mean, 2);
    } else if (el === true) {
      sigma += Math.pow(1 - mean, 2);
    } else {
      sigma += Math.pow(0 - mean, 2);
    }

    if (el !== null) {
      count++;
    }
  }
  return sigma / (count - 1);
};

stats.VARPA = function () {
  let range = utils.flatten(arguments);
  let n = range.length;
  let sigma = 0;
  let count = 0;
  let mean = stats.AVERAGEA(range);
  for (let i = 0; i < n; i++) {
    let el = range[i];
    if (typeof el === 'number') {
      sigma += Math.pow(el - mean, 2);
    } else if (el === true) {
      sigma += Math.pow(1 - mean, 2);
    } else {
      sigma += Math.pow(0 - mean, 2);
    }

    if (el !== null) {
      count++;
    }
  }
  return sigma / count;
};

stats.WEIBULL = {};

stats.WEIBULL.DIST = function (x, alpha, beta, cumulative) {
  x = utils.parseNumber(x);
  alpha = utils.parseNumber(alpha);
  beta = utils.parseNumber(beta);
  if (utils.anyIsError(x, alpha, beta)) {
    return error.value;
  }
  return cumulative
    ? 1 - Math.exp(-Math.pow(x / beta, alpha))
    : (Math.pow(x, alpha - 1) * Math.exp(-Math.pow(x / beta, alpha)) * alpha) /
        Math.pow(beta, alpha);
};

stats.Z = {};

stats.Z.TEST = function (range, x, sd) {
  range = utils.parseNumberArray(utils.flatten(range));
  x = utils.parseNumber(x);
  if (utils.anyIsError(range, x)) {
    return error.value;
  }

  sd = sd || stats.STDEV.S(range);
  let n = range.length;
  return (
    1 -
    stats.NORM.S.DIST((stats.AVERAGE(range) - x) / (sd / Math.sqrt(n)), true)
  );
};

export default stats;
