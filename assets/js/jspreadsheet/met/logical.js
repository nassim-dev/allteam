import utils from '../utils';
import error from '../error';
import information from './information';

let logical = {};

logical.AND = function () {
  let args = utils.flatten(arguments);
  let result = true;
  for (let i = 0; i < args.length; i++) {
    if (!args[i]) {
      result = false;
    }
  }
  return result;
};

logical.CHOOSE = function () {
  if (arguments.length < 2) {
    return error.na;
  }

  let index = arguments[0];
  if (index < 1 || index > 254) {
    return error.value;
  }

  if (arguments.length < index + 1) {
    return error.value;
  }

  return arguments[index];
};

logical.FALSE = function () {
  return false;
};

logical.IF = function (test, then_value, otherwise_value) {
  return test ? then_value : otherwise_value;
};

logical.IFERROR = function (value, valueIfError) {
  if (information.ISERROR(value)) {
    return valueIfError;
  }
  return value;
};

logical.IFNA = function (value, value_if_na) {
  return value === error.na ? value_if_na : value;
};

logical.NOT = function (state) {
  return !state;
};

logical.OR = function () {
  let args = utils.flatten(arguments);
  let result = false;
  for (let i = 0; i < args.length; i++) {
    if (args[i]) {
      result = true;
    }
  }
  return result;
};

logical.TRUE = function () {
  return true;
};

logical.XOR = function () {
  let args = utils.flatten(arguments);
  let result = 0;
  for (let i = 0; i < args.length; i++) {
    if (args[i]) {
      result++;
    }
  }
  return Math.floor(Math.abs(result)) & 1 ? true : false;
};

logical.SWITCH = function () {
  let result;
  if (arguments.length > 0) {
    let targetValue = arguments[0];
    let argc = arguments.length - 1;
    let switchCount = Math.floor(argc / 2);
    let switchSatisfied = false;
    let defaultClause = argc % 2 === 0 ? null : arguments[arguments.length - 1];

    if (switchCount) {
      for (let index = 0; index < switchCount; index++) {
        if (targetValue === arguments[index * 2 + 1]) {
          result = arguments[index * 2 + 2];
          switchSatisfied = true;
          break;
        }
      }
    }

    if (!switchSatisfied && defaultClause) {
      result = defaultClause;
    }
  }

  return result;
};

export default logical;
