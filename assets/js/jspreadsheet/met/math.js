import utils from '../utils';
import error from '../error';
import information from './information';

let math = {};

math.ABS = function (number) {
  number = utils.parseNumber(number);
  if (number instanceof Error) {
    return number;
  }
  return Math.abs(utils.parseNumber(number));
};

math.ACOS = function (number) {
  number = utils.parseNumber(number);
  if (number instanceof Error) {
    return number;
  }
  return Math.acos(number);
};

math.ACOSH = function (number) {
  number = utils.parseNumber(number);
  if (number instanceof Error) {
    return number;
  }
  return Math.log(number + Math.sqrt(number * number - 1));
};

math.ACOT = function (number) {
  number = utils.parseNumber(number);
  if (number instanceof Error) {
    return number;
  }
  return Math.atan(1 / number);
};

math.ACOTH = function (number) {
  number = utils.parseNumber(number);
  if (number instanceof Error) {
    return number;
  }
  return 0.5 * Math.log((number + 1) / (number - 1));
};

math.AGGREGATE = null;

math.ARABIC = function (text) {
  // Credits: Rafa? Kukawski
  if (
    !/^M*(?:D?C{0,3}|C[MD])(?:L?X{0,3}|X[CL])(?:V?I{0,3}|I[XV])$/.test(text)
  ) {
    return error.value;
  }
  let r = 0;
  text.replace(/[MDLV]|C[MD]?|X[CL]?|I[XV]?/g, function (i) {
    r += {
      M: 1000,
      CM: 900,
      D: 500,
      CD: 400,
      C: 100,
      XC: 90,
      L: 50,
      XL: 40,
      X: 10,
      IX: 9,
      V: 5,
      IV: 4,
      I: 1,
    }[i];
  });
  return r;
};

math.ASIN = function (number) {
  number = utils.parseNumber(number);
  if (number instanceof Error) {
    return number;
  }
  return Math.asin(number);
};

math.ASINH = function (number) {
  number = utils.parseNumber(number);
  if (number instanceof Error) {
    return number;
  }
  return Math.log(number + Math.sqrt(number * number + 1));
};

math.ATAN = function (number) {
  number = utils.parseNumber(number);
  if (number instanceof Error) {
    return number;
  }
  return Math.atan(number);
};

math.ATAN2 = function (number_x, number_y) {
  number_x = utils.parseNumber(number_x);
  number_y = utils.parseNumber(number_y);
  if (utils.anyIsError(number_x, number_y)) {
    return error.value;
  }
  return Math.atan2(number_x, number_y);
};

math.ATANH = function (number) {
  number = utils.parseNumber(number);
  if (number instanceof Error) {
    return number;
  }
  return Math.log((1 + number) / (1 - number)) / 2;
};

math.BASE = function (number, radix, min_length) {
  min_length = min_length || 0;

  number = utils.parseNumber(number);
  radix = utils.parseNumber(radix);
  min_length = utils.parseNumber(min_length);
  if (utils.anyIsError(number, radix, min_length)) {
    return error.value;
  }
  min_length = min_length === undefined ? 0 : min_length;
  let result = number.toString(radix);
  return (
    new Array(Math.max(min_length + 1 - result.length, 0)).join('0') + result
  );
};

math.CEILING = function (number, significance, mode) {
  significance = significance === undefined ? 1 : significance;
  mode = mode === undefined ? 0 : mode;

  number = utils.parseNumber(number);
  significance = utils.parseNumber(significance);
  mode = utils.parseNumber(mode);
  if (utils.anyIsError(number, significance, mode)) {
    return error.value;
  }
  if (significance === 0) {
    return 0;
  }

  significance = Math.abs(significance);
  if (number >= 0) {
    return Math.ceil(number / significance) * significance;
  } else {
    if (mode === 0) {
      return -1 * Math.floor(Math.abs(number) / significance) * significance;
    } else {
      return -1 * Math.ceil(Math.abs(number) / significance) * significance;
    }
  }
};

math.CEILING.MATH = math.CEILING;

math.CEILING.PRECISE = math.CEILING;

math.COMBIN = function (number, number_chosen) {
  number = utils.parseNumber(number);
  number_chosen = utils.parseNumber(number_chosen);
  if (utils.anyIsError(number, number_chosen)) {
    return error.value;
  }
  return (
    math.FACT(number) /
    (math.FACT(number_chosen) * math.FACT(number - number_chosen))
  );
};

math.COMBINA = function (number, number_chosen) {
  number = utils.parseNumber(number);
  number_chosen = utils.parseNumber(number_chosen);
  if (utils.anyIsError(number, number_chosen)) {
    return error.value;
  }
  return number === 0 && number_chosen === 0
    ? 1
    : math.COMBIN(number + number_chosen - 1, number - 1);
};

math.COS = function (number) {
  number = utils.parseNumber(number);
  if (number instanceof Error) {
    return number;
  }
  return Math.cos(number);
};

math.COSH = function (number) {
  number = utils.parseNumber(number);
  if (number instanceof Error) {
    return number;
  }
  return (Math.exp(number) + Math.exp(-number)) / 2;
};

math.COT = function (number) {
  number = utils.parseNumber(number);
  if (number instanceof Error) {
    return number;
  }
  return 1 / Math.tan(number);
};

math.COTH = function (number) {
  number = utils.parseNumber(number);
  if (number instanceof Error) {
    return number;
  }
  let e2 = Math.exp(2 * number);
  return (e2 + 1) / (e2 - 1);
};

math.CSC = function (number) {
  number = utils.parseNumber(number);
  if (number instanceof Error) {
    return number;
  }
  return 1 / Math.sin(number);
};

math.CSCH = function (number) {
  number = utils.parseNumber(number);
  if (number instanceof Error) {
    return number;
  }
  return 2 / (Math.exp(number) - Math.exp(-number));
};

math.DECIMAL = function (number, radix) {
  if (arguments.length < 1) {
    return error.value;
  }

  return parseInt(number, radix);
};

math.DEGREES = function (number) {
  number = utils.parseNumber(number);
  if (number instanceof Error) {
    return number;
  }
  return (number * 180) / Math.PI;
};

math.EVEN = function (number) {
  number = utils.parseNumber(number);
  if (number instanceof Error) {
    return number;
  }
  return math.CEILING(number, -2, -1);
};

math.EXP = Math.exp;

let MEMOIZED_FACT = [];
math.FACT = function (number) {
  number = utils.parseNumber(number);
  if (number instanceof Error) {
    return number;
  }
  let n = Math.floor(number);
  if (n === 0 || n === 1) {
    return 1;
  } else if (MEMOIZED_FACT[n] > 0) {
    return MEMOIZED_FACT[n];
  } else {
    MEMOIZED_FACT[n] = math.FACT(n - 1) * n;
    return MEMOIZED_FACT[n];
  }
};

math.FACTDOUBLE = function (number) {
  number = utils.parseNumber(number);
  if (number instanceof Error) {
    return number;
  }
  let n = Math.floor(number);
  if (n <= 0) {
    return 1;
  } else {
    return n * math.FACTDOUBLE(n - 2);
  }
};

math.FLOOR = function (number, significance, mode) {
  significance = significance === undefined ? 1 : significance;
  mode = mode === undefined ? 0 : mode;

  number = utils.parseNumber(number);
  significance = utils.parseNumber(significance);
  mode = utils.parseNumber(mode);
  if (utils.anyIsError(number, significance, mode)) {
    return error.value;
  }
  if (significance === 0) {
    return 0;
  }

  significance = Math.abs(significance);
  if (number >= 0) {
    return Math.floor(number / significance) * significance;
  } else {
    if (mode === 0) {
      return -1 * Math.ceil(Math.abs(number) / significance) * significance;
    } else {
      return -1 * Math.floor(Math.abs(number) / significance) * significance;
    }
  }
};

math.FLOOR.MATH = math.FLOOR;

math.GCD = null;

math.INT = function (number) {
  number = utils.parseNumber(number);
  if (number instanceof Error) {
    return number;
  }
  return Math.floor(number);
};

math.LCM = function () {
  // Credits: Jonas Raoni Soares Silva
  let o = utils.parseNumberArray(utils.flatten(arguments));
  if (o instanceof Error) {
    return o;
  }
  for (let i, j, n, d, r = 1; (n = o.pop()) !== undefined; ) {
    while (n > 1) {
      if (n % 2) {
        for (i = 3, j = Math.floor(Math.sqrt(n)); i <= j && n % i; i += 2) {
          // empty
        }
        d = i <= j ? i : n;
      } else {
        d = 2;
      }
      for (
        n /= d, r *= d, i = o.length;
        i;
        o[--i] % d === 0 && (o[i] /= d) === 1 && o.splice(i, 1)
      ) {
        // empty
      }
    }
  }
  return r;
};

math.LN = function (number) {
  number = utils.parseNumber(number);
  if (number instanceof Error) {
    return number;
  }
  return Math.log(number);
};

math.LOG = function (number, base) {
  number = utils.parseNumber(number);
  base = base === undefined ? 10 : utils.parseNumber(base);

  if (utils.anyIsError(number, base)) {
    return error.value;
  }

  return Math.log(number) / Math.log(base);
};

math.LOG10 = function (number) {
  number = utils.parseNumber(number);
  if (number instanceof Error) {
    return number;
  }
  return Math.log(number) / Math.log(10);
};

math.MDETERM = null;

math.MINVERSE = null;

math.MMULT = null;

math.MOD = function (dividend, divisor) {
  dividend = utils.parseNumber(dividend);
  divisor = utils.parseNumber(divisor);
  if (utils.anyIsError(dividend, divisor)) {
    return error.value;
  }
  if (divisor === 0) {
    return error.div0;
  }
  let modulus = Math.abs(dividend % divisor);
  return divisor > 0 ? modulus : -modulus;
};

math.MROUND = function (number, multiple) {
  number = utils.parseNumber(number);
  multiple = utils.parseNumber(multiple);
  if (utils.anyIsError(number, multiple)) {
    return error.value;
  }
  if (number * multiple < 0) {
    return error.num;
  }

  return Math.round(number / multiple) * multiple;
};

math.MULTINOMIAL = function () {
  let args = utils.parseNumberArray(utils.flatten(arguments));
  if (args instanceof Error) {
    return args;
  }
  let sum = 0;
  let divisor = 1;
  for (let i = 0; i < args.length; i++) {
    sum += args[i];
    divisor *= math.FACT(args[i]);
  }
  return math.FACT(sum) / divisor;
};

math.MUNIT = null;

math.ODD = function (number) {
  number = utils.parseNumber(number);
  if (number instanceof Error) {
    return number;
  }
  let temp = Math.ceil(Math.abs(number));
  temp = temp & 1 ? temp : temp + 1;
  return number > 0 ? temp : -temp;
};

math.PI = function () {
  return Math.PI;
};

math.POWER = function (number, power) {
  number = utils.parseNumber(number);
  power = utils.parseNumber(power);
  if (utils.anyIsError(number, power)) {
    return error.value;
  }
  let result = Math.pow(number, power);
  if (isNaN(result)) {
    return error.num;
  }

  return result;
};

math.PRODUCT = function () {
  let args = utils.parseNumberArray(utils.flatten(arguments));
  if (args instanceof Error) {
    return args;
  }
  let result = 1;
  for (let i = 0; i < args.length; i++) {
    result *= args[i];
  }
  return result;
};

math.QUOTIENT = function (numerator, denominator) {
  numerator = utils.parseNumber(numerator);
  denominator = utils.parseNumber(denominator);
  if (utils.anyIsError(numerator, denominator)) {
    return error.value;
  }
  return parseInt(numerator / denominator, 10);
};

math.RADIANS = function (number) {
  number = utils.parseNumber(number);
  if (number instanceof Error) {
    return number;
  }
  return (number * Math.PI) / 180;
};

math.RAND = function () {
  return Math.random();
};

math.RANDBETWEEN = function (bottom, top) {
  bottom = utils.parseNumber(bottom);
  top = utils.parseNumber(top);
  if (utils.anyIsError(bottom, top)) {
    return error.value;
  }
  // Creative Commons Attribution 3.0 License
  // Copyright (c) 2012 eqcode
  return bottom + Math.ceil((top - bottom + 1) * Math.random()) - 1;
};

math.ROMAN = null;

math.ROUND = function (number, digits) {
  number = utils.parseNumber(number);
  digits = utils.parseNumber(digits);
  if (utils.anyIsError(number, digits)) {
    return error.value;
  }
  return Math.round(number * Math.pow(10, digits)) / Math.pow(10, digits);
};

math.ROUNDDOWN = function (number, digits) {
  number = utils.parseNumber(number);
  digits = utils.parseNumber(digits);
  if (utils.anyIsError(number, digits)) {
    return error.value;
  }
  let sign = number > 0 ? 1 : -1;
  return (
    (sign * Math.floor(Math.abs(number) * Math.pow(10, digits))) /
    Math.pow(10, digits)
  );
};

math.ROUNDUP = function (number, digits) {
  number = utils.parseNumber(number);
  digits = utils.parseNumber(digits);
  if (utils.anyIsError(number, digits)) {
    return error.value;
  }
  let sign = number > 0 ? 1 : -1;
  return (
    (sign * Math.ceil(Math.abs(number) * Math.pow(10, digits))) /
    Math.pow(10, digits)
  );
};

math.SEC = function (number) {
  number = utils.parseNumber(number);
  if (number instanceof Error) {
    return number;
  }
  return 1 / Math.cos(number);
};

math.SECH = function (number) {
  number = utils.parseNumber(number);
  if (number instanceof Error) {
    return number;
  }
  return 2 / (Math.exp(number) + Math.exp(-number));
};

math.SERIESSUM = function (x, n, m, coefficients) {
  x = utils.parseNumber(x);
  n = utils.parseNumber(n);
  m = utils.parseNumber(m);
  coefficients = utils.parseNumberArray(coefficients);
  if (utils.anyIsError(x, n, m, coefficients)) {
    return error.value;
  }
  let result = coefficients[0] * Math.pow(x, n);
  for (let i = 1; i < coefficients.length; i++) {
    result += coefficients[i] * Math.pow(x, n + i * m);
  }
  return result;
};

math.SIGN = function (number) {
  number = utils.parseNumber(number);
  if (number instanceof Error) {
    return number;
  }
  if (number < 0) {
    return -1;
  } else if (number === 0) {
    return 0;
  } else {
    return 1;
  }
};

math.SIN = function (number) {
  number = utils.parseNumber(number);
  if (number instanceof Error) {
    return number;
  }
  return Math.sin(number);
};

math.SINH = function (number) {
  number = utils.parseNumber(number);
  if (number instanceof Error) {
    return number;
  }
  return (Math.exp(number) - Math.exp(-number)) / 2;
};

math.SQRT = function (number) {
  number = utils.parseNumber(number);
  if (number instanceof Error) {
    return number;
  }
  if (number < 0) {
    return error.num;
  }
  return Math.sqrt(number);
};

math.SQRTPI = function (number) {
  number = utils.parseNumber(number);
  if (number instanceof Error) {
    return number;
  }
  return Math.sqrt(number * Math.PI);
};

math.SUBTOTAL = null;

math.ADD = function (num1, num2) {
  if (arguments.length !== 2) {
    return error.na;
  }

  num1 = utils.parseNumber(num1);
  num2 = utils.parseNumber(num2);
  if (utils.anyIsError(num1, num2)) {
    return error.value;
  }

  return num1 + num2;
};

math.MINUS = function (num1, num2) {
  if (arguments.length !== 2) {
    return error.na;
  }

  num1 = utils.parseNumber(num1);
  num2 = utils.parseNumber(num2);
  if (utils.anyIsError(num1, num2)) {
    return error.value;
  }

  return num1 - num2;
};

math.DIVIDE = function (dividend, divisor) {
  if (arguments.length !== 2) {
    return error.na;
  }

  dividend = utils.parseNumber(dividend);
  divisor = utils.parseNumber(divisor);
  if (utils.anyIsError(dividend, divisor)) {
    return error.value;
  }

  if (divisor === 0) {
    return error.div0;
  }

  return dividend / divisor;
};

math.MULTIPLY = function (factor1, factor2) {
  if (arguments.length !== 2) {
    return error.na;
  }

  factor1 = utils.parseNumber(factor1);
  factor2 = utils.parseNumber(factor2);
  if (utils.anyIsError(factor1, factor2)) {
    return error.value;
  }

  return factor1 * factor2;
};

math.GTE = function (num1, num2) {
  if (arguments.length !== 2) {
    return error.na;
  }

  num1 = utils.parseNumber(num1);
  num2 = utils.parseNumber(num2);
  if (utils.anyIsError(num1, num2)) {
    return error.error;
  }

  return num1 >= num2;
};

math.LT = function (num1, num2) {
  if (arguments.length !== 2) {
    return error.na;
  }

  num1 = utils.parseNumber(num1);
  num2 = utils.parseNumber(num2);
  if (utils.anyIsError(num1, num2)) {
    return error.error;
  }

  return num1 < num2;
};

math.LTE = function (num1, num2) {
  if (arguments.length !== 2) {
    return error.na;
  }

  num1 = utils.parseNumber(num1);
  num2 = utils.parseNumber(num2);
  if (utils.anyIsError(num1, num2)) {
    return error.error;
  }

  return num1 <= num2;
};

math.EQ = function (value1, value2) {
  if (arguments.length !== 2) {
    return error.na;
  }

  return value1 === value2;
};

math.NE = function (value1, value2) {
  if (arguments.length !== 2) {
    return error.na;
  }

  return value1 !== value2;
};

math.POW = function (base, exponent) {
  if (arguments.length !== 2) {
    return error.na;
  }

  base = utils.parseNumber(base);
  exponent = utils.parseNumber(exponent);
  if (utils.anyIsError(base, exponent)) {
    return error.error;
  }

  return math.POWER(base, exponent);
};

math.SUM = function () {
  let result = 0;
  let argsKeys = Object.keys(arguments);
  for (let i = 0; i < argsKeys.length; ++i) {
    let elt = arguments[argsKeys[i]];
    if (typeof elt === 'number') {
      result += elt;
    } else if (typeof elt === 'string') {
      let parsed = parseFloat(elt);
      !isNaN(parsed) && (result += parsed);
    } else if (Array.isArray(elt)) {
      result += math.SUM.apply(null, elt);
    }
  }
  return result;
};

math.SUMIF = function () {
  let args = utils.argsToArray(arguments);
  let criteria = args.pop();
  let range = utils.parseNumberArray(utils.flatten(args));
  if (range instanceof Error) {
    return range;
  }
  let result = 0;
  for (let i = 0; i < range.length; i++) {
    result += eval(range[i] + criteria) ? range[i] : 0; // jshint ignore:line
  }
  return result;
};

math.SUMIFS = function () {
  let args = utils.argsToArray(arguments);
  let range = utils.parseNumberArray(utils.flatten(args.shift()));
  if (range instanceof Error) {
    return range;
  }
  let criteria = args;

  let n_range_elements = range.length;
  let n_criterias = criteria.length;

  let result = 0;

  for (let i = 0; i < n_range_elements; i++) {
    let el = range[i];
    let condition = '';
    for (let c = 0; c < n_criterias; c += 2) {
      if (isNaN(criteria[c][i])) {
        condition += '"' + criteria[c][i] + '"' + criteria[c + 1];
      } else {
        condition += criteria[c][i] + criteria[c + 1];
      }
      if (c !== n_criterias - 1) {
        condition += ' && ';
      }
    }
    condition = condition.slice(0, -4);
    if (eval(condition)) {
      // jshint ignore:line
      result += el;
    }
  }
  return result;
};

math.SUMPRODUCT = null;

math.SUMSQ = function () {
  let numbers = utils.parseNumberArray(utils.flatten(arguments));
  if (numbers instanceof Error) {
    return numbers;
  }
  let result = 0;
  let length = numbers.length;
  for (let i = 0; i < length; i++) {
    result += information.ISNUMBER(numbers[i]) ? numbers[i] * numbers[i] : 0;
  }
  return result;
};

math.SUMX2MY2 = function (array_x, array_y) {
  array_x = utils.parseNumberArray(utils.flatten(array_x));
  array_y = utils.parseNumberArray(utils.flatten(array_y));
  if (utils.anyIsError(array_x, array_y)) {
    return error.value;
  }
  let result = 0;
  for (let i = 0; i < array_x.length; i++) {
    result += array_x[i] * array_x[i] - array_y[i] * array_y[i];
  }
  return result;
};

math.SUMX2PY2 = function (array_x, array_y) {
  array_x = utils.parseNumberArray(utils.flatten(array_x));
  array_y = utils.parseNumberArray(utils.flatten(array_y));
  if (utils.anyIsError(array_x, array_y)) {
    return error.value;
  }
  let result = 0;
  array_x = utils.parseNumberArray(utils.flatten(array_x));
  array_y = utils.parseNumberArray(utils.flatten(array_y));
  for (let i = 0; i < array_x.length; i++) {
    result += array_x[i] * array_x[i] + array_y[i] * array_y[i];
  }
  return result;
};

math.SUMXMY2 = function (array_x, array_y) {
  array_x = utils.parseNumberArray(utils.flatten(array_x));
  array_y = utils.parseNumberArray(utils.flatten(array_y));
  if (utils.anyIsError(array_x, array_y)) {
    return error.value;
  }
  let result = 0;
  array_x = utils.flatten(array_x);
  array_y = utils.flatten(array_y);
  for (let i = 0; i < array_x.length; i++) {
    result += Math.pow(array_x[i] - array_y[i], 2);
  }
  return result;
};

math.TAN = function (number) {
  number = utils.parseNumber(number);
  if (number instanceof Error) {
    return number;
  }
  return Math.tan(number);
};

math.TANH = function (number) {
  number = utils.parseNumber(number);
  if (number instanceof Error) {
    return number;
  }
  let e2 = Math.exp(2 * number);
  return (e2 - 1) / (e2 + 1);
};

math.TRUNC = function (number, digits) {
  digits = digits === undefined ? 0 : digits;
  number = utils.parseNumber(number);
  digits = utils.parseNumber(digits);
  if (utils.anyIsError(number, digits)) {
    return error.value;
  }
  let sign = number > 0 ? 1 : -1;
  return (
    (sign * Math.floor(Math.abs(number) * Math.pow(10, digits))) /
    Math.pow(10, digits)
  );
};

export default math;
