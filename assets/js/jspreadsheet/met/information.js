import error from '../error';

let information = {};
information.CELL = null;

information.ERROR = {};
information.ERROR.TYPE = function (error_val) {
  switch (error_val) {
    case error.nil:
      return 1;
    case error.div0:
      return 2;
    case error.value:
      return 3;
    case error.ref:
      return 4;
    case error.name:
      return 5;
    case error.num:
      return 6;
    case error.na:
      return 7;
    case error.data:
      return 8;
  }
  return error.na;
};

information.INFO = null;

information.ISBLANK = function (value) {
  return value === null;
};

information.ISBINARY = function (number) {
  return /^[01]{1,10}$/.test(number);
};

information.ISERR = function (value) {
  return (
    [
      error.value,
      error.ref,
      error.div0,
      error.num,
      error.name,
      error.nil,
    ].indexOf(value) >= 0 ||
    (typeof value === 'number' && (isNaN(value) || !isFinite(value)))
  );
};

information.ISERROR = function (value) {
  return information.ISERR(value) || value === error.na;
};

information.ISEVEN = function (number) {
  return Math.floor(Math.abs(number)) & 1 ? false : true;
};

// TODO
information.ISFORMULA = null;

information.ISLOGICAL = function (value) {
  return value === true || value === false;
};

information.ISNA = function (value) {
  return value === error.na;
};

information.ISNONTEXT = function (value) {
  return typeof value !== 'string';
};

information.ISNUMBER = function (value) {
  return typeof value === 'number' && !isNaN(value) && isFinite(value);
};

information.ISODD = function (number) {
  return Math.floor(Math.abs(number)) & 1 ? true : false;
};

information.ISREF = null;

information.ISTEXT = function (value) {
  return typeof value === 'string';
};

information.N = function (value) {
  if (this.ISNUMBER(value)) {
    return value;
  }
  if (value instanceof Date) {
    return value.getTime();
  }
  if (value === true) {
    return 1;
  }
  if (value === false) {
    return 0;
  }
  if (this.ISERROR(value)) {
    return value;
  }
  return 0;
};

information.NA = function () {
  return error.na;
};

information.SHEET = null;

information.SHEETS = null;

information.TYPE = function (value) {
  if (this.ISNUMBER(value)) {
    return 1;
  }
  if (this.ISTEXT(value)) {
    return 2;
  }
  if (this.ISLOGICAL(value)) {
    return 4;
  }
  if (this.ISERROR(value)) {
    return 16;
  }
  if (Array.isArray(value)) {
    return 64;
  }
};

export default information;
