import error from '../error';
import utils from '../utils';

let database = {};

function compact(array) {
  if (!array) {
    return array;
  }
  let result = [];
  for (let i = 0; i < array.length; ++i) {
    if (!array[i]) {
      continue;
    }
    result.push(array[i]);
  }
  return result;
}

database.FINDFIELD = function (db, title) {
  let index = null;
  for (let i = 0; i < db.length; i++) {
    if (db[i][0] === title) {
      index = i;
      break;
    }
  }

  // Return error if the input field title is incorrect
  if (index == null) {
    return error.value;
  }
  return index;
};

function findResultIndex(db, criterias) {
  let matches = {};
  for (let i = 1; i < db[0].length; ++i) {
    matches[i] = true;
  }
  let maxCriteriaLength = criterias[0].length;
  for (let i = 1; i < criterias.length; ++i) {
    if (criterias[i].length > maxCriteriaLength) {
      maxCriteriaLength = criterias[i].length;
    }
  }

  for (let k = 1; k < db.length; ++k) {
    for (let l = 1; l < db[k].length; ++l) {
      let currentCriteriaResult = false;
      let hasMatchingCriteria = false;
      for (let j = 0; j < criterias.length; ++j) {
        let criteria = criterias[j];
        if (criteria.length < maxCriteriaLength) {
          continue;
        }

        let criteriaField = criteria[0];
        if (database[k][0] !== criteriaField) {
          continue;
        }
        hasMatchingCriteria = true;
        for (let p = 1; p < criteria.length; ++p) {
          currentCriteriaResult =
            currentCriteriaResult || eval(database[k][l] + criteria[p]); // jshint
          // ignore:line
        }
      }
      if (hasMatchingCriteria) {
        matches[l] = matches[l] && currentCriteriaResult;
      }
    }
  }

  let result = [];
  for (let n = 0; n < database[0].length; ++n) {
    if (matches[n]) {
      result.push(n - 1);
    }
  }
  return result;
}

// Database functions
database.DAVERAGE = function (db, field, criteria) {
  // Return error if field is not a number and not a string
  if (isNaN(field) && typeof field !== 'string') {
    return error.value;
  }
  let resultIndexes = findResultIndex(db, criteria);
  let targetFields = [];
  if (typeof field === 'string') {
    let index = db.FINDFIELD(db, field);
    targetFields = utils.rest(db[index]);
  } else {
    targetFields = utils.rest(db[field]);
  }
  let sum = 0;
  for (let i = 0; i < resultIndexes.length; i++) {
    sum += targetFields[resultIndexes[i]];
  }
  return resultIndexes.length === 0 ? error.div0 : sum / resultIndexes.length;
};

database.DCOUNT = function (db, field, criteria) {};

database.DCOUNTA = function (db, field, criteria) {};

database.DGET = function (db, field, criteria) {
  // Return error if field is not a number and not a string
  if (isNaN(field) && typeof field !== 'string') {
    return error.value;
  }
  let resultIndexes = findResultIndex(db, criteria);
  let targetFields = [];
  if (typeof field === 'string') {
    let index = database.FINDFIELD(db, field);
    targetFields = utils.rest(db[index]);
  } else {
    targetFields = utils.rest(db[field]);
  }
  // Return error if no record meets the criteria
  if (resultIndexes.length === 0) {
    return error.value;
  }
  // Returns the #NUM! error value because more than one record meets the
  // criteria
  if (resultIndexes.length > 1) {
    return error.num;
  }

  return targetFields[resultIndexes[0]];
};

database.DMAX = function (db, field, criteria) {
  // Return error if field is not a number and not a string
  if (isNaN(field) && typeof field !== 'string') {
    return error.value;
  }
  let resultIndexes = findResultIndex(db, criteria);
  let targetFields = [];
  if (typeof field === 'string') {
    let index = database.FINDFIELD(db, field);
    targetFields = utils.rest(db[index]);
  } else {
    targetFields = utils.rest(db[field]);
  }
  let maxValue = targetFields[resultIndexes[0]];
  for (let i = 1; i < resultIndexes.length; i++) {
    if (maxValue < targetFields[resultIndexes[i]]) {
      maxValue = targetFields[resultIndexes[i]];
    }
  }
  return maxValue;
};

database.DMIN = function (db, field, criteria) {
  // Return error if field is not a number and not a string
  if (isNaN(field) && typeof field !== 'string') {
    return error.value;
  }
  let resultIndexes = findResultIndex(db, criteria);
  let targetFields = [];
  if (typeof field === 'string') {
    let index = database.FINDFIELD(db, field);
    targetFields = utils.rest(db[index]);
  } else {
    targetFields = utils.rest(db[field]);
  }
  let minValue = targetFields[resultIndexes[0]];
  for (let i = 1; i < resultIndexes.length; i++) {
    if (minValue > targetFields[resultIndexes[i]]) {
      minValue = targetFields[resultIndexes[i]];
    }
  }
  return minValue;
};

database.DPRODUCT = function (db, field, criteria) {
  // Return error if field is not a number and not a string
  if (isNaN(field) && typeof field !== 'string') {
    return error.value;
  }
  let resultIndexes = findResultIndex(db, criteria);
  let targetFields = [];
  if (typeof field === 'string') {
    let index = database.FINDFIELD(db, field);
    targetFields = utils.rest(db[index]);
  } else {
    targetFields = utils.rest(db[field]);
  }
  let targetValues = [];
  for (let i = 0; i < resultIndexes.length; i++) {
    targetValues[i] = targetFields[resultIndexes[i]];
  }
  targetValues = compact(targetValues);
  let result = 1;
  for (let i = 0; i < targetValues.length; i++) {
    result *= targetValues[i];
  }
  return result;
};

database.DSTDEV = function (db, field, criteria) {};

database.DSTDEVP = function (db, field, criteria) {};

database.DSUM = function (db, field, criteria) {};

database.DVAR = function (db, field, criteria) {};

database.DVARP = function (db, field, criteria) {};

database.MATCH = function (lookupValue, lookupArray, matchType) {
  if (!lookupValue && !lookupArray) {
    return error.na;
  }
  if (arguments.length === 2) {
    matchType = 1;
  }
  if (!(lookupArray instanceof Array)) {
    return error.na;
  }
  if (matchType !== -1 && matchType !== 0 && matchType !== 1) {
    return error.na;
  }

  let index;
  let indexValue;

  for (let idx = 0; idx < lookupArray.length; idx++) {
    if (matchType === 1) {
      if (lookupArray[idx] === lookupValue) {
        return idx + 1;
      } else if (lookupArray[idx] < lookupValue) {
        if (!indexValue) {
          index = idx + 1;
          indexValue = lookupArray[idx];
        } else if (lookupArray[idx] > indexValue) {
          index = idx + 1;
          indexValue = lookupArray[idx];
        }
      }
    } else if (matchType === 0) {
      if (typeof lookupValue === 'string') {
        lookupValue = lookupValue.replace(/\?/g, '.');
        if (lookupArray[idx].toLowerCase().match(lookupValue.toLowerCase())) {
          return idx + 1;
        }
      } else {
        if (lookupArray[idx] === lookupValue) {
          return idx + 1;
        }
      }
    } else if (matchType === -1) {
      if (lookupArray[idx] === lookupValue) {
        return idx + 1;
      } else if (lookupArray[idx] > lookupValue) {
        if (!indexValue) {
          index = idx + 1;
          indexValue = lookupArray[idx];
        } else if (lookupArray[idx] < indexValue) {
          index = idx + 1;
          indexValue = lookupArray[idx];
        }
      }
    }
  }

  return index ? index : error.na;
};

export default database;
