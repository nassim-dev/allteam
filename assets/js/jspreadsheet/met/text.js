import utils from '../utils';
import error from '../error';

let text = {};

text.ASC = null;

text.BAHTTEXT = null;

text.CHAR = function (number) {
  number = utils.parseNumber(number);
  if (number instanceof Error) {
    return number;
  }
  return String.fromCharCode(number);
};

text.CLEAN = function (txt) {
  txt = txt || '';
  let re = /[\0-\x1F]/g;
  return txt.replace(re, '');
};

text.CODE = function (txt) {
  txt = txt || '';
  return txt.charCodeAt(0);
};

text.CONCATENATE = function () {
  let args = utils.flatten(arguments);

  let trueFound = 0;
  while ((trueFound = args.indexOf(true)) > -1) {
    args[trueFound] = 'TRUE';
  }

  let falseFound = 0;
  while ((falseFound = args.indexOf(false)) > -1) {
    args[falseFound] = 'FALSE';
  }

  return args.join('');
};

text.DBCS = null;

text.DOLLAR = null;

text.EXACT = function (text1, text2) {
  return text1 === text2;
};

text.FIND = function (find_text, within_text, position) {
  position = position === undefined ? 0 : position;
  return within_text ? within_text.indexOf(find_text, position - 1) + 1 : null;
};

text.FIXED = null;

text.HTML2TEXT = function (value) {
  let result = '';

  if (value) {
    if (value instanceof Array) {
      value.forEach(function (line) {
        if (result !== '') {
          result += '\n';
        }
        result += line.replace(/<(?:.|\n)*?>/gm, '');
      });
    } else {
      result = value.replace(/<(?:.|\n)*?>/gm, '');
    }
  }

  return result;
};

text.LEFT = function (txt, number) {
  number = number === undefined ? 1 : number;
  number = utils.parseNumber(number);
  if (number instanceof Error || typeof txt !== 'string') {
    return error.value;
  }
  return txt ? txt.substring(0, number) : null;
};

text.LEN = function (txt) {
  if (arguments.length === 0) {
    return error.error;
  }

  if (typeof txt === 'string') {
    return txt ? txt.length : 0;
  }

  if (txt.length) {
    return txt.length;
  }

  return error.value;
};

text.LOWER = function (txt) {
  if (typeof txt !== 'string') {
    return error.value;
  }
  return txt ? txt.toLowerCase() : txt;
};

text.MID = function (txt, start, number) {
  start = utils.parseNumber(start);
  number = utils.parseNumber(number);
  if (utils.anyIsError(start, number) || typeof txt !== 'string') {
    return number;
  }

  let begin = start - 1;
  let end = begin + number;

  return text.substring(begin, end);
};

text.NUMBERVALUE = null;

text.PRONETIC = null;

text.PROPER = function (txt) {
  if (txt === undefined || txt.length === 0) {
    return error.value;
  }
  if (txt === true) {
    txt = 'TRUE';
  }
  if (txt === false) {
    txt = 'FALSE';
  }
  if (isNaN(txt) && typeof txt === 'number') {
    return error.value;
  }
  if (typeof txt === 'number') {
    txt = '' + txt;
  }

  return txt.replace(/\w\S*/g, function (str) {
    return str.charAt(0).toUpperCase() + str.substr(1).toLowerCase();
  });
};

text.REGEXEXTRACT = function (txt, regular_expression) {
  let match = txt.match(new RegExp(regular_expression));
  return match ? match[match.length > 1 ? match.length - 1 : 0] : null;
};

text.REGEXMATCH = function (txt, regular_expression, full) {
  let match = txt.match(new RegExp(regular_expression));
  return full ? match : !!match;
};

text.REGEXREPLACE = function (txt, regular_expression, replacement) {
  return txt.replace(new RegExp(regular_expression), replacement);
};

text.REPLACE = function (txt, position, length, new_text) {
  position = utils.parseNumber(position);
  length = utils.parseNumber(length);
  if (
    utils.anyIsError(position, length) ||
    typeof txt !== 'string' ||
    typeof new_text !== 'string'
  ) {
    return error.value;
  }
  return (
    text.substr(0, position - 1) + new_text + text.substr(position - 1 + length)
  );
};

text.REPT = function (txt, number) {
  number = utils.parseNumber(number);
  if (number instanceof Error) {
    return number;
  }
  return new Array(number + 1).join(txt);
};

text.RIGHT = function (txt, number) {
  number = number === undefined ? 1 : number;
  number = utils.parseNumber(number);
  if (number instanceof Error) {
    return number;
  }
  return txt ? txt.substring(txt.length - number) : null;
};

text.SEARCH = function (find_text, within_text, position) {
  let foundAt;
  if (typeof find_text !== 'string' || typeof within_text !== 'string') {
    return error.value;
  }
  position = position === undefined ? 0 : position;
  foundAt =
    within_text.toLowerCase().indexOf(find_text.toLowerCase(), position - 1) +
    1;
  return foundAt === 0 ? error.value : foundAt;
};

text.SPLIT = function (txt, separator) {
  return txt.split(separator);
};

text.SUBSTITUTE = function (txt, old_text, new_text, occurrence) {
  if (!txt || !old_text || !new_text) {
    return txt;
  } else if (occurrence === undefined) {
    return txt.replace(new RegExp(old_text, 'g'), new_text);
  } else {
    let index = 0;
    let i = 0;
    while (txt.indexOf(old_text, index) > 0) {
      index = txt.indexOf(old_text, index + 1);
      i++;
      if (i === occurrence) {
        return (
          txt.substring(0, index) +
          new_text +
          txt.substring(index + old_text.length)
        );
      }
    }
  }
};

text.T = function (value) {
  return typeof value === 'string' ? value : '';
};

text.TEXT = null;

text.TRIM = function (txt) {
  if (typeof txt !== 'string') {
    return error.value;
  }
  return txt.replace(/ +/g, ' ').trim();
};

text.UNICHAR = text.CHAR;

text.UNICODE = text.CODE;

text.UPPER = function (txt) {
  if (typeof txt !== 'string') {
    return error.value;
  }
  return txt.toUpperCase();
};

text.VALUE = null;

export default text;
