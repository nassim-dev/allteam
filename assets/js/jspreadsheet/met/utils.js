let utils = {};

utils.PROGRESS = function (p, c) {
  let color = c ? c : 'red';
  let value = p ? p : '0';

  return (
    '<div style="width:' +
    value +
    '%;height:4px;background-color:' +
    color +
    ';margin-top:1px;"></div>'
  );
};

utils.RATING = function (v) {
  let html = '<div class="jrating">';
  for (let i = 0; i < 5; i++) {
    if (i < v) {
      html += '<div class="jrating-selected"></div>';
    } else {
      html += '<div></div>';
    }
  }
  html += '</div>';
  return html;
};

export default utils;
