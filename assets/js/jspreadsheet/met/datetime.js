import utils from '../utils';
import error from '../error';

let datetime = {};

let d1900 = new Date(1900, 0, 1);
let WEEK_STARTS = [
  undefined,
  0,
  1,
  undefined,
  undefined,
  undefined,
  undefined,
  undefined,
  undefined,
  undefined,
  undefined,
  undefined,
  1,
  2,
  3,
  4,
  5,
  6,
  0,
];
let WEEK_TYPES = [
  [],
  [1, 2, 3, 4, 5, 6, 7],
  [7, 1, 2, 3, 4, 5, 6],
  [6, 0, 1, 2, 3, 4, 5],
  [],
  [],
  [],
  [],
  [],
  [],
  [],
  [7, 1, 2, 3, 4, 5, 6],
  [6, 7, 1, 2, 3, 4, 5],
  [5, 6, 7, 1, 2, 3, 4],
  [4, 5, 6, 7, 1, 2, 3],
  [3, 4, 5, 6, 7, 1, 2],
  [2, 3, 4, 5, 6, 7, 1],
  [1, 2, 3, 4, 5, 6, 7],
];
let WEEKEND_TYPES = [
  [],
  [6, 0],
  [0, 1],
  [1, 2],
  [2, 3],
  [3, 4],
  [4, 5],
  [5, 6],
  undefined,
  undefined,
  undefined,
  [0, 0],
  [1, 1],
  [2, 2],
  [3, 3],
  [4, 4],
  [5, 5],
  [6, 6],
];

datetime.DATE = function (year, month, day) {
  year = utils.parseNumber(year);
  month = utils.parseNumber(month);
  day = utils.parseNumber(day);
  if (utils.anyIsError(year, month, day)) {
    return error.value;
  }
  if (year < 0 || month < 0 || day < 0) {
    return error.num;
  }
  return new Date(year, month - 1, day);
};

datetime.DATEVALUE = function (date_text) {
  if (typeof date_text !== 'string') {
    return error.value;
  }
  let date = Date.parse(date_text);
  if (isNaN(date)) {
    return error.value;
  }
  if (date <= -2203891200000) {
    return (date - d1900) / 86400000 + 1;
  }
  return (date - d1900) / 86400000 + 2;
};

datetime.DAY = function (serial_number) {
  let date = utils.parseDate(serial_number);
  if (date instanceof Error) {
    return date;
  }
  return date.getDate();
};

datetime.DAYS = function (end_date, start_date) {
  end_date = utils.parseDate(end_date);
  start_date = utils.parseDate(start_date);
  if (end_date instanceof Error) {
    return end_date;
  }
  if (start_date instanceof Error) {
    return start_date;
  }
  return serial(end_date) - serial(start_date);
};

datetime.DAYS360 = function (start_date, end_date, method) {};

datetime.EDATE = function (start_date, months) {
  start_date = utils.parseDate(start_date);
  if (start_date instanceof Error) {
    return start_date;
  }
  if (isNaN(months)) {
    return error.value;
  }
  months = parseInt(months, 10);
  start_date.setMonth(start_date.getMonth() + months);
  return serial(start_date);
};

datetime.EOMONTH = function (start_date, months) {
  start_date = utils.parseDate(start_date);
  if (start_date instanceof Error) {
    return start_date;
  }
  if (isNaN(months)) {
    return error.value;
  }
  months = parseInt(months, 10);
  return serial(
    new Date(start_date.getFullYear(), start_date.getMonth() + months + 1, 0)
  );
};

datetime.HOUR = function (serial_number) {
  serial_number = utils.parseDate(serial_number);
  if (serial_number instanceof Error) {
    return serial_number;
  }
  return serial_number.getHours();
};

datetime.INTERVAL = function (second) {
  if (typeof second !== 'number' && typeof second !== 'string') {
    return error.value;
  } else {
    second = parseInt(second, 10);
  }

  let year = Math.floor(second / 946080000);
  second = second % 946080000;
  let month = Math.floor(second / 2592000);
  second = second % 2592000;
  let day = Math.floor(second / 86400);
  second = second % 86400;

  let hour = Math.floor(second / 3600);
  second = second % 3600;
  let min = Math.floor(second / 60);
  second = second % 60;
  let sec = second;

  year = year > 0 ? year + 'Y' : '';
  month = month > 0 ? month + 'M' : '';
  day = day > 0 ? day + 'D' : '';
  hour = hour > 0 ? hour + 'H' : '';
  min = min > 0 ? min + 'M' : '';
  sec = sec > 0 ? sec + 'S' : '';

  return 'P' + year + month + day + 'T' + hour + min + sec;
};

datetime.ISOWEEKNUM = function (date) {
  date = utils.parseDate(date);
  if (date instanceof Error) {
    return date;
  }

  date.setHours(0, 0, 0);
  date.setDate(date.getDate() + 4 - (date.getDay() || 7));
  let yearStart = new Date(date.getFullYear(), 0, 1);
  return Math.ceil(((date - yearStart) / 86400000 + 1) / 7);
};

datetime.MINUTE = function (serial_number) {
  serial_number = utils.parseDate(serial_number);
  if (serial_number instanceof Error) {
    return serial_number;
  }
  return serial_number.getMinutes();
};

datetime.MONTH = function (serial_number) {
  serial_number = utils.parseDate(serial_number);
  if (serial_number instanceof Error) {
    return serial_number;
  }
  return serial_number.getMonth() + 1;
};

datetime.NETWORKDAYS = function (start_date, end_date, holidays) {};

datetime.NETWORKDAYS.INTL = function (
  start_date,
  end_date,
  weekend,
  holidays
) {};

datetime.NOW = function () {
  return new Date();
};

datetime.SECOND = function (serial_number) {
  serial_number = utils.parseDate(serial_number);
  if (serial_number instanceof Error) {
    return serial_number;
  }
  return serial_number.getSeconds();
};

datetime.TIME = function (hour, minute, second) {
  hour = utils.parseNumber(hour);
  minute = utils.parseNumber(minute);
  second = utils.parseNumber(second);
  if (utils.anyIsError(hour, minute, second)) {
    return error.value;
  }
  if (hour < 0 || minute < 0 || second < 0) {
    return error.num;
  }
  return (3600 * hour + 60 * minute + second) / 86400;
};

datetime.TIMEVALUE = function (time_text) {
  time_text = utils.parseDate(time_text);
  if (time_text instanceof Error) {
    return time_text;
  }
  return (
    (3600 * time_text.getHours() +
      60 * time_text.getMinutes() +
      time_text.getSeconds()) /
    86400
  );
};

datetime.TODAY = function () {
  return new Date();
};

datetime.WEEKDAY = function (serial_number, return_type) {
  serial_number = utils.parseDate(serial_number);
  if (serial_number instanceof Error) {
    return serial_number;
  }
  if (return_type === undefined) {
    return_type = 1;
  }
  let day = serial_number.getDay();
  return WEEK_TYPES[return_type][day];
};

datetime.WEEKNUM = function (serial_number, return_type) {};

datetime.WORKDAY = function (start_date, days, holidays) {};

datetime.WORKDAY.INTL = function (start_date, days, weekend, holidays) {};

datetime.YEAR = function (serial_number) {
  serial_number = utils.parseDate(serial_number);
  if (serial_number instanceof Error) {
    return serial_number;
  }
  return serial_number.getFullYear();
};

function isLeapYear(year) {
  return new Date(year, 1, 29).getMonth() === 1;
}

datetime.YEARFRAC = function (start_date, end_date, basis) {};

function serial(date) {
  let addOn = date > -2203891200000 ? 2 : 1;
  return (date - d1900) / 86400000 + addOn;
}

export default datetime;
