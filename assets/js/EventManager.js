import OnScreen from 'onscreen';
import App from './App';
/**
 * @var {array} defaultConfig
 */
const defaultConfig = [];

/**
 *
 *  Class EventManager
 *
 * Represent  object EventManager
 *
 * @category Represent object eventManager
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */

export default class EventManager {
  /**
   *
   * @param {App} app
   * @param {object} config
   */
  constructor(app, config = []) {
    /**
     * @param {App} app
     */
    this.app = app;

    /**
     * Default config
     * @property {array} config
     */
    this.config = config;

    /**
     * Event listeners
     * @property {object} listeners
     */
    this.listeners = {};

    /**
     * OnScreen instance
     * @property {OnScreen} os
     */
    this.os = new OnScreen({
      container: window,
      direction: 'vertical',
      tolerance: 0,
      throttle: 50,
      lazyAttr: null,
      debug: true,
    });

    /**
     * Dom events to bind to object
     * @property {array} bindedEvents
     * @protected
     */
    this.bindedEvents = [];

    /**
     * Component listeners
     */
    this._componentsListerners = [];
  }

  /**
   * Mount component
   */
  mount() {
    this.app.configd
      .parseConfig(defaultConfig, this.config, this)
      .then((config) => {
        this.config = config;
      });

    this.bindedEvents.forEach((event) => {
      window.addEventListener(event, this);
    });
    return this;
  }

  /**
   * Unmount component
   */
  unmount() {
    this.listeners = {};
    this.bindedEvents.forEach((event) => {
      window.removeEventListener(event, this);
    });
    return this;
  }

  /**
   * Reload component
   */
  reload() {
    return this;
  }

  /**
   * Attach handler to an object
   *
   * @param {*} component
   * @param {string} event  event to watch
   * @param {function|EventListener} handler function to execute
   * @param {string|null} selector string selector
   */
  attach(component, event, handler, selector = null) {
    if (typeof component.listeners === 'undefined') {
      component.listeners = {};
    }
    let decoratedHandler = null;
    if (selector != null && typeof handler === 'function') {
      decoratedHandler = (e) => {
        e.preventDefault();
        const element = e.target.closest(selector);
        if (element) {
          return handler(element);
        }
      };
    }

    if (typeof component.listeners[event] === 'undefined') {
      component.listeners[event] = [];
    }

    component.listeners[event].push(decoratedHandler ?? handler);
  }

  /**
   * Detact object's handler
   *
   * @param {*} component
   * @param {string|null} event  event to watch
   * @param {function|string|null} handler function to execute
   */
  detach(component, event = null, handler = null) {
    if (typeof component.listeners === 'undefined') {
      component.listeners = {};
    }
    if (!(event in component.listeners)) {
      return;
    }
    if (typeof handler !== 'function') {
      component.listeners[event ?? 'main'] = [];
      return;
    }
    const stack = component.listeners[event ?? 'main'];
    for (let i = 0, l = stack.length; i < l; i++) {
      if (stack[i] === handler) {
        stack.splice(i, 1);
        return;
      }
    }
  }

  /**
   * Dispatch an event
   *
   * @param {Event|string} event
   * @param {*} component
   * @param {object} detail
   */
  dispatch(event, component, detail = {}) {
    if (typeof component.listeners === 'undefined') {
      component.listeners = {};
    }
    if (typeof event === 'string') {
      event = new CustomEvent(event, { detail: detail });
    }

    if (!(event.type in component.listeners)) {
      return true;
    }

    // this.app.exceptiond.addException('Dispatch event', event);
    const stack = component.listeners[event.type].slice();

    for (let i = 0, l = stack.length; i < l; i++) {
      stack[i].call(component, event);
    }
    return !event.defaultPrevented;
  }

  /**
   * Add an event listener
   * @param {string} type
   * @param {function} callback
   */
  addEventListener(type, callback) {
    this.attach(this, type, callback, null);
  }

  /**
   * Remove an event
   *
   * @param {string} type
   * @param {function} callback
   */
  removeEventListener(type, callback) {
    this.detach(this, type, callback);
  }

  /**
   * Dispatch an event to listeners
   * @param {Event|string} event
   */
  dispatchEvent(event) {
    return this.dispatch(event, this);
  }

  /**
   * Handle function when an event occur
   * @param {Event} event
   */
  handleEvent(event) {
    this.dispatchEvent(event);
  }
}
