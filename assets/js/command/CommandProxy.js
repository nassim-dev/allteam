import Events from '../Events';
import App from '../App';

/**
 *
 *  Class CommandProxy
 *
 * Represent  Base CommandProxy
 *
 * @category Represent html CommandProxy
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */
export default class CommandProxy extends Events {
  /**
   *
   * @param {App} app
   * @param {string} command
   * @param {function} loader
   */
  constructor(app, command, loader) {
    super();
    /**
     * @property {App} app
     */
    this.app = app;

    /**
     * Command File Name
     *
     * @property {string} command
     */
    this.command = command;

    /**
     * Real object
     *
     * @property {DefaultCommand|null} object
     */
    this.object = null;

    /**
     * Module loader function
     *
     * @property {function} loader
     */
    this.loader = loader;
  }

  /**
   * Build object
   */
  async build() {
    if (!this.isBuild()) {
      return this.loader()
        .then((command) => {
          this.object = new command.default(this.app);
        })
        .then(() => {
          return this.object;
        });
    } else {
      return this.object;
    }
  }

  /**
   * Return true if object is created
   *
   * @returns {boolean}
   */
  isBuild() {
    return this.object != null;
  }

  /**
   * Execute the command
   * @param {*} component
   * @param {array} parameters
   */
  execute(component, parameters) {
    if (this.isBuild()) {
      return this.object.execute(component, parameters);
    }
    this.app.exceptiond.addException(
      this.command + ' could not or has not been build yet'
    );
  }
}
