import DefaultCommand from './DefaultCommand';

/**
 *
 *  Class EvalCommand
 *
 * Represent  Base EvalCommand
 *
 * @category Represent html EvalCommand
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */
export default class EvalCommand extends DefaultCommand {
  /**
   * Eval a piece of code
   *
   * @todo remove and use specific method with executeMethod
   * @param {*} component
   * @param {object} command
   *  command: object
   *    parameters: object
   *      value: string (javascript code to be executed)
   *
   */
  execute(component, command) {
    return eval(command.callback);
  }
}
