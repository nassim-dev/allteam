import DefaultCommand from '../../DefaultCommand';
import Swal from 'sweetalert2';
import 'sweetalert2/dist/sweetalert2.css';
/**
 * Class DeleteCommand
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
export default class DeleteCommand extends DefaultCommand {
  /**
   * Use datas attibute to change values
   * @param {*} component
   * @param {object} command
   */
  execute(component, command) {
    Swal.fire({
      title: 'Voulez-vous supprimer cet élément ?',
      text: 'Cette action est définitive!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Oui, supprimer',
    }).then((result) => {
      let datasource = this.app.datad.getDatasource(command.command.endpoint);
      if (result.isConfirmed) {
        this.app.ajaxd.call(datasource).then((response) => {
          //Swal.fire('Supprimé!', "L'élément a été supprimé!", 'success');
        });
      }
    });
  }
}
