import DefaultCommand from '../../DefaultCommand';

/**
 * Class LogoutCommand
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
export default class LogoutCommand extends DefaultCommand {
  /**
   * Use datas attibute to change values
   * @param {*} component
   * @param {object} command
   */
  execute(component, command) {
    this.app.exceptiond.addException('Method not implemented', command);
  }
}
