import DefaultCommand from '../../DefaultCommand';

/**
 * Class PopulateCommand
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
export default class PopulateCommand extends DefaultCommand {
  /**
   * Use datas attibute to change values
   * @param {*} component
   * @param {object} command
   */
  async execute(component, command) {
    let form = this.app.componentd.get(command.command.target);
    let modal = this.app.componentd.get(
      command.command.target.replace('form', 'modal')
    );
    if (null === modal || form === null) {
      return;
    }
    form.blockUi.block();
    setTimeout(() => {
      if (form.blockUi.isBlocked()) {
        form.blockUi.release();
      }
    }, 10000);

    modal.triggerShow();
    this.app.ajaxd
      .call(command.command.endpoint)
      .then((response) => {
        form.populate(response.data);
      })
      .then(() => {
        form.blockUi.release();
      });
  }
}
