import DefaultCommand from '../../DefaultCommand';

/**
 * Class SubmitCommand
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
export default class SubmitCommand extends DefaultCommand {
  /**
   *
   * @param {App} app
   */
  constructor(app) {
    super(app);
  }

  /**
   * Use datas attibute to change values
   * @param {Form} component
   * @param {object} command
   */
  async execute(component, command) {
    let config = command.command;
    component.goto('processing');

    let formDatas = component.getDatas();
    for (let i in config.parameters) {
      formDatas.append(i, config.parameters[i]);
    }

    await this.app.ajaxd
      .call(this.app.datad.getDatasource(config), formDatas)
      .then((response) => {
        let error = typeof response !== 'undefined' ? response.error : true;
        if (!error && typeof component.config.parent != 'undefined') {
          history.back();
          component.clear();
        }
      });

    component.goto('success');
  }
}
