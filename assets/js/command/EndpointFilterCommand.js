import DefaultCommand from './DefaultCommand';

/**
 *
 *  Class EndpointFilterCommand
 *
 * Represent  Base EndpointFilterCommand
 *
 * @category Represent html EndpointFilterCommand
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */
export default class EndpointFilterCommand extends DefaultCommand {
  /**
   * Modify an endpoint
   * @param {*} component
   * @param {object} command
   * command: object
   *    parameters: object
   *      target: string (self or #idhtml)
   *      fiters: array (filters)
   *      datas: array (custom datas for the api call)
   *
   */
  execute(component, command) {
    this.app.datad.fetch(command.endpoint, command.parameters);
  }
}
