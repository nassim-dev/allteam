import DefaultCommand from './DefaultCommand';

/**
 *
 *  Class ChangeAttributeCommand
 *
 * Represent  Base ChangeAttributeCommand
 *
 * @category Represent html ChangeAttributeCommand
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */
export default class ChangeAttributeCommand extends DefaultCommand {
  /**
   * Use datas attibute to change values
   * @param {*} component
   * @param {object} command
   * command: object
   *    target: string (self or #idhtml)
   *    value : string or object (with command)
   *    parameters: object
   *      targetIsValuable: bool (true if target is valuable)
   */
  execute(component, command) {
    let value =
      typeof command.value == 'object'
        ? component.command.execute(
            component,
            command.value,
            command.parameters
          )
        : command.value;

    let $target =
      command.target === '#' + component.id
        ? component.wrapper
        : document.querySelector(command.target);

    if (command.parameters.targetIsValuable) {
      $target.value = value;
      return $target.value;
    }
    $target.dataset[command.attribute] = value;

    return $target.dataset[command.attribute];
  }
}
