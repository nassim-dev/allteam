import DefaultCommand from './DefaultCommand';

/**
 *
 *  Class ElementValueCommand
 *
 * Represent  Base ElementValueCommand
 *
 * @category Represent html ElementValueCommand
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */
export default class ElementValueCommand extends DefaultCommand {
  /**
   * Use an html element's value
   * @param {*} component
   * @param {object} command
   * command: component
   *    parameters: component
   *      source: string (self or #idhtml)
   *      target: string (self or #idhtml)
   *      targetIsValuable: bool (true if target is valuable)
   *      attributeSource: string (attribute to get)
   *      attributeTarget: string (attribute to set)
   */
  execute(component, command) {
    const $source =
      command.parameters.source === 'self'
        ? component.wrapper
        : document.querySelector(command.parameters.source);

    const $target =
      command.parameters.target === 'self'
        ? component.wrapper
        : document.querySelector(command.parameters.target);

    // For select elements
    if (
      $target.classList.contains('mdb-selected') &&
      this.app.componentd.isRegistred($target.getAttribute('id'))
    ) {
      const componentTarget = this.app.componentd.get(
        $target.getAttribute('id')
      );
      new Promise((resolve) => {
        componentTarget.goto('mount');
        resolve();
      })
        .then(() => {
          componentTarget.goto('idle');
        })
        .then(() => {
          componentTarget.preload($source.value);
          componentTarget.dispatch('change');
        });
    } else {
      $target.value = $source.value;
    }
    return $source.value;
  }
}
