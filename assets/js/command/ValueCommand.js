import DefaultCommand from './DefaultCommand';

/**
 *
 *  Class ValueCommand
 *
 * Represent  Base ValueCommand
 *
 * @category Represent html ValueCommand
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */
export default class ValueCommand extends DefaultCommand {
  /**
   * Use the specified value
   * @param {*} component
   * @param {object} command
   *  command: object
   *    parameters: object
   *      value: string
   */
  execute(component, command) {
    return command.parameters.value;
  }
}
