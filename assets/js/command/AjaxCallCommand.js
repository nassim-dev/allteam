import DefaultCommand from './DefaultCommand';

/**
 *
 *  Class AjaxCallCommand
 *
 * Represent  Base AjaxCallCommand
 *
 * @category Represent html AjaxCallCommand
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */
export default class AjaxCallCommand extends DefaultCommand {
  /**
   * Use ajax to fetch a data
   * @param {*} component
   * @param {object} command
   * command: object
   *    parameters: object
   *      target: string (self or #idhtml)
   *      endpoint: string (api endpoint)
   *      datas: array (custom datas for the api call)
   *
   */
  execute(component, command) {
    this.app.ajaxd
      .post(command.endpoint, command.parameters)
      .then((response) => {
        if (typeof response.location !== 'undefined') {
          this.app.windowd.getWindows(response.location);
        }
        if (typeof response.data !== 'undefined') {
          return response.data;
        }
      });
  }
}
