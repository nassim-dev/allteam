import App from '../App';
/**
 *
 *  Class DefaultCommand
 *
 * Represent  Base DefaultCommand
 *
 * @category Represent html DefaultCommand
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */
export default class DefaultCommand {
  /**
   *
   * @param {App} app
   */
  constructor(app) {
    /**
     * @property {App} app
     */
    this.app = app;
  }

  /**
   * Execute the command
   * @param {*} component
   * @param {array} parameters
   */
  execute(component, parameters) {}
}
