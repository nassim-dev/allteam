import Events from './Events';
import App from './App';
import Datasource from './data/Datasource';

/**
 * @var {array} defaultConfig
 */
const defaultConfig = [];

const privateMethods = {
  /**
   *
   * @param {string|object|array} message
   * @param {AjaxManager} instance
   * @returns {FormData|string}
   */
  decorate: (message, instance) => {
    if (typeof message == 'string') {
      return message;
    }
    let csrf = instance.app.getToken('CSRF');
    let jwt = instance.app.getToken('JWT');
    if (message instanceof FormData) {
      message.append(csrf.name, csrf.value);
      message.append(jwt.name, jwt.value);

      return message;
    }
    message[csrf.name] = csrf.value;
    message[jwt.name] = jwt.value;

    return message;
  },
  /**
   *
   * @param {number} time
   * @param {AjaxManager} instance
   */
  waitBeforeNextRefresh: (instance, time) => {
    instance.timer = time;
  },

  /**
   *
   * @param {AjaxManager} instance
   */
  executeTriggers: (instance) => {
    let trigger;
    privateMethods.waitBeforeNextRefresh(instance, 1000);
    instance._loopId = setInterval(() => {
      while (instance._triggers.length > 0) {
        trigger = instance._triggers.shift();

        const component = instance.app.componentd.get(trigger.fieldId);
        instance.app.command.execute(component, trigger.callback);
      }
      privateMethods.waitBeforeNextRefresh(
        instance,
        instance._triggers.length > 0 ? instance.timer * 1.2 : 1000
      );
    }, instance.timer);
  },

  /**
   * Make an ajax call
   *
   * @param {AjaxManager} instance
   * @param {string} method
   * @param {string} url
   * @param {object|string|FormData} datas
   * @param {function|null} xhr
   */
  _callAsync: (instance, method, url, datas = {}, xhr = null) => {
    datas = privateMethods.decorate(datas, instance);
    let body = datas;
    if (datas instanceof FormData) {
      body = [...datas];
    }

    let message = {
      method,
      headers: {
        'Content-Type': 'application/json',
      },
    };

    if (method !== 'GET' && method !== 'HEAD') {
      message.body = new URLSearchParams(body);
    }

    return fetch(url, message)
      .then((response) => {
        if (response.ok) {
          return response.json();
        }

        instance.app.exceptiond.addException(
          `Ajax Error for this url : ${url} with status ${response.status}`,
          this
        );
      })
      .then((response) => {
        if (typeof response === 'undefined') {
          return {};
        }
        if (typeof response.triggers !== 'undefined') {
          response.triggers.forEach((trigger) => {
            instance._triggers.push(trigger);
          });
        }
        if (typeof response.data !== 'undefined') {
          // Save datas in a datasource object
          //instance.app.datad.getDatasource(url).datas = response.data;
          //
          //response.datas.forEach((element) => {
          //  instance.app.dispatchEvent(`ajax.${element}.done`);
          //});
        }
        if (typeof response.setter !== 'undefined') {
          instance.app.componentd.executeSetters(response.setter);
        }

        //let datasource = this.app.datad.getDatasource(method + ':' + url);
        //instance.app.dispatch(datasource.getEventName(), response);

        return response;
      })
      .catch((error) => {
        instance.app.exceptiond.addException(error.message, instance);
      });
  },
};
/**
 *
 *  Class AjaxManager
 *
 * Represent  Base AjaxManager
 *
 * @category Represent html websocketAjaxManager
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 * @todo refactor
 */
export default class AjaxManager extends Events {
  /**
   *
   * @param {App} app
   * @param {object} config
   */
  constructor(app, config = []) {
    super();
    /**
     * @property {App} app
     */
    this.app = app;

    /**
     * Default config components
     * @property {array} config
     */
    this.config = config;

    /**
     * Pending request
     * @property {array} _pendingRequests
     */
    this._pendingRequests = [];

    /**
     * Triggers to execute
     * @property {array} _triggers
     */
    this._triggers = [];

    /**
     * Default interval before a second refresh in milsecondss
     * @property {number} timer
     */
    this.timer = 500;

    /**
     * Loop id of populate function
     * @property {Timeout} _loopId
     */
    this._loopId = null;
  }

  ajaxDecorator(request) {
    request.data = request.data ?? {};

    let csrf = this.app.getToken('CSRF');
    let jwt = this.app.getToken('JWT');

    request.data[csrf.name] = csrf.value;
    request.data[jwt.name] = jwt.value;
    request.options.data = request.data;

    return request;
  }

  /**
   * Mount component
   */
  mount() {
    this.app.configd
      .parseConfig(defaultConfig, this.config, this)
      .then((parsedConfig) => {
        this.config = parsedConfig;
        privateMethods.executeTriggers(this);
      });

    this.app.eventd.addEventListener('fetch', this._callbackOnFetch);
    window.addEventListener('online', this._parsePendingRequests);
    return this;
  }

  /**
   * Callback to execute on fech event
   * @param {Event} event
   */
  _callbackOnFetch(event) {
    return;
  }

  /**
   * Callback to execute on fech event
   * @param {Event} event
   */
  _parsePendingRequests(event) {
    for (let request in this._pendingRequests) {
      this.call(request);
    }
  }

  /**
   *
   * @param {object|Datasource} request
   * @param {object} parameters
   * @returns {Promise}
   */
  call(request, parameters = {}) {
    request.parameters = this.app.configd.mergeDeep(
      request.parameters ?? {},
      parameters
    );
    if (request instanceof Datasource) {
      return this.app.databased.parseDatasource(request);
    }
    if (!navigator.onLine) {
      return this.queue(request);
    }
    switch (request.method) {
      case 'PUT':
        return this.put(request.endpoint, request.parameters);
      case 'GET':
        return this.get(request.endpoint);
      case 'POST':
        return this.post(request.endpoint, request.parameters);
      case 'DELETE':
        return this.delete(request.endpoint, request.parameters);

      default:
        return new Promise((resolve) => {
          this.app.exceptiond.addException(
            'Invalid method : ' + request.method,
            this
          );
          resolve(true);
        });
    }
  }

  /**
   * Unmount component
   */
  unmount() {
    this.app.eventd.removeEventListener('fetch', this._callbackOnFetch);
    window.removeEventListener('online', this._parsePendingRequests);
    clearInterval(this._loopId);
    return this;
  }

  /**
   * Reload component
   */
  reload() {
    return this;
  }

  /**
   * Make GET call
   * @param {string} url
   *
   * @returns {Promise}
   */
  get(url) {
    return privateMethods._callAsync(this, 'GET', url);
  }

  /**
   * Make POST call
   * @param {string} url
   * @param {object} datas
   * @param {string} method
   *
   * @returns {Promise}
   */
  post(url, datas = [], method = 'POST') {
    return privateMethods._callAsync(this, method, url, datas);
  }

  /**
   * Make DELETE call
   * @param {string} url
   * @param {object} datas
   * @param {string} method
   *
   * @returns {Promise}
   */
  delete(url, datas = [], method = 'DELETE') {
    return privateMethods._callAsync(this, method, url, datas);
  }

  /**
   * Make PUT call
   * @param {string} url
   * @param {object} datas
   * @param {string} method
   *
   * @returns {Promise}
   */
  put(url, datas = [], method = 'PUT') {
    return privateMethods._callAsync(this, method, url, datas);
  }
  /**
   *
   * @param {object|Datasource} request
   */
  queue(request) {
    if (navigator.onLine) {
      return this.call(request);
    }
    this._pendingRequests.push(request);
    return Promise.resolve(true);
  }
}
