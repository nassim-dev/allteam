import Events from './Events';
import App from './App';
/**
 *
 *  Class InstantMessaging
 *
 * Represent  Base InstantMessaging
 *
 * @category Represent html instantMessaging
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */

export default class InstantMessaging extends Events {
  constructor(app) {
    super();
  }

  /**
   * Unmount component
   */
  unmount() {
    return this;
  }

  /**
   * Reload component
   */
  reload() {
    return this;
  }

  /**
   * Mount component
   */
  mount() {
    return this;
  }

  ring() {
    return this;
  }
}
