import { isObject } from './Utilities';
import App from './App';
/**
 *
 *  Class ConfigurationManager
 *
 * Represent  Base ConfigurationManager
 *
 * @category Represent html websocketConfigurationManager
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 */

export default class ConfigurationManager {
  /**
   *
   * @param {App} app
   */
  constructor(app) {
    /**
     * @property {App} app
     */
    this.app = app;
  }

  /**
   * Mount component
   */
  mount() {
    return this;
  }

  /**
   * Unmount component
   */
  unmount() {
    return this;
  }

  /**
   * Reload component
   */
  reload() {
    return this;
  }

  /**
   * Parse config for an object
   * @param {object} newConfig
   * @param {object} initialConfig
   * @param {*} object
   */
  parseConfig(newConfig, initialConfig, object) {
    return new Promise((resolve) => {
      let hasBuidinKeyChecker = typeof object.isBuildinKey === 'function';
      for (var key in newConfig) {
        if (
          (hasBuidinKeyChecker && !object.isBuildinKey(key)) ||
          !hasBuidinKeyChecker
        ) {
          initialConfig[key] = this.parseConfigElement(object, newConfig[key]);
        }
      }

      return resolve(initialConfig);
    });
  }

  /**
   * Recursive merge
   * @param {*} target
   * @param  {...any} sources
   */
  mergeDeep(target, ...sources) {
    if (!sources.length) return target;
    const source = sources.shift();

    if (isObject(target) && isObject(source)) {
      for (const key in source) {
        if (isObject(source[key])) {
          if (!target[key]) Object.assign(target, { [key]: {} });
          this.mergeDeep(target[key], source[key]);
        } else {
          Object.assign(target, { [key]: source[key] });
        }
      }
    }

    return this.mergeDeep(target, ...sources);
  }

  /**
   * Parse config element
   * @param {*} object
   * @param {object} config
   */
  parseConfigElement(object, config) {
    if (config === null) {
      return null;
    }
    switch (config.type) {
      case 'METHOD': // old references : CALLABLE
        return this.app.command.execute(
          object,
          config.parameters.value,
          config
        );
      case 'EVAL': // old references : FUNCTION_EVAL
        return this.app.command.execute(object, 'EvalCommand', config);
      case 'FIELD': // old references : FIELD_VALUE
        return this.app.command.execute(object, 'ElementValueCommand', config);
      case 'DATA': // old references : DATA_ATTR && SET_SELF_DATA_ATTR
        return this.app.command.execute(
          object,
          'ChangeAttributeCommand',
          config
        );
      case 'VALUE':
        return this.app.command.execute(object, 'ValueCommand', config);
      case 'AJAX': // old references : AJAX_CALL
        return this.app.command.execute(object, 'AjaxCallCommand', config);
      default:
        return config;
    }
  }
}
