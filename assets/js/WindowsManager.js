import { slugify } from './Utilities';
import Events from './Events';
import App from './App';

/**
 * @var {array} defaultConfig
 */
const defaultConfig = [];

/**
 *
 *  Class WindowsManager
 *
 * Represent  Base WindowsManager
 *
 * @category Represent html websocketWindowsManager
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 * @param {App} app
 */
export default class WindowsManager extends Events {
  /**
   *
   * @param {App} app
   * @param {object} config
   */
  constructor(app, config = []) {
    super();
    /**
     * @param {App} app
     */
    this.app = app;

    /**
     * Default config components
     * @property {array} config
     */
    this.config = config;

    /**
     * Registred windows
     * @property {array<Window>} windows
     */
    this.windows = { main: window };

    /**
     * Used to track whether the user is holding the control key
     * @property {boolean} ctrlIsPressed
     */
    this.ctrlIsPressed = false;
  }

  /**
   * Return main window
   *
   * @returns {Window}
   */
  getMain() {
    return this.windows.main || window;
  }

  /**
   * Factory for windows
   * @param {string} location
   * @param {string} type
   * @param {string} config
   */
  getWindows(
    location,
    type = 'popup',
    config = 'height=800,width=1280, toolbar=no, menubar=no, location=no, resizable=yes, scrollbars=yes, status=no'
  ) {
    const identifier = slugify(location);
    this.windows[identifier] =
      typeof this.windows[identifier] !== 'undefined'
        ? this.windows[identifier]
        : window.open(location, type, config);

    if (window.focus) {
      if (typeof this.windows[identifier] != null) {
        this.windows[identifier].focus();
      }
    }
    return this.windows[identifier];
  }

  /**
   * Create a popup confirmation
   * @param {object} config
   * @param {HTMLElement} object
   */
  createPopup(config, object) {
    const callbackYes =
      typeof config.callbackYes === 'function'
        ? config.callbackYes(object)
        : () => {
            eval(config.callbackYes);
          };

    const callbackNo =
      typeof config.callbackNo === 'function'
        ? config.callbackNo(object)
        : () => {
            eval(config.callbackNo);
          };

    const popupConfig = {
      title: config.title,
      content: config.content,
      animation: 'zoom',
      closeAnimation: 'scale',
      escapeKey: true,
      backgroundDismiss: true,
      theme: 'supervan',
      scrollToPreviousElement: false,
      scrollToPreviousElementAnimate: false,
      buttons: {
        Ok: {
          keys: ['enter'],
          btnClass: ' btn-light-success',
          action: callbackYes,
        },
        Cancel: {
          btnClass: ' btn-light-danger',
          action: callbackNo,
        },
      },
    };

    if (typeof config.type === 'undefined') {
      $.confirm(popupConfig);
    }
  }

  /**
   * Mount component
   */
  mount() {
    this.app.configd
      .parseConfig(defaultConfig, this.config, this)
      .then((parsedConfig) => {
        this.config = parsedConfig;
      });
    return this;
  }

  /**
   * Unmount component
   */
  unmount() {
    return this;
  }

  /**
   * Reload component
   */
  reload() {
    return this;
  }
}
