const evPrefix = 'tc:';

/**
 *
 *  Class Transition
 *
 * Represent  Base Transition
 *
 * @category Represent state Transition
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */
export default class Transition {
  /**
   *
   * @param {string} name
   * @param {function|boolean} onTransition
   * @param {function|boolean} validation
   * @param {function|boolean} onError
   */
  constructor(
    name,
    onTransition = () => {},
    validation = () => {
      return true;
    },
    onError = () => {}
  ) {
    /**
     * Transistion name (name of reached state)
     */
    this.name = name;

    this.nameUc = name[0].toUpperCase() + name.slice(1);

    /**
     * On transistion action
     * @property {function}
     */
    this.onTransition =
      typeof onTransition == 'function' ? onTransition : this.onTransitionF;

    /**
     * On error action
     * @property {function}
     */
    this.onError = typeof onError == 'function' ? onError : this.onErrorF;

    /**
     * Validation function
     * @property {function}
     */
    this.validation =
      typeof validation == 'function' ? validation : this.validationF;
  }

  /**
   * Defautl error action before specific componet methods
   * This method is called on each transistion, on error
   * @param {HTMLElement} component
   * @param {array} parameters
   */
  onErrorF(component, parameters) {
    component.app.command.execute(component, `error${this.nameUc}`, parameters);
    component.app.eventd.dispatch(
      `${evPrefix}:error:${this.name}:${component.id}`,
      component
    );
  }

  /**
   * Defautl validation action before specific componet methods
   * This method is called on each transistion, on validation
   * @param {HTMLElement} component
   * @param {array} parameters
   */
  validationF(component, parameters) {
    return component.app.command.execute(
      component,
      `validation${this.nameUc}`,
      parameters
    );
  }

  /**
   * Defautl transition action before specific componet methods
   * This method is called on each transistion, on transition
   * @param {HTMLElement} component
   * @param {array} parameters
   */
  onTransitionF(component, parameters) {
    component.app.command.execute(
      component,
      `transition${this.nameUc}`,
      parameters
    );
    component.app.eventd.dispatch(
      `${evPrefix}:transition:${this.nameUc}:${component.id}`,
      component
    );
  }
}
