import Transition from './Transition';

/**
 *
 *  Class State
 *
 * Represent  Base State
 *
 * @category Represent state State
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 */
export default class State {
  /**
   *
   * @param {string} name
   * @param {boolean|string} autoswitch
   * @param {function} action
   * @param {function} onReload
   */
  constructor(name, autoswitch = false, action = null, onReload = null) {
    /**
     * State name (name of reached state)
     * @property {string} name
     */
    this.name = name;

    /**
     * Autoswitch to specific state
     * @property {boolean|string} autoswitch
     */
    this.autoswitch = autoswitch;

    /**
     * Registred transitions
     * @property {array<Transition>} transitions
     */
    this.transitions = {};

    this.action = action || this.actionF;
    this.onReload = onReload || this.onReloadF;
  }

  /**
   * Defautl action action before specific component methods
   * This method is called on each transistion, on action
   * @param {HTMLElement} component
   * @param {array} parameters
   */
  actionF(component, parameters) {
    component.app.eventd.dispatch(
      `action.${this.name}.${component.getAttribute('id')}`,
      component
    );
    return component.app.command.execute(component, this.name, parameters);
  }

  /**
   * Defautl reload action before specific componet methods
   * This method is called on each transistion, on reload
   * @param {HTMLElement} component
   * @param {array} parameters
   */
  onReloadF(component, parameters) {
    component.app.command.execute(component, `reload${this.name}`, parameters);
    component.app.eventd.dispatch(
      `reload.${this.name}.${component.getAttribute('id')}`,
      component
    );
  }

  /**
   * Create new transistion
   *
   * @param {string} name
   * @param {function|boolean} onTransition
   * @param {function|boolean} validation
   * @param {function|boolean} onError
   *
   * @returns {State}
   */
  addTransition(
    name,
    onTransition = () => {},
    validation = () => {
      return true;
    },
    onError = () => {}
  ) {
    const transition = new Transition(name, onTransition, validation, onError);
    this.transitions[name] = transition;
    return this;
  }
}
