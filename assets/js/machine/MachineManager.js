import Machine from './Machine';
import Events from '../Events';

/**
 * @var {array} defaultConfig
 */
const defaultConfig = [];

/**
 *
 *  Class MachineManager
 *
 * Represent  Base MachineManager
 *
 * @category Represent html websocketMachineManager
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.ios
 */
export default class MachineManager extends Events {
  /**
   *
   * @param {App} app
   * @param {object} config
   */
  constructor(app, config = {}) {
    super();
    /**
     * @param {App} app
     */
    this.app = app;

    /**
     * Default config components
     * @property {object} config
     */
    this.config = config;

    /**
     * Registred machines
     * @property {array<Machine>} machines
     */
    this.machines = {};
  }

  /**
   * Mount component
   */
  mount() {
    this.app.configd
      .parseConfig(defaultConfig, this.config, this)
      .then((parsedConfig) => {
        this.config = parsedConfig;
      });
    return this;
  }

  /**
   * Unmount component
   */
  unmount() {
    return this;
  }

  /**
   * Reload component
   */
  reload() {
    return this;
  }

  /**
   * Add new command available
   * @param {*} component
   * @param {string} identifier
   * @param {object} config
   *
   * @returns {Machine}
   */
  createMachine(component, identifier, config) {
    return (this.machines[identifier] =
      typeof this.machines[identifier] !== 'undefined'
        ? this.machines[identifier].register(component)
        : new Machine(this.app, component, config));
  }
}
