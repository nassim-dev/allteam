import State from './State';
import Events from '../Events';

/**
 * @var {array} defaultConfig
 */
const defaultConfig = [];

/**
 *
 *  Class Machine
 *
 * Represent  Base Machine
 *
 * @category Represent html websocketMachine
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 * @param {App} app
 */
export default class Machine extends Events {
  /**
   *
   * @param {App} app
   * @param {HTMLElement} component
   * @param {object} config
   */
  constructor(app, component, config = []) {
    super();
    /**
     * @param {App} app
     */
    this.app = app;

    /**
     * Default config components
     * @property {array} config
     */
    this.app.configd
      .parseConfig(defaultConfig, this.config, this)
      .then((parsedConfig) => {
        this.config = parsedConfig;
      });

    /**
     * Limit before a transition is abored if component is locked
     * in milliseconds (5s)
     * @property {number}
     */
    this.limitForExecution = 5000;

    /**
     * Available states && transistions
     *
     * Compoment can specify methods to execute, for exemple, the first state methods are (init, reloadInit, transistionMount, validateMount, errorMount)
     *
     * Every action trigger an event evPrefix + eventReference + component.id
     * @property {array<State>} states
     */
    this.states = {
      default: 'initialized',
    };
    this.addState('initialized')
      .addTransition('mount')
      .addTransition('unmount');
    this.addState('mount', 'idle').addTransition('idle');
    this.addState('idle').addTransition('processing').addTransition('unmount');
    this.addState('processing').addTransition('error').addTransition('success');
    this.addState('error', 'rendering').addTransition('rendering');
    this.addState('success', 'rendering').addTransition('rendering');
    this.addState('rendering', 'idle').addTransition('idle');
    this.addState('unmount').addTransition('mount');

    if (typeof config.states !== 'undefined') {
      config.states.forEach((state) => {
        this.states[state.name] = state;
      });
    }

    /**
     * Components that use this machine
     * @property {array<HTMLElement>} components
     */
    this.components = [];
    this.register(component);
  }

  /**
   * Associate a component to this machine
   * @param {HTMLElement} component
   */
  register(component) {
    this.components[component.id] = {
      component,
      state: this.states[this.states.default],
      _locked: false,
      isLocked() {
        return this._locked;
      },
    };
    return this;
  }

  /**
   * Deregister component
   *
   * @param {HtmlElement|HtmlFormElement|HtmlInputElement} component
   */
  deregister(component) {
    if (this.isRegistred(component.id)) {
      delete this.components[component.id];
    }
    return this;
  }

  /**
   * Return true if elementy is registred
   *
   * @param {string} key
   * @returns {boolean}
   */
  isRegistred(key) {
    return typeof this.components[key] !== 'undefined';
  }

  /**
   * Retrieve component context
   * @param {HTMLElement} component
   */
  getComponentContext(component) {
    if (typeof this.components[component.id] === 'undefined') {
      this.register(component);
    }
    return this.components[component.id];
  }

  /**
   * Lock component
   * @param {HTMLElement} component
   */
  lock(component) {
    this.getComponentContext(component)._locked = true;
    this.app.command.execute(component, 'lock');
  }

  /**
   * Unlock component
   * @param {HTMLElement} component
   */
  unlock(component) {
    this.getComponentContext(component)._locked = false;
    this.app.command.execute(component, 'unlock');
  }

  /**
   * Return objectr state
   * @param {HTMLElement} component

   * @return {object}
   */
  getState(component) {
    const context = this.getComponentContext(component);
    return context.isLocked() ? { name: 'locked' } : context.state;
  }

  /**
   * Create new state
   * @param {string} name
   * @param {boolean|string} autoswitch
   * @param {function|null} action
   * @param {function|null} onReload
   *
   * @returns {State}
   */
  addState(name, autoswitch = false, action = null, onReload = null) {
    const state = new State(name, autoswitch, action, onReload);
    this.states[name] = state;
    return state;
  }

  /**
   * Get a state for edition
   * @param {string} state

   * @returns {State|null}
   */
  editState(state) {
    if (typeof this.states[state] !== 'undefined') {
      return this.states[state];
    }
    this.app.exceptiond.addException(
      `Inexisting state required for edition: ${state}`,
      this
    );
  }

  /**
   * Switch to specific state if possible
   *
   * @param {HTMLElement} component
   * @param {string} state
   * @param {array} parameters
   */
  execute(component, state, parameters = []) {
    let context = this.getComponentContext(component);

    /**
     * @var {Transition} transition
     */
    let transition;

    return (
      new Promise((resolve, reject) => {
        let waitTime = 0;
        // Wait for component lock
        let timeout = setTimeout(() => {
          if (!context.isLocked()) {
            clearTimeout(timeout);
            resolve();
          }
          if (waitTime >= this.limitForExecution) {
            this.app.exceptiond.addException(
              'Unable to process transition, component is locked',
              context
            );
            reject('Unable to process transition, component is locked');
          }
          waitTime += 100;
        }, 100);
      })
        .then(() => {
          return new Promise((resolve, reject) => {
            this.lock(component);

            // Reload if the state is the same
            if (context.state.name == state) {
              if (typeof this.states[state].onReload === 'function') {
                this.states[state].onReload(component, parameters);
              }

              return reject('Same state');
            }

            // Check if transistion exists
            if (
              typeof context.state.transitions == 'undefined' ||
              typeof context.state.transitions[state] == 'undefined'
            ) {
              this.app.exceptiond.addException(
                `Innexisting transistion from ${context.state.name} to ${state}`,
                context
              );
              reject(`Innexisting transistion for ${state}`);
            }
            resolve();
          });
        })
        .then(() => {
          return new Promise((resolve, reject) => {
            // Get required transition
            transition = context.state.transitions[state];

            if (typeof transition === 'undefined') {
              reject('Innexisting transistion');
            }
            // Validation function
            if (
              typeof transition.validation === 'function' &&
              !transition.validation(component, parameters)
            ) {
              this.app.exceptiond.addException(
                `Validation is not satisfied for ${state}`,
                context
              );
              reject(`Validation is not satisfied for ${state}`);
            }
            resolve(transition);
          });
        })
        .then(() => {
          return new Promise((resolve) => {
            // Transistion action
            if (typeof transition.onTransistion === 'function') {
              transition.onTransistion(component, parameters);
            }
            resolve(transition);
          });
        })
        // Do state action
        .then(() => {
          return new Promise((resolve) => {
            context.state = this.states[state];
            if (typeof this.states[state].action === 'function') {
              this.states[state].action(component, parameters);
            }
            resolve(transition);
          });
        })
        // Autoswitch to next state if activate
        .then((transition) => {
          return new Promise((resolve, reject) => {
            if (typeof context.state.autoswitch === 'string') {
              this.unlock(context.component);
              this.execute(component, context.state.autoswitch, parameters);
            }
            resolve(transition);
          });
        })
        .catch((error) => {
          if (
            typeof transition != 'undefined' &&
            typeof transition.onError === 'function'
          ) {
            transition.onError(component, parameters);
          }
          this.app.exceptiond.addException(error, context);
        })
        // Always unlock component
        .finally(() => {
          this.unlock(component);
        })
    );
  }
}
