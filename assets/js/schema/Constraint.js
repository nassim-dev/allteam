/** This file is autogenerated, don't modify */
export default class Constraint {
    // Prototype method
    save() {
        return db.constraint.put(this); // Will only save own props.
    }
        get idcontext() {
        return this.idcontext;
    }    get name() {
        return this.name;
    }    get flag_delete() {
        return this.flag_delete;
    }
}