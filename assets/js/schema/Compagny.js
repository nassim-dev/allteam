/** This file is autogenerated, don't modify */
export default class Compagny {
    // Prototype method
    save() {
        return db.compagny.put(this); // Will only save own props.
    }
        get idcontext() {
        return this.idcontext;
    }    get idaddress() {
        return this.idaddress;
    }    get idcompagny_type() {
        return this.idcompagny_type;
    }    get name() {
        return this.name;
    }    get visual() {
        return this.visual;
    }    get description() {
        return this.description;
    }    get tags() {
        return this.tags;
    }    get flag_delete() {
        return this.flag_delete;
    }
}