/** This file is autogenerated, don't modify */
export default class Event {
    // Prototype method
    save() {
        return db.event.put(this); // Will only save own props.
    }
        get idcontext() {
        return this.idcontext;
    }    get name() {
        return this.name;
    }    get visual() {
        return this.visual;
    }    get description() {
        return this.description;
    }    get start() {
        return this.start;
    }    get status() {
        return this.status;
    }    get flag_delete() {
        return this.flag_delete;
    }
}