/** This file is autogenerated, don't modify */
export default class Operation {
    // Prototype method
    save() {
        return db.operation.put(this); // Will only save own props.
    }
        get idcontext() {
        return this.idcontext;
    }    get name() {
        return this.name;
    }    get duration() {
        return this.duration;
    }    get room_reference() {
        return this.room_reference;
    }    get place_reference() {
        return this.place_reference;
    }    get visual() {
        return this.visual;
    }    get description() {
        return this.description;
    }    get flag_delete() {
        return this.flag_delete;
    }    get idoperation_parent() {
        return this.idoperation_parent;
    }
}