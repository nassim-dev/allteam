import Highway from '@dogstudio/highway';

class Renderer extends Highway.Renderer {
  // Hooks/methods
  onEnter() {}
  onLeave() {}
  onEnterCompleted() {}
  onLeaveCompleted() {}
}

// Don`t forget to export your renderer
export default Renderer;
