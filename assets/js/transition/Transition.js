// File: custom-transition.js
// Import Highway
import Highway from '@dogstudio/highway';
import gsap from 'gsap';

const $header = document.getElementById('header-title');
class Transition extends Highway.Transition {
  // Built-in methods
  in({ from, to, trigger, done }) {
    // Remove Old View

    // Reset Scroll
    window.scrollTo(0, 0);

    //Remove modal overflow
    document.body.style.overflow = 'unset';

    // Animation
    gsap.fromTo(
      to,
      { opacity: 0 },
      {
        duration: 0.5,
        opacity: 1,
        onComplete: done,
      }
    );

    if (typeof $header !== null) {
      $header.innerHTML = to.dataset.routerView;
      // Animation
      gsap.fromTo(
        $header,
        { opacity: 0 },
        {
          duration: 0.5,
          opacity: 1,
          onComplete: done,
        }
      );
    }
  }

  out({ from, trigger, done }) {
    // Animation
    gsap.fromTo(
      from,
      { opacity: 1 },
      { duration: 0.5, opacity: 0, onComplete: done }
    );
    if (typeof $header !== null) {
      gsap.fromTo(
        $header,
        { opacity: 1 },
        { duration: 0.5, opacity: 0, onComplete: done }
      );
    }
    from.remove();
  }
}

// Don`t forget to export your transition
export default Transition;
