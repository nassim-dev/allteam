'use strict';

// Class definition
var KTAppInboxCompose = (function () {
  // Private variables
  var _composeEl;

  // Private methods
  var _init = function () {
    KTAppInboxLib.initEditor(_composeEl, 'kt_inbox_compose_editor');
    KTAppInboxLib.initAttachments(
      document.querySelector('#kt_inbox_compose_attachments')
    );
    KTAppInboxLib.initForm(document.querySelector('#kt_inbox_compose_form'));

    // Remove reply form
    KTUtil.on(_composeEl, '[data-inbox="dismiss"]', 'click', function (e) {
      swal
        .fire({
          text: 'Are you sure to discard this message ?',
          type: 'danger',
          buttonsStyling: false,
          confirmButtonText: 'Discard draft',
          confirmButtonClass: 'btn btn-danger',
          showCancelButton: true,
          cancelButtonText: 'Cancel',
          cancelButtonClass: 'btn btn-light-primary',
        })
        .then(function (result) {
          var myModal = new bootstrap.Modal(_composeEl);
          myModal.hide();
        });
    });
  };

  // Public methods
  return {
    // Public functions
    init: function () {
      // Init variables
      _composeEl = document.querySelector('#kt_inbox_compose');

      if (_composeEl !== null) {
        _init();
      }
    },
  };
})();

// Webpack support
if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
  module.exports = KTAppInboxCompose;
}

export { KTAppInboxCompose };
