/**
 * Start theme js  imports
 */
import '@popperjs/core';
import { initSmoothScroll } from '../functions/init-smoothScroll';

import { KTUtil } from './components/util';
window.KTUtil = KTUtil;

import { KTEventHandler } from './components/event-handler.js';
window.KTEventHandler = KTEventHandler;

import { KTCookie } from './components/cookie.js';
window.KTCookie = KTCookie;

import { KTMenu } from './components/menu.js';
window.KTMenu = KTMenu;

import { KTPasswordMeter } from './components/password-meter.js';
window.KTPasswordMeter = KTPasswordMeter;

import { KTSticky } from './components/sticky.js';
window.KTSticky = KTSticky;

import { KTToggle } from './components/toggle.js';
window.KTToggle = KTToggle;

import { KTApp } from './layout/app.js';
window.KTApp = KTApp;

//import { KTImageInput } from './components/image-input.js';
//window.KTImageInput = KTImageInput;

import { KTDrawer } from './components/drawer.js';
window.KTDrawer = KTDrawer;

//import { KTFeedback } from './components/feedback.js';
//window.KTFeedback = KTFeedback;

//import { KTPlace } from './components/place.js';
//window.KTPlace = KTPlace;

//import { KTScroll } from './components/scroll.js';
//window.KTScroll = KTScroll;

//import { KTScrolltop } from './components/scrolltop.js';
//window.KTScrolltop = KTScrolltop;

//import { KTSearch } from './components/search.js';
//window.KTSearch = KTSearch;

//import { KTStepper } from './components/stepper.js';
//window.KTStepper = KTStepper;

//import { KTDialer } from './components/dialer.js';
//window.KTDialer = KTDialer;

// On document ready
KTUtil.onDOMContentLoaded(function () {
  //KTApp.init();
  initSmoothScroll();
});
