import Toastr from 'toastr2';
import Events from './Events';
import App from './App';
const toastr = new Toastr({
  closeButton: true,
  debug: true,
  newestOnTop: true,
  progressBar: true,
  preventDuplicates: true,
  positionClass: 'toast-top-center',
  showMethod: 'slideDown',
  //hideMethod: 'slideUp',
  showDuration: 300,
  hideDuration: 300,
  timeOut: 2000,
  extendedTimeOut: 300,
  toastClass: ['show'],
});

/**
 * @var {array} defaultConfig
 */
const defaultConfig = [];

const privateMethods = {
  /**
   * Log an error
   * @param {any} message
   * @param {any} context
   *  @param {ExceptionManager} instance
   */
  _log: (message, context, instance) => {
    instance._exceptions.push({
      message,
      context,
    });
  },

  /**
   * Show an error in console
   * @param {any} message
   * @param {any} context
   */
  _show: (message, context) => {
    console.log(message, context);
  },
};
/**
 *
 *  Class ExceptionManager
 *
 * Represent  Base ExceptionManager
 *
 * @category Represent html websocketExceptionManager
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */

export default class ExceptionManager extends Events {
  /**
   *
   * @param {App} app
   * @param {object} config
   */
  constructor(app, config = []) {
    super();
    /**
     * @property {App} app
     */
    this.app = app;

    /**
     * Default config components
     * @property {array} config
     */
    this.config = config;

    /**
     * Saved exceptions
     * @property {Array} exceptions
     */
    this._exceptions = [];
  }

  /**
   *
   * @param {any} message
   * @param {any} context
   */
  addException(message, context) {
    if (this.config.showLogs) {
      privateMethods._show(message, context);
    } else {
      privateMethods._log(message, context, this);
    }
  }

  /**
   * Show an error in an alerte
   *
   * @param {any} message
   * @param {object} properties
   */
  notify(message, properties = {}) {
    const content = message.message || 'Nothing to show';
    const type = message.type || 'success';
    const callback = message.callback || false;
    const title = message.title || '';

    switch (type) {
      case 'success':
        toastr.success(content, title, properties);
        break;
      case 'info':
        toastr.info(content, title, properties);
        break;
      case 'error':
        toastr.error(content, title, properties);
        break;
      case 'warning':
        toastr.warning(content, title, properties);
        break;
      default:
        toastr.info(content, title, properties);
    }

    if (callback) {
      setTimeout(() => {
        window.location.replace(callback);
      }, 3000);
    }
  }

  /**
   * Mount component
   */
  mount() {
    this.app.configd
      .parseConfig(defaultConfig, this.config, this)
      .then((parsedConfig) => {
        this.config = parsedConfig;
      });
    return this;
  }

  /**
   * Unmount component
   */
  unmount() {
    return this;
  }

  /**
   * Reload component
   */
  reload() {
    return this;
  }
}
