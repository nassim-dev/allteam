import EventManager from './EventManager';

/**
 *
 *  Class Events
 *
 * Events listener && target functions
 *
 * @category Events listener && target functions
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */
export default class Events {
  constructor() {
    /**
     * Event manager
     * @property {EventManager} eventd
     */
    this._eventd = null;

    /**
     * Event listeners
     * @property {array} listeners
     */
    this.listeners = [];
  }

  /**
   * Event manager
   *
   * @returns {EventManager} eventd
   */
  get eventd() {
    if (this._eventd == null) {
      this._eventd = new EventManager(window.app);
      this._eventd.mount();
    }
    return this._eventd;
  }

  /**
   * Handle function when an event occur
   * @param {Event} event
   */
  handleEvent(event) {
    return this.eventd.dispatch(event, this);
  }

  /**
   * Attach event listener
   *
   * @param {string} event event to watch
   * @param {function} handler function or command name to exectute
   * @param {string|null} selector string selector
   * @param {array} parameters handler specific parameters
   */
  on(event, handler, selector = null, parameters = []) {
    this.eventd.attach(this, event, handler, selector);
  }

  /**
   * Remove all events listeners for this type of event
   * @param {string} event
   * @param {function|string|null} selector string selector
   */
  off(event, selector = null) {
    this.eventd.detach(this, event, selector);
  }

  /**
   * Dispatch an event
   * @param {string} event
   * @param {object} parameters
   */
  dispatch(event, parameters = []) {
    parameters.context = this;
    const eventObj = new CustomEvent(event, {
      composed: true,
      bubbles: true,
      cancelable: true,
      detail: parameters,
    });
    this.eventd.dispatch(eventObj, this);
    //this.getScope().dispatchEvent(event);
  }
}
