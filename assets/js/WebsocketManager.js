import Events from './Events';
import App from './App';
import * as autobahn from 'autobahn-browser';
//(async () => {
//  const autobahn = await import('autobahn-browser');
//})();

const defaultConfig = {
  skipSubprotocolCheck: true,
};

/**
 *
 *  Class WebsocketManager
 *
 * Represent  Base WebsocketManager
 *
 * @category Represent html websocketWebsocketManager
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */
export default class WebsocketManager extends Events {
  /**
   *
   * @param {App} app
   * @param {object} config
   */
  constructor(app, config = []) {
    super();
    /**
     * @param {App} app
     */
    this.app = app;

    /**
     * Default config components
     * @property {array} config
     */
    this.config = config;

    /**
     * User channel
     * @property {string} channel
     */
    this.channel = config.channel ?? document.querySelector('#CHANNEL')?.value;

    /**
     * Instance channel
     * @property {string} mainchannel
     */
    this.mainchannel =
      config.mainchannel ?? document.querySelector('#MAINCHANNEL')?.value;

    /**
     * Socket server endpoint
     * @property {string} endpoint
     */
    this.endpoint = document.querySelector('#URLSOCKET')?.value;

    /**
     * Actual page channel
     * @property {string} pagechannel
     */
    this.pagechannel =
      config.pagechannel ?? document.querySelector('#PAGECHANNEL')?.value;

    /**
     * Prevous page channel
     * @property {string} oldpagechannel
     */
    this.oldpagechannel = this.pagechannel;

    /**
     * Autobahn instance
     * @todo use native WebSocket class
     * @property Autobahn.Connection
     */
    this.connection = null;

    /**
     * @property autobahn.Session
     */
    this.session = null;

    this.subscriptions = {};
  }

  mount() {
    this.app.configd
      .parseConfig(defaultConfig, this.config, this)
      .then((parsedConfig) => {
        this.channel = document.querySelector('#CHANNEL')?.value;
        this.config = parsedConfig;
        this.connection = new autobahn.Connection({
          //use_es6_promises: true,
          on_user_error: (error, reason) => {
            this.app.exceptiond.addException(error, reason);
          },
          on_internal_error: (error, reason) => {
            this.app.exceptiond.addException(error, reason);
          },

          url: 'wss://' + this.endpoint,
          realm: 'default.domain',
          authextra: this.app.getToken('JWT').value,
        });

        this.connection.onopen = (session) => {
          this.onConnect(session);
        };
        this.connection.onclose = (reason, message) => {
          this.onClose(reason, message);
        };
      })
      .then(() => {
        this.connection.open();
      })
      .then(() => {
        this.oldpagechannel = this.pagechannel;
      });
    return this;
  }

  subscribe(topic) {
    if (!this.session.isOpen) {
      this.app.exceptiond.addException(
        'Session is not opened yet, subscription canceled',
        topic
      );
      return;
    }

    this.session
      .subscribe(topic, (topic, response) => {
        this.processResponse(topic, response);
      })
      .then(
        (subscription) => {
          this.subscriptions[topic] = subscription;
          this.app.exceptiond.addException('New subscription', subscription);
        },
        (error) => {
          this.app.exceptiond.addException(error, this.session);
        }
      );
  }

  unsubscribe(topic) {
    if (
      this.session.isOpen &&
      typeof this.subscriptions[topic] != 'undefined'
    ) {
      this.session.unsubscribe(this.subscriptions[topic]);
    }
  }

  /**
   * Action to execute on socket connect
   */
  onConnect(session) {
    this.session = session;
    this.app.exceptiond.addException(
      'WebSocket connection opened on ' + this.endpoint,
      session
    );

    if (this.session.isOpen) {
      // Subscribe to channels
      if (this.mainchannel != '') {
        this.subscribe(this.mainchannel);
      }
      if (this.channel != '') {
        this.subscribe(this.channel);
      }
      if (this.pagechannel != '') {
        this.subscribe(this.pagechannel);
      }
    }
  }

  publish(channel, data) {
    if (this.session.isOpen) {
      this.session.publish(channel, [{ data: data }]);
    }
  }

  /**
   * Actions to execute on socket close
   */
  onClose(reason, message) {
    this.app.exceptiond.addException(
      'WebSocket connection closed on ' + this.endpoint + ' : ' + reason,
      message
    );
  }

  /**
   * Process socket response
   * @param {string} topic
   * @param {object} response
   */
  processResponse(topic, response) {
    switch (response.type) {
      case 'ERROR':
        this.app.exceptiond.notify({
          message: response.content.message,
          type: response.content.type,
          callback: response.content.callback,
        });

        break;
      case 'ACTIVITY':
        this.app.exceptiond.notify({
          message: response.content.message,
          type: response.content.type,
          callback: response.content.callback,
        });

        break;
      case 'CHAT':
        this.app.comd.ring(response);
        break;
      case 'JSCONFIG':
        //this.app.componentd.populate(response);
        break;
      case 'DATASOURCE':
        this.app.datad.update(response.content.endpoint, response.content.data);
        break;
      case 'RELOADCOMPONENTS':
        response.content.forEach((component) => {
          if (this.app.componentd.isRegistred(component)) {
            this.app.componentd.get(component)?.reload();
          }
        });

        break;
      case 'RECONNECT':
        if (
          typeof response.content.data === 'undefined' ||
          !this.app.componentd.isRegistred('modal_panel')
        ) {
          break;
        }

        const modal = this.app.componentd.get('modal_panel');
        modal.show();

        if (document.getElementById('reconnect') === null) {
          $('#form_create_login').prepend(response.content.data);
          // jsErrorObj.reloadError();
        }
        if (document.getElementById('input-reconnect') === null) {
          $('#email_form_create_login').prepend(
            '<div id="input-reconnect"><input type="hidden" id="input-reconnect-f" name="reconnect" class="form-control" value="true"></div>'
          );
        }

        break;
      default:
        this.app.configd.parseConfigElement(response, this);
    }
  }

  /**
   * Unmount component
   */
  unmount() {
    // UnSubscribe to channels
    this.unsubscribe(this.mainchannel);
    this.unsubscribe(this.channel);
    this.unsubscribe(this.pagechannel);
    this.connection.close();
    return this;
  }

  /**
   * Reload component
   */
  reload() {
    this.unsubscribe(this.oldpagechannel);
    this.channel = document.querySelector('#CHANNEL')?.value;
    this.mainchannel = document.querySelector('#MAINCHANNEL')?.value;
    this.endpoint = document.querySelector('#URLSOCKET')?.value;
    this.pagechannel = document.querySelector('#PAGECHANNEL')?.value;
    this.oldpagechannel = this.pagechannel;
    this.subscribe(this.pagechannel);
    return this;
  }
}
