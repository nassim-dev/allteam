import CommandProxy from './command/CommandProxy';

/**
 * @var {array} defaultConfig
 */
const defaultConfig = [];

/**
 * Other instance of CommandManager
 * @todo create an external factory
 */
const instances = {};

const privateMethods = {
  /**
   *
   * @param {CommandManager} instance
   */
  _loadDefaultCommands: (instance) => {
    //Define custom commands
    let $defaultCommands = document.querySelector('#default-commands');
    if (typeof $defaultCommands != 'undefined' && $defaultCommands != null) {
      let json = $defaultCommands.innerHTML.trim();
      if (json.length > 0) {
        let commands = JSON.parse(json);
        for (var commandType in commands) {
          for (var commandName in commands[commandType]) {
            if (commandType === 'default') {
              instance.addCommand(commandName, async () => {
                let command = commands[commandType][commandName];
                return import(`./command/${command}.js`);
              });
              continue;
            }
            let commandManager = instance.createManager(commandType);
            commandManager.addCommand(commandName, async () => {
              let command = commands[commandType][commandName];
              return import(`./command/${command}.js`);
            });
          }
        }
      }
      $defaultCommands?.parentElement?.removeChild($defaultCommands);
    }
  },
};
/**
 *
 *  Class CommandManager
 *
 * Represent  Base CommandManager
 *
 * @category Represent html CommandManager
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */
export default class CommandManager {
  /**
   *
   * @param {App} app
   * @param {object} config
   */
  constructor(app, config = []) {
    /**
     * @property {App} app
     */
    this.app = app;

    /**
     * Default config components
     * @property {array} config
     */
    this.config = config;

    /**
     * Registred commands classes
     * @property {array<CommandProxy>} commands
     */
    this.commands = [];
  }

  /**
     * Add new command available
     *
     * @todo create an external factory
     * @param {string} identifier

     * @returns {CommandManager}
     */
  createManager(identifier) {
    /**
     * @var {CommandManager} manager
     */
    const manager = (instances[identifier] =
      typeof instances[identifier] !== 'undefined'
        ? instances[identifier]
        : new CommandManager(this.app));

    /**
     * @var {CommandProxy} command
     */
    this.commands.forEach((command) => {
      manager.addCommandProxy(command);
    });

    return manager;
  }

  /**
     * Add new command available
     *
     * @param {string} commandName
     * @param {function|string} loaderCommand

     * @returns {CommandManager}
     */
  addCommand(commandName, loaderCommand) {
    if (!this.commandExist(commandName)) {
      let loader = loaderCommand;
      if (typeof loaderCommand !== 'function') {
        loader = async () => {
          return import(`./command/${commandName}.js`);
        };
      }

      this.commands[commandName] = new CommandProxy(
        this.app,
        commandName,
        loader
      );
    }
    return this;
  }

  /**
     * Add new command available
     * @param {CommandProxy} commandProxy

     * @returns {CommandManager}
     */
  addCommandProxy(commandProxy) {
    if (!this.commandExist(commandProxy.command)) {
      this.commands[commandProxy.command] = commandProxy;
    }
    return this;
  }

  /**
   * Mount component
   */
  mount() {
    this.app.configd
      .parseConfig(defaultConfig, this.config, this)
      .then((parsedConfig) => {
        this.config = parsedConfig;
      })
      .then(() => {
        //privateMethods._loadDefaultCommands(this);
      });

    return this;
  }

  /**
   * Unmount component
   */
  unmount() {
    return this;
  }

  /**
   * Reload component
   */
  reload() {
    privateMethods._loadDefaultCommands(this);
    return this;
  }

  /**
     * Return true if command exists
     * @param {string|function} command

     * @returns {boolean}
     */
  commandExist(command) {
    if (typeof command === 'string') {
      return typeof this.commands[command] !== 'undefined';
    }
    return false;
  }

  /**
     * Return a commandProxy
     * @param {string} command

     *@returns {CommandProxy}
     */
  getCommand(command) {
    return this.commands[command];
  }

  /**
   * Execute method or execute function
   *
   * @todo capacity to call a class
   * @param {HTMLElement} component
   * @param {string|function} command first parameter must be the targeted object if a function is provided
   * @param {array} parameters
   */
  execute(component, command, parameters = []) {
    let result;

    return new Promise((resolve) => {
      if (typeof command === 'function') {
        result = command(component, parameters);
        return resolve(result);
      }

      if (typeof component[command] === 'function') {
        result = component[command](parameters);
        return resolve(result);
      }

      if (this.commandExist(command)) {
        let commandProxy = this.getCommand(command);

        if (!commandProxy.isBuild()) {
          return commandProxy.build().then(() => {
            result = commandProxy.execute(component, parameters);
            resolve(result);
          });
        } else {
          result = commandProxy.execute(component, parameters);
          return resolve(result);
        }
      }
      this.app.exceptiond.addException(
        `Unable to execute command "${command}" for #${component.id}. Reason :  "${command}" must be a method, a function or a registred Command`
      );
    });
  }

  /**
   * Return command name
   */
  getCommandName(command) {
    const path = command.split('/');
    const regex = /\.js/gm;
    return path.pop().replace(regex, '');
  }
}
