import Events from './Events';
import App from './App';
/**
 * @var {array} defaultConfig
 */
const defaultConfig = [];

/**
 *
 *  Class LocalStorageManager
 *
 * Represent  Base LocalStorageManager
 *
 * @category Represent html websocketLocalStorageManager
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */
export default class LocalStorageManager extends Events {
  /**
   *
   * @param {App} app
   * @param {object} config
   */
  constructor(app, config = []) {
    super();
    /**
     * @property {App} app
     */
    this.app = app;

    /**
     * Default config components
     * @property {array} config
     */
    this.config = config;

    /**
     * Storage
     * @property {Storage} storage
     */
    this.storage = config.storage || localStorage;

    this.isAvailable = true;
  }

  /**
   * Mount component
   */
  mount() {
    this.app.configd
      .parseConfig(defaultConfig, this.config, this)
      .then((parsedConfig) => {
        this.config = parsedConfig;
        if (!LocalStorageManager.isEnable('localStorage')) {
          this.app.exceptiond.addException(
            'Unable to activate localStorage',
            this
          );
          this.isAvailable = false;
        }
      });
    return this;
  }

  /**
   * Unmount component
   */
  unmount() {
    this.storage.clear();
    return this;
  }

  /**
   * Reload component
   */
  reload() {
    return this;
  }

  /**
   * Find an item
   * @param {string} item
   */
  get(item) {
    return JSON.parse(this.storage.getItem(item));
  }

  /**
   * Record an item
   * @param {string} item
   * @param {*} content
   */
  set(item, content) {
    this.storage.setItem(item, JSON.stringify(content));
  }

  /**
   * Remove an item
   * @param {string} item
   */
  remove(item) {
    this.storage.remove(item);
  }

  /**
   * Check if storage is enabled
   * @param {string} type

   * @returns {boolean}
   */
  static isEnable(type) {
    let storage;
    let x;
    try {
      storage = Window[type];
      x = '__storage_test__';
      storage.setItem(x, x);
      storage.removeItem(x);
      return true;
    } catch (e) {
      return (
        e instanceof DOMException &&
        // everything except Firefox
        (e.code === 22 ||
          // Firefox
          e.code === 1014 ||
          // test name field too, because code might not be present
          // everything except Firefox
          e.name === 'QuotaExceededError' ||
          // Firefox
          e.name === 'NS_ERROR_DOM_QUOTA_REACHED') &&
        // acknowledge QuotaExceededError only if there's something already stored
        storage &&
        storage.length !== 0
      );
    }
  }
}
