/**
 *
 * @category Represent main application
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */

import jQuery from 'jquery';
window.$ = jQuery;
window.jQuery = jQuery;

// bootstrap
import * as bootstrap from '../node_modules/bootstrap/dist/js/bootstrap.esm.js';
window.bootstrap = bootstrap;

//Application
import App from './js/App.js';

//Page transistion
//import Renderer from './js/transition/Renderer';
import Transition from './js/transition/Transition';

const app = new App(
  {
    lang: document.documentElement.lang,
    environnement: window?.appInit.getEnv(),
    transitionConfig: {
      //renderers: {
      //  name: Renderer,
      //},
      transitions: {
        default: Transition,
      },
    },
  },
  (success) => {
    app.ready(() => {
      //Call  websocket component to mount it
      app.socketd;
      document.getElementById('page-overlay')?.classList.add('invisible');
    });
    app.transition.on('NAVIGATE_IN', ({ to }) => {
      app.switchPage();
    });
    app.transition.on('NAVIGATE_END', ({ to }) => {
      app.appInit.openInitialModal();
      app.appInit.hashListener();
      app.reload();

      document.dispatchEvent(
        new CustomEvent('readystatechange', {
          detail: { readyState: 'complete' },
        })
      );
    });

    app.appInit.hashListener();
  },
  (error) => {
    app.exceptiond.addException(error, app);
  },
  () => {
    return true;
  }
);

app.on('app.mount', () => {
  //Define base commands
  app.command
    .addCommand('AjaxCallCommand', 'AjaxCallCommand')
    .addCommand('ChangeAttributeCommand', 'ChangeAttributeCommand')
    .addCommand('ElementValueCommand', 'ElementValueCommand')
    .addCommand('EndpointFilterCommand', 'EndpointFilterCommand')
    .addCommand('EvalCommand', 'EvalCommand')
    .addCommand('ValueCommand', 'ValueCommand');

  (async () => {
    //Globals StartTheme functions
    import('./js/vendor/index.js');
    //importing customElements
    await import('./js/components/index.js');
    app.appInit.openInitialModal();
  })();
});

app.mount();

//Aditionnal css
import './sass/style.scss';
import 'animate.css';

//import '@fortawesome/fontawesome-free';
import '../node_modules/@fortawesome/fontawesome-free/js/brands';
import '../node_modules/@fortawesome/fontawesome-free/js/solid';
import '../node_modules/@fortawesome/fontawesome-free/js/fontawesome';
import '../node_modules/material-icons/css/material-icons.css';
