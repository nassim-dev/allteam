<?php
/**
 * Cron Action weekly
 * @category cron script
 *
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
const CONTEXT = 'CLI';
define('BASE_DIR', __DIR__ . '/../');
require BASE_DIR . 'functions/cliFunctions.php';

use app\CommandLine;

/** @var CommandLine $cli */
$cli = getCliEnvironement();
cronBase('everyWeek', $cli->getServiceProvider());
