<?php

use core\extensions\ExtensionInterface;
use core\providers\ServiceProviderInterface;
use core\tasks\TaskException;
use core\utils\ClassFinder;
use Nette\Neon\Decoder;

function cronBase(string $context, ServiceProviderInterface $serviceProvider)
{
    /** @var Decoder $decoder */
    $decoder = $serviceProvider->getDi()->singleton(Decoder::class);
    /** @var ClassFinder $classFinder */
    $classFinder = $serviceProvider->getDi()->singleton(ClassFinder::class);

    try {
        $mainCronTasks = $classFinder->getClassesInNamespace('app\\cron');
        foreach ($mainCronTasks as $task) {
            $serviceProvider->getDi()->call($task, $context);
        }
    } catch (TaskException $e) {
        $serviceProvider->getLog()->dump(
            [
                'data'    => $e->getMessage(),
                'logfile' => CRON_LOGFILE
            ]
        );
    }


    try {
        $manifest = $decoder->decode(file_get_contents('nette.safe://' . BASE_DIR . 'extensions/manifest.neon'));
        $manifest = (is_array($manifest)) ? $manifest : [];
        foreach ($manifest as $config) {
            /** @var ExtensionInterface $extension */
            $extension = $serviceProvider->getDi()->singleton($config['class']);
            $extension->executeCronTasks($context);
        }
    } catch (TaskException $e) {
        $serviceProvider->getLog()->dump(
            [
                'data'    => $e->getMessage(),
                'logfile' => CRON_LOGFILE
            ]
        );
    }
}
