<?php

namespace console;

use Nette\Utils\Finder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class GeneratePhpunitTestsCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:generate-phpunit-tests';

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $question = new Question('Please enter the base namespaces separate with a semicon : (default app;core;extensions*)', 'extensions*;app;core');
        $question->setValidator(function ($answer) {
            if ($answer == '') {
                throw new \RuntimeException(
                    'You must provide at least one namespace'
                );
            }

            return  explode(';', $answer);
        });

        $helper  = $this->getHelper('question');
        $answers = $helper->ask($input, $output, $question);

        $config = [];
        foreach ($answers as $namespace) {
            $base      = 'tests';
            $hasSubDir = false;
            if (str_contains($namespace, '*')) {
                $base      = str_replace('\\', '/', $namespace);
                $base      = str_replace('*', '', $base);
                $namespace = str_replace('*', '', $namespace);
                $hasSubDir = true;
            }

            $question = new Question('Please enter the base directory for ' . $namespace . ' : (default ' . $base . ')', $base);
            $config[] = [
                'namespace' => $namespace,
                'path'      => $helper->ask($input, $output, $question) . ($hasSubDir ? '' : '/' . $namespace),
                'hasSubDir' => $hasSubDir
            ];
        }



        $table = new Table($output);
        $table->setHeaderTitle('Configuration');
        $table->setHeaders(['Namespace', 'Path', 'In subdirectory ?']);
        $table->setRows($config);
        $table->render();

        $question = new ChoiceQuestion(
            'Is that ok for you ? (default to no)',
            ['yes', 'no'],
            'no'
        );


        if ($helper->ask($input, $output, $question) == 'yes') {
            $progressBar = new ProgressBar($output, count($config));
            $progressBar->start();
            foreach ($config as $element) {
                if ($element['hasSubDir']) {
                    $dirs = Finder::findDirectories('*')->in(BASE_DIR . $element['path']);
                    foreach ($dirs as $dir) {
                        /** @var \SplFileInfo $dir */
                        exec('vendor/bin/phpunitgen -C tools/phpunitgen_ext.yml ' . $element['namespace'] . '/' . $dir->getFilename() . ' ' . $element['namespace'] . '/' . $dir->getFilename() . '/tests', $output);

                        file_put_contents(BASE_DIR . $element['namespace'] . '/' . $dir->getFilename() . '/phpunit.xml', '
<?xml version="1.0" encoding="utf-8" ?>
<phpunit colors="true">
    <testsuites>
        <testsuite name="' . $element['namespace'] . '/' . $dir->getFilename() . '">
            <directory>' . $element['namespace'] . '/' . $dir->getFilename() . '/tests</directory>
        </testsuite>
    </testsuites>
</phpunit>');
                    }
                    $progressBar->advance();

                    continue;
                }
                exec('vendor/bin/phpunitgen -C tools/phpunitgen.yml ' . $element['namespace'] . ' ' . $element['path'], $output);
                $progressBar->advance();
            }
        }

        if (isset($progressBar)) {
            $progressBar->finish();
        }

        return Command::SUCCESS;
    }
}
