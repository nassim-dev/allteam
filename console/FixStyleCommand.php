<?php

namespace console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class FixStyleCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:phpcs';


    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $question = new Question('Please enter the base directory separate with a semicon : (default app;core;extensions)', 'app;core;extensions');
        $question->setValidator(function ($answer) {
            if ($answer == '') {
                throw new \RuntimeException(
                    'You must provide at least one directory'
                );
            }

            return explode(';', $answer);
        });

        $helper  = $this->getHelper('question');
        $answers = $helper->ask($input, $output, $question);

        $config = [];
        foreach ($answers as $path) {
            $config[] = ['path' => $path];
        }
        $table = new Table($output);
        $table->setHeaderTitle('Configuration');
        $table->setHeaders(['Path']);
        $table->setRows($config);
        $table->render();

        $question = new ChoiceQuestion(
            'Is that ok for you ? (default to no)',
            ['yes', 'no'],
            'no'
        );


        if ($helper->ask($input, $output, $question) == 'yes') {
            foreach ($config as $element) {
                $path = BASE_DIR . $element['path'];
                if (is_dir(!is_dir($path))) {
                    $output->writeln($path . ' is not a directory');

                    return Command::FAILURE;
                }

                exec('vendor/bin/php-cs-fixer fix -v ' . $path, $output);
            }
        }

        return Command::SUCCESS;
    }
}
