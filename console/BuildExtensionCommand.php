<?php

namespace console;

use core\cache\FakeCache;
use core\extensions\ExtensionInterface;
use core\utils\ClassFinder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class BuildExtensionCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:build-extensions';


    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $helper = $this->getHelper('question');

        $question = new ChoiceQuestion(
            'Overwrites existing files ?  (default to no)',
            ['yes', 'no'],
            'no'
        );
        $overwrites = $helper->ask($input, $output, $question);

        if ($overwrites === 'yes') {
            $cache   = new FakeCache();
            $classes = ClassFinder::getClassesInNamespace('extensions', ClassFinder::RECURSIVE_MODE);

            $progressBar = new ProgressBar($output, count($classes));

            $cli = getCliEnvironement();

            // starts and displays the progress bar
            $progressBar->start();
            foreach ($classes as $class) {

                /** @var ExtensionInterface $extension */
                $extension = $cli->getServiceProvider()->getDi()->singleton($class, ['cache' => $cache]);
                $extension->compile();
                $progressBar->advance();
            }

            if (isset($progressBar)) {
                $progressBar->finish();
            }
        }

        return Command::SUCCESS;
    }
}
