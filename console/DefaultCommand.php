<?php

namespace console;

use Nette\Utils\Finder;
use SplFileInfo;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;

class DefaultCommand extends Command
{
    protected static $defaultName = 'list';

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $files    = Finder::findFiles('*.php')->in(__DIR__);
        $commands = [];
        foreach ($files as $file) {
            /** @var SplFileInfo $file */
            $reflection = new \ReflectionClass(__NAMESPACE__ . '\\' . $file->getBasename('.php'));
            if ($reflection->getName() != self::class) {
                $commands[] = $reflection->getStaticPropertyValue('defaultName');
            }
        }

        sort($commands, SORT_STRING);

        $question = new ChoiceQuestion(
            'Choose a command to execute',
            $commands,
        );

        $helper         = $this->getHelper('question');
        $choosenCommand = $helper->ask($input, $output, $question);
        $command        = $this->getApplication()->find($choosenCommand);
        $greetInput     = new ArrayInput([]);

        return  $command->run($greetInput, $output);
    }
}
