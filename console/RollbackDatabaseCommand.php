<?php

namespace console;

use Phinx\Console\PhinxApplication;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class RollbackDatabaseCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:rollback-database';


    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $helper = $this->getHelper('question');

        $date     = $question     = new Question('Specify a date to rollback (default 0) ', 0);
        $question = new ChoiceQuestion(
            'Confirm the rollback ?  (default to no)',
            ['yes', 'no'],
            'no'
        );
        $overwrites = $helper->ask($input, $output, $question);

        if ($overwrites === 'yes') {
            $phinx   = new PhinxApplication();
            $command = $phinx->find('migrate');

            $arguments = [
                'command'         => 'rollback -t ' . $date,
                '--configuration' => __DIR__ . '/../config/phinx.php'
            ];

            $input = new ArrayInput($arguments);
            $command->run(new ArrayInput($arguments), $output);
        }

        return Command::SUCCESS;
    }
}
