<?php

namespace console;

use core\services\generators\EntitieGenerator;
use core\services\generators\Helper;
use core\utils\Utils;
use Nette\Neon\Neon;
use Nette\Utils\FileSystem;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class GenerateEntitieCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:create-entitie';


    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $devNull = [];
        exec('composer dump-autoload --no-ansi &>log', $devNull);
        $helper = $this->getHelper('question');

        $question = new ChoiceQuestion(
            'Overwrites existing files ?  (default to no)',
            ['yes', 'no'],
            'no'
        );
        $overwrites = $helper->ask($input, $output, $question);

        $helper = $this->getHelper('question');

        $question = new Question('Please enter the name of the entitie : ');
        $question->setValidator(function ($answer) {
            if ($answer == '') {
                throw new \RuntimeException(
                    'The name could not be empty'
                );
            }

            return $answer;
        });
        $entitieName = $helper->ask($input, $output, $question);

        $question    = new Question('Please enter the displayed name of the entitie : (default to ' . $entitieName . ')', $entitieName);
        $displayName = $helper->ask($input, $output, $question);

        $question  = new Question('Please enter the name of the namespace : (default to app)', 'app');
        $namespace = $helper->ask($input, $output, $question);

        $properties  = [];
        $addProperty = 'yes';
        while ($addProperty == 'yes') {
            $question = new ChoiceQuestion(
                'Do you want to add a new property ? (default to yes)',
                ['yes', 'no'],
                'yes'
            );
            $addProperty = $helper->ask($input, $output, $question);
            if ($addProperty == 'yes') {
                $question = new ChoiceQuestion(
                    'Does the property is a relation ? (default to no)',
                    ['yes', 'no'],
                    'no'
                );
                $isRelation = $helper->ask($input, $output, $question);

                if ($isRelation == 'no') {
                    $question = new ChoiceQuestion(
                        'Enter the property type (default to text)',
                        array_keys(Helper::PHPDOC_TYPES),
                        'text'
                    );
                    $type = $helper->ask($input, $output, $question);
                } else {
                    $question = new ChoiceQuestion(
                        'Enter the relation type (default to one-to-one)',
                        array_keys(Helper::PHPDOC_RELATIONS),
                        'one-to-one'
                    );
                    $type = $helper->ask($input, $output, $question);
                }

                $question = new Question('Please enter the name of the property : ');
                $question->setValidator(function ($answer) use ($properties, $entitieName, $isRelation) {
                    if ($answer == '') {
                        throw new \RuntimeException(
                            'The name could not be empty'
                        );
                    }
                    $defaults = ['created_at', 'id' . $entitieName, 'updated_at'];

                    if (array_key_exists($answer, $properties) || in_array($answer, $defaults)) {
                        throw new \RuntimeException(
                            'The name must be different : ' . implode(',', $defaults) . ',' . implode(',', array_keys($properties))
                        );
                    }

                    if ($isRelation == 'yes' && $answer[0] . $answer[1] != 'id') {
                        throw new \RuntimeException(
                            'The name must begin by id'
                        );
                    }

                    return $answer;
                });
                $propertyName = $helper->ask($input, $output, $question);


                $question = new Question('Define custom length ? (default to not set)', 'no set');
                $length   = $helper->ask($input, $output, $question);


                $question = new ChoiceQuestion(
                    'Is the property nullable ? (default to yes)',
                    ['yes', 'no'],
                    'yes'
                );
                $isNullable = $helper->ask($input, $output, $question);

                $question = new ChoiceQuestion(
                    'Is the property searcheable ? (default to no)',
                    ['yes', 'no'],
                    'no'
                );
                $searcheable = $helper->ask($input, $output, $question);

                $properties[$propertyName] = [
                    'name'          => $propertyName,
                    'isNullable'    => ($isNullable == 'yes'),
                    'isSearcheable' => ($searcheable == 'yes'),
                    'type'          => $type,
                    'length'        => $length
                ];
            }
        }




        $oldConfig = [];
        if (file_exists('./config/latest_config_succeed.neon')) {
            $oldConfig = Neon::decodeFile('./config/latest_config_succeed.neon');
        }

        $isCreated = true;
        if (!isset($oldConfig[$namespace][$entitieName])) {
            $oldConfig[$namespace][$entitieName] = $properties;
            $isCreated                           = false;
        }

        $needUpdate = Utils::arrayRecursiveDiff($properties, $oldConfig[$namespace][$entitieName] ?? []);

        EntitieGenerator::$overwrite = ($overwrites == 'yes');
        $entitieGenerator            = new EntitieGenerator($entitieName, $namespace, $properties, $displayName);
        $entitieGenerator->getEntitie($isCreated, $needUpdate);


        $question = new ChoiceQuestion(
            'Create repository class ? (default to yes)',
            ['yes', 'no'],
            'yes'
        );

        $createRepository = $helper->ask($input, $output, $question);
        if ($createRepository == 'yes') {
            $entitieGenerator->getRepository();
        }

        $question = new ChoiceQuestion(
            'Create listener class ? (default to yes)',
            ['yes', 'no'],
            'yes'
        );

        $createListener = $helper->ask($input, $output, $question);
        if ($createListener == 'yes') {
            $entitieGenerator->getListener();
        }

        $question = new ChoiceQuestion(
            'Create decorator class ? (default to yes)',
            ['yes', 'no'],
            'yes'
        );

        $createDecorator = $helper->ask($input, $output, $question);
        if ($createDecorator == 'yes') {
            $entitieGenerator->getDecorator();
        }

        $question = new ChoiceQuestion(
            'Create historicalDecorator class ? (default to yes)',
            ['yes', 'no'],
            'yes'
        );

        $createHistoricalDecorator = $helper->ask($input, $output, $question);
        if ($createHistoricalDecorator == 'yes') {
            $entitieGenerator->getHistoricalDecorator();
        }

        $question = new ChoiceQuestion(
            'Create datatableDecorator class ? (default to yes)',
            ['yes', 'no'],
            'yes'
        );

        $createDatatableDecorator = $helper->ask($input, $output, $question);
        if ($createDatatableDecorator == 'yes') {
            $entitieGenerator->getDatatableDecorator();
        }

        $question = new ChoiceQuestion(
            'Create command class ? (default to yes)',
            ['yes', 'no'],
            'yes'
        );

        $createCommand = $helper->ask($input, $output, $question);
        if ($createCommand == 'yes') {
            $entitieGenerator->getCommand();
        }

        $question = new ChoiceQuestion(
            'Create model class ? (default to yes)',
            ['yes', 'no'],
            'yes'
        );

        $createModel = $helper->ask($input, $output, $question);
        if ($createModel == 'yes') {
            $entitieGenerator->getModel();
        }

        $question = new ChoiceQuestion(
            'Create appController class ? (default to yes)',
            ['yes', 'no'],
            'yes'
        );

        $createAppController = $helper->ask($input, $output, $question);
        if ($createAppController == 'yes') {
            $entitieGenerator->getAppController(($createModel == 'yes'));
        }

        $question = new ChoiceQuestion(
            'Create apiController class ? (default to yes)',
            ['yes', 'no'],
            'yes'
        );

        $createApiController = $helper->ask($input, $output, $question);
        if ($createApiController == 'yes') {
            $entitieGenerator->getApiController();
        }

        $question = new ChoiceQuestion(
            'Create datatableWidget class ? (default to yes)',
            ['yes', 'no'],
            'yes'
        );

        $createDatatableWidget = $helper->ask($input, $output, $question);
        if ($createDatatableWidget == 'yes') {
            $entitieGenerator->getDatatableWidget();
        }

        $question = new ChoiceQuestion(
            'Create formWidget class ? (default to yes)',
            ['yes', 'no'],
            'yes'
        );

        $createFormWidget = $helper->ask($input, $output, $question);
        if ($createFormWidget == 'yes') {
            $entitieGenerator->getFormWidget();
        }

        FileSystem::write('./config/latest_config_succeed.neon', Neon::encode($oldConfig));

        $devNull = [];
        exec('composer dump-autoload --no-ansi &>log', $devNull);

        return Command::SUCCESS;
    }
}
