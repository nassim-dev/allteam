<?php

namespace console;

use core\facade\FacadeInterface;
use HaydenPierce\ClassFinder\ClassFinder;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpNamespace;
use Nette\PhpGenerator\PsrPrinter;
use ReflectionClass;
use ReflectionMethod;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class UpdateFacadesCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:update-facades';


    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $helper = $this->getHelper('question');

        $question = new ChoiceQuestion(
            'Overwrites existing files ?  (default to no)',
            ['yes', 'no'],
            'no'
        );
        $overwrites = $helper->ask($input, $output, $question);

        if ($overwrites === 'yes') {
            $classes = ClassFinder::getClassesInNamespace('core', ClassFinder::RECURSIVE_MODE);

            $progressBar = new ProgressBar($output, count($classes));


            // starts and displays the progress bar
            $progressBar->start();
            $printer = new PsrPrinter();

            foreach ($classes as $class) {
                if (in_array(FacadeInterface::class, class_implements($class))) {
                    $classType  = ClassType::withBodiesFrom($class);
                    $reflection = new ReflectionClass($class);

                    if ($classType->isAbstract()) {
                        continue;
                    }


                    $static = $reflection->getStaticProperties();

                    if (!isset($static['serviceInterface'])) {
                        $output->write($class . " hasn'nt propertie serviceInterface");

                        continue;
                    }

                    $interface          = $reflection->getStaticPropertyValue('serviceInterface');
                    $interfaceClassType = new ReflectionClass($interface);

                    $classType->setComment(null);
                    $classType->addComment(' Class ' . $classType->getName());
                    $classType->addComment(' ');
                    $methods = $interfaceClassType->getMethods();

                    foreach ($methods as $method) {
                        if (!$method->isPublic() || str_starts_with($method->getName(), '__')) {
                            continue;
                        }
                        $comment = ' @method static ' . implode('|', $this->listTypes($method)) . ' ' . $method->getName() . '(';
                        $params  = [];
                        foreach ($method->getParameters() as $parameter) {
                            $params[] = implode('|', $this->listTypes($parameter)) . ' $' . $parameter->getName();
                        }
                        $comment .= implode(', ', $params) . ')';
                        $classType->addComment($comment);
                    }

                    $classType->addComment(' @category  Description');
                    $classType->addComment(' @version   Release: 0.2');
                    $classType->addComment(' @author    Nassim Ourami <nassim.ourami@mailo.com>');
                    $classType->addComment(' @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/');
                    $classType->addComment(' ');
                    $classType->addComment(' @link    https://allteam.io');
                    $classType->addComment(' @since   File available since Release 0.2');
                    $classType->addComment(' @package Allteam');


                    $directory = str_replace('\\', '/', $class);
                    $namespace = explode('\\', $class);
                    array_pop($namespace);
                    $namespace     = implode('\\', $namespace);
                    $namespaceType = new PhpNamespace($namespace);
                    $namespaceType->add($classType);
                    file_put_contents($directory . '.php', '<?php ' . $printer->printNamespace($namespaceType));
                }
                $progressBar->advance();
            }

            if (isset($progressBar)) {
                $progressBar->finish();
            }
        }

        return Command::SUCCESS;
    }

    private function listTypes(null|\ReflectionMethod|\ReflectionProperty|\ReflectionParameter $parameter): array
    {
        if ($parameter instanceof ReflectionMethod) {
            $types = $parameter->getReturnType();
        } else {
            $types = $parameter?->getType();
        }

        if (null == $types) {
            $types = ['string'];
        } else {
            if ($types instanceof \ReflectionNamedType) {
                $types = [$types->getName()];
            } else {
                $types = array_map(function (\ReflectionNamedType $type) {
                    return $type->getName();
                }, $types->getTypes());
            }
        }

        return $types;
    }
}
