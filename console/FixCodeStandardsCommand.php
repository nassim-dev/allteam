<?php

namespace console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class FixCodeStandardsCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:rector';


    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        exec('cd tools/rector && vendor/bin/rector', $output);

        return Command::SUCCESS;
    }
}
