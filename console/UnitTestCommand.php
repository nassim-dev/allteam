<?php

namespace console;

use Nette\Utils\Finder;
use SplFileInfo;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class UnitTestCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:unit-test';

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $configs     = Finder::findFiles('phpunit.xml')->from(BASE_DIR);
        $progressBar = new ProgressBar($output, count($configs));
        $progressBar->start();
        foreach ($configs as $file) {
            /** @var SplFileInfo $file */
            exec('vendor/bin/phpunit -c ' . $file->getPathname(), $output);
            //print_r('vendor/bin/phpunit -c ' . $file->getPathname());
            $progressBar->advance();
        }
        if (isset($progressBar)) {
            $progressBar->finish();
        }

        return Command::SUCCESS;
    }
}
