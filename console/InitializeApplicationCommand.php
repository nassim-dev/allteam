<?php

namespace console;

use core\services\generators\EntitieGenerator;
use core\services\generators\Helper;
use core\utils\Utils;
use Nette\Neon\Neon;
use Nette\Utils\FileSystem;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class InitializeApplicationCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:initialize';

    private static $isCreated        = [];
    private static $needUpdate       = [];
    private static $associatedFields = [];

    private static function findRelatedExtension(string $field, array $config): ?string
    {
        if (!isset(self::$associatedFields[$field])) {
            $entity = (str_starts_with($field, 'id')) ? substr($field, 2) : $field;

            foreach ($config as $namespace => $entities) {
                foreach ($entities as $entitieName => $properties) {
                    if ($namespace === 'app') {
                        continue;
                    }
                    if ($entitieName === $entity) {
                        $name = explode('\\', $namespace);
                        $name = end($name);

                        return self::$associatedFields[$field] = $namespace . '\\' . ucfirst($name) . 'Extension';
                    }
                }
            }
        }

        return self::$associatedFields[$field] ?? null;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $devNull = [];
        exec('composer dump-autoload --no-ansi &>log', $devNull);

        $helper = $this->getHelper('question');

        $question = new ChoiceQuestion(
            'Overwrites existing files ?  (default to no)',
            ['yes', 'no'],
            'no'
        );
        $overwrites = $helper->ask($input, $output, $question);

        $helper = $this->getHelper('question');

        $question = new Question('Please enter filepath of the configuration file : (default ./config/init.neon)', './config/init.neon');
        $question->setValidator(function ($answer) {
            if ($answer == '') {
                throw new \RuntimeException(
                    'The filepath could not be empty'
                );
            }
            if (!file_exists($answer)) {
                throw new \RuntimeException(
                    'The filepath does not exists'
                );
            }

            return  Neon::decodeFile($answer);
        });
        $config    = $helper->ask($input, $output, $question);
        $oldConfig = [];
        if (file_exists('./config/latest_config_succeed.neon')) {
            $oldConfig = Neon::decodeFile('./config/latest_config_succeed.neon');
        }

        $modified          = Utils::arrayRecursiveDiff($config, $oldConfig);
        $deleted           = Utils::arrayRecursiveDiff($oldConfig, $config);
        $modifiedOrDeleted = array_merge_recursive($modified, $deleted);

        if (empty($modifiedOrDeleted)) {
            echo 'Nothing to do since the last version.';

            return Command::SUCCESS;
        }

        EntitieGenerator::$overwrite = ($overwrites == 'yes');

        $tables             = [];
        $searchFields       = [];
        $propertiesFormated = [];
        $count              = 0;

        foreach ($config as $namespace => $entities) {
            $count += count($entities);
            foreach ($entities as $entitieName => $properties) {
                if (!isset($modifiedOrDeleted[$namespace][$entitieName])) {
                    continue;
                }
                self::$isCreated[$namespace . '::' . $entitieName]  = (isset($oldConfig[$namespace][$entitieName]));
                self::$needUpdate[$namespace . '::' . $entitieName] = Utils::arrayRecursiveDiff($config[$namespace][$entitieName], $oldConfig[$namespace][$entitieName] ?? []);

                $tables[] = [$entitieName, false];

                $table = new Table($output);
                $table->setHeaderTitle($namespace . ' : ' . $entitieName);
                $table
                    ->setHeaders([
                        'Column',
                        'Type',
                        'Length',
                        'Is nullable ?',
                        'Is searcheable ?',
                        'Target table']);


                $searchFields[$entitieName]                         = [];
                $propertiesFormated[$entitieName]                   = $propertiesFormated[$entitieName] ?? [];
                $propertiesFormated[$entitieName]['__dependencies'] = [];
                foreach ($properties as $k => $propertie) {
                    if (isset($deleted[$namespace][$entitieName][$k])) {
                        continue;
                    }
                    if (!is_array($propertie)) {
                        continue;
                    }

                    if ($propertie['type'] == 'one-to-many') {
                        $propertiesFormated[$entitieName]['__dependencies'] = $propertiesFormated[$entitieName]['__dependencies'] ?? [];
                        $extension                                          = self::findRelatedExtension($propertie['name'], $config);
                        if (!in_array($extension, $propertiesFormated[$entitieName]['__dependencies']) && $extension != null) {
                            $propertiesFormated[$entitieName]['__dependencies'][] = $extension;
                        }

                        $referenceTable = (str_starts_with($propertie['name'], 'id')) ? substr($propertie['name'], 2) : $propertie['name'];
                        if (!isset($propertiesFormated[$referenceTable]['id' . $entitieName])) {
                            $propertiesFormated[$referenceTable]['id' . $entitieName] = [
                                'name'          => 'id' . $entitieName,
                                'type'          => 'many-to-many',
                                'length'        => 'no set',
                                'isNullable'    => true,
                                'isSearcheable' => false,
                            ];
                        }
                    }
                    if ($propertie['type'] == 'many-to-one') {
                        $propertiesFormated[$entitieName]['__dependencies'] = $propertiesFormated[$entitieName]['__dependencies'] ?? [];
                        $extension                                          = self::findRelatedExtension($propertie['name'], $config);
                        if (!in_array($extension, $propertiesFormated[$entitieName]['__dependencies']) && $extension != null) {
                            $propertiesFormated[$entitieName]['__dependencies'][] = $extension;
                        }

                        $referenceTable = (str_starts_with($propertie['name'], 'id')) ? substr($propertie['name'], 2) : $propertie['name'];
                        if (!isset($propertiesFormated[$referenceTable]['id' . $entitieName])) {
                            $propertiesFormated[$referenceTable]['id' . $entitieName] = [
                                'name'          => 'id' . $entitieName,
                                'type'          => 'many-to-many',
                                'length'        => 'no set',
                                'isNullable'    => true,
                                'isSearcheable' => false,
                            ];
                        }
                    }
                    $propertiesFormated[$entitieName][$propertie['name']] = [
                        'name'          => $propertie['name'],
                        'type'          => $propertie['type'],
                        'length'        => $propertie['length'] ?? 'no set',
                        'isNullable'    => $propertie['isNullable'] ?? false,
                        'isSearcheable' => $propertie['isSearcheable'] ?? false,

                    ];
                    if (isset($propertie['isForm'])) {
                        $propertiesFormated[$entitieName][$propertie['name']]['isForm'] = is_array($propertie['isForm']) ? implode(',', $propertie['isForm']) : true;
                    }

                    if (isset($propertie['targetTable'])) {
                        $propertiesFormated[$entitieName][$propertie['name']]['targetTable'] = $propertie['targetTable'];
                    }
                    if (in_array($propertie['type'], array_keys(Helper::PHPDOC_RELATIONS)) && $propertie['type'] != 'one-to-one') {
                        $tableName = (str_starts_with($propertie['name'], 'id')) ? substr($propertie['name'], 2) : $propertie['name'];
                        $tables[]  = [$entitieName . '_' . $tableName, true];
                    }

                    if ($propertie['type'] === 'status' || $propertie['type'] === 'tag') {
                        $tables[] = [$entitieName . '_' . $propertie['name'] . '_list', true];
                    }

                    if (isset($propertie['isSearcheable']) && $propertie['isSearcheable']) {
                        $searchFields[$entitieName][] = $entitieName;
                    }
                }
                $table->setRows($propertiesFormated[$entitieName]);
                $table->render();
            }
        }

        $table = new Table($output);
        $table->setHeaderTitle('Tables created or updated');
        $table->setHeaders(['name', 'Is link table ?']);
        $table->setRows($tables);
        $table->render();


        $question = new ChoiceQuestion(
            'Is that ok for you ? (default to no)',
            ['yes', 'no'],
            'no'
        );


        if ($helper->ask($input, $output, $question) == 'yes') {
            $question = new ChoiceQuestion(
                'Create entity class ? (default to yes)',
                ['yes', 'no'],
                'yes'
            );

            $createEntity = $helper->ask($input, $output, $question);

            $question = new ChoiceQuestion(
                'Create repository class ? (default to yes)',
                ['yes', 'no'],
                'yes'
            );

            $createRepository = $helper->ask($input, $output, $question);


            $question = new ChoiceQuestion(
                'Create listener class ? (default to yes)',
                ['yes', 'no'],
                'yes'
            );

            $createListener = $helper->ask($input, $output, $question);


            $question = new ChoiceQuestion(
                'Create decorator class ? (default to yes)',
                ['yes', 'no'],
                'yes'
            );

            $createDecorator = $helper->ask($input, $output, $question);


            $question = new ChoiceQuestion(
                'Create historicalDecorator class ? (default to yes)',
                ['yes', 'no'],
                'yes'
            );

            $createHistoricalDecorator = $helper->ask($input, $output, $question);

            $question = new ChoiceQuestion(
                'Create datatableDecorator class ? (default to yes)',
                ['yes', 'no'],
                'yes'
            );

            $createDatatableDecorator = $helper->ask($input, $output, $question);


            $question = new ChoiceQuestion(
                'Create command class ? (default to yes)',
                ['yes', 'no'],
                'yes'
            );

            $createCommand = $helper->ask($input, $output, $question);


            $question = new ChoiceQuestion(
                'Create appController class ? (default to yes)',
                ['yes', 'no'],
                'yes'
            );

            $createAppController = $helper->ask($input, $output, $question);


            $question = new ChoiceQuestion(
                'Create apiController class ? (default to yes)',
                ['yes', 'no'],
                'yes'
            );

            $createApiController = $helper->ask($input, $output, $question);

            $question = new ChoiceQuestion(
                'Create model class ? (default to yes)',
                ['yes', 'no'],
                'yes'
            );

            $createModel = $helper->ask($input, $output, $question);


            $question = new ChoiceQuestion(
                'Create datatableWidget class ? (default to yes)',
                ['yes', 'no'],
                'yes'
            );

            $createDatatableWidget = $helper->ask($input, $output, $question);

            $question = new ChoiceQuestion(
                'Create formWidget class ? (default to yes)',
                ['yes', 'no'],
                'yes'
            );
            $createFormWidget = $helper->ask($input, $output, $question);


            $progressBar = new ProgressBar($output, $count);

            // starts and displays the progress bar
            $progressBar->start();


            foreach ($config as $namespace => $entities) {
                foreach ($entities as $entitieName => $properties) {
                    $properties['searchFields'] = $searchFields[$entitieName] ?? [];
                    $progressBar->advance();
                    $this->generateFor(
                        $entitieName,
                        $namespace,
                        $propertiesFormated[$entitieName] ?? null,
                        $properties['createEntity'] ?? $createEntity,
                        $properties['createRepository'] ?? $createRepository,
                        $properties['createListener'] ?? $createListener,
                        $properties['createDecorator'] ?? $createDecorator,
                        $properties['createHistoricalDecorator'] ?? $createHistoricalDecorator,
                        $properties['createDatatableDecorator'] ?? $createDatatableDecorator,
                        $properties['createCommand'] ?? $createCommand,
                        $properties['createAppController'] ?? $createAppController,
                        $properties['createApiController'] ?? $createApiController,
                        $properties['createModel'] ?? $createModel,
                        $properties['createDatatableWidget'] ?? $createDatatableWidget,
                        $properties['createFormWidget'] ?? $createFormWidget
                    );
                }
            }
            FileSystem::write('./config/latest_config_succeed.neon', Neon::encode($config));
        }
        if (isset($progressBar)) {
            $progressBar->finish();
        }

        $table = new Table($output);
        $table->setHeaderTitle('Ignored files');
        $table
            ->setHeaders(['Filename', 'Existing file', 'Just created']);
        $table->setRows(EntitieGenerator::$writed);
        $table->render();

        $devNull = [];
        exec('composer dump-autoload --no-ansi &>log', $devNull);

        return Command::SUCCESS;
    }

    private function generateFor(
        $entitieName,
        $namespace,
        $properties,
        $createEntity,
        $createRepository,
        $createListener,
        $createDecorator,
        $createHistoricalDecorator,
        $createDatatableDecorator,
        $createCommand,
        $createAppController,
        $createApiController,
        $createModel,
        $createDatatableWidget,
        $createFormWidget
    ) {
        if (!is_array($properties)) {
            return;
        }
        $entitieGenerator = new EntitieGenerator($entitieName, $namespace, $properties);

        if ($createEntity == 'yes') {
            $entitieGenerator->getEntitie(
                self::$isCreated[$namespace . '::' . $entitieName],
                self::$needUpdate[$namespace . '::' . $entitieName]
            );

            if ($createRepository == 'yes') {
                $entitieGenerator->getRepository();
            }

            if ($createListener == 'yes') {
                $entitieGenerator->getListener();
            }

            if ($createDecorator == 'yes') {
                $entitieGenerator->getDecorator();
            }

            if ($createHistoricalDecorator == 'yes') {
                $entitieGenerator->getHistoricalDecorator();
            }

            if ($createDatatableDecorator == 'yes') {
                $entitieGenerator->getDatatableDecorator();
            }

            if ($createCommand == 'yes') {
                $entitieGenerator->getCommand();
            }
            if ($createApiController == 'yes') {
                $entitieGenerator->getApiController();
            }

            if ($createModel == 'yes') {
                $entitieGenerator->getModel();
            }

            if ($createDatatableWidget == 'yes') {
                $entitieGenerator->getDatatableWidget();
            }

            if ($createFormWidget == 'yes') {
                $entitieGenerator->getFormWidget(self::$needUpdate[$namespace . '::' . $entitieName]);
            }
        }

        if ($createAppController == 'yes') {
            $entitieGenerator->getAppController(($createEntity == 'yes' && $createModel == 'yes'));
        }
    }
}
