<?php

namespace console;

use HaydenPierce\ClassFinder\ClassFinder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class GenerateKahlanTestsCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:generate-kahlan-tests';

    public const BASE_DIR = __DIR__ . '/../';

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $question = new Question('Please enter the base namespaces separate with a semicon : (default app;core)', 'app;core');
        $question->setValidator(function ($answer) {
            if ($answer == '') {
                throw new \RuntimeException(
                    'You must provide at least one namespace'
                );
            }

            return  explode(';', $answer);
        });

        $helper  = $this->getHelper('question');
        $answers = $helper->ask($input, $output, $question);

        $config = [];
        foreach ($answers as $namespace) {
            $config[] = ['namespace' => $namespace, 'path' => 'spec/' . $namespace];
        }

        $table = new Table($output);
        $table->setHeaderTitle('Configuration');
        $table->setHeaders(['Namespace', 'Path']);
        $table->setRows($config);
        $table->render();

        $question = new ChoiceQuestion(
            'Is that ok for you ? (default to no)',
            ['yes', 'no'],
            'no'
        );
        if ($helper->ask($input, $output, $question) == 'yes') {
            $classes     = $this->getClasses($config);
            $progressBar = new ProgressBar($output, count($classes));

            // starts and displays the progress bar
            $progressBar->start();

            foreach ($classes as $class) {
                $progressBar->advance();
                $reflection = new \ReflectionClass($class);

                if ($reflection->isAbstract() || $reflection->isInterface()) {
                    continue;
                }
                $template = $this->getTemplate($reflection);
                $filename = self::BASE_DIR . 'spec/' . str_replace('\\', '/', $class) . 'Spec.php';

                $elements = explode('/', $filename);
                array_pop($elements);
                $directory = implode('/', $elements);
                if (!is_dir($directory)) {
                    mkdir($directory, 0777, true);
                }

                file_put_contents($filename, '<?php ' . $template);
            }

            if (isset($progressBar)) {
                $progressBar->finish();
            }
        }

        return Command::SUCCESS;
    }

    private function getClasses(array $config): array
    {
        $classes = [];
        foreach ($config as $element) {
            $classes = array_merge($classes, ClassFinder::getClassesInNamespace($element['namespace'], ClassFinder::RECURSIVE_MODE));
        }

        return $classes;
    }

    private function getTemplate(\ReflectionClass $reflectionClass): string
    {
        $beforeEach = '$this->reflection = new ReflectionClass(' . $reflectionClass->getName() . '::class);
        $this->object = $this->reflection->newInstanceWithoutConstructor();
        if($this->reflection->hasProperty("cache")) {
            allow(core\cache\RedisCache::class)->toBe(core\cache\FakeCache::class);
            allow(core\cache\MemcachedCache::class)->toBe(core\cache\FakeCache::class);
            allow(core\cache\CacheServiceInterface::class)->toBe(core\cache\FakeCache::class);
        }';

        $methodsTest = '';
        foreach ($reflectionClass->getMethods() as $method) {
            $methodsTest .= $this->getMethodTest($method);
        }

        return '

/**
 * Unit test of ' . $reflectionClass->getName() . '
 *
 * @category  Unit Test
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
describe("' . $reflectionClass->getName() . ' class", function() {
    beforeEach(function() {
        ' . $beforeEach . '
    });
    ' . $methodsTest . '
});';
    }

    private function getMethodTest(\ReflectionMethod $reflectionMethod, $beforeEach = '')
    {
        $reflection = $reflectionMethod->getDeclaringClass();
        $propertie  = strtolower(str_replace('get', '', $reflectionMethod->getName()));
        $propertie  = strtolower(str_replace('set', '', $propertie));

        if (str_starts_with($reflectionMethod->getName(), '__')) {
            return '';
        }
        if (str_starts_with($reflectionMethod->getName(), 'get')
        && $reflectionMethod->getName() != 'get'
        && $reflection->hasProperty($propertie)) {
            $testMethod = $this->getGetterTemplate($reflectionMethod);
        } elseif (str_starts_with($reflectionMethod->getName(), 'set')
        && $reflectionMethod->getName() != 'set'
        && $reflection->hasProperty($propertie)) {
            $testMethod = $this->getSetterTemplate($reflectionMethod);
        } else {
            $testMethod = $this->getMustBeImplementedTemplate($reflectionMethod);
        }

        return '
        describe("Test of method ' . $reflectionMethod->getDeclaringClass()->getName() . '::' . $reflectionMethod->getName() . '", function() {
            beforeEach(function() {
                ' . $beforeEach . '
            });
            ' . $testMethod . '
        });
        ';
    }

    private function getGetterTemplate(\ReflectionMethod $reflectionMethod): string
    {
        $propertie  = strtolower(str_replace('get', '', $reflectionMethod->getName()));
        $reflection = $reflectionMethod->getDeclaringClass();

        $propertie = $reflection->getProperty($propertie);

        $parameters       = $reflectionMethod->getParameters();
        $defaultParameter = $parameters[0] ?? null;
        $types            = array_merge($this->listTypes($propertie), $this->listTypes($defaultParameter));

        $return = '';
        $passed = [];
        foreach ($types as $type) {
            if (in_array($type, $passed)) {
                continue;
            }
            $passed[] = $type;

            $return .= '
            it("tests getter ' . $reflectionMethod->getName() . ' with ' . $type . '", function() {
            ' . $this->getTestedValue($type) . '
                $this->object->' . $propertie->getName() . ' = $testedValue;
                expect($this->object->' . $reflectionMethod->getName() . '())->toBe($testedValue);
            });';
        }

        return $return;
    }

    private function getSetterTemplate(\ReflectionMethod $reflectionMethod): string
    {
        $propertie  = strtolower(str_replace('set', '', $reflectionMethod->getName()));
        $reflection = $reflectionMethod->getDeclaringClass();
        $propertie  = $reflection->getProperty($propertie);

        $parameters       = $reflectionMethod->getParameters();
        $defaultParameter = $parameters[0] ?? null;
        $types            = array_merge($this->listTypes($propertie), $this->listTypes($defaultParameter));

        $return = '';
        $passed = [];
        foreach ($types as $type) {
            if (in_array($type, $passed)) {
                continue;
            }
            $passed[] = $type;
            $return .= '
            it("tests setter ' . $reflectionMethod->getName() . ' with ' . $type . '", function() {
            ' . $this->getTestedValue($type) . '
                $this->object->' . $reflectionMethod->getName() . '($testedValue);
                expect($this->object->' . $propertie->getName() . ')->toBe($testedValue);
            });';
        }

        return $return;
    }

    private function getMustBeImplementedTemplate(\ReflectionMethod $reflectionMethod): string
    {
        $parameters = $reflectionMethod->getParameters();

        $definition = '
        ';
        foreach ($parameters as $parameter) {
            $definition .= '$' . $parameter->getName() . ' ';
            $type = $parameter->getType();
            $definition .= ($type === null) ? 'with no type' : implode(',', $this->listTypes($parameter));
            $definition .= '
                ';
        }

        return '
        it("must be implemented for method ' . $reflectionMethod->getName() . '", function() {
            /*Test must be implemented manually
            It has these parameters :  ' . $definition . '
            It must return these types : ' . $reflectionMethod->getReturnType() . '*/
        });';
    }

    private function listTypes(null|\ReflectionProperty|\ReflectionParameter $parameter): array
    {
        $types = $parameter?->getType();

        if (null == $types) {
            $types = ['string'];
        } else {
            if ($types instanceof \ReflectionNamedType) {
                $types = [$types->getName()];
            } else {
                $types = array_map(function (\ReflectionNamedType $type) {
                    return $type->getName();
                }, $types->getTypes());
            }
        }

        return $types;
    }

    private function getTestedValue(string $type): string
    {
        switch ($type) {
            case 'string':
                return '$testedValue = "Chaton sachant chatoner sans son chat";';


            case 'int':
                return '$testedValue = 1234;';


            case 'array':
                return '$testedValue = ["Chaton sachant chatoner sans son chat"];';


            case 'bool':
                return '$testedValue = true;';


            case 'float':
                return '$testedValue = 1.234;';

            default:
                return '$testedValue = ""; //It must be defined manually with an instance of ' . $type;
        }
    }
}
