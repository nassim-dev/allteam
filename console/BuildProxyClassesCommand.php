<?php

namespace console;

use core\app\ApplicationServiceInterface;
use core\app\Bootstrap;
use core\cache\CacheServiceInterface;
use core\cache\FakeCache;
use core\cache\RedisCache;
use core\DI\DependenciesInjector;
use HaydenPierce\ClassFinder\ClassFinder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;

/**
 * @return ApplicationServiceInterface
 */
function cli(): ApplicationServiceInterface
{
    $bootstrap = new Bootstrap(
        applicationClass:ApplicationServiceInterface::class,
        container:['class' => DependenciesInjector::class, 'cache' => FakeCache::class],
        configClass:Config::class,
    );

    $bootstrap->startup(
        [
            'CONTEXT' => CONTEXT,
            'envFile' => 'config/env.neon'
        ]
    );

    $_SERVER['REQUEST_SCHEME'] = $bootstrap->getConfig()->find('PHP.HTTP_PROTOCOL');
    $_SERVER['SERVER_NAME']    = $bootstrap->getConfig()->find('PHP.SERVER_NAME');
    $_SERVER['HTTP_HOST']      = $bootstrap->getConfig()->find('PHP.SERVER_NAME');


    /** @var ApplicationServiceInterface $app */
    $app = $bootstrap->buildApplication();

    $app->boot() //Startup actions
        ->process();//Render view

    return $app;
}


/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class BuildProxyClassesCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:build-proxy';


    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $helper = $this->getHelper('question');

        $question = new ChoiceQuestion(
            'Overwrites existing files ?  (default to no)',
            ['yes', 'no'],
            'no'
        );
        $overwrites = $helper->ask($input, $output, $question);

        $overwrites = ($overwrites === 'yes');
        $cli        = cli();

        $di = $cli->getServiceProvider()->getDi();
        $di->bind(CacheServiceInterface::class, FakeCache::class);
        $di->bind(RedisCache::class, FakeCache::class);
        $proxyFactory = $di->getProxyFactory();

        $classes = ClassFinder::getClassesInNamespace('extensions', ClassFinder::RECURSIVE_MODE);
        $classes = array_merge($classes, ClassFinder::getClassesInNamespace('app', ClassFinder::RECURSIVE_MODE));
        $classes = array_merge($classes, ClassFinder::getClassesInNamespace('core', ClassFinder::RECURSIVE_MODE));

        $progressBar = new ProgressBar($output, count($classes));

        // starts and displays the progress bar
        $progressBar->start();
        foreach ($classes as $class) {
            $proxyFactory->getProxyFor($class, $overwrites);
            $progressBar->advance();
        }

        if (isset($progressBar)) {
            $progressBar->finish();
        }


        return Command::SUCCESS;
    }
}
