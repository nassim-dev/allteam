# How to Contribute to AllTeam

## Get the Source

## Build the Source

## What to Change?

## Test Your Changes

### Functional Tests

Read [README](https://gitlab.com/Tricksoft/allteam/-/blob/master/README.md) for more details.

### Performance Tests

## Polish Your Commits

Before submitting your patch, be sure to read :

- [PHP coding guidelines](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-2-coding-style-guide.md)
- [JAVASCRIPT coding guidelines](https://developer.mozilla.org/en-US/docs/MDN/Guidelines/Code_guidelines/JavaScript)

Then check your code to match as best you can.
This can be a lot of effort, but it saves time during review to avoid style issues.

The other possibly major difference between the mailing list submissions and GitHub PR workflows
is that each commit will be reviewed independently. Even if you are submitting a
patch series with multiple commits, each commit must stand on it's own and be reviewable
by itself. Make sure the commit message clearly explain the why of the commit not the how.
Describe what is wrong with the current code and how your changes have made the code better.

When preparing your patch, it is important to put yourself in the shoes of the Git community.
Accepting a patch requires more justification than approving a pull request from someone on
your team. The community has a stable product and is responsible for keeping it stable. If
you introduce a bug, then they cannot count on you being around to fix it. When you decided
to start work on a new feature, they were not part of the design discussion and may not
even believe the feature is worth introducing.

Questions to answer in your patch message (and commit messages) may include:

- Why is this patch necessary?
- How does the current behavior cause pain for users?
- What kinds of repositories are necessary for noticing a difference?
- What design options did you consider before writing this version? Do you have links to
  code for those alternate designs?
- Is this a performance fix? Provide clear performance numbers for various well-known repos.

Here are some other tips that we use when cleaning up our commits:

- Commit messages should be wrapped at 76 columns per line (or less; 72 is also a
  common choice).
- Make sure the commits are signed off using `git commit (-s|--signoff)`. See
  [SubmittingPatches](https://github.com/git/git/blob/v2.8.1/Documentation/SubmittingPatches#L234-L286)
  for more details about what this sign-off means.
- Check for whitespace errors using `git diff --check [base]...HEAD` or `git log --check`.
- Run `git rebase --whitespace=fix` to correct upstream issues with whitespace.
- Become familiar with interactive rebase (`git rebase -i`) because you will be reordering,
  squashing, and editing commits as your patch or series of patches is reviewed.
- Make sure any shell scripts that you add have the executable bit set on them. This is
  usually for test files that you add in the `/t` directory. You can use
  `git add --chmod=+x [file]` to update it. You can test whether a file is marked as executable
  using `git ls-files --stage \*.sh`; the first number is 100755 for executable files.
- Your commit titles should match the "area: change description" format. Rules of thumb:
  - Choose "<area>: " prefix appropriately.
  - Keep the description short and to the point.
  - The word that follows the "<area>: " prefix is not capitalized.
  - Do not include a full-stop at the end of the title.
  - Read a few commit messages -- using `git log origin/master`, for instance -- to
    become acquainted with the preferred commit message style.
- Build source using `make DEVELOPER=1` for extra-strict compiler warnings.

## Submit Your Patch

### Examples

To generate a single commit patch file:

```
git format-patch -s -o [dir] -1
```

To generate four patch files from the last three commits with a cover letter:

```
git format-patch --cover-letter -s -o [dir] HEAD~4
```

To generate version 3 with four patch files from the last four commits with a cover letter:

```
git format-patch --cover-letter -s -o [dir] -v 3 HEAD~4
```

### Submit the Patch

Run [`git send-email`](https://git-scm.com/docs/git-send-email), starting with a test email:

```
git send-email --to=yourself@address.com  [dir with patches]/*.patch
```

After checking the receipt of your test email, you can send to the list and to any
potentially interested reviewers.

```
git send-email --to=git@vger.kernel.org --cc=<email1> --cc=<email2> [dir with patches]/*.patch
```

To submit a nth version patch (say version 3):

```
git send-email --to=git@vger.kernel.org --cc=<email1> --cc=<email2> \
    --in-reply-to=<the message id of cover letter of patch v2> [dir with patches]/*.patch
```
