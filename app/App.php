<?php

namespace app;

use core\app\AbstractApplication;
use core\app\ApplicationServiceInterface;
use core\cache\CacheServiceInterface;
use core\cache\CacheUpdaterTrait;
use core\config\Config;
use core\controller\ControllerInterface;
use core\entities\EntitieFactoryInterface;
use core\factory\FactoryInterface;
use core\html\HtmlElement;
use core\logger\application\TracyApplicationPanel;
use core\messages\response\HttpResponseInterface;
use core\providers\ServiceProvider;
use core\transport\websocket\WebsocketServiceInterface;
use core\utils\ClassFinder;
use core\utils\Utils;
use ReflectionClass;
use Tracy\Debugger;

/**
 * Class App
 *
 * Main application object
 *
 * @category  Main application object
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class App extends AbstractApplication implements ApplicationServiceInterface
{
    use CacheUpdaterTrait;

    /**
     * ControllerObject
     */
    private ?\core\controller\ControllerInterface $_controller = null;

    /**
     * Event Listerners
     */
    private ?array $_listeners = null;

    /**
     * HtmlElement factories
     */
    private ?array $_factories = null;

    /**
     * Application Entities
     */
    private ?array $_appEntities = null;

    public function __construct(
        protected ServiceProvider $serviceProvider,
        protected Config $config,
        protected CacheServiceInterface $cache
    ) {
        $this->_fetchPropsFromCache(['_listeners', '_factories', '_repository', '_appEntities']);

        if ($this->config->find('ENVIRONNEMENT') === 'development' && CONTEXT === 'APP' && !isset($_SERVER['x-requested-with'])) {
            Debugger::getBar()->addPanel(new TracyApplicationPanel($this));
        }
    }

    public function __destruct()
    {
        $this->_cacheMultiplesProps(['_listeners', '_factories', '_repository', '_appEntities']);
    }

    /**
     * Get dependencies after construction to prevent cyclic-dependencies
     */
    public function build(): self
    {
        $eventManager = $this->getServiceProvider()->getEventManager();
        $eventManager->emit('APP_PRE_BUILD');

        if (null === $this->_appEntities) {
            /** @var EntitieFactoryInterface $entitieFactory */
            $entitieFactory     = $this->getDi()->singleton(EntitieFactoryInterface::class);
            $this->_appEntities = $entitieFactory->registerApplicationEntities();
        }

        if (!is_array($this->_listeners)) {
            /** @var ClassFinder $finder */
            $finder           = $this->getDi()->singleton(ClassFinder::class);
            $this->_listeners = $finder->findClasses(__NAMESPACE__ . '\event', ClassFinder::RECURSIVE_MODE) ?? [];
        }

        $eventManager->registerListerners($this->_listeners);

        parent::build();

        $this->registerRepository($this->serviceProvider->getRepositoryFactory(), $this->serviceProvider->getExtensionLauncher(), __NAMESPACE__);

        $eventManager->emit('APP_POST_BUILD');

        return $this;
    }


    /**
     * Startup action
     */
    public function boot(): self
    {
        $this->serviceProvider->getEventManager()->emit('APP_PRE_BOOT');
        //Make clean
        $this->serviceProvider->getNotification()->clean();
        $this->serviceProvider->getJavascript()->clearConfig();

        $view = $this->serviceProvider->getView();

        //Generate Token CSRF
        $csrf = $this->serviceProvider->getCsrf();
        $csrf->generateToken();
        $view->assign('CSRF', $csrf->getArray());

        //Send to router, witch populate $this->_controller
        $router = $this->serviceProvider->getDispatcher()->dispatch();

        $auth = $this->serviceProvider->getAuth();
        $router->run($auth, $this);

        //Add Assets
        $this->serviceProvider->getDi()->call(
            $this->serviceProvider->getAssets(),
            'addMainAssets'
        );

        //Check if context is active
        $session = $this->serviceProvider->getSession();
        $session->set('CONTEXT.ACTIVE', ($session->has('CONTEXT.ACTIVE') || null));

        //Check if isAdmin
        $isAdminOfContext = $session->get('CONTEXT.IS_ADMIN') ?? false;
        $session->set('CONTEXT.IS_ADMIN', $isAdminOfContext);
        $view->assign('IS_ADMIN_OF_CONTEXT', $isAdminOfContext);
        $view->assign('IS_CONNECTED', $auth->isConnected());
        $view->assign('CONNECTED_USER', $auth->getUser()?->first_name);
        $view->assign('DATE_FORMAT', Utils::getDateFormat());

        //Get Page Name && MODE
        $request     = $this->serviceProvider->getRequest();
        $visitedPage = $request->getPage() ?? 'My Page title';
        $view->assign('PAGE', $visitedPage);
        $view->assign('MODE', $this->config->find('ENVIRONNEMENT'));

        //Set page title
        $constant       = 'MENU_TITLE_' . strtoupper($visitedPage);
        $actualPageName = (defined($constant)) ? ' - ' . constant($constant) : '';
        $view->assign('TITLE', $this->config->find('CONTEXT.TITLE') . $actualPageName);

        //Get language for interface
        $language        = 'fr-FR';
        $languageHTML    = 'fr';
        $sessionLanguage = $session->get('USER.LANGUAGE');
        if ('en' === $sessionLanguage) {
            $language     = 'en-US';
            $languageHTML = 'en';
        }

        $view->assign('LANGUAGE', $language);
        $view->assign('HTML_LANG', $languageHTML);
        $view->assign('ASSETS', $this->serviceProvider->getAssets());

        //Generate URLS
        $baseUrl = $this->config->find('PHP.HTTP_PROTOCOL') . '://' . $this->config->find('PHP.SERVER_NAME');
        $view->assign('SITEURL', $baseUrl . $request->getUrl());
        $view->assign('ADDRESS', $baseUrl);
        $view->assign('ENVIRONNEMENT', $this->config->find('ENVIRONNEMENT'));


        //Generate socket
        $this->_assignSocketChannels($this->serviceProvider->getWebsocket());

        $view->assign('isAjaxify', $request->isAjaxify());
        $view->assign('IS_PUBLIC', $this->getController()->isPublic());

        $this->serviceProvider->getEventManager()->emit('APP_POST_BOOT');

        return $this;
    }

    /**
     * Render view
     */
    public function process(): ?HttpResponseInterface
    {
        $this->serviceProvider->getEventManager()->emit('APP_PRE_PROCESS');
        $this->getDi()->call($this->_controller, 'render');
        $response = $this->_controller->display();
        $this->serviceProvider->getJavascript()->saveConfig();
        $this->serviceProvider->getEventManager()->emit('APP_POST_PROCESS');

        return $response;
    }

    /**
     * Shutdown callbacks, generaly destroy all objects
     *
     * @return void
     */
    public function shutdown()
    {
        //$this->getDi()->__destruct();
    }


    /**
     * Get config
     *
     * @PhpUnitGen\get("config")
     */
    public function getConfig(): Config
    {
        return $this->config;
    }

    /**
     * Return array of registred factories
     */
    public function getFactories(): array
    {
        if (!is_array($this->_factories)) {
            /** @var ClassFinder $finder */
            $finder           = $this->getDi()->singleton(ClassFinder::class);
            $classes          = $finder->findClasses('core\html', ClassFinder::RECURSIVE_MODE) ?? [];
            $this->_factories = [];
            foreach ($classes as $class) {
                $reflection = new ReflectionClass($class);

                if ($reflection->implementsInterface(FactoryInterface::class)) {
                    $this->_factories[] = $reflection->getName();
                }
            }
        }

        return $this->_factories;
    }

    /**
     * Get controllerObject
     *
     * @PhpUnitGen\get("_controller")
     */
    public function getController(): ?ControllerInterface
    {
        return $this->_controller;
    }

    public function getServiceProvider(): ServiceProvider
    {
        return $this->serviceProvider;
    }

    /**
     * Set controllerObject
     *
     * @PhpUnitGen\set("_controller")
     *
     * @param ControllerInterface $controller ControllerObject
     */
    public function setController(ControllerInterface $controller): self
    {
        $this->_controller = $controller;

        return $this;
    }

    /**
     * Add assets to includes in view
     *
     * @return void
     */
    private function _addAssets()
    {
        $assets = $this->serviceProvider->getAssets();
        $view   = $this->serviceProvider->getView();

        //$assets->setIsPublic($this->_controller->isPublic());
        //$assets->addMainScripts();
        //$this->getDi()->call($assets, 'addForm');
        //$this->getDi()->call($assets, 'addDatatables');
        //$this->getDi()->call($assets, 'addFullcalendar');
        //$this->getDi()->call($assets, 'addPriceTable');
        //$this->getDi()->call($assets, 'addBootstrapTour');

        //Generate custom color styles
        $view->assign('CUSTOM_COLORS', $this->config->find('CUSTOM_COLORS'));

        //Generate fathom && Posthog script in prod
        $view->assign('FATHOM', $this->getDi()->call($assets, 'getFathom'));
        $view->assign('POSTHOG', $this->getDi()->call($assets, 'getPosthog'));
    }

    /**
     * Assign url && channels for websocket connection
     *
     * @return void
     */
    private function _assignSocketChannels(WebsocketServiceInterface $websocket)
    {
        $auth = $this->serviceProvider->getAuth();
        $view = $this->serviceProvider->getView();

        $view->assign('URLSOCKET', $this->config->find('PHP.WEBSOCKET_URL'));
        $view->assign('CHANNEL', $websocket->getUserChannel($auth->getUser()?->iduser ?? null));
        $view->assign('MAINCHANNEL', $websocket->getContextChannel($auth->getContext()?->idcontext ?? null));
        $view->assign('PAGECHANNEL', $websocket->getPageChannel($this->serviceProvider->getRequest()->getPage()));
    }
}
