<?php

namespace app\data;

use core\cache\CacheServiceInterface;
use core\cache\CacheUpdaterTrait;

use core\config\Config;
use DateInterval;

/**
 * Class Dataset
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Dataset
{
    use CacheUpdaterTrait;

    public const REVERSE = true;

    private ?array $_hourlist = [];

    public function __construct(private CacheServiceInterface $cache, private Config $config)
    {
        $this->_fetchPropsFromCache(['_hourList']);
    }

    public function __destruct()
    {
        $this->_cacheMultiplesProps(['_hourList']);
    }

    /**
     * @param $reverse
     * @return mixed
     */
    public static function get(string $listName, $reverse = false)
    {
        $datas = call_user_func_array([self::class, "_$listName"], []);

        return ($reverse) ? array_flip($datas) : $datas;
    }

    private static function _amperageTypes()
    {
        return [
            '16A ' . _('Single-phase') => '16A_MONO',
            '32A ' . _('Single-phase') => '32A_MONO',
            '40A ' . _('Single-phase') => '40A_MONO',
            '63A ' . _('Single-phase') => '63A_MONO',
            '32A ' . _('three-phase')  => '32A__TRI',
            '63A ' . _('three-phase')  => '63A_TRI',
            '96A ' . _('three-phase')  => '96A_TRI',
            '125A ' . _('three-phase') => '125A_TRI',
            '160A ' . _('three-phase') => '160_TRI'
        ];
    }

    /**
     * @return array{MO: string, TU: string, WE: string, TH: string, FR: string, SA: string, SU: string}
     */
    private static function _availableDays()
    {
        return [
            'MO' => _('Monday'),
            'TU' => _('Tuesday'),
            'WE' => _('Wednesday'),
            'TH' => _('Thursday'),
            'FR' => _('Friday'),
            'SA' => _('Saturday'),
            'SU' => _('Sunday')
        ];
    }

    private static function _civilityTypes()
    {
        return [1 => _('Mister'), 2 => _('Madam'), 3 => _('Non gender')];
    }

    private static function _contractTypes()
    {
        return [
            5  => _('Collaborator contract'),
            6  => _('Service contract'),
            -1 => _('Undefined document')
        ];
    }

    private static function _costPaymentMethods()
    {
        return [
            'Credit card' => 'payment_cb',
            'Cash money'  => 'payment_money',
            'Check'       => 'payment_chek'
        ];
    }

    private static function _costStates()
    {
        return [
            _('Lack of proof') => 4,
            _('To refund')     => 1,
            _('Refunded')      => 2,
            _('Invalid')       => 3
        ];
    }

    private static function _costTypes()
    {
        return [
            _('Cost') . ' - ' . _('Lunch')          => 'cost_lunch',
            _('Cost') . ' - ' . _('Accommodations') => 'cost_sleep',
            _('Cost') . ' - ' . _('Transport')      => 'cost_tranport'
        ];
    }

    /**
     * @return array{FR: string, AF: string, ZA: string, AX: string, AL: string, DZ: string, DE: string, AD: string, AO: string, AI: string, AQ: string, AG: string, AN: string, SA: string, AR: string, AM: string, AW: string, AU: string, AT: string, AZ: string, BS: string, BH: string, BD: string, BB: string, BE: string, BZ: string, BJ: string, BM: string, BT: string, BY: string, MM: string, BO: string, BA: string, BW: string, BR: string, BN: string, BG: string, BF: string, BI: string, KH: string, CM: string, CA: string, CV: string, CF: string, CL: string, CN: string, CY: string, CO: string, KM: string, CG: string, CD: string, KR: string, KP: string, CR: string, CI: string, HR: string, CU: string, DK: string, DJ: string, DM: string, EG: string, AE: string, EC: string, ER: string, ES: string, EE: string, US: string, ET: string, FJ: string, FI: string, GA: string, GM: string, GE: string, GS: string, GH: string, GI: string, GR: string, GD: string, GL: string, GP: string, GU: string, GT: string, GG: string, GN: string, GW: string, GQ: string, GY: string, GF: string, HT: string, HN: string, HK: string, HU: string, BV: string, CX: string, IM: string, KY: string, CC: string, CK: string, FO: string, FK: string, MP: string, UM: string, SB: string, TC: string, VI: string, VG: string, IN: string, ID: string, IQ: string, IR: string, IE: string, IS: string, IL: string, IT: string, JM: string, JP: string, JE: string, JO: string, KZ: string, KE: string, KG: string, KI: string, KW: string, LA: string, LS: string, LV: string, LB: string, LR: string, LY: string, LI: string, LT: string, LU: string, MO: string, MK: string, MG: string, MY: string, MW: string, MV: string, ML: string, MT: string, MA: string, MH: string, MQ: string, MU: string, MR: string, YT: string, MX: string, FM: string, MD: string, MC: string, MN: string, ME: string, MS: string, MZ: string, NA: string, NR: string, NP: string, NI: string, NE: string, NG: string, NU: string, NF: string, NO: string, NC: string, NZ: string, OM: string, UG: string, UZ: string, PK: string, PW: string, PS: string, PA: string, PG: string, PY: string, NL: string, PE: string, PH: string, PN: string, PL: string, PF: string, PR: string, PT: string, QA: string, DO: string, CZ: string, RE: string, RO: string, GB: string, RU: string, RW: string, EH: string, KN: string, SH: string, LC: string, SM: string, PM: string, VC: string, SV: string, WS: string, AS: string, ST: string, SN: string, RS: string, SC: string, SL: string, SG: string, SK: string, SI: string, SO: string, SD: string, LK: string, SE: string, CH: string, SR: string, SJ: string, SZ: string, SY: string, TJ: string, TW: string, TZ: string, TD: string, TF: string, IO: string, __: string, TH: string, TL: string, TG: string, TK: string, TO: string, TT: string, TN: string, TM: string, TR: string, TV: string, UA: string, UY: string, VU: string, VA: string, VE: string, VN: string, WF: string, YE: string, ZM: string, ZW: string}
     */
    private static function _countryList()
    {
        return [

            'FR' => _('France'),
            'AF' => _('Afghanistan'),
            'ZA' => _('South Africa'),
            'AX' => _('Åland'),
            'AL' => _('Albania'),
            'DZ' => _('Algeria'),
            'DE' => _('Germany'),
            'AD' => _('Andorra'),
            'AO' => _('Angola'),
            'AI' => _('Anguilla'),
            'AQ' => _('Antarctica'),
            'AG' => _('Antigua and Barbuda'),
            'AN' => _('Netherlands Antilles'),
            'SA' => _('Saudi Arabia'),
            'AR' => _('Argentina'),
            'AM' => _('Armenia'),
            'AW' => _('Aruba'),
            'AU' => _('Australia'),
            'AT' => _('Austria'),
            'AZ' => _('Azerbaijan'),
            'BS' => _('Bahamas'),
            'BH' => _('Bahrain'),
            'BD' => _('Bangladesh'),
            'BB' => _('Barbados'),
            'BE' => _('Belgium'),
            'BZ' => _('Belize'),
            'BJ' => _('Benin'),
            'BM' => _('Bermuda'),
            'BT' => _('Bhutan'),
            'BY' => _('Belarus'),
            'MM' => _('Burma'),
            'BO' => _('Bolivia'),
            'BA' => _('Bosnia and Herzegovina'),
            'BW' => _('Botswana'),
            'BR' => _('Brazil'),
            'BN' => _('Brunei'),
            'BG' => _('Bulgaria'),
            'BF' => _('Burkina Faso'),
            'BI' => _('Burundi'),
            'KH' => _('Cambodia'),
            'CM' => _('Cameroon'),
            'CA' => _('Canada'),
            'CV' => _('Cape Verde'),
            'CF' => _('Central African Republic'),
            'CL' => _('Chile'),
            'CN' => _('China'),
            'CY' => _('Cyprus'),
            'CO' => _('Colombia'),
            'KM' => _('Comoros'),
            'CG' => _('Congo-Brazzaville'),
            'CD' => _('Congo-Kinshasa'),
            'KR' => _('North Korea'),
            'KP' => _('South Korea'),
            'CR' => _('Costa Rica'),
            'CI' => _('Côte d\'Ivoire'),
            'HR' => _('Croatia'),
            'CU' => _('Cuba'),
            'DK' => _('Denmark'),
            'DJ' => _('Djibouti'),
            'DM' => _('Dominica'),
            'EG' => _('Egypt'),
            'AE' => _('United Arab Emirates'),
            'EC' => _('Ecuador'),
            'ER' => _('Eritrea'),
            'ES' => _('Spain'),
            'EE' => _('Estonia'),
            'US' => _('United States'),
            'ET' => _('Ethiopia'),
            'FJ' => _('Fiji'),
            'FI' => _('Finland'),
            'GA' => _('Gabon'),
            'GM' => _('Gambia'),
            'GE' => _('Georgia'),
            'GS' => _('South Georgia and the South Sandwich Islands'),
            'GH' => _('Ghana'),
            'GI' => _('Gibraltar'),
            'GR' => _('Greece'),
            'GD' => _('Grenada'),
            'GL' => _('Greenland'),
            'GP' => _('Guadeloupe'),
            'GU' => _('Guam'),
            'GT' => _('Guatemala'),
            'GG' => _('Guernsey'),
            'GN' => _('Guinea'),
            'GW' => _('Equatorial Guinea'),
            'GQ' => _('Guinea-Bissau'),
            'GY' => _('Guyana'),
            'GF' => _('Guyana'),
            'HT' => _('Haiti'),
            'HN' => _('Honduras'),
            'HK' => _('Hong Kong'),
            'HU' => _('Hungary'),
            'BV' => _('Bouvet Island'),
            'CX' => _('Christmas Island'),
            'IM' => _('Isle of Man'),
            'KY' => _('Cayman Islands'),
            'CC' => _('Cocos'),
            'CK' => _('Cook Islands'),
            'FO' => _('Faroe Islands'),
            'FK' => _('Islas Malvinas (Falkland)'),
            'MP' => _('Northern Mariana Islands'),
            'UM' => _('Minor Outlying Islands of the United States'),
            'SB' => _('Solomon Islands'),
            'TC' => _('Turks and Caicos Islands'),
            'VI' => _('United States Virgin Islands'),
            'VG' => _('BVI'),
            'IN' => _('India'),
            'ID' => _('Indonesia'),
            'IQ' => _('Iraq'),
            'IR' => _('Iran'),
            'IE' => _('Ireland'),
            'IS' => _('Iceland'),
            'IL' => _('Israel'),
            'IT' => _('Italy'),
            'JM' => _('Jamaica'),
            'JP' => _('Japan'),
            'JE' => _('Jersey'),
            'JO' => _('Jordan'),
            'KZ' => _('Kazakhstan'),
            'KE' => _('Kenya'),
            'KG' => _('Kyrgyzstan'),
            'KI' => _('Kiribati'),
            'KW' => _('Kuwait'),
            'LA' => _('Laos'),
            'LS' => _('Lesotho'),
            'LV' => _('Latvia'),
            'LB' => _('Lebanon'),
            'LR' => _('Liberia'),
            'LY' => _('Libya'),
            'LI' => _('Liechtenstein'),
            'LT' => _('Lithuania'),
            'LU' => _('Luxembourg'),
            'MO' => _('Macao'),
            'MK' => _('Macedonia'),
            'MG' => _('Madagascar'),
            'MY' => _('Malaysia'),
            'MW' => _('Malawi'),
            'MV' => _('Maldives'),
            'ML' => _('Mali'),
            'MT' => _('Malta'),
            'MA' => _('Morocco'),
            'MH' => _('Marshall'),
            'MQ' => _('Martinique'),
            'MU' => _('Mauritius'),
            'MR' => _('Mauritania'),
            'YT' => _('Mayotte'),
            'MX' => _('Mexico'),
            'FM' => _('Micronesia'),
            'MD' => _('Moldova'),
            'MC' => _('Monaco'),
            'MN' => _('Mongolia'),
            'ME' => _('Montenegro'),
            'MS' => _('Montserrat'),
            'MZ' => _('Mozambique'),
            'NA' => _('Namibia'),
            'NR' => _('Nauru'),
            'NP' => _('Nepal'),
            'NI' => _('Nicaragua'),
            'NE' => _('Niger'),
            'NG' => _('Nigeria'),
            'NU' => _('Niue'),
            'NF' => _('Norfolk'),
            'NO' => _('Norway'),
            'NC' => _('New Caledonia'),
            'NZ' => _('New Zealand'),
            'OM' => _('Oman'),
            'UG' => _('Uganda'),
            'UZ' => _('Uzbekistan'),
            'PK' => _('Pakistan'),
            'PW' => _('Palau'),
            'PS' => _('Palestine'),
            'PA' => _('Panamá'),
            'PG' => _('Papua New Guinea'),
            'PY' => _('Paraguay'),
            'NL' => _('Netherlands'),
            'PE' => _('Peru'),
            'PH' => _('Philippines'),
            'PN' => _('Pitcairn'),
            'PL' => _('Poland'),
            'PF' => _('French Polynesia'),
            'PR' => _('Puerto Rico'),
            'PT' => _('Portugal'),
            'QA' => _('Qatar'),
            'DO' => _('Dominican Republic'),
            'CZ' => _('Czech Republic'),
            'RE' => _('Reunion'),
            'RO' => _('Romania'),
            'GB' => _('United Kingdom'),
            'RU' => _('Russia'),
            'RW' => _('Rwanda'),
            'EH' => _('Western Sahara'),
            'KN' => _('St. Christopher St Kitts and Nevis'),
            'SH' => _('St. Helena'),
            'LC' => _('St. Lucia'),
            'SM' => _('San Marino'),
            'PM' => _('Saint-Pierre and Miquelon'),
            'VC' => _('Saint Vincent and the Grenadines'),
            'SV' => _('Salvador'),
            'WS' => _('Samoa'),
            'AS' => _('American Samoa'),
            'ST' => _('Sao Tome and Principe'),
            'SN' => _('Senegal'),
            'RS' => _('Serbia'),
            'SC' => _('Seychelles'),
            'SL' => _('Sierra Leone'),
            'SG' => _('Singapore'),
            'SK' => _('Slovakia'),
            'SI' => _('Slovenia'),
            'SO' => _('Somalia'),
            'SD' => _('Sudan'),
            'LK' => _('Sri Lanka'),
            'SE' => _('Sweden'),
            'CH' => _('Switzerland'),
            'SR' => _('Suriname'),
            'SJ' => _('Svalbard and Jan Mayen Island'),
            'SZ' => _('Swaziland'),
            'SY' => _('Syria'),
            'TJ' => _('Tajikistan'),
            'TW' => _('Taiwan'),
            'TZ' => _('Tanzania'),
            'TD' => _('Chad'),
            'TF' => _('French Southern and Antarctic Territories'),
            'IO' => _('British Indian Ocean'),
            '__' => _('External territories of Australia'),
            'TH' => _('Thailand'),
            'TL' => _('East Timor'),
            'TG' => _('Togo'),
            'TK' => _('Tokelau'),
            'TO' => _('Tonga'),
            'TT' => _('Trinidad and Tobago'),
            'TN' => _('Tunisia'),
            'TM' => _('Turkmenistan'),
            'TR' => _('Turkey'),
            'TV' => _('Tuvalu'),
            'UA' => _('Ukraine'),
            'UY' => _('Uruguay'),
            'VU' => _('Vanuatu'),
            'VA' => _('Vatican'),
            'VE' => _('Venezuela'),
            'VN' => _('Vietnam'),
            'WF' => _('Wallis and Futuna'),
            'YE' => _('Yemen'),
            'ZM' => _('Zambia'),
            'ZW' => _('Zimbabwe')
        ];
    }

    /**
     * @return array{euro: string, dollar: string}
     */
    private static function _currencyTypes()
    {
        return ['euro' => '€', 'dollar' => '$'];
    }

    /**
     * @return mixed
     */
    private static function _deadlineModules()
    {
        $type                                = [];
        $type[_('Performance')]              = 'performance';
        $type[_('Administrative documents')] = 'invoice';
        $type[_('Transport')]                = 'transport';
        $type[_('Task')]                     = 'task';

        $f_modules = [];
        foreach ($type as $key => $module) {
            $f_modules[$key] = $module->_id;
        }

        return $f_modules;
    }

    private static function _escapeUnits()
    {
        return [
            sprintf(ngettext('%d passage unit', '%d passage units', 1), 1)             => '1',
            sprintf(ngettext('%d passage unit', '%d passage units', 1), 1) . ' + 0,60' => '1_5',
            sprintf(ngettext('%d passage unit', '%d passage units', 2), 2)             => '2',
            sprintf(ngettext('%d passage unit', '%d passage units', 3), 3)             => '3',
            sprintf(ngettext('%d passage unit', '%d passage units', 4), 4)             => '4',
            sprintf(ngettext('%d passage unit', '%d passage units', 5), 5)             => '5',
            sprintf(ngettext('%d passage unit', '%d passage units', 6), 6)             => '6',
            sprintf(ngettext('%d passage unit', '%d passage units', 7), 7)             => '7',
            sprintf(ngettext('%d passage unit', '%d passage units', 8), 8)             => '8',
            sprintf(ngettext('%d passage unit', '%d passage units', 9), 9)             => '9',
            sprintf(ngettext('%d passage unit', '%d passage units', 10), 10)           => '10',
            sprintf(ngettext('%d passage unit', '%d passage units', 11), 11)           => '11',
            sprintf(ngettext('%d passage unit', '%d passage units', 12), 12)           => '12'
        ];
    }

    private static function _fileExtensions()
    {
        return [
            '.jpg'  => 'image',
            '.JPG'  => 'image',
            '.gif'  => 'image',
            '.GIF'  => 'image',
            '.png'  => 'image',
            '.PNG'  => 'image',
            '.jpeg' => 'image',
            '.JPEG' => 'image',
            '.PNG'  => 'image',
            '.png'  => 'image',
            '.bmp'  => 'image',
            '.BMP'  => 'image',
            '.tif'  => 'image',
            '.tiff' => 'image',
            '.TIF'  => 'image',
            '.TIFF' => 'image',
            '.avi'  => 'video',
            '.AVI'  => 'video',
            '.mpeg' => 'video',
            '.MPEG' => 'video',
            '.mkv'  => 'video',
            '.MKV'  => 'video',
            '.mp4'  => 'video',
            '.MP4'  => 'video',
            '.avi'  => 'video',
            '.PDF'  => 'pdf',
            '.PDF'  => 'video',
            '.txt'  => 'text',
            '.TXT'  => 'text',
            '.doc'  => 'text',
            '.DOC'  => 'text',
            '.docx' => 'text',
            '.DOCX' => 'text',
            '.rtf'  => 'text',
            '.RTF'  => 'text',
            '.xls'  => 'text',
            '.XLS'  => 'text',
            '.xlsx' => 'text',
            '.xslx' => 'text',
            '.csv'  => 'text',
            '.CSV'  => 'text',
            '.ods'  => 'text',
            '.ODS'  => 'text',
            '.ODT'  => 'text',
            '.odt'  => 'text',
            '.ppt'  => 'text',
            '.PPT'  => 'text',
            '.pptx' => 'text',
            '.PPTX' => 'text',
            '.odp'  => 'text',
            '.ODP'  => 'text',
            '.html' => 'html',
            '.html' => 'html',
            '.htm'  => 'html',
            '.HTM'  => 'html',
            '.pdf'  => 'pdf',
            '.PDF'  => 'pdf'
        ];
    }

    /**
     * @return array{contrete: string, wood: string, floor_tile: string, carpet: string, linen: string, pavement: string, plaster: string, lawn: string, other: string}
     */
    private static function _floorMaterials()
    {
        return [
            'contrete'   => _('Concrete'),
            'wood'       => _('Wood'),
            'floor_tile' => _('Floor tile'),
            'carpet'     => _('Carpet'),
            'linen'      => _('Linen'),
            'pavement'   => _('Pavement'),
            'plaster'    => _('Plaster'),
            'lawn'       => _('Lawn'),
            'other'      => _('Other'),
        ];
    }

    private static function _floorTypes()
    {
        return [
            'gnd'  => _('Ground floor'),
            '1st'  => _('1st floor'),
            '2nd ' => _('2nd floor'),
            '3rd ' => _('3rd floor'),
            '4rd ' => _('4rd floor'),
            '5th ' => _('5th floor')
        ];
    }

    /**
     * @return array{P1D: string, P7D: string, P14D: string, P21D: string, P1M: string, P35D: string, P42D: string, P49D: string, P2M: string, P3M: string, P4M: string, P6M: string, P1Y: string, P2Y: string}
     */
    private static function _templateRecurrence()
    {
        return [
            'P1D'  => _('Everyday'),
            'P7D'  => sprintf(ngettext('Every %d week', 'Every %d weeks', 1), 1),
            'P14D' => sprintf(ngettext('Every %d week', 'Every %d weeks', 2), 2),
            'P21D' => sprintf(ngettext('Every %d week', 'Every %d weeks', 3), 3),
            'P1M'  => sprintf(ngettext('Every %d month', 'Every %d months', 1), 1),
            'P35D' => sprintf(ngettext('Every %d week', 'Every %d weeks', 5), 5),
            'P42D' => sprintf(ngettext('Every %d week', 'Every %d weeks', 6), 6),
            'P49D' => sprintf(ngettext('Every %d week', 'Every %d weeks', 7), 7),
            'P2M'  => sprintf(ngettext('Every %d month', 'Every %d months', 2), 2),
            'P3M'  => sprintf(ngettext('Every %d month', 'Every %d months', 3), 3),
            'P4M'  => sprintf(ngettext('Every %d month', 'Every %d months', 4), 4),
            'P6M'  => sprintf(ngettext('Every %d month', 'Every %d months', 6), 6),
            'P1Y'  => sprintf(ngettext('Every %d year', 'Every %d years', 1), 1),
            'P2Y'  => sprintf(ngettext('Every %d year', 'Every %d years', 2), 2),
        ];
    }

    /**
     * @return array{YEARLY: string, MONTHLY: string, WEEKLY: string, DAILY: string}
     */
    private static function _frequencyTypes()
    {
        return [
            'YEARLY'  => _('Yearly'),
            'MONTHLY' => _('Monthly'),
            'WEEKLY'  => _('Weekly'),
            'DAILY'   => _('Dayly')
        ];
    }

    /**
     * @return mixed
     */
    private static function _hours()
    {
        $i      = 0;
        $return = [];
        while ($i <= 23) {
            $return[] = $i;
            ++$i;
        }

        return $return;
    }

    private static function _hoursList(): array
    {
        if (is_array(self::$_hourlist)) {
            return self::$_hourlist;
        }

        $initial         = new DateInterval('F');
        $initial->m      = 10;
        self::$_hourlist = [$initial->format('F') => sprintf(
            ngettext('%d day', '%d days', $initial->format('d')) . ' ' .
                ngettext('%d hour', '%d hours', $initial->format('h')) . ' ' .
                ngettext('%d minute', '%d minutes', $initial->format('i')) . ' ',
            $initial->format('d'),
            $initial->format('h'),
            $initial->format('i')
        )
        ];

        for ($new = clone $initial; $new->d < 36; $new->m += 10) {
            self::$_hourlist[$new->format('F')] = sprintf(
                ngettext('%d day', '%d days', $new->format('d')) . ' ' .
                ngettext('%d hour', '%d hours', $new->format('h')) . ' ' .
                ngettext('%d minute', '%d minutes', $new->format('i')) . ' ',
                $new->format('d'),
                $new->format('h'),
                $new->format('i')
            );
        }

        return self::$_hourlist;
    }

    /**
     * @return array{performance: string[], place: string[], compagny: string[], material: string[], type: string[], stock: string[], room: string[], user: string[]}
     */
    private static function _importKeys()
    {
        return [
            'performance' => [
                'name',
                'various',
                'start',
                'end'
            ],
            'place' => [
                'mark',
                'address',
                'city',
                'city_code',
                'country',
                'unloading_adress',
                'schedule'
            ],
            'compagny' => [
                'name',
                'address',
                'city',
                'city_code',
                'country',
                'siret',
                'iban',
                'type'
            ],
            'material' => [
                'ref',
                'designation',
                'weight',
                'm3'
            ],
            'type' => [
                'name',
                'color'
            ],
            'stock' => [
                'idmaterial',
                'quantity'
            ],
            'room' => [
                'idmaterial',
                'quantity'
            ],
            'user' => [
                'civility',
                'name',
                'first_name',
                'address',
                'city',
                'city_code',
                'country',
                'date_birth',
                'place_birth',
                'num_ss',
                'num_cg_spect',
                'iban',
                'email',
                'phone'
            ]
        ];
    }

    /**
     * @return array{LOCKED: int, AUTOCONFIG: int, NEEDVALIDATION: int, UPDATEMAIN: int}
     */
    private static function _contextModes()
    {
        return [
            'LOCKED'         => 1,
            'AUTOCONFIG'     => 2,
            'NEEDVALIDATION' => 3,
            'UPDATEMAIN'     => 4
        ];
    }

    private static function _invoiceTypes()
    {
        return [
            1  => _('Down payment invoice'),
            2  => _('Invoice balance'),
            3  => _('Quotation'),
            4  => _('Commercial proposition'),
            5  => _('Collaborator contract'),
            6  => _('Service contract'),
            -1 => _('Undefined document')
        ];
    }

    /**
     * @return array{A: string, B: string, C: string, D: string, E: string, F: string, G: string, H: string, I: string, J: string, K: string, L: string, M: string, N: string, O: string, P: string, Q: string, R: string, S: string, T: string, U: string, V: string, W: string, X: string, Y: string, Z: string}
     */
    private static function _lettersList()
    {
        return [
            'A' => 'A',
            'B' => 'B',
            'C' => 'C',
            'D' => 'D',
            'E' => 'E',
            'F' => 'F',
            'G' => 'G',
            'H' => 'H',
            'I' => 'I',
            'J' => 'J',
            'K' => 'K',
            'L' => 'L',
            'M' => 'M',
            'N' => 'N',
            'O' => 'O',
            'P' => 'P',
            'Q' => 'Q',
            'R' => 'R',
            'S' => 'S',
            'T' => 'T',
            'U' => 'U',
            'V' => 'V',
            'W' => 'W',
            'X' => 'X',
            'Y' => 'Y',
            'Z' => 'Z'
        ];
    }

    /**
     * @return mixed
     */
    private static function _minutes()
    {
        $i      = 0;
        $return = [];
        while ($i <= 59) {
            $return[] = $i;
            ++$i;
        }

        return $return;
    }

    /**
     * @return mixed
     */
    private static function _monthDays()
    {
        $i      = 1;
        $return = [];
        while ($i <= 31) {
            $return[] = $i;
            ++$i;
        }

        return $return;
    }

    /**
     * @return mixed
     */
    private static function _months()
    {
        $i      = 1;
        $return = [];
        while ($i <= 12) {
            $return[] = $i;
            ++$i;
        }

        return $return;
    }

    /**
     * Defaults states
     *
     * @var array
     */
    private static function _performanceStates()
    {
        return [
            1 => _('To confirm'),
            2 => _('Confirmed'),
            3 => _('Canceled'),
            4 => _('In progress'),
            5 => _('Done'),
            6 => _('Post prod'),
            7 => _('Archived'),

        ];
    }

    /**
     * @return mixed
     */
    private static function _permissionsFormated()
    {
        $permissions = [];
        foreach ($this->config->find('CONTEXT.DEFAULT_PERMISSIONS') as $key => $value) {
            $key               = preg_replace('/ *_/', ' ', $key);
            $permissions[$key] = $value;
        }

        return $permissions;
    }

    private static function _planTypes()
    {
        return [
            sprintf(ngettext('%d user', '%d users', 1), 1)     => 1,
            sprintf(ngettext('%d user', '%d users', 5), 5)     => 5,
            sprintf(ngettext('%d user', '%d users', 10), 10)   => 10,
            sprintf(ngettext('%d user', '%d users', 25), 25)   => 25,
            sprintf(ngettext('%d user', '%d users', 50), 50)   => 50,
            sprintf(ngettext('%d user', '%d users', 100), 100) => 100
        ];
    }

    /**
     * @return array{Select: string, Switch: string, Text: string}
     */
    private static function _questionTypes()
    {
        return ['Select' => 'Select', 'Switch' => 'Switch', 'Text' => 'Text'];
    }

    private static function _roomTypes()
    {
        return [
            _('Bedroom simple') => 'solo',
            _('Bedroom double') => 'double',
            _('Bedroom triple') => 'triple',
            _('Other')          => 'other'
        ];
    }

    private static function _runnerTypes()
    {
        return [
            _('Plane')   => '1',
            _('Bus')     => '2',
            _('Boat')    => '3',
            _('Minibus') => '4',
            _('Car')     => '5',
            _('Train')   => '6',
            _('Other')   => '7'
        ];
    }

    /**
     * @return mixed
     */
    private static function _seconds()
    {
        $i      = 0;
        $return = [];
        while ($i <= 59) {
            $return[] = $i;
            ++$i;
        }

        return $return;
    }

    private static function _serviceTypes()
    {
        return [
            _('Company')  => 'compagny',
            _('Trade')    => 'trade',
            _('User')     => 'user',
            _('Place')    => 'place',
            _('Show')     => 'show',
            _('Material') => 'material'
        ];
    }

    private static function _storageValues()
    {
        return [
            '1'    => 104_857_600,
            '5'    => 1_048_576_000,
            '10'   => 1_572_864_000,
            '25'   => 10_485_760_000,
            '50'   => 20_971_520_000,
            '100'  => 52_428_800_000,
            '1000' => 52_428_800_000 * 2
        ];
    }

    private static function _transportStatus()
    {
        return [
            '-1' => _('Not valiated'),
            1    => _('Validated'),
            0    => _('Pending')
        ];
    }

    /**
     * List of VAT
     *
     * @var array
     */
    private static function _tvaList()
    {
        return [
            '1.021' => _('VAT') . ' 2,1%',
            '1.055' => _('VAT') . ' 5,5%',
            '1.1'   => _('VAT') . ' 10%',
            '1.2'   => _('VAT') . ' 20%',
            '1'     => _('VAT') . ' 0%'
        ];
    }

    private static function _typeGroupe()
    {
        return [
            '-1' => _('Not valiated'),
            1    => _('Validated'),
            0    => _('Pending')
        ];
    }

    private static function _typeList()
    {
        return [
            'all'         => _('All'),
            'band'        => _('Band'),
            'show'        => _('Show'),
            'compagny'    => _('Company'),
            'earning'     => _('Earning'),
            'event'       => _('Performance'),
            'invoice'     => _('Invoice'),
            'trade'       => _('Trade'),
            'material'    => _('Material'),
            'performance' => _('Performance'),
            'service'     => _('Service'),
            'spent'       => _('Spent'),
            'staff'       => _('Staff'),
            'patch'       => _('Patch')
        ];
    }

    private static function _unitTypes()
    {
        return [
            1 => _('By hour'),
            2 => _('By day'),
            3 => _('By product'),
            4 => _('Rent ratio')
        ];
    }

    /**
     * @return array{contrete: string, wood: string, non_existent: string, carpet: string, plasterboard: string, plaster: string, other: string}
     */
    private static function _wallMaterials()
    {
        return [
            'contrete'     => _('Concrete'),
            'wood'         => _('Wood'),
            'non_existent' => _('Non existent'),
            'carpet'       => _('Carpet'),
            'plasterboard' => _('Plasterboard'),
            'plaster'      => _('Plaster'),
            'other'        => _('Other'),
        ];
    }

    /**
     * @return mixed
     */
    private static function _yearDays()
    {
        $i      = 1;
        $return = [];
        while ($i <= 365) {
            $return[] = $i;
            ++$i;
        }

        return $return;
    }

    private static function _yesNoOptions()
    {
        return [_('Yes') => '1', _('No') => '0'];
    }
}
