<?php

namespace app;

use core\app\AbstractApplication;
use core\app\ApplicationException;
use core\app\ApplicationServiceInterface;
use core\cache\CacheServiceInterface;
use core\cache\CacheUpdaterTrait;
use core\config\Config;
use core\controller\ControllerInterface;
use core\entities\EntitieFactoryInterface;
use core\factory\FactoryInterface;
use core\messages\response\HttpResponseInterface;
use core\providers\ServiceProvider;
use core\utils\ClassFinder;
use ReflectionClass;

/**
 * Class CommandLine
 *
 * Main application object
 *
 * @category  Main CommandLine application object
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class CommandLine extends AbstractApplication implements ApplicationServiceInterface
{
    use CacheUpdaterTrait;

    /**
     * Event Listerners
     */
    private ?array $_listeners = null;

    /**
     * HtmlElement factories
     */
    private ?array $_factories = null;

    /**
     * Application Entities
     */
    private ?array $_appEntities = null;


    public function __construct(
        private ServiceProvider $serviceProvider,
        private Config $config,
        private CacheServiceInterface $cache
    ) {
        $this->_fetchPropsFromCache(
            [
                '_listeners',
                '_factories',
                '_repository',
                '_appEntities'
            ]
        );
    }

    public function __destruct()
    {
        $this->_cacheMultiplesProps(
            [
                '_listeners',
                '_factories',
                '_repository',
                '_appEntities'
            ]
        );
    }


    /**
     * Get dependencies after construction to prevent cyclic-dependencies
     */
    public function build(): self
    {
        $eventManager = $this->serviceProvider->getEventManager();
        $eventManager->emit('CLI_PRE_BUILD');

        if (null === $this->_appEntities) {
            /** @var EntitieFactoryInterface $entitieFactory */
            $entitieFactory     = $this->getDi()->singleton(EntitieFactoryInterface::class);
            $this->_appEntities = $entitieFactory->registerApplicationEntities();
        }

        if (!is_array($this->_listeners)) {
            /** @var ClassFinder $finder */
            $finder           = $this->getDi()->singleton(ClassFinder::class);
            $this->_listeners = $finder->findClasses(__NAMESPACE__ . '\event', ClassFinder::RECURSIVE_MODE) ?? [];
        }

        $eventManager->registerListerners($this->_listeners);

        parent::build();

        $this->registerRepository(
            $this->serviceProvider->getRepositoryFactory(),
            $this->serviceProvider->getExtensionLauncher(),
            __NAMESPACE__
        );

        $eventManager->emit('CLI_POST_BUILD');

        return $this;
    }

    /**
     * Startup action
     */
    public function boot(): self
    {
        $eventManager = $this->serviceProvider->getEventManager();
        $eventManager->emit('CLI_PRE_BOOT');
        $eventManager->emit('CLI_POST_BOOT');

        return $this;
    }


    /**
     * Render view
     */
    public function process(): ?HttpResponseInterface
    {
        $this->serviceProvider->getEventManager()->emit('CLI_PRE_PROCESS');
        $this->serviceProvider->getEventManager()->emit('CLI_POST_PROCESS');

        return null;
    }

    /**
     * Shutdown callbacks, generaly destroy all objects
     *
     * @return void
     */
    public function shutdown()
    {
        //$this->getDi()->__destruct();
    }

    /**
     * Return array of registred factories
     */
    public function getFactories(): array
    {
        if (!is_array($this->_factories)) {
            /** @var ClassFinder $finder */
            $finder           = $this->getDi()->singleton(ClassFinder::class);
            $classes          = $finder->findClasses('core\html', ClassFinder::RECURSIVE_MODE) ?? [];
            $this->_factories = [];
            foreach ($classes as $class) {
                $reflection = new ReflectionClass($class);
                if ($reflection->implementsInterface(FactoryInterface::class)) {
                    $this->_factories[] = $reflection->getName();
                }
            }
        }

        return $this->_factories;
    }

    /**
     * Get config
     *
     * @PhpUnitGen\get("config")
     */
    public function getConfig(): Config
    {
        return $this->config;
    }


    public function getServiceProvider(): ServiceProvider
    {
        return $this->serviceProvider;
    }

    public function setController(ControllerInterface $controller)
    {
        throw new ApplicationException('You cannot use this method ' . __METHOD__ . ' in ' . self::class);
    }
}
