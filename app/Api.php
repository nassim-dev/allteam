<?php

namespace app;

use core\app\AbstractApplication;
use core\app\ApplicationServiceInterface;
use core\cache\CacheServiceInterface;
use core\cache\CacheUpdaterTrait;
use core\config\Config;
use core\controller\ControllerInterface;
use core\entities\EntitieFactoryInterface;
use core\factory\FactoryInterface;
use core\logger\application\TracyApplicationPanel;
use core\messages\response\HttpResponseInterface;
use core\providers\ServiceProvider;
use core\utils\ClassFinder;
use ReflectionClass;
use Tracy\Debugger;

/**
 * Class Api
 *
 * Main api application object
 *
 * @category  Main api application object
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Api extends AbstractApplication implements ApplicationServiceInterface
{
    use CacheUpdaterTrait;

    /**
     * ControllerObject
     */
    private ?\core\controller\ControllerInterface $_controller = null;

    /**
     * Event Listerners
     */
    private ?array $_listeners = null;

    /**
     * HtmlElement factories
     */
    private ?array $_factories = null;

    /**
     * True if data mus be encrypted
     */
    private bool $encryptedDatas = false;

    private $_appEntities;

    public function __construct(
        protected ServiceProvider $serviceProvider,
        protected Config $config,
        private CacheServiceInterface $cache
    ) {
        $this->_fetchPropsFromCache(
            [
                '_listeners',
                '_factories',
                '_repository',
                '_appEntities'
            ]
        );

        if (
            $this->config->find('ENVIRONNEMENT') === 'development'
            && CONTEXT === 'APP'
            && !isset($_SERVER['x-requested-with'])
        ) {
            Debugger::getBar()->addPanel(new TracyApplicationPanel($this));
        }
    }

    public function __destruct()
    {
        $this->_cacheMultiplesProps(['_listeners', '_factories', '_repository', '_appEntities']);
    }

    /**
     * Get dependencies after construction to prevent cyclic-dependencies
     */
    public function build(): self
    {
        $eventManager = $this->serviceProvider->getEventManager();
        $eventManager->emit('API_PRE_BUILD');

        if (null === $this->_appEntities) {
            /** @var EntitieFactoryInterface $entitieFactory */
            $entitieFactory     = $this->getDi()->singleton(EntitieFactoryInterface::class);
            $this->_appEntities = $entitieFactory->registerApplicationEntities();
        }

        if (!is_array($this->_listeners)) {
            /** @var ClassFinder $finder */
            $finder           = $this->getDi()->singleton(ClassFinder::class);
            $this->_listeners = $finder->findClasses(__NAMESPACE__ . '\event', ClassFinder::RECURSIVE_MODE) ?? [];
        }

        $eventManager->registerListerners($this->_listeners);

        parent::build();

        $this->registerRepository(
            $this->serviceProvider->getRepositoryFactory(),
            $this->serviceProvider->getExtensionLauncher(),
            __NAMESPACE__
        );

        $eventManager->emit('API_POST_BUILD');

        return $this;
    }

    /**
     * Startup action
     */
    public function boot(): self
    {
        $this->serviceProvider->getEventManager()->emit('API_PRE_BOOT');
        //Send To router, witch populate $this->_controller

        $router = $this->getServiceProvider()->getDispatcher()->dispatch();
        $auth   = $this->serviceProvider->getAuth();
        $router->run($auth, $this);

        $this->serviceProvider->getEventManager()->emit('API_POST_BOOT');

        return $this;
    }

    /**
     * Render view
     *
     * @return null|HttpResponseInterface
     */
    public function process(): ?HttpResponseInterface
    {
        $this->serviceProvider->getEventManager()->emit('API_PRE_PROCESS');
        $this->serviceProvider->getJavascript()->saveConfig();
        $this->getDi()->call($this->_controller, 'render');
        $response = $this->_controller->display();
        $this->serviceProvider->getEventManager()->emit('API_POST_PROCESS');

        return $response;
    }

    /**
     * Shutdown callbacks, generaly destroy all objects
     *
     * @return void
     */
    public function shutdown()
    {
        //$this->getDi()->__destruct();
    }

    /**
     * Return array of registred factories
     */
    public function getFactories(): array
    {
        if (!is_array($this->_factories)) {
            /** @var ClassFinder $finder */
            $finder           = $this->getDi()->singleton(ClassFinder::class);
            $classes          = $finder->findClasses('core\html', ClassFinder::RECURSIVE_MODE) ?? [];
            $this->_factories = [];
            foreach ($classes as $class) {
                $reflection = new ReflectionClass($class);
                if ($reflection->implementsInterface(FactoryInterface::class)) {
                    $this->_factories[] = $reflection->getName();
                }
            }
        }

        return $this->_factories;
    }

    /**
     * Get controllerObject
     *
     * @PhpUnitGen\get("_controller")
     */
    public function getController(): ?ControllerInterface
    {
        return $this->_controller;
    }

    public function getServiceProvider(): ServiceProvider
    {
        return $this->serviceProvider;
    }

    /**
     * Get config
     *
     * @PhpUnitGen\get("config")
     */
    public function getConfig(): Config
    {
        return $this->config;
    }

    /**
     * Set encryptedDatas
     *
     * @PhpUnitGen\set("encryptedDatas")
     */
    public function setEncryptedDatas(bool $status): self
    {
        $this->encryptedDatas = $status;

        return $this;
    }


    /**
     * Set controllerObject
     *
     * @PhpUnitGen\set("_controller")
     *
     * @param ControllerInterface $controller ControllerObject
     */
    public function setController(ControllerInterface $controller): self
    {
        $this->_controller = $controller;

        return $this;
    }
}
