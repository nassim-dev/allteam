<?php namespace app\widgets\form;

/**
 * Class PlaceFormWidget
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class PlaceFormWidget extends \core\html\form\AbstractFormTemplate implements \core\entities\WidgetInterface
{
    /**
     * Return Form Add Place
     *
     * @PhpUnitGen\assertInstanceOf("Form::class")
     */
    public function add(\core\html\CommandFactory $commandFactory): \core\html\form\Form
    {
        $form = $this->getDi()->make(\core\html\form\Form::class, [
            'name'   => 'create_place',
            'method' => 'POST',
            'action' => '#'
        ]);

        $form->setValue(\_('Save'));

        $form->on('submit')
            ->command($commandFactory->create(
                \core\html\form\command\SubmitCommand::class,
                [
                    'target'   => $form,
                    'method'   => 'POST',
                    'endpoint' => '/api/' . $this->getRessource() . '/create'
                ]
            ));

        $form->setTitle(\_('Add a place'));
        $form->keepOpen();

        $form->setHtmlTemplate(self::getDefaultTemplateDir() . 'form/form.template.place.html');


        /** @var \core\html\form\elements\SelectElement $field */
        $field = $form->add(\core\html\form\elements\SelectElement::class);
        $field->setName('idcontext')
            ->setLabel(_('Context'))
            ->btn('context');

        /** @var \core\html\form\FormFactory $formFactory */
        $formFactory = $this->getDi()->singleton(\core\html\form\FormFactory::class);
        $subForm     = $formFactory->createFromWidget('address', ['add']);

        /** @var \core\html\form\elements\SelectElement $field */
        $field = $form->addFormAsField($subForm, 'address', null, 'OneToOne');

        $field->setName('idaddress')
            ->setLabel(_('Address'))
            ->btn('address');

        /** @var \core\html\form\elements\SelectElement $field */
        $field = $form->add(\core\html\form\elements\SelectElement::class);
        $field->setName('idplace_type')
            ->setLabel(_('Place type'))
            ->btn('place_type');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idroom')
            ->setLabel(_('Room'))
            ->btn('room');

        /** @var \core\html\form\elements\TextElement $field */
        $field = $form->add(\core\html\form\elements\TextElement::class);
        $field->setName('name')
            ->setLabel(_('Name'));

        /** @var \core\html\form\elements\FileinputElement $field */
        $field = $form->add(\core\html\form\elements\FileinputElement::class);
        $field->setName('visual')
            ->setLabel(_('Visual'));

        /** @var \core\html\form\elements\RichtextElement $field */
        $field = $form->add(\core\html\form\elements\RichtextElement::class);
        $field->setName('description')
            ->setLabel(_('Description'));

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idplace_opening_time')
            ->setLabel(_('Place opening time'))
            ->btn('place_opening_time');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idplace_escape')
            ->setLabel(_('Place escape'))
            ->btn('place_escape');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idplace_contact')
            ->setLabel(_('Place contact'))
            ->btn('place_contact');

        /** @var \core\html\form\elements\TagElement $field */
        $field = $form->add(\core\html\form\elements\TagElement::class);
        $field->setRessource('place_tags_list')
            ->setName('tags')
            ->setLabel(_('Tags'));

        return $form;
    }

    /**
     * Return Form Edit Place
     *
     * @PhpUnitGen\assertInstanceOf("Form::class")
     */
    public function edit(\core\html\CommandFactory $commandFactory): \core\html\form\Form
    {
        $ressource = $this->getRessource();

        $form = clone $this->add($commandFactory);
        $form->setValue(\_('Edit'));
        $form->setTitle(\sprintf(\_('Edit %s'), $ressource));
        $form->setName("update_$ressource");

        /** @var \core\html\form\elements\HiddenElement $field */
        $field = $form->add(\core\html\form\elements\HiddenElement::class);
        $field->setName("id$ressource")
            ->setValue(0);

        $form->keepOpen(\false);

        $form->on('submit')
            ->command($commandFactory->create(
                \core\html\form\command\SubmitCommand::class,
                [
                    'target'   => $form,
                    'method'   => 'PUT',
                    'endpoint' => '/api/' . $ressource . '/update'
                ]
            ));

        return $form;
    }
}
