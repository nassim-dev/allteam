<?php namespace app\widgets\form;

/**
 * Class ContextFormWidget
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class ContextFormWidget extends \core\html\form\AbstractFormTemplate implements \core\entities\WidgetInterface
{
    /**
     * Return Form Add Context
     *
     * @PhpUnitGen\assertInstanceOf("Form::class")
     */
    public function add(\core\html\CommandFactory $commandFactory): \core\html\form\Form
    {
        $form = $this->getDi()->make(\core\html\form\Form::class, [
            'name'   => 'create_context',
            'method' => 'POST',
            'action' => '#'
        ]);

        $form->setValue(\_('Save'));

        $form->on('submit')
            ->command($commandFactory->create(
                \core\html\form\command\SubmitCommand::class,
                [
                    'target'   => $form,
                    'method'   => 'POST',
                    'endpoint' => '/api/' . $this->getRessource() . '/create'
                ]
            ));

        $form->setTitle(\_('Add a context'));
        $form->keepOpen();

        $form->setHtmlTemplate(self::getDefaultTemplateDir() . 'form/form.template.context.html');


        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idgdpr')
            ->setLabel(_('Gdpr'))
            ->btn('gdpr');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idcontact')
            ->setLabel(_('Contact'))
            ->btn('contact');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idsocial')
            ->setLabel(_('Social'))
            ->btn('social');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idhiring')
            ->setLabel(_('Hiring'))
            ->btn('hiring');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idaddress')
            ->setLabel(_('Address'))
            ->btn('address');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idplace')
            ->setLabel(_('Place'))
            ->btn('place');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idplace_contact')
            ->setLabel(_('Place contact'))
            ->btn('place_contact');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idplace_type')
            ->setLabel(_('Place type'))
            ->btn('place_type');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idplace_opening_time')
            ->setLabel(_('Place opening time'))
            ->btn('place_opening_time');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idplace_escape')
            ->setLabel(_('Place escape'))
            ->btn('place_escape');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idroom')
            ->setLabel(_('Room'))
            ->btn('room');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idroom_type')
            ->setLabel(_('Room type'))
            ->btn('room_type');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idroom_escape')
            ->setLabel(_('Room escape'))
            ->btn('room_escape');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idvat')
            ->setLabel(_('Vat'))
            ->btn('vat');

        /** @var \core\html\form\elements\SelectElement $field */
        $field = $form->add(\core\html\form\elements\SelectElement::class);
        $field->setName('iduser')
            ->setLabel(_('User'))
            ->btn('user');

        /** @var \core\html\form\elements\TextElement $field */
        $field = $form->add(\core\html\form\elements\TextElement::class);
        $field->setName('name')
            ->setLabel(_('Name'));

        /** @var \core\html\form\elements\SelectElement $field */
        $field = $form->add(\core\html\form\elements\SelectElement::class);
        $field->setRessource('context_status_list')
            ->setName('status')
            ->setLabel(_('Status'))
            ->btn('context_status_list');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idconnector')
            ->setLabel(_('Connector'))
            ->btn('connector');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idconnector_type')
            ->setLabel(_('Connector type'))
            ->btn('connector_type');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idrole')
            ->setLabel(_('Role'))
            ->btn('role');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idpermission')
            ->setLabel(_('Permission'))
            ->btn('permission');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idcompagny')
            ->setLabel(_('Compagny'))
            ->btn('compagny');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idcompagny_opening_time')
            ->setLabel(_('Compagny opening time'))
            ->btn('compagny_opening_time');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idcompagny_contact')
            ->setLabel(_('Compagny contact'))
            ->btn('compagny_contact');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idcompagny_type')
            ->setLabel(_('Compagny type'))
            ->btn('compagny_type');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idactivitie')
            ->setLabel(_('Activitie'))
            ->btn('activitie');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idactivitie_type')
            ->setLabel(_('Activitie type'))
            ->btn('activitie_type');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idoperation')
            ->setLabel(_('Operation'))
            ->btn('operation');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idknowledge')
            ->setLabel(_('Knowledge'))
            ->btn('knowledge');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idknowledge_type')
            ->setLabel(_('Knowledge type'))
            ->btn('knowledge_type');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idneed')
            ->setLabel(_('Need'))
            ->btn('need');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idneed_type')
            ->setLabel(_('Need type'))
            ->btn('need_type');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idevent')
            ->setLabel(_('Event'))
            ->btn('event');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idtask')
            ->setLabel(_('Task'))
            ->btn('task');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idcategorie')
            ->setLabel(_('Categorie'))
            ->btn('categorie');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idhistorical')
            ->setLabel(_('Historical'))
            ->btn('historical');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idlog')
            ->setLabel(_('Log'))
            ->btn('log');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idconfiguration')
            ->setLabel(_('Configuration'))
            ->btn('configuration');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idconnector_type_smtp')
            ->setLabel(_('Connector type smtp'))
            ->btn('connector_type_smtp');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idconnector_type_imap')
            ->setLabel(_('Connector type imap'))
            ->btn('connector_type_imap');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idcompagny_type_band')
            ->setLabel(_('Compagny type band'))
            ->btn('compagny_type_band');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idshow')
            ->setLabel(_('Show'))
            ->btn('show');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idshow_contact')
            ->setLabel(_('Show contact'))
            ->btn('show_contact');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idactivitie_type_show')
            ->setLabel(_('Activitie type show'))
            ->btn('activitie_type_show');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idactivitie_type_mounting')
            ->setLabel(_('Activitie type mounting'))
            ->btn('activitie_type_mounting');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idactivitie_type_unmounting')
            ->setLabel(_('Activitie type unmounting'))
            ->btn('activitie_type_unmounting');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idreminder')
            ->setLabel(_('Reminder'))
            ->btn('reminder');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idmeeting')
            ->setLabel(_('Meeting'))
            ->btn('meeting');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idskill')
            ->setLabel(_('Skill'))
            ->btn('skill');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idcertificate')
            ->setLabel(_('Certificate'))
            ->btn('certificate');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idhire')
            ->setLabel(_('Hire'))
            ->btn('hire');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idhire_period')
            ->setLabel(_('Hire period'))
            ->btn('hire_period');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idneed_type_skill')
            ->setLabel(_('Need type skill'))
            ->btn('need_type_skill');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idneed_type_certificate')
            ->setLabel(_('Need type certificate'))
            ->btn('need_type_certificate');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idknowledge_type_skill')
            ->setLabel(_('Knowledge type skill'))
            ->btn('knowledge_type_skill');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idknowledge_type_certificate')
            ->setLabel(_('Knowledge type certificate'))
            ->btn('knowledge_type_certificate');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idmaterial')
            ->setLabel(_('Material'))
            ->btn('material');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idmaterial_type')
            ->setLabel(_('Material type'))
            ->btn('material_type');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idmaterial_type_nonconsumable')
            ->setLabel(_('Material type nonconsumable'))
            ->btn('material_type_nonconsumable');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idmaterial_type_consumable')
            ->setLabel(_('Material type consumable'))
            ->btn('material_type_consumable');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idpackage')
            ->setLabel(_('Package'))
            ->btn('package');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idpackage_item')
            ->setLabel(_('Package item'))
            ->btn('package_item');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idpackage_affectation')
            ->setLabel(_('Package affectation'))
            ->btn('package_affectation');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idneed_type_package')
            ->setLabel(_('Need type package'))
            ->btn('need_type_package');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idquotation_request')
            ->setLabel(_('Quotation request'))
            ->btn('quotation_request');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idquotation_request_item')
            ->setLabel(_('Quotation request item'))
            ->btn('quotation_request_item');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idquotation_response')
            ->setLabel(_('Quotation response'))
            ->btn('quotation_response');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idquotation_response_item')
            ->setLabel(_('Quotation response item'))
            ->btn('quotation_response_item');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idprovider_purchase')
            ->setLabel(_('Provider purchase'))
            ->btn('provider_purchase');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idquotation')
            ->setLabel(_('Quotation'))
            ->btn('quotation');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idquotation_item')
            ->setLabel(_('Quotation item'))
            ->btn('quotation_item');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idorder')
            ->setLabel(_('Order'))
            ->btn('order');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idorder_item')
            ->setLabel(_('Order item'))
            ->btn('order_item');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idinvoice')
            ->setLabel(_('Invoice'))
            ->btn('invoice');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idinvoice_item')
            ->setLabel(_('Invoice item'))
            ->btn('invoice_item');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idrefund')
            ->setLabel(_('Refund'))
            ->btn('refund');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idrefund_item')
            ->setLabel(_('Refund item'))
            ->btn('refund_item');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idactivitie_type_transport')
            ->setLabel(_('Activitie type transport'))
            ->btn('activitie_type_transport');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idactivitie_type_travel')
            ->setLabel(_('Activitie type travel'))
            ->btn('activitie_type_travel');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idplace_type_hostel')
            ->setLabel(_('Place type hostel'))
            ->btn('place_type_hostel');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idroom_type_bedroom')
            ->setLabel(_('Room type bedroom'))
            ->btn('room_type_bedroom');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idnuity')
            ->setLabel(_('Nuity'))
            ->btn('nuity');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idreservation_request')
            ->setLabel(_('Reservation request'))
            ->btn('reservation_request');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idreservation_response')
            ->setLabel(_('Reservation response'))
            ->btn('reservation_response');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idroom_type_canteen')
            ->setLabel(_('Room type canteen'))
            ->btn('room_type_canteen');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idmeal')
            ->setLabel(_('Meal'))
            ->btn('meal');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idneed_type_menu')
            ->setLabel(_('Need type menu'))
            ->btn('need_type_menu');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idmenu_affectation')
            ->setLabel(_('Menu affectation'))
            ->btn('menu_affectation');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idmenu')
            ->setLabel(_('Menu'))
            ->btn('menu');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idmenu_item')
            ->setLabel(_('Menu item'))
            ->btn('menu_item');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idrecipe')
            ->setLabel(_('Recipe'))
            ->btn('recipe');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idrecipe_item')
            ->setLabel(_('Recipe item'))
            ->btn('recipe_item');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idmaterial_type_food')
            ->setLabel(_('Material type food'))
            ->btn('material_type_food');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idaccount')
            ->setLabel(_('Account'))
            ->btn('account');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idaccounting_category')
            ->setLabel(_('Accounting category'))
            ->btn('accounting_category');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idanalytical_item')
            ->setLabel(_('Analytical item'))
            ->btn('analytical_item');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idexpense')
            ->setLabel(_('Expense'))
            ->btn('expense');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idrevenue')
            ->setLabel(_('Revenue'))
            ->btn('revenue');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idsubsidie_request')
            ->setLabel(_('Subsidie request'))
            ->btn('subsidie_request');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idsubsidie_response')
            ->setLabel(_('Subsidie response'))
            ->btn('subsidie_response');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idbudget')
            ->setLabel(_('Budget'))
            ->btn('budget');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idbudget_view')
            ->setLabel(_('Budget view'))
            ->btn('budget_view');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idbudget_view_expense_item')
            ->setLabel(_('Budget view expense item'))
            ->btn('budget_view_expense_item');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idbudget_view_revenue_item')
            ->setLabel(_('Budget view revenue item'))
            ->btn('budget_view_revenue_item');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idbudget_expense_item')
            ->setLabel(_('Budget expense item'))
            ->btn('budget_expense_item');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idbudget_revenue_item')
            ->setLabel(_('Budget revenue item'))
            ->btn('budget_revenue_item');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idbudget_dynamic_expense_item')
            ->setLabel(_('Budget dynamic expense item'))
            ->btn('budget_dynamic_expense_item');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idbudget_dynamic_revenue_item')
            ->setLabel(_('Budget dynamic revenue item'))
            ->btn('budget_dynamic_revenue_item');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idpersonnal_expense')
            ->setLabel(_('Personnal expense'))
            ->btn('personnal_expense');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idroom_type_stock')
            ->setLabel(_('Room type stock'))
            ->btn('room_type_stock');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idstock_entrie')
            ->setLabel(_('Stock entrie'))
            ->btn('stock_entrie');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('iddocumentation')
            ->setLabel(_('Documentation'))
            ->btn('documentation');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('iddocumentation_item')
            ->setLabel(_('Documentation item'))
            ->btn('documentation_item');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idsurvey')
            ->setLabel(_('Survey'))
            ->btn('survey');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idsurvey_item')
            ->setLabel(_('Survey item'))
            ->btn('survey_item');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idsurvey_response')
            ->setLabel(_('Survey response'))
            ->btn('survey_response');

        return $form;
    }

    /**
     * Return Form Edit Context
     *
     * @PhpUnitGen\assertInstanceOf("Form::class")
     */
    public function edit(\core\html\CommandFactory $commandFactory): \core\html\form\Form
    {
        $ressource = $this->getRessource();

        $form = clone $this->add($commandFactory);
        $form->setValue(\_('Edit'));
        $form->setTitle(\sprintf(\_('Edit %s'), $ressource));
        $form->setName("update_$ressource");

        /** @var \core\html\form\elements\HiddenElement $field */
        $field = $form->add(\core\html\form\elements\HiddenElement::class);
        $field->setName("id$ressource")
            ->setValue(0);

        $form->keepOpen(\false);

        $form->on('submit')
            ->command($commandFactory->create(
                \core\html\form\command\SubmitCommand::class,
                [
                    'target'   => $form,
                    'method'   => 'PUT',
                    'endpoint' => '/api/' . $ressource . '/update'
                ]
            ));

        return $form;
    }
}
