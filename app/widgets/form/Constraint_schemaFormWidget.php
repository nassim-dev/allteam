<?php namespace app\widgets\form;

/**
 * Class Constraint_schemaFormWidget
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class Constraint_schemaFormWidget extends \core\html\form\AbstractFormTemplate implements \core\entities\WidgetInterface
{
    /**
     * Return Form Add Constraint_schema
     *
     * @PhpUnitGen\assertInstanceOf("Form::class")
     */
    public function add(\core\html\CommandFactory $commandFactory): \core\html\form\Form
    {
        $form = $this->getDi()->make(\core\html\form\Form::class, [
            'name'   => 'create_constraint_schema',
            'method' => 'POST',
            'action' => '#'
        ]);

        $form->setValue(\_('Save'));

        $form->on('submit')
            ->command($commandFactory->create(
                \core\html\form\command\SubmitCommand::class,
                [
                    'target'   => $form,
                    'method'   => 'POST',
                    'endpoint' => '/api/' . $this->getRessource() . '/create'
                ]
            ));

        $form->setTitle(\_('Add a constraint schema'));
        $form->keepOpen();

        $form->setHtmlTemplate(self::getDefaultTemplateDir() . 'form/form.template.constraint_schema.html');


        /** @var \core\html\form\elements\SelectElement $field */
        $field = $form->add(\core\html\form\elements\SelectElement::class);
        $field->setName('idcontext')
            ->setLabel(_('Context'))
            ->btn('context');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idconstraint')
            ->setLabel(_('Constraint'))
            ->btn('constraint');

        /** @var \core\html\form\elements\TextElement $field */
        $field = $form->add(\core\html\form\elements\TextElement::class);
        $field->setName('name')
            ->setLabel(_('Name'));

        /** @var \core\html\form\elements\TagElement $field */
        $field = $form->add(\core\html\form\elements\TagElement::class);
        $field->setRessource('constraint_schema_tags_list')
            ->setName('tags')
            ->setLabel(_('Tags'));

        return $form;
    }

    /**
     * Return Form Edit Constraint_schema
     *
     * @PhpUnitGen\assertInstanceOf("Form::class")
     */
    public function edit(\core\html\CommandFactory $commandFactory): \core\html\form\Form
    {
        $ressource = $this->getRessource();

        $form = clone $this->add($commandFactory);
        $form->setValue(\_('Edit'));
        $form->setTitle(\sprintf(\_('Edit %s'), $ressource));
        $form->setName("update_$ressource");

        /** @var \core\html\form\elements\HiddenElement $field */
        $field = $form->add(\core\html\form\elements\HiddenElement::class);
        $field->setName("id$ressource")
            ->setValue(0);

        $form->keepOpen(\false);

        $form->on('submit')
            ->command($commandFactory->create(
                \core\html\form\command\SubmitCommand::class,
                [
                    'target'   => $form,
                    'method'   => 'PUT',
                    'endpoint' => '/api/' . $ressource . '/update'
                ]
            ));

        return $form;
    }
}
