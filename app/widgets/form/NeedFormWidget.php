<?php namespace app\widgets\form;

/**
 * Class NeedFormWidget
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class NeedFormWidget extends \core\html\form\AbstractFormTemplate implements \core\entities\WidgetInterface
{
    /**
     * Return Form Add Need
     *
     * @PhpUnitGen\assertInstanceOf("Form::class")
     */
    public function add(\core\html\CommandFactory $commandFactory): \core\html\form\Form
    {
        $form = $this->getDi()->make(\core\html\form\Form::class, [
            'name'   => 'create_need',
            'method' => 'POST',
            'action' => '#'
        ]);

        $form->setValue(\_('Save'));

        $form->on('submit')
            ->command($commandFactory->create(
                \core\html\form\command\SubmitCommand::class,
                [
                    'target'   => $form,
                    'method'   => 'POST',
                    'endpoint' => '/api/' . $this->getRessource() . '/create'
                ]
            ));

        $form->setTitle(\_('Add a need'));
        $form->keepOpen();

        $form->setHtmlTemplate(self::getDefaultTemplateDir() . 'form/form.template.need.html');


        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idcontext')
            ->setLabel(_('Context'))
            ->btn('context');

        /** @var \core\html\form\elements\TextElement $field */
        $field = $form->add(\core\html\form\elements\TextElement::class);
        $field->setName('name')
            ->setLabel(_('Name'));

        /** @var \core\html\form\elements\SelectElement $field */
        $field = $form->add(\core\html\form\elements\SelectElement::class);
        $field->setName('idneed_type')
            ->setLabel(_('Need type'))
            ->btn('need_type');

        /** @var \core\html\form\elements\FileinputElement $field */
        $field = $form->add(\core\html\form\elements\FileinputElement::class);
        $field->setName('visual')
            ->setLabel(_('Visual'));

        /** @var \core\html\form\elements\RichtextElement $field */
        $field = $form->add(\core\html\form\elements\RichtextElement::class);
        $field->setName('description')
            ->setLabel(_('Description'));

        /** @var \core\html\form\elements\TagElement $field */
        $field = $form->add(\core\html\form\elements\TagElement::class);
        $field->setRessource('need_tags_list')
            ->setName('tags')
            ->setLabel(_('Tags'));

        return $form;
    }

    /**
     * Return Form Edit Need
     *
     * @PhpUnitGen\assertInstanceOf("Form::class")
     */
    public function edit(\core\html\CommandFactory $commandFactory): \core\html\form\Form
    {
        $ressource = $this->getRessource();

        $form = clone $this->add($commandFactory);
        $form->setValue(\_('Edit'));
        $form->setTitle(\sprintf(\_('Edit %s'), $ressource));
        $form->setName("update_$ressource");

        /** @var \core\html\form\elements\HiddenElement $field */
        $field = $form->add(\core\html\form\elements\HiddenElement::class);
        $field->setName("id$ressource")
            ->setValue(0);

        $form->keepOpen(\false);

        $form->on('submit')
            ->command($commandFactory->create(
                \core\html\form\command\SubmitCommand::class,
                [
                    'target'   => $form,
                    'method'   => 'PUT',
                    'endpoint' => '/api/' . $ressource . '/update'
                ]
            ));

        return $form;
    }
}
