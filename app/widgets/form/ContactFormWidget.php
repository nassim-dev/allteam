<?php namespace app\widgets\form;

/**
 * Class ContactFormWidget
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class ContactFormWidget extends \core\html\form\AbstractFormTemplate implements \core\entities\WidgetInterface
{
    /**
     * Return Form Add Contact
     *
     * @PhpUnitGen\assertInstanceOf("Form::class")
     */
    public function add(\core\html\CommandFactory $commandFactory): \core\html\form\Form
    {
        $form = $this->getDi()->make(\core\html\form\Form::class, [
            'name'   => 'create_contact',
            'method' => 'POST',
            'action' => '#'
        ]);

        $form->setValue(\_('Save'));

        $form->on('submit')
            ->command($commandFactory->create(
                \core\html\form\command\SubmitCommand::class,
                [
                    'target'   => $form,
                    'method'   => 'POST',
                    'endpoint' => '/api/' . $this->getRessource() . '/create'
                ]
            ));

        $form->setTitle(\_('Add a contact'));
        $form->keepOpen();

        $form->setHtmlTemplate(self::getDefaultTemplateDir() . 'form/form.template.contact.html');


        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idcontext')
            ->setLabel(_('Context'))
            ->btn('context');

        /** @var \core\html\form\FormFactory $formFactory */
        $formFactory = $this->getDi()->singleton(\core\html\form\FormFactory::class);
        $subForm     = $formFactory->createFromWidget('address', ['add']);

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->addFormAsField($subForm, 'address', null, 'OneToMany');

        $field->setName('idaddress')
            ->setLabel(_('Address'))
            ->btn('address');

        /** @var \core\html\form\FormFactory $formFactory */
        $formFactory = $this->getDi()->singleton(\core\html\form\FormFactory::class);
        $subForm     = $formFactory->createFromWidget('user', ['add']);

        /** @var \core\html\form\elements\SelectElement $field */
        $field = $form->addFormAsField($subForm, 'user', null, 'OneToOne');

        $field->setName('iduser')
            ->setLabel(_('User'))
            ->btn('user');

        /** @var \core\html\form\FormFactory $formFactory */
        $formFactory = $this->getDi()->singleton(\core\html\form\FormFactory::class);
        $subForm     = $formFactory->createFromWidget('social', ['add']);

        /** @var \core\html\form\elements\SelectElement $field */
        $field = $form->addFormAsField($subForm, 'social', null, 'OneToOne');

        $field->setName('idsocial')
            ->setLabel(_('Social'))
            ->btn('social');

        /** @var \core\html\form\FormFactory $formFactory */
        $formFactory = $this->getDi()->singleton(\core\html\form\FormFactory::class);
        $subForm     = $formFactory->createFromWidget('hiring', ['add']);

        /** @var \core\html\form\elements\SelectElement $field */
        $field = $form->addFormAsField($subForm, 'hiring', null, 'OneToOne');

        $field->setName('idhiring')
            ->setLabel(_('Hiring'))
            ->btn('hiring');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idknowledge')
            ->setLabel(_('Knowledge'))
            ->btn('knowledge');

        /** @var \core\html\form\elements\FileinputElement $field */
        $field = $form->add(\core\html\form\elements\FileinputElement::class);
        $field->setName('photo')
            ->setLabel(_('Photo'));

        /** @var \core\html\form\elements\TextElement $field */
        $field = $form->add(\core\html\form\elements\TextElement::class);
        $field->setName('civility')
            ->setLabel(_('Civility'));

        /** @var \core\html\form\elements\TextElement $field */
        $field = $form->add(\core\html\form\elements\TextElement::class);
        $field->setName('pseudo')
            ->setLabel(_('Pseudo'));

        /** @var \core\html\form\elements\TextElement $field */
        $field = $form->add(\core\html\form\elements\TextElement::class);
        $field->setName('name')
            ->setLabel(_('Name'));

        /** @var \core\html\form\elements\TextElement $field */
        $field = $form->add(\core\html\form\elements\TextElement::class);
        $field->setName('first_name')
            ->setLabel(_('First name'));

        /** @var \core\html\form\elements\TextElement $field */
        $field = \core\html\form\Helper::phone($form);

        $field->setName('phone')
            ->setLabel(_('Phone'));

        /** @var \core\html\form\elements\EmailElement $field */
        $field = $form->add(\core\html\form\elements\EmailElement::class);
        $field->setName('email')
            ->setLabel(_('Email'));

        /** @var \core\html\form\elements\TagElement $field */
        $field = $form->add(\core\html\form\elements\TagElement::class);
        $field->setRessource('contact_tags_list')
            ->setName('tags')
            ->setLabel(_('Tags'));

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idplace_contact')
            ->setLabel(_('Place contact'))
            ->btn('place_contact');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idcompagny_contact')
            ->setLabel(_('Compagny contact'))
            ->btn('compagny_contact');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idshow')
            ->setLabel(_('Show'))
            ->btn('show');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idhire')
            ->setLabel(_('Hire'))
            ->btn('hire');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idmeal')
            ->setLabel(_('Meal'))
            ->btn('meal');

        return $form;
    }

    /**
     * Return Form Edit Contact
     *
     * @PhpUnitGen\assertInstanceOf("Form::class")
     */
    public function edit(\core\html\CommandFactory $commandFactory): \core\html\form\Form
    {
        $ressource = $this->getRessource();

        $form = clone $this->add($commandFactory);
        $form->setValue(\_('Edit'));
        $form->setTitle(\sprintf(\_('Edit %s'), $ressource));
        $form->setName("update_$ressource");

        /** @var \core\html\form\elements\HiddenElement $field */
        $field = $form->add(\core\html\form\elements\HiddenElement::class);
        $field->setName("id$ressource")
            ->setValue(0);

        $form->keepOpen(\false);

        $form->on('submit')
            ->command($commandFactory->create(
                \core\html\form\command\SubmitCommand::class,
                [
                    'target'   => $form,
                    'method'   => 'PUT',
                    'endpoint' => '/api/' . $ressource . '/update'
                ]
            ));

        return $form;
    }
}
