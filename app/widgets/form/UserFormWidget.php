<?php namespace app\widgets\form;

/**
 * Class UserFormWidget
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class UserFormWidget extends \core\html\form\AbstractFormTemplate implements \core\entities\WidgetInterface
{
    /**
     * Return Form Add User
     *
     * @PhpUnitGen\assertInstanceOf("Form::class")
     */
    public function add(\core\html\CommandFactory $commandFactory): \core\html\form\Form
    {
        $form = $this->getDi()->make(\core\html\form\Form::class, [
            'name'   => 'create_user',
            'method' => 'POST',
            'action' => '#'
        ]);

        $form->setValue(\_('Save'));
        $form->on('submit')
            ->command($commandFactory->create(\core\html\form\command\SubmitCommand::class, ['target' => $form, 'method' => 'POST', 'endpoint' => '/api/' . $this->getRessource() . '/create']));

        $form->setTitle(\_('Add a user'));
        $form->keepOpen();

        $form->setHtmlTemplate(self::getDefaultTemplateDir() . 'form/form.template.user.html');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idcontext')
            ->setLabel(_('Context'))
            ->btn('context');

        /** @var \core\html\form\elements\EmailElement $field */
        $field = $form->add(\core\html\form\elements\EmailElement::class);
        $field->setName('email')
            ->setLabel(_('Email'));

        /** @var \core\html\form\elements\PasswordElement $field */
        $field = $form->add(\core\html\form\elements\PasswordElement::class);
        $field->setName('password')
            ->setLabel(_('Password'));

        /** @var \core\html\form\elements\SelectElement $field */
        $field = $form->add(\core\html\form\elements\SelectElement::class);
        $field->setRessource('user_status_list')
            ->setName('status')
            ->setLabel(_('Status'))
            ->btn('user_status_list');

        /** @var \core\html\form\elements\SwitchElement $field */
        $field = $form->add(\core\html\form\elements\SwitchElement::class);
        $field->setName('enabled')
            ->setLabel(_('Enabled'));

        /** @var \core\html\form\elements\TextElement $field */
        $field = $form->add(\core\html\form\elements\TextElement::class);
        $field->setName('validation')
            ->setLabel(_('Validation'));

        /** @var \core\html\form\elements\TextElement $field */
        $field = $form->add(\core\html\form\elements\TextElement::class);
        $field->setName('hash_validation')
            ->setLabel(_('Hash validation'));

        /** @var \core\html\form\elements\SelectElement $field */
        $field = $form->add(\core\html\form\elements\SelectElement::class);
        $field->setRessource('user_language_list')
            ->setName('language')
            ->setLabel(_('Language'))
            ->btn('user_language_list');

        return $form;
    }

    /**
     * Return Form Edit User
     *
     * @PhpUnitGen\assertInstanceOf("Form::class")
     */
    public function edit(\core\html\CommandFactory $commandFactory): \core\html\form\Form
    {
        $ressource = $this->getRessource();

        $form = clone $this->add($commandFactory);
        $form->setValue(\_('Edit'));
        $form->setTitle(\sprintf(\_('Edit %s'), $ressource));
        $form->setName("update_$ressource");

        /** @var \core\html\form\elements\HiddenElement $field */
        $field = $form->add(\core\html\form\elements\HiddenElement::class);
        $field->setName("id$ressource")
            ->setValue(0);
        $form->keepOpen(\false);

        $form->on('submit')
            ->command($commandFactory->create(\core\html\form\command\SubmitCommand::class, ['target' => $form, 'method' => 'PUT', 'endpoint' => '/api/' . $ressource . '/update']));

        return $form;
    }

    /**
     * Return Form Login User
     *
     * @PhpUnitGen\assertInstanceOf("Form::class")
     */
    public function login(\core\html\CommandFactory $commandFactory): \core\html\form\Form
    {
        $form = $this->getDi()->make(\core\html\form\Form::class, [
            'name'   => 'login_user',
            'method' => 'POST',
            'action' => '#'
        ]);

        $form->setValue(\_('Save'));
        $form->on('submit')
            ->command($commandFactory->create(\core\html\form\command\SubmitCommand::class, ['target' => $form, 'method' => 'POST', 'endpoint' => '/api/' . $this->getRessource() . '/login']));

        $form->setTitle(\_('Login'));
        $form->keepOpen();

        //$form->setHtmlTemplate(self::getDefaultTemplateDir() . 'form/form.template.user.html');

        /** @var \core\html\form\elements\EmailElement $field */
        $field = $form->add(\core\html\form\elements\EmailElement::class);
        $field->setName('email')
            ->setLabel(_('Email'));

        /** @var \core\html\form\elements\PasswordElement $field */
        $field = $form->add(\core\html\form\elements\PasswordElement::class);
        $field->setName('password')
            ->setLabel(_('Password'));

        return $form;
    }

    /**
     * Return Form Password User
     *
     * @PhpUnitGen\assertInstanceOf("Form::class")
     */
    public function password(\core\html\CommandFactory $commandFactory): \core\html\form\Form
    {
        $form = $this->getDi()->make(\core\html\form\Form::class, [
            'name'   => 'password_user',
            'method' => 'POST',
            'action' => '#'
        ]);

        $form->setValue(\_('Save'));
        $form->on('submit')
            ->command($commandFactory->create(\core\html\form\command\SubmitCommand::class, ['target' => $form, 'method' => 'POST', 'endpoint' => '/api/' . $this->getRessource() . '/password']));

        $form->setTitle(\_('Password'));
        $form->keepOpen();

        //$form->setHtmlTemplate(self::getDefaultTemplateDir() . 'form/form.template.user.html');

        /** @var \core\html\form\elements\EmailElement $field */
        $field = $form->add(\core\html\form\elements\EmailElement::class);
        $field->setName('email')
            ->setLabel(_('Email'));

        return $form;
    }
}
