<?php namespace app\widgets\form;

/**
 * Class OperationFormWidget
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class OperationFormWidget extends \core\html\form\AbstractFormTemplate implements \core\entities\WidgetInterface
{
    /**
     * Return Form Add Operation
     *
     * @PhpUnitGen\assertInstanceOf("Form::class")
     */
    public function add(\core\html\CommandFactory $commandFactory): \core\html\form\Form
    {
        $form = $this->getDi()->make(\core\html\form\Form::class, [
            'name'   => 'create_operation',
            'method' => 'POST',
            'action' => '#'
        ]);

        $form->setValue(\_('Save'));

        $form->on('submit')
            ->command($commandFactory->create(
                \core\html\form\command\SubmitCommand::class,
                [
                    'target'   => $form,
                    'method'   => 'POST',
                    'endpoint' => '/api/' . $this->getRessource() . '/create'
                ]
            ));

        $form->setTitle(\_('Add a operation'));
        $form->keepOpen();

        $form->setHtmlTemplate(self::getDefaultTemplateDir() . 'form/form.template.operation.html');


        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idactivitie')
            ->setLabel(_('Activitie'))
            ->btn('activitie');

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idcontext')
            ->setLabel(_('Context'))
            ->btn('context');

        /** @var \core\html\form\elements\TextElement $field */
        $field = $form->add(\core\html\form\elements\TextElement::class);
        $field->setName('name')
            ->setLabel(_('Name'));

        /** @var \core\html\form\elements\MultiselectElement $field */
        $field = $form->add(\core\html\form\elements\MultiselectElement::class);
        $field->setName('idneed')
            ->setLabel(_('Need'))
            ->btn('need');

        /** @var \core\html\form\elements\TimeElement $field */
        $field = $form->add(\core\html\form\elements\TimeElement::class);
        $field->setName('duration')
            ->setLabel(_('Duration'));

        /** @var \core\html\form\elements\SwitchElement $field */
        $field = $form->add(\core\html\form\elements\SwitchElement::class);
        $field->setName('constraint_type')
            ->setLabel(_('Constraint type'));

        /** @var \core\html\form\elements\TimeElement $field */
        $field = $form->add(\core\html\form\elements\TimeElement::class);
        $field->setName('constraint_value')
            ->setLabel(_('Constraint value'));

        /** @var \core\html\form\elements\TextElement $field */
        $field = $form->add(\core\html\form\elements\TextElement::class);
        $field->setName('room_reference')
            ->setLabel(_('Room reference'));

        /** @var \core\html\form\elements\TextElement $field */
        $field = $form->add(\core\html\form\elements\TextElement::class);
        $field->setName('place_reference')
            ->setLabel(_('Place reference'));

        /** @var \core\html\form\elements\FileinputElement $field */
        $field = $form->add(\core\html\form\elements\FileinputElement::class);
        $field->setName('visual')
            ->setLabel(_('Visual'));

        /** @var \core\html\form\elements\RichtextElement $field */
        $field = $form->add(\core\html\form\elements\RichtextElement::class);
        $field->setName('description')
            ->setLabel(_('Description'));

        /** @var \core\html\form\elements\SelectElement $field */
        $field = $form->add(\core\html\form\elements\SelectElement::class);
        $field->setName('idoperation_parent')
            ->setLabel(_('Operation parent'))
            ->btn('operation_parent');

        return $form;
    }

    /**
     * Return Form Edit Operation
     *
     * @PhpUnitGen\assertInstanceOf("Form::class")
     */
    public function edit(\core\html\CommandFactory $commandFactory): \core\html\form\Form
    {
        $ressource = $this->getRessource();

        $form = clone $this->add($commandFactory);
        $form->setValue(\_('Edit'));
        $form->setTitle(\sprintf(\_('Edit %s'), $ressource));
        $form->setName("update_$ressource");

        /** @var \core\html\form\elements\HiddenElement $field */
        $field = $form->add(\core\html\form\elements\HiddenElement::class);
        $field->setName("id$ressource")
            ->setValue(0);

        $form->keepOpen(\false);

        $form->on('submit')
            ->command($commandFactory->create(
                \core\html\form\command\SubmitCommand::class,
                [
                    'target'   => $form,
                    'method'   => 'PUT',
                    'endpoint' => '/api/' . $ressource . '/update'
                ]
            ));

        return $form;
    }
}
