<?php

namespace app\widgets\mail;

use core\config\Conf;
use core\DI\DiProvider;
use core\mail\Mail;
use core\utils\Utils;

/**
 * Class MailTemplates
 *
 * Mail factory
 *
 * @todo create specific classes for each template
 *
 * @category Mail factory
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class MailTemplates
{
    use DiProvider;
    /**
     * Mail after record booking
     *
     * @param mixed $object
     */
    public function mailBooking($object, $idbooking, $mailTop = null): Mail
    {
        $link = [];
        $mailTop ??= _('Your registration has been registered. Please download your ticket by clicking on the link in this mail.<br><br>
        This ticket will be requested by the organizer to access the venues of the event.');
        $user_admin = $this->auth->getContext()->getUserAdmin();

        $mail = new Mail(); // ->Création mail
        $mail->addAddress($object->email); // Les destinataires.
        $mail->addReplyTo($user_admin->email, $user_admin->getFormattedName());
        $mail->setFrom($user_admin->email, $user_admin->getFormattedName());
        $mail->Subject = 'Allteam : Votre billet numérique';

        $link['href'] = Conf::find('PHP.HTTP_PROTOCOL') . '://' . Conf::find('PHP.SERVER_NAME') . '/api/booking/' . Utils::encrypt($idbooking) . '/pdf';
        $link['text'] = 'Téléchargez votre billet';
        $mailBottom   = '';

        /*if (Session::has('form_vars.idpricelist')) {

        $mailBottom .= "<br>Votre hébergement est suceptible d'être modifié en cas de non disponibilité de la chambre choisie. Dans ce cas vous recevrez un email avec la nouvelle réservation qui vous a été attribuée.";

        //name_idaccommodation_price_idpricelist
        $idpricelist = explode("_", Session::get("form_vars.idpricelist"));

        $sleeperlist = new Sleeperlist(Session::get("form_vars"));
        $sleeperlist->setIdbooking($object->idbooking);
        $sleeperlist->setIdcontext($object->idcontext);
        $sleeperlist->setIdperformance($object->performance->idperformance);
        $sleeperlist->setIdAccommodation($idpricelist[1]);
        $sleeperlist->setRoom_type($idpricelist[0] . ":" . $idpricelist[3]);
        $sleeperlist->setIdpricelist($idpricelist[3]);

        //Generate card
        $accommodation = $sleeperlist->getAccommodation();
        $mailBottom .= "<br>" . $accommodation->genFormatCard();

        //If Payment done
        if (-1 !== $idstripe) {
        $sleeperlist->setIdPaying("1");
        }
        //If sleeperlist already exist
        $array_params = array("idperformance" => $sleeperlist->idperformance,
        "idbooking"                           => $object->idbooking);
        $test_sleeperlist = $repositoryFactory->table('sleeperlist')->getByParams($array_params);

        if (false !== $test_sleeperlist) {
        $sleeperlist->setIdsleeperlist($test_sleeperlist->idsleeperlist);
        $idsleeperlist = $test_sleeperlist->idsleeperlist;
        $repositoryFactory->table('sleeperlist')->update($sleeperlist);
        } else {
        $idsleeperlist = $repositoryFactory->table('sleeperlist')->add($sleeperlist);
        }

        $object->setIdsleeperlist($idsleeperlist);

        $mailBottom .= '<br>';
        }

        //Affect Runnings
        if (!empty($object->getRunningsObj())) {
        $object->affectRunnings();
        $mailBottom .= "<br>";
        $mailBottom .= "Vos transports sont suceptible d'être modifié en cas de non modification par l'organisatateur. Dans ce cas vous recevrez un email avec les nouvelles horaires des transpors.";
        //print_r($object->getRunningsObj());
        foreach ($object->getRunningsObj() as $running) {
        if (false !== $running) {
        $mailBottom .= "<br>" . $running->genFormatCard();
        }
        }
        }
        $this->getDBM()->update($object); */

        $mailBottom .= _('The Team of') . ' Allteam';

        $mail->setMailTop($mailTop);
        $mail->setMailBottom($mailBottom);
        $mail->setLink($link);

        $mail->getTpl($this->getDi()->make(ViewServiceInterface::class, [], 'SINGLET0N'), '../');

        return $mail;
    }

    /**
     * Send Mail after long time without connexion
     *
     * @param User $user
     */
    public function mailComeBack($user): Mail
    {
        $link = [];
        $mail = new Mail(); // ->Création mail
        $mail->addAddress($user->getEmail()); // Les destinataires.
        $mail->addReplyTo('admin@mailo.com', 'Administrateur');
        $mail->setFrom('admin@mailo.com', 'Administrateur');

        $mail->Subject = 'Allteam  : Retrouvez nous !';

        $link['href'] = Conf::find('PHP.HTTP_PROTOCOL') . '://' . Conf::find('PHP.SERVER_NAME') . '/validation/' . $user->getValidation();
        $link['text'] = _('Sign in');

        $mailTop = ''; //LAST_CONNECTION_OLD

        $mailBottom = 'Vos identifiants de connexion : <br><br>';
        $mailBottom .= 'Email : ' . $user->getEmail() . '<br>';
        $mailBottom .= 'L\'équipe de Allteam</HTML>';

        $mail->setMailTop($mailTop);
        $mail->setMailBottom($mailBottom);
        $mail->setLink($link);

        $mail->getTpl($this->getDi()->make(ViewServiceInterface::class, [], 'SINGLET0N'));

        return $mail;
    }

    /**
     * Mail for confirm subscription
     *
     * @param User $user
     */
    public function mailConfirmSubscribe($user): Mail
    {
        $link = [];
        $mail = new Mail();
        $mail->addAddress($user->email); // Les destinataires
        $mail->addReplyTo('admin@mailo.com', 'Administrateur');
        $mail->setFrom('admin@mailo.com', 'Administrateur');
        $mail->Subject = 'Allteam : Inscription';

        $link['href'] = Conf::find('PHP.HTTP_PROTOCOL') . '://' . Conf::find('PHP.SERVER_NAME') . '/validation/' . $user->getValidation();
        $link['text'] = 'Validez votre inscription';

        $mailTop = _('Your account will be reviewed by an administrator to validate the registration process. You will be contacted within 24 hours by our team to present you the software.<br> <br>
Please click on the link below to validate your email address.');

        $mailBottom = 'Vos identifiants de connexion : <br><br>';
        $mailBottom .= 'Email : ' . $user->email . '<br><br>';
        $mailBottom .= _('The Team of') . ' Allteam';

        $mail->setMailTop($mailTop);
        $mail->setMailBottom($mailBottom);
        $mail->setLink($link);
        $mail->setUserName($user->getFormattedName());

        $mail->getTpl($this->getDi()->make(ViewServiceInterface::class, [], 'SINGLET0N'), '../');

        return $mail;
    }

    /**
     * Send mail to recorded booking
     *
     * @param Booking $booking
     */
    public function mailContactBookingSubscriber($booking, array $formVars): Mail
    {
        $link       = [];
        $user_admin = $booking->bookinglist->context->getUserAdmin();

        $mail = new Mail(); // ->Création mail
        $mail->addAddress($booking->email); // Les destinataires.
        $mail->addReplyTo($user_admin->email, $user_admin->getFormattedName());
        $mail->setFrom($user_admin->email, $user_admin->getFormattedName());
        $mail->Subject = 'Allteam : ' . $formVars['subject'];

        $link['href'] = Conf::find('PHP.HTTP_PROTOCOL') . '://' . Conf::find('PHP.SERVER_NAME');
        $link['text'] = 'Se rendre sur Allteam';
        $mailTop      = $formVars['message'];
        $mailBottom   = _('The Team of') . ' Allteam';

        $mail->setMailTop($mailTop);
        $mail->setMailBottom($mailBottom);
        $mail->setUserName($booking->first_name);
        $mail->setLink($link);

        $mail->getTpl($this->getDi()->make(ViewServiceInterface::class, [], 'SINGLET0N'), '../');

        return $mail;
    }

    /**
     * Invitation Mail
     *
     * @param User $user_admin
     * @param User $user
     */
    public function mailInviteNewUser($user_admin, $user): Mail
    {
        $link = [];
        $mail = new Mail(); // ->Création mail
        $mail->addAddress($user->email); // Les destinataires.
        $mail->addReplyTo($user_admin->email, $user_admin->getFormattedName());
        $mail->setFrom($user_admin->email, $user_admin->getFormattedName());
        $mail->Subject = 'Allteam : Invitation';

        $link['href'] = Conf::find('PHP.HTTP_PROTOCOL') . '://' . Conf::find('PHP.SERVER_NAME') . '/validation/' . $user->getValidation();
        $link['text'] = 'Validez votre inscription';

        $mailTop = _('You have received an invitation to use the Allteam software. <br> Please click on the link below to access the registration form.');

        if (!empty($this->request->getContents('message'))) {
            $message = 'Message de ' . ucfirst($user_admin->first_name);
            $message .= ' ' . ucfirst($user_admin->name) . ' : <br><br>' . htmlspecialchars_decode($this->request->getContents('message')) . '<br><br>';
        } else {
            $message = '';
        }

        $mailBottom = $message;
        $mailBottom .= _('The Team of') . ' Allteam';

        $mail->setMailTop($mailTop);
        $mail->setMailBottom($mailBottom);
        $mail->setLink($link);

        $mail->getTpl($this->getDi()->make(ViewServiceInterface::class, [], 'SINGLET0N'), '../');

        return $mail;
    }

    /**
     * Create Gitlab Issue
     */
    public function mailNewIssueDetected(array $formVars): Mail
    {
        $link = [];
        $mail = new Mail();
        $mail->addAddress('incoming+Tricksoft/allteam@incoming.gitlab.com'); // Les destinataires
        $mail->addReplyTo($formVars['email']);
        $mail->setFrom($formVars['email']);
        $mail->Subject = $formVars['type'] . ' : ' . $formVars['subject'];

        $link['href'] = Conf::find('PHP.HTTP_PROTOCOL') . '://' . Conf::find('PHP.SERVER_NAME');
        $link['text'] = 'Aller sur le site';

        if (!empty($formVars['attachment'])) {
            $attachments = explode(';', $formVars['attachment']);
            foreach ($attachments as $attachment) {
                if (file_exists(BASE_DIR . Conf::find('PHP.ROOT_PUBLIC_FILES') . 'context_' . $this->auth->getContext()?->getIdcontext() . '/' . $attachment)) {
                    $mail->addAttachment(BASE_DIR . Conf::find('PHP.ROOT_PUBLIC_FILES') . 'context_' . $this->auth->getContext()?->getIdcontext() . '/' . $attachment, explode('/', $attachment)[1]);
                }
            }
        }

        $mailTop = $formVars['text'];

        $mailBottom = '<pre>Contexte : ' . print_r(Utils::encrypt(json_decode($formVars['context'], null, 512, JSON_THROW_ON_ERROR)), true) . '<br><br>';
        $mailBottom .= 'HttpRequest : ' . print_r(Utils::encrypt(json_decode($formVars['httprequest'], null, 512, JSON_THROW_ON_ERROR)), true) . '</pre><br><br>';
        $mailBottom .= _('The Team of') . ' Allteam';

        $mail->setMailTop($mailTop);
        $mail->setMailBottom($mailBottom);
        $mail->setLink($link);

        $mail->getTpl($this->getDi()->make(ViewServiceInterface::class, [], 'SINGLET0N'), '../');

        return $mail;
    }

    /**
     * Send mail when a message is recorded
     */
    public function mailNewMessage(array $args, array $ids): Mail
    {
        $repositoryFactory = null;
        $link              = [];
        $users             = $repositoryFactory->table('user')->getByParams(['iduser' => $ids]);
        $user_admin        = $repositoryFactory->table('user')->getById($args['object']->idfrom);

        $mail = new Mail(); // ->Création mail
        foreach ($users as $user) {
            if (!null === $user && !is_bool($user)) {
                $mail->addAddress($user->email, $user->getFormattedName()); // Les destinataires.
            }
        }

        $mail->addReplyTo($user_admin->email, $user_admin->getFormattedName());
        $mail->setFrom($user_admin->email, $user_admin->getFormattedName());
        $mail->Subject = 'Allteam : ' . $args['object']->object;

        $link['href'] = Conf::find('PHP.HTTP_PROTOCOL') . '://' . Conf::find('PHP.SERVER_NAME');
        $link['text'] = 'Se rendre sur Allteam';
        $mailTop      = $args['object']->text;
        $mailBottom   = _('The Team of') . ' Allteam';

        $mail->setMailTop($mailTop);
        $mail->setMailBottom($mailBottom);
        $mail->setLink($link);

        if (!null === $args['object']->attachment) {
            $attachments = explode(';', $args['object']->attachment);
            foreach ($attachments as $attachment) {
                if (file_exists(BASE_DIR . Conf::find('PHP.ROOT_PUBLIC_FILES') . 'context_' . $this->auth->getContext()?->getIdcontext() . '/' . $attachment)) {
                    $mail->addAttachment(BASE_DIR . Conf::find('PHP.ROOT_PUBLIC_FILES') . 'context_' . $this->auth->getContext()?->getIdcontext() . '/' . $attachment, explode('/', $attachment)[1]);
                }
            }
        }

        $mail->getTpl($this->getDi()->make(ViewServiceInterface::class, [], 'SINGLET0N'), '../');

        return $mail;
    }

    /**
     * Send mail to admin after new subscription
     *
     * @param User $user
     */
    public function mailNewRegistredUser($user): Mail
    {
        $link = [];
        $mail = new Mail();
        $mail->addAddress('admin@mailo.com');
        $mail->addReplyTo('admin@mailo.com', 'Administrateur');
        $mail->setFrom('admin@mailo.com', 'Administrateur');
        $mail->Subject = 'Allteam : Nouvelle inscription';

        $link['href'] = Conf::find('PHP.HTTP_PROTOCOL') . '://' . Conf::find('PHP.SERVER_NAME');
        $link['text'] = "Validez l'inscription";

        $mailTop = 'Une nouvelle inscription a été enregistrée ';

        $mailBottom = 'Informations utilisateur : <br><br>';
        $mailBottom .= 'Nom : ' . $user->name . '<br>';
        $mailBottom .= 'Prénom : ' . $user->first_name . '<br>';
        $mailBottom .= 'Téléphone : ' . $user->phone . '<br>';
        $mailBottom .= 'Email : ' . $user->email . '<br><br>';
        $mailBottom .= _('The Team of') . ' Allteam';

        $mail->setMailTop($mailTop);
        $mail->setMailBottom($mailBottom);
        $mail->setLink($link);

        $mail->getTpl($this->getDi()->make(ViewServiceInterface::class, [], 'SINGLET0N'), '../');

        return $mail;
    }

    /**
     * Return mail for notification
     *
     * @param User $user
     */
    public function mailNotification($user, $content): Mail
    {
        $link       = [];
        $user_admin = $this->auth->getContext()->getUserAdmin();

        $link['href'] = Conf::find('PHP.HTTP_PROTOCOL') . '://' . Conf::find('PHP.SERVER_NAME') . '/login';
        $link['text'] = _('Sign in');

        $mailHeader  = 'Veuillez trouver ci-dessous votre récapitulatif quotidien.';
        $mailBottom  = _('The Team of') . ' Allteam';
        $mailContent = $content;

        $mail = new Mail();
        $mail->addAddress($user->email); // Les destinataires.
        $mail->addReplyTo($user_admin->email, $user_admin->getFormattedName());
        $mail->setFrom($user_admin->email, $user_admin->getFormattedName());
        $mail->Subject = 'Allteam : ' . _('Notification');

        $mail->setLink($link);
        $mail->setMailTop($mailHeader);
        $mail->setArrayContent($mailContent);
        $mail->setMailBottom($mailBottom);
        $mail->setUserName($user->first_name);

        if (isset($content['files'])) {
            foreach ($content['files'] as $file) {
                $mail->addAttachment($file);
            }
        }

        $mail->getTpl($this->getDi()->make(ViewServiceInterface::class, [], 'SINGLET0N'), '../');

        return $mail;
    }

    /**
     * Mail after password update
     *
     * @param User $user
     */
    public function mailUpdatePassword($user, $password): Mail
    {
        $link = [];
        $mail = new Mail(); // ->Création mail
        $mail->addAddress($user->email); // Les destinataires.
        $mail->addReplyTo('admin@mailo.com', 'Administrateur');
        $mail->setFrom('admin@mailo.com', 'Administrateur');
        $mail->Subject = 'Allteam  : Changement de mot de passe';

        $link['href'] = Conf::find('PHP.HTTP_PROTOCOL') . '://' . Conf::find('PHP.SERVER_NAME') . '/login';
        $link['text'] = _('Sign in');

        $mailTop = _('Your password was reset, check on your box e-mail.');

        $mailBottom = 'Vos identifiants de connexion : <br><br>';
        $mailBottom .= 'Mot de passe : ' . $password . '<br>';
        $mailBottom .= _('The Team of') . ' Allteam';

        $mail->setMailTop($mailTop);
        $mail->setMailBottom($mailBottom);
        $mail->setLink($link);

        $mail->getTpl($this->getDi()->make(ViewServiceInterface::class, [], 'SINGLET0N'), '../');

        return $mail;
    }

    /**
     * Validate Subscription
     * Possibly duplicate with self::MAIL_CONFIRM_SUBSCRIBE()
     *
     * @param User $user
     */
    public function mailValidateUserEmail($user): Mail
    {
        $link = [];
        $mail = new Mail(); // ->Création mail
        $mail->addAddress($user->email); // Les destinataires.
        $mail->addReplyTo('admin@mailo.com', 'Administrateur');
        $mail->setFrom('admin@mailo.com', 'Administrateur');
        $mail->Subject = 'Allteam : Validation inscription';

        $link['href'] = Conf::find('PHP.HTTP_PROTOCOL') . '://' . Conf::find('PHP.SERVER_NAME') . '/login';
        $link['text'] = _('Sign in');

        $mailTop = _('Your registration has been successfully registered, you will receive a confirmation email once an administrator has verified your account.');

        $mailBottom = 'Vos identifiants de connexion : <br><br>';
        $mailBottom .= 'Email : ' . $user->email . '<br><br>';
        $mailBottom .= _('The Team of') . ' Allteam';

        $mail->setMailTop($mailTop);
        $mail->setMailBottom($mailBottom);
        $mail->setLink($link);

        $mail->getTpl($this->getDi()->make(ViewServiceInterface::class, [], 'SINGLET0N'), '../');

        return $mail;
    }
}
