<?php namespace app\widgets\datatable;

/**
 * Class Place_opening_timeDatatableWidget
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class Place_opening_timeDatatableWidget extends \core\html\tables\datatable\AbstractTableTemplate implements \core\entities\WidgetInterface
{
    /**
     * Return Datatable template PLACE_OPENING_TIME_LIST
     *
     * @PhpUnitGen\assertInstanceOf("Datatable::class")
     */
    public function PLACE_OPENING_TIME_LIST(): \core\html\tables\datatable\Datatable
    {
        $table = new \core\html\tables\datatable\Datatable(
            \_('Place opening time'),
            'place_opening_time',
            [
                'idcontext' => [
                    'label' => _('Context'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('context', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'hours' => [
                    'label' => _('Hours'),
                    'type'  => 'datetimerange'
                ]
            ]
        );

        $link = new \core\routing\Link(\app\api\controller\Place_opening_timeController::class, 'datatableAction');
        $table->addEndpoint('readData', $link);

        $link = new \core\routing\Link(\app\api\controller\Place_opening_timeController::class, 'editorAction');
        $table->addEndpoint('modifyData', $link);

        $link = new \core\routing\Link(\app\api\controller\Place_opening_timeController::class, 'createAction');
        $table->addEndpoint('createData', $link);

        $link = new \core\routing\Link(\app\api\controller\Place_opening_timeController::class, 'updateAction');
        $table->addEndpoint('updateData', $link);

        return $table;
    }
}
