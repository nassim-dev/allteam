<?php namespace app\widgets\datatable;

/**
 * Class SocialDatatableWidget
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class SocialDatatableWidget extends \core\html\tables\datatable\AbstractTableTemplate implements \core\entities\WidgetInterface
{
    /**
     * Return Datatable template SOCIAL_LIST
     *
     * @PhpUnitGen\assertInstanceOf("Datatable::class")
     */
    public function SOCIAL_LIST(): \core\html\tables\datatable\Datatable
    {
        $table = new \core\html\tables\datatable\Datatable(
            \_('Social'),
            'social',
            [
                'idcontext' => [
                    'label' => _('Context'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('context', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'date_birth' => [
                    'label' => _('Date birth'),
                    'type'  => 'date'
                ],
                'place_birth' => [
                    'label' => _('Place birth'),
                    'type'  => 'text'
                ]
            ]
        );

        $link = new \core\routing\Link(\app\api\controller\SocialController::class, 'datatableAction');
        $table->addEndpoint('readData', $link);

        $link = new \core\routing\Link(\app\api\controller\SocialController::class, 'editorAction');
        $table->addEndpoint('modifyData', $link);

        $link = new \core\routing\Link(\app\api\controller\SocialController::class, 'createAction');
        $table->addEndpoint('createData', $link);

        $link = new \core\routing\Link(\app\api\controller\SocialController::class, 'updateAction');
        $table->addEndpoint('updateData', $link);

        return $table;
    }
}
