<?php namespace app\widgets\datatable;

/**
 * Class ConnectorDatatableWidget
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class ConnectorDatatableWidget extends \core\html\tables\datatable\AbstractTableTemplate implements \core\entities\WidgetInterface
{
    /**
     * Return Datatable template CONNECTOR_LIST
     *
     * @PhpUnitGen\assertInstanceOf("Datatable::class")
     */
    public function CONNECTOR_LIST(): \core\html\tables\datatable\Datatable
    {
        $table = new \core\html\tables\datatable\Datatable(
            \_('Connector'),
            'connector',
            [
                'idcontext' => [
                    'label' => _('Context'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('context', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'iduser' => [
                    'label' => _('User'),
                    'type'  => 'select',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('user', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idconnector_type' => [
                    'label' => _('Connector type'),
                    'type'  => 'select',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('connector_type', 'api'), 'htmlListAction'))->getEndpoint()
                ]
            ]
        );

        $link = new \core\routing\Link(\app\api\controller\ConnectorController::class, 'datatableAction');
        $table->addEndpoint('readData', $link);

        $link = new \core\routing\Link(\app\api\controller\ConnectorController::class, 'editorAction');
        $table->addEndpoint('modifyData', $link);

        $link = new \core\routing\Link(\app\api\controller\ConnectorController::class, 'createAction');
        $table->addEndpoint('createData', $link);

        $link = new \core\routing\Link(\app\api\controller\ConnectorController::class, 'updateAction');
        $table->addEndpoint('updateData', $link);

        return $table;
    }
}
