<?php namespace app\widgets\datatable;

/**
 * Class AddressDatatableWidget
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class AddressDatatableWidget extends \core\html\tables\datatable\AbstractTableTemplate implements \core\entities\WidgetInterface
{
    /**
     * Return Datatable template ADDRESS_LIST
     *
     * @PhpUnitGen\assertInstanceOf("Datatable::class")
     */
    public function ADDRESS_LIST(): \core\html\tables\datatable\Datatable
    {
        $table = new \core\html\tables\datatable\Datatable(
            \_('Address'),
            'address',
            [
                'idcontact' => [
                    'label' => _('Contact'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('contact', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idcontext' => [
                    'label' => _('Context'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('context', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'name' => [
                    'label' => _('Name'),
                    'type'  => 'text'
                ],
                'street' => [
                    'label' => _('Street'),
                    'type'  => 'text'
                ],
                'city' => [
                    'label' => _('City'),
                    'type'  => 'text'
                ],
                'zipcode' => [
                    'label' => _('Zipcode'),
                    'type'  => 'number'
                ],
                'country' => [
                    'label' => _('Country'),
                    'type'  => 'text'
                ],
                'geopoint' => [
                    'label' => _('Geopoint'),
                    'type'  => 'text'
                ]
            ]
        );

        $link = new \core\routing\Link(\app\api\controller\AddressController::class, 'datatableAction');
        $table->addEndpoint('readData', $link);

        $link = new \core\routing\Link(\app\api\controller\AddressController::class, 'editorAction');
        $table->addEndpoint('modifyData', $link);

        $link = new \core\routing\Link(\app\api\controller\AddressController::class, 'createAction');
        $table->addEndpoint('createData', $link);

        $link = new \core\routing\Link(\app\api\controller\AddressController::class, 'updateAction');
        $table->addEndpoint('updateData', $link);

        return $table;
    }
}
