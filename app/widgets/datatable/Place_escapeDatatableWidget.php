<?php namespace app\widgets\datatable;

/**
 * Class Place_escapeDatatableWidget
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class Place_escapeDatatableWidget extends \core\html\tables\datatable\AbstractTableTemplate implements \core\entities\WidgetInterface
{
    /**
     * Return Datatable template PLACE_ESCAPE_LIST
     *
     * @PhpUnitGen\assertInstanceOf("Datatable::class")
     */
    public function PLACE_ESCAPE_LIST(): \core\html\tables\datatable\Datatable
    {
        $table = new \core\html\tables\datatable\Datatable(
            \_('Place escape'),
            'place_escape',
            [
                'idcontext' => [
                    'label' => _('Context'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('context', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'size' => [
                    'label' => _('Size'),
                    'type'  => 'number'
                ],
                'idplace' => [
                    'label' => _('Place'),
                    'type'  => 'select',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('place', 'api'), 'htmlListAction'))->getEndpoint()
                ]
            ]
        );

        $link = new \core\routing\Link(\app\api\controller\Place_escapeController::class, 'datatableAction');
        $table->addEndpoint('readData', $link);

        $link = new \core\routing\Link(\app\api\controller\Place_escapeController::class, 'editorAction');
        $table->addEndpoint('modifyData', $link);

        $link = new \core\routing\Link(\app\api\controller\Place_escapeController::class, 'createAction');
        $table->addEndpoint('createData', $link);

        $link = new \core\routing\Link(\app\api\controller\Place_escapeController::class, 'updateAction');
        $table->addEndpoint('updateData', $link);

        return $table;
    }
}
