<?php namespace app\widgets\datatable;

/**
 * Class ContextDatatableWidget
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class ContextDatatableWidget extends \core\html\tables\datatable\AbstractTableTemplate implements \core\entities\WidgetInterface
{
    /**
     * Return Datatable template CONTEXT_LIST
     *
     * @PhpUnitGen\assertInstanceOf("Datatable::class")
     */
    public function CONTEXT_LIST(): \core\html\tables\datatable\Datatable
    {
        $table = new \core\html\tables\datatable\Datatable(
            \_('Context'),
            'context',
            [
                'idgdpr' => [
                    'label' => _('Gdpr'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('gdpr', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idcontact' => [
                    'label' => _('Contact'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('contact', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idsocial' => [
                    'label' => _('Social'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('social', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idhiring' => [
                    'label' => _('Hiring'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('hiring', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idaddress' => [
                    'label' => _('Address'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('address', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idplace' => [
                    'label' => _('Place'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('place', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idplace_contact' => [
                    'label' => _('Place contact'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('place_contact', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idplace_type' => [
                    'label' => _('Place type'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('place_type', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idplace_opening_time' => [
                    'label' => _('Place opening time'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('place_opening_time', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idplace_escape' => [
                    'label' => _('Place escape'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('place_escape', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idroom' => [
                    'label' => _('Room'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('room', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idroom_type' => [
                    'label' => _('Room type'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('room_type', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idroom_escape' => [
                    'label' => _('Room escape'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('room_escape', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idvat' => [
                    'label' => _('Vat'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('vat', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'iduser' => [
                    'label' => _('User'),
                    'type'  => 'select',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('user', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'name' => [
                    'label' => _('Name'),
                    'type'  => 'text'
                ],
                'status' => [
                    'label' => _('Status'),
                    'type'  => 'select',
                    'url'   => (new \core\routing\Link(app\api\controller\ContextController::class, 'tagListAction'))->getEndpoint()
                ],
                'idconnector' => [
                    'label' => _('Connector'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('connector', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idconnector_type' => [
                    'label' => _('Connector type'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('connector_type', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idrole' => [
                    'label' => _('Role'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('role', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idpermission' => [
                    'label' => _('Permission'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('permission', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idcompagny' => [
                    'label' => _('Compagny'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('compagny', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idcompagny_opening_time' => [
                    'label' => _('Compagny opening time'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('compagny_opening_time', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idcompagny_contact' => [
                    'label' => _('Compagny contact'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('compagny_contact', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idcompagny_type' => [
                    'label' => _('Compagny type'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('compagny_type', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idactivitie' => [
                    'label' => _('Activitie'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('activitie', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idactivitie_type' => [
                    'label' => _('Activitie type'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('activitie_type', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idoperation' => [
                    'label' => _('Operation'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('operation', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idknowledge' => [
                    'label' => _('Knowledge'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('knowledge', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idknowledge_type' => [
                    'label' => _('Knowledge type'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('knowledge_type', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idneed' => [
                    'label' => _('Need'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('need', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idneed_type' => [
                    'label' => _('Need type'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('need_type', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idevent' => [
                    'label' => _('Event'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('event', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idtask' => [
                    'label' => _('Task'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('task', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idcategorie' => [
                    'label' => _('Categorie'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('categorie', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idhistorical' => [
                    'label' => _('Historical'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('historical', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idlog' => [
                    'label' => _('Log'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('log', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idconfiguration' => [
                    'label' => _('Configuration'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('configuration', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idconnector_type_smtp' => [
                    'label' => _('Connector type smtp'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('connector_type_smtp', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idconnector_type_imap' => [
                    'label' => _('Connector type imap'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('connector_type_imap', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idcompagny_type_band' => [
                    'label' => _('Compagny type band'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('compagny_type_band', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idshow' => [
                    'label' => _('Show'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('show', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idshow_contact' => [
                    'label' => _('Show contact'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('show_contact', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idactivitie_type_show' => [
                    'label' => _('Activitie type show'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('activitie_type_show', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idactivitie_type_mounting' => [
                    'label' => _('Activitie type mounting'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('activitie_type_mounting', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idactivitie_type_unmounting' => [
                    'label' => _('Activitie type unmounting'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('activitie_type_unmounting', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idreminder' => [
                    'label' => _('Reminder'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('reminder', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idmeeting' => [
                    'label' => _('Meeting'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('meeting', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idskill' => [
                    'label' => _('Skill'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('skill', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idcertificate' => [
                    'label' => _('Certificate'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('certificate', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idhire' => [
                    'label' => _('Hire'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('hire', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idhire_period' => [
                    'label' => _('Hire period'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('hire_period', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idneed_type_skill' => [
                    'label' => _('Need type skill'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('need_type_skill', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idneed_type_certificate' => [
                    'label' => _('Need type certificate'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('need_type_certificate', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idknowledge_type_skill' => [
                    'label' => _('Knowledge type skill'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('knowledge_type_skill', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idknowledge_type_certificate' => [
                    'label' => _('Knowledge type certificate'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('knowledge_type_certificate', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idmaterial' => [
                    'label' => _('Material'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('material', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idmaterial_type' => [
                    'label' => _('Material type'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('material_type', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idmaterial_type_nonconsumable' => [
                    'label' => _('Material type nonconsumable'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('material_type_nonconsumable', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idmaterial_type_consumable' => [
                    'label' => _('Material type consumable'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('material_type_consumable', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idpackage' => [
                    'label' => _('Package'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('package', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idpackage_item' => [
                    'label' => _('Package item'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('package_item', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idpackage_affectation' => [
                    'label' => _('Package affectation'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('package_affectation', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idneed_type_package' => [
                    'label' => _('Need type package'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('need_type_package', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idquotation_request' => [
                    'label' => _('Quotation request'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('quotation_request', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idquotation_request_item' => [
                    'label' => _('Quotation request item'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('quotation_request_item', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idquotation_response' => [
                    'label' => _('Quotation response'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('quotation_response', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idquotation_response_item' => [
                    'label' => _('Quotation response item'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('quotation_response_item', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idprovider_purchase' => [
                    'label' => _('Provider purchase'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('provider_purchase', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idquotation' => [
                    'label' => _('Quotation'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('quotation', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idquotation_item' => [
                    'label' => _('Quotation item'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('quotation_item', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idorder' => [
                    'label' => _('Order'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('order', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idorder_item' => [
                    'label' => _('Order item'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('order_item', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idinvoice' => [
                    'label' => _('Invoice'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('invoice', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idinvoice_item' => [
                    'label' => _('Invoice item'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('invoice_item', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idrefund' => [
                    'label' => _('Refund'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('refund', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idrefund_item' => [
                    'label' => _('Refund item'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('refund_item', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idactivitie_type_transport' => [
                    'label' => _('Activitie type transport'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('activitie_type_transport', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idactivitie_type_travel' => [
                    'label' => _('Activitie type travel'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('activitie_type_travel', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idplace_type_hostel' => [
                    'label' => _('Place type hostel'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('place_type_hostel', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idroom_type_bedroom' => [
                    'label' => _('Room type bedroom'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('room_type_bedroom', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idnuity' => [
                    'label' => _('Nuity'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('nuity', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idreservation_request' => [
                    'label' => _('Reservation request'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('reservation_request', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idreservation_response' => [
                    'label' => _('Reservation response'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('reservation_response', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idroom_type_canteen' => [
                    'label' => _('Room type canteen'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('room_type_canteen', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idmeal' => [
                    'label' => _('Meal'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('meal', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idneed_type_menu' => [
                    'label' => _('Need type menu'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('need_type_menu', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idmenu_affectation' => [
                    'label' => _('Menu affectation'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('menu_affectation', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idmenu' => [
                    'label' => _('Menu'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('menu', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idmenu_item' => [
                    'label' => _('Menu item'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('menu_item', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idrecipe' => [
                    'label' => _('Recipe'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('recipe', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idrecipe_item' => [
                    'label' => _('Recipe item'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('recipe_item', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idmaterial_type_food' => [
                    'label' => _('Material type food'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('material_type_food', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idaccount' => [
                    'label' => _('Account'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('account', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idaccounting_category' => [
                    'label' => _('Accounting category'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('accounting_category', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idanalytical_item' => [
                    'label' => _('Analytical item'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('analytical_item', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idexpense' => [
                    'label' => _('Expense'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('expense', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idrevenue' => [
                    'label' => _('Revenue'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('revenue', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idsubsidie_request' => [
                    'label' => _('Subsidie request'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('subsidie_request', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idsubsidie_response' => [
                    'label' => _('Subsidie response'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('subsidie_response', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idbudget' => [
                    'label' => _('Budget'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('budget', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idbudget_view' => [
                    'label' => _('Budget view'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('budget_view', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idbudget_view_expense_item' => [
                    'label' => _('Budget view expense item'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('budget_view_expense_item', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idbudget_view_revenue_item' => [
                    'label' => _('Budget view revenue item'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('budget_view_revenue_item', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idbudget_expense_item' => [
                    'label' => _('Budget expense item'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('budget_expense_item', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idbudget_revenue_item' => [
                    'label' => _('Budget revenue item'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('budget_revenue_item', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idbudget_dynamic_expense_item' => [
                    'label' => _('Budget dynamic expense item'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('budget_dynamic_expense_item', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idbudget_dynamic_revenue_item' => [
                    'label' => _('Budget dynamic revenue item'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('budget_dynamic_revenue_item', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idpersonnal_expense' => [
                    'label' => _('Personnal expense'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('personnal_expense', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idroom_type_stock' => [
                    'label' => _('Room type stock'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('room_type_stock', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idstock_entrie' => [
                    'label' => _('Stock entrie'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('stock_entrie', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'iddocumentation' => [
                    'label' => _('Documentation'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('documentation', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'iddocumentation_item' => [
                    'label' => _('Documentation item'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('documentation_item', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idsurvey' => [
                    'label' => _('Survey'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('survey', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idsurvey_item' => [
                    'label' => _('Survey item'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('survey_item', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idsurvey_response' => [
                    'label' => _('Survey response'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('survey_response', 'api'), 'htmlListAction'))->getEndpoint()
                ]
            ]
        );

        $link = new \core\routing\Link(\app\api\controller\ContextController::class, 'datatableAction');
        $table->addEndpoint('readData', $link);

        $link = new \core\routing\Link(\app\api\controller\ContextController::class, 'editorAction');
        $table->addEndpoint('modifyData', $link);

        $link = new \core\routing\Link(\app\api\controller\ContextController::class, 'createAction');
        $table->addEndpoint('createData', $link);

        $link = new \core\routing\Link(\app\api\controller\ContextController::class, 'updateAction');
        $table->addEndpoint('updateData', $link);

        return $table;
    }
}
