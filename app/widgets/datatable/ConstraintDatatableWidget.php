<?php namespace app\widgets\datatable;

/**
 * Class ConstraintDatatableWidget
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class ConstraintDatatableWidget extends \core\html\tables\datatable\AbstractTableTemplate implements \core\entities\WidgetInterface
{
    /**
     * Return Datatable template CONSTRAINT_LIST
     *
     * @PhpUnitGen\assertInstanceOf("Datatable::class")
     */
    public function CONSTRAINT_LIST(): \core\html\tables\datatable\Datatable
    {
        $table = new \core\html\tables\datatable\Datatable(
            \_('Constraint'),
            'constraint',
            [
                'idcontext' => [
                    'label' => _('Context'),
                    'type'  => 'select',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('context', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'name' => [
                    'label' => _('Name'),
                    'type'  => 'text'
                ],
                'formula' => [
                    'label' => _('Formula'),
                    'type'  => 'text',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('formula', 'api'), 'htmlListAction'))->getEndpoint()
                ]
            ]
        );

        $link = new \core\routing\Link(\app\api\controller\ConstraintController::class, 'datatableAction');
        $table->addEndpoint('readData', $link);

        $link = new \core\routing\Link(\app\api\controller\ConstraintController::class, 'editorAction');
        $table->addEndpoint('modifyData', $link);

        $link = new \core\routing\Link(\app\api\controller\ConstraintController::class, 'createAction');
        $table->addEndpoint('createData', $link);

        $link = new \core\routing\Link(\app\api\controller\ConstraintController::class, 'updateAction');
        $table->addEndpoint('updateData', $link);

        return $table;
    }
}
