<?php

namespace app\widgets\datatable;

/**
 * Class SkeletonDatatableWidget
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class SkeletonDatatableWidget extends \core\html\tables\datatable\AbstractTableTemplate implements \core\entities\WidgetInterface
{
    /**
     * Return Datatable template SKELETON_LIST
     *
     * @PhpUnitGen\assertInstanceOf("Datatable::class")
     */
    public function SKELETON_LIST(): \core\html\tables\datatable\Datatable
    {
        $table = new \core\html\tables\datatable\Datatable(
            _('Skeleton'),
            'skeleton',
            [
                '__CONFIG__'
            ]
        );

        $link = new \core\routing\Link(\app\api\controller\SkeletonController::class, 'datatableAction');
        $table->addEndpoint('readData', $link);

        $link = new \core\routing\Link(\app\api\controller\SkeletonController::class, 'editorAction');
        $table->addEndpoint('modifyData', $link);

        $link = new \core\routing\Link(\app\api\controller\SkeletonController::class, 'createAction');
        $table->addEndpoint('createData', $link);

        $link = new \core\routing\Link(\app\api\controller\SkeletonController::class, 'updateAction');
        $table->addEndpoint('updateData', $link);

        $link = new \core\routing\Link(\app\api\controller\SkeletonController::class, 'deleteMultipleAction');
        $table->addEndpoint('deleteData', $link);


        return $table;
    }
}
