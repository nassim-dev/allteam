<?php namespace app\widgets\datatable;

/**
 * Class EventDatatableWidget
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class EventDatatableWidget extends \core\html\tables\datatable\AbstractTableTemplate implements \core\entities\WidgetInterface
{
    /**
     * Return Datatable template EVENT_LIST
     *
     * @PhpUnitGen\assertInstanceOf("Datatable::class")
     */
    public function EVENT_LIST(): \core\html\tables\datatable\Datatable
    {
        $table = new \core\html\tables\datatable\Datatable(
            \_('Event'),
            'event',
            [
                'idcontext' => [
                    'label' => _('Context'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('context', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'name' => [
                    'label' => _('Name'),
                    'type'  => 'text'
                ],
                'visual' => [
                    'label' => _('Visual'),
                    'type'  => 'fileinput'
                ],
                'description' => [
                    'label' => _('Description'),
                    'type'  => 'richtext'
                ],
                'start' => [
                    'label' => _('Start'),
                    'type'  => 'datetime'
                ],
                'status' => [
                    'label' => _('Status'),
                    'type'  => 'select',
                    'url'   => (new \core\routing\Link(app\api\controller\EventController::class, 'tagListAction'))->getEndpoint()
                ]
            ]
        );

        $link = new \core\routing\Link(\app\api\controller\EventController::class, 'datatableAction');
        $table->addEndpoint('readData', $link);

        $link = new \core\routing\Link(\app\api\controller\EventController::class, 'editorAction');
        $table->addEndpoint('modifyData', $link);

        $link = new \core\routing\Link(\app\api\controller\EventController::class, 'createAction');
        $table->addEndpoint('createData', $link);

        $link = new \core\routing\Link(\app\api\controller\EventController::class, 'updateAction');
        $table->addEndpoint('updateData', $link);

        return $table;
    }
}
