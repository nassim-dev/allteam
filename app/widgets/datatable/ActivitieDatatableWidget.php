<?php namespace app\widgets\datatable;

/**
 * Class ActivitieDatatableWidget
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class ActivitieDatatableWidget extends \core\html\tables\datatable\AbstractTableTemplate implements \core\entities\WidgetInterface
{
    /**
     * Return Datatable template ACTIVITIE_LIST
     *
     * @PhpUnitGen\assertInstanceOf("Datatable::class")
     */
    public function ACTIVITIE_LIST(): \core\html\tables\datatable\Datatable
    {
        $table = new \core\html\tables\datatable\Datatable(
            \_('Activitie'),
            'activitie',
            [
                'idcontext' => [
                    'label' => _('Context'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('context', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'room_reference' => [
                    'label' => _('Room reference'),
                    'type'  => 'autocomplete',
                    'url'   => (new \core\routing\Link(app\api\controller\ActivitieController::class, 'tagListAction'))->getEndpoint()
                ],
                'place_reference' => [
                    'label' => _('Place reference'),
                    'type'  => 'autocomplete',
                    'url'   => (new \core\routing\Link(app\api\controller\ActivitieController::class, 'tagListAction'))->getEndpoint()
                ],
                'name' => [
                    'label' => _('Name'),
                    'type'  => 'text'
                ],
                'idactivitie_type' => [
                    'label' => _('Activitie type'),
                    'type'  => 'select',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('activitie_type', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idoperation' => [
                    'label' => _('Operation'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('operation', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idneed' => [
                    'label' => _('Need'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('need', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'visual' => [
                    'label' => _('Visual'),
                    'type'  => 'fileinput'
                ],
                'description' => [
                    'label' => _('Description'),
                    'type'  => 'richtext'
                ],
                'tags' => [
                    'label' => _('Tags'),
                    'type'  => 'autocomplete',
                    'url'   => (new \core\routing\Link(app\api\controller\ActivitieController::class, 'tagListAction'))->getEndpoint()
                ]
            ]
        );

        $link = new \core\routing\Link(\app\api\controller\ActivitieController::class, 'datatableAction');
        $table->addEndpoint('readData', $link);

        $link = new \core\routing\Link(\app\api\controller\ActivitieController::class, 'editorAction');
        $table->addEndpoint('modifyData', $link);

        $link = new \core\routing\Link(\app\api\controller\ActivitieController::class, 'createAction');
        $table->addEndpoint('createData', $link);

        $link = new \core\routing\Link(\app\api\controller\ActivitieController::class, 'updateAction');
        $table->addEndpoint('updateData', $link);

        return $table;
    }
}
