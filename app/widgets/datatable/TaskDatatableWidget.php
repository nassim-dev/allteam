<?php namespace app\widgets\datatable;

/**
 * Class TaskDatatableWidget
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class TaskDatatableWidget extends \core\html\tables\datatable\AbstractTableTemplate implements \core\entities\WidgetInterface
{
    /**
     * Return Datatable template TASK_LIST
     *
     * @PhpUnitGen\assertInstanceOf("Datatable::class")
     */
    public function TASK_LIST(): \core\html\tables\datatable\Datatable
    {
        $table = new \core\html\tables\datatable\Datatable(
            \_('Task'),
            'task',
            [
                'idcontext' => [
                    'label' => _('Context'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('context', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'name' => [
                    'label' => _('Name'),
                    'type'  => 'text'
                ],
                'idoperation' => [
                    'label' => _('Operation'),
                    'type'  => 'select',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('operation', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idevent' => [
                    'label' => _('Event'),
                    'type'  => 'select',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('event', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idplace' => [
                    'label' => _('Place'),
                    'type'  => 'select',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('place', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idroom' => [
                    'label' => _('Room'),
                    'type'  => 'select',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('room', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'visual' => [
                    'label' => _('Visual'),
                    'type'  => 'fileinput'
                ],
                'lock' => [
                    'label' => _('Lock'),
                    'type'  => 'checkbox'
                ],
                'status' => [
                    'label' => _('Status'),
                    'type'  => 'select',
                    'url'   => (new \core\routing\Link(app\api\controller\TaskController::class, 'tagListAction'))->getEndpoint()
                ],
                'description' => [
                    'label' => _('Description'),
                    'type'  => 'richtext'
                ],
                'schedule' => [
                    'label' => _('Schedule'),
                    'type'  => 'datetimerange'
                ],
                'tags' => [
                    'label' => _('Tags'),
                    'type'  => 'autocomplete',
                    'url'   => (new \core\routing\Link(app\api\controller\TaskController::class, 'tagListAction'))->getEndpoint()
                ]
            ]
        );

        $link = new \core\routing\Link(\app\api\controller\TaskController::class, 'datatableAction');
        $table->addEndpoint('readData', $link);

        $link = new \core\routing\Link(\app\api\controller\TaskController::class, 'editorAction');
        $table->addEndpoint('modifyData', $link);

        $link = new \core\routing\Link(\app\api\controller\TaskController::class, 'createAction');
        $table->addEndpoint('createData', $link);

        $link = new \core\routing\Link(\app\api\controller\TaskController::class, 'updateAction');
        $table->addEndpoint('updateData', $link);

        return $table;
    }
}
