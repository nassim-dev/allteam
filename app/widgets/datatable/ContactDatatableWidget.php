<?php namespace app\widgets\datatable;

/**
 * Class ContactDatatableWidget
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class ContactDatatableWidget extends \core\html\tables\datatable\AbstractTableTemplate implements \core\entities\WidgetInterface
{
    /**
     * Return Datatable template CONTACT_LIST
     *
     * @PhpUnitGen\assertInstanceOf("Datatable::class")
     */
    public function CONTACT_LIST(): \core\html\tables\datatable\Datatable
    {
        $table = new \core\html\tables\datatable\Datatable(
            \_('Contact'),
            'contact',
            [
                'idcontext' => [
                    'label' => _('Context'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('context', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idaddress' => [
                    'label' => _('Address'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('address', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'iduser' => [
                    'label' => _('User'),
                    'type'  => 'select',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('user', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idsocial' => [
                    'label' => _('Social'),
                    'type'  => 'select',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('social', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idhiring' => [
                    'label' => _('Hiring'),
                    'type'  => 'select',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('hiring', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idknowledge' => [
                    'label' => _('Knowledge'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('knowledge', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'photo' => [
                    'label' => _('Photo'),
                    'type'  => 'fileinput'
                ],
                'civility' => [
                    'label' => _('Civility'),
                    'type'  => 'text'
                ],
                'pseudo' => [
                    'label' => _('Pseudo'),
                    'type'  => 'text'
                ],
                'name' => [
                    'label' => _('Name'),
                    'type'  => 'text'
                ],
                'first_name' => [
                    'label' => _('First name'),
                    'type'  => 'text'
                ],
                'phone' => [
                    'label' => _('Phone'),
                    'type'  => 'phone'
                ],
                'email' => [
                    'label' => _('Email'),
                    'type'  => 'email'
                ],
                'tags' => [
                    'label' => _('Tags'),
                    'type'  => 'autocomplete',
                    'url'   => (new \core\routing\Link(app\api\controller\ContactController::class, 'tagListAction'))->getEndpoint()
                ],
                'idplace_contact' => [
                    'label' => _('Place contact'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('place_contact', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idcompagny_contact' => [
                    'label' => _('Compagny contact'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('compagny_contact', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idshow' => [
                    'label' => _('Show'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('show', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idhire' => [
                    'label' => _('Hire'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('hire', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idmeal' => [
                    'label' => _('Meal'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('meal', 'api'), 'htmlListAction'))->getEndpoint()
                ]
            ]
        );

        $link = new \core\routing\Link(\app\api\controller\ContactController::class, 'datatableAction');
        $table->addEndpoint('readData', $link);

        $link = new \core\routing\Link(\app\api\controller\ContactController::class, 'editorAction');
        $table->addEndpoint('modifyData', $link);

        $link = new \core\routing\Link(\app\api\controller\ContactController::class, 'createAction');
        $table->addEndpoint('createData', $link);

        $link = new \core\routing\Link(\app\api\controller\ContactController::class, 'updateAction');
        $table->addEndpoint('updateData', $link);

        return $table;
    }
}
