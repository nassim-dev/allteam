<?php namespace app\widgets\datatable;

/**
 * Class Room_typeDatatableWidget
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class Room_typeDatatableWidget extends \core\html\tables\datatable\AbstractTableTemplate implements \core\entities\WidgetInterface
{
    /**
     * Return Datatable template ROOM_TYPE_LIST
     *
     * @PhpUnitGen\assertInstanceOf("Datatable::class")
     */
    public function ROOM_TYPE_LIST(): \core\html\tables\datatable\Datatable
    {
        $table = new \core\html\tables\datatable\Datatable(
            \_('Room type'),
            'room_type',
            [
                'idcontext' => [
                    'label' => _('Context'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('context', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'name' => [
                    'label' => _('Name'),
                    'type'  => 'text'
                ],
                'property' => [
                    'label' => _('Property'),
                    'type'  => 'text'
                ],
                'namespace' => [
                    'label' => _('Namespace'),
                    'type'  => 'text'
                ]
            ]
        );

        $link = new \core\routing\Link(\app\api\controller\Room_typeController::class, 'datatableAction');
        $table->addEndpoint('readData', $link);

        $link = new \core\routing\Link(\app\api\controller\Room_typeController::class, 'editorAction');
        $table->addEndpoint('modifyData', $link);

        $link = new \core\routing\Link(\app\api\controller\Room_typeController::class, 'createAction');
        $table->addEndpoint('createData', $link);

        $link = new \core\routing\Link(\app\api\controller\Room_typeController::class, 'updateAction');
        $table->addEndpoint('updateData', $link);

        return $table;
    }
}
