<?php namespace app\widgets\datatable;

/**
 * Class Constraint_schemaDatatableWidget
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class Constraint_schemaDatatableWidget extends \core\html\tables\datatable\AbstractTableTemplate implements \core\entities\WidgetInterface
{
    /**
     * Return Datatable template CONSTRAINT_SCHEMA_LIST
     *
     * @PhpUnitGen\assertInstanceOf("Datatable::class")
     */
    public function CONSTRAINT_SCHEMA_LIST(): \core\html\tables\datatable\Datatable
    {
        $table = new \core\html\tables\datatable\Datatable(
            \_('Constraint schema'),
            'constraint_schema',
            [
                'idcontext' => [
                    'label' => _('Context'),
                    'type'  => 'select',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('context', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idconstraint' => [
                    'label' => _('Constraint'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('constraint', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'name' => [
                    'label' => _('Name'),
                    'type'  => 'text'
                ],
                'tags' => [
                    'label' => _('Tags'),
                    'type'  => 'autocomplete',
                    'url'   => (new \core\routing\Link(app\api\controller\Constraint_schemaController::class, 'tagListAction'))->getEndpoint()
                ]
            ]
        );

        $link = new \core\routing\Link(\app\api\controller\Constraint_schemaController::class, 'datatableAction');
        $table->addEndpoint('readData', $link);

        $link = new \core\routing\Link(\app\api\controller\Constraint_schemaController::class, 'editorAction');
        $table->addEndpoint('modifyData', $link);

        $link = new \core\routing\Link(\app\api\controller\Constraint_schemaController::class, 'createAction');
        $table->addEndpoint('createData', $link);

        $link = new \core\routing\Link(\app\api\controller\Constraint_schemaController::class, 'updateAction');
        $table->addEndpoint('updateData', $link);

        return $table;
    }
}
