<?php namespace app\widgets\datatable;

/**
 * Class CompagnyDatatableWidget
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class CompagnyDatatableWidget extends \core\html\tables\datatable\AbstractTableTemplate implements \core\entities\WidgetInterface
{
    /**
     * Return Datatable template COMPAGNY_LIST
     *
     * @PhpUnitGen\assertInstanceOf("Datatable::class")
     */
    public function COMPAGNY_LIST(): \core\html\tables\datatable\Datatable
    {
        $table = new \core\html\tables\datatable\Datatable(
            \_('Compagny'),
            'compagny',
            [
                'idcontext' => [
                    'label' => _('Context'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('context', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idaddress' => [
                    'label' => _('Address'),
                    'type'  => 'select',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('address', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idcompagny_type' => [
                    'label' => _('Compagny type'),
                    'type'  => 'select',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('compagny_type', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'name' => [
                    'label' => _('Name'),
                    'type'  => 'text'
                ],
                'visual' => [
                    'label' => _('Visual'),
                    'type'  => 'fileinput'
                ],
                'description' => [
                    'label' => _('Description'),
                    'type'  => 'richtext'
                ],
                'idcompagny_opening_time' => [
                    'label' => _('Compagny opening time'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('compagny_opening_time', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idcompagny_contact' => [
                    'label' => _('Compagny contact'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('compagny_contact', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'tags' => [
                    'label' => _('Tags'),
                    'type'  => 'autocomplete',
                    'url'   => (new \core\routing\Link(app\api\controller\CompagnyController::class, 'tagListAction'))->getEndpoint()
                ]
            ]
        );

        $link = new \core\routing\Link(\app\api\controller\CompagnyController::class, 'datatableAction');
        $table->addEndpoint('readData', $link);

        $link = new \core\routing\Link(\app\api\controller\CompagnyController::class, 'editorAction');
        $table->addEndpoint('modifyData', $link);

        $link = new \core\routing\Link(\app\api\controller\CompagnyController::class, 'createAction');
        $table->addEndpoint('createData', $link);

        $link = new \core\routing\Link(\app\api\controller\CompagnyController::class, 'updateAction');
        $table->addEndpoint('updateData', $link);

        return $table;
    }
}
