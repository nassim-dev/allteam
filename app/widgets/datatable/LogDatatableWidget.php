<?php namespace app\widgets\datatable;

/**
 * Class LogDatatableWidget
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class LogDatatableWidget extends \core\html\tables\datatable\AbstractTableTemplate implements \core\entities\WidgetInterface
{
    /**
     * Return Datatable template LOG_LIST
     *
     * @PhpUnitGen\assertInstanceOf("Datatable::class")
     */
    public function LOG_LIST(): \core\html\tables\datatable\Datatable
    {
        $table = new \core\html\tables\datatable\Datatable(
            \_('Log'),
            'log',
            [
                'idcontext' => [
                    'label' => _('Context'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('context', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'iduser' => [
                    'label' => _('User'),
                    'type'  => 'select',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('user', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'content' => [
                    'label' => _('Content'),
                    'type'  => 'text'
                ]
            ]
        );

        $link = new \core\routing\Link(\app\api\controller\LogController::class, 'datatableAction');
        $table->addEndpoint('readData', $link);

        $link = new \core\routing\Link(\app\api\controller\LogController::class, 'editorAction');
        $table->addEndpoint('modifyData', $link);

        $link = new \core\routing\Link(\app\api\controller\LogController::class, 'createAction');
        $table->addEndpoint('createData', $link);

        $link = new \core\routing\Link(\app\api\controller\LogController::class, 'updateAction');
        $table->addEndpoint('updateData', $link);

        return $table;
    }
}
