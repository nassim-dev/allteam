<?php namespace app\widgets\datatable;

/**
 * Class NeedDatatableWidget
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class NeedDatatableWidget extends \core\html\tables\datatable\AbstractTableTemplate implements \core\entities\WidgetInterface
{
    /**
     * Return Datatable template NEED_LIST
     *
     * @PhpUnitGen\assertInstanceOf("Datatable::class")
     */
    public function NEED_LIST(): \core\html\tables\datatable\Datatable
    {
        $table = new \core\html\tables\datatable\Datatable(
            \_('Need'),
            'need',
            [
                'idcontext' => [
                    'label' => _('Context'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('context', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'name' => [
                    'label' => _('Name'),
                    'type'  => 'text'
                ],
                'idneed_type' => [
                    'label' => _('Need type'),
                    'type'  => 'select',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('need_type', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'visual' => [
                    'label' => _('Visual'),
                    'type'  => 'fileinput'
                ],
                'description' => [
                    'label' => _('Description'),
                    'type'  => 'richtext'
                ],
                'tags' => [
                    'label' => _('Tags'),
                    'type'  => 'autocomplete',
                    'url'   => (new \core\routing\Link(app\api\controller\NeedController::class, 'tagListAction'))->getEndpoint()
                ]
            ]
        );

        $link = new \core\routing\Link(\app\api\controller\NeedController::class, 'datatableAction');
        $table->addEndpoint('readData', $link);

        $link = new \core\routing\Link(\app\api\controller\NeedController::class, 'editorAction');
        $table->addEndpoint('modifyData', $link);

        $link = new \core\routing\Link(\app\api\controller\NeedController::class, 'createAction');
        $table->addEndpoint('createData', $link);

        $link = new \core\routing\Link(\app\api\controller\NeedController::class, 'updateAction');
        $table->addEndpoint('updateData', $link);

        return $table;
    }
}
