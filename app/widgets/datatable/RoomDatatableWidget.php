<?php namespace app\widgets\datatable;

/**
 * Class RoomDatatableWidget
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class RoomDatatableWidget extends \core\html\tables\datatable\AbstractTableTemplate implements \core\entities\WidgetInterface
{
    /**
     * Return Datatable template ROOM_LIST
     *
     * @PhpUnitGen\assertInstanceOf("Datatable::class")
     */
    public function ROOM_LIST(): \core\html\tables\datatable\Datatable
    {
        $table = new \core\html\tables\datatable\Datatable(
            \_('Room'),
            'room',
            [
                'idplace' => [
                    'label' => _('Place'),
                    'type'  => 'select',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('place', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idcontext' => [
                    'label' => _('Context'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('context', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'name' => [
                    'label' => _('Name'),
                    'type'  => 'text'
                ],
                'visual' => [
                    'label' => _('Visual'),
                    'type'  => 'fileinput'
                ],
                'description' => [
                    'label' => _('Description'),
                    'type'  => 'richtext'
                ],
                'tags' => [
                    'label' => _('Tags'),
                    'type'  => 'autocomplete',
                    'url'   => (new \core\routing\Link(app\api\controller\RoomController::class, 'tagListAction'))->getEndpoint()
                ],
                'idroom_escape' => [
                    'label' => _('Room escape'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('room_escape', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idroom_type' => [
                    'label' => _('Room type'),
                    'type'  => 'select',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('room_type', 'api'), 'htmlListAction'))->getEndpoint()
                ]
            ]
        );

        $link = new \core\routing\Link(\app\api\controller\RoomController::class, 'datatableAction');
        $table->addEndpoint('readData', $link);

        $link = new \core\routing\Link(\app\api\controller\RoomController::class, 'editorAction');
        $table->addEndpoint('modifyData', $link);

        $link = new \core\routing\Link(\app\api\controller\RoomController::class, 'createAction');
        $table->addEndpoint('createData', $link);

        $link = new \core\routing\Link(\app\api\controller\RoomController::class, 'updateAction');
        $table->addEndpoint('updateData', $link);

        return $table;
    }
}
