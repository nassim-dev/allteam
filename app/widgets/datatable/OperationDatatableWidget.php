<?php namespace app\widgets\datatable;

/**
 * Class OperationDatatableWidget
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class OperationDatatableWidget extends \core\html\tables\datatable\AbstractTableTemplate implements \core\entities\WidgetInterface
{
    /**
     * Return Datatable template OPERATION_LIST
     *
     * @PhpUnitGen\assertInstanceOf("Datatable::class")
     */
    public function OPERATION_LIST(): \core\html\tables\datatable\Datatable
    {
        $table = new \core\html\tables\datatable\Datatable(
            \_('Operation'),
            'operation',
            [
                'idactivitie' => [
                    'label' => _('Activitie'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('activitie', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idcontext' => [
                    'label' => _('Context'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('context', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'name' => [
                    'label' => _('Name'),
                    'type'  => 'text'
                ],
                'idneed' => [
                    'label' => _('Need'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('need', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'duration' => [
                    'label' => _('Duration'),
                    'type'  => 'time'
                ],
                'constraint_type' => [
                    'label' => _('Constraint type'),
                    'type'  => 'checkbox'
                ],
                'constraint_value' => [
                    'label' => _('Constraint value'),
                    'type'  => 'time'
                ],
                'room_reference' => [
                    'label' => _('Room reference'),
                    'type'  => 'text'
                ],
                'place_reference' => [
                    'label' => _('Place reference'),
                    'type'  => 'text'
                ],
                'visual' => [
                    'label' => _('Visual'),
                    'type'  => 'fileinput'
                ],
                'description' => [
                    'label' => _('Description'),
                    'type'  => 'richtext'
                ],
                'idoperation_parent' => [
                    'label' => _('Operation parent'),
                    'type'  => 'select',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('operation_parent', 'api'), 'htmlListAction'))->getEndpoint()
                ]
            ]
        );

        $link = new \core\routing\Link(\app\api\controller\OperationController::class, 'datatableAction');
        $table->addEndpoint('readData', $link);

        $link = new \core\routing\Link(\app\api\controller\OperationController::class, 'editorAction');
        $table->addEndpoint('modifyData', $link);

        $link = new \core\routing\Link(\app\api\controller\OperationController::class, 'createAction');
        $table->addEndpoint('createData', $link);

        $link = new \core\routing\Link(\app\api\controller\OperationController::class, 'updateAction');
        $table->addEndpoint('updateData', $link);

        return $table;
    }
}
