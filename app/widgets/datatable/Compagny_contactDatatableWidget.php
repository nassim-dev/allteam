<?php namespace app\widgets\datatable;

/**
 * Class Compagny_contactDatatableWidget
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class Compagny_contactDatatableWidget extends \core\html\tables\datatable\AbstractTableTemplate implements \core\entities\WidgetInterface
{
    /**
     * Return Datatable template COMPAGNY_CONTACT_LIST
     *
     * @PhpUnitGen\assertInstanceOf("Datatable::class")
     */
    public function COMPAGNY_CONTACT_LIST(): \core\html\tables\datatable\Datatable
    {
        $table = new \core\html\tables\datatable\Datatable(
            \_('Compagny contact'),
            'compagny_contact',
            [
                'idcompagny' => [
                    'label' => _('Compagny'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('compagny', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idcontext' => [
                    'label' => _('Context'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('context', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idcontact' => [
                    'label' => _('Contact'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('contact', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'name' => [
                    'label' => _('Name'),
                    'type'  => 'text'
                ],
                'roles' => [
                    'label' => _('Roles'),
                    'type'  => 'autocomplete',
                    'url'   => (new \core\routing\Link(app\api\controller\Compagny_contactController::class, 'tagListAction'))->getEndpoint()
                ]
            ]
        );

        $link = new \core\routing\Link(\app\api\controller\Compagny_contactController::class, 'datatableAction');
        $table->addEndpoint('readData', $link);

        $link = new \core\routing\Link(\app\api\controller\Compagny_contactController::class, 'editorAction');
        $table->addEndpoint('modifyData', $link);

        $link = new \core\routing\Link(\app\api\controller\Compagny_contactController::class, 'createAction');
        $table->addEndpoint('createData', $link);

        $link = new \core\routing\Link(\app\api\controller\Compagny_contactController::class, 'updateAction');
        $table->addEndpoint('updateData', $link);

        return $table;
    }
}
