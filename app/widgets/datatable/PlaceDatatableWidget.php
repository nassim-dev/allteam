<?php namespace app\widgets\datatable;

/**
 * Class PlaceDatatableWidget
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class PlaceDatatableWidget extends \core\html\tables\datatable\AbstractTableTemplate implements \core\entities\WidgetInterface
{
    /**
     * Return Datatable template PLACE_LIST
     *
     * @PhpUnitGen\assertInstanceOf("Datatable::class")
     */
    public function PLACE_LIST(): \core\html\tables\datatable\Datatable
    {
        $table = new \core\html\tables\datatable\Datatable(
            \_('Place'),
            'place',
            [
                'idcontext' => [
                    'label' => _('Context'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('context', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idaddress' => [
                    'label' => _('Address'),
                    'type'  => 'select',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('address', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idplace_type' => [
                    'label' => _('Place type'),
                    'type'  => 'select',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('place_type', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idroom' => [
                    'label' => _('Room'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('room', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'name' => [
                    'label' => _('Name'),
                    'type'  => 'text'
                ],
                'visual' => [
                    'label' => _('Visual'),
                    'type'  => 'fileinput'
                ],
                'description' => [
                    'label' => _('Description'),
                    'type'  => 'richtext'
                ],
                'idplace_opening_time' => [
                    'label' => _('Place opening time'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('place_opening_time', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idplace_escape' => [
                    'label' => _('Place escape'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('place_escape', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idplace_contact' => [
                    'label' => _('Place contact'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('place_contact', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'tags' => [
                    'label' => _('Tags'),
                    'type'  => 'autocomplete',
                    'url'   => (new \core\routing\Link(app\api\controller\PlaceController::class, 'tagListAction'))->getEndpoint()
                ]
            ]
        );

        $link = new \core\routing\Link(\app\api\controller\PlaceController::class, 'datatableAction');
        $table->addEndpoint('readData', $link);

        $link = new \core\routing\Link(\app\api\controller\PlaceController::class, 'editorAction');
        $table->addEndpoint('modifyData', $link);

        $link = new \core\routing\Link(\app\api\controller\PlaceController::class, 'createAction');
        $table->addEndpoint('createData', $link);

        $link = new \core\routing\Link(\app\api\controller\PlaceController::class, 'updateAction');
        $table->addEndpoint('updateData', $link);

        return $table;
    }
}
