<?php namespace app\widgets\datatable;

/**
 * Class UserDatatableWidget
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class UserDatatableWidget extends \core\html\tables\datatable\AbstractTableTemplate implements \core\entities\WidgetInterface
{
    /**
     * Return Datatable template USER_LIST
     *
     * @PhpUnitGen\assertInstanceOf("Datatable::class")
     */
    public function USER_LIST(): \core\html\tables\datatable\Datatable
    {
        $table = new \core\html\tables\datatable\Datatable(
            \_('User'),
            'user',
            [
                'idcontext' => [
                    'label' => _('Context'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('context', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'email' => [
                    'label' => _('Email'),
                    'type'  => 'email'
                ],
                'password' => [
                    'label' => _('Password'),
                    'type'  => 'password'
                ],
                'status' => [
                    'label' => _('Status'),
                    'type'  => 'select',
                    'url'   => (new \core\routing\Link(app\api\controller\UserController::class, 'tagListAction'))->getEndpoint()
                ],
                'enabled' => [
                    'label' => _('Enabled'),
                    'type'  => 'checkbox'
                ],
                'validation' => [
                    'label' => _('Validation'),
                    'type'  => 'text'
                ],
                'hash_validation' => [
                    'label' => _('Hash validation'),
                    'type'  => 'text'
                ],
                'language' => [
                    'label' => _('Language'),
                    'type'  => 'select',
                    'url'   => (new \core\routing\Link(app\api\controller\UserController::class, 'tagListAction'))->getEndpoint()
                ]
            ]
        );

        $link = new \core\routing\Link(\app\api\controller\UserController::class, 'datatableAction');
        $table->addEndpoint('readData', $link);

        $link = new \core\routing\Link(\app\api\controller\UserController::class, 'editorAction');
        $table->addEndpoint('modifyData', $link);

        $link = new \core\routing\Link(\app\api\controller\UserController::class, 'createAction');
        $table->addEndpoint('createData', $link);

        $link = new \core\routing\Link(\app\api\controller\UserController::class, 'updateAction');
        $table->addEndpoint('updateData', $link);

        return $table;
    }
}
