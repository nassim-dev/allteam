<?php namespace app\widgets\datatable;

/**
 * Class Room_escapeDatatableWidget
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class Room_escapeDatatableWidget extends \core\html\tables\datatable\AbstractTableTemplate implements \core\entities\WidgetInterface
{
    /**
     * Return Datatable template ROOM_ESCAPE_LIST
     *
     * @PhpUnitGen\assertInstanceOf("Datatable::class")
     */
    public function ROOM_ESCAPE_LIST(): \core\html\tables\datatable\Datatable
    {
        $table = new \core\html\tables\datatable\Datatable(
            \_('Room escape'),
            'room_escape',
            [
                'idroom' => [
                    'label' => _('Room'),
                    'type'  => 'select',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('room', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idcontext' => [
                    'label' => _('Context'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('context', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'size' => [
                    'label' => _('Size'),
                    'type'  => 'number'
                ]
            ]
        );

        $link = new \core\routing\Link(\app\api\controller\Room_escapeController::class, 'datatableAction');
        $table->addEndpoint('readData', $link);

        $link = new \core\routing\Link(\app\api\controller\Room_escapeController::class, 'editorAction');
        $table->addEndpoint('modifyData', $link);

        $link = new \core\routing\Link(\app\api\controller\Room_escapeController::class, 'createAction');
        $table->addEndpoint('createData', $link);

        $link = new \core\routing\Link(\app\api\controller\Room_escapeController::class, 'updateAction');
        $table->addEndpoint('updateData', $link);

        return $table;
    }
}
