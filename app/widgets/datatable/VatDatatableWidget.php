<?php namespace app\widgets\datatable;

/**
 * Class VatDatatableWidget
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class VatDatatableWidget extends \core\html\tables\datatable\AbstractTableTemplate implements \core\entities\WidgetInterface
{
    /**
     * Return Datatable template VAT_LIST
     *
     * @PhpUnitGen\assertInstanceOf("Datatable::class")
     */
    public function VAT_LIST(): \core\html\tables\datatable\Datatable
    {
        $table = new \core\html\tables\datatable\Datatable(
            \_('Vat'),
            'vat',
            [
                'idcontext' => [
                    'label' => _('Context'),
                    'type'  => 'multiselect',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('context', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idaccount_deducted' => [
                    'label' => _('Account deducted'),
                    'type'  => 'select',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('account_deducted', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'idaccount_collected' => [
                    'label' => _('Account collected'),
                    'type'  => 'select',
                    'url'   => (new \core\routing\Link($this->controllerService->getClass('account_collected', 'api'), 'htmlListAction'))->getEndpoint()
                ],
                'name' => [
                    'label' => _('Name'),
                    'type'  => 'text'
                ],
                'percentage' => [
                    'label' => _('Percentage'),
                    'type'  => 'number'
                ]
            ]
        );

        $link = new \core\routing\Link(\app\api\controller\VatController::class, 'datatableAction');
        $table->addEndpoint('readData', $link);

        $link = new \core\routing\Link(\app\api\controller\VatController::class, 'editorAction');
        $table->addEndpoint('modifyData', $link);

        $link = new \core\routing\Link(\app\api\controller\VatController::class, 'createAction');
        $table->addEndpoint('createData', $link);

        $link = new \core\routing\Link(\app\api\controller\VatController::class, 'updateAction');
        $table->addEndpoint('updateData', $link);

        return $table;
    }
}
