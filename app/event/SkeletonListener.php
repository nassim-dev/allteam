<?php

namespace app\event;

/**
 * Class  SkeletonCallbackManager
 *
 * @PhpUnitGen\mock("\\app\\entitie\\Skeleton", "skeleton")
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class SkeletonListener extends \app\event\EntitieListenerBase
{
    /**
     * Callback to execute afer create row
     *
     * @PhpUnitGen\params("$this->skeleton")
     * @PhpUnitGen\assertInternalType("'array'")
     * @PhpUnitGen\assertArrayHasKey("'obj'")
     */
    #[\core\events\ListenerAttribute(event:'EVENT_NAME')]
    public function callbackPostAdd(array &$args): array
    {
        return $args;
    }
}
