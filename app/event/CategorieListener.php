<?php namespace app\event;

/**
 * Class CategorieListener
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class CategorieListener extends EntitieListenerBase
{
    #[\core\events\ListenerAttribute(event: 'DEFAULT_CATEGORIE_EVENT')]
    public function defaultListener(array &$args): array
    {
        return $args;
    }
}
