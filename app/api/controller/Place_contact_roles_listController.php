<?php

namespace app\api\controller;

/**
 * Class Place_contact_roles_listController
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class Place_contact_roles_listController extends ApiControllerBase
{
    /**
     * Undocumented function
     */
    public function __construct(
        public \core\secure\authentification\AuthServiceInterface $auth,
        public \core\messages\request\HttpRequestInterface $request,
        public \core\messages\response\HttpResponseInterface $response,
        public \core\database\RepositoryFactoryInterface $repositoryFactory,
    ) {
        parent::__construct($auth, $request, $response, $repositoryFactory);
        $this->dbManager = $repositoryFactory->table('place_contact_roles_list');
        $this->setTable('place_contact_roles_list');
    }

    /**
     * Copy an entitie by key
     *
     * @RouteRequiredParameters($idplace_contact_roles_list:int)
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/place_contact_roles_list/copy/:idplace_contact_roles_list',
        method: 'POST',
        permission: ['create:place_contact_roles_list'],
        encoding: \false,
        visible: \false
    )]
    public function copyAction(\core\notifications\NotificationServiceInterface $notifier, \core\tasks\AsyncJob $task)
    {
        parent::copyAction($notifier, $task);
    }

    /**
     * Create an entitie
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/place_contact_roles_list/create',
        method: 'POST',
        permission: ['create:place_contact_roles_list'],
        encoding: \false,
        visible: \false
    )]
    public function createAction(
        \core\notifications\NotificationServiceInterface $notifier,
        \core\entities\EntitieFactoryInterface $entitieFactory,
        \core\html\form\FormFactory $formFactory,
        \core\tasks\AsyncJob $task
    ) {
        parent::createAction($notifier, $entitieFactory, $formFactory, $task);
    }

    /**
     * Get all elements formated to by showed in datatable pluggin
     *
     * @param null|array $objects
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/place_contact_roles_list/datatable',
        method: 'POST',
        permission: ['read:place_contact_roles_list'],
        encoding: \false,
        visible: \false
    )]
    public function datatableAction(
        \core\database\mysql\lib\nette\CustomNetteSelection|array|null $objects = \null,
        array $datatableArgs = []
    ) {
        parent::datatableAction($objects, $datatableArgs);
    }

    /**
     * Use to create entitie with just required fields
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/place_contact_roles_list/createInline',
        method: 'POST',
        permission: ['create:place_contact_roles_list'],
        encoding: \false,
        visible: \false
    )]
    public function createInlineAction(
        \core\notifications\NotificationServiceInterface $notifier,
        \core\entities\EntitieFactoryInterface $entitieFactory,
        \core\secure\csrf\CsrfServiceInterface $csrf
    ) {
        parent::createInlineAction($notifier, $entitieFactory, $csrf);
    }

    /**
     * Delete an entitie by key
     *
     * @RouteRequiredParameters($idplace_contact_roles_list:int)
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/place_contact_roles_list/delete/:idplace_contact_roles_list',
        method: 'DELETE',
        permission: ['delete:place_contact_roles_list'],
        encoding: \false,
        visible: \false
    )]
    public function deleteAction(\core\notifications\NotificationServiceInterface $notifier)
    {
        parent::deleteAction($notifier);
    }

    /**
     * Delete multiples entities by keys
     *
     * @RouteRequiredParameters($keys:array)
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/place_contact_roles_list/deleteMultiples',
        method: 'POST',
        permission: ['delete:place_contact_roles_list'],
        encoding: \false,
        visible: \false
    )]
    public function deleteMultipleAction(\core\notifications\NotificationServiceInterface $notifier)
    {
        parent::deleteMultipleAction($notifier);
    }

    /**
     * Download an entitie as pdf
     *
     * @RouteRequiredParameters($idplace_contact_roles_list:int)
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/place_contact_roles_list/pdf/:idplace_contact_roles_list',
        method: 'GET',
        permission: ['read:place_contact_roles_list'],
        encoding: \false,
        visible: \false
    )]
    public function downloadAction(
        \core\html\AssetsServiceInterface $assetsService,
        \core\view\ViewServiceInterface $viewService,
        \core\config\Config $config
    ) {
        parent::downloadAction($assetsService, $viewService, $config);
    }

    /**
     * Update specific field of entitie with datatable editor pluggin
     *
     * @RouteRequiredParameters($idplace_contact_roles_list:int)
     * @RouteRequiredParameters($datas:array)
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/place_contact_roles_list/editor/:idplace_contact_roles_list',
        method: 'PATCH',
        permission: ['update:place_contact_roles_list'],
        encoding: \false,
        visible: \false
    )]
    public function editorAction(
        \core\notifications\NotificationServiceInterface $notifier,
        \core\events\EventManagerInterface $eventManager
    ) {
        parent::editorAction($notifier, $eventManager);
    }

    /**
     * Lock & unlock entitie
     *
     * @RouteRequiredParameters($idplace_contact_roles_list:int)
     * @RouteRequiredParameters($state:string)
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/place_contact_roles_list/state/:idplace_contact_roles_list',
        method: 'POST',
        permission: ['@public'],
        encoding: \false,
        visible: \false
    )]
    public function entitieStateAction(\core\cache\CacheServiceInterface $cache)
    {
        parent::entitieStateAction($cache);
    }

    /**
     * Get list for select fields
     *
     * @RouteExtraParameters($datas:array)
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/place_contact_roles_list/select',
        method: 'POST',
        permission: ['@public'],
        encoding: \false,
        visible: \false
    )]
    public function formListAction()
    {
        parent::formListAction();
    }

    /**
     * Return Tag List updated
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/place_contact_roles_list/tag',
        method: 'POST',
        permission: ['@public'],
        encoding: \false,
        visible: \false
    )]
    public function tagListAction()
    {
        parent::tagListAction();
    }

    /**
     * Get all entities
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/place_contact_roles_list/all',
        method: 'GET',
        permission: ['read:place_contact_roles_list'],
        encoding: \false,
        visible: \false
    )]
    public function getAllAction()
    {
        parent::getAllAction();
    }

    /**
     * Get entitie by key
     *
     * @RouteRequiredParameters($idplace_contact_roles_list:int)
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/place_contact_roles_list/byId/:idplace_contact_roles_list',
        method: 'GET',
        permission: ['read:place_contact_roles_list'],
        encoding: false,
        visible: false
    )]
    public function getByIdAction(\core\notifications\NotificationServiceInterface $notifier)
    {
        parent::getByIdAction($notifier);
    }

    /**
     * Get entities by parameters
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/place_contact_roles_list/byParameters',
        method: 'POST',
        permission: ['read:place_contact_roles_list'],
        encoding: \false,
        visible: \false
    )]
    public function getByParametersAction()
    {
        parent::getByParametersAction();
    }

    /**
     * Get related entitie by key
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/place_contact_roles_list/related/:idplace_contact_roles_list',
        method: 'POST',
        permission: ['read:place_contact_roles_list'],
        encoding: \false,
        visible: \false
    )]
    public function relatedAction()
    {
        parent::relatedAction();
    }

    /**
     * Get initial value for a select
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/place_contact_roles_list/selectById',
        method: 'POST',
        permission: ['@public'],
        encoding: \false,
        visible: \false
    )]
    public function htmlListByIdAction()
    {
        parent::htmlListByIdAction();
    }

    /**
     * Return Html List updated
     *
     * @RouteExtraParameters($q:string)
     * @RouteExtraParameters($p:int)
     * @RouteExtraParameters($datas:array)
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/place_contact_roles_list/search',
        method: 'POST',
        permission: ['@public'],
        encoding: \false,
        visible: \false
    )]
    public function htmlListAction()
    {
        parent::htmlListAction();
    }

    /**
     * Default method
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/place_contact_roles_list',
        method: 'GET',
        permission: ['read:place_contact_roles_list'],
        encoding: \false,
        visible: \false
    )]
    public function indexAction()
    {
        parent::indexAction();
    }

    /**
     * Update an entitie by key
     *
     * @RouteRequiredParameters($idplace_contact_roles_list:int)
     * @RouteRequiredParameters($datas:array)
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/place_contact_roles_list/update',
        method: 'PUT',
        permission: ['update:place_contact_roles_list'],
        encoding: \false,
        visible: \false
    )]
    public function updateAction(
        \core\notifications\NotificationServiceInterface $notifier,
        \core\html\form\FormFactory $formFactory,
        \core\tasks\AsyncJob $task
    ) {
        parent::updateAction($notifier, $formFactory, $task);
    }
}
