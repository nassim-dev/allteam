<?php

namespace app\api\controller;

/**
 * Class ContactController
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class ContactController extends ApiControllerBase
{
    /**
     * Undocumented function
     */
    public function __construct(
        public \core\secure\authentification\AuthServiceInterface $auth,
        public \core\messages\request\HttpRequestInterface $request,
        public \core\messages\response\HttpResponseInterface $response,
        public \core\database\RepositoryFactoryInterface $repositoryFactory,
    ) {
        parent::__construct($auth, $request, $response, $repositoryFactory);
        $this->dbManager = $repositoryFactory->table('contact');
        $this->setTable('contact');
    }

    /**
     * Copy an entitie by key
     *
     * @RouteRequiredParameters($idcontact:int)
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/contact/copy/:idcontact',
        method: 'POST',
        permission: ['create:contact'],
        encoding: \false,
        visible: \false
    )]
    public function copyAction(\core\notifications\NotificationServiceInterface $notifier, \core\tasks\AsyncJob $task)
    {
        parent::copyAction($notifier, $task);
    }

    /**
     * Create an entitie
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/contact/create',
        method: 'POST',
        permission: ['create:contact'],
        encoding: \false,
        visible: \false
    )]
    public function createAction(
        \core\notifications\NotificationServiceInterface $notifier,
        \core\entities\EntitieFactoryInterface $entitieFactory,
        \core\html\form\FormFactory $formFactory,
        \core\tasks\AsyncJob $task
    ) {
        parent::createAction($notifier, $entitieFactory, $formFactory, $task);
    }

    /**
     * Get all elements formated to by showed in datatable pluggin
     *
     * @param null|array $objects
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/contact/datatable',
        method: 'POST',
        permission: ['read:contact'],
        encoding: \false,
        visible: \false
    )]
    public function datatableAction(
        \core\database\mysql\lib\nette\CustomNetteSelection|array|null $objects = \null,
        array $datatableArgs = []
    ) {
        parent::datatableAction($objects, $datatableArgs);
    }

    /**
     * Use to create entitie with just required fields
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/contact/createInline',
        method: 'POST',
        permission: ['create:contact'],
        encoding: \false,
        visible: \false
    )]
    public function createInlineAction(
        \core\notifications\NotificationServiceInterface $notifier,
        \core\entities\EntitieFactoryInterface $entitieFactory,
        \core\secure\csrf\CsrfServiceInterface $csrf
    ) {
        parent::createInlineAction($notifier, $entitieFactory, $csrf);
    }

    /**
     * Delete an entitie by key
     *
     * @RouteRequiredParameters($idcontact:int)
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/contact/delete/:idcontact',
        method: 'DELETE',
        permission: ['delete:contact'],
        encoding: \false,
        visible: \false
    )]
    public function deleteAction(\core\notifications\NotificationServiceInterface $notifier)
    {
        parent::deleteAction($notifier);
    }

    /**
     * Delete multiples entities by keys
     *
     * @RouteRequiredParameters($keys:array)
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/contact/deleteMultiples',
        method: 'POST',
        permission: ['delete:contact'],
        encoding: \false,
        visible: \false
    )]
    public function deleteMultipleAction(\core\notifications\NotificationServiceInterface $notifier)
    {
        parent::deleteMultipleAction($notifier);
    }

    /**
     * Download an entitie as pdf
     *
     * @RouteRequiredParameters($idcontact:int)
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/contact/pdf/:idcontact',
        method: 'GET',
        permission: ['read:contact'],
        encoding: \false,
        visible: \false
    )]
    public function downloadAction(
        \core\html\AssetsServiceInterface $assetsService,
        \core\view\ViewServiceInterface $viewService,
        \core\config\Config $config
    ) {
        parent::downloadAction($assetsService, $viewService, $config);
    }

    /**
     * Update specific field of entitie with datatable editor pluggin
     *
     * @RouteRequiredParameters($idcontact:int)
     * @RouteRequiredParameters($datas:array)
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/contact/editor/:idcontact',
        method: 'PATCH',
        permission: ['update:contact'],
        encoding: \false,
        visible: \false
    )]
    public function editorAction(
        \core\notifications\NotificationServiceInterface $notifier,
        \core\events\EventManagerInterface $eventManager
    ) {
        parent::editorAction($notifier, $eventManager);
    }

    /**
     * Lock & unlock entitie
     *
     * @RouteRequiredParameters($idcontact:int)
     * @RouteRequiredParameters($state:string)
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/contact/state/:idcontact',
        method: 'POST',
        permission: ['@public'],
        encoding: \false,
        visible: \false
    )]
    public function entitieStateAction(\core\cache\CacheServiceInterface $cache)
    {
        parent::entitieStateAction($cache);
    }

    /**
     * Get list for select fields
     *
     * @RouteExtraParameters($datas:array)
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(url: '/api/contact/select', method: 'POST', permission: ['@public'], encoding: \false, visible: \false)]
    public function formListAction()
    {
        parent::formListAction();
    }

    /**
     * Return Tag List updated
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(url: '/api/contact/tag', method: 'POST', permission: ['@public'], encoding: \false, visible: \false)]
    public function tagListAction()
    {
        parent::tagListAction();
    }

    /**
     * Get all entities
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(url: '/api/contact/all', method: 'GET', permission: ['read:contact'], encoding: \false, visible: \false)]
    public function getAllAction()
    {
        parent::getAllAction();
    }

    /**
     * Get entitie by key
     *
     * @RouteRequiredParameters($idcontact:int)
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(url: '/api/contact/byId/:idcontact', method: 'GET', permission: ['read:contact'], encoding: false, visible: false)]
    public function getByIdAction(\core\notifications\NotificationServiceInterface $notifier)
    {
        parent::getByIdAction($notifier);
    }

    /**
     * Get entities by parameters
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/contact/byParameters',
        method: 'POST',
        permission: ['read:contact'],
        encoding: \false,
        visible: \false
    )]
    public function getByParametersAction()
    {
        parent::getByParametersAction();
    }

    /**
     * Get related entitie by key
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/contact/related/:idcontact',
        method: 'POST',
        permission: ['read:contact'],
        encoding: \false,
        visible: \false
    )]
    public function relatedAction()
    {
        parent::relatedAction();
    }

    /**
     * Get initial value for a select
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(url: '/api/contact/selectById', method: 'POST', permission: ['@public'], encoding: \false, visible: \false)]
    public function htmlListByIdAction()
    {
        parent::htmlListByIdAction();
    }

    /**
     * Return Html List updated
     *
     * @RouteExtraParameters($q:string)
     * @RouteExtraParameters($p:int)
     * @RouteExtraParameters($datas:array)
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(url: '/api/contact/search', method: 'POST', permission: ['@public'], encoding: \false, visible: \false)]
    public function htmlListAction()
    {
        parent::htmlListAction();
    }

    /**
     * Default method
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(url: '/api/contact', method: 'GET', permission: ['read:contact'], encoding: \false, visible: \false)]
    public function indexAction()
    {
        parent::indexAction();
    }

    /**
     * Update an entitie by key
     *
     * @RouteRequiredParameters($idcontact:int)
     * @RouteRequiredParameters($datas:array)
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(url: '/api/contact/update', method: 'PUT', permission: ['update:contact'], encoding: \false, visible: \false)]
    public function updateAction(
        \core\notifications\NotificationServiceInterface $notifier,
        \core\html\form\FormFactory $formFactory,
        \core\tasks\AsyncJob $task
    ) {
        parent::updateAction($notifier, $formFactory, $task);
    }
}
