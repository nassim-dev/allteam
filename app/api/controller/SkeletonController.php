<?php

namespace app\api\controller;

use core\notifications\NotificationServiceInterface;

/**
 * SkeletonController
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class SkeletonController extends \app\api\controller\ApiControllerBase
{
    /**
     * Undocumented function
     */
    public function __construct(
        protected \core\secure\authentification\AuthServiceInterface $auth,
        protected \core\messages\request\HttpRequestInterface $request,
        protected \core\messages\response\HttpResponseInterface $response,
        protected \core\database\RepositoryFactoryInterface $repositoryFactory
    ) {
        parent::__construct($auth, $request, $response, $repositoryFactory);
        $this->dbManager = $repositoryFactory->table('skeleton');
        $this->setTable('skeleton');
    }

    /**
     * Copy an entitie by key
     *
     * @RouteRequiredParameters($idskeleton:int)
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url:'/api/skeleton/copy/:idskeleton',
        method:'POST',
        permission:['create:skeleton'],
        encoding:false,
        visible:false
    )]
    public function copyAction(
        \core\notifications\NotificationServiceInterface $notifier,
        \core\tasks\AsyncJob $task
    ) {
        parent::copyAction($notifier, $task);
    }

    /**
     * Create an entitie
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url:'/api/skeleton/create',
        method:'POST',
        permission:['create:skeleton'],
        encoding:false,
        visible:false
    )]
    public function createAction(
        \core\notifications\NotificationServiceInterface $notifier,
        \core\entities\EntitieFactoryInterface $entitieFactory,
        \core\html\form\FormFactory $formFactory,
        \core\tasks\AsyncJob $task
    ) {
        parent::createAction($notifier, $entitieFactory, $formFactory, $task);
    }

    /**
     * Get all elements formated to by showed in datatable pluggin
     *
     * @param null|array $objects
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url:'/api/skeleton/datatable',
        method:'POST',
        permission:['read:skeleton'],
        encoding:false,
        visible:false
    )]
    public function datatableAction(
        \core\database\mysql\lib\nette\CustomNetteSelection | null | array $objects = null,
        array $datatableArgs = []
    ) {
        parent::datatableAction($objects, $datatableArgs);
    }

    /**
     * Use to create entitie with just required fields
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url:'/api/skeleton/createInline',
        method:'POST',
        permission:['create:skeleton'],
        encoding:false,
        visible:false
    )]
    public function createInlineAction(
        \core\notifications\NotificationServiceInterface $notifier,
        \core\entities\EntitieFactoryInterface $entitieFactory,
        \core\secure\csrf\CsrfServiceInterface $csrf
    ) {
        parent::createInlineAction($notifier, $entitieFactory, $csrf);
    }

    /**
     * Delete an entitie by key
     *
     * @RouteRequiredParameters($idskeleton:int)
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url:'/api/skeleton/delete/:idskeleton',
        method:'DELETE',
        permission:['delete:skeleton'],
        encoding:false,
        visible:false
    )]
    public function deleteAction(\core\notifications\NotificationServiceInterface $notifier)
    {
        parent::deleteAction($notifier);
    }

    /**
     * Delete multiples entities by keys
     *
     * @RouteRequiredParameters($keys:array)
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url:'/api/skeleton/deleteMultiples',
        method:'POST',
        permission:['delete:skeleton'],
        encoding:false,
        visible:false
    )]
    public function deleteMultipleAction(\core\notifications\NotificationServiceInterface $notifier)
    {
        parent::deleteMultipleAction($notifier);
    }

    /**
     * Download an entitie as pdf
     *
     * @RouteRequiredParameters($idskeleton:int)
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url:'/api/skeleton/pdf/:idskeleton',
        method:'GET',
        permission:['read:skeleton'],
        encoding:false,
        visible:false
    )]
    public function downloadAction(
        \core\html\AssetsServiceInterface $assetsService,
        \core\view\ViewServiceInterface $viewService,
        \core\config\Config $config
    ) {
        parent::downloadAction($assetsService, $viewService, $config);
    }

    /**
     * Update specific field of entitie with datatable editor pluggin
     *
     * @RouteRequiredParameters($idskeleton:int)
     * @RouteRequiredParameters($datas:array)
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url:'/api/skeleton/editor/:idskeleton',
        method:'PATCH',
        permission:['update:skeleton'],
        encoding:false,
        visible:false
    )]
    public function editorAction(
        \core\notifications\NotificationServiceInterface $notifier,
        \core\events\EventManagerInterface $eventManager
    ) {
        parent::editorAction($notifier, $eventManager);
    }

    /**
     * Lock & unlock entitie
     *
     * @RouteRequiredParameters($idskeleton:int)
     * @RouteRequiredParameters($state:string)
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url:'/api/skeleton/state/:idskeleton',
        method:'POST',
        permission:['@public'],
        encoding:false,
        visible:false
    )]
    public function entitieStateAction(\core\cache\CacheServiceInterface $cache)
    {
        parent::entitieStateAction($cache);
    }

    /**
     * Get list for select fields
     *
     * @RouteExtraParameters($datas:array)
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url:'/api/skeleton/select',
        method:'POST',
        permission:['@public'],
        encoding:false,
        visible:false
    )]
    public function formListAction()
    {
        parent::formListAction();
    }

    /**
     * Return Tag List updated
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url:'/api/skeleton/tag',
        method:'POST',
        permission:['@public'],
        encoding:false,
        visible:false
    )]
    public function tagListAction()
    {
        parent::tagListAction();
    }

    /**
     * Get all entities
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url:'/api/skeleton/all',
        method:'GET',
        permission:['read:skeleton'],
        encoding:false,
        visible:false
    )]
    public function getAllAction()
    {
        parent::getAllAction();
    }

    /**
     * Get entitie by key
     *
     * @RouteRequiredParameters($idskeleton:int)
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url:'/api/skeleton/byId/:idskeleton',
        method:'GET',
        permission:['read:skeleton'],
        encoding:false,
        visible:false
    )]
    public function getByIdAction(NotificationServiceInterface $notificationService)
    {
        parent::getByIdAction($notificationService);
    }

    /**
     * Get entities by parameters
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url:'/api/skeleton/byParameters',
        method:'POST',
        permission:['read:skeleton'],
        encoding:false,
        visible:false
    )]
    public function getByParametersAction()
    {
        parent::getByParametersAction();
    }

    /**
     * Get related entitie by key
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url:'/api/skeleton/related/:idskeleton',
        method:'POST',
        permission:['read:skeleton'],
        encoding:false,
        visible:false
    )]
    public function relatedAction()
    {
        parent::relatedAction();
    }


    /**
     * Get initial value for a select
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url:'/api/skeleton/selectById',
        method:'POST',
        permission:['@public'],
        encoding:false,
        visible:false
    )]
    public function htmlListByIdAction()
    {
        parent::htmlListByIdAction();
    }

    /**
     * Return Html List updated
     *
     * @RouteExtraParameters($q:string)
     * @RouteExtraParameters($p:int)
     * @RouteExtraParameters($datas:array)
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url:'/api/skeleton/search',
        method:'POST',
        permission:['@public'],
        encoding:false,
        visible:false
    )]
    public function htmlListAction()
    {
        parent::htmlListAction();
    }


    /**
     * Default method
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url:'/api/skeleton',
        method:'GET',
        permission:['read:skeleton'],
        encoding:false,
        visible:false
    )]
    public function indexAction()
    {
        parent::indexAction();
    }

    /**
     * Update an entitie by key
     *
     * @RouteRequiredParameters($idskeleton:int)
     * @RouteRequiredParameters($datas:array)
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url:'/api/skeleton/update',
        method:'PUT',
        permission:['update:skeleton'],
        encoding:false,
        visible:false
    )]
    public function updateAction(
        \core\notifications\NotificationServiceInterface $notifier,
        \core\html\form\FormFactory $formFactory,
        \core\tasks\AsyncJob $task
    ) {
        parent::updateAction($notifier, $formFactory, $task);
    }
}
