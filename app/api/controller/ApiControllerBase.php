<?php

namespace app\api\controller;

use app\entities\EntitieFactory;
use core\api\AbstractApiController;
use core\cache\CacheServiceInterface;
use core\config\Config;
use core\database\datafilter\SearchFilter;
use core\database\mysql\lib\nette\CustomNetteSelection;
use core\entities\EntitieFactoryInterface;
use core\entities\EntitieInterface;
use core\events\EventManagerInterface;
use core\html\AssetsServiceInterface;
use core\html\form\FormFactory;
use core\html\tables\datatable\Datatable;
use core\html\tables\datatable\formatters\DatatableFormatter;
use core\notifications\NotificationServiceInterface;
use core\notifications\Notify;
use core\routing\RouteAttribute;
use core\secure\csrf\CsrfServiceInterface;
use core\tasks\AsyncJob;
use core\view\ViewServiceInterface;
use Knp\Snappy\Pdf;
use ReflectionClass;

/**
 * Class ApiControllerBase
 *
 * Base api controller with defaults methods
 *
 * @category  Base api controller with defaults methods
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
abstract class ApiControllerBase extends AbstractApiController
{
    public const HTML_FORMAT  = EntitieInterface::HTML_FORMAT;
    public const ARRAY_JS     = EntitieInterface::ARRAY_JS;
    public const ARRAY_FORMAT = EntitieInterface::ARRAY_FORMAT;

    /**
     * Buttons enabled
     *
     * @var array
     */
    protected $buttonsEnabled = [
        'edit'   => true,
        'copy'   => true,
        'delete' => true
    ];

    public function indexAction()
    {
        $this->apiDocumentationAction();
    }

    /**
     * List all available methods
     *
     * @return void
     */
    public function apiDocumentationAction()
    {
        $reflection = new ReflectionClass($this);

        foreach ($reflection->getMethods() as $method) {
            $attributes = $method->getAttributes(RouteAttribute::class);
            foreach ($attributes as $attributeReflected) {
                $re = '/\* ([a-zA-Z].*)/m';
                preg_match_all($re, $method->getDocComment(), $matches, PREG_SET_ORDER, 0);

                /**
                 * @var RouteAttribute $attribute
                 */
                $attribute                                 = $attributeReflected->newInstance();
                $data[$attribute->method][$attribute->url] = [
                    'description' => $matches[0][1] ?? 'Not defined',
                    'endpoint'    => $attribute->url,
                    'method'      => $attribute->method,
                    'permissions' => $attribute->permission,
                    'parameters'  => 'Not defined'
                ];
            }
        }

        $this->response->setData('data', $data ?? false);
    }

    public function errorAction()
    {
        Notify::notify(_('You are not authorized to process this action. Please contact an administrator.'));
        $this->response->setData('error', 'Undefined error');
    }

    /**
     * Copy an entitie by key
     *
     * @return void
     */
    public function copyAction(
        NotificationServiceInterface $notifier,
        AsyncJob $task
    ) {
        parent::copyAction($notifier, $task);
        $this->addReloadedComponent(Datatable::prefix() . $this->getTable());
    }

    /**
     * Use to create entitie with just required fields
     *
     * @param EntitieFactory $entitieFactory
     *
     * @return void
     */
    public function createInlineAction(
        NotificationServiceInterface $notifier,
        EntitieFactoryInterface $entitieFactory,
        CsrfServiceInterface $csrf
    ) {
        parent::createInlineAction($notifier, $entitieFactory, $csrf);
        $this->addReloadedComponent(Datatable::prefix() . $this->getTable());
    }

    /**
     * Create an entitie
     *
     * @param EntitieFactory $entitieFactory
     *
     * @return void
     */
    public function createAction(
        NotificationServiceInterface $notifier,
        EntitieFactoryInterface $entitieFactory,
        FormFactory $formFactory,
        AsyncJob $task
    ) {
        parent::createAction($notifier, $entitieFactory, $formFactory, $task);
        $this->addReloadedComponent(Datatable::prefix() . $this->getTable());
    }

    /**
     * Get all elements formated to by showed in datatable pluggin
     *
     * @return void
     */
    public function datatableAction(
        CustomNetteSelection|null|array $objects = null,
        array $datatableArgs = []
    ) {
        $objects ??= $this->getDbManager()->getByParams($this->request->getContents());

        /** @var DatatableFormatter $formatter */
        $formatter = $this->getDi()->singleton(DatatableFormatter::class);

        $datatableArgs['buttonsEnabled'] = $this->buttonsEnabled;

        foreach ($objects as $object) {
            /** @var EntitieInterface $object */
            $data ??= [];
            if (is_array($object)) {
                $data[] = $object;
            } else {
                $row = $formatter->useContext($datatableArgs)
                    ->format($object);

                if (null !== $row) {
                    $data[] = $row;
                }
            }
        }

        $this->response->setData('result', (isset($data)));
        $this->response->setData('data', $data ?? []);
    }

    /**
     * Delete an entitie by key
     *
     * @return void
     */
    public function deleteAction(NotificationServiceInterface $notifier)
    {
        parent::deleteAction($notifier);
        $this->addReloadedComponent(Datatable::prefix() . $this->getTable());
    }

    /**
     * Delete multiples entities by keys
     *
     * @return void
     */
    public function deleteMultipleAction(NotificationServiceInterface $notifier)
    {
        parent::deleteMultipleAction($notifier);
        $this->addReloadedComponent(Datatable::prefix() . $this->getTable());
    }

    /**
     * Download an entitie as pdf
     *
     * @return void
     */
    public function downloadAction(
        AssetsServiceInterface $assetsService,
        ViewServiceInterface $viewService,
        Config $config
    ) {
        $indexKey = $this->request->getContents($this->getDbManager()->getIndexKey());

        if (null === $indexKey || null === $this->request->getContents('type')) {
            $this->killServer(206);
        }

        $ressource = $this->getDbManager()->getById($indexKey);

        if (!null === $this->request->getContents('html')) {
            $viewService->assign('ressource', $ressource);
            $viewService->assign('date', date("d/m/Y H\hi"));
            $viewService->assign('CSS', $assetsService->getCss());

            header('Content-Type: text/html');

            $html = $viewService->fetch('tpl/pdfTemplates/' . $this->getParameter('type') . '.html');
            echo $html;
            $this->killServer(200);
        }

        $filename = 'Export ' . $this->getTable() . ' - Id ' . $indexKey . ' - ' . date("d-m-Y H\hi") . '.pdf';

        $snappy = new Pdf('/usr/local/bin/wkhtmltopdf');
        $snappy->setOption('enable-forms', true);
        $snappy->setOption('lowquality', null);
        $snappy->setOption('page-size', 'A4');
        $snappy->setOption('page-height', '29.7cm');
        $snappy->setOption('page-width', '21cm');
        $snappy->setOption('disable-smart-shrinking', true);

        header('Content-Type: application/pdf');
        header('Content-Disposition: attachment; filename="' . $filename . '"');

        echo $snappy->getOutput('127.0.0.1' . $config->find('PHP.SERVER_NAME') . '/html-true');
        $this->killServer(200);
    }

    /**
     * Update specific field of entitie with datatable editor pluggin
     *
     * @return void
     */
    public function editorAction(
        NotificationServiceInterface $notifier,
        EventManagerInterface $eventManager
    ) {
        $datas = $this->request->getContents('data');
        if (null === $datas) {
            $this->killServer(206);
        }

        $updated = false;
        $objects = [];

        foreach ($datas as $key => $objectProperties) {
            $object = $this->getDbManager()->getById($key);
            if (is_bool($object)) {
                $this->killServer(204);
            }

            //Lock update if someone is editing
            $lockerUser = $object->action()->locker();
            if (!is_bool($lockerUser)) {
                $notifier->notify(_("Ressource is locked because it's edited by : ") . $lockerUser, $notifier::WARNING);
                $this->killServer(204);
            }

            //Prevent for inexisting columns
            $objectProperties = $this->removeInexistingColumns($objectProperties, $object);

            if (!empty($objectProperties)) {
                $this->request->setContents(array_merge($object->getVarsKeys(), $objectProperties));
                $object = $object->hydrate($objectProperties);
                $eventManager->disable();
                $this->getDbManager()->update($object);
                $eventManager->enable();

                $objects[] = $this->getDbManager()->getById($key);
            }
        }

        if ($updated) {
            $this->response->setCode(201);
            $this->datatableAction($objects);
        }
    }

    /**
     * Lock & unlock entitie
     *
     * @return void
     */
    public function entitieStateAction(CacheServiceInterface $cache)
    {
        $indexKey = $this->request->getContents($this->getDbManager()->getIndexKey());
        $object   = $this->getDbManager()->getById($indexKey);

        if (is_bool($object)) {
            $this->killServer(204);
        }

        $iduser = $cache->get('entitieState.' . $this->getTable() . '.' . $indexKey);
        if ($this->auth->getUser()?->getIduser() === $iduser || $iduser === '') {
            $status = ($this->request->getContents('state') === 'lock') ? $this->auth->getUser()?->getIduser() : false;

            //Save userLocker id in memcached for 120s
            $cache->set('entitieState.' . $this->getTable() . '.' . $indexKey, $status, 120);

            $this->response->setCode(200);
        }
    }

    /**
     * Generate content for selects
     *
     * performance->getCrews-getTasks->getEvents
     *
     * @return void
     */
    public function formListAction()
    {
        $indexKey  = $this->request->getContents('indexKey');
        $ressource = $this->request->getContents('ressource');
        $tree      = $this->request->getContents('tree');
        $fields    = $this->request->getContents('fields');

        if (null === $indexKey || null === $ressource || (is_countable($tree) ? count($tree) : 0) < 1 || (is_countable($fields) ? count($fields) : 0) < 1) {
            $this->killServer(206);
        }

        $mainObject = $this->repositoryFactory->table($ressource)->getById($indexKey);

        if (is_bool($mainObject)) {
            $this->killServer(204);
        }

        foreach ($tree as $method) {
            if (method_exists($mainObject, $method)) {
                $mainObject = $mainObject->{$method}();
            } else {
                $method     = strtolower(str_replace('get', '', $method));
                $mainObject = $mainObject->$method;
            }
        }

        $this->setDataTriggers($this->formatDataList($mainObject), $fields);
    }

    /**
     * Get related entities by key
     *
     * @return void
     */
    public function relatedAction()
    {
        $indexKey   = $this->request->getContents('indexKey');
        $conditions = $this->request->getContents('conditions');

        $objects = (is_array($indexKey)) ? $this->getDbManager()->getByParams($indexKey) : $this->getDbManager()->getById($indexKey);

        if ($conditions != null) {
            $objects->where($conditions);
        }
    }

    /**
     * Return Html List updated
     *
     * @return void
     */
    public function htmlListAction()
    {
        $page = $this->request->getContents('p') ?? 1;
        if (empty($page)) {
            $page = 1;
        }

        $this->getDbManager()->addFilter(SearchFilter::class);
        $datas = $this->getDbManager()->getHtmlList(self::ARRAY_JS, $page);
        $this->response->setData('content', $datas);

        $this->response->setCode(200);

        //$count = (empty($params)) ? count($this->getDbManager()->getList()) : count($this->getDbManager()->getList()->whereOr($params)->group($this->getDbManager()->getTable() . '.' . $this->getDbManager()->getIndexKey()));
        $count = $this->getDbManager()->count();

        $this->response->setData('total_count', $count);
    }

    /**
     * Return Tag List updated
     *
     * @return void
     */
    public function tagListAction()
    {
        $datas = $this->getDbManager()->getHtmlList(self::ARRAY_JS);
        $this->response->setData('content', $datas);
        $this->response->setCode(200);
    }

    /**
     * Get initial value for a select
     *
     * @return void
     */
    public function htmlListByIdAction()
    {
        $indexKey = $this->request->getContents($this->dbManager->getIndexKey());

        if (!is_array($indexKey)) {
            $object = $this->dbManager->getById($indexKey);
            if (is_bool($object)) {
                $this->killServer(204);
            }

            $this->response->setData('content', [$object->getOptionHtml(self::ARRAY_JS) + ['selected' => true]]);

            return;
        }

        $content = [];

        foreach ($indexKey as $id) {
            $object = $this->getDbManager()->getById($id);

            if (!is_bool($object)) {
                $content[] = $object->getOptionHtml(self::ARRAY_JS) + ['selected' => true];
            }
        }

        $this->response->setData('content', $content);
    }

    /**
     * Update an entitie by key
     *
     * @return void
     */
    public function updateAction(
        NotificationServiceInterface $notifier,
        FormFactory $formFactory,
        AsyncJob $task
    ) {
        parent::updateAction($notifier, $formFactory, $task);
        $this->addReloadedComponent(Datatable::prefix() . $this->getTable());
    }

    /**
     * Get component to reload in view
     *
     * @PhpUnitGen\get("reloadedComponents")
     */
    public function getReloadedComponents(): array
    {
        return $this->reloadedComponents;
    }

    /**
     * Set component to reload in view
     *
     * @PhpUnitGen\set("reloadedComponents")
     *
     * @param array $reloadedComponents Component to reload in view
     */
    protected function reloadedComponents(array $reloadedComponents): self
    {
        $this->reloadedComponents = $reloadedComponents;

        return $this;
    }

    /**
     * Add new component to reload in view
     *
     * @param string $reloadedComponents Component to reload in view
     */
    protected function addReloadedComponent(string $defaultComponent): self
    {
        if (!in_array($defaultComponent, $this->reloadedComponents)) {
            $this->reloadedComponents[] = $defaultComponent;
        }

        return $this;
    }



    /**
     * Generate options for selects
     *
     * @return mixed
     */
    protected function formatDataList(EntitieInterface | array $objects)
    {
        $return = [];
        if (!is_object($objects) && !is_array($objects)) {
            return $objects;
        }

        /** @var EntitieInterface $objects */
        if (is_object($objects)) {
            $return['#' . $objects->getPrimary()] = $objects->getOptionHtml(self::ARRAY_JS);
        }

        if (is_array($objects)
        || (is_object($objects) && $objects::class === CustomNetteSelection::class)) {
            /** @var EntitieInterface $object */
            foreach ($objects as $object) {
                $return['#' . $object->getPrimary()] = $object->getOptionHtml(self::ARRAY_JS);
            }

            return $return;
        }

        return $return;
    }


    /**
     * Get the value of buttonsEnabled
     *
     * @PhpUnitGen\get("buttonsEnabled")
     */
    protected function getButtonsEnabled(): array
    {
        return $this->buttonsEnabled;
    }



    /**
     * Set the value of buttonsEnabled
     *
     * @PhpUnitGen\set("buttonsEnabled")
     *
     * @param mixed[] $buttonsEnabled
     */
    protected function setButtonsEnabled(array $buttonsEnabled): self
    {
        $this->buttonsEnabled = $buttonsEnabled;

        return $this;
    }

    /**
     * Generate triggers for select
     *
     * @param mixed $objects
     *
     * @return void
     */
    protected function setDataTriggers($objects, array $fields)
    {
        foreach ($fields as $field) {
            //TODO implements
            //$trigger = new JsTrigger($field['id'], $field['form'], $field['type']);
            //$trigger->datas($objects);
        }
    }

    /**
     * Generate triggers populate fields
     *
     * @param mixed $value
     *
     * @return void
     */
    protected function setValueTriggers($value, array $fields, ?string $dateType = null)
    {
        foreach ($fields as $field) {
            //TODO Implements
            //$trigger = new JsTrigger($field['id'], $field['form'], $field['type']);
            //$trigger->value($value);
            //if (null !== $dateType) {
            //    $trigger->dateType($dateType);
            //}
        }
    }

    /**
     * Update hasMany table for editor
     *
     * @param  string $column
     * @param  mixed  $value
     * @return void
     */
    protected function updateHasManyObject(EntitieInterface $object, $column, $value)
    {
        $column = (str_starts_with($column, 'id')) ? substr($column, 2) : $column;

        if (in_array(ucfirst($column), $object::getLinkTables()) && $object->findRelated("id$column") !== $value) {
            $object->updateRelations("id$column", $value);

            return true;
        }

        return false;
    }

    /**
     * Prevent for inexisting columns
     */
    private function removeInexistingColumns(array $contents, EntitieInterface $object): array
    {
        foreach ($contents as $column => $value) {
            if (null === $value || '' === $value) {
                unset($contents[$column]);

                continue;
            }

            if (!in_array($column, $this->getDbManager()->getKeys())) {
                unset($contents[$column]);
                if (in_array($this->getDbManager()->realField($column), $this->getDbManager()->getKeys())) {
                    $contents[$this->getDbManager()->realField($column)] = $value;

                    continue;
                }

                if ($this->updateHasManyObject($object, $this->getDbManager()->realField($column), $value)) {
                    $updated = true;

                    continue;
                }

                continue;
            }

            if ($this->updateHasManyObject($object, $column, $value)) {
                $updated = true;
            }
        }

        return $contents;
    }
}
