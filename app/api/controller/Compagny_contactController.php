<?php

namespace app\api\controller;

/**
 * Class Compagny_contactController
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class Compagny_contactController extends ApiControllerBase
{
    /**
     * Undocumented function
     */
    public function __construct(
        public \core\secure\authentification\AuthServiceInterface $auth,
        public \core\messages\request\HttpRequestInterface $request,
        public \core\messages\response\HttpResponseInterface $response,
        public \core\database\RepositoryFactoryInterface $repositoryFactory,
    ) {
        parent::__construct($auth, $request, $response, $repositoryFactory);
        $this->dbManager = $repositoryFactory->table('compagny_contact');
        $this->setTable('compagny_contact');
    }

    /**
     * Copy an entitie by key
     *
     * @RouteRequiredParameters($idcompagny_contact:int)
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/compagny_contact/copy/:idcompagny_contact',
        method: 'POST',
        permission: ['create:compagny_contact'],
        encoding: \false,
        visible: \false
    )]
    public function copyAction(\core\notifications\NotificationServiceInterface $notifier, \core\tasks\AsyncJob $task)
    {
        parent::copyAction($notifier, $task);
    }

    /**
     * Create an entitie
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/compagny_contact/create',
        method: 'POST',
        permission: ['create:compagny_contact'],
        encoding: \false,
        visible: \false
    )]
    public function createAction(
        \core\notifications\NotificationServiceInterface $notifier,
        \core\entities\EntitieFactoryInterface $entitieFactory,
        \core\html\form\FormFactory $formFactory,
        \core\tasks\AsyncJob $task
    ) {
        parent::createAction($notifier, $entitieFactory, $formFactory, $task);
    }

    /**
     * Get all elements formated to by showed in datatable pluggin
     *
     * @param null|array $objects
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/compagny_contact/datatable',
        method: 'POST',
        permission: ['read:compagny_contact'],
        encoding: \false,
        visible: \false
    )]
    public function datatableAction(
        \core\database\mysql\lib\nette\CustomNetteSelection|array|null $objects = \null,
        array $datatableArgs = []
    ) {
        parent::datatableAction($objects, $datatableArgs);
    }

    /**
     * Use to create entitie with just required fields
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/compagny_contact/createInline',
        method: 'POST',
        permission: ['create:compagny_contact'],
        encoding: \false,
        visible: \false
    )]
    public function createInlineAction(
        \core\notifications\NotificationServiceInterface $notifier,
        \core\entities\EntitieFactoryInterface $entitieFactory,
        \core\secure\csrf\CsrfServiceInterface $csrf
    ) {
        parent::createInlineAction($notifier, $entitieFactory, $csrf);
    }

    /**
     * Delete an entitie by key
     *
     * @RouteRequiredParameters($idcompagny_contact:int)
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/compagny_contact/delete/:idcompagny_contact',
        method: 'DELETE',
        permission: ['delete:compagny_contact'],
        encoding: \false,
        visible: \false
    )]
    public function deleteAction(\core\notifications\NotificationServiceInterface $notifier)
    {
        parent::deleteAction($notifier);
    }

    /**
     * Delete multiples entities by keys
     *
     * @RouteRequiredParameters($keys:array)
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/compagny_contact/deleteMultiples',
        method: 'POST',
        permission: ['delete:compagny_contact'],
        encoding: \false,
        visible: \false
    )]
    public function deleteMultipleAction(\core\notifications\NotificationServiceInterface $notifier)
    {
        parent::deleteMultipleAction($notifier);
    }

    /**
     * Download an entitie as pdf
     *
     * @RouteRequiredParameters($idcompagny_contact:int)
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/compagny_contact/pdf/:idcompagny_contact',
        method: 'GET',
        permission: ['read:compagny_contact'],
        encoding: \false,
        visible: \false
    )]
    public function downloadAction(
        \core\html\AssetsServiceInterface $assetsService,
        \core\view\ViewServiceInterface $viewService,
        \core\config\Config $config
    ) {
        parent::downloadAction($assetsService, $viewService, $config);
    }

    /**
     * Update specific field of entitie with datatable editor pluggin
     *
     * @RouteRequiredParameters($idcompagny_contact:int)
     * @RouteRequiredParameters($datas:array)
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/compagny_contact/editor/:idcompagny_contact',
        method: 'PATCH',
        permission: ['update:compagny_contact'],
        encoding: \false,
        visible: \false
    )]
    public function editorAction(
        \core\notifications\NotificationServiceInterface $notifier,
        \core\events\EventManagerInterface $eventManager
    ) {
        parent::editorAction($notifier, $eventManager);
    }

    /**
     * Lock & unlock entitie
     *
     * @RouteRequiredParameters($idcompagny_contact:int)
     * @RouteRequiredParameters($state:string)
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/compagny_contact/state/:idcompagny_contact',
        method: 'POST',
        permission: ['@public'],
        encoding: \false,
        visible: \false
    )]
    public function entitieStateAction(\core\cache\CacheServiceInterface $cache)
    {
        parent::entitieStateAction($cache);
    }

    /**
     * Get list for select fields
     *
     * @RouteExtraParameters($datas:array)
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/compagny_contact/select',
        method: 'POST',
        permission: ['@public'],
        encoding: \false,
        visible: \false
    )]
    public function formListAction()
    {
        parent::formListAction();
    }

    /**
     * Return Tag List updated
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(url: '/api/compagny_contact/tag', method: 'POST', permission: ['@public'], encoding: \false, visible: \false)]
    public function tagListAction()
    {
        parent::tagListAction();
    }

    /**
     * Get all entities
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/compagny_contact/all',
        method: 'GET',
        permission: ['read:compagny_contact'],
        encoding: \false,
        visible: \false
    )]
    public function getAllAction()
    {
        parent::getAllAction();
    }

    /**
     * Get entitie by key
     *
     * @RouteRequiredParameters($idcompagny_contact:int)
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/compagny_contact/byId/:idcompagny_contact',
        method: 'GET',
        permission: ['read:compagny_contact'],
        encoding: false,
        visible: false
    )]
    public function getByIdAction(\core\notifications\NotificationServiceInterface $notifier)
    {
        parent::getByIdAction($notifier);
    }

    /**
     * Get entities by parameters
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/compagny_contact/byParameters',
        method: 'POST',
        permission: ['read:compagny_contact'],
        encoding: \false,
        visible: \false
    )]
    public function getByParametersAction()
    {
        parent::getByParametersAction();
    }

    /**
     * Get related entitie by key
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/compagny_contact/related/:idcompagny_contact',
        method: 'POST',
        permission: ['read:compagny_contact'],
        encoding: \false,
        visible: \false
    )]
    public function relatedAction()
    {
        parent::relatedAction();
    }

    /**
     * Get initial value for a select
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/compagny_contact/selectById',
        method: 'POST',
        permission: ['@public'],
        encoding: \false,
        visible: \false
    )]
    public function htmlListByIdAction()
    {
        parent::htmlListByIdAction();
    }

    /**
     * Return Html List updated
     *
     * @RouteExtraParameters($q:string)
     * @RouteExtraParameters($p:int)
     * @RouteExtraParameters($datas:array)
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/compagny_contact/search',
        method: 'POST',
        permission: ['@public'],
        encoding: \false,
        visible: \false
    )]
    public function htmlListAction()
    {
        parent::htmlListAction();
    }

    /**
     * Default method
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/compagny_contact',
        method: 'GET',
        permission: ['read:compagny_contact'],
        encoding: \false,
        visible: \false
    )]
    public function indexAction()
    {
        parent::indexAction();
    }

    /**
     * Update an entitie by key
     *
     * @RouteRequiredParameters($idcompagny_contact:int)
     * @RouteRequiredParameters($datas:array)
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url: '/api/compagny_contact/update',
        method: 'PUT',
        permission: ['update:compagny_contact'],
        encoding: \false,
        visible: \false
    )]
    public function updateAction(
        \core\notifications\NotificationServiceInterface $notifier,
        \core\html\form\FormFactory $formFactory,
        \core\tasks\AsyncJob $task
    ) {
        parent::updateAction($notifier, $formFactory, $task);
    }
}
