<?php

namespace app\api\controller;

use core\database\RepositoryFactoryInterface;
use core\messages\request\HttpRequestInterface;
use core\messages\response\HttpResponseInterface;
use core\notifications\Notify;
use core\routing\RouteAttribute;
use core\secure\authentification\AuthServiceInterface;

/**
 * Class DefaultController
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class DefaultController extends ApiControllerBase
{
    /**
     * Constructor
     *
     * @param RepositoryFactoryInterface $repositoryFactoryService
     */
    public function __construct(
        protected AuthServiceInterface $auth,
        protected HttpRequestInterface $request,
        protected HttpResponseInterface $response,
        protected RepositoryFactoryInterface $repositoryFactory,
    ) {
        parent::__construct($auth, $request, $response, $repositoryFactory);
        $this->dbManager = null;
        $this->setTable(null);

        $this->setButtonsEnabled(
            [
                'edit'   => false,
                'copy'   => false,
                'delete' => false
            ]
        );
    }

    /**
     * Error Action
     *
     * @return void
     */
    #[RouteAttribute(url:'/api/error', method:'GET', permission:['@public'], encoding:false, visible:false)]
    public function errorAction(int $errorCode = 404)
    {
        Notify::notify(_('You are not authorized to process this action. Please contact an administrator.'));
        $this->response->setCode($errorCode);
    }

    /**
     * Default method
     *
     * @return void
     */
    #[RouteAttribute(url:'/api/', method:'GET', permission:['@public'], encoding:false, visible:false)]
    public function indexAction()
    {
        $this->apiDocumentationAction();
    }
}
