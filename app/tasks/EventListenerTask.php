<?php

namespace app\tasks;

use core\DI\DiProvider;
use core\tasks\TaskInterface;

/**
 * Class EventListenerTask
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class EventListenerTask implements TaskInterface
{
    use DiProvider;

    public function run(array $context)
    {
        $this->getDi()->call($context['callback']['class'], $context['callback']['method'], ['args' => $context['args']]);
    }
}
