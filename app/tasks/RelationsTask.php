<?php

namespace app\tasks;

use core\database\RepositoryFactoryInterface;
use core\tasks\TaskInterface;

/**
 * Class RelationsTask
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class RelationsTask implements TaskInterface
{
    public function __construct(private RepositoryFactoryInterface $repositoryFacty)
    {
    }

    public function run(array $context)
    {
        extract($context, EXTR_OVERWRITE);

        $users = $this->repositoryFactory->table('user')->getList();

        $offset = 0;
        $limit  = 10000;
        foreach ($users as $user) {
            $user->isRequired(true, ' LIMIT ' . $offset . ',' . $limit);
            $offset = $limit + 1;
            $limit += 1000;
        }
    }
}
