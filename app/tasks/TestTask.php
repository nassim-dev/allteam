<?php

namespace app\tasks;

use core\tasks\TaskInterface;

/**
 * Class TestTask
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class TestTask implements TaskInterface
{
    public function run(array $context)
    {
        extract($context, EXTR_OVERWRITE);
    }
}
