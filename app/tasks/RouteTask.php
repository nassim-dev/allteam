<?php

namespace app\tasks;

use core\config\Conf;
use core\secure\authentification\AuthServiceInterface;
use core\tasks\TaskInterface;
use core\utils\Attachment;
use core\services\storage\ObjectStorageInterface;
use core\services\pdf\PhpWordToPdf;
use core\utils\Utils;
use PhpOffice\PhpWord\Element\Section;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\SimpleType\Jc;

/**
 * Class RouteTask
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class RouteTask implements TaskInterface
{
    public function __construct(private AuthServiceInterface $auth, private ObjectStorageInterface $storage)
    {
    }

    /**
     * Php word Object
     *
     * @var PhpWord
     */
    public $phpword = null;

    public function run(array $context)
    {
        extract($context, EXTR_OVERWRITE);

        $this->_getPhpWord();
        $this->_generateTitle($context);
        $this->_generateSections($context);

        $objWriter = IOFactory::createWriter($this->phpword, 'Word2007');
        $objWriter->save(BASE_DIR . Conf::find('PHP.ROOT_PUBLIC_FILES') . 'context_' . $context['idcontextParent'] . '/route/performance.' . $context['filename'] . '.doc');

        $pdf = new PhpWordToPdf($this->phpword);
        $pdf->render(BASE_DIR . Conf::find('PHP.ROOT_PUBLIC_FILES') . 'context_' . $context['idcontextParent'] . '/route/performance.' . $context['filename'] . '.pdf');
    }

    /**
     * Execute callback function on Section $this->assertObjectHasAttribute($attributeName);
     *
     * @param mixed $element
     */
    private function _executeElement(Section $section, $element): Section
    {
        foreach ($element as $actions) {
            $method = $actions['function'];
            unset($actions['function']);
            call_user_func_array([$section, $method], $actions);
        }

        return $section;
    }

    /**
     * Generate Footer contents
     *
     * @return void
     */
    private function _generateFooter(array $context, Section $section)
    {
        if (isset($context['footer'])) {
            $this->_executeElement($section, $context['footer']);
        }
    }

    /**
     * Generate Ole documents
     */
    private function _generateOleDocs(Section $section, array $ole): Section
    {
        $directory = BASE_DIR . Conf::find('PHP.ROOT_PUBLIC_FILES') . 'context_' . $this->auth->getContext()?->getIdcontext() . '/';

        foreach ($ole as $document) {
            $doc = Utils::decrypt($document);
            $ext = explode('.', $doc);

            switch (end($ext)) {
                case 'doc':
                    $section->addText(Attachment::cleanName($doc), ['size' => 12]);
                    $section->addOLEObject($directory . $doc);

                    break;
                case 'xls':
                    $section->addText(Attachment::cleanName($doc), ['size' => 12]);
                    $section->addOLEObject($directory . $doc);

                    break;
                case 'ppt':
                    $section->addText(Attachment::cleanName($doc), ['size' => 12]);
                    $section->addOLEObject($directory . $doc);

                    break;
                case 'docx':
                    $section->addText(Attachment::cleanName($doc), ['size' => 12]);
                    $section->addOLEObject($directory . $doc);

                    break;
                case 'xlsx':
                    $section->addText(Attachment::cleanName($doc), ['size' => 12]);
                    $section->addOLEObject($directory . $doc);

                    break;
                case 'pptx':
                    $section->addText(Attachment::cleanName($doc), ['size' => 12]);
                    $section->addOLEObject($directory . $doc);

                    break;
                case 'jpg':
                    //TODO: TO implement
                    //$section->addText(Attachment::cleanName($doc), array('size' => 12));
                    //$source = file_get_contents($directory . $doc);
                    //$section->addImage($source);
                    break;
                case 'jpeg':
                    //TODO: TO implement
                    //$section->addText(Attachment::cleanName($doc), array('size' => 12));
                    //$source = file_get_contents($directory . $doc);
                    //$section->addImage($source);
                    break;
                case 'gif':
                    //TODO: TO implement
                    //$section->addText(Attachment::cleanName($doc), array('size' => 12));
                    //$source = file_get_contents($directory . $doc);
                    //$section->addImage($source);
                    break;
                case 'png':
                    //TODO: TO implement
                    //$section->addText(Attachment::cleanName($doc), array('size' => 12));
                    //$source = file_get_contents($directory . $doc);
                    //$section->addImage($source);
                    break;
                case 'odt':

                    //TODO: TO implement
                    //$section->addText(Attachment::cleanName($doc), array('size' => 12));
                    //$section->addImage($directory . $doc);
                    break;
                case 'odp':
                    //TODO: TO implement
                    //$section->addText(Attachment::cleanName($doc), array('size' => 12));
                    //$section->addImage($directory . $doc);
                    break;
                case 'ods':
                    //TODO: TO implement
                    //$section->addText(Attachment::cleanName($doc), array('size' => 12));
                    //$section->addImage($directory . $doc);
                    break;
                case 'pdf':
                    //TODO: TO implement
                    //$section->addText(Attachment::cleanName($doc), array('size' => 12));
                    //$section->addImage($directory . $doc);
                    break;
            }
        }

        return $section;
    }

    /**
     * Generate Sections from context
     *
     * @return void
     */
    private function _generateSections(array $context)
    {
        if (isset($context['sections'])) {
            foreach ($context['sections'] as $element) {
                $options = $element['options'] ?? [];
                $section = $this->phpword->addSection($options);

                if (array_key_exists('datatable', $element)) {
                    $element['datatable']->initDocument($section, $this->phpword);
                    $element['datatable']->initTable()
                        ->generateHeader()
                        ->generateContents();
                }

                if (array_key_exists('ole', $element)) {
                    $this->_generateOleDocs($section, $element['ole']);
                }

                if (array_key_exists('calendar', $element)) {
                    $element['calendar']->initDocument($section, $this->phpword);
                }

                if (array_key_exists('function', $element)) {
                    $action['function'] = $element['function'][0];
                    unset($element['function'][0]);
                    $actions[] = array_merge($action, $element['function']);
                    $this->_executeElement($section, $actions);
                }
            }
        }
    }

    /**
     * Generate Title Section
     *
     * @return void
     */
    private function _generateTitle(array $context)
    {
        $section = $this->phpword->addSection();

        if (isset($context['title'])) {
            $this->_executeElement($section, $context['title']);
        }

        $this->_generateFooter($context, $section);

        if (isset($context['firstSection'])) {
            $this->_executeElement($section, $context['firstSection']);
        }
    }

    /**
     * Get PhpWord Object
     */
    private function _getPhpWord(): PhpWord
    {
        $this->phpword = new PhpWord();
        $this->phpword->addTitleStyle(1, ['size' => 14, 'bold' => true], ['alignment' => Jc::CENTER]);
        $this->phpword->addTitleStyle(2, ['size' => 13, 'bold' => true], ['alignment' => Jc::CENTER]);

        return $this->phpword;
    }
}
