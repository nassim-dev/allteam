<?php

namespace app\tasks;

use core\cache\CacheProvider;
use core\logger\LogServiceInterface;
use core\tasks\TaskInterface;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

/**
 * Class ClearCacheTask
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class ClearCacheTask implements TaskInterface
{
    use CacheProvider;

    public function __construct(private LogServiceInterface $logger)
    {
    }

    public function run(array $context)
    {
        extract($context, EXTR_OVERWRITE);

        opcache_reset();

        $this->getFileCache()->clearAll();
        $this->getCache()->clearAll();

        $this->logger->dump(
            [
                'data'    => 'Preload ' . $context['context'] . ' context...',
                'logfile' => APP_LOGFILE
            ]
        );

        try {
            $process = new Process(
                [
                    '/usr/local/bin/php',
                    '--define',
                    'memory_limit=128M',
                    BASE_DIR . 'functions/preload/preloadContext.php ' . $context['context']
                ]
            );
            $process->setTimeout(null);
            $process->mustRun();
        } catch (ProcessFailedException $e) {
            $this->logger->dump(
                [
                    'data'    => $e->getMessage(),
                    'logfile' => APP_LOGFILE
                ]
            );
        }

        $this->logger->dump(
            [
                'data'    => "$context loaded ! ",
                'logfile' => APP_LOGFILE
            ]
        );
    }
}
