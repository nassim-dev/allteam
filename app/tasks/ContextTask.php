<?php

namespace app\tasks;

use core\database\RepositoryFactoryInterface;
use core\logger\LogServiceInterface;
use core\tasks\TaskInterface;

/**
 * Class ContextTask
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class ContextTask implements TaskInterface
{
    public function __construct(
        private RepositoryFactoryInterface $repositoryFactory,
        private LogServiceInterface $logger
    ) {
    }

    public function run(array $context)
    {
        extract($context, EXTR_OVERWRITE);
        if (!isset($context) && isset($idcontextParent)) {
            $context = $this->repositoryFactory->table('context')->getById($idcontextParent);
        }

        if (isset($context)) {
            $context->initData();
            $this->logger->dump(
                [
                    'data'    => 'InitData Context ' . $idcontextParent,
                    'logfile' => ASYNC_LOGFILE
                ]
            );
        }
    }
}
