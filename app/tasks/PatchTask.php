<?php

namespace app\tasks;

use core\logger\LogServiceInterface;
use core\tasks\TaskInterface;

/**
 * Class PatchTask
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class PatchTask implements TaskInterface
{
    public function __construct(private LogServiceInterface $logger)
    {
    }

    public function run(array $context)
    {
        $patch = null;
        extract($context, EXTR_OVERWRITE);

        try {
            include BASE_DIR . 'updates/' . $patch;
        } catch (\Exception $e) {
            $this->logger->dump(
                [
                    'data'    => $e->getMessage(),
                    'logfile' => ASYNC_LOGFILE
                ]
            );
        }
    }
}
