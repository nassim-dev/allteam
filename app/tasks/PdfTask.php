<?php

namespace app\tasks;

use core\logger\LogServiceInterface;
use core\tasks\TaskInterface;

/**
 * Class PdfTask
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class PdfTask implements TaskInterface
{
    public function __construct(private LogServiceInterface $logger)
    {
    }

    public function run(array $context)
    {
        extract($context, EXTR_OVERWRITE);
        $this->logger->dump(
            [
                'data'    => 'Pdf module to implemented',
                'logfile' => ASYNC_LOGFILE
            ]
        );
    }
}
