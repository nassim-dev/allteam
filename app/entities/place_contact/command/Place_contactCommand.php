<?php namespace app\entities\place_contact\command;

/**
 * Class Place_contactCommand
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class Place_contactCommand implements \core\entities\CommandInterface
{
    /**
     * Execute action
     *
     * @param Place_contact $object
     */
    public function action(&$object, ?array $arguments = []): ?array
    {
        \extract($arguments);

        return \null;
    }
}
