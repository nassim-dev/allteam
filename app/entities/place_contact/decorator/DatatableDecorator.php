<?php namespace app\entities\place_contact\decorator;

/**
 * Class DatatableDecorator
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class DatatableDecorator extends \core\entities\decorator\DatatableDecoratorBase
{
    /**
     * Return datatable
     *
     * @param Place_contact $object
     */
    public function decorate(&$object, ?array $args = null): ?array
    {
        $row = parent::decorate($object, $args);

                $relatedRows = $object->findRelated('place', $object::HTML_FORMAT, $args['selector'] ?? null);
                $rows        = [];
                foreach ($relatedRows as $relatedRow) {
                    $rows[] = $relatedRow->historical();
                }

                $row['idplace']  = implode('', $rows);
                $row['idcontext'] = $object->context?->historical();
                $relatedRows = $object->findRelated('contact', $object::HTML_FORMAT, $args['selector'] ?? null);
                $rows        = [];
                foreach ($relatedRows as $relatedRow) {
                    $rows[] = $relatedRow->historical();
                }

                $row['idcontact']  = implode('', $rows);
                $row['name'] = $object->name;$row['roles'] = $object->roles;
        return $row;
    }
}
