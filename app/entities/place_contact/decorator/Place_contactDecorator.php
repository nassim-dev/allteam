<?php namespace app\entities\place_contact\decorator;

/**
 * Class Place_contactDecorator
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class Place_contactDecorator implements \core\entities\DecoratorInterface
{
    /**
     * Return place_contact
     *
     * @param \core\entities\place_contact\Place_contact $object
     */
    public function decorate(&$object, ?array $args = null): ?array
    {
        return \null;
    }
}
