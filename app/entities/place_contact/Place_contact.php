<?php namespace app\entities\place_contact;

/**
 * Class Place_contact
 *
 *  Representation of mysql row place_contact
 *
 *  @property ?int $idplace_contact
 *  @property ?int $idcontext
 *  @property string $name
 *  @property ?\core\database\types\ArrayStringType $roles
 *  @property bool $flag_delete
 *  @property ?\Nette\Utils\DateTime $created_at
 *  @property ?\Nette\Utils\DateTime $updated_at
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
#[\core\entities\EntitieAttribute(tableName: 'place_contact')]
class Place_contact extends \core\entities\EntitieAbstract
{
    protected ?int $idplace_contact = null;
    protected ?int $idcontext = null;
    protected string $name = '';
    protected ?\core\database\types\ArrayStringType $roles = null;
    protected bool $flag_delete = false;
    protected ?\Nette\Utils\DateTime $created_at = null;
    protected ?\Nette\Utils\DateTime $updated_at = null;
    private array $searchFields = ['name'];

    public function getSearchFields(): array
    {
        return $this->searchFields;
    }

    public function __construct(array $data, ?\core\database\mysql\lib\nette\CustomNetteSelection $selection)
    {
        parent::__construct($data, $selection);
    }

    public function getRequiredField(): string|null|array
    {
        return 'name';
    }

    /**
     * Set value of propertie idplace_contact
     *  @param null|int $value
     *  @return self
     */
    public function setIdplace_contact(?int $value): self
    {
        $this->idplace_contact = $value;

                return $this;
    }

    /**
     * Get value of propertie idplace_contact
     *  @return null|int
     */
    public function getIdplace_contact(): ?int
    {
        return $this->idplace_contact;
    }

    /**
     * Set value of propertie idplace_s
     *  @param null|array $value
     *  @return self
     */
    public function setPlaces(?array $value): self
    {
        $this->updateRelations('idplace', $value);

                return $this;
    }

    /**
     * Get value of propertie idplace_s
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getPlaces(): array|\core\database\mysql\lib\nette\CustomNetteSelection
    {
        return $this->findRelated('idplace');
    }

    /**
     * Set value of propertie idcontext
     *  @param null|int $value
     *  @return self
     */
    public function setIdcontext(?int $value): self
    {
        $this->idcontext = $value;

                return $this;
    }

    /**
     * Get value of propertie idcontext
     *  @return null|int
     */
    public function getIdcontext(): ?int
    {
        return $this->idcontext;
    }

    /**
     * Set value of propertie idcontact_s
     *  @param null|array $value
     *  @return self
     */
    public function setContacts(?array $value): self
    {
        $this->updateRelations('idcontact', $value);

                return $this;
    }

    /**
     * Get value of propertie idcontact_s
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getContacts(): array|\core\database\mysql\lib\nette\CustomNetteSelection
    {
        return $this->findRelated('idcontact');
    }

    /**
     * Set value of propertie name
     *  @param string $value
     *  @return self
     */
    public function setName(string $value): self
    {
        $this->name = $value;

                return $this;
    }

    /**
     * Get value of propertie name
     *  @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set value of propertie roles
     *  @param null|\core\database\types\ArrayStringType $value
     *  @return self
     */
    public function setRoles(?\core\database\types\ArrayStringType $value): self
    {
        $this->roles = $value;

                return $this;
    }

    /**
     * Get value of propertie roles
     *  @return null|\core\database\types\ArrayStringType
     */
    public function getRoles(): ?\core\database\types\ArrayStringType
    {
        return $this->roles;
    }

    /**
     * Set value of propertie flag_delete
     *  @param bool $value
     *  @return self
     */
    public function setFlag_delete(bool $value): self
    {
        $this->flag_delete = $value;

                return $this;
    }

    /**
     * Get value of propertie flag_delete
     *  @return bool
     */
    public function getFlag_delete(): bool
    {
        return $this->flag_delete;
    }

    /**
     * Set value of propertie created_at
     *  @param null|\Nette\Utils\DateTime $value
     *  @return self
     */
    public function setCreated_at(?\Nette\Utils\DateTime $value): self
    {
        $this->created_at = $value;

                return $this;
    }

    /**
     * Get value of propertie created_at
     *  @return null|\Nette\Utils\DateTime
     */
    public function getCreated_at(): ?\Nette\Utils\DateTime
    {
        return $this->created_at;
    }

    /**
     * Set value of propertie updated_at
     *  @param null|\Nette\Utils\DateTime $value
     *  @return self
     */
    public function setUpdated_at(?\Nette\Utils\DateTime $value): self
    {
        $this->updated_at = $value;

                return $this;
    }

    /**
     * Get value of propertie updated_at
     *  @return null|\Nette\Utils\DateTime
     */
    public function getUpdated_at(): ?\Nette\Utils\DateTime
    {
        return $this->updated_at;
    }

    public function getPropertyVisibilities(): array
    {
        return array (
          'idplace_contact' => '@public',
          'idcontext' => '@public',
          'name' => '@public',
          'roles' => '@public',
          'flag_delete' => '@public',
          'created_at' => '@public',
          'updated_at' => '@public',
        );
    }
}
