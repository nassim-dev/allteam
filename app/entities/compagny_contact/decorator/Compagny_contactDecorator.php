<?php namespace app\entities\compagny_contact\decorator;

/**
 * Class Compagny_contactDecorator
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class Compagny_contactDecorator implements \core\entities\DecoratorInterface
{
    /**
     * Return compagny_contact
     *
     * @param \core\entities\compagny_contact\Compagny_contact $object
     */
    public function decorate(&$object, ?array $args = null): ?array
    {
        return \null;
    }
}
