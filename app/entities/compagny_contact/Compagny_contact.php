<?php namespace app\entities\compagny_contact;

/**
 * Class Compagny_contact
 *
 *  Representation of mysql row compagny_contact
 *
 *  @property ?int $idcompagny_contact
 *  @property ?int $idcontext
 *  @property string $name
 *  @property ?\core\database\types\ArrayStringType $roles
 *  @property bool $flag_delete
 *  @property ?\Nette\Utils\DateTime $created_at
 *  @property ?\Nette\Utils\DateTime $updated_at
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
#[\core\entities\EntitieAttribute(tableName: 'compagny_contact')]
class Compagny_contact extends \core\entities\EntitieAbstract
{
    protected ?int $idcompagny_contact = null;
    protected ?int $idcontext = null;
    protected string $name = '';
    protected ?\core\database\types\ArrayStringType $roles = null;
    protected bool $flag_delete = false;
    protected ?\Nette\Utils\DateTime $created_at = null;
    protected ?\Nette\Utils\DateTime $updated_at = null;
    private array $searchFields = ['name'];

    public function getSearchFields(): array
    {
        return $this->searchFields;
    }

    public function __construct(array $data, ?\core\database\mysql\lib\nette\CustomNetteSelection $selection)
    {
        parent::__construct($data, $selection);
    }

    public function getRequiredField(): string|null|array
    {
        return 'name';
    }

    /**
     * Set value of propertie idcompagny_contact
     *  @param null|int $value
     *  @return self
     */
    public function setIdcompagny_contact(?int $value): self
    {
        $this->idcompagny_contact = $value;
                    return $this;
    }

    /**
     * Get value of propertie idcompagny_contact
     *  @return null|int
     */
    public function getIdcompagny_contact(): ?int
    {
        return $this->idcompagny_contact;
    }

    /**
     * Set value of propertie idcompagny_s
     *  @param null|array $value
     *  @return self
     */
    public function setCompagnys(?array $value): self
    {
        $this->updateRelations('idcompagny', $value);
                        return $this;
    }

    /**
     * Get value of propertie idcompagny_s
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getCompagnys(): array|\core\database\mysql\lib\nette\CustomNetteSelection
    {
        return $this->findRelated('idcompagny');
    }

    /**
     * Set value of propertie idcontext
     *  @param null|int $value
     *  @return self
     */
    public function setIdcontext(?int $value): self
    {
        $this->idcontext = $value;
                    return $this;
    }

    /**
     * Get value of propertie idcontext
     *  @return null|int
     */
    public function getIdcontext(): ?int
    {
        return $this->idcontext;
    }

    /**
     * Set value of propertie idcontact_s
     *  @param null|array $value
     *  @return self
     */
    public function setContacts(?array $value): self
    {
        $this->updateRelations('idcontact', $value);
                        return $this;
    }

    /**
     * Get value of propertie idcontact_s
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getContacts(): array|\core\database\mysql\lib\nette\CustomNetteSelection
    {
        return $this->findRelated('idcontact');
    }

    /**
     * Set value of propertie name
     *  @param string $value
     *  @return self
     */
    public function setName(string $value): self
    {
        $this->name = $value;
                    return $this;
    }

    /**
     * Get value of propertie name
     *  @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set value of propertie roles
     *  @param null|\core\database\types\ArrayStringType $value
     *  @return self
     */
    public function setRoles(?\core\database\types\ArrayStringType $value): self
    {
        $this->roles = $value;
                    return $this;
    }

    /**
     * Get value of propertie roles
     *  @return null|\core\database\types\ArrayStringType
     */
    public function getRoles(): ?\core\database\types\ArrayStringType
    {
        return $this->roles;
    }

    /**
     * Set value of propertie flag_delete
     *  @param bool $value
     *  @return self
     */
    public function setFlag_delete(bool $value): self
    {
        $this->flag_delete = $value;
                    return $this;
    }

    /**
     * Get value of propertie flag_delete
     *  @return bool
     */
    public function getFlag_delete(): bool
    {
        return $this->flag_delete;
    }

    /**
     * Set value of propertie created_at
     *  @param null|\Nette\Utils\DateTime $value
     *  @return self
     */
    public function setCreated_at(?\Nette\Utils\DateTime $value): self
    {
        $this->created_at = $value;
                    return $this;
    }

    /**
     * Get value of propertie created_at
     *  @return null|\Nette\Utils\DateTime
     */
    public function getCreated_at(): ?\Nette\Utils\DateTime
    {
        return $this->created_at;
    }

    /**
     * Set value of propertie updated_at
     *  @param null|\Nette\Utils\DateTime $value
     *  @return self
     */
    public function setUpdated_at(?\Nette\Utils\DateTime $value): self
    {
        $this->updated_at = $value;
                    return $this;
    }

    /**
     * Get value of propertie updated_at
     *  @return null|\Nette\Utils\DateTime
     */
    public function getUpdated_at(): ?\Nette\Utils\DateTime
    {
        return $this->updated_at;
    }

    public function getPropertyVisibilities(): array
    {
        return array (
          'idcompagny_contact' => '@public',
          'idcompagny_s' => '@public',
          'idcontext' => '@public',
          'idcontact_s' => '@public',
          'name' => '@public',
          'roles' => '@public',
          'flag_delete' => '@public',
          'created_at' => '@public',
          'updated_at' => '@public',
          'searchFields' => '@public',
        );
    }

    /**
     * Set value of propertie searchFields
     *  @param null|array $value
     *  @return self
     */
    public function setArchFieldss(?array $value): self
    {
        $this->updateRelations('searchFields', $value);
                        return $this;
    }

    /**
     * Get value of propertie searchFields
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getArchFieldss(): \core\database\mysql\lib\nette\CustomNetteSelection|array
    {
        return $this->findRelated('searchFields');
    }

    /**
     * Set value of propertie idcontact_s
     *  @param null|array $value
     *  @return self
     */
    public function setId(?array $value): self
    {
        $this->updateRelations('idcontact', $value);
                        return $this;
    }

    /**
     * Get value of propertie idcontact_s
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getId(): \core\database\mysql\lib\nette\CustomNetteSelection|array
    {
        return $this->findRelated('idcontact');
    }

    /**
     * Set value of propertie searchFields
     *  @param null|array $value
     *  @return self
     */
    public function setSe(?array $value): self
    {
        $this->updateRelations('searchFields', $value);
                        return $this;
    }

    /**
     * Get value of propertie searchFields
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getSe(): \core\database\mysql\lib\nette\CustomNetteSelection|array
    {
        return $this->findRelated('searchFields');
    }

    /**
     * Set value of propertie idcompagny_s
     *  @param null|array $value
     *  @return self
     */
    public function setIdcompagnys(?array $value): self
    {
        $this->updateRelations('idcompagny', $value);
                        return $this;
    }

    /**
     * Get value of propertie idcompagny_s
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getIdcompagnys(): \core\database\mysql\lib\nette\CustomNetteSelection|array
    {
        return $this->findRelated('idcompagny');
    }

    /**
     * Set value of propertie idcontact_s
     *  @param null|array $value
     *  @return self
     */
    public function setIdcontacts(?array $value): self
    {
        $this->updateRelations('idcontact', $value);
                        return $this;
    }

    /**
     * Get value of propertie idcontact_s
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getIdcontacts(): \core\database\mysql\lib\nette\CustomNetteSelection|array
    {
        return $this->findRelated('idcontact');
    }

    /**
     * Set value of propertie searchFields
     *  @param null|array $value
     *  @return self
     */
    public function setSearchFieldss(?array $value): self
    {
        $this->updateRelations('searchFields', $value);
                        return $this;
    }

    /**
     * Get value of propertie searchFields
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getSearchFieldss(): \core\database\mysql\lib\nette\CustomNetteSelection|array
    {
        return $this->findRelated('searchFields');
    }
}
