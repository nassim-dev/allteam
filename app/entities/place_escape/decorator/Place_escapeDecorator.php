<?php namespace app\entities\place_escape\decorator;

/**
 * Class Place_escapeDecorator
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class Place_escapeDecorator implements \core\entities\DecoratorInterface
{
    /**
     * Return place_escape
     *
     * @param \core\entities\place_escape\Place_escape $object
     */
    public function decorate(&$object, ?array $args = null): ?array
    {
        return \null;
    }
}
