<?php namespace app\entities\place_escape\decorator;

/**
 * Class DatatableDecorator
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class DatatableDecorator extends \core\entities\decorator\DatatableDecoratorBase
{
    /**
     * Return datatable
     *
     * @param Place_escape $object
     */
    public function decorate(&$object, ?array $args = null): ?array
    {
        $row = parent::decorate($object, $args);
        $row['idcontext'] = $object->context?->historical();$row['size'] = $object->size;$row['idplace'] = $object->place?->historical();
        return $row;
    }
}
