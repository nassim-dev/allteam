<?php namespace app\entities\place_escape;

/**
 * Class Place_escape
 *
 *  Representation of mysql row place_escape
 *
 *  @property ?int $idplace_escape
 *  @property ?int $idcontext
 *  @property int $size
 *  @property ?int $idplace
 *  @property bool $flag_delete
 *  @property ?\Nette\Utils\DateTime $created_at
 *  @property ?\Nette\Utils\DateTime $updated_at
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
#[\core\entities\EntitieAttribute(tableName: 'place_escape')]
class Place_escape extends \core\entities\EntitieAbstract
{
    protected ?int $idplace_escape = null;
    protected ?int $idcontext = null;
    protected int $size;
    protected ?int $idplace = null;
    protected bool $flag_delete = false;
    protected ?\Nette\Utils\DateTime $created_at = null;
    protected ?\Nette\Utils\DateTime $updated_at = null;
    private array $searchFields = [];

    public function getSearchFields(): array
    {
        return $this->searchFields;
    }

    public function __construct(array $data, ?\core\database\mysql\lib\nette\CustomNetteSelection $selection)
    {
        parent::__construct($data, $selection);
    }

    public function getRequiredField(): string|null|array
    {
        return 'name';
    }

    /**
     * Set value of propertie idplace_escape
     *  @param null|int $value
     *  @return self
     */
    public function setIdplace_escape(?int $value): self
    {
        $this->idplace_escape = $value;

                return $this;
    }

    /**
     * Get value of propertie idplace_escape
     *  @return null|int
     */
    public function getIdplace_escape(): ?int
    {
        return $this->idplace_escape;
    }

    /**
     * Set value of propertie idcontext
     *  @param null|int $value
     *  @return self
     */
    public function setIdcontext(?int $value): self
    {
        $this->idcontext = $value;

                return $this;
    }

    /**
     * Get value of propertie idcontext
     *  @return null|int
     */
    public function getIdcontext(): ?int
    {
        return $this->idcontext;
    }

    /**
     * Set value of propertie size
     *  @param int $value
     *  @return self
     */
    public function setSize(int $value): self
    {
        $this->size = $value;

                return $this;
    }

    /**
     * Get value of propertie size
     *  @return int
     */
    public function getSize(): int
    {
        return $this->size;
    }

    /**
     * Set value of propertie idplace
     *  @param null|int $value
     *  @return self
     */
    public function setIdplace(?int $value): self
    {
        $this->idplace = $value;

                return $this;
    }

    /**
     * Get value of propertie idplace
     *  @return null|int
     */
    public function getIdplace(): ?int
    {
        return $this->idplace;
    }

    /**
     * Set value of propertie flag_delete
     *  @param bool $value
     *  @return self
     */
    public function setFlag_delete(bool $value): self
    {
        $this->flag_delete = $value;

                return $this;
    }

    /**
     * Get value of propertie flag_delete
     *  @return bool
     */
    public function getFlag_delete(): bool
    {
        return $this->flag_delete;
    }

    /**
     * Set value of propertie created_at
     *  @param null|\Nette\Utils\DateTime $value
     *  @return self
     */
    public function setCreated_at(?\Nette\Utils\DateTime $value): self
    {
        $this->created_at = $value;

                return $this;
    }

    /**
     * Get value of propertie created_at
     *  @return null|\Nette\Utils\DateTime
     */
    public function getCreated_at(): ?\Nette\Utils\DateTime
    {
        return $this->created_at;
    }

    /**
     * Set value of propertie updated_at
     *  @param null|\Nette\Utils\DateTime $value
     *  @return self
     */
    public function setUpdated_at(?\Nette\Utils\DateTime $value): self
    {
        $this->updated_at = $value;

                return $this;
    }

    /**
     * Get value of propertie updated_at
     *  @return null|\Nette\Utils\DateTime
     */
    public function getUpdated_at(): ?\Nette\Utils\DateTime
    {
        return $this->updated_at;
    }

    public function getPropertyVisibilities(): array
    {
        return array (
          'idplace_escape' => '@public',
          'idcontext' => '@public',
          'size' => '@public',
          'idplace' => '@public',
          'flag_delete' => '@public',
          'created_at' => '@public',
          'updated_at' => '@public',
        );
    }
}
