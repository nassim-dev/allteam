<?php namespace app\entities\context\decorator;

/**
 * Class DatatableDecorator
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class DatatableDecorator extends \core\entities\decorator\DatatableDecoratorBase
{
    /**
     * Return datatable
     *
     * @param Context $object
     */
    public function decorate(&$object, ?array $args = null): ?array
    {
        $row = parent::decorate($object, $args);
        $row['iduser'] = $object->user?->historical();$row['name'] = $object->name;$row['status'] = $object->status;
        return $row;
    }
}
