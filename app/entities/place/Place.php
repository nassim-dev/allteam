<?php namespace app\entities\place;

/**
 * Class Place
 *
 *  Representation of mysql row place
 *
 *  @property ?int $idplace
 *  @property ?int $idcontext
 *  @property ?int $idaddress
 *  @property ?int $idplace_type
 *  @property string $name
 *  @property ?\core\database\types\FileType $visual
 *  @property ?string $description
 *  @property ?\core\database\types\ArrayStringType $tags
 *  @property bool $flag_delete
 *  @property ?\Nette\Utils\DateTime $created_at
 *  @property ?\Nette\Utils\DateTime $updated_at
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
#[\core\entities\EntitieAttribute(tableName: 'place')]
class Place extends \core\entities\EntitieAbstract
{
    protected ?int $idplace = null;
    protected ?int $idcontext = null;
    protected ?int $idaddress = null;
    protected ?int $idplace_type = null;
    protected string $name = '';
    protected ?\core\database\types\FileType $visual = null;
    protected ?string $description = null;
    protected ?\core\database\types\ArrayStringType $tags = null;
    protected bool $flag_delete = false;
    protected ?\Nette\Utils\DateTime $created_at = null;
    protected ?\Nette\Utils\DateTime $updated_at = null;
    private array $searchFields = ['name', 'description'];

    public function getSearchFields(): array
    {
        return $this->searchFields;
    }

    public function __construct(array $data, ?\core\database\mysql\lib\nette\CustomNetteSelection $selection)
    {
        parent::__construct($data, $selection);
    }

    public function getRequiredField(): string|null|array
    {
        return 'name';
    }

    /**
     * Set value of propertie idplace
     *  @param null|int $value
     *  @return self
     */
    public function setIdplace(?int $value): self
    {
        $this->idplace = $value;

                return $this;
    }

    /**
     * Get value of propertie idplace
     *  @return null|int
     */
    public function getIdplace(): ?int
    {
        return $this->idplace;
    }

    /**
     * Set value of propertie idcontext
     *  @param null|int $value
     *  @return self
     */
    public function setIdcontext(?int $value): self
    {
        $this->idcontext = $value;

                return $this;
    }

    /**
     * Get value of propertie idcontext
     *  @return null|int
     */
    public function getIdcontext(): ?int
    {
        return $this->idcontext;
    }

    /**
     * Set value of propertie idaddress
     *  @param null|int $value
     *  @return self
     */
    public function setIdaddress(?int $value): self
    {
        $this->idaddress = $value;

                return $this;
    }

    /**
     * Get value of propertie idaddress
     *  @return null|int
     */
    public function getIdaddress(): ?int
    {
        return $this->idaddress;
    }

    /**
     * Set value of propertie idplace_type
     *  @param null|int $value
     *  @return self
     */
    public function setIdplace_type(?int $value): self
    {
        $this->idplace_type = $value;

                return $this;
    }

    /**
     * Get value of propertie idplace_type
     *  @return null|int
     */
    public function getIdplace_type(): ?int
    {
        return $this->idplace_type;
    }

    /**
     * Set value of propertie idroom_s
     *  @param null|array $value
     *  @return self
     */
    public function setRooms(?array $value): self
    {
        $this->updateRelations('idroom', $value);

                return $this;
    }

    /**
     * Get value of propertie idroom_s
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getRooms(): array|\core\database\mysql\lib\nette\CustomNetteSelection
    {
        return $this->findRelated('idroom');
    }

    /**
     * Set value of propertie name
     *  @param string $value
     *  @return self
     */
    public function setName(string $value): self
    {
        $this->name = $value;

                return $this;
    }

    /**
     * Get value of propertie name
     *  @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set value of propertie visual
     *  @param null|\core\database\types\FileType $value
     *  @return self
     */
    public function setVisual(?\core\database\types\FileType $value): self
    {
        $this->visual = $value;

                return $this;
    }

    /**
     * Get value of propertie visual
     *  @return null|\core\database\types\FileType
     */
    public function getVisual(): ?\core\database\types\FileType
    {
        return $this->visual;
    }

    /**
     * Set value of propertie description
     *  @param null|string $value
     *  @return self
     */
    public function setDescription(?string $value): self
    {
        $this->description = $value;

                return $this;
    }

    /**
     * Get value of propertie description
     *  @return null|string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * Set value of propertie idplace_opening_time_s
     *  @param null|array $value
     *  @return self
     */
    public function setPlace_opening_times(?array $value): self
    {
        $this->updateRelations('idplace_opening_time', $value);

                return $this;
    }

    /**
     * Get value of propertie idplace_opening_time_s
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getPlace_opening_times(): array|\core\database\mysql\lib\nette\CustomNetteSelection
    {
        return $this->findRelated('idplace_opening_time');
    }

    /**
     * Set value of propertie idplace_escape_s
     *  @param null|array $value
     *  @return self
     */
    public function setPlace_escapes(?array $value): self
    {
        $this->updateRelations('idplace_escape', $value);

                return $this;
    }

    /**
     * Get value of propertie idplace_escape_s
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getPlace_escapes(): array|\core\database\mysql\lib\nette\CustomNetteSelection
    {
        return $this->findRelated('idplace_escape');
    }

    /**
     * Set value of propertie idplace_contact_s
     *  @param null|array $value
     *  @return self
     */
    public function setPlace_contacts(?array $value): self
    {
        $this->updateRelations('idplace_contact', $value);

                return $this;
    }

    /**
     * Get value of propertie idplace_contact_s
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getPlace_contacts(): array|\core\database\mysql\lib\nette\CustomNetteSelection
    {
        return $this->findRelated('idplace_contact');
    }

    /**
     * Set value of propertie tags
     *  @param null|\core\database\types\ArrayStringType $value
     *  @return self
     */
    public function setTags(?\core\database\types\ArrayStringType $value): self
    {
        $this->tags = $value;

                return $this;
    }

    /**
     * Get value of propertie tags
     *  @return null|\core\database\types\ArrayStringType
     */
    public function getTags(): ?\core\database\types\ArrayStringType
    {
        return $this->tags;
    }

    /**
     * Set value of propertie flag_delete
     *  @param bool $value
     *  @return self
     */
    public function setFlag_delete(bool $value): self
    {
        $this->flag_delete = $value;

                return $this;
    }

    /**
     * Get value of propertie flag_delete
     *  @return bool
     */
    public function getFlag_delete(): bool
    {
        return $this->flag_delete;
    }

    /**
     * Set value of propertie created_at
     *  @param null|\Nette\Utils\DateTime $value
     *  @return self
     */
    public function setCreated_at(?\Nette\Utils\DateTime $value): self
    {
        $this->created_at = $value;

                return $this;
    }

    /**
     * Get value of propertie created_at
     *  @return null|\Nette\Utils\DateTime
     */
    public function getCreated_at(): ?\Nette\Utils\DateTime
    {
        return $this->created_at;
    }

    /**
     * Set value of propertie updated_at
     *  @param null|\Nette\Utils\DateTime $value
     *  @return self
     */
    public function setUpdated_at(?\Nette\Utils\DateTime $value): self
    {
        $this->updated_at = $value;

                return $this;
    }

    /**
     * Get value of propertie updated_at
     *  @return null|\Nette\Utils\DateTime
     */
    public function getUpdated_at(): ?\Nette\Utils\DateTime
    {
        return $this->updated_at;
    }

    public function getPropertyVisibilities(): array
    {
        return array (
          'idplace' => '@public',
          'idcontext' => '@public',
          'idaddress' => '@public',
          'idplace_type' => '@public',
          'name' => '@public',
          'visual' => '@public',
          'description' => '@public',
          'tags' => '@public',
          'flag_delete' => '@public',
          'created_at' => '@public',
          'updated_at' => '@public',
        );
    }
}
