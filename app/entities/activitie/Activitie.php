<?php namespace app\entities\activitie;

/**
 * Class Activitie
 *
 *  Representation of mysql row activitie
 *
 *  @property ?int $idactivitie
 *  @property ?int $idcontext
 *  @property ?\core\database\types\ArrayStringType $room_reference
 *  @property ?\core\database\types\ArrayStringType $place_reference
 *  @property string $name
 *  @property ?int $idactivitie_type
 *  @property ?\core\database\types\FileType $visual
 *  @property ?string $description
 *  @property ?\core\database\types\ArrayStringType $tags
 *  @property bool $flag_delete
 *  @property ?\Nette\Utils\DateTime $created_at
 *  @property ?\Nette\Utils\DateTime $updated_at
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
#[\core\entities\EntitieAttribute(tableName: 'activitie')]
class Activitie extends \core\entities\EntitieAbstract
{
    protected ?int $idactivitie = null;
    protected ?int $idcontext = null;
    protected ?\core\database\types\ArrayStringType $room_reference = null;
    protected ?\core\database\types\ArrayStringType $place_reference = null;
    protected string $name = '';
    protected ?int $idactivitie_type = null;
    protected ?\core\database\types\FileType $visual = null;
    protected ?string $description = null;
    protected ?\core\database\types\ArrayStringType $tags = null;
    protected bool $flag_delete = false;
    protected ?\Nette\Utils\DateTime $created_at = null;
    protected ?\Nette\Utils\DateTime $updated_at = null;
    private array $searchFields = ['name', 'description'];

    public function getSearchFields(): array
    {
        return $this->searchFields;
    }

    public function __construct(array $data, ?\core\database\mysql\lib\nette\CustomNetteSelection $selection)
    {
        parent::__construct($data, $selection);
    }

    public function getRequiredField(): string|null|array
    {
        return 'name';
    }

    /**
     * Set value of propertie idactivitie
     *  @param null|int $value
     *  @return self
     */
    public function setIdactivitie(?int $value): self
    {
        $this->idactivitie = $value;
                    return $this;
    }

    /**
     * Get value of propertie idactivitie
     *  @return null|int
     */
    public function getIdactivitie(): ?int
    {
        return $this->idactivitie;
    }

    /**
     * Set value of propertie idcontext
     *  @param null|int $value
     *  @return self
     */
    public function setIdcontext(?int $value): self
    {
        $this->idcontext = $value;
                    return $this;
    }

    /**
     * Get value of propertie idcontext
     *  @return null|int
     */
    public function getIdcontext(): ?int
    {
        return $this->idcontext;
    }

    /**
     * Set value of propertie room_reference
     *  @param null|\core\database\types\ArrayStringType $value
     *  @return self
     */
    public function setRoom_reference(?\core\database\types\ArrayStringType $value): self
    {
        $this->room_reference = $value;
                    return $this;
    }

    /**
     * Get value of propertie room_reference
     *  @return null|\core\database\types\ArrayStringType
     */
    public function getRoom_reference(): ?\core\database\types\ArrayStringType
    {
        return $this->room_reference;
    }

    /**
     * Set value of propertie place_reference
     *  @param null|\core\database\types\ArrayStringType $value
     *  @return self
     */
    public function setPlace_reference(?\core\database\types\ArrayStringType $value): self
    {
        $this->place_reference = $value;
                    return $this;
    }

    /**
     * Get value of propertie place_reference
     *  @return null|\core\database\types\ArrayStringType
     */
    public function getPlace_reference(): ?\core\database\types\ArrayStringType
    {
        return $this->place_reference;
    }

    /**
     * Set value of propertie name
     *  @param string $value
     *  @return self
     */
    public function setName(string $value): self
    {
        $this->name = $value;
                    return $this;
    }

    /**
     * Get value of propertie name
     *  @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set value of propertie idactivitie_type
     *  @param null|int $value
     *  @return self
     */
    public function setIdactivitie_type(?int $value): self
    {
        $this->idactivitie_type = $value;
                    return $this;
    }

    /**
     * Get value of propertie idactivitie_type
     *  @return null|int
     */
    public function getIdactivitie_type(): ?int
    {
        return $this->idactivitie_type;
    }

    /**
     * Set value of propertie idoperation_s
     *  @param null|array $value
     *  @return self
     */
    public function setOperations(?array $value): self
    {
        $this->updateRelations('idoperation', $value);
                        return $this;
    }

    /**
     * Get value of propertie idoperation_s
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getOperations(): array|\core\database\mysql\lib\nette\CustomNetteSelection
    {
        return $this->findRelated('idoperation');
    }

    /**
     * Set value of propertie idneed_s
     *  @param null|array $value
     *  @return self
     */
    public function setNeeds(?array $value): self
    {
        $this->updateRelations('idneed', $value);
                        return $this;
    }

    /**
     * Get value of propertie idneed_s
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getNeeds(): array|\core\database\mysql\lib\nette\CustomNetteSelection
    {
        return $this->findRelated('idneed');
    }

    /**
     * Set value of propertie visual
     *  @param null|\core\database\types\FileType $value
     *  @return self
     */
    public function setVisual(?\core\database\types\FileType $value): self
    {
        $this->visual = $value;
                    return $this;
    }

    /**
     * Get value of propertie visual
     *  @return null|\core\database\types\FileType
     */
    public function getVisual(): ?\core\database\types\FileType
    {
        return $this->visual;
    }

    /**
     * Set value of propertie description
     *  @param null|string $value
     *  @return self
     */
    public function setDescription(?string $value): self
    {
        $this->description = $value;
                    return $this;
    }

    /**
     * Get value of propertie description
     *  @return null|string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * Set value of propertie tags
     *  @param null|\core\database\types\ArrayStringType $value
     *  @return self
     */
    public function setTags(?\core\database\types\ArrayStringType $value): self
    {
        $this->tags = $value;
                    return $this;
    }

    /**
     * Get value of propertie tags
     *  @return null|\core\database\types\ArrayStringType
     */
    public function getTags(): ?\core\database\types\ArrayStringType
    {
        return $this->tags;
    }

    /**
     * Set value of propertie flag_delete
     *  @param bool $value
     *  @return self
     */
    public function setFlag_delete(bool $value): self
    {
        $this->flag_delete = $value;
                    return $this;
    }

    /**
     * Get value of propertie flag_delete
     *  @return bool
     */
    public function getFlag_delete(): bool
    {
        return $this->flag_delete;
    }

    /**
     * Set value of propertie created_at
     *  @param null|\Nette\Utils\DateTime $value
     *  @return self
     */
    public function setCreated_at(?\Nette\Utils\DateTime $value): self
    {
        $this->created_at = $value;
                    return $this;
    }

    /**
     * Get value of propertie created_at
     *  @return null|\Nette\Utils\DateTime
     */
    public function getCreated_at(): ?\Nette\Utils\DateTime
    {
        return $this->created_at;
    }

    /**
     * Set value of propertie updated_at
     *  @param null|\Nette\Utils\DateTime $value
     *  @return self
     */
    public function setUpdated_at(?\Nette\Utils\DateTime $value): self
    {
        $this->updated_at = $value;
                    return $this;
    }

    /**
     * Get value of propertie updated_at
     *  @return null|\Nette\Utils\DateTime
     */
    public function getUpdated_at(): ?\Nette\Utils\DateTime
    {
        return $this->updated_at;
    }

    public function getPropertyVisibilities(): array
    {
        return array (
          'idactivitie' => '@public',
          'idcontext' => '@public',
          'room_reference' => '@public',
          'place_reference' => '@public',
          'name' => '@public',
          'idactivitie_type' => '@public',
          'idoperation_s' => '@public',
          'idneed_s' => '@public',
          'visual' => '@public',
          'description' => '@public',
          'tags' => '@public',
          'flag_delete' => '@public',
          'created_at' => '@public',
          'updated_at' => '@public',
          'searchFields' => '@public',
        );
    }

    /**
     * Set value of propertie searchFields
     *  @param null|array $value
     *  @return self
     */
    public function setArchFieldss(?array $value): self
    {
        $this->updateRelations('searchFields', $value);
                        return $this;
    }

    /**
     * Get value of propertie searchFields
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getArchFieldss(): \core\database\mysql\lib\nette\CustomNetteSelection|array
    {
        return $this->findRelated('searchFields');
    }

    /**
     * Set value of propertie idneed_s
     *  @param null|array $value
     *  @return self
     */
    public function setId(?array $value): self
    {
        $this->updateRelations('idneed', $value);
                        return $this;
    }

    /**
     * Get value of propertie idneed_s
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getId(): \core\database\mysql\lib\nette\CustomNetteSelection|array
    {
        return $this->findRelated('idneed');
    }

    /**
     * Set value of propertie searchFields
     *  @param null|array $value
     *  @return self
     */
    public function setSe(?array $value): self
    {
        $this->updateRelations('searchFields', $value);
                        return $this;
    }

    /**
     * Get value of propertie searchFields
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getSe(): \core\database\mysql\lib\nette\CustomNetteSelection|array
    {
        return $this->findRelated('searchFields');
    }

    /**
     * Set value of propertie idoperation_s
     *  @param null|array $value
     *  @return self
     */
    public function setIdoperations(?array $value): self
    {
        $this->updateRelations('idoperation', $value);
                        return $this;
    }

    /**
     * Get value of propertie idoperation_s
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getIdoperations(): \core\database\mysql\lib\nette\CustomNetteSelection|array
    {
        return $this->findRelated('idoperation');
    }

    /**
     * Set value of propertie idneed_s
     *  @param null|array $value
     *  @return self
     */
    public function setIdneeds(?array $value): self
    {
        $this->updateRelations('idneed', $value);
                        return $this;
    }

    /**
     * Get value of propertie idneed_s
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getIdneeds(): \core\database\mysql\lib\nette\CustomNetteSelection|array
    {
        return $this->findRelated('idneed');
    }

    /**
     * Set value of propertie searchFields
     *  @param null|array $value
     *  @return self
     */
    public function setSearchFieldss(?array $value): self
    {
        $this->updateRelations('searchFields', $value);
                        return $this;
    }

    /**
     * Get value of propertie searchFields
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getSearchFieldss(): \core\database\mysql\lib\nette\CustomNetteSelection|array
    {
        return $this->findRelated('searchFields');
    }
}
