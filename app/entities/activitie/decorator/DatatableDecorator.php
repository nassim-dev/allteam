<?php namespace app\entities\activitie\decorator;

/**
 * Class DatatableDecorator
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class DatatableDecorator extends \core\entities\decorator\DatatableDecoratorBase
{
    /**
     * Return datatable
     *
     * @param Activitie $object
     */
    public function decorate(&$object, array $args = null): ?array
    {
        $row = parent::decorate($object, $args);
        $row['idcontext'] = $object->context?->historical();$row['room_reference'] = $object->room_reference;$row['place_reference'] = $object->place_reference;$row['name'] = $object->name;$row['idactivitie_type'] = $object->activitie_type?->historical();
                    $relatedRows = $object->findRelated('operation', $object::HTML_FORMAT, $args['selector'] ?? null);
                    $rows        = [];
                    foreach ($relatedRows as $relatedRow) {
                        $rows[] = $relatedRow->historical();
                    }

                    $row['idoperation']  = implode('', $rows);

                    $relatedRows = $object->findRelated('need', $object::HTML_FORMAT, $args['selector'] ?? null);
                    $rows        = [];
                    foreach ($relatedRows as $relatedRow) {
                        $rows[] = $relatedRow->historical();
                    }

                    $row['idneed']  = implode('', $rows);
                    $row['visual'] = $object->visual;$row['description'] = $object->description;$row['tags'] = $object->tags;
        return $row;
    }
}
