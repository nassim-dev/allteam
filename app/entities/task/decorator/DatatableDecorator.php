<?php namespace app\entities\task\decorator;

/**
 * Class DatatableDecorator
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class DatatableDecorator extends \core\entities\decorator\DatatableDecoratorBase
{
    /**
     * Return datatable
     *
     * @param Task $object
     */
    public function decorate(&$object, ?array $args = null): ?array
    {
        $row = parent::decorate($object, $args);
        $row['idcontext'] = $object->context?->historical();$row['name'] = $object->name;$row['idoperation'] = $object->operation?->historical();$row['idevent'] = $object->event?->historical();$row['idplace'] = $object->place?->historical();$row['idroom'] = $object->room?->historical();$row['visual'] = $object->visual;$row['lock'] = $object->lock;$row['status'] = $object->status;$row['description'] = $object->description;$row['schedule'] = $object->schedule;$row['tags'] = $object->tags;
        return $row;
    }
}
