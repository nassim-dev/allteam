<?php

namespace app\entities;

use core\DI\DiProvider;
use core\entities\EntitieAttribute;
use core\entities\EntitieFactoryAbstract;
use core\entities\EntitieFactoryInterface;
use core\entities\EntitieInterface;
use core\utils\ClassFinder;
use ReflectionClass;

/**
 * Class EntitieFactory
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class EntitieFactory extends EntitieFactoryAbstract implements EntitieFactoryInterface
{
    use DiProvider;

    /**
     * Register all entities classes from attributes
     *
     * @return array
     */
    public function registerApplicationEntities(): array
    {
        $finder      = $this->getDi()->singleton(ClassFinder::class);
        $classes     = $finder->findClasses($this->getNamespace(), ClassFinder::RECURSIVE_MODE) ?? [];
        $appEntities = [];

        foreach ($classes as $class) {
            $reflection = new ReflectionClass($class);
            $attributes = $reflection->getAttributes(EntitieAttribute::class);
            if (empty($attributes)) {
                continue;
            }

            foreach ($attributes as $reflectedAttribute) {
                /** @var EntitieAttribute $attribute */
                $attribute                          = $reflectedAttribute->newInstance();
                $appEntities[$attribute->tableName] = $class;
                $this->registerEntitie($attribute->tableName, $class);
            }
        }

        return $appEntities;
    }

    public function get(string $entitie): EntitieInterface
    {
        return $this->create($entitie);
    }

    /**
     * Get namespace
     */
    public function getNameSpace(): string
    {
        return __NAMESPACE__;
    }
}
