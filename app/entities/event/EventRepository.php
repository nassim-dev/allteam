<?php namespace app\entities\event;

/**
 * Class EventRepository
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class EventRepository extends \core\database\mysql\MysqlRepository
{
    /**
     * Undocumented function
     */
    public function __construct(
        public \core\database\mysql\MysqlManagerInterface $database,
        public \core\events\EventManagerInterface $eventManager,
        public string $table = 'event',
    ) {
        parent::__construct($database, $eventManager, $table);
    }

    /**
     * Copy an object with new params
     */
    public function copy(
        \core\entities\EntitieInterface $oldObject,
        array $params = [],
        bool $mode = self::INCLUDE_RELATED,
        array $excludedTable = ['context']
    ): int {
        return parent::copy($oldObject, $params, $mode, $excludedTable);
    }

    /**
     * Get Html for select field
     */
    public function getHtmlList(
        int $type = \core\entities\EntitieInterface::HTML_FORMAT,
        int $page = 1,
        ?string $order = null,
        array $filter = []
    ): array|string {
        return parent::getHtmlList($type, $page, $order, $filter);
    }

    /**
     * Parameters to filters objects, if user owns it, however he hasn't rights
     */
    public function getOwnsFilters(\core\secure\authentification\AuthServiceInterface $auth): ?array
    {
        return \null;
    }
}
