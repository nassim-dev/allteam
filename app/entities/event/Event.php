<?php namespace app\entities\event;

/**
 * Class Event
 *
 *  Representation of mysql row event
 *
 *  @property ?int $idevent
 *  @property ?int $idcontext
 *  @property string $name
 *  @property ?\core\database\types\FileType $visual
 *  @property ?string $description
 *  @property ?\Nette\Utils\DateTime $start
 *  @property ?string $status
 *  @property bool $flag_delete
 *  @property ?\Nette\Utils\DateTime $created_at
 *  @property ?\Nette\Utils\DateTime $updated_at
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
#[\core\entities\EntitieAttribute(tableName: 'event')]
class Event extends \core\entities\EntitieAbstract
{
    protected ?int $idevent = null;
    protected ?int $idcontext = null;
    protected string $name = '';
    protected ?\core\database\types\FileType $visual = null;
    protected ?string $description = null;
    protected ?\Nette\Utils\DateTime $start = null;
    protected ?string $status = null;
    protected bool $flag_delete = false;
    protected ?\Nette\Utils\DateTime $created_at = null;
    protected ?\Nette\Utils\DateTime $updated_at = null;
    private array $searchFields = ['name', 'description'];

    public function getSearchFields(): array
    {
        return $this->searchFields;
    }

    public function __construct(array $data, ?\core\database\mysql\lib\nette\CustomNetteSelection $selection)
    {
        parent::__construct($data, $selection);
    }

    public function getRequiredField(): string|null|array
    {
        return 'name';
    }

    /**
     * Set value of propertie idevent
     *  @param null|int $value
     *  @return self
     */
    public function setIdevent(?int $value): self
    {
        $this->idevent = $value;
                    return $this;
    }

    /**
     * Get value of propertie idevent
     *  @return null|int
     */
    public function getIdevent(): ?int
    {
        return $this->idevent;
    }

    /**
     * Set value of propertie idcontext
     *  @param null|int $value
     *  @return self
     */
    public function setIdcontext(?int $value): self
    {
        $this->idcontext = $value;
                    return $this;
    }

    /**
     * Get value of propertie idcontext
     *  @return null|int
     */
    public function getIdcontext(): ?int
    {
        return $this->idcontext;
    }

    /**
     * Set value of propertie name
     *  @param string $value
     *  @return self
     */
    public function setName(string $value): self
    {
        $this->name = $value;
                    return $this;
    }

    /**
     * Get value of propertie name
     *  @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set value of propertie visual
     *  @param null|\core\database\types\FileType $value
     *  @return self
     */
    public function setVisual(?\core\database\types\FileType $value): self
    {
        $this->visual = $value;
                    return $this;
    }

    /**
     * Get value of propertie visual
     *  @return null|\core\database\types\FileType
     */
    public function getVisual(): ?\core\database\types\FileType
    {
        return $this->visual;
    }

    /**
     * Set value of propertie description
     *  @param null|string $value
     *  @return self
     */
    public function setDescription(?string $value): self
    {
        $this->description = $value;
                    return $this;
    }

    /**
     * Get value of propertie description
     *  @return null|string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * Set value of propertie start
     *  @param null|\Nette\Utils\DateTime $value
     *  @return self
     */
    public function setStart(?\Nette\Utils\DateTime $value): self
    {
        $this->start = $value;
                    return $this;
    }

    /**
     * Get value of propertie start
     *  @return null|\Nette\Utils\DateTime
     */
    public function getStart(): ?\Nette\Utils\DateTime
    {
        return $this->start;
    }

    /**
     * Set value of propertie status
     *  @param null|string $value
     *  @return self
     */
    public function setStatus(?string $value): self
    {
        $this->status = $value;
                    return $this;
    }

    /**
     * Get value of propertie status
     *  @return null|string
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * Set value of propertie flag_delete
     *  @param bool $value
     *  @return self
     */
    public function setFlag_delete(bool $value): self
    {
        $this->flag_delete = $value;
                    return $this;
    }

    /**
     * Get value of propertie flag_delete
     *  @return bool
     */
    public function getFlag_delete(): bool
    {
        return $this->flag_delete;
    }

    /**
     * Set value of propertie created_at
     *  @param null|\Nette\Utils\DateTime $value
     *  @return self
     */
    public function setCreated_at(?\Nette\Utils\DateTime $value): self
    {
        $this->created_at = $value;
                    return $this;
    }

    /**
     * Get value of propertie created_at
     *  @return null|\Nette\Utils\DateTime
     */
    public function getCreated_at(): ?\Nette\Utils\DateTime
    {
        return $this->created_at;
    }

    /**
     * Set value of propertie updated_at
     *  @param null|\Nette\Utils\DateTime $value
     *  @return self
     */
    public function setUpdated_at(?\Nette\Utils\DateTime $value): self
    {
        $this->updated_at = $value;
                    return $this;
    }

    /**
     * Get value of propertie updated_at
     *  @return null|\Nette\Utils\DateTime
     */
    public function getUpdated_at(): ?\Nette\Utils\DateTime
    {
        return $this->updated_at;
    }

    public function getPropertyVisibilities(): array
    {
        return array (
          'idevent' => '@public',
          'idcontext' => '@public',
          'name' => '@public',
          'visual' => '@public',
          'description' => '@public',
          'start' => '@public',
          'status' => '@public',
          'flag_delete' => '@public',
          'created_at' => '@public',
          'updated_at' => '@public',
          'searchFields' => '@public',
        );
    }

    /**
     * Set value of propertie searchFields
     *  @param null|array $value
     *  @return self
     */
    public function setArchFieldss(?array $value): self
    {
        $this->updateRelations('searchFields', $value);
                        return $this;
    }

    /**
     * Get value of propertie searchFields
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getArchFieldss(): \core\database\mysql\lib\nette\CustomNetteSelection|array
    {
        return $this->findRelated('searchFields');
    }

    /**
     * Set value of propertie searchFields
     *  @param null|array $value
     *  @return self
     */
    public function setSe(?array $value): self
    {
        $this->updateRelations('searchFields', $value);
                        return $this;
    }

    /**
     * Get value of propertie searchFields
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getSe(): \core\database\mysql\lib\nette\CustomNetteSelection|array
    {
        return $this->findRelated('searchFields');
    }

    /**
     * Set value of propertie searchFields
     *  @param null|array $value
     *  @return self
     */
    public function setSearchFieldss(?array $value): self
    {
        $this->updateRelations('searchFields', $value);
                        return $this;
    }

    /**
     * Get value of propertie searchFields
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getSearchFieldss(): \core\database\mysql\lib\nette\CustomNetteSelection|array
    {
        return $this->findRelated('searchFields');
    }
}
