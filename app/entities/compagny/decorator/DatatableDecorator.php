<?php namespace app\entities\compagny\decorator;

/**
 * Class DatatableDecorator
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class DatatableDecorator extends \core\entities\decorator\DatatableDecoratorBase
{
    /**
     * Return datatable
     *
     * @param Compagny $object
     */
    public function decorate(&$object, array $args = null): ?array
    {
        $row                    = parent::decorate($object, $args);
        $row['idcontext']       = $object->context?->historical();
        $row['idaddress']       = $object->address?->historical();
        $row['idcompagny_type'] = $object->compagny_type?->historical();
        $row['name']            = $object->name;
        $row['visual']          = $object->visual;
        $row['description']     = $object->description;
        $relatedRows            = $object->findRelated('compagny_opening_time', $object::HTML_FORMAT, $args['selector'] ?? null);
        $rows                   = [];
        foreach ($relatedRows as $relatedRow) {
            $rows[] = $relatedRow->historical();
        }

        $row['idcompagny_opening_time'] = implode('', $rows);

        $relatedRows = $object->findRelated('compagny_contact', $object::HTML_FORMAT, $args['selector'] ?? null);
        $rows        = [];
        foreach ($relatedRows as $relatedRow) {
            $rows[] = $relatedRow->historical();
        }

        $row['idcompagny_contact'] = implode('', $rows);
        $row['tags']               = $object->tags;

        return $row;
    }
}
