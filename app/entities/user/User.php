<?php namespace app\entities\user;

/**
 * Class User
 *
 *  Representation of mysql row user
 *
 *  @property ?int $iduser
 *  @property ?int $idcontext
 *  @property ?string $email
 *  @property ?string $password
 *  @property ?string $status
 *  @property bool $enabled
 *  @property ?string $validation
 *  @property ?string $hash_validation
 *  @property ?string $language
 *  @property bool $flag_delete
 *  @property ?\Nette\Utils\DateTime $created_at
 *  @property ?\Nette\Utils\DateTime $updated_at
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
#[\core\entities\EntitieAttribute(tableName: 'user')]
class User extends \core\entities\EntitieAbstract
{
    protected ?int $iduser = null;
    protected ?int $idcontext = null;
    protected ?string $email = null;
    protected ?string $password = null;
    protected ?string $status = null;
    protected bool $enabled = false;
    protected ?string $validation = null;
    protected ?string $hash_validation = null;
    protected ?string $language = null;
    protected bool $flag_delete = false;
    protected ?\Nette\Utils\DateTime $created_at = null;
    protected ?\Nette\Utils\DateTime $updated_at = null;
    private array $searchFields = ['email'];

    public function getSearchFields(): array
    {
        return $this->searchFields;
    }

    public function __construct(array $data, ?\core\database\mysql\lib\nette\CustomNetteSelection $selection)
    {
        parent::__construct($data, $selection);
    }

    public function getRequiredField(): string|null|array
    {
        return 'name';
    }

    /**
     * Set value of propertie iduser
     *  @param null|int $value
     *  @return self
     */
    public function setIduser(?int $value): self
    {
        $this->iduser = $value;

                return $this;
    }

    /**
     * Get value of propertie iduser
     *  @return null|int
     */
    public function getIduser(): ?int
    {
        return $this->iduser;
    }

    /**
     * Set value of propertie idcontext
     *  @param null|int $value
     *  @return self
     */
    public function setIdcontext(?int $value): self
    {
        $this->idcontext = $value;

                return $this;
    }

    /**
     * Get value of propertie idcontext
     *  @return null|int
     */
    public function getIdcontext(): ?int
    {
        return $this->idcontext;
    }

    /**
     * Set value of propertie email
     *  @param null|string $value
     *  @return self
     */
    public function setEmail(?string $value): self
    {
        $this->email = $value;

                return $this;
    }

    /**
     * Get value of propertie email
     *  @return null|string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * Set value of propertie password
     *  @param null|string $value
     *  @return self
     */
    public function setPassword(?string $value): self
    {
        $this->password = $value;

                return $this;
    }

    /**
     * Get value of propertie password
     *  @return null|string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * Set value of propertie status
     *  @param null|string $value
     *  @return self
     */
    public function setStatus(?string $value): self
    {
        $this->status = $value;

                return $this;
    }

    /**
     * Get value of propertie status
     *  @return null|string
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * Set value of propertie enabled
     *  @param bool $value
     *  @return self
     */
    public function setEnabled(bool $value): self
    {
        $this->enabled = $value;

                return $this;
    }

    /**
     * Get value of propertie enabled
     *  @return bool
     */
    public function getEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * Set value of propertie validation
     *  @param null|string $value
     *  @return self
     */
    public function setValidation(?string $value): self
    {
        $this->validation = $value;

                return $this;
    }

    /**
     * Get value of propertie validation
     *  @return null|string
     */
    public function getValidation(): ?string
    {
        return $this->validation;
    }

    /**
     * Set value of propertie hash_validation
     *  @param null|string $value
     *  @return self
     */
    public function setHash_validation(?string $value): self
    {
        $this->hash_validation = $value;

                return $this;
    }

    /**
     * Get value of propertie hash_validation
     *  @return null|string
     */
    public function getHash_validation(): ?string
    {
        return $this->hash_validation;
    }

    /**
     * Set value of propertie language
     *  @param null|string $value
     *  @return self
     */
    public function setLanguage(?string $value): self
    {
        $this->language = $value;

                return $this;
    }

    /**
     * Get value of propertie language
     *  @return null|string
     */
    public function getLanguage(): ?string
    {
        return $this->language;
    }

    /**
     * Set value of propertie flag_delete
     *  @param bool $value
     *  @return self
     */
    public function setFlag_delete(bool $value): self
    {
        $this->flag_delete = $value;

                return $this;
    }

    /**
     * Get value of propertie flag_delete
     *  @return bool
     */
    public function getFlag_delete(): bool
    {
        return $this->flag_delete;
    }

    /**
     * Set value of propertie created_at
     *  @param null|\Nette\Utils\DateTime $value
     *  @return self
     */
    public function setCreated_at(?\Nette\Utils\DateTime $value): self
    {
        $this->created_at = $value;

                return $this;
    }

    /**
     * Get value of propertie created_at
     *  @return null|\Nette\Utils\DateTime
     */
    public function getCreated_at(): ?\Nette\Utils\DateTime
    {
        return $this->created_at;
    }

    /**
     * Set value of propertie updated_at
     *  @param null|\Nette\Utils\DateTime $value
     *  @return self
     */
    public function setUpdated_at(?\Nette\Utils\DateTime $value): self
    {
        $this->updated_at = $value;

                return $this;
    }

    /**
     * Get value of propertie updated_at
     *  @return null|\Nette\Utils\DateTime
     */
    public function getUpdated_at(): ?\Nette\Utils\DateTime
    {
        return $this->updated_at;
    }

    public function getPropertyVisibilities(): array
    {
        return array (
          'iduser' => '@public',
          'idcontext' => '@public',
          'email' => '@public',
          'password' => '@public',
          'status' => '@public',
          'enabled' => '@public',
          'validation' => '@public',
          'hash_validation' => '@public',
          'language' => '@public',
          'flag_delete' => '@public',
          'created_at' => '@public',
          'updated_at' => '@public',
        );
    }
}
