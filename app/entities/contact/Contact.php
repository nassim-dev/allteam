<?php namespace app\entities\contact;

/**
 * Class Contact
 *
 *  Representation of mysql row contact
 *
 *  @property ?int $idcontact
 *  @property ?int $idcontext
 *  @property ?int $iduser
 *  @property ?int $idsocial
 *  @property ?int $idhiring
 *  @property ?\core\database\types\FileType $photo
 *  @property ?string $civility
 *  @property string $pseudo
 *  @property ?string $name
 *  @property ?string $first_name
 *  @property ?string $phone
 *  @property ?string $email
 *  @property ?\core\database\types\ArrayStringType $tags
 *  @property bool $flag_delete
 *  @property ?\Nette\Utils\DateTime $created_at
 *  @property ?\Nette\Utils\DateTime $updated_at
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
#[\core\entities\EntitieAttribute(tableName: 'contact')]
class Contact extends \core\entities\EntitieAbstract
{
    protected ?int $idcontact = null;
    protected ?int $idcontext = null;
    protected ?int $iduser = null;
    protected ?int $idsocial = null;
    protected ?int $idhiring = null;
    protected ?\core\database\types\FileType $photo = null;
    protected ?string $civility = null;
    protected string $pseudo = '';
    protected ?string $name = null;
    protected ?string $first_name = null;
    protected ?string $phone = null;
    protected ?string $email = null;
    protected ?\core\database\types\ArrayStringType $tags = null;
    protected bool $flag_delete = false;
    protected ?\Nette\Utils\DateTime $created_at = null;
    protected ?\Nette\Utils\DateTime $updated_at = null;
    private array $searchFields = ['pseudo', 'name', 'first_name', 'phone', 'email'];

    public function getSearchFields(): array
    {
        return $this->searchFields;
    }

    public function __construct(array $data, ?\core\database\mysql\lib\nette\CustomNetteSelection $selection)
    {
        parent::__construct($data, $selection);
    }

    public function getRequiredField(): string|null|array
    {
        return 'name';
    }

    /**
     * Set value of propertie idcontact
     *  @param null|int $value
     *  @return self
     */
    public function setIdcontact(?int $value): self
    {
        $this->idcontact = $value;
                    return $this;
    }

    /**
     * Get value of propertie idcontact
     *  @return null|int
     */
    public function getIdcontact(): ?int
    {
        return $this->idcontact;
    }

    /**
     * Set value of propertie idcontext
     *  @param null|int $value
     *  @return self
     */
    public function setIdcontext(?int $value): self
    {
        $this->idcontext = $value;
                    return $this;
    }

    /**
     * Get value of propertie idcontext
     *  @return null|int
     */
    public function getIdcontext(): ?int
    {
        return $this->idcontext;
    }

    /**
     * Set value of propertie idaddress_s
     *  @param null|array $value
     *  @return self
     */
    public function setAddresss(?array $value): self
    {
        $this->updateRelations('idaddress', $value);
                        return $this;
    }

    /**
     * Get value of propertie idaddress_s
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getAddresss(): array|\core\database\mysql\lib\nette\CustomNetteSelection
    {
        return $this->findRelated('idaddress');
    }

    /**
     * Set value of propertie iduser
     *  @param null|int $value
     *  @return self
     */
    public function setIduser(?int $value): self
    {
        $this->iduser = $value;
                    return $this;
    }

    /**
     * Get value of propertie iduser
     *  @return null|int
     */
    public function getIduser(): ?int
    {
        return $this->iduser;
    }

    /**
     * Set value of propertie idsocial
     *  @param null|int $value
     *  @return self
     */
    public function setIdsocial(?int $value): self
    {
        $this->idsocial = $value;
                    return $this;
    }

    /**
     * Get value of propertie idsocial
     *  @return null|int
     */
    public function getIdsocial(): ?int
    {
        return $this->idsocial;
    }

    /**
     * Set value of propertie idhiring
     *  @param null|int $value
     *  @return self
     */
    public function setIdhiring(?int $value): self
    {
        $this->idhiring = $value;
                    return $this;
    }

    /**
     * Get value of propertie idhiring
     *  @return null|int
     */
    public function getIdhiring(): ?int
    {
        return $this->idhiring;
    }

    /**
     * Set value of propertie idknowledge_s
     *  @param null|array $value
     *  @return self
     */
    public function setKnowledges(?array $value): self
    {
        $this->updateRelations('idknowledge', $value);
                        return $this;
    }

    /**
     * Get value of propertie idknowledge_s
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getKnowledges(): array|\core\database\mysql\lib\nette\CustomNetteSelection
    {
        return $this->findRelated('idknowledge');
    }

    /**
     * Set value of propertie photo
     *  @param null|\core\database\types\FileType $value
     *  @return self
     */
    public function setPhoto(?\core\database\types\FileType $value): self
    {
        $this->photo = $value;
                    return $this;
    }

    /**
     * Get value of propertie photo
     *  @return null|\core\database\types\FileType
     */
    public function getPhoto(): ?\core\database\types\FileType
    {
        return $this->photo;
    }

    /**
     * Set value of propertie civility
     *  @param null|string $value
     *  @return self
     */
    public function setCivility(?string $value): self
    {
        $this->civility = $value;
                    return $this;
    }

    /**
     * Get value of propertie civility
     *  @return null|string
     */
    public function getCivility(): ?string
    {
        return $this->civility;
    }

    /**
     * Set value of propertie pseudo
     *  @param string $value
     *  @return self
     */
    public function setPseudo(string $value): self
    {
        $this->pseudo = $value;
                    return $this;
    }

    /**
     * Get value of propertie pseudo
     *  @return string
     */
    public function getPseudo(): string
    {
        return $this->pseudo;
    }

    /**
     * Set value of propertie name
     *  @param null|string $value
     *  @return self
     */
    public function setName(?string $value): self
    {
        $this->name = $value;
                    return $this;
    }

    /**
     * Get value of propertie name
     *  @return null|string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set value of propertie first_name
     *  @param null|string $value
     *  @return self
     */
    public function setFirst_name(?string $value): self
    {
        $this->first_name = $value;
                    return $this;
    }

    /**
     * Get value of propertie first_name
     *  @return null|string
     */
    public function getFirst_name(): ?string
    {
        return $this->first_name;
    }

    /**
     * Set value of propertie phone
     *  @param null|string $value
     *  @return self
     */
    public function setPhone(?string $value): self
    {
        $this->phone = $value;
                    return $this;
    }

    /**
     * Get value of propertie phone
     *  @return null|string
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * Set value of propertie email
     *  @param null|string $value
     *  @return self
     */
    public function setEmail(?string $value): self
    {
        $this->email = $value;
                    return $this;
    }

    /**
     * Get value of propertie email
     *  @return null|string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * Set value of propertie tags
     *  @param null|\core\database\types\ArrayStringType $value
     *  @return self
     */
    public function setTags(?\core\database\types\ArrayStringType $value): self
    {
        $this->tags = $value;
                    return $this;
    }

    /**
     * Get value of propertie tags
     *  @return null|\core\database\types\ArrayStringType
     */
    public function getTags(): ?\core\database\types\ArrayStringType
    {
        return $this->tags;
    }

    /**
     * Set value of propertie idplace_contact_s
     *  @param null|array $value
     *  @return self
     */
    public function setPlace_contacts(?array $value): self
    {
        $this->updateRelations('idplace_contact', $value);
                        return $this;
    }

    /**
     * Get value of propertie idplace_contact_s
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getPlace_contacts(): array|\core\database\mysql\lib\nette\CustomNetteSelection
    {
        return $this->findRelated('idplace_contact');
    }

    /**
     * Set value of propertie idcompagny_contact_s
     *  @param null|array $value
     *  @return self
     */
    public function setCompagny_contacts(?array $value): self
    {
        $this->updateRelations('idcompagny_contact', $value);
                        return $this;
    }

    /**
     * Get value of propertie idcompagny_contact_s
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getCompagny_contacts(): array|\core\database\mysql\lib\nette\CustomNetteSelection
    {
        return $this->findRelated('idcompagny_contact');
    }

    /**
     * Set value of propertie idshow_s
     *  @param null|array $value
     *  @return self
     */
    public function setShows(?array $value): self
    {
        $this->updateRelations('idshow', $value);
                        return $this;
    }

    /**
     * Get value of propertie idshow_s
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getShows(): array|\core\database\mysql\lib\nette\CustomNetteSelection
    {
        return $this->findRelated('idshow');
    }

    /**
     * Set value of propertie idhire_s
     *  @param null|array $value
     *  @return self
     */
    public function setHires(?array $value): self
    {
        $this->updateRelations('idhire', $value);
                        return $this;
    }

    /**
     * Get value of propertie idhire_s
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getHires(): array|\core\database\mysql\lib\nette\CustomNetteSelection
    {
        return $this->findRelated('idhire');
    }

    /**
     * Set value of propertie idmeal_s
     *  @param null|array $value
     *  @return self
     */
    public function setMeals(?array $value): self
    {
        $this->updateRelations('idmeal', $value);
                        return $this;
    }

    /**
     * Get value of propertie idmeal_s
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getMeals(): array|\core\database\mysql\lib\nette\CustomNetteSelection
    {
        return $this->findRelated('idmeal');
    }

    /**
     * Set value of propertie flag_delete
     *  @param bool $value
     *  @return self
     */
    public function setFlag_delete(bool $value): self
    {
        $this->flag_delete = $value;
                    return $this;
    }

    /**
     * Get value of propertie flag_delete
     *  @return bool
     */
    public function getFlag_delete(): bool
    {
        return $this->flag_delete;
    }

    /**
     * Set value of propertie created_at
     *  @param null|\Nette\Utils\DateTime $value
     *  @return self
     */
    public function setCreated_at(?\Nette\Utils\DateTime $value): self
    {
        $this->created_at = $value;
                    return $this;
    }

    /**
     * Get value of propertie created_at
     *  @return null|\Nette\Utils\DateTime
     */
    public function getCreated_at(): ?\Nette\Utils\DateTime
    {
        return $this->created_at;
    }

    /**
     * Set value of propertie updated_at
     *  @param null|\Nette\Utils\DateTime $value
     *  @return self
     */
    public function setUpdated_at(?\Nette\Utils\DateTime $value): self
    {
        $this->updated_at = $value;
                    return $this;
    }

    /**
     * Get value of propertie updated_at
     *  @return null|\Nette\Utils\DateTime
     */
    public function getUpdated_at(): ?\Nette\Utils\DateTime
    {
        return $this->updated_at;
    }

    public function getPropertyVisibilities(): array
    {
        return array (
          'idcontact' => '@public',
          'idcontext' => '@public',
          'idaddress_s' => '@public',
          'iduser' => '@public',
          'idsocial' => '@public',
          'idhiring' => '@public',
          'idknowledge_s' => '@public',
          'photo' => '@public',
          'civility' => '@public',
          'pseudo' => '@public',
          'name' => '@public',
          'first_name' => '@public',
          'phone' => '@public',
          'email' => '@public',
          'tags' => '@public',
          'idplace_contact_s' => '@public',
          'idcompagny_contact_s' => '@public',
          'idshow_s' => '@public',
          'idhire_s' => '@public',
          'idmeal_s' => '@public',
          'flag_delete' => '@public',
          'created_at' => '@public',
          'updated_at' => '@public',
          'searchFields' => '@public',
        );
    }

    /**
     * Set value of propertie searchFields
     *  @param null|array $value
     *  @return self
     */
    public function setArchFieldss(?array $value): self
    {
        $this->updateRelations('searchFields', $value);
                        return $this;
    }

    /**
     * Get value of propertie searchFields
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getArchFieldss(): \core\database\mysql\lib\nette\CustomNetteSelection|array
    {
        return $this->findRelated('searchFields');
    }

    /**
     * Set value of propertie idmeal_s
     *  @param null|array $value
     *  @return self
     */
    public function setId(?array $value): self
    {
        $this->updateRelations('idmeal', $value);
                        return $this;
    }

    /**
     * Get value of propertie idmeal_s
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getId(): \core\database\mysql\lib\nette\CustomNetteSelection|array
    {
        return $this->findRelated('idmeal');
    }

    /**
     * Set value of propertie searchFields
     *  @param null|array $value
     *  @return self
     */
    public function setSe(?array $value): self
    {
        $this->updateRelations('searchFields', $value);
                        return $this;
    }

    /**
     * Get value of propertie searchFields
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getSe(): \core\database\mysql\lib\nette\CustomNetteSelection|array
    {
        return $this->findRelated('searchFields');
    }

    /**
     * Set value of propertie idaddress_s
     *  @param null|array $value
     *  @return self
     */
    public function setIdaddresss(?array $value): self
    {
        $this->updateRelations('idaddress', $value);
                        return $this;
    }

    /**
     * Get value of propertie idaddress_s
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getIdaddresss(): \core\database\mysql\lib\nette\CustomNetteSelection|array
    {
        return $this->findRelated('idaddress');
    }

    /**
     * Set value of propertie idknowledge_s
     *  @param null|array $value
     *  @return self
     */
    public function setIdknowledges(?array $value): self
    {
        $this->updateRelations('idknowledge', $value);
                        return $this;
    }

    /**
     * Get value of propertie idknowledge_s
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getIdknowledges(): \core\database\mysql\lib\nette\CustomNetteSelection|array
    {
        return $this->findRelated('idknowledge');
    }

    /**
     * Set value of propertie idplace_contact_s
     *  @param null|array $value
     *  @return self
     */
    public function setIdplace_contacts(?array $value): self
    {
        $this->updateRelations('idplace_contact', $value);
                        return $this;
    }

    /**
     * Get value of propertie idplace_contact_s
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getIdplace_contacts(): \core\database\mysql\lib\nette\CustomNetteSelection|array
    {
        return $this->findRelated('idplace_contact');
    }

    /**
     * Set value of propertie idcompagny_contact_s
     *  @param null|array $value
     *  @return self
     */
    public function setIdcompagny_contacts(?array $value): self
    {
        $this->updateRelations('idcompagny_contact', $value);
                        return $this;
    }

    /**
     * Get value of propertie idcompagny_contact_s
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getIdcompagny_contacts(): \core\database\mysql\lib\nette\CustomNetteSelection|array
    {
        return $this->findRelated('idcompagny_contact');
    }

    /**
     * Set value of propertie idshow_s
     *  @param null|array $value
     *  @return self
     */
    public function setIdshows(?array $value): self
    {
        $this->updateRelations('idshow', $value);
                        return $this;
    }

    /**
     * Get value of propertie idshow_s
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getIdshows(): \core\database\mysql\lib\nette\CustomNetteSelection|array
    {
        return $this->findRelated('idshow');
    }

    /**
     * Set value of propertie idhire_s
     *  @param null|array $value
     *  @return self
     */
    public function setIdhires(?array $value): self
    {
        $this->updateRelations('idhire', $value);
                        return $this;
    }

    /**
     * Get value of propertie idhire_s
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getIdhires(): \core\database\mysql\lib\nette\CustomNetteSelection|array
    {
        return $this->findRelated('idhire');
    }

    /**
     * Set value of propertie idmeal_s
     *  @param null|array $value
     *  @return self
     */
    public function setIdmeals(?array $value): self
    {
        $this->updateRelations('idmeal', $value);
                        return $this;
    }

    /**
     * Get value of propertie idmeal_s
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getIdmeals(): \core\database\mysql\lib\nette\CustomNetteSelection|array
    {
        return $this->findRelated('idmeal');
    }

    /**
     * Set value of propertie searchFields
     *  @param null|array $value
     *  @return self
     */
    public function setSearchFieldss(?array $value): self
    {
        $this->updateRelations('searchFields', $value);
                        return $this;
    }

    /**
     * Get value of propertie searchFields
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getSearchFieldss(): \core\database\mysql\lib\nette\CustomNetteSelection|array
    {
        return $this->findRelated('searchFields');
    }
}
