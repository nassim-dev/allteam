<?php namespace app\entities\contact\decorator;

/**
 * Class DatatableDecorator
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class DatatableDecorator extends \core\entities\decorator\DatatableDecoratorBase
{
    /**
     * Return datatable
     *
     * @param Contact $object
     */
    public function decorate(&$object, array $args = null): ?array
    {
        $row = parent::decorate($object, $args);
        $row['idcontext'] = $object->context?->historical();
                    $relatedRows = $object->findRelated('address', $object::HTML_FORMAT, $args['selector'] ?? null);
                    $rows        = [];
                    foreach ($relatedRows as $relatedRow) {
                        $rows[] = $relatedRow->historical();
                    }

                    $row['idaddress']  = implode('', $rows);
                    $row['iduser'] = $object->user?->historical();$row['idsocial'] = $object->social?->historical();$row['idhiring'] = $object->hiring?->historical();
                    $relatedRows = $object->findRelated('knowledge', $object::HTML_FORMAT, $args['selector'] ?? null);
                    $rows        = [];
                    foreach ($relatedRows as $relatedRow) {
                        $rows[] = $relatedRow->historical();
                    }

                    $row['idknowledge']  = implode('', $rows);
                    $row['photo'] = $object->photo;$row['civility'] = $object->civility;$row['pseudo'] = $object->pseudo;$row['name'] = $object->name;$row['first_name'] = $object->first_name;$row['phone'] = $object->phone;$row['email'] = $object->email;$row['tags'] = $object->tags;
                    $relatedRows = $object->findRelated('place_contact', $object::HTML_FORMAT, $args['selector'] ?? null);
                    $rows        = [];
                    foreach ($relatedRows as $relatedRow) {
                        $rows[] = $relatedRow->historical();
                    }

                    $row['idplace_contact']  = implode('', $rows);

                    $relatedRows = $object->findRelated('compagny_contact', $object::HTML_FORMAT, $args['selector'] ?? null);
                    $rows        = [];
                    foreach ($relatedRows as $relatedRow) {
                        $rows[] = $relatedRow->historical();
                    }

                    $row['idcompagny_contact']  = implode('', $rows);

                    $relatedRows = $object->findRelated('show', $object::HTML_FORMAT, $args['selector'] ?? null);
                    $rows        = [];
                    foreach ($relatedRows as $relatedRow) {
                        $rows[] = $relatedRow->historical();
                    }

                    $row['idshow']  = implode('', $rows);

                    $relatedRows = $object->findRelated('hire', $object::HTML_FORMAT, $args['selector'] ?? null);
                    $rows        = [];
                    foreach ($relatedRows as $relatedRow) {
                        $rows[] = $relatedRow->historical();
                    }

                    $row['idhire']  = implode('', $rows);

                    $relatedRows = $object->findRelated('meal', $object::HTML_FORMAT, $args['selector'] ?? null);
                    $rows        = [];
                    foreach ($relatedRows as $relatedRow) {
                        $rows[] = $relatedRow->historical();
                    }

                    $row['idmeal']  = implode('', $rows);

        return $row;
    }
}
