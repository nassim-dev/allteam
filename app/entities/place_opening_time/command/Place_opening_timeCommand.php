<?php namespace app\entities\place_opening_time\command;

/**
 * Class Place_opening_timeCommand
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class Place_opening_timeCommand implements \core\entities\CommandInterface
{
    /**
     * Execute action
     *
     * @param Place_opening_time $object
     */
    public function action(&$object, ?array $arguments = []): ?array
    {
        \extract($arguments);

        return \null;
    }
}
