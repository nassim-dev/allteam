<?php namespace app\entities\place_opening_time\decorator;

/**
 * Class Place_opening_timeDecorator
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class Place_opening_timeDecorator implements \core\entities\DecoratorInterface
{
    /**
     * Return place_opening_time
     *
     * @param \core\entities\place_opening_time\Place_opening_time $object
     */
    public function decorate(&$object, ?array $args = null): ?array
    {
        return \null;
    }
}
