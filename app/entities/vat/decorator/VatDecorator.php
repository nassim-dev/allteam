<?php namespace app\entities\vat\decorator;

/**
 * Class VatDecorator
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class VatDecorator implements \core\entities\DecoratorInterface
{
    /**
     * Return vat
     *
     * @param \core\entities\vat\Vat $object
     */
    public function decorate(&$object, ?array $args = null): ?array
    {
        return \null;
    }
}
