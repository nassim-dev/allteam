<?php namespace app\entities\vat;

/**
 * Class Vat
 *
 *  Representation of mysql row vat
 *
 *  @property ?int $idvat
 *  @property ?int $idcontext
 *  @property ?int $idaccount_deducted
 *  @property ?int $idaccount_collected
 *  @property string $name
 *  @property ?int $percentage
 *  @property bool $flag_delete
 *  @property ?\Nette\Utils\DateTime $created_at
 *  @property ?\Nette\Utils\DateTime $updated_at
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
#[\core\entities\EntitieAttribute(tableName: 'vat')]
class Vat extends \core\entities\EntitieAbstract
{
    protected ?int $idvat = null;
    protected ?int $idcontext = null;
    protected ?int $idaccount_deducted = null;
    protected ?int $idaccount_collected = null;
    protected string $name = '';
    protected ?int $percentage = null;
    protected bool $flag_delete = false;
    protected ?\Nette\Utils\DateTime $created_at = null;
    protected ?\Nette\Utils\DateTime $updated_at = null;
    private array $searchFields = ['name'];

    public function getSearchFields(): array
    {
        return $this->searchFields;
    }

    public function __construct(array $data, ?\core\database\mysql\lib\nette\CustomNetteSelection $selection)
    {
        parent::__construct($data, $selection);
    }

    public function getRequiredField(): string|null|array
    {
        return 'name';
    }

    /**
     * Set value of propertie idvat
     *  @param null|int $value
     *  @return self
     */
    public function setIdvat(?int $value): self
    {
        $this->idvat = $value;
                    return $this;
    }

    /**
     * Get value of propertie idvat
     *  @return null|int
     */
    public function getIdvat(): ?int
    {
        return $this->idvat;
    }

    /**
     * Set value of propertie idcontext
     *  @param null|int $value
     *  @return self
     */
    public function setIdcontext(?int $value): self
    {
        $this->idcontext = $value;
                    return $this;
    }

    /**
     * Get value of propertie idcontext
     *  @return null|int
     */
    public function getIdcontext(): ?int
    {
        return $this->idcontext;
    }

    /**
     * Set value of propertie idaccount_deducted
     *  @param null|int $value
     *  @return self
     */
    public function setIdaccount_deducted(?int $value): self
    {
        $this->idaccount_deducted = $value;
                    return $this;
    }

    /**
     * Get value of propertie idaccount_deducted
     *  @return null|int
     */
    public function getIdaccount_deducted(): ?int
    {
        return $this->idaccount_deducted;
    }

    /**
     * Set value of propertie idaccount_collected
     *  @param null|int $value
     *  @return self
     */
    public function setIdaccount_collected(?int $value): self
    {
        $this->idaccount_collected = $value;
                    return $this;
    }

    /**
     * Get value of propertie idaccount_collected
     *  @return null|int
     */
    public function getIdaccount_collected(): ?int
    {
        return $this->idaccount_collected;
    }

    /**
     * Set value of propertie name
     *  @param string $value
     *  @return self
     */
    public function setName(string $value): self
    {
        $this->name = $value;
                    return $this;
    }

    /**
     * Get value of propertie name
     *  @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set value of propertie percentage
     *  @param null|int $value
     *  @return self
     */
    public function setPercentage(?int $value): self
    {
        $this->percentage = $value;
                    return $this;
    }

    /**
     * Get value of propertie percentage
     *  @return null|int
     */
    public function getPercentage(): ?int
    {
        return $this->percentage;
    }

    /**
     * Set value of propertie flag_delete
     *  @param bool $value
     *  @return self
     */
    public function setFlag_delete(bool $value): self
    {
        $this->flag_delete = $value;
                    return $this;
    }

    /**
     * Get value of propertie flag_delete
     *  @return bool
     */
    public function getFlag_delete(): bool
    {
        return $this->flag_delete;
    }

    /**
     * Set value of propertie created_at
     *  @param null|\Nette\Utils\DateTime $value
     *  @return self
     */
    public function setCreated_at(?\Nette\Utils\DateTime $value): self
    {
        $this->created_at = $value;
                    return $this;
    }

    /**
     * Get value of propertie created_at
     *  @return null|\Nette\Utils\DateTime
     */
    public function getCreated_at(): ?\Nette\Utils\DateTime
    {
        return $this->created_at;
    }

    /**
     * Set value of propertie updated_at
     *  @param null|\Nette\Utils\DateTime $value
     *  @return self
     */
    public function setUpdated_at(?\Nette\Utils\DateTime $value): self
    {
        $this->updated_at = $value;
                    return $this;
    }

    /**
     * Get value of propertie updated_at
     *  @return null|\Nette\Utils\DateTime
     */
    public function getUpdated_at(): ?\Nette\Utils\DateTime
    {
        return $this->updated_at;
    }

    public function getPropertyVisibilities(): array
    {
        return array (
          'idvat' => '@public',
          'idcontext' => '@public',
          'idaccount_deducted' => '@public',
          'idaccount_collected' => '@public',
          'name' => '@public',
          'percentage' => '@public',
          'flag_delete' => '@public',
          'created_at' => '@public',
          'updated_at' => '@public',
          'searchFields' => '@public',
        );
    }

    /**
     * Set value of propertie searchFields
     *  @param null|array $value
     *  @return self
     */
    public function setArchFieldss(?array $value): self
    {
        $this->updateRelations('searchFields', $value);
                        return $this;
    }

    /**
     * Get value of propertie searchFields
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getArchFieldss(): \core\database\mysql\lib\nette\CustomNetteSelection|array
    {
        return $this->findRelated('searchFields');
    }

    /**
     * Set value of propertie searchFields
     *  @param null|array $value
     *  @return self
     */
    public function setSe(?array $value): self
    {
        $this->updateRelations('searchFields', $value);
                        return $this;
    }

    /**
     * Get value of propertie searchFields
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getSe(): \core\database\mysql\lib\nette\CustomNetteSelection|array
    {
        return $this->findRelated('searchFields');
    }

    /**
     * Set value of propertie searchFields
     *  @param null|array $value
     *  @return self
     */
    public function setSearchFieldss(?array $value): self
    {
        $this->updateRelations('searchFields', $value);
                        return $this;
    }

    /**
     * Get value of propertie searchFields
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getSearchFieldss(): \core\database\mysql\lib\nette\CustomNetteSelection|array
    {
        return $this->findRelated('searchFields');
    }
}
