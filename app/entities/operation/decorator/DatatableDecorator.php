<?php namespace app\entities\operation\decorator;

/**
 * Class DatatableDecorator
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class DatatableDecorator extends \core\entities\decorator\DatatableDecoratorBase
{
    /**
     * Return datatable
     *
     * @param Operation $object
     */
    public function decorate(&$object, array $args = null): ?array
    {
        $row = parent::decorate($object, $args);

                    $relatedRows = $object->findRelated('activitie', $object::HTML_FORMAT, $args['selector'] ?? null);
                    $rows        = [];
                    foreach ($relatedRows as $relatedRow) {
                        $rows[] = $relatedRow->historical();
                    }

                    $row['idactivitie']  = implode('', $rows);
                    $row['idcontext'] = $object->context?->historical();$row['name'] = $object->name;
                    $relatedRows = $object->findRelated('need', $object::HTML_FORMAT, $args['selector'] ?? null);
                    $rows        = [];
                    foreach ($relatedRows as $relatedRow) {
                        $rows[] = $relatedRow->historical();
                    }

                    $row['idneed']  = implode('', $rows);
                    $row['duration'] = $object->duration;
                    $relatedRows = $object->findRelated('constraint_schema', $object::HTML_FORMAT, $args['selector'] ?? null);
                    $rows        = [];
                    foreach ($relatedRows as $relatedRow) {
                        $rows[] = $relatedRow->historical();
                    }

                    $row['idconstraint_schema']  = implode('', $rows);
                    $row['room_reference'] = $object->room_reference;$row['place_reference'] = $object->place_reference;$row['visual'] = $object->visual;$row['description'] = $object->description;$row['idoperation_parent'] = $object->operation_parent?->historical();
        return $row;
    }
}
