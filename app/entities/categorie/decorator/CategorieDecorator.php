<?php namespace app\entities\categorie\decorator;

/**
 * Class CategorieDecorator
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class CategorieDecorator implements \core\entities\DecoratorInterface
{
    /**
     * Return categorie
     *
     * @param \core\entities\categorie\Categorie $object
     */
    public function decorate(&$object, ?array $args = null): ?array
    {
        return \null;
    }
}
