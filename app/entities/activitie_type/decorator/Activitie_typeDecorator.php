<?php namespace app\entities\activitie_type\decorator;

/**
 * Class Activitie_typeDecorator
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class Activitie_typeDecorator implements \core\entities\DecoratorInterface
{
    /**
     * Return activitie_type
     *
     * @param \core\entities\activitie_type\Activitie_type $object
     */
    public function decorate(&$object, ?array $args = null): ?array
    {
        return \null;
    }
}
