<?php namespace app\entities\room_type\decorator;

/**
 * Class Room_typeDecorator
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class Room_typeDecorator implements \core\entities\DecoratorInterface
{
    /**
     * Return room_type
     *
     * @param \core\entities\room_type\Room_type $object
     */
    public function decorate(&$object, ?array $args = null): ?array
    {
        return \null;
    }
}
