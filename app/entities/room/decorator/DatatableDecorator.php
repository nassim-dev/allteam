<?php namespace app\entities\room\decorator;

/**
 * Class DatatableDecorator
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class DatatableDecorator extends \core\entities\decorator\DatatableDecoratorBase
{
    /**
     * Return datatable
     *
     * @param Room $object
     */
    public function decorate(&$object, ?array $args = null): ?array
    {
        $row = parent::decorate($object, $args);
        $row['idplace'] = $object->place?->historical();$row['idcontext'] = $object->context?->historical();$row['name'] = $object->name;$row['visual'] = $object->visual;$row['description'] = $object->description;$row['tags'] = $object->tags;
                $relatedRows = $object->findRelated('room_escape', $object::HTML_FORMAT, $args['selector'] ?? null);
                $rows        = [];
                foreach ($relatedRows as $relatedRow) {
                    $rows[] = $relatedRow->historical();
                }

                $row['idroom_escape']  = implode('', $rows);
                $row['idroom_type'] = $object->room_type?->historical();
        return $row;
    }
}
