<?php namespace app\entities\place_type;

/**
 * Class Place_type
 *
 *  Representation of mysql row place_type
 *
 *  @property ?int $idplace_type
 *  @property ?int $idcontext
 *  @property string $name
 *  @property ?string $property
 *  @property ?string $related_namespace
 *  @property bool $flag_delete
 *  @property ?\Nette\Utils\DateTime $created_at
 *  @property ?\Nette\Utils\DateTime $updated_at
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
#[\core\entities\EntitieAttribute(tableName: 'place_type')]
class Place_type extends \core\entities\EntitieAbstract
{
    protected ?int $idplace_type = null;
    protected ?int $idcontext = null;
    protected string $name = '';
    protected ?string $property = null;
    protected ?string $related_namespace = null;
    protected bool $flag_delete = false;
    protected ?\Nette\Utils\DateTime $created_at = null;
    protected ?\Nette\Utils\DateTime $updated_at = null;
    private array $searchFields = ['name'];

    public function getSearchFields(): array
    {
        return $this->searchFields;
    }

    public function __construct(array $data, ?\core\database\mysql\lib\nette\CustomNetteSelection $selection)
    {
        parent::__construct($data, $selection);
    }

    public function getRequiredField(): string|null|array
    {
        return 'name';
    }

    /**
     * Set value of propertie idplace_type
     *  @param null|int $value
     *  @return self
     */
    public function setIdplace_type(?int $value): self
    {
        $this->idplace_type = $value;

                return $this;
    }

    /**
     * Get value of propertie idplace_type
     *  @return null|int
     */
    public function getIdplace_type(): ?int
    {
        return $this->idplace_type;
    }

    /**
     * Set value of propertie idcontext
     *  @param null|int $value
     *  @return self
     */
    public function setIdcontext(?int $value): self
    {
        $this->idcontext = $value;

                return $this;
    }

    /**
     * Get value of propertie idcontext
     *  @return null|int
     */
    public function getIdcontext(): ?int
    {
        return $this->idcontext;
    }

    /**
     * Set value of propertie name
     *  @param string $value
     *  @return self
     */
    public function setName(string $value): self
    {
        $this->name = $value;

                return $this;
    }

    /**
     * Get value of propertie name
     *  @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set value of propertie property
     *  @param null|string $value
     *  @return self
     */
    public function setProperty(?string $value): self
    {
        $this->property = $value;

                return $this;
    }

    /**
     * Get value of propertie property
     *  @return null|string
     */
    public function getProperty(): ?string
    {
        return $this->property;
    }

    /**
     * Set value of propertie related_namespace
     *  @param null|string $value
     *  @return self
     */
    public function setRelated_namespace(?string $value): self
    {
        $this->related_namespace = $value;

                return $this;
    }

    /**
     * Get value of propertie related_namespace
     *  @return null|string
     */
    public function getRelated_namespace(): ?string
    {
        return $this->related_namespace;
    }

    /**
     * Set value of propertie flag_delete
     *  @param bool $value
     *  @return self
     */
    public function setFlag_delete(bool $value): self
    {
        $this->flag_delete = $value;

                return $this;
    }

    /**
     * Get value of propertie flag_delete
     *  @return bool
     */
    public function getFlag_delete(): bool
    {
        return $this->flag_delete;
    }

    /**
     * Set value of propertie created_at
     *  @param null|\Nette\Utils\DateTime $value
     *  @return self
     */
    public function setCreated_at(?\Nette\Utils\DateTime $value): self
    {
        $this->created_at = $value;

                return $this;
    }

    /**
     * Get value of propertie created_at
     *  @return null|\Nette\Utils\DateTime
     */
    public function getCreated_at(): ?\Nette\Utils\DateTime
    {
        return $this->created_at;
    }

    /**
     * Set value of propertie updated_at
     *  @param null|\Nette\Utils\DateTime $value
     *  @return self
     */
    public function setUpdated_at(?\Nette\Utils\DateTime $value): self
    {
        $this->updated_at = $value;

                return $this;
    }

    /**
     * Get value of propertie updated_at
     *  @return null|\Nette\Utils\DateTime
     */
    public function getUpdated_at(): ?\Nette\Utils\DateTime
    {
        return $this->updated_at;
    }

    public function getPropertyVisibilities(): array
    {
        return array (
          'idplace_type' => '@public',
          'idcontext' => '@public',
          'name' => '@public',
          'property' => '@public',
          'related_namespace' => '@public',
          'flag_delete' => '@public',
          'created_at' => '@public',
          'updated_at' => '@public',
        );
    }
}
