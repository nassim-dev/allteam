<?php namespace app\entities\place_type\decorator;

/**
 * Class HistoricalDecorator
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class HistoricalDecorator extends \core\entities\decorator\HistoricalDecoratorBase
{
    /**
     * @param Place_type $object
     */
    public function decorate(&$object, ?array $args = null): ?string
    {
        return parent::decorate($object, $args);
    }
}
