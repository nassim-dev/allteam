<?php namespace app\entities\hiring\command;

/**
 * Class HiringCommand
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class HiringCommand implements \core\entities\CommandInterface
{
    /**
     * Execute action
     *
     * @param Hiring $object
     */
    public function action(&$object, ?array $arguments = []): ?array
    {
        \extract($arguments);

        return \null;
    }
}
