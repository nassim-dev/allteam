<?php namespace app\entities\hiring;

/**
 * Class Hiring
 *
 *  Representation of mysql row hiring
 *
 *  @property ?int $idhiring
 *  @property ?int $idcontext
 *  @property ?string $type
 *  @property ?string $iban
 *  @property ?string $num_ss
 *  @property ?string $num_cs
 *  @property ?int $siret
 *  @property ?int $vat
 *  @property bool $flag_delete
 *  @property ?\Nette\Utils\DateTime $created_at
 *  @property ?\Nette\Utils\DateTime $updated_at
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
#[\core\entities\EntitieAttribute(tableName: 'hiring')]
class Hiring extends \core\entities\EntitieAbstract
{
    protected ?int $idhiring = null;
    protected ?int $idcontext = null;
    protected ?string $type = null;
    protected ?string $iban = null;
    protected ?string $num_ss = null;
    protected ?string $num_cs = null;
    protected ?int $siret = null;
    protected ?int $vat = null;
    protected bool $flag_delete = false;
    protected ?\Nette\Utils\DateTime $created_at = null;
    protected ?\Nette\Utils\DateTime $updated_at = null;
    private array $searchFields = ['num_ss', 'siret'];

    public function getSearchFields(): array
    {
        return $this->searchFields;
    }

    public function __construct(array $data, ?\core\database\mysql\lib\nette\CustomNetteSelection $selection)
    {
        parent::__construct($data, $selection);
    }

    public function getRequiredField(): string|null|array
    {
        return 'name';
    }

    /**
     * Set value of propertie idhiring
     *  @param null|int $value
     *  @return self
     */
    public function setIdhiring(?int $value): self
    {
        $this->idhiring = $value;
                    return $this;
    }

    /**
     * Get value of propertie idhiring
     *  @return null|int
     */
    public function getIdhiring(): ?int
    {
        return $this->idhiring;
    }

    /**
     * Set value of propertie idcontext
     *  @param null|int $value
     *  @return self
     */
    public function setIdcontext(?int $value): self
    {
        $this->idcontext = $value;
                    return $this;
    }

    /**
     * Get value of propertie idcontext
     *  @return null|int
     */
    public function getIdcontext(): ?int
    {
        return $this->idcontext;
    }

    /**
     * Set value of propertie type
     *  @param null|string $value
     *  @return self
     */
    public function setType(?string $value): self
    {
        $this->type = $value;
                    return $this;
    }

    /**
     * Get value of propertie type
     *  @return null|string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * Set value of propertie iban
     *  @param null|string $value
     *  @return self
     */
    public function setIban(?string $value): self
    {
        $this->iban = $value;
                    return $this;
    }

    /**
     * Get value of propertie iban
     *  @return null|string
     */
    public function getIban(): ?string
    {
        return $this->iban;
    }

    /**
     * Set value of propertie num_ss
     *  @param null|string $value
     *  @return self
     */
    public function setNum_ss(?string $value): self
    {
        $this->num_ss = $value;
                    return $this;
    }

    /**
     * Get value of propertie num_ss
     *  @return null|string
     */
    public function getNum_ss(): ?string
    {
        return $this->num_ss;
    }

    /**
     * Set value of propertie num_cs
     *  @param null|string $value
     *  @return self
     */
    public function setNum_cs(?string $value): self
    {
        $this->num_cs = $value;
                    return $this;
    }

    /**
     * Get value of propertie num_cs
     *  @return null|string
     */
    public function getNum_cs(): ?string
    {
        return $this->num_cs;
    }

    /**
     * Set value of propertie siret
     *  @param null|int $value
     *  @return self
     */
    public function setSiret(?int $value): self
    {
        $this->siret = $value;
                    return $this;
    }

    /**
     * Get value of propertie siret
     *  @return null|int
     */
    public function getSiret(): ?int
    {
        return $this->siret;
    }

    /**
     * Set value of propertie vat
     *  @param null|int $value
     *  @return self
     */
    public function setVat(?int $value): self
    {
        $this->vat = $value;
                    return $this;
    }

    /**
     * Get value of propertie vat
     *  @return null|int
     */
    public function getVat(): ?int
    {
        return $this->vat;
    }

    /**
     * Set value of propertie flag_delete
     *  @param bool $value
     *  @return self
     */
    public function setFlag_delete(bool $value): self
    {
        $this->flag_delete = $value;
                    return $this;
    }

    /**
     * Get value of propertie flag_delete
     *  @return bool
     */
    public function getFlag_delete(): bool
    {
        return $this->flag_delete;
    }

    /**
     * Set value of propertie created_at
     *  @param null|\Nette\Utils\DateTime $value
     *  @return self
     */
    public function setCreated_at(?\Nette\Utils\DateTime $value): self
    {
        $this->created_at = $value;
                    return $this;
    }

    /**
     * Get value of propertie created_at
     *  @return null|\Nette\Utils\DateTime
     */
    public function getCreated_at(): ?\Nette\Utils\DateTime
    {
        return $this->created_at;
    }

    /**
     * Set value of propertie updated_at
     *  @param null|\Nette\Utils\DateTime $value
     *  @return self
     */
    public function setUpdated_at(?\Nette\Utils\DateTime $value): self
    {
        $this->updated_at = $value;
                    return $this;
    }

    /**
     * Get value of propertie updated_at
     *  @return null|\Nette\Utils\DateTime
     */
    public function getUpdated_at(): ?\Nette\Utils\DateTime
    {
        return $this->updated_at;
    }

    public function getPropertyVisibilities(): array
    {
        return array (
          'idhiring' => '@public',
          'idcontext' => '@public',
          'type' => '@public',
          'iban' => '@public',
          'num_ss' => '@public',
          'num_cs' => '@public',
          'siret' => '@public',
          'vat' => '@public',
          'flag_delete' => '@public',
          'created_at' => '@public',
          'updated_at' => '@public',
          'searchFields' => '@public',
        );
    }

    /**
     * Set value of propertie searchFields
     *  @param null|array $value
     *  @return self
     */
    public function setArchFieldss(?array $value): self
    {
        $this->updateRelations('searchFields', $value);
                        return $this;
    }

    /**
     * Get value of propertie searchFields
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getArchFieldss(): \core\database\mysql\lib\nette\CustomNetteSelection|array
    {
        return $this->findRelated('searchFields');
    }

    /**
     * Set value of propertie searchFields
     *  @param null|array $value
     *  @return self
     */
    public function setSe(?array $value): self
    {
        $this->updateRelations('searchFields', $value);
                        return $this;
    }

    /**
     * Get value of propertie searchFields
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getSe(): \core\database\mysql\lib\nette\CustomNetteSelection|array
    {
        return $this->findRelated('searchFields');
    }

    /**
     * Set value of propertie searchFields
     *  @param null|array $value
     *  @return self
     */
    public function setSearchFieldss(?array $value): self
    {
        $this->updateRelations('searchFields', $value);
                        return $this;
    }

    /**
     * Get value of propertie searchFields
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getSearchFieldss(): \core\database\mysql\lib\nette\CustomNetteSelection|array
    {
        return $this->findRelated('searchFields');
    }
}
