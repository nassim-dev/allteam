<?php namespace app\entities\configuration\decorator;

/**
 * Class ConfigurationDecorator
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class ConfigurationDecorator implements \core\entities\DecoratorInterface
{
    /**
     * Return configuration
     *
     * @param \core\entities\configuration\Configuration $object
     */
    public function decorate(&$object, ?array $args = null): ?array
    {
        return \null;
    }
}
