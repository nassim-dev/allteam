<?php namespace app\entities\log\decorator;

/**
 * Class LogDecorator
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class LogDecorator implements \core\entities\DecoratorInterface
{
    /**
     * Return log
     *
     * @param \core\entities\log\Log $object
     */
    public function decorate(&$object, ?array $args = null): ?array
    {
        return \null;
    }
}
