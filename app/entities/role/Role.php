<?php namespace app\entities\role;

/**
 * Class Role
 *
 *  Representation of mysql row role
 *
 *  @property ?int $idrole
 *  @property ?int $idcontext
 *  @property string $name
 *  @property bool $flag_delete
 *  @property ?\Nette\Utils\DateTime $created_at
 *  @property ?\Nette\Utils\DateTime $updated_at
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
#[\core\entities\EntitieAttribute(tableName: 'role')]
class Role extends \core\entities\EntitieAbstract
{
    protected ?int $idrole = null;
    protected ?int $idcontext = null;
    protected string $name = '';
    protected bool $flag_delete = false;
    protected ?\Nette\Utils\DateTime $created_at = null;
    protected ?\Nette\Utils\DateTime $updated_at = null;
    private array $searchFields = ['name'];

    public function getSearchFields(): array
    {
        return $this->searchFields;
    }

    public function __construct(array $data, ?\core\database\mysql\lib\nette\CustomNetteSelection $selection)
    {
        parent::__construct($data, $selection);
    }

    public function getRequiredField(): string|null|array
    {
        return 'name';
    }

    /**
     * Set value of propertie idrole
     *  @param null|int $value
     *  @return self
     */
    public function setIdrole(?int $value): self
    {
        $this->idrole = $value;

                return $this;
    }

    /**
     * Get value of propertie idrole
     *  @return null|int
     */
    public function getIdrole(): ?int
    {
        return $this->idrole;
    }

    /**
     * Set value of propertie idcontext
     *  @param null|int $value
     *  @return self
     */
    public function setIdcontext(?int $value): self
    {
        $this->idcontext = $value;

                return $this;
    }

    /**
     * Get value of propertie idcontext
     *  @return null|int
     */
    public function getIdcontext(): ?int
    {
        return $this->idcontext;
    }

    /**
     * Set value of propertie name
     *  @param string $value
     *  @return self
     */
    public function setName(string $value): self
    {
        $this->name = $value;

                return $this;
    }

    /**
     * Get value of propertie name
     *  @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set value of propertie idpermission_s
     *  @param null|array $value
     *  @return self
     */
    public function setPermissions(?array $value): self
    {
        $this->updateRelations('idpermission', $value);

                return $this;
    }

    /**
     * Get value of propertie idpermission_s
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getPermissions(): array|\core\database\mysql\lib\nette\CustomNetteSelection
    {
        return $this->findRelated('idpermission');
    }

    /**
     * Set value of propertie iduser_s
     *  @param null|array $value
     *  @return self
     */
    public function setUsers(?array $value): self
    {
        $this->updateRelations('iduser', $value);

                return $this;
    }

    /**
     * Get value of propertie iduser_s
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getUsers(): array|\core\database\mysql\lib\nette\CustomNetteSelection
    {
        return $this->findRelated('iduser');
    }

    /**
     * Set value of propertie flag_delete
     *  @param bool $value
     *  @return self
     */
    public function setFlag_delete(bool $value): self
    {
        $this->flag_delete = $value;

                return $this;
    }

    /**
     * Get value of propertie flag_delete
     *  @return bool
     */
    public function getFlag_delete(): bool
    {
        return $this->flag_delete;
    }

    /**
     * Set value of propertie created_at
     *  @param null|\Nette\Utils\DateTime $value
     *  @return self
     */
    public function setCreated_at(?\Nette\Utils\DateTime $value): self
    {
        $this->created_at = $value;

                return $this;
    }

    /**
     * Get value of propertie created_at
     *  @return null|\Nette\Utils\DateTime
     */
    public function getCreated_at(): ?\Nette\Utils\DateTime
    {
        return $this->created_at;
    }

    /**
     * Set value of propertie updated_at
     *  @param null|\Nette\Utils\DateTime $value
     *  @return self
     */
    public function setUpdated_at(?\Nette\Utils\DateTime $value): self
    {
        $this->updated_at = $value;

                return $this;
    }

    /**
     * Get value of propertie updated_at
     *  @return null|\Nette\Utils\DateTime
     */
    public function getUpdated_at(): ?\Nette\Utils\DateTime
    {
        return $this->updated_at;
    }

    public function getPropertyVisibilities(): array
    {
        return array (
          'idrole' => '@public',
          'idcontext' => '@public',
          'name' => '@public',
          'flag_delete' => '@public',
          'created_at' => '@public',
          'updated_at' => '@public',
        );
    }
}
