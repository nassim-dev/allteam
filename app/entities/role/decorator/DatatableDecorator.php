<?php namespace app\entities\role\decorator;

/**
 * Class DatatableDecorator
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class DatatableDecorator extends \core\entities\decorator\DatatableDecoratorBase
{
    /**
     * Return datatable
     *
     * @param Role $object
     */
    public function decorate(&$object, array $args = null): ?array
    {
        $row = parent::decorate($object, $args);
        $row['idcontext'] = $object->context?->historical();$row['name'] = $object->name;
                $relatedRows = $object->findRelated('permission', $object::HTML_FORMAT, $args['selector'] ?? null);
                $rows        = [];
                foreach ($relatedRows as $relatedRow) {
                    $rows[] = $relatedRow->historical();
                }

                $row['idpermission']  = implode('', $rows);

                $relatedRows = $object->findRelated('user', $object::HTML_FORMAT, $args['selector'] ?? null);
                $rows        = [];
                foreach ($relatedRows as $relatedRow) {
                    $rows[] = $relatedRow->historical();
                }

                $row['iduser']  = implode('', $rows);

        return $row;
    }
}
