<?php namespace app\entities\social;

/**
 * Class Social
 *
 *  Representation of mysql row social
 *
 *  @property ?int $idsocial
 *  @property ?int $idcontext
 *  @property ?\Nette\Utils\DateTime $date_birth
 *  @property ?string $place_birth
 *  @property bool $flag_delete
 *  @property ?\Nette\Utils\DateTime $created_at
 *  @property ?\Nette\Utils\DateTime $updated_at
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
#[\core\entities\EntitieAttribute(tableName: 'social')]
class Social extends \core\entities\EntitieAbstract
{
    protected ?int $idsocial = null;
    protected ?int $idcontext = null;
    protected ?\Nette\Utils\DateTime $date_birth = null;
    protected ?string $place_birth = null;
    protected bool $flag_delete = false;
    protected ?\Nette\Utils\DateTime $created_at = null;
    protected ?\Nette\Utils\DateTime $updated_at = null;
    private array $searchFields = [];

    public function getSearchFields(): array
    {
        return $this->searchFields;
    }

    public function __construct(array $data, ?\core\database\mysql\lib\nette\CustomNetteSelection $selection)
    {
        parent::__construct($data, $selection);
    }

    public function getRequiredField(): string|null|array
    {
        return 'name';
    }

    /**
     * Set value of propertie idsocial
     *  @param null|int $value
     *  @return self
     */
    public function setIdsocial(?int $value): self
    {
        $this->idsocial = $value;

                return $this;
    }

    /**
     * Get value of propertie idsocial
     *  @return null|int
     */
    public function getIdsocial(): ?int
    {
        return $this->idsocial;
    }

    /**
     * Set value of propertie idcontext
     *  @param null|int $value
     *  @return self
     */
    public function setIdcontext(?int $value): self
    {
        $this->idcontext = $value;

                return $this;
    }

    /**
     * Get value of propertie idcontext
     *  @return null|int
     */
    public function getIdcontext(): ?int
    {
        return $this->idcontext;
    }

    /**
     * Set value of propertie date_birth
     *  @param null|\Nette\Utils\DateTime $value
     *  @return self
     */
    public function setDate_birth(?\Nette\Utils\DateTime $value): self
    {
        $this->date_birth = $value;

                return $this;
    }

    /**
     * Get value of propertie date_birth
     *  @return null|\Nette\Utils\DateTime
     */
    public function getDate_birth(): ?\Nette\Utils\DateTime
    {
        return $this->date_birth;
    }

    /**
     * Set value of propertie place_birth
     *  @param null|string $value
     *  @return self
     */
    public function setPlace_birth(?string $value): self
    {
        $this->place_birth = $value;

                return $this;
    }

    /**
     * Get value of propertie place_birth
     *  @return null|string
     */
    public function getPlace_birth(): ?string
    {
        return $this->place_birth;
    }

    /**
     * Set value of propertie flag_delete
     *  @param bool $value
     *  @return self
     */
    public function setFlag_delete(bool $value): self
    {
        $this->flag_delete = $value;

                return $this;
    }

    /**
     * Get value of propertie flag_delete
     *  @return bool
     */
    public function getFlag_delete(): bool
    {
        return $this->flag_delete;
    }

    /**
     * Set value of propertie created_at
     *  @param null|\Nette\Utils\DateTime $value
     *  @return self
     */
    public function setCreated_at(?\Nette\Utils\DateTime $value): self
    {
        $this->created_at = $value;

                return $this;
    }

    /**
     * Get value of propertie created_at
     *  @return null|\Nette\Utils\DateTime
     */
    public function getCreated_at(): ?\Nette\Utils\DateTime
    {
        return $this->created_at;
    }

    /**
     * Set value of propertie updated_at
     *  @param null|\Nette\Utils\DateTime $value
     *  @return self
     */
    public function setUpdated_at(?\Nette\Utils\DateTime $value): self
    {
        $this->updated_at = $value;

                return $this;
    }

    /**
     * Get value of propertie updated_at
     *  @return null|\Nette\Utils\DateTime
     */
    public function getUpdated_at(): ?\Nette\Utils\DateTime
    {
        return $this->updated_at;
    }

    public function getPropertyVisibilities(): array
    {
        return array (
          'idsocial' => '@public',
          'idcontext' => '@public',
          'date_birth' => '@public',
          'place_birth' => '@public',
          'flag_delete' => '@public',
          'created_at' => '@public',
          'updated_at' => '@public',
        );
    }
}
