<?php namespace app\entities\room_escape\decorator;

/**
 * Class Room_escapeDecorator
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class Room_escapeDecorator implements \core\entities\DecoratorInterface
{
    /**
     * Return room_escape
     *
     * @param \core\entities\room_escape\Room_escape $object
     */
    public function decorate(&$object, ?array $args = null): ?array
    {
        return \null;
    }
}
