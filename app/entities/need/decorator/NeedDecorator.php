<?php namespace app\entities\need\decorator;

/**
 * Class NeedDecorator
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class NeedDecorator implements \core\entities\DecoratorInterface
{
    /**
     * Return need
     *
     * @param \core\entities\need\Need $object
     */
    public function decorate(&$object, ?array $args = null): ?array
    {
        return \null;
    }
}
