<?php namespace app\entities\compagny_opening_time;

/**
 * Class Compagny_opening_time
 *
 *  Representation of mysql row compagny_opening_time
 *
 *  @property ?int $idcompagny_opening_time
 *  @property ?int $idcontext
 *  @property \Nette\Utils\DateTime $hours_start
 *  @property \Nette\Utils\DateTime $hours_end
 *  @property ?int $idcompagny
 *  @property bool $flag_delete
 *  @property ?\Nette\Utils\DateTime $created_at
 *  @property ?\Nette\Utils\DateTime $updated_at
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
#[\core\entities\EntitieAttribute(tableName: 'compagny_opening_time')]
class Compagny_opening_time extends \core\entities\EntitieAbstract
{
    protected ?int $idcompagny_opening_time = null;
    protected ?int $idcontext = null;
    protected \Nette\Utils\DateTime $hours_start;
    protected \Nette\Utils\DateTime $hours_end;
    protected ?int $idcompagny = null;
    protected bool $flag_delete = false;
    protected ?\Nette\Utils\DateTime $created_at = null;
    protected ?\Nette\Utils\DateTime $updated_at = null;
    private array $searchFields = [];

    public function getSearchFields(): array
    {
        return $this->searchFields;
    }

    public function __construct(array $data, ?\core\database\mysql\lib\nette\CustomNetteSelection $selection)
    {
        parent::__construct($data, $selection);
    }

    public function getRequiredField(): string|null|array
    {
        return 'name';
    }

    /**
     * Set value of propertie idcompagny_opening_time
     *  @param null|int $value
     *  @return self
     */
    public function setIdcompagny_opening_time(?int $value): self
    {
        $this->idcompagny_opening_time = $value;
                    return $this;
    }

    /**
     * Get value of propertie idcompagny_opening_time
     *  @return null|int
     */
    public function getIdcompagny_opening_time(): ?int
    {
        return $this->idcompagny_opening_time;
    }

    /**
     * Set value of propertie idcontext
     *  @param null|int $value
     *  @return self
     */
    public function setIdcontext(?int $value): self
    {
        $this->idcontext = $value;
                    return $this;
    }

    /**
     * Get value of propertie idcontext
     *  @return null|int
     */
    public function getIdcontext(): ?int
    {
        return $this->idcontext;
    }

    /**
     * Set value of propertie hours_start
     *  @param \Nette\Utils\DateTime $value
     *  @return self
     */
    public function setHours_start(\Nette\Utils\DateTime $value): self
    {
        $this->hours_start = $value;
                    return $this;
    }

    /**
     * Get value of propertie hours_start
     *  @return \Nette\Utils\DateTime
     */
    public function getHours_start(): \Nette\Utils\DateTime
    {
        return $this->hours_start;
    }

    /**
     * Set value of propertie hours_end
     *  @param \Nette\Utils\DateTime $value
     *  @return self
     */
    public function setHours_end(\Nette\Utils\DateTime $value): self
    {
        $this->hours_end = $value;
                    return $this;
    }

    /**
     * Get value of propertie hours_end
     *  @return \Nette\Utils\DateTime
     */
    public function getHours_end(): \Nette\Utils\DateTime
    {
        return $this->hours_end;
    }

    /**
     * Set value of propertie idcompagny
     *  @param null|int $value
     *  @return self
     */
    public function setIdcompagny(?int $value): self
    {
        $this->idcompagny = $value;
                    return $this;
    }

    /**
     * Get value of propertie idcompagny
     *  @return null|int
     */
    public function getIdcompagny(): ?int
    {
        return $this->idcompagny;
    }

    /**
     * Set value of propertie flag_delete
     *  @param bool $value
     *  @return self
     */
    public function setFlag_delete(bool $value): self
    {
        $this->flag_delete = $value;
                    return $this;
    }

    /**
     * Get value of propertie flag_delete
     *  @return bool
     */
    public function getFlag_delete(): bool
    {
        return $this->flag_delete;
    }

    /**
     * Set value of propertie created_at
     *  @param null|\Nette\Utils\DateTime $value
     *  @return self
     */
    public function setCreated_at(?\Nette\Utils\DateTime $value): self
    {
        $this->created_at = $value;
                    return $this;
    }

    /**
     * Get value of propertie created_at
     *  @return null|\Nette\Utils\DateTime
     */
    public function getCreated_at(): ?\Nette\Utils\DateTime
    {
        return $this->created_at;
    }

    /**
     * Set value of propertie updated_at
     *  @param null|\Nette\Utils\DateTime $value
     *  @return self
     */
    public function setUpdated_at(?\Nette\Utils\DateTime $value): self
    {
        $this->updated_at = $value;
                    return $this;
    }

    /**
     * Get value of propertie updated_at
     *  @return null|\Nette\Utils\DateTime
     */
    public function getUpdated_at(): ?\Nette\Utils\DateTime
    {
        return $this->updated_at;
    }

    public function getPropertyVisibilities(): array
    {
        return array (
          'idcompagny_opening_time' => '@public',
          'idcontext' => '@public',
          'hours_start' => '@public',
          'hours_end' => '@public',
          'idcompagny' => '@public',
          'flag_delete' => '@public',
          'created_at' => '@public',
          'updated_at' => '@public',
          'searchFields' => '@public',
        );
    }

    /**
     * Set value of propertie searchFields
     *  @param null|array $value
     *  @return self
     */
    public function setArchFieldss(?array $value): self
    {
        $this->updateRelations('searchFields', $value);
                        return $this;
    }

    /**
     * Get value of propertie searchFields
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getArchFieldss(): \core\database\mysql\lib\nette\CustomNetteSelection|array
    {
        return $this->findRelated('searchFields');
    }

    /**
     * Set value of propertie searchFields
     *  @param null|array $value
     *  @return self
     */
    public function setSe(?array $value): self
    {
        $this->updateRelations('searchFields', $value);
                        return $this;
    }

    /**
     * Get value of propertie searchFields
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getSe(): \core\database\mysql\lib\nette\CustomNetteSelection|array
    {
        return $this->findRelated('searchFields');
    }

    /**
     * Set value of propertie searchFields
     *  @param null|array $value
     *  @return self
     */
    public function setSearchFieldss(?array $value): self
    {
        $this->updateRelations('searchFields', $value);
                        return $this;
    }

    /**
     * Get value of propertie searchFields
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getSearchFieldss(): \core\database\mysql\lib\nette\CustomNetteSelection|array
    {
        return $this->findRelated('searchFields');
    }
}
