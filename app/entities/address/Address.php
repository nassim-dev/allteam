<?php namespace app\entities\address;

/**
 * Class Address
 *
 *  Representation of mysql row address
 *
 *  @property ?int $idaddress
 *  @property ?int $idcontext
 *  @property string $name
 *  @property ?string $street
 *  @property ?string $city
 *  @property ?int $zipcode
 *  @property ?string $country
 *  @property ?string $geopoint
 *  @property bool $flag_delete
 *  @property ?\Nette\Utils\DateTime $created_at
 *  @property ?\Nette\Utils\DateTime $updated_at
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
#[\core\entities\EntitieAttribute(tableName: 'address')]
class Address extends \core\entities\EntitieAbstract
{
    protected ?int $idaddress = null;
    protected ?int $idcontext = null;
    protected string $name = '';
    protected ?string $street = null;
    protected ?string $city = null;
    protected ?int $zipcode = null;
    protected ?string $country = null;
    protected ?string $geopoint = null;
    protected bool $flag_delete = false;
    protected ?\Nette\Utils\DateTime $created_at = null;
    protected ?\Nette\Utils\DateTime $updated_at = null;
    private array $searchFields = ['name', 'street', 'city', 'zipcode', 'country'];

    public function getSearchFields(): array
    {
        return $this->searchFields;
    }

    public function __construct(array $data, ?\core\database\mysql\lib\nette\CustomNetteSelection $selection)
    {
        parent::__construct($data, $selection);
    }

    public function getRequiredField(): string|null|array
    {
        return 'name';
    }

    /**
     * Set value of propertie idaddress
     *  @param null|int $value
     *  @return self
     */
    public function setIdaddress(?int $value): self
    {
        $this->idaddress = $value;
                    return $this;
    }

    /**
     * Get value of propertie idaddress
     *  @return null|int
     */
    public function getIdaddress(): ?int
    {
        return $this->idaddress;
    }

    /**
     * Set value of propertie idcontact_s
     *  @param null|array $value
     *  @return self
     */
    public function setContacts(?array $value): self
    {
        $this->updateRelations('idcontact', $value);
                        return $this;
    }

    /**
     * Get value of propertie idcontact_s
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getContacts(): array|\core\database\mysql\lib\nette\CustomNetteSelection
    {
        return $this->findRelated('idcontact');
    }

    /**
     * Set value of propertie idcontext
     *  @param null|int $value
     *  @return self
     */
    public function setIdcontext(?int $value): self
    {
        $this->idcontext = $value;
                    return $this;
    }

    /**
     * Get value of propertie idcontext
     *  @return null|int
     */
    public function getIdcontext(): ?int
    {
        return $this->idcontext;
    }

    /**
     * Set value of propertie name
     *  @param string $value
     *  @return self
     */
    public function setName(string $value): self
    {
        $this->name = $value;
                    return $this;
    }

    /**
     * Get value of propertie name
     *  @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set value of propertie street
     *  @param null|string $value
     *  @return self
     */
    public function setStreet(?string $value): self
    {
        $this->street = $value;
                    return $this;
    }

    /**
     * Get value of propertie street
     *  @return null|string
     */
    public function getStreet(): ?string
    {
        return $this->street;
    }

    /**
     * Set value of propertie city
     *  @param null|string $value
     *  @return self
     */
    public function setCity(?string $value): self
    {
        $this->city = $value;
                    return $this;
    }

    /**
     * Get value of propertie city
     *  @return null|string
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * Set value of propertie zipcode
     *  @param null|int $value
     *  @return self
     */
    public function setZipcode(?int $value): self
    {
        $this->zipcode = $value;
                    return $this;
    }

    /**
     * Get value of propertie zipcode
     *  @return null|int
     */
    public function getZipcode(): ?int
    {
        return $this->zipcode;
    }

    /**
     * Set value of propertie country
     *  @param null|string $value
     *  @return self
     */
    public function setCountry(?string $value): self
    {
        $this->country = $value;
                    return $this;
    }

    /**
     * Get value of propertie country
     *  @return null|string
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * Set value of propertie geopoint
     *  @param null|string $value
     *  @return self
     */
    public function setGeopoint(?string $value): self
    {
        $this->geopoint = $value;
                    return $this;
    }

    /**
     * Get value of propertie geopoint
     *  @return null|string
     */
    public function getGeopoint(): ?string
    {
        return $this->geopoint;
    }

    /**
     * Set value of propertie flag_delete
     *  @param bool $value
     *  @return self
     */
    public function setFlag_delete(bool $value): self
    {
        $this->flag_delete = $value;
                    return $this;
    }

    /**
     * Get value of propertie flag_delete
     *  @return bool
     */
    public function getFlag_delete(): bool
    {
        return $this->flag_delete;
    }

    /**
     * Set value of propertie created_at
     *  @param null|\Nette\Utils\DateTime $value
     *  @return self
     */
    public function setCreated_at(?\Nette\Utils\DateTime $value): self
    {
        $this->created_at = $value;
                    return $this;
    }

    /**
     * Get value of propertie created_at
     *  @return null|\Nette\Utils\DateTime
     */
    public function getCreated_at(): ?\Nette\Utils\DateTime
    {
        return $this->created_at;
    }

    /**
     * Set value of propertie updated_at
     *  @param null|\Nette\Utils\DateTime $value
     *  @return self
     */
    public function setUpdated_at(?\Nette\Utils\DateTime $value): self
    {
        $this->updated_at = $value;
                    return $this;
    }

    /**
     * Get value of propertie updated_at
     *  @return null|\Nette\Utils\DateTime
     */
    public function getUpdated_at(): ?\Nette\Utils\DateTime
    {
        return $this->updated_at;
    }

    public function getPropertyVisibilities(): array
    {
        return array (
          'idaddress' => '@public',
          'idcontact_s' => '@public',
          'idcontext' => '@public',
          'name' => '@public',
          'street' => '@public',
          'city' => '@public',
          'zipcode' => '@public',
          'country' => '@public',
          'geopoint' => '@public',
          'flag_delete' => '@public',
          'created_at' => '@public',
          'updated_at' => '@public',
          'searchFields' => '@public',
        );
    }

    /**
     * Set value of propertie searchFields
     *  @param null|array $value
     *  @return self
     */
    public function setArchFieldss(?array $value): self
    {
        $this->updateRelations('searchFields', $value);
                        return $this;
    }

    /**
     * Get value of propertie searchFields
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getArchFieldss(): \core\database\mysql\lib\nette\CustomNetteSelection|array
    {
        return $this->findRelated('searchFields');
    }

    /**
     * Set value of propertie idcontact_s
     *  @param null|array $value
     *  @return self
     */
    public function setId(?array $value): self
    {
        $this->updateRelations('idcontact', $value);
                        return $this;
    }

    /**
     * Get value of propertie idcontact_s
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getId(): \core\database\mysql\lib\nette\CustomNetteSelection|array
    {
        return $this->findRelated('idcontact');
    }

    /**
     * Set value of propertie searchFields
     *  @param null|array $value
     *  @return self
     */
    public function setSe(?array $value): self
    {
        $this->updateRelations('searchFields', $value);
                        return $this;
    }

    /**
     * Get value of propertie searchFields
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getSe(): \core\database\mysql\lib\nette\CustomNetteSelection|array
    {
        return $this->findRelated('searchFields');
    }

    /**
     * Set value of propertie idcontact_s
     *  @param null|array $value
     *  @return self
     */
    public function setIdcontacts(?array $value): self
    {
        $this->updateRelations('idcontact', $value);
                        return $this;
    }

    /**
     * Get value of propertie idcontact_s
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getIdcontacts(): \core\database\mysql\lib\nette\CustomNetteSelection|array
    {
        return $this->findRelated('idcontact');
    }

    /**
     * Set value of propertie searchFields
     *  @param null|array $value
     *  @return self
     */
    public function setSearchFieldss(?array $value): self
    {
        $this->updateRelations('searchFields', $value);
                        return $this;
    }

    /**
     * Get value of propertie searchFields
     *  @return array|\core\database\mysql\lib\nette\CustomNetteSelection
     */
    public function getSearchFieldss(): \core\database\mysql\lib\nette\CustomNetteSelection|array
    {
        return $this->findRelated('searchFields');
    }
}
