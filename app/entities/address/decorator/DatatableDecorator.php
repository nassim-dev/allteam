<?php namespace app\entities\address\decorator;

/**
 * Class DatatableDecorator
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class DatatableDecorator extends \core\entities\decorator\DatatableDecoratorBase
{
    /**
     * Return datatable
     *
     * @param Address $object
     */
    public function decorate(&$object, array $args = null): ?array
    {
        $row = parent::decorate($object, $args);

                    $relatedRows = $object->findRelated('contact', $object::HTML_FORMAT, $args['selector'] ?? null);
                    $rows        = [];
                    foreach ($relatedRows as $relatedRow) {
                        $rows[] = $relatedRow->historical();
                    }

                    $row['idcontact']  = implode('', $rows);
                    $row['idcontext'] = $object->context?->historical();$row['name'] = $object->name;$row['street'] = $object->street;$row['city'] = $object->city;$row['zipcode'] = $object->zipcode;$row['country'] = $object->country;$row['geopoint'] = $object->geopoint;
        return $row;
    }
}
