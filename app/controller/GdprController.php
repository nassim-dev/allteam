<?php namespace app\controller;

/**
 * Class GdprController
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class GdprController extends ControllerBase
{
    protected $name = 'gdpr';

    #[\core\routing\RouteAttribute(url: '/gdpr', method: 'GET', permission: ['read:gdpr'], encoding: false, displayName: 'gdpr')]
    public function indexAction()
    {
        parent::indexAction();
    }

    #[\core\routing\RouteAttribute(url: '/gdpr/:id', method: 'GET', permission: ['read:gdpr'], encoding: false)]
    public function byIdAction(\core\model\ModelServiceInterface $modelFactory, \core\html\bar\BarFactory $barFactory)
    {
        parent::byIdAction($modelFactory, $barFactory);
    }

    protected function getModelBase(): \app\model\GdprModel
    {
        return $this->getModel("Gdpr");
    }
}
