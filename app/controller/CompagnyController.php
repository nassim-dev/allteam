<?php namespace app\controller;

/**
 * Class CompagnyController
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class CompagnyController extends ControllerBase
{
    protected $name = 'compagny';

    #[\core\routing\RouteAttribute(url: '/compagny', method: 'GET', permission: ['read:compagny'], encoding: false, displayName: 'compagny')]
    public function indexAction()
    {
        if (!$this->auth->isAllowed() && $this->getParameter('id') === null) {
            return $this->errorAction(403);
        }

        //Create default edit form && modals
        $this->getDi()->callInside($this, 'createMainModalEditWidget');

        //Create default add form && modals
        $this->getDi()->callInside($this, 'createMainModalAddWidget');


        //Default table && buttons
        $table = $this->getDi()->callInside($this, 'createMainTableWidget');

        /**
         * Generate pills table
         */
        $pillTable = $this->getPillsTable();
        $pillTable->setTitle('');

        $pill = $pillTable->addPill('table', _('Table'));
        $pill->appendChild($table);
        $pill->setDefault(true);

        $pillTable->setDefault($pill);
    }

    protected function getModelBase(): \app\model\CompagnyModel
    {
        return $this->getModel('Compagny');
    }

    #[\core\routing\RouteAttribute(url: '/compagny/:id', method: 'GET', permission: ['read:compagny'], encoding: false)]
    public function byIdAction(\core\model\ModelServiceInterface $modelFactory, \core\html\bar\BarFactory $barFactory)
    {
        parent::byIdAction($modelFactory, $barFactory);
    }
}
