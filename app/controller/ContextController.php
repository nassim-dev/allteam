<?php namespace app\controller;

/**
 * Class ContextController
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class ContextController extends ControllerBase
{
    protected $name = 'context';

    #[\core\routing\RouteAttribute(url: '/context', method: 'GET', permission: ['read:context'], encoding: false, displayName: 'context')]
    public function indexAction()
    {
        if (!$this->auth->isAllowed() && $this->getParameter('id') === null) {
            return $this->errorAction(403);
        }

        //Create default edit form && modals
        $this->getDi()->callInside($this, 'createMainModalEditWidget');

        //Create default add form && modals
        $this->getDi()->callInside($this, 'createMainModalAddWidget');


        //Default table && buttons
        $table = $this->getDi()->callInside($this, 'createMainTableWidget');

        /**
         * Generate pills table
         */
        $pillTable = $this->getPillsTable();
        $pillTable->setTitle('');

        $pill = $pillTable->addPill('table', _('Table'));
        $pill->appendChild($table);
        $pill->setDefault(true);

        $pillTable->setDefault($pill);


        $draglist = $this->getDi()->call($this, 'createDraglistWidget');
        $pill     = $pillTable->addPill('draglist', _('Draglist'));
        $pill->appendChild($draglist);
    }

    protected function getModelBase(): \app\model\ContextModel
    {
        return $this->getModel('Context');
    }

    public function createDraglistWidget(
        \core\html\draglist\DraglistFactory $draglistFactory,
        \core\html\bar\BarFactory $barFactory,
        \core\database\mysql\lib\nette\CustomNetteSelection|array $items = []
    ): \core\html\draglist\Draglist {
        $draglist = $draglistFactory->create('Context');
        $draglist->setId('todo');

        $todo = $draglist->addColumn('Status 1', 'status-1');
        $todo->addClass('info');

        $progress = $draglist->addColumn('Status 2', 'status-2');
        $progress->addClass('warning');

        $review = $draglist->addColumn('Status 3', 'status-3');
        $review->addClass('danger');

        $done = $draglist->addColumn('Status 4', 'status-4');
        $done->addClass('success');

        foreach ($items as $item) {
            $itemDecorated = new \core\html\draglist\DraglistItem($item->toArray());
            $itemDecorated->setActionBar($barFactory->createDefaultButtons($item));
            $itemDecorated->addDataAttribute('uuid', $item->idcontext);
            switch ($itemDecorated->checked) {
                case 'Status 1':
                    $todo->appendChild($itemDecorated);

                    break;
                case 'Status 2':
                    $progress->appendChild($itemDecorated);

                    break;
                case 'Status 3':
                    $review->appendChild($itemDecorated);

                    break;
                case 'Status 4':
                    $done->appendChild($itemDecorated);

                    break;
                default:
                    $todo->appendChild($itemDecorated);
            }
        }

        $link = new \core\routing\Link(\app\api\controller\ContextController::class, 'htmlListAction');
        $draglist->addEndpoint('readData', $link);

        $this->view->assignWidget($draglist);

        return $draglist;
    }

    #[\core\routing\RouteAttribute(url: '/context/:id', method: 'GET', permission: ['read:context'], encoding: false)]
    public function byIdAction(\core\model\ModelServiceInterface $modelFactory, \core\html\bar\BarFactory $barFactory)
    {
        parent::byIdAction($modelFactory, $barFactory);
    }
}
