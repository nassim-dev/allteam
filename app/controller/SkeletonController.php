<?php

namespace app\controller;

/**
 * Class SkeletonController
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class SkeletonController extends \app\controller\ControllerBase
{
    /**
     * @var string
     */
    protected $name = 'skeleton';

    /**
     * Default Action
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url:'/skeleton',
        method:'GET',
        permission:['read:skeleton'],
        encoding:false,
        displayName: 'Skeleton'
    )]
    public function indexAction()
    {
        parent::indexAction();
    }

    /**
     * Sigle entitie view
     *
     * @return void
     */
    #[\core\routing\RouteAttribute(
        url:'/skeleton/:id',
        method:'GET',
        permission:['read:skeleton'],
        encoding:false
    )]
    public function byIdAction(\core\model\ModelServiceInterface $modelFactory, \core\html\bar\BarFactory $barFactory)
    {
        parent::byIdAction($modelFactory, $barFactory);
    }

    /**
     * Return default Model
     */
    protected function getModelBase(): \app\model\SkeletonModel
    {
        return $this->getModel('Skeleton');
    }

    public function createTimeline(): \core\html\timeline\Timeline
    {
        /** @var \core\html\timeline\TimelineFactory $timelineFactory */
        $timelineFactory = $this->getDi()->singleton(\core\html\timeline\TimelineFactory::class);

        $timeline = $timelineFactory->create('test');
        $timeline->addDateSelector()
            ->addFitSelector()
            ->addTodaySelector();

        $link = new \core\routing\Link(
            \app\api\controller\SkeletonController::class,
            'getAllAction'
        );
        $timeline->addEndpoint('readData', $link);

        $link = new \core\routing\Link(
            \app\api\controller\SkeletonController::class,
            'createAction'
        );
        $timeline->addEndpoint('createData', $link);

        $link = new \core\routing\Link(
            \app\api\controller\SkeletonController::class,
            'updateAction'
        );
        $timeline->addEndpoint('updateData', $link);

        $this->view->assignWidget($timeline);

        return $timeline;
    }


    /**
     * Create personnal user calendar
     */
    public function createCalendarWidget(
        \core\html\calendar\CalendarFactory $calendarFactory,
        \core\html\CommandFactory $commandFactory
    ): \core\html\calendar\Calendar {
        $calendar = $calendarFactory->create('skeleton', ['name' => 'skeleton']);

        $calendar->setTitle(_('Planning'));

        $link = new \core\routing\Link(
            \app\api\controller\SkeletonController::class,
            'getAllAction'
        );

        $calendar->addEndpoint('readData', $link);

        $link = new \core\routing\Link(
            \app\api\controller\SkeletonController::class,
            'getAllAction'
        );

        $calendar->addEndpoint('readRessources', $link);

        $link = new \core\routing\Link(
            \app\api\controller\SkeletonController::class,
            'createAction'
        );

        $calendar->addEndpoint('createData', $link);

        $link = new \core\routing\Link(
            \app\api\controller\SkeletonController::class,
            'updateAction'
        );

        $calendar->addEndpoint('updateData', $link);

        $calendar->addConfig('refetchResourcesOnNavigate')
            ->value(true);

        $calendar->addConfig('selectable')
            ->value(true);

        $eventRender = \core\html\calendar\CalendarHelper::getSimpleRenderEvent();
        $calendar->on('eventRender')->command($commandFactory->create(
            \core\html\javascript\command\EvalCommand::class,
            [
                'callback' => $eventRender
            ]
        ));

        $calendar->addExportButton();

        $this->view->assignWidget($calendar);

        return $calendar;
    }

    /**
     * Create personnal user flow
     */
    public function createFlowWidget(\core\html\flow\FlowFactory $flowFactory): \core\html\flow\Flow
    {
        $flow = $flowFactory->create('skeleton', ['name' => 'skeleton']);

        $flow->setTitle(_('Planning'));

        $link = new \core\routing\Link(
            \app\api\controller\SkeletonController::class,
            'getAllAction'
        );

        $flow->addEndpoint('readData', $link);

        $link = new \core\routing\Link(
            \app\api\controller\SkeletonController::class,
            'getAllAction'
        );

        $flow->addEndpoint('readRessources', $link);

        $link = new \core\routing\Link(
            \app\api\controller\SkeletonController::class,
            'createAction'
        );

        $flow->addEndpoint('createData', $link);

        $link = new \core\routing\Link(
            \app\api\controller\SkeletonController::class,
            'updateAction'
        );

        $flow->addEndpoint('updateData', $link);

        $this->view->assignWidget($flow);

        return $flow;
    }

    /**
     * Create user todoList
     */
    public function createDraglistWidget(
        \core\html\draglist\DraglistFactory $draglistFactory,
        \core\html\bar\BarFactory $barFactory,
        \core\database\mysql\lib\nette\CustomNetteSelection|array $items = []
    ): \core\html\draglist\Draglist {
        $draglist = $draglistFactory->create('Skeleton');
        $draglist->setId('todo');

        $todo = $draglist->addColumn('Status 1', 'status-1');
        $todo->addClass('info');

        $progress = $draglist->addColumn('Status 2', 'status-2');
        $progress->addClass('warning');

        $review = $draglist->addColumn('Status 3', 'status-3');
        $review->addClass('danger');

        $done = $draglist->addColumn('Status 4', 'status-4');
        $done->addClass('success');

        foreach ($items as $item) {
            $itemDecorated = new \core\html\draglist\DraglistItem($item->toArray());
            $itemDecorated->setActionBar($barFactory->createDefaultButtons($item));
            $itemDecorated->addDataAttribute('uuid', $item->idskeleton);
            switch ($itemDecorated->checked) {
                case 'Status 1':
                    $todo->appendChild($itemDecorated);

                    break;
                case 'Status 2':
                    $progress->appendChild($itemDecorated);

                    break;
                case 'Status 3':
                    $review->appendChild($itemDecorated);

                    break;
                case 'Status 4':
                    $done->appendChild($itemDecorated);

                    break;
                default:
                    $todo->appendChild($itemDecorated);
            }
        }

        $link = new \core\routing\Link(
            \app\api\controller\SkeletonController::class,
            'htmlListAction'
        );

        $draglist->addEndpoint('readData', $link);

        $this->view->assignWidget($draglist);

        return $draglist;
    }
}
