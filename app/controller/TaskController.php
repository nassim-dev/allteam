<?php namespace app\controller;

/**
 * Class TaskController
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class TaskController extends ControllerBase
{
    protected $name = 'task';

    #[\core\routing\RouteAttribute(url: '/task', method: 'GET', permission: ['read:task'], encoding: false, displayName: 'task')]
    public function indexAction()
    {
        if (!$this->auth->isAllowed() && $this->getParameter('id') === null) {
            return $this->errorAction(403);
        }

        //Create default edit form && modals
        $this->getDi()->callInside($this, 'createMainModalEditWidget');

        //Create default add form && modals
        $this->getDi()->callInside($this, 'createMainModalAddWidget');


        //Default table && buttons
        $table = $this->getDi()->callInside($this, 'createMainTableWidget');

        /**
         * Generate pills table
         */
        $pillTable = $this->getPillsTable();
        $pillTable->setTitle('');

        $pill = $pillTable->addPill('table', _('Table'));
        $pill->appendChild($table);
        $pill->setDefault(true);

        $pillTable->setDefault($pill);


        $timeline = $this->createTimeline();
        $pill     = $pillTable->addPill('timeline', _('Timeline'));
        $pill->appendChild($timeline);

        $calendar = $this->getDi()->call($this, 'createCalendarWidget');
        $pill     = $pillTable->addPill('calendar', _('Calendar'));
        $pill->appendChild($calendar);

        $draglist = $this->getDi()->call($this, 'createDraglistWidget');
        $pill     = $pillTable->addPill('draglist', _('Draglist'));
        $pill->appendChild($draglist);
    }

    protected function getModelBase(): \app\model\TaskModel
    {
        return $this->getModel('Task');
    }

    public function createTimeline(): \core\html\timeline\Timeline
    {
        /** @var \core\html\timeline\TimelineFactory $timelineFactory */
        $timelineFactory = $this->getDi()->singleton(\core\html\timeline\TimelineFactory::class);

        $timeline = $timelineFactory->create('test');
        $timeline->addDateSelector()
            ->addFitSelector()
            ->addTodaySelector();

        $link = new \core\routing\Link(\app\api\controller\TaskController::class, 'getAllAction');
        $timeline->addEndpoint('readData', $link);

        $link = new \core\routing\Link(\app\api\controller\TaskController::class, 'createAction');
        $timeline->addEndpoint('createData', $link);

        $link = new \core\routing\Link(\app\api\controller\TaskController::class, 'updateAction');
        $timeline->addEndpoint('updateData', $link);

        $this->view->assignWidget($timeline);

        return $timeline;
    }

    public function createCalendarWidget(
        \core\html\calendar\CalendarFactory $calendarFactory,
        \core\html\CommandFactory $commandFactory
    ): \core\html\calendar\Calendar {
        $calendar = $calendarFactory->create('task', ['name' => 'task']);

        $calendar->setTitle(_('Planning'));

        $link = new \core\routing\Link(\app\api\controller\TaskController::class, 'getAllAction');
        $calendar->addEndpoint('readData', $link);

        $link = new \core\routing\Link(\app\api\controller\TaskController::class, 'getAllAction');
        $calendar->addEndpoint('readRessources', $link);

        $link = new \core\routing\Link(\app\api\controller\TaskController::class, 'createAction');
        $calendar->addEndpoint('createData', $link);

        $link = new \core\routing\Link(\app\api\controller\TaskController::class, 'updateAction');
        $calendar->addEndpoint('updateData', $link);

        $calendar->addConfig('refetchResourcesOnNavigate')
            ->value(true);
        $calendar->addConfig('selectable')
            ->value(true);
        //$calendar->addConfig("defaultView")->value('agendaWeek');

        $eventRender = \core\html\calendar\CalendarHelper::getSimpleRenderEvent();
        $calendar->on('eventRender')->command($commandFactory->create(\core\html\javascript\command\EvalCommand::class, ['callback' => $eventRender]));

        $calendar->addExportButton();

        $this->view->assignWidget($calendar);

        return $calendar;
    }

    public function createDraglistWidget(
        \core\html\draglist\DraglistFactory $draglistFactory,
        \core\html\bar\BarFactory $barFactory,
        \core\database\mysql\lib\nette\CustomNetteSelection|array $items = []
    ): \core\html\draglist\Draglist {
        $draglist = $draglistFactory->create('Task');
        $draglist->setId('todo');

        $todo = $draglist->addColumn('Status 1', 'status-1');
        $todo->addClass('info');

        $progress = $draglist->addColumn('Status 2', 'status-2');
        $progress->addClass('warning');

        $review = $draglist->addColumn('Status 3', 'status-3');
        $review->addClass('danger');

        $done = $draglist->addColumn('Status 4', 'status-4');
        $done->addClass('success');

        foreach ($items as $item) {
            $itemDecorated = new \core\html\draglist\DraglistItem($item->toArray());
            $itemDecorated->setActionBar($barFactory->createDefaultButtons($item));
            $itemDecorated->addDataAttribute('uuid', $item->idtask);
            switch ($itemDecorated->checked) {
                case 'Status 1':
                    $todo->appendChild($itemDecorated);

                    break;
                case 'Status 2':
                    $progress->appendChild($itemDecorated);

                    break;
                case 'Status 3':
                    $review->appendChild($itemDecorated);

                    break;
                case 'Status 4':
                    $done->appendChild($itemDecorated);

                    break;
                default:
                    $todo->appendChild($itemDecorated);
            }
        }

        $link = new \core\routing\Link(\app\api\controller\TaskController::class, 'htmlListAction');
        $draglist->addEndpoint('readData', $link);

        $this->view->assignWidget($draglist);

        return $draglist;
    }

    #[\core\routing\RouteAttribute(url: '/task/:id', method: 'GET', permission: ['read:task'], encoding: false)]
    public function byIdAction(\core\model\ModelServiceInterface $modelFactory, \core\html\bar\BarFactory $barFactory)
    {
        parent::byIdAction($modelFactory, $barFactory);
    }
}
