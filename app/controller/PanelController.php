<?php

namespace app\controller;

use app\model\PanelModel;
use core\database\RepositoryFactoryInterface;
use core\DI\DiProvider;
use core\html\bar\BarFactory;
use core\html\calendar\Calendar;
use core\html\calendar\CalendarFactory;
use core\html\calendar\CalendarHelper;
use core\html\CommandFactory;
use core\html\flow\FlowFactory;
use core\html\javascript\command\EvalCommand;
use core\html\timeline\Timeline;
use core\html\timeline\TimelineFactory;
use core\html\tour\BootstrapTour;
use core\html\tour\BootstrapTourFactory;
use core\model\ModelServiceInterface;
use core\routing\RouteAttribute;

/**
 * Class PanelController
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class PanelController extends ControllerBase
{
    use DiProvider;
    /**
     * @var string
     */
    protected $name = 'panel';

    /**
     * Sigle entitie view
     *
     * @return void
     */
    #[RouteAttribute(url:'/panel/:id', method:'GET', permission:['read:panel'], encoding:false, visible:false)]
    public function byIdAction(ModelServiceInterface $modelFactory, BarFactory $barFactory)
    {
        parent::byIdAction($modelFactory, $barFactory);
    }

    /**
     * By performance
     *
     * @todo implement
     *
     * @return void
     */
    #[RouteAttribute(url:'/panel/byPerformance/:id', method:'GET', permission:['read:panel'], encoding:false, visible:false)]
    public function byPerformanceAction()
    {
        $this->setPageTemplate(BASE_DIR . 'templates/pages/page.under.construct.html');
        //throw new ControllerException('Implements methods ' . __METHOD__ . ' in ' . __CLASS__);
    }

    /**
     * Create Bootstrap tour
     *
     * @todo fix autoreload page anormal
     */
    public function createBootstrapTourWidget(): BootstrapTour
    {
        $tour = $this->getDi()->singleton(BootstrapTourFactory::class)->getPageTour();
        $tour->addStep(
            [
                'element' => '#navbar-header',
                'title'   => _('User menu'),
                'content' => 'All your personals datas'
            ]
        );

        $tour->addStep(
            [
                'element' => '#slide-out',
                'title'   => 'Main menu',
                'content' => 'Access to allteam pluggins'
            ]
        );

        $tour->addStep(
            [
                'element' => '#slide-chat',
                'title'   => 'Instant messaging',
                'content' => 'Share your ideas with yout team'
            ]
        );
        $tour->addConfig('autostart')->value(true);

        $this->view->assignWidget($tour);

        return $tour;
    }

    /**
     * Create personnal user calendar
     */
    public function createUserCalendarWidget(CalendarFactory $calendarFactory, CommandFactory $commandFactory): Calendar
    {
        $calendar = $calendarFactory->create('global', ['name' => 'global']);
        $urls     = $this->getModelBase()->getUrls();

        $calendar->setTitle(_('Planning'));
        $calendar->addConfig('urls')
            ->addParameters($urls['events']);
        $calendar->addConfig('resources')
            ->addParameters($urls['ressources']);
        $calendar->addConfig('refetchResourcesOnNavigate')
            ->value(true);
        $calendar->addConfig('selectable')
            ->value(true);
        //$calendar->addConfig("defaultView")->value('agendaWeek');

        $eventRender = CalendarHelper::getSimpleRenderEvent();
        $calendar->on('eventRender')->command($commandFactory->create(EvalCommand::class, ['callback' => $eventRender]));

        $calendar->addExportButton();

        $this->view->assignWidget($calendar);

        return $calendar;
    }

    /**
     * Default Action
     *
     * @return void
     */
    #[RouteAttribute(url:'/panel', method:'GET', permission:['read:panel'], encoding:false, visible:false)]
    public function indexAction()
    {
        if (!$this->auth->isAllowed() && $this->getParameter('id') === null) {
            return $this->errorAction('403');
        }

        $calendar = $this->getDi()->call($this, 'createUserCalendarWidget');
        $timeline = $this->createTimeline();

        /** @var RepositoryFactoryInterface $repositoryFactory */
        //$repositoryFactory =$this->getDi()->singleton(RepositoryFactoryInterface::class);
        //$draglist =$this->getDi()->call(TodoController::class, 'createTodoListWidget', ['items' => []]);
        //$this->getDi()->call(TodoController::class, 'createTodoFormsWidget', ['draglist' => $draglist]);

        /**
         * Generate pills table
         */
        $pillTable = $this->getPillsTable();
        $pillTable->setTitle('');

        //$pill = $pillTable->addPill('todo', _('Todo'));
        //$pill->appendChild($draglist);

        $pill = $pillTable->addPill('timeline', _('Timeline'));
        $pill->appendChild($timeline);

        $pill->setDefault(true);
        $pillTable->setDefault($pill);

        $pill = $pillTable->addPill('calendar', _('Calendar'));
        $pill->appendChild($calendar);

        /** @var FlowFactory $flowFactory */
        $flowFactory = $this->getDi()->singleton(FlowFactory::class);
        $flow        = $flowFactory->create('Flow');

        $pill = $pillTable->addPill('flow', _('Flow'));
        $pill->appendChild($flow);

        //$this->view->assign('alertes', $this->getModelBase()->getLastAlertes());
        //$this->view->assign('historicals', $this->getModelBase()->getLastHistoricals());
        //$this->view->assign('nb_event_out', $this->getModelBase()->getPerformanceTofinalizeCount());
        //$this->view->assign('nb_event_in', $this->getModelBase()->getPerformanceFutureCount());
        //$this->view->assign('nb_task', $this->getModelBase()->getNewTaskCount());
        //$this->view->assign('nb_message', $this->getModelBase()->getNewMessageCount());

        $this->view->assign('TEMPLATE_PAGE', 'panel');
    }

    public function createTimeline(): Timeline
    {
        /** @var TimelineFactory $timelineFactory */
        $timelineFactory = $this->getDi()->singleton(TimelineFactory::class);

        $timeline = $timelineFactory->create('test');
        $timeline->addDateSelector()
            ->addFitSelector()
            ->addTodaySelector();

        //$link = new Link(TodoControllerApi::class, 'getAllAction');
        //$timeline->addEndpoint('readData', $link);

        //$link = new Link(TodoControllerApi::class, 'createAction');
        //$timeline->addEndpoint('createData', $link);

        //$link = new Link(TodoControllerApi::class, 'updateAction');
        //$timeline->addEndpoint('updateData', $link);

        $this->view->assignWidget($timeline);

        return $timeline;
    }

    /**
     * Return default Model
     */
    protected function getModelBase(): PanelModel
    {
        return $this->getModel('Panel');
    }
}
