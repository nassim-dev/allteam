<?php

namespace app\controller;

use core\DI\DiProvider;
use core\html\AssetsServiceInterface;
use core\messages\request\HttpRequestInterface;
use core\messages\response\HttpResponseInterface;
use core\routing\RouteAttribute;
use core\secure\authentification\AuthServiceInterface;
use core\view\ViewServiceInterface;

/**
 * Class ContractController
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class WidgetController extends ControllerBase
{
    use DiProvider;

    /**
     * @var string
     */
    protected $name = 'widget';

    public function __construct(
        protected AuthServiceInterface $auth,
        protected HttpRequestInterface $request,
        protected HttpResponseInterface $response,
        protected ViewServiceInterface $view
    ) {
        parent::__construct($auth, $request, $response, $view);
        $this->setPopup(true);
    }

        /**
         * Default Action
         *
         * @return void
         */
        #[RouteAttribute(url:'/widget', method:'GET', permission:['read:widget'], encoding:false, visible:false)]
        public function indexAction()
        {
            /** @var AssetsServiceInterface $assets */
            $assets = $this->getDi()->singleton(AssetsServiceInterface::class);
            $assets->addJS('scripts/script.widget.init.js');

            $this->view->assign('TEMPLATE_PAGE', 'widget');
            $this->view->assign('NO_MENU', true);

            parent::indexAction();
        }
}
