<?php namespace app\controller;

/**
 * Class LogController
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class LogController extends ControllerBase
{
    protected $name = 'log';

    #[\core\routing\RouteAttribute(url: '/log', method: 'GET', permission: ['read:log'], encoding: false, displayName: 'log')]
    public function indexAction()
    {
        parent::indexAction();
    }

    #[\core\routing\RouteAttribute(url: '/log/:id', method: 'GET', permission: ['read:log'], encoding: false)]
    public function byIdAction(\core\model\ModelServiceInterface $modelFactory, \core\html\bar\BarFactory $barFactory)
    {
        parent::byIdAction($modelFactory, $barFactory);
    }

    protected function getModelBase(): \app\model\LogModel
    {
        return $this->getModel("Log");
    }
}
