<?php namespace app\controller;

/**
 * Class HistoricalController
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class HistoricalController extends ControllerBase
{
    protected $name = 'historical';

    #[\core\routing\RouteAttribute(url: '/historical', method: 'GET', permission: ['read:historical'], encoding: false, displayName: 'historical')]
    public function indexAction()
    {
        parent::indexAction();
    }

    #[\core\routing\RouteAttribute(url: '/historical/:id', method: 'GET', permission: ['read:historical'], encoding: false)]
    public function byIdAction(\core\model\ModelServiceInterface $modelFactory, \core\html\bar\BarFactory $barFactory)
    {
        parent::byIdAction($modelFactory, $barFactory);
    }

    protected function getModelBase(): \app\model\HistoricalModel
    {
        return $this->getModel("Historical");
    }
}
