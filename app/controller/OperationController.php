<?php namespace app\controller;

/**
 * Class OperationController
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class OperationController extends ControllerBase
{
    protected $name = 'operation';

    #[\core\routing\RouteAttribute(url: '/operation', method: 'GET', permission: ['read:operation'], encoding: false, displayName: 'operation')]
    public function indexAction()
    {
        if (!$this->auth->isAllowed() && $this->getParameter('id') === null) {
            return $this->errorAction(403);
        }

        //Create default edit form && modals
        $this->getDi()->callInside($this, 'createMainModalEditWidget');

        //Create default add form && modals
        $this->getDi()->callInside($this, 'createMainModalAddWidget');


        //Default table && buttons
        $table = $this->getDi()->callInside($this, 'createMainTableWidget');

        /**
         * Generate pills table
         */
        $pillTable = $this->getPillsTable();
        $pillTable->setTitle('');

        $pill = $pillTable->addPill('table', _('Table'));
        $pill->appendChild($table);
        $pill->setDefault(true);

        $pillTable->setDefault($pill);


        $timeline = $this->createTimeline();
        $pill     = $pillTable->addPill('timeline', _('Timeline'));
        $pill->appendChild($timeline);

        $calendar = $this->getDi()->call($this, 'createCalendarWidget');
        $pill     = $pillTable->addPill('calendar', _('Calendar'));
        $pill->appendChild($calendar);

        $flow = $this->getDi()->call($this, 'createFlowWidget');
        $pill = $pillTable->addPill('flow', _('Flow'));
        $pill->appendChild($flow);
    }

    protected function getModelBase(): \app\model\OperationModel
    {
        return $this->getModel('Operation');
    }

    public function createTimeline(): \core\html\timeline\Timeline
    {
        /** @var \core\html\timeline\TimelineFactory $timelineFactory */
        $timelineFactory = $this->getDi()->singleton(\core\html\timeline\TimelineFactory::class);

        $timeline = $timelineFactory->create('test');
        $timeline->addDateSelector()
            ->addFitSelector()
            ->addTodaySelector();

        $link = new \core\routing\Link(\app\api\controller\OperationController::class, 'getAllAction');
        $timeline->addEndpoint('readData', $link);

        $link = new \core\routing\Link(\app\api\controller\OperationController::class, 'createAction');
        $timeline->addEndpoint('createData', $link);

        $link = new \core\routing\Link(\app\api\controller\OperationController::class, 'updateAction');
        $timeline->addEndpoint('updateData', $link);

        $this->view->assignWidget($timeline);

        return $timeline;
    }

    public function createCalendarWidget(
        \core\html\calendar\CalendarFactory $calendarFactory,
        \core\html\CommandFactory $commandFactory
    ): \core\html\calendar\Calendar {
        $calendar = $calendarFactory->create('operation', ['name' => 'operation']);

        $calendar->setTitle(_('Planning'));

        $link = new \core\routing\Link(\app\api\controller\OperationController::class, 'getAllAction');
        $calendar->addEndpoint('readData', $link);

        $link = new \core\routing\Link(\app\api\controller\OperationController::class, 'getAllAction');
        $calendar->addEndpoint('readRessources', $link);

        $link = new \core\routing\Link(\app\api\controller\OperationController::class, 'createAction');
        $calendar->addEndpoint('createData', $link);

        $link = new \core\routing\Link(\app\api\controller\OperationController::class, 'updateAction');
        $calendar->addEndpoint('updateData', $link);

        $calendar->addConfig('refetchResourcesOnNavigate')
            ->value(true);
        $calendar->addConfig('selectable')
            ->value(true);
        //$calendar->addConfig("defaultView")->value('agendaWeek');

        $eventRender = \core\html\calendar\CalendarHelper::getSimpleRenderEvent();
        $calendar->on('eventRender')->command($commandFactory->create(\core\html\javascript\command\EvalCommand::class, ['callback' => $eventRender]));

        $calendar->addExportButton();

        $this->view->assignWidget($calendar);

        return $calendar;
    }

    public function createFlowWidget(
        \core\html\flow\FlowFactory $flowFactory,
        \core\html\CommandFactory $commandFactory
    ): \core\html\flow\Flow {
        $flow = $flowFactory->create('operation', ['name' => 'operation']);

        $flow->setTitle(_('Planning'));

        $link = new \core\routing\Link(\app\api\controller\OperationController::class, 'getAllAction');
        $flow->addEndpoint('readData', $link);

        $link = new \core\routing\Link(\app\api\controller\OperationController::class, 'getAllAction');
        $flow->addEndpoint('readRessources', $link);

        $link = new \core\routing\Link(\app\api\controller\OperationController::class, 'createAction');
        $flow->addEndpoint('createData', $link);

        $link = new \core\routing\Link(\app\api\controller\OperationController::class, 'updateAction');
        $flow->addEndpoint('updateData', $link);

        $this->view->assignWidget($flow);

        return $flow;
    }

    #[\core\routing\RouteAttribute(url: '/operation/:id', method: 'GET', permission: ['read:operation'], encoding: false)]
    public function byIdAction(\core\model\ModelServiceInterface $modelFactory, \core\html\bar\BarFactory $barFactory)
    {
        parent::byIdAction($modelFactory, $barFactory);
    }
}
