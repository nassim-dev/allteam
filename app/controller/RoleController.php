<?php namespace app\controller;

/**
 * Class RoleController
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class RoleController extends ControllerBase
{
    protected $name = 'role';

    #[\core\routing\RouteAttribute(url: '/role', method: 'GET', permission: ['read:role'], encoding: false, displayName: 'role')]
    public function indexAction()
    {
        parent::indexAction();
    }

    #[\core\routing\RouteAttribute(url: '/role/:id', method: 'GET', permission: ['read:role'], encoding: false)]
    public function byIdAction(\core\model\ModelServiceInterface $modelFactory, \core\html\bar\BarFactory $barFactory)
    {
        parent::byIdAction($modelFactory, $barFactory);
    }

    protected function getModelBase(): \app\model\RoleModel
    {
        return $this->getModel("Role");
    }
}
