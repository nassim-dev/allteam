<?php

namespace app\controller;

use app\widgets\datatable\HistoricalDatatableWidget;
use core\controller\AbstractController;
use core\database\RepositoryFactoryInterface;
use core\html\AssetsServiceInterface;
use core\html\bar\BarFactory;
use core\html\button\Button;
use core\html\button\command\LogoutCommand;
use core\html\button\command\SendMailCommand;
use core\html\CommandFactory;
use core\html\form\command\SubmitCommand;
use core\html\form\elements\RichtextElement;
use core\html\form\elements\SelectElement;
use core\html\form\Form;
use core\html\form\FormFactory;
use core\html\menu\MenuFactory;
use core\html\modal\command\ModalShowCommand;
use core\html\modal\Modal;
use core\html\modal\ModalFactory;
use core\html\progressBar\ProgressBar;
use core\html\progressBar\ProgressBarFactory;
use core\html\tables\datatable\command\RenderCommand;
use core\html\tables\datatable\Datatable;
use core\html\tables\datatable\DatatableFactory;
use core\html\tables\pillsTable\PillsTable;
use core\html\tables\pillsTable\PillsTableFactory;
use core\messages\response\HttpResponseInterface;
use core\model\ModelFactory;
use core\model\ModelInterface;
use core\model\ModelServiceInterface;
use core\routing\RouteAttribute;
use core\secure\jwt\JwtServiceInterface;
use core\session\SessionInterface;
use core\transport\TransportServiceInterface;
use Nette\Utils\DateTime;

/**
 * Class ControllerBase
 *
 * Parent class for controllers
 *
 * @category  Parent class for controllers
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
abstract class ControllerBase extends AbstractController
{
    /**
     * Default model for controller
     *
     * @var ModelInterface
     */
    protected $modelBase;

    /**
     * Controller ressource name
     *
     * @var string|null
     */
    protected $name;


    protected function initMenu()
    {
        /** @var MenuFactory $menuFactory */
        $menuFactory = $this->getDi()->singleton(MenuFactory::class);
        $this->view->assignWidget($menuFactory->getMessengerMenu());
        $this->view->disposeWidget((new RichtextElement())->setName('messenger-content'), 'messenger-form');

        $this->view->assignWidget($menuFactory->getUserMenu());
        //$this->view->assignWidget($menuFactory->getShortcutsMenu());
        $this->view->assignWidget($menuFactory->getMainMenu());
    }

    public function getPillsTable(): PillsTable
    {
        /** @var PillsTableFactory $pillsTableFactory */
        $pillsTableFactory = $this->getDi()->singleton(PillsTableFactory::class);
        $pillsTable        = $pillsTableFactory->create(
            $this->name,
            [
                'title' => _('Management of : ') . $this->name
            ]
        );
        $pillsTable->setDefault('main');
        $pillsTable->setInlineMenu();

        $this->view->assignWidget($pillsTable);

        return $pillsTable;
    }

    /**
     * Sigle entitie view
     *
     * @return void
     */
    public function byIdAction(ModelServiceInterface $modelFactory, BarFactory $barFactory)
    {
        //Fetch default model

        $this->modelBase = $modelFactory->create(ucfirst($this->name));
        $ressource       = $this->modelBase->byIdAction();

        if (is_bool($ressource)) {
            $this->setPageTemplate(BASE_DIR . 'templates/pages/page.ressource.notfound.html');

            return;
        }

        $actionBar = $barFactory->createForEntity($ressource);
        $button    = $ressource->createButton(_('Edit'));
        $button->setPage('button.html');
        $this->view->assign('actionBar', $actionBar);


        //Create default edit form && modals
        $this->getDi()->callInside($this, 'createMainModalEditWidget');
        $this->view->assign('TEMPLATE_PAGE', 'ressource');
        $this->view->assign('ressource', $ressource);
    }

    /**
     * Add historical table on private pages
     */
    protected function createHistoricalTableWidget(DatatableFactory $datatableFactory): ?Datatable
    {
        if ($this->name !== 'historical' && !empty($datatableFactory->getInstances())) {
            $table = $datatableFactory->createFromWidget(
                HistoricalDatatableWidget::class,
                [
                    'method'    => 'HISTORICAL_LIST',
                    'ressource' => ucfirst($this->name)
                ]
            );
            $this->view->assignWidget($table);

            return $table;
        }

        return null;
    }

    /**
     * Get all contexts for user
     */
    public function createContextSelectorWidget(FormFactory $formFactory): SelectElement
    {
        $contexts = [$this->auth->getContext()?->getIdcontext()];
        $options  = [];
        foreach ($contexts as $key => $val) {
            $options[$val['value']] = $key;
        }

        $form = $formFactory->create(uniqid());

        /** @var SelectElement $selector */
        $selector = $form->add(SelectElement::class);
        $selector->options($options)
            ->setName('contextSelector')
            ->setLabel(_('Network'))
            ->setBasicSelect()
            ->setValue($this->auth->getContext()?->idcontext ?? null);

        $this->view->assignWidget($selector);

        return $selector;
    }

    /**
     * Create loggout Button
     */
    public function createLoggoutButtonWidget(CommandFactory $commandFactory): Button
    {
        $button = new Button('logout');
        $button->addClass('logout');

        $command = $commandFactory->create(LogoutCommand::class);
        $button->on('click')
            ->selector('.logout')
            ->command($command);

        $button->enable();

        $this->view->assignWidget($button);

        return $button;
    }

    /**
     * Create main modal add
     */
    public function createMainModalAddWidget(ModalFactory $modalFactory, FormFactory $formFactory): ?Modal
    {
        $that = $this;

        return  $modalFactory->createThen(
            'create_' . $this->name,
            function ($modal) use ($formFactory, $that) {
                $form = $formFactory->createFromWidget(ucfirst($that->name), ['method' => 'add']);
                $modal->addForm($form);

                $that->view->assignWidget($modal);
            }
        );
    }

    /**
     * Create main modal edit
     */
    public function createMainModalEditWidget(ModalFactory $modalFactory, FormFactory $formFactory): ?Modal
    {
        $that = $this;

        return $modalFactory->createThen(
            'update_' . $this->name,
            function ($modal) use ($formFactory, $that) {
                $form = $formFactory->createFromWidget(ucfirst($that->name), ['method' => 'edit']);
                $modal->registerLockEvents($that->name);
                $modal->addForm($form);

                $that->view->assignWidget($modal);
            }
        );
    }

    /**
     * Create modal with main table inside
     */
    public function createMainModalWidget(
        ModalFactory $modalFactory,
        DatatableFactory $datatableFactory,
        CommandFactory $commandFactory,
        ?string $identifier = null
    ): ?Modal {
        $identifier ??= 'view_' . $this->name;
        $that = $this;

        return $modalFactory->createThen(
            $identifier,
            function ($modal) use ($datatableFactory, $commandFactory, $that) {
                $table = $that->createMainTableWidget($datatableFactory);
                $modal->addTable($table);

                $modal->on('shown.bs.modal')
                    ->command($commandFactory->create(RenderCommand::class, ['target' => $table]));

                $that->view->assignWidget($modal);
            }
        );
    }

    /**
     * Create main table
     */
    public function createMainTableWidget(DatatableFactory $datatableFactory): ?Datatable
    {
        $table = $datatableFactory->createFromWidget(
            ucfirst($this->name),
            [
                'method' => strtoupper($this->name) . '_LIST'
            ]
        );
        $datatableFactory->createButonFor($table, 'create_' . $this->name);
        $this->view->assignWidget($table);

        return $table;
    }

    /**
     * Create modal comment
     */
    public function createModalCommentWidget(
        ModalFactory $modalFactory,
        FormFactory $formFactory,
        CommandFactory $commandFactory
    ): ?Modal {
        return $modalFactory->createThen(
            'create_comment',
            function (Modal $modal) use ($formFactory, $commandFactory) {
                $form = $formFactory->createFromWidget('Utils', ['method' => 'comment']);
                $form->on('submit')
                    ->command($commandFactory->create(
                        SubmitCommand::class,
                        [
                            'target'   => $form,
                            'method'   => 'POST',
                            'endpoint' => '/api/comments'
                        ]
                    ));

                $modal->addForm($form);

                //$modal->on('click')
            //      ->target('modal_create_comment')
            //      ->command($commandFactory->create(ModalShowCommand::class, ["target" => $modal]));

                $this->view->assignWidget($modal);
            }
        );
    }



    /**
     * Create modal send message && register mailto://
     */
    public function createModalMessageWidget(
        ModalFactory $modalFactory,
        FormFactory $formFactory,
        CommandFactory $commandFactory
    ): ?Modal {
        $modal = $modalFactory->createThen(
            'create_inbox',
            function (Modal $modal) use ($formFactory, $commandFactory) {
                $form = $formFactory->createFromWidget('Inbox', ['method' => 'add']);
                $form->on('submit')
                    ->command($commandFactory->create(
                        SubmitCommand::class,
                        [
                            'target'   => $form,
                            'method'   => 'POST',
                            'endpoint' => '/api/inbox'
                        ]
                    ));

                $modal->addForm($form);
                $button = new Button('mailto', 'mailto');
                $button->addClass('mailto');

                $button->on('click')
                    ->selector('.mailto')
                    ->command($commandFactory->create(SendMailCommand::class, []));

                $button->enable();

                $this->view->assignWidget($modal);
            }
        );

        return $modal;
    }

    /**
     * Generate progress Bar
     */
    public function createProgressBarCountUsersWidget(
        SessionInterface $session,
        ProgressBarFactory $progressBarFactory
    ): ?ProgressBar {
        if ($this->auth->getUser()?->getIduser() === $this->auth->getContext()?->iduser) {
            $sessionContainer = $session->getContainer('CONTEXT');
            if (!$sessionContainer->has('ACTIVE_USERS')) {
                $sessionContainer->set(
                    'ACTIVE_USERS',
                    $this->auth->getContext()?->getRealActiveUser(new DateTime()) ?? 1
                );
            }

            $percentUser = $sessionContainer->get('REAL_COUNT') / $this->auth->getContext()?->nb_user * 100;
            $subtitle    = $sessionContainer->get('REAL_COUNT') . ' / ' . $this->auth->getContext()?->nb_user;
            $colorCss    = 'info';

            $users = $progressBarFactory->create(
                _('Users'),
                [
                    'percentUser' => $percentUser,
                    'colorCss'    => $colorCss,
                    'subtitle'    => $subtitle
                ]
            );

            $this->view->assignWidget($users);

            return $users;
        }

        return null;
    }

    /**
     * Error Action
     *
     * @return null
     */
    #[RouteAttribute(url:'/error', method:'GET', permission:['@public'], encoding:false, visible:false)]
    public function errorAction(int $errorCode = 404)
    {
        $this->setPageTemplate(BASE_DIR . "templates/pages/page.error.$errorCode.html");
        $this->view->assign('USERS', null);

        //Error code
        $this->view->assign('ERRORCODE', $errorCode);

        return null;
    }

    /**
     * Get page html template
     *
     * @PhpUnitGen\get("_pageTemplate")
     */
    public function getPageTemplate(): ?string
    {
        if (null === parent::getPageTemplate()) {
            if ($this->isInPopup()) {
                return 'templates/pages/page.popup.default.html';
            }

            return ($this->isPublic())
            ? BASE_DIR . 'templates/pages/page.public.default.html'
            : BASE_DIR . 'templates/pages/page.private.default.html';
        }

        return parent::getPageTemplate();
    }

    /**
     * Default Action
     *
     * @return null|void
     */
    public function indexAction()
    {
        if (!$this->auth->isAllowed() && $this->getParameter('id') === null) {
            return $this->errorAction(403);
        }

        //Create default edit form && modals
        $this->getDi()->callInside($this, 'createMainModalEditWidget');

        //Create default add form && modals
        $this->getDi()->callInside($this, 'createMainModalAddWidget');

        //Default table && buttons
        $this->getDi()->callInside($this, 'createMainTableWidget');
    }

    /**
     *  Render the view
     *
     * @param AssetsServiceInterface    $javascript
     * @param TransportServiceInterface $transport
     *
     * @return void
     */
    public function render(
        AssetsServiceInterface $javascript,
        TransportServiceInterface $transport
    ): self {
        $this->view->assign('TEMPLATE_PAGE', $this->getPageTemplate());

        if (!$this->isPublic()) {
            $this->initMenu();
            $this->getDi()->callInside($this, 'assignUsersToView');
            $this->getDi()->callInside($this, 'createHistoricalTableWidget');
            $this->getDi()->callInside($this, 'createModalCommentWidget');
            $this->getDi()->callInside($this, 'createModalMessageWidget');
            $this->getDi()->callInside($this, 'createLoggoutButtonWidget');
        }


        $this->view->assign('CSS', $javascript->getCss());
        /** @var CommandFactory $commandFactory */
        $commandFactory = $this->getDi()->singleton(CommandFactory::class);

        $this->view->assign('COMMANDS', json_encode(
            $commandFactory->getCommandFiles(),
            JSON_THROW_ON_ERROR
        ));

        /** @var JwtServiceInterface $jwt */
        $jwt = $this->getDi()->singleton(JwtServiceInterface::class);
        $jwt->getDecodedToken();

        $this->view->assign(
            'JWT',
            [
                'name'  => $jwt::TOKEN_NAME,
                'value' => $jwt->getToken()
            ]
        );

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return HttpResponseInterface
     */
    public function display(): HttpResponseInterface
    {
        if ($this->isInPopup()) {
            $this->response->setData('html', $this->view->fetch(BASE_DIR . 'templates/layout/layout.popup.html'));

            return $this->response;
        }

        $data = ($this->isPublic())
              ? $this->view->fetch(BASE_DIR . 'templates/layout/layout.public.html')
              : $this->view->fetch(BASE_DIR . 'templates/layout/layout.private.html');

        return  $this->response->setData('html', $data);
    }

    /**
     * Assign users to view
     *
     * @return void
     */
    protected function assignUsersToView(RepositoryFactoryInterface $repositoryFactory)
    {
        $repository = $repositoryFactory->table('user');
        $users      = $repository->getList(orderParam:'first_name', limit:null, order: $repository::DESC);

        $this->view->assign('USERS', $users);
    }

    /**
     * Call ModelFactory to get Model element
     *
     * @PhpUnitGen\assertInstanceOf("ModelInterface::class")
     */
    protected function getModel(string $identifier): ?ModelInterface
    {
        return $this->getDi()->singleton(ModelServiceInterface::class)->create($identifier);
    }

    /**
     * Return default Model
     */
    protected function getModelBase(): ?ModelInterface
    {
        return null;
    }
}
