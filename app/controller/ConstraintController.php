<?php namespace app\controller;

/**
 * Class ConstraintController
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class ConstraintController extends ControllerBase
{
    protected $name = 'constraint';

    #[\core\routing\RouteAttribute(url: '/constraint', method: 'GET', permission: ['read:constraint'], encoding: false, displayName: 'constraint')]
    public function indexAction()
    {
        parent::indexAction();
    }

    #[\core\routing\RouteAttribute(url: '/constraint/:id', method: 'GET', permission: ['read:constraint'], encoding: false)]
    public function byIdAction(\core\model\ModelServiceInterface $modelFactory, \core\html\bar\BarFactory $barFactory)
    {
        parent::byIdAction($modelFactory, $barFactory);
    }

    protected function getModelBase(): \app\model\ConstraintModel
    {
        return $this->getModel("Constraint");
    }
}
