<?php namespace app\controller;

/**
 * Class VatController
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class VatController extends ControllerBase
{
    protected $name = 'vat';

    #[\core\routing\RouteAttribute(url: '/vat', method: 'GET', permission: ['read:vat'], encoding: false, displayName: 'vat')]
    public function indexAction()
    {
        parent::indexAction();
    }

    #[\core\routing\RouteAttribute(url: '/vat/:id', method: 'GET', permission: ['read:vat'], encoding: false)]
    public function byIdAction(\core\model\ModelServiceInterface $modelFactory, \core\html\bar\BarFactory $barFactory)
    {
        parent::byIdAction($modelFactory, $barFactory);
    }

    protected function getModelBase(): \app\model\VatModel
    {
        return $this->getModel("Vat");
    }
}
