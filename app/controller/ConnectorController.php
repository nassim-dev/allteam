<?php namespace app\controller;

/**
 * Class ConnectorController
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class ConnectorController extends ControllerBase
{
    protected $name = 'connector';

    #[\core\routing\RouteAttribute(url: '/connector', method: 'GET', permission: ['read:connector'], encoding: false, displayName: 'connector')]
    public function indexAction()
    {
        parent::indexAction();
    }

    #[\core\routing\RouteAttribute(url: '/connector/:id', method: 'GET', permission: ['read:connector'], encoding: false)]
    public function byIdAction(\core\model\ModelServiceInterface $modelFactory, \core\html\bar\BarFactory $barFactory)
    {
        parent::byIdAction($modelFactory, $barFactory);
    }

    protected function getModelBase(): \app\model\ConnectorModel
    {
        return $this->getModel("Connector");
    }
}
