<?php namespace app\controller;

/**
 * Class AddressController
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class AddressController extends ControllerBase
{
    protected $name = 'address';

    #[\core\routing\RouteAttribute(url: '/address', method: 'GET', permission: ['read:address'], encoding: false, displayName: 'address')]
    public function indexAction()
    {
        parent::indexAction();
    }

    #[\core\routing\RouteAttribute(url: '/address/:id', method: 'GET', permission: ['read:address'], encoding: false)]
    public function byIdAction(\core\model\ModelServiceInterface $modelFactory, \core\html\bar\BarFactory $barFactory)
    {
        parent::byIdAction($modelFactory, $barFactory);
    }

    protected function getModelBase(): \app\model\AddressModel
    {
        return $this->getModel("Address");
    }
}
