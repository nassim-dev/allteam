<?php

namespace app\controller;

use core\html\button\Button;
use core\html\calendar\Calendar;
use core\html\calendar\CalendarFactory;
use core\html\calendar\CalendarHelper;
use core\html\CommandFactory;
use core\html\form\command\PopulateCommand;
use core\html\form\command\SubmitCommand;
use core\html\form\FormFactory;
use core\html\javascript\command\EvalCommand;
use core\html\modal\command\ModalShowCommand;
use core\html\modal\Modal;
use core\html\modal\ModalFactory;
use core\routing\RouteAttribute;

/**
 * Class LoginController
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class LoginController extends ControllerBase
{
    /**
     * @var string
     */
    protected $name = 'login';

    /**
     * Is that a public controller
     *
     * @var boolean
     */
    protected $_public = true;

    /**
     * Create availabilie calendar
     */
    public function createAvailabilitieCalendarWidget(CalendarFactory $calendarFactory, CommandFactory $commandFactory): Calendar
    {
        $calendar = $calendarFactory->create('availabilitie', ['name' => _('Availabilities')]);

        $calendar->addConfig('urls')
            ->addParameters(['/api/Availabilitie/load']);
        $calendar->addConfig('editable')
            ->value(true);
        $calendar->addConfig('selectable')
            ->value(true);

        $dayClick    = CalendarHelper::getDayClickEvent('create_availabilitie');
        $editEvent   = CalendarHelper::getEditEvent();
        $eventRender = CalendarHelper::getRenderEvent();

        $calendar->on('eventRender')
            ->command($commandFactory->create(EvalCommand::class, ['callback' => $eventRender]));
        $calendar->on('eventResize')
            ->command($commandFactory->create(EvalCommand::class, ['callback' => $editEvent]));
        $calendar->on('eventDrop')
            ->command($commandFactory->create(EvalCommand::class, ['callback' => $editEvent]));
        $calendar->on('select')
            ->command($commandFactory->create(EvalCommand::class, ['callback' => $dayClick]));

        $this->view->assignWidget($calendar);

        return $calendar;
    }

    /**
     * Create login form && modal
     */
    public function createModalLoginWidget(ModalFactory $modalFactory, FormFactory $formFactory, CommandFactory $commandFactory): ?Modal
    {
        $autoshowLogin = ($this->request->getPage() === 'login' && !$this->auth->isConnected()) ? Modal::AUTOSHOW : Modal::DONTSHOW;
        $modal         = $modalFactory->create('panel', ['autoshow' => $autoshowLogin]);
        if (null !== $modal) {
            $modal->on('click')
                ->selector('.panel-button')
                ->target('#modal_panel')
                ->command($commandFactory->create(ModalShowCommand::class, ['target' => $modal]));

            $form = $formFactory->createFromWidget('User', ['method' => 'login']);

            $form->on('submit')
                ->command($commandFactory->create(SubmitCommand::class, ['target' => $form, 'method' => 'POST', 'endpoint' => '/api/user/login']));

            $modal->addForm($form);

            $this->view->assignWidget($modal);
        }

        return $modal;
    }

    /**
     * Create password lost form && modal
     */
    public function createModalPasswordWidget(ModalFactory $modalFactory, FormFactory $formFactory, CommandFactory $commandFactory): ?Modal
    {
        $modal = $modalFactory->create('password');
        if (null !== $modal) {
            $modal->on('click')
                ->selector('.password-button')
                ->command($commandFactory->create(ModalShowCommand::class, ['target' => $modal]));

            $form = $formFactory->createFromWidget('User', ['method' => 'password']);

            $form->on('submit')
                ->command($commandFactory->create(SubmitCommand::class, ['target' => $form, 'method' => 'POST', 'endpoint' => '/api/user/password']));

            $modal->addForm($form);

            $this->view->assignWidget($modal);
        }

        return $modal;
    }


    /**
     * Default Action
     *
     * @return void
     */
    #[RouteAttribute(url:'/login', method:'GET', permission:['@public'], encoding:false, visible:false)]
    public function indexAction()
    {
        $this->getDi()->callInside($this, 'createModalLoginWidget');
        $this->getDi()->callInside($this, 'createModalPasswordWidget');

        if ($this->auth->isConnected()) {
            $calendar = $this->getDi()->callInside($this, 'createAvailabilitieCalendarWidget');

            $this->getDi()->callInside($this, 'createModalProfileWidget', ['calendar' => $calendar]);
            //$this->getDi()->call(AvailabilitieController::class, 'createMainModalEditWidget');
            //$this->getDi()->call(AvailabilitieController::class, 'createMainModalAddWidget');
        }
    }

    /**
     * Create profile edit form && modal
     */
    private function createModalProfileWidget(ModalFactory $modalFactory, FormFactory $formFactory, CommandFactory $commandFactory, ?Calendar $calendar = null): ?Modal
    {
        $modal = $modalFactory->create('update_user');

        if (null !== $modal) {
            $modal->registerLockEvents('user');

            $form = $formFactory->createFromWidget(
                'User',
                [
                    'method'    => 'edit',
                    'type'      => $this->auth->getUser()?->type ?? 'default',
                    'idcontext' => $this->auth->getContext()?->getIdcontext(),
                    'admin'     => false
                ]
            );
            $form->setValue(_('Edit'))
                ->setTitle(_("Edit user's profil"))
                ->setName('update_user')
                ->apiMethod('PUT');

            $form->on('submit')
                ->command($commandFactory->create(SubmitCommand::class, ['target' => $form, 'method' => 'PUT', 'endpoint' => '/api/user/user']));

            $form->addConfig('popParameters')->addParameters(['action' => 'profile']);
            if (null !== $calendar) {
                $form->addCalendar($calendar);
            }

            $this->registerEditProfileEvent($formFactory, $commandFactory);

            $modal->addForm($form);
        }

        return $modal;
    }

    /**
     * Create event for button with css class .profile-edit
     *
     * @return void
     */
    private function registerEditProfileEvent(FormFactory $formFactory, CommandFactory $commandFactory)
    {
        $form   = $formFactory->get('update_user');
        $button = new Button(_('Edit'), uniqid());

        $button->on('click')
            ->selector('.profile-edit')
            ->command($commandFactory->create(PopulateCommand::class, ['target' => $form, 'indexKey' => 'iduser']))
            ->addParameters(['parentNode' => 'header']);
    }
}
