<?php namespace app\controller;

/**
 * Class ConfigurationController
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class ConfigurationController extends ControllerBase
{
    protected $name = 'configuration';

    #[\core\routing\RouteAttribute(url: '/configuration', method: 'GET', permission: ['read:configuration'], encoding: false, displayName: 'configuration')]
    public function indexAction()
    {
        parent::indexAction();
    }

    #[\core\routing\RouteAttribute(url: '/configuration/:id', method: 'GET', permission: ['read:configuration'], encoding: false)]
    public function byIdAction(\core\model\ModelServiceInterface $modelFactory, \core\html\bar\BarFactory $barFactory)
    {
        parent::byIdAction($modelFactory, $barFactory);
    }

    protected function getModelBase(): \app\model\ConfigurationModel
    {
        return $this->getModel("Configuration");
    }
}
