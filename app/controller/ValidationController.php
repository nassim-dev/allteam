<?php

namespace app\controller;

use app\model\ValidationModel;
use core\html\CommandFactory;
use core\html\form\command\SubmitCommand;
use core\html\form\FormFactory;
use core\html\modal\Modal;
use core\html\modal\ModalFactory;
use core\notifications\NotificationServiceInterface;
use core\routing\RouteAttribute;
use core\secure\authentification\AuthServiceInterface;
use core\secure\user\UserInterface;
use core\utils\Utils;

/**
 * Class ValidationController
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class ValidationController extends ControllerBase
{
    /**
     * @var string
     */
    protected $name = 'validation';

    /**
     * Is that a public controller
     *
     * @var boolean
     */
    protected $_public = true;

    private mixed $_tmpUser = null;

    /**
     * @param $user
     *
     * @return mixed
     */
    public function createValidationModalWidget(ModalFactory $modalFactory, FormFactory $formFactory, CommandFactory $commandFactory, ?UserInterface $user = null): ?Modal
    {
        //On récupère les infos déjà enregistrées

        $autoshow = ($this->request->getPage() === 'validation' && !$this->auth->isConnected()) ? Modal::AUTOSHOW : Modal::DONTSHOW;
        $modal    = $modalFactory->create('create_validation', ['autoshow' => $autoshow]);
        if (null !== $modal) {
            $infos            = $user->getVarsKeys();
            $infos['idtrade'] = $user->findRelated('trade');

            //Création du formualaire
            $fieldPassword = ($user->getPassword() === Utils::hash($this->getParameter('key')));

            $form = $formFactory->createFromWidget(
                'User',
                [
                    'method'     => 'edit',
                    'type'       => $user->type,
                    'idcontext'  => $user->idcontext,
                    'admin'      => false,
                    'pass'       => $fieldPassword,
                    'validation' => $this->getParameter('key')
                ]
            );

            unset($infos['password']);

            $form->setValue(_('Validate'))
                ->setTitle('Vérifier mes informations')
                ->setName('create_validation')
                ->apiMethod('PUT')
                ->setAction('action-validation')
                ->pre($infos);

            $form->on('submit')
                ->command($commandFactory->create(SubmitCommand::class, ['target' => $form, 'method' => 'PUT', 'endpoint' => '/api/user/validation']));

            $modal->addForm($form);
        }

        return $modal;
    }

    /**
     * Default Action
     *
     * @return void
     */
    #[RouteAttribute(url:'/validation/:key', method:'GET', permission:['@public'], encoding:false, visible:false)]
    public function indexAction()
    {
        /** @var NotificationServiceInterface $notifier */
        $notifier = $this->getDi()->singleton(NotificationServiceInterface::class);

        /** @var AuthServiceInterface $auth */
        $auth = $this->getDi()->singleton(AuthServiceInterface::class);

        if ($this->getParameter('key') === null) {
            header('Location:/subscribe');
        }

        //Check if user exist
        $this->_tmpUser = $this->getModelBase()->userExist(['validation' => $this->getParameter('key')], 'validation');
        if (!$this->_tmpUser) {
            $notifier->notify(_('The link that you used is incorrect, or the user does not exist.'), $notifier::WARNING, '/subscribe');

            return;
        }

        $auth->getAuth()->setEmail($this->_tmpUser->getEmail());
        $auth->getAuth()->setPassword($this->_tmpUser->getPassword());
        if ($this->_tmpUser->hasValidate() && $auth->login($notifier)) {
            $notifier->notify(_('Authentication complete.'), $notifier::SUCCESS, '/');

            return;
        }

        $this->getDi()->callInside($this, 'createValidationModalWidget', ['user' => $this->_tmpUser]);
    }

    /**
     * Return default Model
     */
    protected function getModelBase(): ValidationModel
    {
        return $this->getModel('Validation');
    }
}
