<?php

namespace app\controller;

use core\routing\RouteAttribute;

/**
 * Class LogoutController
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class LogoutController extends ControllerBase
{
    /**
     * @var string
     */
    protected $name = 'logout';

    /**
     * Is that a public controller
     *
     * @var boolean
     */
    protected $_public = true;

    /**
     * Default Action
     *
     * @return void
     */
    #[RouteAttribute(url:'/logout', method:'GET', permission:['@public'], encoding:false, visible:false)]
    public function indexAction()
    {
        $this->getDi()->call($this->auth, 'logout');
    }
}
