<?php

namespace app\controller;

use app\model\SubscribeModel;
use core\html\CommandFactory;
use core\html\form\FormFactory;
use core\html\modal\command\ModalShowCommand;
use core\html\modal\Modal;
use core\html\modal\ModalFactory;
use core\routing\RouteAttribute;

/**
 * Class SubscribeController
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class SubscribeController extends ControllerBase
{
    /**
     * @var string
     */
    protected $name = 'subscribe';

    /**
     * Is that a public controller
     *
     * @var boolean
     */
    protected $_public = true;

    /**
     * @return mixed
     */
    public function createModalAddSubscribeWidget(ModalFactory $modalFactory, FormFactory $formFactory, CommandFactory $commandFactory): ?Modal
    {
        $autoshow = ($this->request->getPage() === 'subscribe');

        $modal = $modalFactory->createThen('create_subscribe', function (Modal $modal) use ($formFactory, $commandFactory): void {
            $form = $formFactory->createFromWidget('User', ['method' => 'add', 'type' => 'Employee', 'idcontext' => 'new']);
            $modal->addForm($form);
            //$modal->addText(
            //    title: _('Registration'),
            //    content: _('Ask for a demo to discover Allteam !')
            //);

            $modal->on('click')
                ->command($commandFactory->create(ModalShowCommand::class, ['target' => $modal]));

            $this->view->assignWidget($modal);
        }, ['autoshow' => $autoshow]);

        return $modal;
    }

    /**
     * Default Action
     *
     * @return void
     */
    #[RouteAttribute(url:'/subscribe', method:'GET', permission:['@public'], encoding:false, visible:false)]
    public function indexAction()
    {
        $this->getDi()->call($this, 'createModalAddSubscribeWidget');
    }

    /**
     * Return default Model
     */
    protected function getModelBase(): SubscribeModel
    {
        return $this->getModel('Subscribe');
    }
}
