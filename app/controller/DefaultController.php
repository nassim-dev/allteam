<?php

namespace app\controller;

use app\model\DefaultModel;
use core\html\carousel\Carousel;
use core\html\carousel\CarouselFactory;
use core\routing\RouteAttribute;

/**
 * Class DefaultController
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class DefaultController extends ControllerBase
{
    /**
     * @var string
     */
    protected $name = 'default';

    /**
     * Is that a public controller
     *
     * @var boolean
     */
    protected $_public = true;

    /**
     * Default Action
     *
     * @return void
     */
    #[RouteAttribute(url:'/', method:'GET', permission:['@public'], encoding:false, visible:false)]
    public function indexAction()
    {
        $this->getDi()->call(LoginController::class, 'indexAction');
        $this->getDi()->call(SubscribeController::class, 'indexAction');
        $this->getDi()->call($this, 'createHomeCarouselWidget');
    }

    /**
     * Return default Model
     */
    protected function getModelBase(): DefaultModel
    {
        return $this->getModel('Default');
    }

    /**
     * Create Homepage Carousel
     */
    public function createHomeCarouselWidget(CarouselFactory $carouselFactory): ?Carousel
    {
        return $carouselFactory->createThen('panel', function (Carousel $carousel): void {
            $isActive = true;
            $carousel->addSlide('/images/iphone_1.png', 'O', 'AllTeam - Outil gestion de projets événementiel', $isActive);
            $carousel->addSlide('/images/iphone_2.png', 'O', 'AllTeam - Gestion de conduites techniques');
            $carousel->addSlide('/images/iphone_3.png', 'O', 'AllTeam - Gestion de stock de matériel');
            $carousel->addSlide('/images/iphone_4.png', 'O', 'AllTeam - Gestion de planning et roadmap');

            $this->view->disposeWidget($carousel, 'home_carousel_container');
        });
    }
}
