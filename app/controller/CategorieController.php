<?php namespace app\controller;

/**
 * Class CategorieController
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class CategorieController extends ControllerBase
{
    protected $name = 'categorie';

    #[\core\routing\RouteAttribute(url: '/categorie', method: 'GET', permission: ['read:categorie'], encoding: false, displayName: 'categorie')]
    public function indexAction()
    {
        parent::indexAction();
    }

    #[\core\routing\RouteAttribute(url: '/categorie/:id', method: 'GET', permission: ['read:categorie'], encoding: false)]
    public function byIdAction(\core\model\ModelServiceInterface $modelFactory, \core\html\bar\BarFactory $barFactory)
    {
        parent::byIdAction($modelFactory, $barFactory);
    }

    protected function getModelBase(): \app\model\CategorieModel
    {
        return $this->getModel("Categorie");
    }
}
