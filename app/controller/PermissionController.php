<?php namespace app\controller;

/**
 * Class PermissionController
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class PermissionController extends ControllerBase
{
    protected $name = 'permission';

    #[\core\routing\RouteAttribute(url: '/permission', method: 'GET', permission: ['read:permission'], encoding: false, displayName: 'permission')]
    public function indexAction()
    {
        parent::indexAction();
    }

    #[\core\routing\RouteAttribute(url: '/permission/:id', method: 'GET', permission: ['read:permission'], encoding: false)]
    public function byIdAction(\core\model\ModelServiceInterface $modelFactory, \core\html\bar\BarFactory $barFactory)
    {
        parent::byIdAction($modelFactory, $barFactory);
    }

    protected function getModelBase(): \app\model\PermissionModel
    {
        return $this->getModel("Permission");
    }
}
