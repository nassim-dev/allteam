<?php namespace app\controller;

/**
 * Class SocialController
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class SocialController extends ControllerBase
{
    protected $name = 'social';

    #[\core\routing\RouteAttribute(url: '/social', method: 'GET', permission: ['read:social'], encoding: false, displayName: 'social')]
    public function indexAction()
    {
        if (!$this->auth->isAllowed() && $this->getParameter('id') === null) {
            return $this->errorAction(403);
        }

        //Create default edit form && modals
        $this->getDi()->callInside($this, 'createMainModalEditWidget');

        //Create default add form && modals
        $this->getDi()->callInside($this, 'createMainModalAddWidget');


        //Default table && buttons
        $table = $this->getDi()->callInside($this, 'createMainTableWidget');

        /**
         * Generate pills table
         */
        $pillTable = $this->getPillsTable();
        $pillTable->setTitle('');

        $pill = $pillTable->addPill('table', _('Table'));
        $pill->appendChild($table);
        $pill->setDefault(true);

        $pillTable->setDefault($pill);


        $timeline = $this->createTimeline();
        $pill     = $pillTable->addPill('timeline', _('Timeline'));
        $pill->appendChild($timeline);

        $calendar = $this->getDi()->call($this, 'createCalendarWidget');
        $pill     = $pillTable->addPill('calendar', _('Calendar'));
        $pill->appendChild($calendar);
    }

    protected function getModelBase(): \app\model\SocialModel
    {
        return $this->getModel('Social');
    }

    public function createTimeline(): \core\html\timeline\Timeline
    {
        /** @var \core\html\timeline\TimelineFactory $timelineFactory */
        $timelineFactory = $this->getDi()->singleton(\core\html\timeline\TimelineFactory::class);

        $timeline = $timelineFactory->create('test');
        $timeline->addDateSelector()
            ->addFitSelector()
            ->addTodaySelector();

        $link = new \core\routing\Link(\app\api\controller\SocialController::class, 'getAllAction');
        $timeline->addEndpoint('readData', $link);

        $link = new \core\routing\Link(\app\api\controller\SocialController::class, 'createAction');
        $timeline->addEndpoint('createData', $link);

        $link = new \core\routing\Link(\app\api\controller\SocialController::class, 'updateAction');
        $timeline->addEndpoint('updateData', $link);

        $this->view->assignWidget($timeline);

        return $timeline;
    }

    public function createCalendarWidget(
        \core\html\calendar\CalendarFactory $calendarFactory,
        \core\html\CommandFactory $commandFactory
    ): \core\html\calendar\Calendar {
        $calendar = $calendarFactory->create('social', ['name' => 'social']);

        $calendar->setTitle(_('Planning'));

        $link = new \core\routing\Link(\app\api\controller\SocialController::class, 'getAllAction');
        $calendar->addEndpoint('readData', $link);

        $link = new \core\routing\Link(\app\api\controller\SocialController::class, 'getAllAction');
        $calendar->addEndpoint('readRessources', $link);

        $link = new \core\routing\Link(\app\api\controller\SocialController::class, 'createAction');
        $calendar->addEndpoint('createData', $link);

        $link = new \core\routing\Link(\app\api\controller\SocialController::class, 'updateAction');
        $calendar->addEndpoint('updateData', $link);

        $calendar->addConfig('refetchResourcesOnNavigate')
            ->value(true);
        $calendar->addConfig('selectable')
            ->value(true);
        //$calendar->addConfig("defaultView")->value('agendaWeek');

        $eventRender = \core\html\calendar\CalendarHelper::getSimpleRenderEvent();
        $calendar->on('eventRender')->command($commandFactory->create(\core\html\javascript\command\EvalCommand::class, ['callback' => $eventRender]));

        $calendar->addExportButton();

        $this->view->assignWidget($calendar);

        return $calendar;
    }

    #[\core\routing\RouteAttribute(url: '/social/:id', method: 'GET', permission: ['read:social'], encoding: false)]
    public function byIdAction(\core\model\ModelServiceInterface $modelFactory, \core\html\bar\BarFactory $barFactory)
    {
        parent::byIdAction($modelFactory, $barFactory);
    }
}
