<?php namespace app\model;

/**
 * Class Compagny_opening_timeModel
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class Compagny_opening_timeModel extends ModelBase
{
    protected ?string $tableName = 'compagny_opening_time';
    protected ?string $tableNameUcFirst = 'Compagny_opening_time';
    protected ?string $tableNameUpper = 'COMPAGNY_OPENING_TIME';
}
