<?php namespace app\model;

/**
 * Class Knowledge_typeModel
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class Knowledge_typeModel extends ModelBase
{
    protected ?string $tableName = 'knowledge_type';
    protected ?string $tableNameUcFirst = 'Knowledge_type';
    protected ?string $tableNameUpper = 'KNOWLEDGE_TYPE';
}
