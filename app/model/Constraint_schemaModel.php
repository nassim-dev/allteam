<?php namespace app\model;

/**
 * Class Constraint_schemaModel
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class Constraint_schemaModel extends ModelBase
{
    protected ?string $tableName = 'constraint_schema';
    protected ?string $tableNameUcFirst = 'Constraint_schema';
    protected ?string $tableNameUpper = 'CONSTRAINT_SCHEMA';
}
