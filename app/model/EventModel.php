<?php namespace app\model;

/**
 * Class EventModel
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class EventModel extends ModelBase
{
    protected ?string $tableName = 'event';
    protected ?string $tableNameUcFirst = 'Event';
    protected ?string $tableNameUpper = 'EVENT';
}
