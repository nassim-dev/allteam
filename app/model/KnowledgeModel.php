<?php namespace app\model;

/**
 * Class KnowledgeModel
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class KnowledgeModel extends ModelBase
{
    protected ?string $tableName = 'knowledge';
    protected ?string $tableNameUcFirst = 'Knowledge';
    protected ?string $tableNameUpper = 'KNOWLEDGE';
}
