<?php

namespace app\model;

use core\model\AbstractModel;

/**
 * Class ModelBase
 *
 * Parent class for model
 *
 * @category  Parent class for model
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
abstract class ModelBase extends AbstractModel
{
    /**
     * Sigle entitie view
     */
    public function byidAction(): bool|\core\database\mysql\lib\nette\CustomNetteSelection
    {
        if (null !== $this->getParameter('id')) {
            return false;
        }

        return $this->repositoryFactory->table($this->getTableName())
            ->getById($this->getParameter('id'));
    }
}
