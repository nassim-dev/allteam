<?php namespace app\model;

/**
 * Class HistoricalModel
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class HistoricalModel extends ModelBase
{
    protected ?string $tableName = 'historical';
    protected ?string $tableNameUcFirst = 'Historical';
    protected ?string $tableNameUpper = 'HISTORICAL';
}
