<?php namespace app\model;

/**
 * Class Place_opening_timeModel
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class Place_opening_timeModel extends ModelBase
{
    protected ?string $tableName = 'place_opening_time';
    protected ?string $tableNameUcFirst = 'Place_opening_time';
    protected ?string $tableNameUpper = 'PLACE_OPENING_TIME';
}
