<?php

namespace app\model;

use core\secure\user\UserInterface;

/**
 * Class ValidationModel
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class ValidationModel extends ModelBase
{
    public function userExist(array $formVars, string $key = 'email'): ?UserInterface
    {
        return  $this->repositoryFactory->table('user')->getByParams([$key => $formVars[$key]])->fetch() ?? null;
    }
}
