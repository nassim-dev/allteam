<?php

namespace app\model;

use core\database\mysql\lib\nette\CustomNetteSelection;
use core\utils\Utils;
use DateTimeInterface;

/**
 * Class PanelModel
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class PanelModel extends ModelBase
{
    /**
     * Get Last Alertes
     */
    public function getLastAlertes(): ?CustomNetteSelection
    {
        return $this->getDi()->call($this->repositoryFactory->table('alerte'), 'getLasts');
    }

    /**
     * Get Last Historical
     */
    public function getLastHistoricals(): CustomNetteSelection | array
    {
        /** @var Historical $historical */
        $historicals = $this->getDi()->call($this->repositoryFactory->table('historical'), 'getLasts');
        foreach ($historicals as $historical) {
            $historical->datetime = Utils::formatDate($historical->getDatetime());
        }

        return $historicals;
    }

    /**
     * Get material needed at specific date
     */
    public function getMaterialListNeededAt(DateTimeInterface $date): CustomNetteSelection
    {
        return $this->repositoryFactory->table('stock')->getNeeds($date);
    }

    /**
     * Return message count
     */
    public function getNewMessageCount(): int
    {
        $newMessages = $this->repositoryFactory->table('inbox')->getNewMessage($this->auth->getUser()?->getIduser());

        return is_countable($newMessages) ? count($newMessages) : 0;
    }

    /**
     * return task count
     */
    public function getNewTaskCount(): int
    {
        $newTasks = $this->repositoryFactory->table('task')->getNewTask($this->auth->getUser()?->getIduser());

        return is_countable($newTasks) ? count($newTasks) : 0;
    }

    /**
     * Return performance not passed && validate
     */
    public function getPerformanceFutureCount(): int
    {
        $events = $this->repositoryFactory->table('performance')->getEventIn();

        return is_countable($events) ? count($events) : 0;
    }

    /**
     * Return performance passed && not finazlized
     */
    public function getPerformanceTofinalizeCount(): int
    {
        $events = $this->repositoryFactory->table('performance')->getEventOut();

        return is_countable($events) ? count($events) : 0;
    }

    /**
     * Return url for calendar
     * @return array{events: string[], ressources: string[]}
     */
    public function getUrls(): array
    {
        return [
            'events' => [
                '/api/Transport/load',
                '/api/Task/load',
                '/api/Performance/load'],
            'ressources' => ['/api/Task/room']
        ];
    }

    /**
     * Return user todo list
     */
    public function getUserTodo(): CustomNetteSelection
    {
        return $this->repositoryFactory->table('todo')->getByParams(['iduser' => [$this->auth->getUser()?->getIduser(), -1]], 'due_date', self::ASC);
    }
}
