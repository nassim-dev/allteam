<?php namespace app\model;

/**
 * Class CompagnyModel
 *
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class CompagnyModel extends ModelBase
{
    protected ?string $tableName = 'compagny';
    protected ?string $tableNameUcFirst = 'Compagny';
    protected ?string $tableNameUpper = 'COMPAGNY';
}
