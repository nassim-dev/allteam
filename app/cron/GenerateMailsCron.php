<?php

namespace app\cron;

use app\entities\context\Context;
use app\widgets\mail\MailTemplates;
use core\database\RepositoryFactoryInterface;
use core\logger\LogServiceInterface;
use core\tasks\CronInterface;
use core\utils\Utils;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class GenerateMailsCron implements CronInterface
{
    public function __construct(
        private LogServiceInterface $logService,
        private RepositoryFactoryInterface $repositoryFactory,
        private MailTemplates $mailTemplates
    ) {
    }

    public function everyMinute()
    {
    }

    public function everyHour()
    {
    }

    public function everyDay()
    {
        //Do something
    }

    public function everyWeek()
    {
        /** @var Context[] $contexts */
        $contexts = $this->repositoryFactory->table('context')
            ->getList();
        foreach ($contexts as $context) {
            $this->logService->dump([
                'data'    => 'Clean tmp files',
                'logfile' => CRON_LOGFILE
            ]);

            if ($context->idcontext !== 0) {
                $today = date('Y-m-d');


                /** @var User[] $users */
                $users = $this->repositoryFactory->table('user')
                    ->getList();
                foreach ($users as $user) {
                    //Recall validation mail
                    $testDate = date('Y-m-d', strtotime('+15 day', strtotime(Utils::formatDate($user->last_connection, 'Y-m-d'))));

                    if ($user->hash_validation === null) {
                        $mail = $this->mailtemplates->mailConfirmSubscribe($user);
                        $mail->save();
                        $this->logService->dump([
                            'data'    => 'Send Invitation to ' . $user->email,
                            'logfile' => MAIL_LOGFILE
                        ]);
                    } elseif ($today >= $testDate) {
                        $mail = $this->mailtemplates->mailComeBack($user);
                        $mail->save();

                        $this->logService->dump([
                            'data'    => 'Send Invitation to ' . $user->email,
                            'logfile' => MAIL_LOGFILE
                        ]);
                    }
                }
            }
        }
    }

    public function everyMonth()
    {
    }
}
