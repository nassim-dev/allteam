<?php

namespace app\cron;

use core\config\Config;
use core\tasks\CronInterface;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class SecurityCron implements CronInterface
{
    public function __construct(private Config $config)
    {
    }

    public function everyMinute()
    {
        //Do something
    }

    public function everyHour()
    {
        $this->config->saveInFile(BASE_DIR . 'config/env.neon', 'SECURITY.JSONRPC.KEY', uniqid(mt_rand(), true));
    }

    public function everyDay()
    {
        //Do something
    }

    public function everyWeek()
    {
        //Do something
    }

    public function everyMonth()
    {
        //Do something
    }
}
