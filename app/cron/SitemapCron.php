<?php

namespace app\cron;

use core\config\Config;
use core\html\Sitemap;
use core\logger\LogServiceInterface;
use core\tasks\CronInterface;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class SitemapCron implements CronInterface
{
    public function __construct(
        private LogServiceInterface $logService,
        private Config $config
    ) {
    }

    public function everyMinute()
    {
        //Do something
    }

    public function everyHour()
    {
        if (!is_dir(BASE_DIR . $this->config->find('PHP.ROOT_PUBLIC_FILES') . '../sitemap/')) {
            mkdir(BASE_DIR . $this->config->find('PHP.ROOT_PUBLIC_FILES') . '../sitemap/', 0755, true);
        }

        $baseUrl = $this->config->find('PHP.HTTP_PROTOCOL') . '://' . $this->config->find('PHP.SERVER_NAME');

        /** @var Sitemap $sitemap */
        $sitemap = $this->di->singleton(Sitemap::class, ['baseUrl' => $baseUrl]);
        $sitemap->setPath(BASE_DIR . $this->config->find('PHP.ROOT_PUBLIC_FILES') . '../sitemap/');
        $sitemap->addItem('/', '1.0', 'monthly', 'Today');
        $sitemap->addItem('/subscribe', '0.9', 'monthly', 'Today');
        $sitemap->addItem('/login', '0.9', 'monthly', 'Today');
        $sitemap->addItem('/', '0.5', 'monthly', 'Today');
        $sitemap->createSitemapIndex("$baseUrl/sitemap/", 'Today');


        $this->logService->dump(
            [
                'data'    => "Sitemap generated at $baseUrl/sitemap/",
                'logfile' => CRON_LOGFILE
            ]
        );
    }

    public function everyDay()
    {
        //Do something
    }

    public function everyWeek()
    {
        //Do something
    }

    public function everyMonth()
    {
        //Do something
    }
}
