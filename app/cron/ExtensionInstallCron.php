<?php

namespace app\cron;

use core\extensions\ExtensionInstallerServiceInterface;
use core\extensions\ExtensionInterface;
use core\tasks\CronInterface;
use Nette\Neon\Decoder;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class ExtensionInstallCron implements CronInterface
{
    public function __construct(
        private ExtensionInstallerServiceInterface $installer,
        private Decoder $decoder
    ) {
    }

    public function everyMinute()
    {
        $manifest = $this->decoder->decode(file_get_contents('nette.safe://' . BASE_DIR . 'extensions/manifest.neon'));
        $manifest = (is_array($manifest)) ? $manifest : [];
        foreach ($manifest as $config) {
            /** @var ExtensionInterface $extension */
            $extension = $this->di->singleton($config['class']);
            if (!$extension->isInstalled()) {
                $extension = $this->installer->install($extension, $cli);
            }
        }
    }

    public function everyHour()
    {
    }

    public function everyDay()
    {
        //Do something
    }

    public function everyWeek()
    {
        //Do something
    }

    public function everyMonth()
    {
        //Do something
    }
}
