<?php

namespace app\cron;

use core\services\reminder\Reminder;
use core\services\reminder\ReminderManager;
use core\database\RepositoryFactoryInterface;
use core\tasks\CronInterface;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class ReminderCron implements CronInterface
{
    public function __construct(
        private ReminderManager $reminderManager,
        private  RepositoryFactoryInterface $repository
    ) {
    }

    public function everyMinute()
    {
        //Do something
    }

    public function everyHour()
    {
        $users = $this->repository->table('user')
            ->getList();

        foreach ($users as $user) {
            /** @var Reminder $reminder  */
            $reminder = $this->di->make(Reminder::class, ['user' => $user]);
            $this->reminderManager->process($reminder);
        }
    }

    public function everyDay()
    {
        //Do something
    }

    public function everyWeek()
    {
        //Do something
    }

    public function everyMonth()
    {
        //Do something
    }
}
