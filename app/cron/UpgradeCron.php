<?php

namespace app\cron;

use app\tasks\PatchTask;
use core\tasks\CronInterface;
use core\tasks\AsyncJob;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class UpgradeCron implements CronInterface
{
    public function __construct()
    {
    }

    public function everyMinute()
    {
        //Do something
    }

    public function everyHour()
    {
        $upgrades = scandir(BASE_DIR . 'updates/');

        foreach ($upgrades as $upgrade) {
            if (is_file(BASE_DIR . 'updates/' . $upgrade)) {
                /** @var AsyncJob $task */
                $task = $this->di->singleton(AsyncJob::class, ['id' => PatchTask::class]);
                $task->setContext(['patch' => $upgrade])
                    ->setCallable(PatchTask::class)
                    ->save();
            }
        }
    }

    public function everyDay()
    {
        //Do something
    }

    public function everyWeek()
    {
        //Do something
    }

    public function everyMonth()
    {
        //Do something
    }
}
