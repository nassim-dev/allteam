<?php

namespace app\cron;

use app\entities\context\Context;
use core\config\Config;
use core\database\RepositoryFactoryInterface;
use core\logger\LogServiceInterface;
use core\services\storage\ObjectStorageInterface;
use core\tasks\CronInterface;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class CleanFilesystemCron implements CronInterface
{
    public function __construct(
        private LogServiceInterface $logService,
        private ObjectStorageInterface $objectStorage,
        private RepositoryFactoryInterface $repositoryFactory,
        private Config $config
    ) {
    }

    public function everyMinute()
    {
    }

    public function everyHour()
    {
    }

    public function everyDay()
    {
        //Do something
    }

    public function everyWeek()
    {
        /** @var Context[] $contexts */
        $contexts = $this->repositoryFactory->table('context')
            ->getList();
        foreach ($contexts as $context) {
            $this->logService->dump([
                'data'    => 'Clean tmp files',
                'logfile' => CRON_LOGFILE
            ]);

            if ($context->idcontext == 0) {
                continue;
            }

            $dir   = BASE_DIR . $this->config->find('PHP.ROOT_PUBLIC_FILES') . 'context_' . $context->idcontext . '/temp';
            $files = scandir($dir);
            if (!empty($files)) {
                foreach ($files as $file) {
                    $file_tmp = realpath($dir . '/' . $file);
                    if (is_file($file_tmp)) {
                        $this->objectStorage->delete($file_tmp);
                    }
                }
            }

            $dir   = BASE_DIR . 'temp';
            $files = scandir($dir);
            if (!empty($files)) {
                foreach ($files as $file) {
                    $file_tmp = realpath($dir . '/' . $file);
                    if (is_file($file_tmp)) {
                        $this->objectStorage->delete($file_tmp);
                    }
                }
            }
        }
    }

    public function everyMonth()
    {
        //clear old logs
        $this->logService->dump([
            'data'    => 'Clear olds logs...',
            'logfile' => CRON_LOGFILE
        ]);

        if ($handle = opendir(BASE_DIR . 'logs/')) {
            while (false !== ($file = readdir($handle))) {
                $filelastmodified = filemtime($file);
                //7776000 = 24 * 3600 * 30 * 3
                if ((time() - $filelastmodified) > 7776000) {
                    unlink($file);
                }
            }

            closedir($handle);
        }
    }
}
