<?php

define('OBJECT_FORMAT', true);
define('FORMATED_FORMAT', false);

define('DATEINTERVAL_FORMAT', true);
define('EXISTING_ID', true);

define('WORKER_LOGFILE', 'worker.manager.' . date('Ym') . '.log');
define('APP_LOGFILE', 'app.startup.' . date('Ym') . '.log');
define('API_LOGFILE', 'api.server.' . date('Ym') . '.log');
define('ASYNC_LOGFILE', 'async.task.' . date('Ym') . '.log');
define('WEBSOCKET_LOGFILE', 'websocket.server.' . date('Ym') . '.log');
define('CRON_LOGFILE', 'cron.' . date('Ym') . '.log');
define('MAIL_LOGFILE', 'mail.' . date('Ym') . '.log');

//Import custom constant
//require BASE_DIR . 'constants/custom.php';
