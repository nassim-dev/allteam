import e from"./DefaultCommand.23718136.js";import"./index.a7ff2035.js";import"./vendor.9963c748.js";
/**
 *
 *  Class ChangeAttributeCommand
 *
 * Represent  Base ChangeAttributeCommand
 *
 * @category Represent html ChangeAttributeCommand
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */class t extends e{execute(e,t){let a="object"==typeof t.value?e.command.execute(e,t.value,t.parameters):t.value,r=t.target==="#"+e.id?e.wrapper:document.querySelector(t.target);return t.parameters.targetIsValuable?(r.value=a,r.value):(r.dataset[t.attribute]=a,r.dataset[t.attribute])}}export{t as default};
//# sourceMappingURL=ChangeAttributeCommand.c223515c.js.map
