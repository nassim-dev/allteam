import"./index.a7ff2035.js";import"./vendor.9963c748.js";
/**
 *
 *  Class DefaultCommand
 *
 * Represent  Base DefaultCommand
 *
 * @category Represent html DefaultCommand
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */class t{constructor(t){this.app=t}execute(t,e){}}export{t as default};
//# sourceMappingURL=DefaultCommand.23718136.js.map
