(function(){System.register(["./DefaultCommand-legacy.cdc77cc5.js","./index-legacy.fb656449.js","./vendor-legacy.f283aa6f.js"],(function(exports,module){"use strict";var DefaultCommand;return{setters:[e=>{DefaultCommand=e.default},()=>{},()=>{}],execute:function(){
/**
       *
       *  Class EvalCommand
       *
       * Represent  Base EvalCommand
       *
       * @category Represent html EvalCommand
       * @version 0.1
       * @author Nassim Ourami <nassim.ourami@mailo.com>
       * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
       *
       * @link https://allteam.io
       *
       */
class EvalCommand extends DefaultCommand{execute(component,command){return eval(command.callback)}}exports("default",EvalCommand)}}}))})();
//# sourceMappingURL=EvalCommand-legacy.28d45252.js.map
