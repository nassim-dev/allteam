System.register(["./DefaultCommand-legacy.cdc77cc5.js","./index-legacy.fb656449.js","./vendor-legacy.f283aa6f.js"],(function(e,t){"use strict";var a;return{setters:[e=>{a=e.default},()=>{},()=>{}],execute:function(){e("default",
/**
       *
       *  Class EndpointFilterCommand
       *
       * Represent  Base EndpointFilterCommand
       *
       * @category Represent html EndpointFilterCommand
       * @version 0.1
       * @author Nassim Ourami <nassim.ourami@mailo.com>
       * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
       *
       * @link https://allteam.io
       *
       */
class extends a{execute(e,t){this.app.datad.fetch(t.endpoint,t.parameters)}})}}}));
//# sourceMappingURL=EndpointFilterCommand-legacy.910d6534.js.map
