System.register(["./DefaultCommand-legacy.cdc77cc5.js","./index-legacy.fb656449.js","./vendor-legacy.f283aa6f.js"],(function(e,t){"use strict";var a;return{setters:[e=>{a=e.default},()=>{},()=>{}],execute:function(){e("default",
/**
       * Class PopulateCommand
       *
       * Description
       *
       * @category  Description
       * @author    Nassim Ourami <nassim.ourami@mailo.com>
       * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
       * @version   Release: 0.1
       *
       * @link    https://allteam.io
       * @since   File available since Release 0.1
       * @package Allteam
       */
class extends a{async execute(e,t){let a=this.app.componentd.get(t.command.target),c=this.app.componentd.get(t.command.target.replace("form","modal"));null!==c&&null!==a&&(a.blockUi.block(),setTimeout((()=>{a.blockUi.isBlocked()&&a.blockUi.release()}),1e4),c.triggerShow(),this.app.ajaxd.call(t.command.endpoint).then((e=>{a.populate(e.data)})).then((()=>{a.blockUi.release()})))}})}}}));
//# sourceMappingURL=PopulateCommand-legacy.a7c473e3.js.map
