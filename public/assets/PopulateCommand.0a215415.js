import e from"./DefaultCommand.23718136.js";import"./index.a7ff2035.js";import"./vendor.9963c748.js";
/**
 * Class PopulateCommand
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */class t extends e{async execute(e,t){let o=this.app.componentd.get(t.command.target),a=this.app.componentd.get(t.command.target.replace("form","modal"));null!==a&&null!==o&&(o.blockUi.block(),setTimeout((()=>{o.blockUi.isBlocked()&&o.blockUi.release()}),1e4),a.triggerShow(),this.app.ajaxd.call(t.command.endpoint).then((e=>{o.populate(e.data)})).then((()=>{o.blockUi.release()})))}}export{t as default};
//# sourceMappingURL=PopulateCommand.0a215415.js.map
