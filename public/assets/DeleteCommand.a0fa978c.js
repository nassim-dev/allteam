import t from"./DefaultCommand.23718136.js";import{S as e}from"./index.a333e001.js";import"./index.a7ff2035.js";import"./vendor.9963c748.js";import"./event-handler.9853cb40.js";
/**
 * Class DeleteCommand
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */class o extends t{execute(t,o){e.fire({title:"Voulez-vous supprimer cet élément ?",text:"Cette action est définitive!",icon:"warning",showCancelButton:!0,confirmButtonColor:"#3085d6",cancelButtonColor:"#d33",confirmButtonText:"Oui, supprimer"}).then((t=>{let e=this.app.datad.getDatasource(o.command.endpoint);t.isConfirmed&&this.app.ajaxd.call(e).then((t=>{}))}))}}export{o as default};
//# sourceMappingURL=DeleteCommand.a0fa978c.js.map
