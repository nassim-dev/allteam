import e from"./DefaultCommand.23718136.js";import"./index.a7ff2035.js";import"./vendor.9963c748.js";
/**
 *
 *  Class ValueCommand
 *
 * Represent  Base ValueCommand
 *
 * @category Represent html ValueCommand
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */class r extends e{execute(e,r){return r.parameters.value}}export{r as default};
//# sourceMappingURL=ValueCommand.0d497638.js.map
