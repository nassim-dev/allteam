System.register(["./DefaultCommand-legacy.cdc77cc5.js","./index-legacy.fb656449.js","./vendor-legacy.f283aa6f.js"],(function(e,t){"use strict";var a;return{setters:[e=>{a=e.default},()=>{},()=>{}],execute:function(){e("default",
/**
       *
       *  Class ChangeAttributeCommand
       *
       * Represent  Base ChangeAttributeCommand
       *
       * @category Represent html ChangeAttributeCommand
       * @version 0.1
       * @author Nassim Ourami <nassim.ourami@mailo.com>
       * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
       *
       * @link https://allteam.io
       *
       */
class extends a{execute(e,t){let a="object"==typeof t.value?e.command.execute(e,t.value,t.parameters):t.value,r=t.target==="#"+e.id?e.wrapper:document.querySelector(t.target);return t.parameters.targetIsValuable?(r.value=a,r.value):(r.dataset[t.attribute]=a,r.dataset[t.attribute])}})}}}));
//# sourceMappingURL=ChangeAttributeCommand-legacy.a6b8276f.js.map
