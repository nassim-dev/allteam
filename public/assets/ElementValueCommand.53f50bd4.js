import e from"./DefaultCommand.23718136.js";import"./index.a7ff2035.js";import"./vendor.9963c748.js";
/**
 *
 *  Class ElementValueCommand
 *
 * Represent  Base ElementValueCommand
 *
 * @category Represent html ElementValueCommand
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */class t extends e{execute(e,t){const r="self"===t.parameters.source?e.wrapper:document.querySelector(t.parameters.source),a="self"===t.parameters.target?e.wrapper:document.querySelector(t.parameters.target);if(a.classList.contains("mdb-selected")&&this.app.componentd.isRegistred(a.getAttribute("id"))){const e=this.app.componentd.get(a.getAttribute("id"));new Promise((t=>{e.goto("mount"),t()})).then((()=>{e.goto("idle")})).then((()=>{e.preload(r.value),e.dispatch("change")}))}else a.value=r.value;return r.value}}export{t as default};
//# sourceMappingURL=ElementValueCommand.53f50bd4.js.map
