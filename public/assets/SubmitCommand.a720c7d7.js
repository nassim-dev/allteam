import a from"./DefaultCommand.23718136.js";import"./index.a7ff2035.js";import"./vendor.9963c748.js";
/**
 * Class SubmitCommand
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */class e extends a{constructor(a){super(a)}async execute(a,e){let t=e.command;a.goto("processing");let o=a.getDatas();for(let r in t.parameters)o.append(r,t.parameters[r]);await this.app.ajaxd.call(this.app.datad.getDatasource(t),o).then((e=>{void 0===e||e.error||void 0===a.config.parent||(history.back(),a.clear())})),a.goto("success")}}export{e as default};
//# sourceMappingURL=SubmitCommand.a720c7d7.js.map
