System.register(["./DefaultCommand-legacy.cdc77cc5.js","./index-legacy.fb656449.js","./vendor-legacy.f283aa6f.js"],(function(e,t){"use strict";var n;return{setters:[e=>{n=e.default},()=>{},()=>{}],execute:function(){e("default",
/**
       * Class RedirectCommand
       *
       * Description
       *
       * @category  Description
       * @author    Nassim Ourami <nassim.ourami@mailo.com>
       * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
       * @version   Release: 0.1
       *
       * @link    https://allteam.io
       * @since   File available since Release 0.1
       * @package Allteam
       */
class extends n{execute(e,t){void 0!==t.command.endpoint&&this.app.transition.redirect(t.command.endpoint)}})}}}));
//# sourceMappingURL=RedirectCommand-legacy.ec40d93b.js.map
