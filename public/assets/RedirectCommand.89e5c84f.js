import e from"./DefaultCommand.23718136.js";import"./index.a7ff2035.js";import"./vendor.9963c748.js";
/**
 * Class RedirectCommand
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */class t extends e{execute(e,t){void 0!==t.command.endpoint&&this.app.transition.redirect(t.command.endpoint)}}export{t as default};
//# sourceMappingURL=RedirectCommand.89e5c84f.js.map
