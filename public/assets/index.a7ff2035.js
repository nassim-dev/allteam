function __vite_legacy_guard(){import("data:text/javascript,")}import{O as OnScreen,T as Toastr,a as autobahn_min,D as Dexie$1,H as Highway,g as gsapWithCSS,j as jQuery,b as bootstrap}from"./vendor.9963c748.js";const scriptRel="modulepreload",assetsURL=function(t){return"/assets/"+t},seen={},__vitePreload=function(t,e,n){return e&&0!==e.length?Promise.all(e.map((t=>{if((t=assetsURL(t))in seen)return;seen[t]=!0;const e=t.endsWith(".css"),n=e?'[rel="stylesheet"]':"";if(document.querySelector(`link[href="${t}"]${n}`))return;const i=document.createElement("link");return i.rel=e?"stylesheet":scriptRel,e||(i.as="script",i.crossOrigin=""),i.href=t,document.head.appendChild(i),e?new Promise(((e,n)=>{i.addEventListener("load",e),i.addEventListener("error",(()=>n(new Error(`Unable to preload CSS for ${t}`))))})):void 0}))).then((()=>t())):t()},defaultConfig$c=[];
/**
 *
 *  Class EventManager
 *
 * Represent  object EventManager
 *
 * @category Represent object eventManager
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */
class EventManager{constructor(t,e=[]){this.app=t,this.config=e,this.listeners={},this.os=new OnScreen({container:window,direction:"vertical",tolerance:0,throttle:50,lazyAttr:null,debug:!0}),this.bindedEvents=[],this._componentsListerners=[]}mount(){return this.app.configd.parseConfig(defaultConfig$c,this.config,this).then((t=>{this.config=t})),this.bindedEvents.forEach((t=>{window.addEventListener(t,this)})),this}unmount(){return this.listeners={},this.bindedEvents.forEach((t=>{window.removeEventListener(t,this)})),this}reload(){return this}attach(t,e,n,i=null){void 0===t.listeners&&(t.listeners={});let s=null;null!=i&&"function"==typeof n&&(s=t=>{t.preventDefault();const e=t.target.closest(i);if(e)return n(e)}),void 0===t.listeners[e]&&(t.listeners[e]=[]),t.listeners[e].push(null!=s?s:n)}detach(t,e=null,n=null){if(void 0===t.listeners&&(t.listeners={}),!(e in t.listeners))return;if("function"!=typeof n)return void(t.listeners[e]=[]);const i=t.listeners[e];for(let s=0,a=i.length;s<a;s++)if(i[s]===n)return void i.splice(s,1)}dispatch(t,e){if(void 0===e.listeners&&(e.listeners={}),"string"==typeof t&&(t=new CustomEvent(t)),!(t.type in e.listeners))return!0;const n=e.listeners[t.type].slice();for(let i=0,s=n.length;i<s;i++)n[i].call(e,t);return!t.defaultPrevented}addEventListener(t,e){this.attach(this,t,e,null)}removeEventListener(t,e){this.detach(this,t,e)}dispatchEvent(t){return this.dispatch(t,this)}handleEvent(t){this.dispatchEvent(t)}}
/**
 *
 *  Class Events
 *
 * Events listener && target functions
 *
 * @category Events listener && target functions
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */class Events{constructor(){this._eventd=null,this.listeners=[]}get eventd(){return null==this._eventd&&(this._eventd=new EventManager(window.app),this._eventd.mount()),this._eventd}handleEvent(t){return this.eventd.dispatch(t,this)}on(t,e,n=null,i=[]){this.eventd.attach(this,t,e,n)}off(t,e=null){this.eventd.detach(this,t,e)}dispatch(t,e=[]){e.context=this;const n=new CustomEvent(t,{composed:!0,bubbles:!0,cancelable:!0,detail:e});this.eventd.dispatch(n,this)}}
/**
 *
 *  Class CommandProxy
 *
 * Represent  Base CommandProxy
 *
 * @category Represent html CommandProxy
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */class CommandProxy extends Events{constructor(t,e,n){super(),this.app=t,this.command=e,this.object=null,this.loader=n}async build(){return this.isBuild()?this.object:this.loader().then((t=>{this.object=new t.default(this.app)})).then((()=>this.object))}isBuild(){return null!=this.object}execute(t,e){if(this.isBuild())return this.object.execute(t,e);this.app.exceptiond.addException(this.command+" could not or has not been build yet")}}const CommandProxy$1=Object.freeze(Object.defineProperty({__proto__:null,default:CommandProxy},Symbol.toStringTag,{value:"Module"}));function __variableDynamicImportRuntime2__(t){switch(t){case"./command/AjaxCallCommand.js":return __vitePreload((()=>import("./AjaxCallCommand.c92ef1f0.js")),["AjaxCallCommand.c92ef1f0.js","DefaultCommand.23718136.js","vendor.9963c748.js"]);case"./command/ChangeAttributeCommand.js":return __vitePreload((()=>import("./ChangeAttributeCommand.c223515c.js")),["ChangeAttributeCommand.c223515c.js","DefaultCommand.23718136.js","vendor.9963c748.js"]);case"./command/CommandProxy.js":return __vitePreload((()=>Promise.resolve().then((()=>CommandProxy$1))),void 0);case"./command/DefaultCommand.js":return __vitePreload((()=>import("./DefaultCommand.23718136.js")),["DefaultCommand.23718136.js","vendor.9963c748.js"]);case"./command/ElementValueCommand.js":return __vitePreload((()=>import("./ElementValueCommand.53f50bd4.js")),["ElementValueCommand.53f50bd4.js","DefaultCommand.23718136.js","vendor.9963c748.js"]);case"./command/EndpointFilterCommand.js":return __vitePreload((()=>import("./EndpointFilterCommand.cb106a6d.js")),["EndpointFilterCommand.cb106a6d.js","DefaultCommand.23718136.js","vendor.9963c748.js"]);case"./command/EvalCommand.js":return __vitePreload((()=>import("./EvalCommand.0b5736e3.js")),["EvalCommand.0b5736e3.js","DefaultCommand.23718136.js","vendor.9963c748.js"]);case"./command/ValueCommand.js":return __vitePreload((()=>import("./ValueCommand.0d497638.js")),["ValueCommand.0d497638.js","DefaultCommand.23718136.js","vendor.9963c748.js"]);default:return Promise.reject(new Error("Unknown variable dynamic import: "+t))}}function __variableDynamicImportRuntime1__(t){switch(t){case"./command/AjaxCallCommand.js":return __vitePreload((()=>import("./AjaxCallCommand.c92ef1f0.js")),["AjaxCallCommand.c92ef1f0.js","DefaultCommand.23718136.js","vendor.9963c748.js"]);case"./command/ChangeAttributeCommand.js":return __vitePreload((()=>import("./ChangeAttributeCommand.c223515c.js")),["ChangeAttributeCommand.c223515c.js","DefaultCommand.23718136.js","vendor.9963c748.js"]);case"./command/CommandProxy.js":return __vitePreload((()=>Promise.resolve().then((()=>CommandProxy$1))),void 0);case"./command/DefaultCommand.js":return __vitePreload((()=>import("./DefaultCommand.23718136.js")),["DefaultCommand.23718136.js","vendor.9963c748.js"]);case"./command/ElementValueCommand.js":return __vitePreload((()=>import("./ElementValueCommand.53f50bd4.js")),["ElementValueCommand.53f50bd4.js","DefaultCommand.23718136.js","vendor.9963c748.js"]);case"./command/EndpointFilterCommand.js":return __vitePreload((()=>import("./EndpointFilterCommand.cb106a6d.js")),["EndpointFilterCommand.cb106a6d.js","DefaultCommand.23718136.js","vendor.9963c748.js"]);case"./command/EvalCommand.js":return __vitePreload((()=>import("./EvalCommand.0b5736e3.js")),["EvalCommand.0b5736e3.js","DefaultCommand.23718136.js","vendor.9963c748.js"]);case"./command/ValueCommand.js":return __vitePreload((()=>import("./ValueCommand.0d497638.js")),["ValueCommand.0d497638.js","DefaultCommand.23718136.js","vendor.9963c748.js"]);default:return Promise.reject(new Error("Unknown variable dynamic import: "+t))}}function __variableDynamicImportRuntime0__$1(t){switch(t){case"./command/AjaxCallCommand.js":return __vitePreload((()=>import("./AjaxCallCommand.c92ef1f0.js")),["AjaxCallCommand.c92ef1f0.js","DefaultCommand.23718136.js","vendor.9963c748.js"]);case"./command/ChangeAttributeCommand.js":return __vitePreload((()=>import("./ChangeAttributeCommand.c223515c.js")),["ChangeAttributeCommand.c223515c.js","DefaultCommand.23718136.js","vendor.9963c748.js"]);case"./command/CommandProxy.js":return __vitePreload((()=>Promise.resolve().then((()=>CommandProxy$1))),void 0);case"./command/DefaultCommand.js":return __vitePreload((()=>import("./DefaultCommand.23718136.js")),["DefaultCommand.23718136.js","vendor.9963c748.js"]);case"./command/ElementValueCommand.js":return __vitePreload((()=>import("./ElementValueCommand.53f50bd4.js")),["ElementValueCommand.53f50bd4.js","DefaultCommand.23718136.js","vendor.9963c748.js"]);case"./command/EndpointFilterCommand.js":return __vitePreload((()=>import("./EndpointFilterCommand.cb106a6d.js")),["EndpointFilterCommand.cb106a6d.js","DefaultCommand.23718136.js","vendor.9963c748.js"]);case"./command/EvalCommand.js":return __vitePreload((()=>import("./EvalCommand.0b5736e3.js")),["EvalCommand.0b5736e3.js","DefaultCommand.23718136.js","vendor.9963c748.js"]);case"./command/ValueCommand.js":return __vitePreload((()=>import("./ValueCommand.0d497638.js")),["ValueCommand.0d497638.js","DefaultCommand.23718136.js","vendor.9963c748.js"]);default:return Promise.reject(new Error("Unknown variable dynamic import: "+t))}}const defaultConfig$b=[],instances={},privateMethods$2={_loadDefaultCommands:t=>{let e=document.querySelector("#default-commands");if(void 0!==e&&null!=e){let s=e.innerHTML.trim();if(s.length>0){let e=JSON.parse(s);for(var n in e)for(var i in e[n]){if("default"===n){t.addCommand(i,(async()=>__variableDynamicImportRuntime0__$1(`./command/${e[n][i]}.js`)));continue}t.createManager(n).addCommand(i,(async()=>__variableDynamicImportRuntime1__(`./command/${e[n][i]}.js`)))}}e.parentElement.removeChild(e)}}};
/**
 *
 *  Class CommandManager
 *
 * Represent  Base CommandManager
 *
 * @category Represent html CommandManager
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */
class CommandManager{constructor(t,e=[]){this.app=t,this.config=e,this.commands=[]}createManager(t){const e=instances[t]=void 0!==instances[t]?instances[t]:new CommandManager(this.app);return this.commands.forEach((t=>{e.addCommandProxy(t)})),e}addCommand(t,e){if(!this.commandExist(t)){let n=e;"function"!=typeof e&&(n=async()=>__variableDynamicImportRuntime2__(`./command/${t}.js`)),this.commands[t]=new CommandProxy(this.app,t,n)}return this}addCommandProxy(t){return this.commandExist(t.command)||(this.commands[t.command]=t),this}mount(){return this.app.configd.parseConfig(defaultConfig$b,this.config,this).then((t=>{this.config=t})).then((()=>{})),this}unmount(){return this}reload(){return privateMethods$2._loadDefaultCommands(this),this}commandExist(t){if("string"==typeof t)return void 0!==this.commands[t]}getCommand(t){return this.commands[t]}execute(t,e,n=[]){let i;return new Promise((s=>{if("function"==typeof e)return i=e(t,n),s(i);if("function"==typeof t[e])return i=t[e](n),s(i);if(this.commandExist(e)){let a=this.getCommand(e);return a.isBuild()?(i=a.execute(t,n),s(i)):a.build().then((()=>{i=a.execute(t,n),s(i)}))}this.app.exceptiond.addException(`Unable to execute command "${e}" for #${t.id}. Reason :  "${e}" must be a method, a function or a registred Command`)}))}getCommandName(t){return t.split("/").pop().replace(/\.js/gm,"")}}const evPrefix="tc:";
/**
 *
 *  Class Transition
 *
 * Represent  Base Transition
 *
 * @category Represent state Transition
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */class Transition$1{constructor(t,e=(()=>{}),n=(()=>!0),i=(()=>{})){this.name=t,this.nameUc=t[0].toUpperCase()+t.slice(1),this.onTransition="function"==typeof e?e:this.onTransitionF,this.onError="function"==typeof i?i:this.onErrorF,this.validation="function"==typeof n?n:this.validationF}onErrorF(t,e){t.app.command.execute(t,`error${this.nameUc}`,e),t.app.eventd.dispatch(`${evPrefix}:error:${this.name}:${t.id}`,t)}validationF(t,e){return t.app.command.execute(t,`validation${this.nameUc}`,e)}onTransitionF(t,e){t.app.command.execute(t,`transition${this.nameUc}`,e),t.app.eventd.dispatch(`${evPrefix}:transition:${this.nameUc}:${t.id}`,t)}}
/**
 *
 *  Class State
 *
 * Represent  Base State
 *
 * @category Represent state State
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 */class State{constructor(t,e=!1,n=null,i=null){this.name=t,this.autoswitch=e,this.transitions={},this.action=n||this.actionF,this.onReload=i||this.onReloadF}actionF(t,e){return t.app.eventd.dispatch(`action.${this.name}.${t.getAttribute("id")}`,t),t.app.command.execute(t,this.name,e)}onReloadF(t,e){t.app.command.execute(t,`reload${this.name}`,e),t.app.eventd.dispatch(`reload.${this.name}.${t.getAttribute("id")}`,t)}addTransition(t,e=(()=>{}),n=(()=>!0),i=(()=>{})){const s=new Transition$1(t,e,n,i);return this.transitions[t]=s,this}}const defaultConfig$a=[];
/**
 *
 *  Class Machine
 *
 * Represent  Base Machine
 *
 * @category Represent html websocketMachine
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 * @param {App} app
 */class Machine extends Events{constructor(t,e,n=[]){super(),this.app=t,this.app.configd.parseConfig(defaultConfig$a,this.config,this).then((t=>{this.config=t})),this.limitForExecution=5e3,this.states={default:"initialized"},this.addState("initialized").addTransition("mount").addTransition("unmount"),this.addState("mount","idle").addTransition("idle"),this.addState("idle").addTransition("processing").addTransition("unmount"),this.addState("processing").addTransition("error").addTransition("success"),this.addState("error","rendering").addTransition("rendering"),this.addState("success","rendering").addTransition("rendering"),this.addState("rendering","idle").addTransition("idle"),this.addState("unmount").addTransition("mount"),void 0!==n.states&&n.states.forEach((t=>{this.states[t.name]=t})),this.components=[],this.register(e)}register(t){return this.components[t.id]={component:t,state:this.states[this.states.default],_locked:!1,isLocked(){return this._locked}},this}deregister(t){return this.isRegistred(t.id)&&delete this.components[t.id],this}isRegistred(t){return void 0!==this.components[t]}getComponentContext(t){return void 0===this.components[t.id]&&this.register(t),this.components[t.id]}lock(t){this.getComponentContext(t)._locked=!0,this.app.command.execute(t,"lock")}unlock(t){this.getComponentContext(t)._locked=!1,this.app.command.execute(t,"unlock")}getState(t){const e=this.getComponentContext(t);return e.isLocked()?{name:"locked"}:e.state}addState(t,e=!1,n=null,i=null){const s=new State(t,e,n,i);return this.states[t]=s,s}editState(t){if(void 0!==this.states[t])return this.states[t];this.app.exceptiond.addException(`Inexisting state required for edition: ${t}`,this)}execute(t,e,n=[]){let i,s=this.getComponentContext(t);return new Promise(((t,e)=>{let n=0,i=setTimeout((()=>{s.isLocked()||(clearTimeout(i),t()),n>=this.limitForExecution&&(this.app.exceptiond.addException("Unable to process transition, component is locked",s),e("Unable to process transition, component is locked")),n+=100}),100)})).then((()=>new Promise(((i,a)=>{if(this.lock(t),s.state.name==e)return"function"==typeof this.states[e].onReload&&this.states[e].onReload(t,n),a("Same state");void 0!==s.state.transitions&&void 0!==s.state.transitions[e]||(this.app.exceptiond.addException(`Innexisting transistion from ${s.state.name} to ${e}`,s),a(`Innexisting transistion for ${e}`)),i()})))).then((()=>new Promise(((a,o)=>{i=s.state.transitions[e],void 0===i&&o("Innexisting transistion"),"function"!=typeof i.validation||i.validation(t,n)||(this.app.exceptiond.addException(`Validation is not satisfied for ${e}`,s),o(`Validation is not satisfied for ${e}`)),a(i)})))).then((()=>new Promise((e=>{"function"==typeof i.onTransistion&&i.onTransistion(t,n),e(i)})))).then((()=>new Promise((a=>{s.state=this.states[e],"function"==typeof this.states[e].action&&this.states[e].action(t,n),a(i)})))).then((e=>new Promise(((i,a)=>{"string"==typeof s.state.autoswitch&&(this.unlock(s.component),this.execute(t,s.state.autoswitch,n)),i(e)})))).catch((e=>{void 0!==i&&"function"==typeof i.onError&&i.onError(t,n),this.app.exceptiond.addException(e,s)})).finally((()=>{this.unlock(t)}))}}function __variableDynamicImportRuntime0__(t){switch(t){case"../command/AjaxCallCommand.js":return __vitePreload((()=>import("./AjaxCallCommand.c92ef1f0.js")),["AjaxCallCommand.c92ef1f0.js","DefaultCommand.23718136.js","vendor.9963c748.js"]);case"../command/ChangeAttributeCommand.js":return __vitePreload((()=>import("./ChangeAttributeCommand.c223515c.js")),["ChangeAttributeCommand.c223515c.js","DefaultCommand.23718136.js","vendor.9963c748.js"]);case"../command/CommandProxy.js":return __vitePreload((()=>Promise.resolve().then((()=>CommandProxy$1))),void 0);case"../command/DefaultCommand.js":return __vitePreload((()=>import("./DefaultCommand.23718136.js")),["DefaultCommand.23718136.js","vendor.9963c748.js"]);case"../command/ElementValueCommand.js":return __vitePreload((()=>import("./ElementValueCommand.53f50bd4.js")),["ElementValueCommand.53f50bd4.js","DefaultCommand.23718136.js","vendor.9963c748.js"]);case"../command/EndpointFilterCommand.js":return __vitePreload((()=>import("./EndpointFilterCommand.cb106a6d.js")),["EndpointFilterCommand.cb106a6d.js","DefaultCommand.23718136.js","vendor.9963c748.js"]);case"../command/EvalCommand.js":return __vitePreload((()=>import("./EvalCommand.0b5736e3.js")),["EvalCommand.0b5736e3.js","DefaultCommand.23718136.js","vendor.9963c748.js"]);case"../command/ValueCommand.js":return __vitePreload((()=>import("./ValueCommand.0d497638.js")),["ValueCommand.0d497638.js","DefaultCommand.23718136.js","vendor.9963c748.js"]);default:return Promise.reject(new Error("Unknown variable dynamic import: "+t))}}const utils={parseHtmlConfig:t=>{let e=t.querySelector(".component-config");if(null!=e){let n=e.innerHTML.trim();n.length>0&&(t.config=Object.assign(t.config,JSON.parse(n))),t.removeChild(e)}},saveDatas:(t,e)=>{if(void 0!==t.config.endpoints){let n=t.config.endpoints;if(void 0===n.updateData)return;let i=t.app.datad.getDatasource(n.updateData);t.app.ajaxd.call(i,null!=e?e:{}).then((e=>{void 0!==e.content&&t.initFromState(e.content)}))}},loadDatas:(t,e)=>{if(void 0!==t.config.endpoints){let n=t.config.endpoints;if(void 0===n.readData)return;let i=t.app.datad.getDatasource(n.readData);void 0!==e&&(i.parameters=e),t.app.datad.handlerWatch(i,(e=>{t.initFromState(e.detail.content)})),t.app.ajaxd.call(i).then((e=>{void 0!==e.content&&t.initFromState(e.content)}))}},_appHandler:t=>e=>{t.app.componentd.register(t)},disconnectedCallback:t=>{for(var e in t.removeEventListener("app.mount",utils._appHandler(t)),t.listeners)for(var n in t.listeners[e])t.removeEventListener(e,t.listeners[e][n]);t.app.componentd.deregister(t)},connectedCallback:t=>{void 0!==t.app?t.app.componentd.register(t):t.addEventListener("app.mount",utils._appHandler(t))},mount:t=>t.app.configd.parseConfig(t.config,t.getDefaultConfig(),t).then((e=>{t.config=e,null!==t.wrapper&&void 0!==t.wrapper&&"function"==typeof t.wrapper.addEventListener&&t.bindedEvents.forEach((e=>{t.wrapper.addEventListener(e,globalThis)}))})),unmount:t=>{null!==t.wrapper&&t.bindedEvents.forEach((e=>{void 0!==t.wrapper&&"function"==typeof t.wrapper.removeEventListener&&t.wrapper.removeEventListener(e,t)}))},addListener:(t,e)=>{let n=n=>{if(n.preventDefault(),e.app.exceptiond.addException("Event triggered in component",n),void 0!==t.callback.addPopup)return void e.app.windowd.createPopup(t.callback.addPopup,e);let i=t.callback.split("/");i=i.pop(),e.command.commandExist(i)||e.command.addCommand(i,(async()=>__variableDynamicImportRuntime0__(`../command/${t.callback}.js`))),e.command.execute(e,i,t)};e.addEventListener(t.event,n),void 0===e.listeners[t.event]&&(e.listeners[t.event]=[]),e.listeners[t.event].push(n)},on:(t,e,n,i=null,s=[])=>{t.addEventListener(e,utils._getHandler(t,n,s)),void 0===t.listeners[e]&&(t.listeners[e]=[]),t.listeners[e].push(utils._getHandler(t,n,s))},off:(t,e,n=null)=>{for(var i in t.listeners[e])t.removeEventListener(e,t.listeners[e][i])},_getHandler:(t,e,n)=>i=>{if(i.preventDefault(),t.app.exceptiond.addException("Event triggered in component",i),t.command.commandExist(e))return t.command.execute(t,e,n)},datas:(t,e=null,n=null)=>null==typeof e?t.dataset:null!==n&&null!==e?(t.dataset[e]=n,null):void 0!==t.dataset[e]?t.dataset[e]:null},utils$1=utils,defaultConfig$9=[];
/**
 *
 *  Class ComponentManager
 *
 * Represent  object ComponentManager
 *
 * @category Represent object Componentmanager
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */
class ComponentManager extends Events{constructor(t,e=[]){super(),this.app=t,this.config=e,this.components=[],this.mode="sync",this.stack=[],this.listeners=[]}setMode(t){this.mode=t,this.app.eventd.dispatch("componentd.switchto."+t,this)}initModeSwitchListener(){this.app.eventd.attach(this,"componentd.switchto.sync",(()=>{for(;0!=this.stack.length;)this.register(this.stack.pop())}))}reload(){return this}hideFields(t){document.querySelectorAll(t).forEach((t=>{let e=this.get(t.id);null!=e&&e.hide()}),this)}executeSetters(t){for(let e in t)for(let n in this.components)n.includes(e)&&this.components[n].setValue(t[e])}mount(){return this.app.configd.parseConfig(defaultConfig$9,this.config,this).then((t=>{this.config=t})),this}unmount(){return new Promise((t=>{this.components.forEach((t=>{t.goto("unmount")})),t()})).then((t=>!0),(t=>!0)),this}isRegistred(t){return void 0!==this.components[t]}register(t){if("async"!=this.mode){if(!this.isRegistred(t.id)){let e=()=>{this.components[t.id]=t,this.initializeComponent(t)};if("complete"===document.readyState)return void e();this.app.ready(e)}}else this.stack.push(t)}deregister(t){t.goto("unmount"),this.isRegistred(t.id)&&(t.machine.deregister(t),delete this.components[t.id])}get(t){return"string"!=typeof t&&(t=t[0]),this.isRegistred(t)?this.components[t]:(this.app.exceptiond.addException("Innexisting component",t),null)}initializeComponent(t){var e;if(void 0===t.config.buildOn||!t.config.buildOn)return void this.build(t);const n=document.querySelector(t.config.buildOn.parameters.selector);null!==n?"onViewPort"!=t.config.buildOn.value?"deferred"==t.config.buildOn.value&&n.addEventListener(t.config.buildOn.parameters.event,(()=>{this.build(t)}),{once:!0}):this.app.eventd.os.on(null!=(e=t.config.buildOn.parameters.event)?e:"enter",n,(()=>{this.build(t)})):this.build(t)}build(t){null!=t&&t.is("initialized")&&t.goto("mount")}}const toastr=new Toastr({closeButton:!0,debug:!0,newestOnTop:!0,progressBar:!0,preventDuplicates:!0,positionClass:"toast-top-center",showMethod:"slideDown",showDuration:300,hideDuration:300,timeOut:2e3,extendedTimeOut:300,toastClass:["show"]}),defaultConfig$8=[],privateMethods$1={_log:(t,e,n)=>{n._exceptions.push({message:t,context:e})},_show:(t,e)=>{console.log(t,e)}};
/**
 *
 *  Class ExceptionManager
 *
 * Represent  Base ExceptionManager
 *
 * @category Represent html websocketExceptionManager
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */
class ExceptionManager extends Events{constructor(t,e=[]){super(),this.app=t,this.config=e,this._exceptions=[]}addException(t,e){this.config.showLogs?privateMethods$1._show(t,e):privateMethods$1._log(t,e,this)}notify(t,e={}){const n=t.message||"Nothing to show",i=t.type||"success",s=t.callback||!1,a=t.title||"";switch(i){case"success":toastr.success(n,a,e);break;case"info":default:toastr.info(n,a,e);break;case"error":toastr.error(n,a,e);break;case"warning":toastr.warning(n,a,e)}s&&setTimeout((()=>{window.location.replace(s)}),3e3)}mount(){return this.app.configd.parseConfig(defaultConfig$8,this.config,this).then((t=>{this.config=t})),this}unmount(){return this}reload(){return this}}
/**
 *
 *  Class Datasource
 *
 * Represent  Base Datasource
 *
 * @category Represent html websocketDatasource
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */class Datasource{constructor(t){this.listeners=[],this.apiVersion=null,this.ressource=null,this.parameters=[],this.endpoint=t,this.datas={},this.lastUpdated=new Date,this.method=null,this.parseEndpoint(t)}parseEndpoint(t){"/"===t.charAt(0)&&(t=t.substr(1)),"/"===t.charAt(t.length-1)&&(t=t.substr(0,t.length-1));const e=t.split("/");this.apiVersion=e.shift(),this.ressource=e.shift(),this.parameters=e}update(t=[]){this.datas=t,this.lastUpdated=new Date}getEventName(){return this.endpoint}}const defaultConfig$7=[],privateMethods={getToken:t=>{if(null===t.token){let e=document.querySelector("#CSRF");t.token={name:e.name,value:e.value}}return t.token},decorate:(t,e)=>{if("string"==typeof t)return t;let n=privateMethods.getToken(e);return t instanceof FormData?(t.append(n.name,n.value),t):(t[n.name]=n.value,t)},waitBeforeNextRefresh:(t,e)=>{t.timer=e},executeTriggers:t=>{let e;privateMethods.waitBeforeNextRefresh(t,1e3),t._loopId=setInterval((()=>{for(;t._triggers.length>0;){e=t._triggers.shift();const n=t.app.componentd.get(e.fieldId);t.app.command.execute(n,e.callback)}privateMethods.waitBeforeNextRefresh(t,t._triggers.length>0?1.2*t.timer:1e3)}),t.timer)},_callAsync:(t,e,n,i={},s=null)=>{let a=i=privateMethods.decorate(i,t);i instanceof FormData&&(a=[...i]);let o={method:e,cache:"default",headers:{"Content-Type":"application/json"}};return"GET"!==e&&"HEAD"!==e&&(o.body=new URLSearchParams(a)),fetch(n,o).then((e=>{if(e.ok)return e.json();t.app.exceptiond.addException(`Ajax Error for this url : ${n} with status ${e.status}`,globalThis)})).then((e=>void 0===e?{}:(void 0!==e.triggers&&e.triggers.forEach((e=>{t._triggers.push(e)})),e.data,void 0!==e.setter&&t.app.componentd.executeSetters(e.setter),e))).catch((e=>{t.app.exceptiond.addException(e.message,t)}))}};
/**
 *
 *  Class AjaxManager
 *
 * Represent  Base AjaxManager
 *
 * @category Represent html websocketAjaxManager
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 * @todo refactor
 */
class AjaxManager extends Events{constructor(t,e=[]){super(),this.app=t,this.config=e,this._pendingRequests=[],this._triggers=[],this.timer=500,this._loopId=null,this.token=null}ajaxDecorator(t){var e;t.data=null!=(e=t.data)?e:{};let n=privateMethods.getToken(this);return t.data[n.name]=n.value,t.options.data=t.data,t}mount(){return this.app.configd.parseConfig(defaultConfig$7,this.config,this).then((t=>{this.config=t,privateMethods.getToken(this),privateMethods.executeTriggers(this)})),this.app.eventd.addEventListener("fetch",this._callbackOnFetch),window.addEventListener("online",this._parsePendingRequests),this}_callbackOnFetch(t){}_parsePendingRequests(t){for(let e in this._pendingRequests)this.call(e)}call(t,e={}){var n;if(t.parameters=this.app.configd.mergeDeep(null!=(n=t.parameters)?n:{},e),t instanceof Datasource)return this.app.databased.parseDatasource(t);if(!navigator.onLine)return this.queue(t);switch(t.method){case"PUT":return this.put(t.endpoint,t.parameters);case"GET":return this.get(t.endpoint);case"POST":return this.post(t.endpoint,t.parameters);case"DELETE":return this.delete(t.endpoint,t.parameters);default:return new Promise((e=>{this.app.exceptiond.addException("Invalid method : "+t.method,this),e(!0)}))}}unmount(){return this.app.eventd.removeEventListener("fetch",this._callbackOnFetch),window.removeEventListener("online",this._parsePendingRequests),clearInterval(this._loopId),this}reload(){return this.token=null,privateMethods.getToken(this),this}get(t){return privateMethods._callAsync(this,"GET",t)}post(t,e=[],n="POST"){return privateMethods._callAsync(this,n,t,e)}delete(t,e=[],n="DELETE"){return privateMethods._callAsync(this,n,t,e)}put(t,e=[],n="PUT"){return privateMethods._callAsync(this,n,t,e)}queue(t){return navigator.onLine?this.call(t):(this._pendingRequests.push(t),Promise.resolve(!0))}}const defaultConfig$6=[];
/**
 *
 *  Class LocalStorageManager
 *
 * Represent  Base LocalStorageManager
 *
 * @category Represent html websocketLocalStorageManager
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */class LocalStorageManager extends Events{constructor(t,e=[]){super(),this.app=t,this.config=e,this.storage=e.storage||localStorage,this.isAvailable=!0}mount(){return this.app.configd.parseConfig(defaultConfig$6,this.config,this).then((t=>{this.config=t,LocalStorageManager.isEnable("localStorage")||(this.app.exceptiond.addException("Unable to activate localStorage",this),this.isAvailable=!1)})),this}unmount(){return this.storage.clear(),this}reload(){return this}get(t){return JSON.parse(this.storage.getItem(t))}set(t,e){this.storage.setItem(t,JSON.stringify(e))}remove(t){this.storage.remove(t)}static isEnable(t){let e,n;try{return e=Window[t],n="__storage_test__",e.setItem(n,n),e.removeItem(n),!0}catch(i){return i instanceof DOMException&&(22===i.code||1014===i.code||"QuotaExceededError"===i.name||"NS_ERROR_DOM_QUOTA_REACHED"===i.name)&&e&&0!==e.length}}}const defaultConfig$5=[];
/**
 *
 *  Class MachineManager
 *
 * Represent  Base MachineManager
 *
 * @category Represent html websocketMachineManager
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.ios
 */class MachineManager extends Events{constructor(t,e={}){super(),this.app=t,this.config=e,this.machines={}}mount(){return this.app.configd.parseConfig(defaultConfig$5,this.config,this).then((t=>{this.config=t})),this}unmount(){return this}reload(){return this}createMachine(t,e,n){return this.machines[e]=void 0!==this.machines[e]?this.machines[e].register(t):new Machine(this.app,t,n)}}const defaultConfig$4={skipSubprotocolCheck:!0};
/**
 *
 *  Class WebsocketManager
 *
 * Represent  Base WebsocketManager
 *
 * @category Represent html websocketWebsocketManager
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */class WebsocketManager extends Events{constructor(t,e=[]){var n,i,s,a;super(),this.app=t,this.config=e,this.channel=null!=(n=e.channel)?n:document.querySelector("#CHANNEL").value,this.mainchannel=null!=(i=e.mainchannel)?i:document.querySelector("#MAINCHANNEL").value,this.endpoint=document.querySelector("#URLSOCKET").value,this.pagechannel=null!=(s=e.pagechannel)?s:document.querySelector("#PAGECHANNEL").value,this.oldpagechannel=this.pagechannel,this.connection=null,this.session=null,this.subscriptions={};let o=document.querySelector("#JWT");this.jwt=null!=(a=o.value)?a:null}mount(){return this.app.configd.parseConfig(defaultConfig$4,this.config,this).then((t=>{this.channel=document.querySelector("#CHANNEL").value,this.config=t,this.connection=new autobahn_min.exports.Connection({on_user_error:(t,e)=>{this.app.exceptiond.addException(t,e)},on_internal_error:(t,e)=>{this.app.exceptiond.addException(t,e)},url:"wss://"+this.endpoint,realm:"default.domain"}),this.connection.onopen=t=>{this.onConnect(t)},this.connection.onclose=(t,e)=>{this.onClose(t,e)}})).then((()=>{this.connection.open()})).then((()=>{this.oldpagechannel=this.pagechannel})),this}subscribe(t){this.session.isOpen?this.session.subscribe(t,((t,e)=>{this.processResponse(t,e)})).then((e=>{this.subscriptions[t]=e,this.app.exceptiond.addException("New subscription",e)}),(t=>{this.app.exceptiond.addException(t,this.session)})):this.app.exceptiond.addException("Session is not opened yet, subscription canceled",t)}unsubscribe(t){this.session.isOpen&&void 0!==this.subscriptions[t]&&this.session.unsubscribe(this.subscriptions[t])}onConnect(t){this.session=t,this.app.exceptiond.addException("WebSocket connection opened on "+this.endpoint,t),this.session.isOpen&&(""!=this.mainchannel&&this.subscribe(this.mainchannel),""!=this.channel&&this.subscribe(this.channel),""!=this.pagechannel&&this.subscribe(this.pagechannel))}publish(t,e){this.session.isOpen&&this.session.publish(t,[{data:e}])}onClose(t,e){this.app.exceptiond.addException("WebSocket connection closed on "+this.endpoint+" : "+t,e)}processResponse(t,e){switch(e.type){case"ERROR":case"ACTIVITY":this.app.exceptiond.notify({message:e.content.message,type:e.content.type,callback:e.content.callback});break;case"CHAT":this.app.comd.ring(e);break;case"JSCONFIG":break;case"DATASOURCE":this.app.datad.update(e.content.endpoint,e.content.data);break;case"RELOADCOMPONENTS":e.content.forEach((t=>{this.app.componentd.isRegistred(t)&&this.app.componentd.get(t).reload()}));break;case"RECONNECT":if(void 0===e.content.data||!this.app.componentd.isRegistred("modal_panel"))break;this.app.componentd.get("modal_panel").show(),null===document.getElementById("reconnect")&&$("#form_create_login").prepend(e.content.data),null===document.getElementById("input-reconnect")&&$("#email_form_create_login").prepend('<div id="input-reconnect"><input type="hidden" id="input-reconnect-f" name="reconnect" class="form-control" value="true"></div>');break;default:this.app.configd.parseConfigElement(e,this)}}unmount(){return this.unsubscribe(this.mainchannel),this.unsubscribe(this.channel),this.unsubscribe(this.pagechannel),this.connection.close(),this}reload(){return this.unsubscribe(this.oldpagechannel),this.channel=document.querySelector("#CHANNEL").value,this.mainchannel=document.querySelector("#MAINCHANNEL").value,this.endpoint=document.querySelector("#URLSOCKET").value,this.pagechannel=document.querySelector("#PAGECHANNEL").value,this.oldpagechannel=this.pagechannel,this.subscribe(this.pagechannel),this}}
/**
 *
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */const isObject=function(t){return t&&"object"==typeof t&&!Array.isArray(t)},slugify=function(t){t=(t=t.replace(/^\s+|\s+$/g,"")).toLowerCase();const e="ÁÄÂÀÃÅČÇĆĎÉĚËÈÊẼĔȆÍÌÎÏŇÑÓÖÒÔÕØŘŔŠŤÚŮÜÙÛÝŸŽáäâàãåčçćďéěëèêẽĕȇíìîïňñóöòôõøðřŕšťúůüùûýÿžþÞĐđßÆa·/_,:;";for(let n=0,i=e.length;n<i;n++)t=t.replace(new RegExp(e.charAt(n),"g"),"AAAAAACCCDEEEEEEEEIIIINNOOOOOORRSTUUUUUYYZaaaaaacccdeeeeeeeeiiiinnooooooorrstuuuuuyyzbBDdBAa------".charAt(n));return t=t.replace(/[^a-z0-9 -]/g,"").replace(/\s+/g,"-").replace(/-+/g,"-").replace(/^-+/,"").replace(/-+$/,"")};
/**
 *
 *  Class ConfigurationManager
 *
 * Represent  Base ConfigurationManager
 *
 * @category Represent html websocketConfigurationManager
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 */
class ConfigurationManager{constructor(t){this.app=t}mount(){return this}unmount(){return this}reload(){return this}parseConfig(t,e,n){return new Promise((i=>{let s="function"==typeof n.isBuildinKey;for(var a in t)(s&&!n.isBuildinKey(a)||!s)&&(e[a]=this.parseConfigElement(n,t[a]));return i(e)}))}mergeDeep(t,...e){if(!e.length)return t;const n=e.shift();if(isObject(t)&&isObject(n))for(const i in n)isObject(n[i])?(t[i]||Object.assign(t,{[i]:{}}),this.mergeDeep(t[i],n[i])):Object.assign(t,{[i]:n[i]});return this.mergeDeep(t,...e)}parseConfigElement(t,e){if(null===e)return null;switch(e.type){case"METHOD":return this.app.command.execute(t,e.parameters.value,e);case"EVAL":return this.app.command.execute(t,"EvalCommand",e);case"FIELD":return this.app.command.execute(t,"ElementValueCommand",e);case"DATA":return this.app.command.execute(t,"ChangeAttributeCommand",e);case"VALUE":return this.app.command.execute(t,"ValueCommand",e);case"AJAX":return this.app.command.execute(t,"AjaxCallCommand",e);default:return e}}}const defaultConfig$3=[];
/**
 *
 *  Class WindowsManager
 *
 * Represent  Base WindowsManager
 *
 * @category Represent html websocketWindowsManager
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 * @param {App} app
 */class WindowsManager extends Events{constructor(t,e=[]){super(),this.app=t,this.config=e,this.windows={main:window},this.ctrlIsPressed=!1}getMain(){return this.windows.main||window}getWindows(t,e="popup",n="height=800,width=1280, toolbar=no, menubar=no, location=no, resizable=yes, scrollbars=yes, status=no"){const i=slugify(t);return this.windows[i]=void 0!==this.windows[i]?this.windows[i]:window.open(t,e,n),window.focus&&null!=typeof this.windows[i]&&this.windows[i].focus(),this.windows[i]}createPopup(config,object){const callbackYes="function"==typeof config.callbackYes?config.callbackYes(object):()=>{eval(config.callbackYes)},callbackNo="function"==typeof config.callbackNo?config.callbackNo(object):()=>{eval(config.callbackNo)},popupConfig={title:config.title,content:config.content,animation:"zoom",closeAnimation:"scale",escapeKey:!0,backgroundDismiss:!0,theme:"supervan",scrollToPreviousElement:!1,scrollToPreviousElementAnimate:!1,buttons:{Ok:{keys:["enter"],btnClass:" btn-light-success",action:callbackYes},Cancel:{btnClass:" btn-light-danger",action:callbackNo}}};void 0===config.type&&$.confirm(popupConfig)}mount(){return this.app.configd.parseConfig(defaultConfig$3,this.config,this).then((t=>{this.config=t})),this}unmount(){return this}reload(){return this}}
/**
 *
 *  Class InstantMessaging
 *
 * Represent  Base InstantMessaging
 *
 * @category Represent html instantMessaging
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */class InstantMessaging extends Events{constructor(t){super()}unmount(){return this}reload(){return this}mount(){return this}ring(){return this}}const defaultConfig$2=[];
/**
 *
 *  Class DatasourcesManager
 *
 * Represent  Base DatasourcesManager
 *
 * @category Represent html websocketDatasourcesManager
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */class DatasourcesManager extends Events{constructor(t,e=[]){super(),this.app=t,this.config=e,this._datasources={}}mount(){this.app.configd.parseConfig(defaultConfig$2,this.config,this).then((t=>{this.config=t}))}getDatasource(t){if(t instanceof Datasource)return t;let e="object"==typeof t?t.method+":"+t.endpoint:t;if(void 0===this._datasources[e]){let t=e.split(":");this._datasources[e]=new Datasource(t[1]),this._datasources[e].method=t[0]}return void 0!==t.parameters&&(this._datasources[e].parameters=t.parameters),this._datasources[e]}attach(t,e){const n=t instanceof Datasource?t:this.getDatasource(t);this.on(n.getEventName(),e)}detach(t,e=null){const n=t instanceof Datasource?t:this.getDatasource(t);this.off(n.getEventName(),e)}stringWatch(t,e){return this.attach(t,e),t}handlerWatch(t,e){return this.attach(t,e),()=>{this.fetch(t)}}update(t,e=[]){const n=t instanceof Datasource?t:this.getDatasource(t);n.update(e),this.dispatch(n.getEventName(),e)}fetch(t,e=[]){let n=this.getDatasource(t);return this.app.configd.parseConfig(e,n.parameters,n).then((()=>{this.app.ajaxd.call(n).then((e=>{this.update(t,e)}))}))}unmount(){this._datasources=[]}reload(){return this}}
/**
 *
 *  Class Store
 *
 * Represent  Base Store
 *
 * @category Represent html websocketStore
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 */class Store extends Events{constructor(t){super(),this.app=t,this.proxy=null,this.store={},this.subscriptions={}}subscribe(t,e,n=null){if(void 0===this.subscriptions[e]&&(this.subscriptions[e]=new Set),!this.subscriptions[e].has(t)){let i="state.change."+e;this.on(i,(i=>{null!=n?n(i,t):t[e]=i.detail.newValue})),this.subscriptions[e].add(t)}}unsubscribe(t,e){void 0===this.subscriptions[e]&&(this.subscriptions[e]=new Set),this.subscriptions[e].has(t)&&this.subscriptions[e].delete(t)}get(t,e,n=null){return this.subscribe(e,t,n),this.proxy[t]}set(t,e,n,i=null){this.subscribe(n,t,i),this.proxy[t]=e}mount(){return this.proxy=new Proxy(this.store,{set:(t,e,n,i)=>(this.dispatch("state.change."+e,{key:e,oldValue:null!=this.store[e]?this.store[e].valueOf():this.store[e],updated_at:new Date,newValue:n}),this.store[e]=n,!0),get:(t,e,n)=>{var i;return null!=(i=this.store[e])?i:null}}),this}unmount(){return this.subscriptions={},this}reload(){return this}}Dexie$1.Syncable.registerSyncProtocol("CustomAjax",{sync:function(t,e,n,i,s,a,o,r,d,c,l){var h={clientIdentity:t.clientIdentity||null,baseRevision:i,partial:o,changes:a,syncedRevision:s};window.app.ajaxd.post(e,h).then((e=>{e.success?"clientIdentity"in e?(t.clientIdentity=e.clientIdentity,t.save().then((()=>{d(),r(e.changes,e.currentRevision,e.partial,e.needsResync),c({again:1e4})})).catch((t=>{l(t,1/0)}))):(d(),r(e.changes,e.currentRevision,e.partial,e.needsResync),c({again:1e4})):l(e.errorMessage,1/0)}))}});const defaultConfig$1=[];
/**
 *
 *  Class DatabaseManager
 *
 * Represent  Base DatabaseManager
 *
 * @category Represent html websocketDatabaseManager
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */class DatabaseManager extends Events{constructor(t,e={}){super(),this.app=t,this.config=e,this.db=null,this.isAvailable=!0,this.updatedStore=[],this.lastUpdate=new Date}mount(){return this.app.configd.parseConfig(defaultConfig$1,this.config,this).then((t=>{this.config=t,window.indexedDB||(this.app.exceptiond.addException("Unable to activate indexedDB",this),this.isAvailable=!1)})).then((()=>{this.db=new Dexie$1("allteam"),this.db.open(),this.db.syncable.connect("CustomAjax",this.config.endpoint)})),this}parseDatasource(t){let e=this.getStore(t.ressource),n=new Date;return new Promise((i=>{if(void 0===e){let n={};n[t.ressource]="id",this.initStores(n),e=this.getStore(t.ressource)}i(e),t.parameters.map((t=>{t.updated_at=n}))})).then((()=>{switch(this.updatedStore.push({store:e,datasource:t}),t.method){case"PUT":null==e||e.bulkPut(t.parameters);case"GET":null==e||e.bulkGet(t.parameters);case"POST":null==e||e.bulkAdd(t.parameters);case"DELETE":null==e||e.bulkDelete(t.parameters);default:this.app.exceptiond.addException("Invalid method : "+t.method,this)}}))}async _syncDatabase(t){for(var e;0!=this.updatedStore.length;){let t=this.updatedStore.pop();this.app.ajaxd.call(t.datasource,await(null==(e=t.store)?void 0:e.where("updated_at").above(this.lastUpdate).toArray()))}}initStores(t){var e;null==(e=this.db)||e.version(1).stores(t)}updateStore(t,e){var n;null==(n=this.getStore(t))||n.bulkPut(e)}getStore(t){var e;return null==(e=this.db)?void 0:e.table(t)}unmount(){var t;return null==(t=this.db)||t.close(),this}reload(){return this}}const appInit={hashListener:function(){window.onhashchange=function(t){let e=t.oldURL.split("#"),n=t.newURL.split("#");if(void 0!==e[1]&&""!=e[1]){let t=app.componentd.get("modal_"+e[1]);null!=t&&t.triggerHide()}if(void 0!==n[1]&&""!=n[1]){let t=app.componentd.get("modal_"+n[1]);null!=t&&t.triggerShow()}}},openInitialModal:function(){if(""!=window.location.hash){let t=window.location.hash.replace("#","");""!=t&&setTimeout((()=>{let e=app.componentd.get("modal_"+t);null!=e&&e.triggerShow()}),100)}},asideMenuTrigger:function(){document.body.removeAttribute("data-kt-aside-minimize")},getEnv:function(){let t=document.getElementById("ENVIRONNEMENT");if(t instanceof HTMLElement){let e=t.value;return t.parentElement.removeChild(t),e}return"production"}};window.appInit=appInit;const defaultConfig=[];
/**
 *
 *  Class App
 *
 * Represent main application
 *
 * @category Represent main application
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */class App extends Events{constructor(t,e,n,i){super(),this.appInit=appInit,window.app=this,this.onConstruct=e,this.onError="function"==typeof n?n:(t,e)=>{this.exceptiond.addException(t,e)},this.onDestroy=i,this.config=t,this._configd=null,this._datad=null,this._command=null,this._exceptiond=null,this._componentd=null,this._machined=null,this._socketd=null,this._ajaxd=null,this._storaged=null,this._windowd=null,this._comd=null,this._stored=null,this._databased=null,this.modules=[],this._transition=null}mount(){return this.configd.parseConfig(defaultConfig,this.config,this).then((t=>{this.config=t,"function"==typeof this.onConstruct&&this.onConstruct(this)})).then((()=>{this.dispatch("app.mount")})).then((()=>{this.componentd.initModeSwitchListener()})).catch((t=>{"function"==typeof this.onError&&this.onError(t,this)})),this}switchPage(){return this.ajaxd.token=null,this}ready(t){if("complete"===document.readyState)t();else{let e=()=>{"complete"===document.readyState&&(t(),document.removeEventListener("readystatechange",e))};document.addEventListener("readystatechange",e)}}unmount(){return new Promise((t=>{this.modules.forEach((t=>{t.unmount()}),this),t(!0)})).then((()=>{"function"==typeof this.onDestroy&&this.onDestroy()})).catch((t=>{"function"==typeof this.onError&&this.onError(t,this)})),this}reload(){return this.command.reload(),this.componentd.reload(),this.ajaxd.reload(),this.socketd.reload(),this}get transition(){return null==this._transition&&(this._transition=new Highway.Core(this.config.transitionConfig)),this._transition}get configd(){return null==this._configd&&(this._configd=new ConfigurationManager(this),this._configd.mount()),this._configd}get datad(){return null==this._datad&&(this._datad=new DatasourcesManager(this),this._datad.mount()),this._datad}get command(){return null==this._command&&(this._command=new CommandManager(this),this._command.mount()),this._command}get exceptiond(){return null==this._exceptiond&&(this._exceptiond=new ExceptionManager(this,{showLogs:"development"===this.config.environnement}),this._exceptiond.mount()),this._exceptiond}get componentd(){return null==this._componentd&&(this._componentd=new ComponentManager(this),this._componentd.mount()),this._componentd}get stored(){return null==this._stored&&(this._stored=new Store(this),this._stored.mount()),this._stored}get machined(){return null==this._machined&&(this._machined=new MachineManager(this),this._machined.mount()),this._machined}get socketd(){return null==this._socketd&&(this._socketd=new WebsocketManager(this),this._socketd.mount()),this._socketd}get ajaxd(){return null==this._ajaxd&&(this._ajaxd=new AjaxManager(this),this._ajaxd.mount()),this._ajaxd}get storaged(){return null==this._storaged&&(this._storaged=new LocalStorageManager(this),this._storaged.mount()),this._storaged}get databased(){return null==this._databased&&(this._databased=new DatabaseManager(this,{method:"POST",endpoint:"/api/v2/sync"}),this._databased.mount()),this._databased}get windowd(){return null==this._windowd&&(this._windowd=new WindowsManager(this),this._windowd.mount()),this._windowd}get comd(){return null==this._comd&&(this._comd=new InstantMessaging(this),this._comd.mount()),this._comd}}const $header=document.getElementById("header-title");class Transition extends Highway.Transition{in({from:t,to:e,trigger:n,done:i}){window.scrollTo(0,0),document.body.style.overflow="unset",gsapWithCSS.fromTo(e,{opacity:0},{duration:.5,opacity:1,onComplete:i}),null!==typeof $header&&($header.innerHTML=e.dataset.routerView,gsapWithCSS.fromTo($header,{opacity:0},{duration:.5,opacity:1,onComplete:i}))}out({from:t,trigger:e,done:n}){gsapWithCSS.fromTo(t,{opacity:1},{duration:.5,opacity:0,onComplete:n}),null!==typeof $header&&gsapWithCSS.fromTo($header,{opacity:1},{duration:.5,opacity:0,onComplete:n}),t.remove()}}const style="",animate="",materialIcons="";window.$=jQuery,window.jQuery=jQuery,window.bootstrap=bootstrap;const app$1=new App({lang:document.documentElement.lang,environnement:window.appInit.getEnv(),transitionConfig:{transitions:{default:Transition}}},(t=>{app$1.ready((()=>{app$1.socketd,document.getElementById("page-overlay").classList.add("invisible")})),app$1.transition.on("NAVIGATE_IN",(({to:t})=>{app$1.switchPage()})),app$1.transition.on("NAVIGATE_END",(({to:t})=>{app$1.appInit.openInitialModal(),app$1.appInit.hashListener(),app$1.reload(),document.dispatchEvent(new CustomEvent("readystatechange",{readyState:"complete"}))})),app$1.appInit.hashListener()}),(t=>{app$1.exceptiond.addException(t,app$1)}),(()=>!0));app$1.on("app.mount",(()=>{app$1.command.addCommand("AjaxCallCommand","AjaxCallCommand").addCommand("ChangeAttributeCommand","ChangeAttributeCommand").addCommand("ElementValueCommand","ElementValueCommand").addCommand("EndpointFilterCommand","EndpointFilterCommand").addCommand("EvalCommand","EvalCommand").addCommand("ValueCommand","ValueCommand"),(async()=>{__vitePreload((()=>import("./index.97f040bf.js")),["index.97f040bf.js","event-handler.9853cb40.js","vendor.9963c748.js"]),await __vitePreload((()=>import("./index.a333e001.js").then((t=>t.i))),["index.a333e001.js","index.d33e71d1.css","vendor.9963c748.js","event-handler.9853cb40.js"]),app$1.appInit.openInitialModal()})()})),app$1.mount();export{__vitePreload as _,__vite_legacy_guard,utils$1 as u};
//# sourceMappingURL=index.a7ff2035.js.map
