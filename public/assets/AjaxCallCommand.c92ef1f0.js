import t from"./DefaultCommand.23718136.js";import"./index.a7ff2035.js";import"./vendor.9963c748.js";
/**
 *
 *  Class AjaxCallCommand
 *
 * Represent  Base AjaxCallCommand
 *
 * @category Represent html AjaxCallCommand
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */class a extends t{execute(t,a){this.app.ajaxd.post(a.endpoint,a.parameters).then((t=>{if(void 0!==t.location&&this.app.windowd.getWindows(t.location),void 0!==t.data)return t.data}))}}export{a as default};
//# sourceMappingURL=AjaxCallCommand.c92ef1f0.js.map
