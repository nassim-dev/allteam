import DefaultCommand from"./DefaultCommand.23718136.js";import"./index.a7ff2035.js";import"./vendor.9963c748.js";
/**
 *
 *  Class EvalCommand
 *
 * Represent  Base EvalCommand
 *
 * @category Represent html EvalCommand
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */class EvalCommand extends DefaultCommand{execute(component,command){return eval(command.callback)}}export{EvalCommand as default};
//# sourceMappingURL=EvalCommand.0b5736e3.js.map
