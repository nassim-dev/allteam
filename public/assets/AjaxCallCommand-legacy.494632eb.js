System.register(["./DefaultCommand-legacy.cdc77cc5.js","./index-legacy.fb656449.js","./vendor-legacy.f283aa6f.js"],(function(e,t){"use strict";var a;return{setters:[e=>{a=e.default},()=>{},()=>{}],execute:function(){e("default",
/**
       *
       *  Class AjaxCallCommand
       *
       * Represent  Base AjaxCallCommand
       *
       * @category Represent html AjaxCallCommand
       * @version 0.1
       * @author Nassim Ourami <nassim.ourami@mailo.com>
       * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
       *
       * @link https://allteam.io
       *
       */
class extends a{execute(e,t){this.app.ajaxd.post(t.endpoint,t.parameters).then((e=>{if(void 0!==e.location&&this.app.windowd.getWindows(e.location),void 0!==e.data)return e.data}))}})}}}));
//# sourceMappingURL=AjaxCallCommand-legacy.494632eb.js.map
