System.register(["./DefaultCommand-legacy.cdc77cc5.js","./index-legacy.fb656449.js","./vendor-legacy.f283aa6f.js"],(function(e,t){"use strict";var a;return{setters:[e=>{a=e.default},()=>{},()=>{}],execute:function(){e("default",
/**
       * Class SubmitCommand
       *
       * Description
       *
       * @category  Description
       * @author    Nassim Ourami <nassim.ourami@mailo.com>
       * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
       * @version   0.1
       *
       * @link    https://allteam.io
       * @since   File available since Release 0.1
       * @package Allteam
       */
class extends a{constructor(e){super(e)}async execute(e,t){let a=t.command;e.goto("processing");let s=e.getDatas();for(let c in a.parameters)s.append(c,a.parameters[c]);await this.app.ajaxd.call(this.app.datad.getDatasource(a),s).then((t=>{void 0===t||t.error||void 0===e.config.parent||(history.back(),e.clear())})),e.goto("success")}})}}}));
//# sourceMappingURL=SubmitCommand-legacy.23b6416b.js.map
