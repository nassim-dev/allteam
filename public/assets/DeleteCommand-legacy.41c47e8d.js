System.register(["./DefaultCommand-legacy.cdc77cc5.js","./index-legacy.71c7b92c.js","./index-legacy.fb656449.js","./vendor-legacy.f283aa6f.js","./event-handler-legacy.a8eba0d6.js"],(function(e,t){"use strict";var n,a;return{setters:[e=>{n=e.default},e=>{a=e.S},()=>{},()=>{},()=>{}],execute:function(){e("default",
/**
       * Class DeleteCommand
       *
       * Description
       *
       * @category  Description
       * @author    Nassim Ourami <nassim.ourami@mailo.com>
       * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
       * @version   Release: 0.1
       *
       * @link    https://allteam.io
       * @since   File available since Release 0.1
       * @package Allteam
       */
class extends n{execute(e,t){a.fire({title:"Voulez-vous supprimer cet élément ?",text:"Cette action est définitive!",icon:"warning",showCancelButton:!0,confirmButtonColor:"#3085d6",cancelButtonColor:"#d33",confirmButtonText:"Oui, supprimer"}).then((e=>{let n=this.app.datad.getDatasource(t.command.endpoint);e.isConfirmed&&this.app.ajaxd.call(n).then((e=>{}))}))}})}}}));
//# sourceMappingURL=DeleteCommand-legacy.41c47e8d.js.map
