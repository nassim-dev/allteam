import e from"./DefaultCommand.23718136.js";import"./index.a7ff2035.js";import"./vendor.9963c748.js";
/**
 *
 *  Class EndpointFilterCommand
 *
 * Represent  Base EndpointFilterCommand
 *
 * @category Represent html EndpointFilterCommand
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 *
 */class t extends e{execute(e,t){this.app.datad.fetch(t.endpoint,t.parameters)}}export{t as default};
//# sourceMappingURL=EndpointFilterCommand.cb106a6d.js.map
