<?php

$start_time = microtime(true);

const CONTEXT = 'APP';
define('BASE_DIR', __DIR__ . '/../');
require BASE_DIR . 'vendor/autoload.php';

use core\app\ApplicationServiceInterface;
use core\app\Bootstrap;
use core\cache\RedisCache;
use core\config\Conf;
use core\DI\DependenciesInjector;
use Tracy\Debugger;
use Tracy\OutputDebugger;

//OutputDebugger::enable();
//Debugger::enable(Debugger::DEVELOPMENT, BASE_DIR . 'logs');

$bootstrap = new Bootstrap(
    applicationClass:ApplicationServiceInterface::class,
    container:[
        'class' => DependenciesInjector::class,
        'cache' => RedisCache::class
    ],
    configClass:Config::class,
);

$bootstrap->startup(
    [
        'CONTEXT' => CONTEXT,
        'envFile' => 'config/env.neon'
    ]
);

require BASE_DIR . 'constants/default.php';
require BASE_DIR . 'functions/default.php';

$app = $bootstrap->buildApplication();

$app->boot(); //Startup actions
$response = $app->process(); //Render view
echo $response->html();
$app->shutdown(); //Execute shutdown callbacks


$end_time = microtime(true);

// Calculating the script execution time
$execution_time = $end_time - $start_time;

var_dump(' Execution time of script = ' . $execution_time . ' sec');

var_dump(Conf::find('ENVIRONNEMENT'));
