<?php

const CONTEXT = 'HOOK';
define('BASE_DIR', __DIR__ . '/../../');
require BASE_DIR . 'vendor/autoload.php';

use core\app\ApplicationServiceInterface;
use core\app\Bootstrap;
use core\cache\RedisCache;
use core\DI\DependenciesInjector;
use Tracy\OutputDebugger;

//OutputDebugger::enable();

$bootstrap = new Bootstrap(
    applicationClass:ApplicationServiceInterface::class,
    container:['class' => DependenciesInjector::class, 'cache' => RedisCache::class],
    configClass:Config::class
);

$bootstrap->startup(
    [
        'CONTEXT' => CONTEXT,
        'envFile' => 'config/env.neon'
    ]
);

require BASE_DIR . 'constants/default.php';
require BASE_DIR . 'functions/default.php';

/** @var ApplicationServiceInterface $app */
$app = $bootstrap->buildApplication();

$app->boot(); //Startup actions
$response = $app->process(); //Render view
echo $response->json();
$app->shutdown(); //Execute shutdown callbacks
