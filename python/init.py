import os
import json

from importlib import import_module

while True:
    for fileName in os.listdir('scripts'):
        realFile = os.path.join('scripts', fileName)
        if os.path.isfile(realFile):
            if realFile.__contains__(".py"):
                print ("Executing ", end="")
                importlib.import_module(realFile)
                os.remove(realFile)