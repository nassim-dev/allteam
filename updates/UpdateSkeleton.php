<?php

use core\database\RepositoryFactoryInterface;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$function = function (RepositoryFactoryInterface $repositoryFactory) {
    $tmp     = explode('/', __FILE__);
    $file    = end($tmp);
    $upgrade = $repositoryFactory->table('upgrade')->check($file);
    if (!is_bool($upgrade)) {
        //DO some stuff

        //then disable update
        $repositoryFactory->table('upgrade')->disable($upgrade);
    }
};

$function();
unset($function);
