FROM php:8.1.0-fpm-bullseye

#DEBIAN DEPENDANCIES
RUN apt-get update -y --fix-missing \
    && apt-get upgrade -y \
    && apt-get install -y \
    apt-utils \
    libfreetype6-dev \
    libmemcached-dev \
    libjpeg62-turbo-dev \
    libmcrypt-dev \
    libpng-dev \
    zlib1g-dev \
    libicu-dev \
    libzip-dev \
    sudo \
    wget \
    apt-utils \
    cron \
    nano \
    git\
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libpng-dev \
    libonig-dev \
    ntp \
    libsqlite3-dev  \
    libsqlite3-0 \
    libzmq3-dev \
    libxml2-dev \
    libsasl2-dev \
    libevent-dev \
    libc-client-dev \
    libkrb5-dev \
    logrotate \
    --no-install-recommends && rm -r /var/lib/apt/lists/*

#PHP EXTENSIONS
RUN docker-php-ext-install -j$(nproc) \
    iconv \
    intl \
    mbstring \
    opcache \
    zip \
    sockets \
    mbstring \
    pdo_sqlite \
    pdo_mysql \
    && docker-php-ext-enable iconv \
    && docker-php-ext-enable intl \
    && docker-php-ext-enable mbstring \
    && docker-php-ext-enable opcache \
    && docker-php-ext-enable zip \
    && docker-php-ext-enable sockets \
    && docker-php-ext-enable mbstring \
    && docker-php-ext-enable pdo_sqlite \
    && docker-php-ext-enable pdo_mysql

#GD
RUN docker-php-ext-install -j$(nproc) iconv \
    && docker-php-ext-configure gd \
    && docker-php-ext-install -j$(nproc) gd

#APC APCU : Not working with php 8.0
#RUN pecl install apcu \
#    && pecl install apcu_bc-1.0.3 \
#    && docker-php-ext-enable apcu --ini-name 10-docker-php-ext-apcu.ini \
#    && docker-php-ext-enable apc --ini-name 20-docker-php-ext-apc.ini

#PDO MYSQL
RUN docker-php-ext-install pdo pdo_mysql

#REDIS
RUN pecl install -o -f redis \
    &&  rm -rf /tmp/pear \
    &&  docker-php-ext-enable redis

#MEMCACHED
RUN pecl install memcached \
    && docker-php-ext-enable memcached

#MCRYPT
#RUN pecl install mcrypt \
#    && docker-php-ext-enable mcrypt

#CRON RULES
USER root
COPY cron/crontab /tmp/crontab
RUN crontab -u root /tmp/crontab \
    && service cron restart

#TIMEZONE && NTP
ARG TZ
ENV TZ=$TZ
RUN echo ${TZ} > /etc/timezone \
    && echo 'date.timezone=${TZ}' >> $PHP_INI_DIR/php.ini \
    && ln -sf /usr/share/zoneinfo/${TZ} /etc/localtime \
    && dpkg-reconfigure -f noninteractive tzdata \
    && service ntp restart

#PHP IMAP EXT
RUN docker-php-ext-configure imap --with-kerberos --with-imap-ssl \
    && docker-php-ext-install imap

#MONGODB
#RUN pecl install mongodb
#RUN echo "extension=mongodb.so" >> $PHP_INI_DIR/conf.d/mongodb.ini

#IGBINARY
#RUN pecl install igbinary
RUN git clone https://github.com/igbinary/igbinary \
    && cd igbinary \
    && phpize \
    && ./configure \
    && make \
    && make install
RUN echo "session.serialize_handler=igbinary" >> $PHP_INI_DIR/php.ini
RUN echo "extension=igbinary.so" >> $PHP_INI_DIR/conf.d/docker-php-ext-igbinary.ini
RUN echo "session.serialize_handler=igbinary" >> $PHP_INI_DIR/conf.d/docker-php-ext-igbinary.ini
RUN echo "apc.serializer=igbinary" >> $PHP_INI_DIR/conf.d/docker-php-ext-igbinary.ini

#PHP ZMQ
RUN pecl install ev \
    &&  pecl install event \
    && docker-php-ext-install soap

#RUN pecl install channel://pecl.php.net/zmq-1.1.3
RUN git clone https://github.com/zeromq/php-zmq.git \
    && cd php-zmq \
    && phpize \
    && ./configure \
    && make \
    && make install

RUN docker-php-ext-install intl
RUN docker-php-ext-enable intl

#Gettext extension
RUN docker-php-ext-install gettext

#PHP-FPM APC Configuration
RUN echo "apc.enable_cli=On" >> $PHP_INI_DIR/conf.d/20-docker-php-ext-apc.ini
RUN echo "apc.ttl=3600" >> $PHP_INI_DIR/conf.d/20-docker-php-ext-apc.ini

#ENABLE Memcached Session handler
RUN echo "session.save_handler=memcached" >> $PHP_INI_DIR/php.ini
RUN echo 'session.save_path="/tmp/memcached/memcached.sock"' >> $PHP_INI_DIR/php.ini
RUN echo 'memcached.sess_locking=Off' >> $PHP_INI_DIR/conf.d/docker-php-ext-memcached.ini

#PHP-FPM POOL CONFIGURATION
RUN echo "pm=ondemand"  >> /usr/local/etc/php-fpm.conf
RUN echo "pm.max_children=75" >> /usr/local/etc/php-fpm.conf
RUN echo "pm.process_idle_timeout=10s" >> /usr/local/etc/php-fpm.conf
RUN echo "pm.max_requests=500" >> /usr/local/etc/php-fpm.conf
RUN echo "pm.start_servers=10" >> /usr/local/etc/php-fpm.conf
RUN echo "pm.min_spare_servers=5" >> /usr/local/etc/php-fpm.conf
RUN echo "pm.max_spare_servers=30" >> /usr/local/etc/php-fpm.conf
RUN echo "request_slowlog_timeout=30" >> /usr/local/etc/php-fpm.conf
RUN echo "slowlog=/usr/local/var/log/php7-fpm.log.slow" >> /usr/local/etc/php-fpm.conf

#PHP-FPM Opcache configuration
ARG APP_PRELOAD_SCRIPT
RUN echo 'opcache.save_comments=1' >> $PHP_INI_DIR/conf.d/docker-php-ext-opcache.ini
RUN echo 'opcache.enable=1' >> $PHP_INI_DIR/conf.d/docker-php-ext-opcache.ini
RUN echo 'opcache.memory_consumption=512' >> $PHP_INI_DIR/conf.d/docker-php-ext-opcache.ini
RUN echo 'opcache.max_accelerated_files=5000' >> $PHP_INI_DIR/conf.d/docker-php-ext-opcache.ini
RUN echo 'opcache.validate_timestamps=1' >> $PHP_INI_DIR/conf.d/docker-php-ext-opcache.ini
RUN echo 'opcache.revalidate_freq=1' >> $PHP_INI_DIR/conf.d/docker-php-ext-opcache.ini
RUN echo "opcache.enable_file_override=1" >> $PHP_INI_DIR/conf.d/docker-php-ext-opcache.ini

#Disable environnment cleanning on php process
RUN echo "clear_env=no"  >> /usr/local/etc/php-fpm.d/www.conf

#Enable JIT
RUN echo "opcache.enable_cli=1" >> /usr/local/etc/php/conf.d/docker-php-ext-opcache.ini
RUN echo "opcache.jit=tracing" >> /usr/local/etc/php/conf.d/docker-php-ext-opcache.ini
RUN echo "opcache.jit_buffer_size=100M" >> /usr/local/etc/php/conf.d/docker-php-ext-opcache.ini

#WKHTMLTOX
#RUN cd /tmp \
#    && git clone https://github.com/krakjoe/wkhtmltox \
#    && cd wkhtmltox  \
#    && phpize
#RUN cd /tmp/wkhtmltox && ./configure --with-wkhtmltox=/opt/wkhtmltox/installation \
#    && make \
#    && make install \
#    && rm -rf /tmp/wkhtmltox

ARG APP_SOURCE_DIR
ONBUILD ADD $APP_SOURCE_DIR /var/www/html/

#XDEBUG
#RUN pecl install xdebug
#RUN docker-php-ext-enable xdebug
#ARG XDEBUG_IDE_KEY
#RUN echo "xdebug.mode=debug" >> $PHP_INI_DIR/conf.d/xdebug.ini
#RUN echo "xdebug.client_port=9000" >> $PHP_INI_DIR/conf.d/xdebug.ini
#RUN echo "xdebug.idekey=${XDEBUG_IDE_KEY}" >> $PHP_INI_DIR/conf.d/xdebug.ini
#RUN echo "xdebug.client_host=host.docker.internal" >> $PHP_INI_DIR/conf.d/xdebug.ini
#RUN echo "xdebug.start_with_request=yes" >> $PHP_INI_DIR/conf.d/xdebug.ini

#Blackfire
#RUN apt-get update && apt-get install gnupg2 -y
#RUN wget -q -O - https://packages.blackfire.io/gpg.key | sudo apt-key add -
#RUN echo "deb http://packages.blackfire.io/debian any main" | sudo tee /etc/apt/sources.list.d/blackfire.list
#RUN apt-get update && apt-get install blackfire -y
#
#ARG BLACKFIRE_SERVER_TOKEN
#ARG BLACKFIRE_SERVER_ID
#ARG BLACKFIRE_CLIENT_ID
#ARG BLACKFIRE_CLIENT_TOKEN
#ARG BLACKFIRE_AGENT_SOCKET
#
#RUN blackfire agent:config --server-id=${BLACKFIRE_SERVER_ID} --server-token=${BLACKFIRE_SERVER_TOKEN}
#RUN sudo /etc/init.d/blackfire-agent restart
#RUN apt-get install blackfire-php -y
#RUN blackfire client:config --client-id=${BLACKFIRE_CLIENT_ID} --client-token=${BLACKFIRE_CLIENT_TOKEN}
#
#
RUN version=$(php -r "echo PHP_MAJOR_VERSION.PHP_MINOR_VERSION;") \
    && architecture=$(uname -m) \
    && curl -A "Docker" -o /tmp/blackfire-probe.tar.gz -D - -L -s https://blackfire.io/api/v1/releases/probe/php/linux/$architecture/$version \
    && mkdir -p /tmp/blackfire \
    && tar zxpf /tmp/blackfire-probe.tar.gz -C /tmp/blackfire \
    && mv /tmp/blackfire/blackfire-*.so $(php -r "echo ini_get ('extension_dir');")/blackfire.so \
    && printf "extension=blackfire.so\nblackfire.agent_socket=tcp://blackfire:8307\n" > $PHP_INI_DIR/conf.d/blackfire.ini \
    && rm -rf /tmp/blackfire /tmp/blackfire-probe.tar.gz

# Please note that the Blackfire Probe is dependent on the session module.
# If it isn't present in your install, you will need to enable it yourself.

#MAP LOCAL HOST USER
ARG UID
ARG GID

RUN groupadd -f -g 1000 hostuser
RUN useradd -u 1000 -g 1000 hostuser
USER hostuser