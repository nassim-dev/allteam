import { Plugin } from 'vite';
import { CustomPluginOptions } from 'rollup';
import { resolve } from 'path/posix';
export const absolutifyPaths = (options: CustomPluginOptions = {}): Plugin => {
  const { strings = [], enforce = 'pre', apply = 'serve' } = options;
  return {
    name: 'absolutify-paths',
    enforce: enforce,
    apply: apply,
    transform: (code: string, id: string) => {
      let transformedCode = code;
      strings.forEach((str) => {
        if (typeof str[1] == 'function') {
          transformedCode = str[1](transformedCode, str[0]);
        } else {
          if (code.includes(str[0])) {
            transformedCode = transformedCode.split(str[0]).join(str[1]);
          }
        }
        transformedCode.replace('allteam.localhost', 'localhost:3000');
      });
      return {
        code: transformedCode,
        map: null,
      };
    },
  };
};
