<?php

$config = new PhpCsFixer\Config();

return $config->setRules([
    '@PSR12'                                 => true,
    'array_indentation'                      => true,
    'array_syntax'                           => ['syntax' => 'short'],
    'combine_consecutive_unsets'             => true,
    'multiline_whitespace_before_semicolons' => true,
    'single_quote'                           => true,
    'binary_operator_spaces'                 => [
        'operators' => [
            '=>' => 'align_single_space_minimal',
            '='  => 'align_single_space_minimal',
        ]
    ],
    #'blank_line_before_statement'                   => true,
    'braces'                                        => ['allow_single_line_closure' => true],
    'concat_space'                                  => ['spacing' => 'one'],
    'declare_equal_normalize'                       => true,
    'function_typehint_space'                       => true,
    'include'                                       => true,
    'lowercase_cast'                                => true,
    'no_blank_lines_after_phpdoc'                   => true,
    'method_chaining_indentation'                   => true,
    'phpdoc_trim_consecutive_blank_line_separation' => true,
    'no_multiline_whitespace_around_double_arrow'   => true,
    'no_spaces_around_offset'                       => true,
    'no_trailing_comma_in_singleline'               => true,
    'no_unused_imports'                             => true,
    'no_whitespace_before_comma_in_array'           => true,
    'no_whitespace_in_blank_line'                   => true,
    'object_operator_without_whitespace'            => true,
    'phpdoc_align'                                  => true,
    'phpdoc_indent'                                 => true,
    'combine_consecutive_issets'                    => true,
    'ternary_to_null_coalescing'                    => true,
    'phpdoc_trim'                                   => true,
    'phpdoc_types'                                  => true,
    'align_multiline_comment'                       => true,
    'single_blank_line_before_namespace'            => true,
    'standardize_not_equals'                        => true,
    'ternary_operator_spaces'                       => true,
    'trim_array_spaces'                             => true,
    'unary_operator_spaces'                         => true,
    'whitespace_after_comma_in_array'               => true,
    //'no_trailing_comma_in_singleline_array' => true,
])
    ->setLineEnding("\n");
