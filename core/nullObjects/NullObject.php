<?php

namespace core\nullObjects;

/**
 * Class AbstractNullObject
 *
 * Does nothing exept returning null
 *
 * @category Does nothing exept returning null
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class NullObject
{
    private static $instance;

    private function __construct()
    {
    }

    /**
     * @param $name
     * @param $arguments
     *
     * @return bool
     */
    public function __call($name, $arguments)
    {
        if ('isNull' === $name) {
            return $this->isNull();
        }
    }

    public static function get()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @return void
     */
    public function __get(string $name)
    {
    }

    /**
     * @return void
     */
    public function __isset(string $name)
    {
    }

    /**
     * @param $value
     * @return void
     */
    public function __set(string $name, $value)
    {
    }

    /**
     * @return void
     */
    public function __unset(string $name)
    {
    }

    /**
     * Undocumented function
     */
    public function isNull(): bool
    {
        return true;
    }
}
