<?php

namespace core\mail;

use core\config\Config;
use core\html\HtmlElementInterface;
use core\html\traits\Contenable;
use core\tasks\AsyncJob;
use core\tasks\SendMailTask;
use core\utils\Utils;
use core\view\ViewServiceInterface;
use PHPMailer\PHPMailer\PHPMailer;

/**
 * Class Mail
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class Mail extends PHPMailer implements HtmlElementInterface
{
    use Contenable;

    /**
     * Html template to use
     *
     * @var string|null
     */
    protected $htmlTemplate = null;

    private array $_arrayConfig = [];

    private mixed $_array_content = null;

    private mixed $_contents = null;

    //Mail HTML
    private array $_link = [];

    private mixed $_mail_bottom = null;

    private mixed $_mail_top = null;

    /**
     * @var mixed
     */
    private $_openedLink = null;

    private mixed $_userName = null;

    public function __construct(private Config $config)
    {
        $mode = ($this->config->find('ENVIRONNEMENT') === 'development') ? 'SANDBOX' : 'SMTP';
        parent::__construct();
        $this->isSMTP();

        $this->CharSet    = $this->config->find('PHP.ENCODING');
        $this->Mailer     = 'smtp';
        $this->Host       = $this->config->find("SMTP.$mode.SERVER");
        $this->SMTPAuth   = ($this->config->find("SMTP.$mode.SECURE"));
        $this->SMTPSecure = $this->config->find("SMTP.$mode.SECURE");
        $this->Username   = $this->config->find("SMTP.$mode.USER");
        $this->Password   = $this->config->find("SMTP.$mode.PASSWORD");
        $this->Port       = $this->config->find("SMTP.$mode.PORT");
        $this->isHTML($this->config->find("SMTP.$mode.HTML"));
        $this->SMTPDebug = $this->config->find("SMTP.$mode.DEBUG");
        $this->CharSet   = $this->config->find('SMTP.ENCODING');
        $this->From      = $this->config->find("SMTP.$mode.USER");
        $this->FromName  = $this->config->find('SMTP.FROM');
        $this->addCustomHeader('X-ProjectProvider', $this->config->find('SMTP.HEADER_PROVIDER'));
    }

    public function getReference(): ?string
    {
        return spl_object_id();
    }

    //Start HtmlElementInterrface implementation
    /**
     * Return Id attribute for html element
     */
    public function getIdAttribute(): string
    {
        return $this->getViewReference() . '_' . uniqid();
    }

    /**
     * Reference for smarty assignment
     */
    public function getViewReference(): string
    {
        $class = $this::class;
        $class = explode('\\', $class);

        return strtolower(end($class));
    }

    /**
     * Set html template to use
     *
     * @PhpUnitGen\set("htmlTemplate")
     *
     * @param string $htmlTemplate Html template to use
     */
    public function setHtmlTemplate(string $htmlTemplate): self
    {
        $this->htmlTemplate = $htmlTemplate;

        return $this;
    }

    /**
     * List of allowed childs elements
     * could be a class, an abstract class or an interface
     */
    protected static function getAllowedChilds(): array
    {
        return [];
    }

    /**
     * Get html template to use
     *
     * @PhpUnitGen\get("htmlTemplate")
     */
    public function getHtmlTemplate(): string
    {
        return $this->htmlTemplate ?? self::getBaseTemplateDir() . 'mail/Mail.html';
    }

    /**
     * Return template directory
     */
    public static function getBaseTemplateDir(): string
    {
        return BASE_DIR . 'templates/components/';
    }

    //End HtmlElementInterrface implementation

    /**
     * @return mixed
     */
    public function getArrayContent()
    {
        return $this->_array_content;
    }

    /**
     * @return mixed
     */
    public function getContents()
    {
        return $this->_contents;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->_link;
    }

    /**
     * @return mixed
     */
    public function getMailBottom()
    {
        return $this->_mail_bottom;
    }

    /**
     * @return mixed
     */
    public function getMailTop()
    {
        return $this->_mail_top;
    }

    /**
     * @param $lvl
     */
    public function getTpl(ViewServiceInterface $view)
    {
        if (!empty($this->getArrayContent())) {
            $view->assign('main_content', $this->getArrayContent());
        }

        if (!empty($this->getMailBottom())) {
            $view->assign('content_bottom', $this->getMailBottom());
        }

        if (!empty($this->getMailTop())) {
            $view->assign('content_top', $this->getMailTop());
        }

        if (!empty($this->getLink())) {
            $view->assign('link', $this->getLink());
        }

        if (!empty($this->getUserName())) {
            $view->assign('userName', $this->getUserName());
        }

        $view->assign('ADDRESS', $this->config->find('PHP.HTTP_PROTOCOL') . '://' . $this->config->find('PHP.SERVER_NAME'));

        if (!null === $this->_openedLink) {
            $view->assign('OPENEDLINK', $this->_getOpenedLink());
        }

        $this->setContents($view->fetch(BASE_DIR . '/templates/mail/mail.html'));

        //echo ($this->getContents());
        $this->Body = $this->getContents();
        $this->_Mail->setContentTop(Utils::serialize($this->Body));
    }

    /**
     * Get the value of _userName
     */
    public function getUserName()
    {
        return $this->_userName;
    }

    public function save()
    {
        /** @var AsyncJob $task */
        $task = $this->di->singleton(AsyncJob::class)
            ->setCallable(SendMailTask::class)
            ->setContext([
                'mail' => $this
            ]);
        $task->save();
    }

    public function send(): bool
    {
        $this->Body = $this->getContents();

        return parent::send();
    }

    /**
     * @param $var
     */
    public function setArrayContent(array $var)
    {
        if (is_array($var)) {
            $this->_array_content = $var;
        } else {
            $this->_array_content[] = $var;
        }
    }

    /**
     * Return the array of attachments.
     * @return array
     * @param  mixed[] $attachment
     */
    public function setAttachments(array $attachment)
    {
        $this->attachment = $attachment;
    }

    /**
     * @param $var
     */
    public function setContents(string $var)
    {
        $this->_contents = $var;
    }

    public function setLink(array $var)
    {
        $this->_link['href'] = $var['href'];
        $this->_link['text'] = $var['text'];
    }

    /**
     * @param $var
     */
    public function setMailBottom(string $var)
    {
        $this->_mail_bottom = $var;
    }

    /**
     * @param $var
     */
    public function setMailTop(string $var)
    {
        $this->_mail_top = $var;
    }

    public function setOpenedLink(array $params = [])
    {
        if (!empty($params)) {
            $this->_openedLink = $this->config->find('PHP.HTTP_PROTOCOL') . '://' . $this->config->find('PHP.SERVER_NAME') . '/api/utils/' . Utils::encrypt(Utils::serialize($params)) . '/opener';
        }
    }

    /**
     * Set the value of _userName
     */
    public function setUserName(string $_userName): self
    {
        $this->_userName = $_userName;

        return $this;
    }

    /**
     * @return mixed
     */
    private function _getOpenedLink()
    {
        return $this->_openedLink;
    }

    public function toArray(): array
    {
        return [];
    }
}
