<?php

namespace core\mail;

use core\factory\FactoryInterface;
use core\html\HtmlElementFactory;

/**
 * Class MailFactory
 *
 * Mail factory
 *
 * @category  Mail factory
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class MailFactory extends HtmlElementFactory implements FactoryInterface
{
    /**
     * Return element class to build
     */
    protected function getElementClass(): string
    {
        return Mail::class;
    }

    /**
     * Return suffix for widget classes
     */
    protected function getWidgetSuffix(): string
    {
        return 'MailWidget';
    }

    /**
     * Create new instance or return singleton if exist
     *
     * @PhpUnitGen\assertInstanceOf("Mail::class")
     */
    public function create(string $identifier, array $params = []): ?Mail
    {
        return parent::create($identifier, $params);
    }

    /**
     * Create from WidgetInterface
     */
    public function createFromWidget(string $className, array $params): ?Mail
    {
        return parent::createFromWidget($className, $params);
    }

    public function get(string $identifier): ?Mail
    {
        return $this->getInstance($identifier) ?? $this->create($identifier);
    }
}
