<?php

namespace core\tasks;

use core\config\Conf;
use core\database\DatabaseManagerInterface;
use Nette\Neon\Neon;
use Nette\Utils\Finder;
use SplFileInfo;

/**
 * Class SendMailTask
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class DownloadExtensionTask implements TaskInterface
{
    public const EXTENSION_PATH = BASE_DIR . 'extensions/';

    public function __construct(private DatabaseManagerInterface $database)
    {
    }

    public function run(array $context)
    {
        if (isset($context['update'])) {
            $directories = Finder::findDirectories()->in(self::EXTENSION_PATH);
            foreach ($directories as $extensionPath) {
                $this->update($extensionPath);
            }

            exec('composer dump-autoload');

            return;
        }

        if (!isset($context['url'])) {
            throw new TaskException('Git repository url is missing!');
        }

        $this->download($context['url'], self::EXTENSION_PATH);
        $this->updateManifest($context['ur']);
        exec('composer dump-autoload');
        $this->reanameMigrationsFiles($context['url']);
    }

    public function reanameMigrationsFiles(string $url)
    {
        //$lastMigration = $this->database->getOrm('phinxlog')->max('version');
        $namespace = $this->findGitRepository($url);
        $files     = Finder::findFiles('*.php')
            ->in(self::EXTENSION_PATH . $namespace . '/config/db/migrations');

        $uuid = date('Ymdhis');

        /** @var SplFileInfo $file */
        foreach ($files as $file) {
            $filename    = explode('_', $file->getFilename());
            $filename[0] = $uuid++;
            rename($file->getPathname(), $file->getPath() . '/' . implode($filename));
        }
    }

    private function download(string $path)
    {
        exec("cd $path && git clone");
    }

    private function update(string $extensionPath)
    {
        exec("cd $extensionPath && git pull");
    }

    private function findGitRepository(string $url)
    {
        $components = parse_url($url);

        return (str_ends_with('.git', $components['path'])) ? str_replace('.git', '', $components['path']) : $components['path'];
    }

    private function updateManifest(string $url)
    {
        $manifest = Neon::decode(file_get_contents('nette.safe://' . self::EXTENSION_PATH . 'manifest.neon'));

        $namespace = $this->findGitRepository($url);
        $files     = Finder::findFiles('*Extension.php')->in(self::EXTENSION_PATH . $namespace);

        /** @var SplFileInfo $file */
        foreach ($files as $file) {
            $className = "extensions\\$namespace\\" . $file->getBasename('.php');
            if (isset($manifest[$className])) {
                continue;
            }

            Conf::saveInFile(self::EXTENSION_PATH . 'manifest.neon', $className, [
                'isEnabled'   => false,
                'isInstalled' => false,
                'url'         => null,
            ]);
        }
    }
}
