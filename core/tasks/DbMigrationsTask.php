<?php

namespace core\tasks;

use core\logger\LogServiceInterface;
use Symfony\Component\Process\Process;

/**
 * Class DbMigrationsTask
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class DbMigrationsTask implements TaskInterface
{
    public function __construct(private LogServiceInterface $logger)
    {
    }

    public function run(array $context)
    {
        $process = Process::fromShellCommandline('/usr/local/bin/php -f /var/www/html/vendor/bin/phinx migrate --configuration /var/www/html/config/phinx.php');


        $process->start();
        while (!$process->isTerminated()) {
            if ($process->getOutput() != '' && $process->getOutput() != null) {
                $this->logger->dump(
                    [
                        'data'    => $process->getOutput(),
                        'logfile' => ASYNC_LOGFILE
                    ]
                );
            }
        }

        $this->logger->dump(
            [
                'data'    => $process->getErrorOutput(),
                'logfile' => ASYNC_LOGFILE
            ]
        );
    }
}
