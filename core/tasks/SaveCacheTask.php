<?php

namespace core\tasks;

use core\cache\CacheServiceInterface;

/**
 * Class SaveCacheTask
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class SaveCacheTask implements TaskInterface
{
    public function __construct(private CacheServiceInterface $cache)
    {
    }

    public function run(array $context)
    {
        $this->cache->setCachePrefix($context['context']);
        $this->cache->setMulti($context['queue']);
    }
}
