<?php

namespace core\tasks;

use core\cache\CacheServiceInterface;
use core\config\Conf;
use core\DI\DiProvider;
use core\extensions\ExtensionInterface;
use core\extensions\Launcher;
use core\logger\LogServiceInterface;
use Nette\Neon\Decoder;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class InstallExtensionTask implements TaskInterface
{
    use DiProvider;

    public function __construct(
        private LogServiceInterface $logger,
        private CacheServiceInterface $cache,
        private Decoder $decoder
    ) {
    }

    public function run(array $context)
    {
        extract($context, EXTR_OVERWRITE);

        /** @var ExtensionInterface $extension */
        $extension  = $this->getDi()->singleton($context['extension']);
        $configFile = 'nette.safe://' . BASE_DIR . Launcher::CONFIG_FILE;
        $manifest   = $this->decoder->decode(file_get_contents($configFile));

        if ($manifest[$extension::class]['isInstalled']) {
            return;
        }

        if ($manifest[$extension::class]['isInstalling']) {
            $this->logger->dump(
                [
                    'data'    => $context['extension'] . ' installation still in progress...',
                    'logfile' => APP_LOGFILE
                ]
            );

            return;
        }

        Conf::saveInFile(Launcher::CONFIG_FILE, $extension::class . '.isInstalling', true);

        $this->logger->dump(
            [
                'data'    => 'Start installation of : ' . $context['extension'],
                'logfile' => APP_LOGFILE
            ]
        );

        $output = [];

        $this->logger->dump(
            [
                'data'    => $output[0] ?? 'No migrations required.',
                'logfile' => APP_LOGFILE
            ]
        );

        $extension->install();
        $extension->compile();

        //$contexts = ['APP', 'API', 'CLI', 'HOOK'];
        //foreach ($contexts as $context) {
        //    $this->cache->setCachePrefix($context);
        //    $this->updateCache($extension);
        //}

        $this->logger->dump(
            [
                'data'    => $context['extension'] . ' has been installed !',
                'logfile' => APP_LOGFILE
            ]
        );

        Conf::saveInFile(Launcher::CONFIG_FILE, $extension::class . '.isInstalling', false);
        Conf::saveInFile(Launcher::CONFIG_FILE, $extension::class . '.isInstalled', true);

        //todo send alert to user
        //todo Install extension
    }

    private function updateCache(ExtensionInterface $extension)
    {
        //$this->cache->invalidate(md5(Launcher::class) . '._manifest');
        $this->cache->set(md5($extension::class) . '._controllers', $extension->getControllers());
        $this->cache->set(md5($extension::class) . '._entities', $extension->getEntities());
        $this->cache->set(md5($extension::class) . '._listeners', $extension->getListeners());
        $this->cache->set(md5($extension::class) . '._models', $extension->getModels());
        $this->cache->set(md5($extension::class) . '._widgetNamespaces', $extension->getWidgetNamespaces());
    }
}
