<?php

namespace core\tasks;

use core\database\RepositoryFactoryInterface;

/**
 * Class DeepcopyTask
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class DeepcopyTask implements TaskInterface
{
    public function __construct(private RepositoryFactoryInterface $repositoryFactory)
    {
    }

    public function run(array $context)
    {
        extract($context, EXTR_OVERWRITE);

        $object = $this->repositoryFactory->table($resource)->getById($initialId);
        if (!is_bool($object)) {
            $this->repositoryFactory->table($resource)->copy($object, $formVarsCopy);
        }
    }
}
