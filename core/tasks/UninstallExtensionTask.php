<?php

namespace core\tasks;

use core\cache\CacheServiceInterface;
use core\DI\DiProvider;
use core\extensions\ExtensionInterface;
use core\extensions\Launcher;
use core\logger\LogServiceInterface;
use core\utils\Utils;
use Nette\Neon\Decoder;
use Nette\Neon\Encoder;
use Nette\Neon\Neon;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class UninstallExtensionTask implements TaskInterface
{
    use DiProvider;

    public function __construct(
        private LogServiceInterface $logger,
        private CacheServiceInterface $cache
    ) {
    }

    public function run(array $context)
    {
        extract($context, EXTR_OVERWRITE);

        /** @var ExtensionInterface $extension */
        $extension  = $this->getDi()->singleton($context['extension']);
        $encoder    = $this->getDi()->singleton(Encoder::class);
        $configFile = 'nette.safe://' . BASE_DIR . Launcher::CONFIG_FILE;
        $manifest   = $this->getDi()->singleton(Decoder::class)->decode(file_get_contents($configFile));

        if ($manifest[$extension::class]['isInstalled']) {
            return;
        }

        if ($manifest[$extension::class]['isInstalling']) {
            $this->logger->dump(
                [
                    'data'    => $context['extension'] . ' uninstallation still in progress...',
                    'logfile' => APP_LOGFILE
                ]
            );

            return;
        }

        $manifest[$extension::class]['isInstalling'] = true;
        file_put_contents($configFile, $encoder->encode($manifest, Neon::BLOCK));



        $this->logger->dump(
            [
                'data'    => 'Start uninstallation of : ' . $context['extension'],
                'logfile' => APP_LOGFILE
            ]
        );

        $extension->uninstall();
        Utils::rmFiles($extension->getBaseDir());
        unset($manifest[$extension::class]);

        file_put_contents($configFile, $encoder->encode($manifest, Neon::BLOCK));

        $this->logger->dump(
            [
                'data'    => $context['extension'] . ' has been uninstalled!',
                'logfile' => APP_LOGFILE
            ]
        );

        //todo send alert to user
        //todo Install extension
    }

    private function updateCache(ExtensionInterface $extension)
    {
        $this->cache->invalidate(md5(Launcher::class) . '._manifest');
    }
}
