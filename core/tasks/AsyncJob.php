<?php

namespace core\tasks;

use core\config\Config;
use core\DI\DI;
use core\DI\DiProvider;
use core\logger\LogServiceInterface;
use core\messages\queue\MessageQueueInterface;
use core\secure\authentification\AuthServiceInterface;

/**
 * Class AsyncJob
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class AsyncJob
{
    use DiProvider;

    public const USER_CHANNEL    = 'USER_CHANNEL';
    public const CONTEXT_CHANNEL = 'CONTEXT_CHANNEL';

    protected int $retryCount = 0;

    /**
     * Arguments
     *
     * @var array
     */
    public $context = [];

    /**
     * Class name of function
     *
     * @var mixed
     */
    protected $callable = null;

    /**
     * Socket channels
     *
     * @var array
     */
    protected array $socketChannels = [];

    /**
     * @return static
     */
    public function __construct(
        protected ?string $id,
        protected MessageQueueInterface $queue,
        protected LogServiceInterface $logger,
        protected Config $config,
    ) {
    }

    public function retry()
    {
        $this->retryCount++;
        $maxRetry = $this->config->find('WORKER.MAX_TASK_RETRY') ?? 1;
        if ($this->retryCount > $maxRetry) {
            $this->logger->dump(
                [
                    'data'    => "Task failed after $maxRetry try, please check logs. The task have been pushed into manual queue : " . $this->callable,
                    'logfile' => ASYNC_LOGFILE
                ]
            );
            $this->save('manual');

            return;
        }

        $this->save();
    }


    /**
     * Get the value of id
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * Get the value of socketChannels
     *
     * @PhpUnitGen\get("socketChannels")
     */
    public function getSocketChannels(): array
    {
        return $this->socketChannels;
    }


    /**
     * @return null
     */
    public function run()
    {
        if (!class_exists($this->callable)) {
            $this->logger->dump(
                [
                    'data'    => 'Innexisting ' . TaskInterface::class . ' : ' . $this->callable,
                    'logfile' => ASYNC_LOGFILE
                ]
            );

            return;
        }

        /** @var TaskInterface $task */
        $task                             = $this->getDi()->singleton($this->callable);
        $this->context['socketsChannels'] = $this->getSocketChannels();

        $task->run($this->context);
    }

    public function save(string $priority = 'normal')
    {
        if (null === $this->callable && $this->id !== null) {
            $this->setCallable($this->id);
        }

        $this->queue->push($this, 'queue.priority.' . $priority);
    }

    public function __sleep()
    {
        return ['context', 'callable', 'id', 'socketChannels', 'retryCount'];
    }

    public function __wakeup()
    {
        $this->di     = DI::getContainer()->getWeakReference();
        $this->queue  = $this->getDi()->singleton(MessageQueueInterface::class);
        $this->logger = $this->getDi()->singleton(LogServiceInterface::class);
        $this->config = $this->getDi()->singleton(Config::class);
        $this->auth   = $this->getDi()->singleton(AuthServiceInterface::class);
    }


    /**
     * Set the value of callable
     */
    public function setCallable(string $callable): self
    {
        $this->callable = $callable;

        return $this;
    }

    /**
     * Set the value of context
     * @param mixed[] $context
     */
    public function setContext(array $context): self
    {
        $this->context = $context;

        return $this;
    }

    /**
     * Set the value of socketChannels
     *
     * @PhpUnitGen\set("socketChannels")
     *
     * @param mixed[] $socketChannels
     */
    public function setSocketChannels(array $socketChannels): self
    {
        $this->socketChannels = $socketChannels;

        return $this;
    }

    /**
     * Get the value of callable
     *
     * @PhpUnitGen\get("callable")
     */
    public function getCallable(): ?string
    {
        return $this->callable;
    }
}
