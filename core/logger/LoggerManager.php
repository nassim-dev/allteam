<?php

namespace core\logger;

/**
 * Class Log
 *
 * Provide log manipulation
 *
 * @category Provide log manipulation
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class LoggerManager implements LogServiceInterface
{
    /**
     * Loggers
     * @var LoggerInterface[]
     */
    private array $loggers = [];

    /**
     * Add log to show
     */
    public function dump(array $params)
    {
        /** @var LoggerInterface $logger */
        foreach ($this->loggers as $logger) {
            $logger->dump($params);
        }
    }


    /**
     * Add log and replace last line
     *
     * @return void
     */
    public function dumpLastLine(array $params)
    {
        /** @var LoggerInterface $logger */
        foreach ($this->loggers as $logger) {
            $logger->dumpLastLine($params);
        }
    }


    /**
     * Erase Logs
     */
    public function erase(array $params = [])
    {
        foreach ($this->loggers as $logger) {
            $logger->erase($params);
        }
    }

    public function registerLogger(LoggerInterface $logger)
    {
        $this->loggers[$logger::class] = $logger;
    }

    /**
     * Show logs
     *
     * @return mixed
     */
    public function view(array $params = [])
    {
        foreach ($this->loggers as $logger) {
            $logger->view($params);
        }
    }
}
