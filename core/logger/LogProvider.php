<?php

namespace core\logger;

use core\DI\DiProvider;

/**
 * Trait LogProvider
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait LogProvider
{
    use DiProvider;

    /**
     * LogServiceInterface implémentation
     *
     * @var LogServiceInterface
     */
    private $log;

    /**
     * Return LogServiceInterface implémetation
     */
    public function getLog(): LogServiceInterface
    {
        if (null === $this->log) {
            $this->log = $this->getDi()->singleton(LogServiceInterface::class);
        }

        return $this->log;
    }
}
