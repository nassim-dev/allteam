<?php

namespace core\logger\auth;

use core\secure\authentification\AuthServiceInterface;
use Nette\Utils\Helpers;
use Tracy\IBarPanel;

/**
 * Debug panel for AuthServiceInterface
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class TracyAuthPanel implements IBarPanel
{
    public function __construct(private AuthServiceInterface $auth)
    {
    }

    public function getTab(): string
    {
        return Helpers::capture(
            function (): void {
                $isConnected = $this->auth->isConnected() ? _('Connected') : _('Disconnected');
                $isAllowed   = $this->auth->isAllowed() ? _('Allowed') : _('Not allowed');

                require __DIR__ . '/templates/TracyAuthPanel.tab.phtml';
            }
        );
    }

    public function getPanel(): ?string
    {
        return Helpers::capture(
            function (): void {
                $permissions = $this->auth->getUser()?->getPermissions() ?? [];
                $config      = [
                    'User'           => $this->auth->getUser()?->getEmail() ?? _('No user'),
                    'Context'        => 'Id #' . $this->auth->getContext()?->getIdcontext() ?? _('No context'),
                    'Is connected ?' => $this->auth->isConnected() ? _('Connected') : _('Disconnected'),
                    'Is allowed ?'   => $this->auth->isAllowed() ? _('Allowed') : _('Not allowed'),
                    'IP validity'    => $this->auth->getStatus($this->auth::IP_VALIDITY_TEST) ? "<i class='fas fa-check'></i>" : _('Invalid'),
                    'Server adress'  => $this->auth->getStatus($this->auth::SERVER_ADRESS_TEST) ? "<i class='fas fa-check'></i>" : _('Invalid'),
                    'Server name'    => $this->auth->getStatus($this->auth::SERVER_NAME_TEST) ? "<i class='fas fa-check'></i>" : _('Invalid'),
                    'Session age'    => $this->auth->getStatus($this->auth::SESSION_AGE_TEST) ? "<i class='fas fa-check'></i>" : _('Too old')
                ];

                require __DIR__ . '/templates/TracyAuthPanel.panel.phtml';
            }
        );
    }
}
