<?php

namespace core\logger;

/**
 * Class ConsoleLogger
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class ConsoleLogger implements LoggerInterface
{
    /**
     * Add log and replace last line
     *
     * @return void
     */
    public function dumpLastLine(array $params)
    {
        $this->dump($params);
    }

    /**
     * Add log to show
     *
     * @param bool $lineBreak
     */
    public function dump(array $params)
    {
        $pid = getmypid();
        echo date("Y-m-d\TH:i:s") . substr((string)microtime(), 1, 8) . " [Process {$pid}] " . $params['data'] . "\n";
    }

    /**
     * Erase Logs
     */
    public function erase(array $params = [])
    {
    }

    /**
     * Show logs
     *
     * @return mixed
     */
    public function view(array $params = [])
    {
        return null;
    }
}
