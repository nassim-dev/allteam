<?php

namespace core\logger;

use core\utils\Utils;
use Exception;

/**
 * Class FileLogger
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class FileLogger implements LoggerInterface
{
    /**
     * Add log and replace last line
     *
     * @return void
     */
    public function dumpLastLine(array $params)
    {
        $baseDirectory = $params['baseDirectory'] ?? $_ENV['PHP_LOG_DIR'] ?? BASE_DIR . 'logs/';
        if (!isset($params['logfile'])) {
            throw new Exception("You must profide parameter 'logfile'");
        }

        $filename = $baseDirectory . $params['logfile'] ?? 'default.log';
        $size     = filesize($filename);
        $block    = 8192;
        $trunc    = max($size - $block, 0);

        $stream = fopen('nette.safe://' . $filename, 'c+');
        if (!is_resource($stream)) {
            return;
        }

        fseek($stream, $trunc);
        $bin = rtrim(fread($stream, $block), "\r");
        if ($r = strrpos($bin, "\r")) {
            ftruncate($stream, $trunc + $r + 1);
        }

        fputs($stream, date('[d/m/Y H:i:s]') . ' ' . $params['data'] ?? 'Nothing to log');
        fputs($stream, "\r");
        fclose($stream);
    }


    /**
     * Add log to show
     *
     * @param bool $lineBreak
     */
    public function dump(array $params)
    {
        $baseDirectory = $params['baseDirectory'] ?? $_ENV['PHP_LOG_DIR'] ?? BASE_DIR . 'logs/';
        if (!isset($params['logfile'])) {
            throw new Exception("You must profide parameter 'logfile'");
        }

        $datas = date('[d/m/Y H:i:s]') . ' ' . $params['data'] ?? 'Nothing to log';
        $file  = $baseDirectory . $params['logfile'] ?? 'default.log';

        if (isset($params['async'])) {
            $this->asyncSaveInFile($file, $datas);

            return;
        }

        $stream = fopen('nette.safe://' . $file, 'a');
        if (!is_resource($stream)) {
            return;
        }

        fputs($stream, $datas);
        fputs($stream, "\r");
        fclose($stream);
    }

    private function asyncSaveInFile(string $file, mixed $newValue)
    {
        Utils::mkdir(BASE_DIR . 'tmp/files_operations');
        $filename = microtime(true) . '_' . uniqid() . '_file_migration.php';

        $contents = '<?php
        $streams["' . $file . '"] =  $streams["' . $file . '"] ?? fopen("' . $file . '", "a");
        fputs($streams["' . $file . '"],' . var_export($newValue, true) . ');
        fputs($streams["' . $file . '"], "\r");';

        file_put_contents('nette.safe://' . BASE_DIR . 'tmp/files_operations/' . $filename, $contents);
    }


    /**
     * Erase Logs
     */
    public function erase(array $params = [])
    {
        $baseDirectory = $params['baseDirectory'] ?? $_ENV['PHP_LOG_DIR'] ?? BASE_DIR . 'logs/';
        if (!isset($params['logfile'])) {
            throw new Exception("You must profide parameter 'logfile'");
        }

        $stream = fopen('nette.safe://' . $baseDirectory . $params['logfile'], 'w');
        if (!is_resource($stream)) {
            return;
        }

        fputs($stream, date('[d/m/Y H:i:s]') . ' Erase log file');
        fputs($stream, "\r");
        fclose($stream);
    }

    /**
     * Show logs
     *
     * @return mixed
     */
    public function view(array $params = [])
    {
        $baseDirectory = $params['baseDirectory'] ?? $_ENV['PHP_LOG_DIR'] ?? BASE_DIR . 'logs/';
        if (!isset($params['logfile'])) {
            throw new Exception("You must profide parameter 'logfile'");
        }

        $file = 'nette.safe://' . $baseDirectory . $params['logfile'];
        if (file_exists($file)) {
            return file_get_contents($file);
        }

        return null;
    }
}
