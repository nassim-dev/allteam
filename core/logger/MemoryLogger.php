<?php

namespace core\logger;

/**
 * Class FileLogger
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class MemoryLogger implements LoggerInterface
{
    private array $buffer = [];

    /**
     * Add log to show
     */
    public function dump(array $params)
    {
        $this->buffer[] = $params['data'] ?? 'Nothing to log';
    }

    /**
     * Add log and replace last line
     *
     * @return void
     */
    public function dumpLastLine(array $params)
    {
        $this->dump($params);
    }

    /**
     * Erase Logs
     */
    public function erase(array $params = [])
    {
        $this->buffer = [];
    }

    /**
     * Show logs
     *
     * @return mixed
     */
    public function view(array $params = [])
    {
        return $this->buffer;
    }
}
