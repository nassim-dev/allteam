<?php

namespace core\logger\extensions;

use core\extensions\ExtensionLauncherServiceInterface;
use Nette\Utils\Helpers;
use Tracy\IBarPanel;

/**
 * Debug panel for ExtensionLauncherServiceInterface
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class TracyExtensionsPanel implements IBarPanel
{
    public function __construct(private ExtensionLauncherServiceInterface $launcher)
    {
    }

    public function getTab(): string
    {
        return Helpers::capture(
            function (): void {
                $count = count($this->launcher->getExtensions());
                $title = sprintf(ngettext('%d extension loaded', '%d extensions loaded', $count), $count);
                require __DIR__ . '/templates/TracyExtensionsPanel.tab.phtml';
            }
        );
    }

    public function getPanel(): ?string
    {
        return Helpers::capture(
            function (): void {
                $extensions = $this->launcher->getExtensions();

                require __DIR__ . '/templates/TracyExtensionsPanel.panel.phtml';
            }
        );
    }
}
