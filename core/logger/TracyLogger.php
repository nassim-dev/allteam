<?php

namespace core\logger;

use Tracy\Debugger;

/**
 * Class TracyLogger
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class TracyLogger implements LoggerInterface
{
    /**
     * Add log to show
     *
     * @param bool $lineBreak
     */
    public function dump(array $params)
    {
        Debugger::barDump($params['data'] ?? 'Nothing to log', $params['logfile'] ?? 'Log');
    }

    /**
     * Add log and replace last line
     *
     * @return void
     */
    public function dumpLastLine(array $params)
    {
        $this->dump($params);
    }

    /**
     * Erase Logs
     */
    public function erase(array $params = [])
    {
        return null;
    }

    /**
     * Show logs
     *
     * @return mixed
     */
    public function view(array $params = [])
    {
        return null;
    }
}
