<?php

namespace core\logger\di;

use core\DI\DependencieInjectorInterface;
use Nette\Utils\Helpers;
use SplObjectStorage;
use Tracy\Debugger;
use Tracy\Dumper;
use Tracy\IBarPanel;

/**
 * Debug panel for DiServiceInterface
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class TracyDiPanel implements IBarPanel
{
    private $_filteredContainer = null;

    public function __construct(private DependencieInjectorInterface $di)
    {
    }

    private function filterContainer(array $container): SplObjectStorage
    {
        if ($this->_filteredContainer === null) {
            $this->_filteredContainer = new SplObjectStorage();
            foreach ($container as $element) {
                $this->_filteredContainer->attach($element);
            }
        }

        return $this->_filteredContainer;
    }

    public function getTab(): string
    {
        return Helpers::capture(
            function (): void {
                $count = count($this->filterContainer($this->di->container)) . ' singletons';
                require __DIR__ . '/templates/TracyDiPanel.tab.phtml';
            }
        );
    }

    public function getPanel(): ?string
    {
        return Helpers::capture(
            function (): void {
                $objects = $this->filterContainer($this->di->container);

                $affectations = [];
                foreach ($this->di->getStore() as $name => $value) {
                    $affectations[$name] = Dumper::toHtml($value, [
                        Dumper::DEPTH    => Debugger::$maxDepth,
                        Dumper::TRUNCATE => Debugger::$maxLength,
                        Dumper::LOCATION => Debugger::$showLocation ?: Dumper::LOCATION_CLASS | Dumper::LOCATION_SOURCE,
                        Dumper::LAZY     => true,
                    ]);
                }

                $mapping = $this->di->getMapping();
                require __DIR__ . '/templates/TracyDiPanel.panel.phtml';
            }
        );
    }
}
