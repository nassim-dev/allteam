<?php namespace core\logger;

/**
 * Class Log
 *
 *  @method static string dump(array $params)
 *  @method static string dumpLastLine(array $params)
 *  @method static string erase(array $params)
 *  @method static string registerLogger(core\logger\LoggerInterface $logger)
 *  @method static string view(array $params)
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class Log extends \core\facade\HelperFacade
{
    protected static ?string $serviceInterface = 'core\logger\LogServiceInterface';
}
