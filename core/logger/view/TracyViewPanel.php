<?php

namespace core\logger\view;

use core\view\ViewServiceInterface;
use Nette\Utils\Helpers;
use Tracy\Debugger;
use Tracy\Dumper;
use Tracy\IBarPanel;

/**
 * Debug panel for ViewServiceInterface
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class TracyViewPanel implements IBarPanel
{
    private $_filteredContainer = null;

    public function __construct(private ViewServiceInterface $view)
    {
    }

    public function getTab(): string
    {
        return Helpers::capture(
            function (): void {
                $count = count($this->view->getParameters());
                $count = sprintf(ngettext('%d variable', '%d variables', $count), $count);
                require __DIR__ . '/templates/TracyViewPanel.tab.phtml';
            }
        );
    }

    public function getPanel(): ?string
    {
        return Helpers::capture(
            function (): void {
                $objects = [];
                foreach ($this->view->getParameters() as $name => $value) {
                    $objects[$name] = Dumper::toHtml($value, [
                        Dumper::DEPTH    => Debugger::$maxDepth,
                        Dumper::TRUNCATE => Debugger::$maxLength,
                        Dumper::LOCATION => Debugger::$showLocation ?: Dumper::LOCATION_CLASS | Dumper::LOCATION_SOURCE,
                        Dumper::LAZY     => true,
                    ]);
                }

                require __DIR__ . '/templates/TracyViewPanel.panel.phtml';
            }
        );
    }
}
