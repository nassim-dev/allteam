<?php

namespace core\logger;

/**
 * Interface LogServiceInterface
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
interface LogServiceInterface
{
    /**
     * Add log to show
     *
     * @return void
     */
    public function dump(array $params);

    /**
     * Add log and replace last line
     *
     * @return void
     */
    public function dumpLastLine(array $params);

    /**
     * Erase Logs
     *
     * @return void
     */
    public function erase(array $params = []);

    /**
     * @return void
     */
    public function registerLogger(LoggerInterface $logger);

    /**
     * Show logs
     *
     * @return mixed
     */
    public function view(array $params = []);
}
