<?php

namespace core\logger\router;

use core\routing\Route;
use core\routing\RouterServiceInterface;
use Nette\Utils\Helpers;
use Tracy\IBarPanel;

/**
 * Debug panel for RouterServiceInterface
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class TracyRouterPanel implements IBarPanel
{
    public function __construct(private RouterServiceInterface $router)
    {
    }

    public function getTab(): string
    {
        return Helpers::capture(
            function (): void {
                $route = $this->router->getMatchingRoute() ?? _('No matching route');
                if (!is_string($route)) {
                    /** @var Route $route */
                    $route = 'Route pattern : ' . $route->getPattern();
                }

                require __DIR__ . '/templates/TracyRouterPanel.tab.phtml';
            }
        );
    }

    public function getPanel(): ?string
    {
        return Helpers::capture(
            function (): void {
                $routes = $this->router->routes;

                require __DIR__ . '/templates/TracyRouterPanel.panel.phtml';
            }
        );
    }
}
