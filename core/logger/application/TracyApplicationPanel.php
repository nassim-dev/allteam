<?php

namespace core\logger\application;

use core\app\ApplicationServiceInterface;
use Nette\Utils\Helpers;
use Tracy\IBarPanel;

/**
 * Debug panel for ApplicationServiceInterface
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class TracyApplicationPanel implements IBarPanel
{
    public function __construct(private ApplicationServiceInterface $app)
    {
    }

    public function getTab(): string
    {
        return Helpers::capture(
            function (): void {
                $controller = $this->app->getController();
                $context    = 'Controller : ' . ((is_object($controller)) ? $controller::class : 'No controller');

                require __DIR__ . '/templates/TracyApplicationPanel.tab.phtml';
            }
        );
    }

    public function getPanel(): ?string
    {
        return Helpers::capture(
            function (): void {
                $config = [
                    'application' => $this->app::class,
                    'context'     => CONTEXT,
                    'factories'   => $this->app->getFactories()
                ];

                require __DIR__ . '/templates/TracyApplicationPanel.panel.phtml';
            }
        );
    }
}
