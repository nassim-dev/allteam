<?php

namespace core\logger\request;

use core\messages\request\HttpRequestInterface;
use Nette\Utils\Helpers;
use Tracy\IBarPanel;

/**
 * Debug panel for RequestServiceInterface
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class TracyRequestPanel implements IBarPanel
{
    public function __construct(private HttpRequestInterface $request)
    {
    }

    public function getTab(): string
    {
        return Helpers::capture(
            function (): void {
                $url = 'Request : ' . $this->request->getMethod() . ' ' . $this->request->getUrl();
                require __DIR__ . '/templates/TracyRequestPanel.tab.phtml';
            }
        );
    }

    public function getPanel(): ?string
    {
        return null;
    }
}
