<?php namespace core\database;

/**
 * Class Repository
 *
 *  @method static core\database\RepositoryInterface create(string $identifier)
 *  @method static core\database\RepositoryInterface table(string $table, array $params)
 *  @method static string addNamespace(string $namespace)
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class Repository extends \core\facade\HelperFacade
{
    protected static ?string $serviceInterface = 'core\database\RepositoryFactoryInterface';
}
