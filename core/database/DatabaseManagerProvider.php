<?php

namespace core\database;

use core\DI\DiProvider;

/**
 * Trait DatabaseManagerProvider
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait DatabaseManagerProvider
{
    use DiProvider;

    /**
     * RepositoryFactoryInterfaceémentation
     *
     * @var DatabaseManagerInterface
     */
    private $databaseManager;

    /**
     * Return DatabaseManagerInterfaceémetation
     */
    public function getDatabaseManager(): DatabaseManagerInterface
    {
        return $this->databaseManager ??= $this->getDi()->singleton(DatabaseManagerInterface::class);
    }
}
