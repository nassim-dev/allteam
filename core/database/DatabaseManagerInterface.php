<?php

namespace core\database;

/**
 * Interface DatabaseManagerInterface
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
interface DatabaseManagerInterface
{
    public function getColumnsFor(string $table): ?array;

    /**
     * @return mixed
     */
    public function getConnection();

    public function getIndexKeyFor(string $table): ?string;

    /**
     * Return database structure
     *
     * @return mixed
     */
    public function getStructure();

    /**
     * @return mixed
     */
    public function getOrm(string $table);
}
