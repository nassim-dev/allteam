<?php namespace core\database;

/**
 * Class DBM
 *
 *  @method static array getColumnsFor(string $table)
 *  @method static string getConnection()
 *  @method static string getIndexKeyFor(string $table)
 *  @method static string getStructure()
 *  @method static string getOrm(string $table)
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class DBM extends \core\facade\HelperFacade
{
    protected static ?string $serviceInterface = 'core\database\DatabaseManagerInterface';
}
