<?php

namespace core\database;

use core\database\mysql\lib\nette\CustomNetteSelection;
use core\entities\EntitieInterface;

/**
 * Interface RepositoryInterface
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
interface RepositoryInterface
{
    /**
     * Add Object
     */
    public function add(EntitieInterface $object, array $options = []): ?int;

    /**
     * Add multiples objects Object
     *
     * @param EntitieInterface[] $objects
     *
     * @return mixed number of row added
     */
    public function addMultiple(array $objects): mixed;

    /**
     * Copy an object with new params
     */
    public function copy(
        EntitieInterface $oldObject,
        array $params = [],
        bool $mode = self::INCLUDE_RELATED,
        array $excludedTable = []
    ): ?int;

    /**
     * Delete Object
     */
    public function delete(EntitieInterface $object): ?int;

    /**
     * Delete Object By Id
     */
    public function deleteById(int $indexKey): ?int;

    /**
     * Delete objects by parameters
     */
    public function deleteByParams(array|string $parameter): ?int;

    /**
     * Set flag_delete to TRUE
     */
    public function flagDelete(EntitieInterface $object);

    /**
     * @param array $parameter
     */
    public function flagDeleteByParams(array $parameters);

    /**
     * Set flag_delete to FALSE
     */
    public function flagRestore(EntitieInterface $object);

    /**
     * @param array $parameter
     */
    public function flagRestoreByParams(array $parameters);

    /**
     * Get Object by id (index)
     *
     * @return EntitieInterface|mixed|bool
     */
    public function getById(?int $indexKey);

    /**
     * Get List filtered by parameters
     */
    public function getByParams(
        array|string $parameter,
        ?string $orderParam = null,
        string $order = 'DESC'
    ): array|CustomNetteSelection;

    /**
     * Generate fake object
     */
    public function getFakeObject(): EntitieInterface;

    /**
     * Get Index
     */
    public function getIndexKey(): string;

    /**
     * Get fields list
     */
    public function getKeys(): array;

    /**
     * Get list with filter (idcontext && flag_delete === 0)
     */
    public function getList(
        ?string $orderParam = null,
        ?array $limit = null,
        string $order = 'DESC'
    ): array|CustomNetteSelection;

    /**
     * Get smart list for Form select element
     */
    public function getSmartList(
        ?string $key = null,
        ?string $val = null,
        ?string $orderParam = null
    ): array;

    /**
     * Get smart list filtered for Form select element
     */
    public function getSmartListByParam(
        string $parameter,
        string $paramValue,
        ?string $key = null,
        ?string $val = null,
        ?string $orderParam = null
    ): array;

    /**
     * Get Table name
     */
    public function getTable(): string;

    /**
     * Test if ressource exist && return it
     *
     * @param mixed $indexKey indexKey
     *
     * @return boolean|mixed
     */
    public function isRessource($indexKey);

    /**
     * Slide an object
     *
     * @return void
     */
    public function slide(
        EntitieInterface $object,
        array $params = [],
        bool $mode = self::INCLUDE_RELATED,
        array $excludedTable = []
    );

    /**
     * Update object
     */
    public function update(EntitieInterface $object): ?int;
}
