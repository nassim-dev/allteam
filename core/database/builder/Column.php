<?php

namespace core\database\builder;

/**
 * Class Column
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class Column implements \Stringable
{
    private array $attributes = [];

    public function __construct(private string $name, private string $type, private Table $table)
    {
    }

    public function __toString(): string
    {
        $name       = $this->name;
        $attributes = implode(' ', $this->attributes);

        return (string) <<<SQL

    `{$name}` {$attributes},
SQL;
    }

    /**
     * Add multiple attributes
     *
     * @return void
     */
    public function addAttributes(array $attributes): self
    {
        foreach ($attributes as $attribute) {
            $this->addAttribute($attribute);
        }

        return $this;
    }

    /**
     * Add one attribute
     *
     * @return void
     */
    public function addAttribute(string $attribute): self
    {
        $this->attributes[] = $attribute;

        return $this;
    }

    /**
     * Get the value of name
     *
     * @PhpUnitGen\get("name")
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Get the value of type
     *
     * @PhpUnitGen\get("type")
     */
    public function getType(): string
    {
        return $this->type;
    }
}
