<?php

namespace core\database\builder;

use core\database\DatabaseManagerInterface;

/**
 * Class TableBuilder
 *
 * Build Mysql Table
 *
 * @category Build Mysql Table
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class TableBuilder
{
    /**
     * Undocumented function
     */
    public function __construct(private DatabaseManagerInterface $connection, private string $database)
    {
        $this->connection = $connection;
        $this->database   = $database;
        $this->structure  = $this->connection->structure;
    }
}
