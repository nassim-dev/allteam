<?php

namespace core\database\builder;

/**
 * Class FAttribute
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class FieldAttribute
{
    public const CASCADE = 'CASCADE';

    public const NO_ACTION = 'NO ACTION';

    private static array $REFERENCE = ['RESTRICT', 'CASCADE', 'SET NULL', 'NO ACTION', 'SET DEFAULT'];

    public const RESTRICT = 'RESTRICT';

    public const SET_DEFAULT = 'SET DEFAULT';

    public const SET_NULL = 'SET NULL';

    public static function AUTO_INCREMENT()
    {
        return __FUNCTION__;
    }

    public static function NOT_NULL()
    {
        return <<<SQL
NOT NULL
SQL;
    }

    public static function ON_DELETE(string $action = 'NO ACTION')
    {
        if (!in_array($action, self::$REFERENCE)) {
            $action = <<<SQL
NO ACTION
SQL;
        }

        return <<<SQL
ON DELETE {$action}
SQL;
    }

    public static function ON_UPDATE(string $action = 'NO ACTION')
    {
        if (!in_array($action, self::$REFERENCE)) {
            $action = <<<SQL
NO ACTION
SQL;
        }

        return <<<SQL
ON UPDATE {$action}
SQL;
    }

    public static function UNIQUE()
    {
        return __FUNCTION__;
    }

    public static function DEFAULT(string $defaultValue)
    {
        $function = __FUNCTION__ ;

        return <<<SQL
{$function} {$defaultValue}
SQL;
    }

    public static function IS_NULLABLE()
    {
        return <<<SQL
NULL
SQL;
    }
}
