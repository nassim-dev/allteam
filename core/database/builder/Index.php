<?php

namespace core\database\builder;

/**
 * Class Index
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class Index implements \Stringable
{
    public function __construct(private Column $column, private Table $table)
    {
    }

    public function __toString(): string
    {
        $columnName = $this->column->getName();

        return (string) <<<SQL
    ADD PRIMARY KEY (`{$columnName}`)
SQL;
    }
}
