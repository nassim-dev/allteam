<?php

namespace core\database\builder;

/**
 * Class FieldType
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class FieldType
{
    public static function BIGINT(int $size)
    {
        return __FUNCTION__ . "($size)";
    }

    public static function BLOB(int $size)
    {
        return __FUNCTION__ . "($size)";
    }

    /**
     * @param int $size
     */
    public static function BOOLEAN()
    {
        return self::TINYINT(1);
    }

    public static function CHAR(int $size)
    {
        return __FUNCTION__ . "($size)";
    }

    /**
     * @param int $size
     */
    public static function DATE()
    {
        return __FUNCTION__;
    }

    /**
     * @param int $size
     */
    public static function DATETIME()
    {
        return __FUNCTION__;
    }

    public static function DECIMAL(int $size)
    {
        return __FUNCTION__ . "($size)";
    }

    public static function DOUBLE(int $size)
    {
        return __FUNCTION__ . "($size)";
    }

    public static function ENUM(int $size)
    {
        return __FUNCTION__ . "($size)";
    }

    public static function FLOAT(int $size)
    {
        return __FUNCTION__ . "($size)";
    }

    public static function INT(int $size)
    {
        return __FUNCTION__ . "($size)";
    }

    public static function LONGBLOB(int $size)
    {
        return __FUNCTION__ . "($size)";
    }

    public static function LONGTEXT(int $size)
    {
        return __FUNCTION__ . "($size)";
    }

    public static function MEDIUMBLOB(int $size)
    {
        return __FUNCTION__ . "($size)";
    }

    public static function MEDIUMINT(int $size)
    {
        return __FUNCTION__ . "($size)";
    }

    public static function MEDIUMTEXT(int $size)
    {
        return __FUNCTION__ . "($size)";
    }

    public static function SMALLINT(int $size)
    {
        return __FUNCTION__ . "($size)";
    }

    public static function TEXT(int $size)
    {
        return __FUNCTION__ . "($size)";
    }

    /**
     * @param int $size
     */
    public static function TIME()
    {
        return __FUNCTION__;
    }

    /**
     * @param int $size
     */
    public static function TIMESTAMP()
    {
        return __FUNCTION__;
    }

    public static function TINYINT(int $size)
    {
        return __FUNCTION__ . "($size)";
    }

    public static function TINYTEXT(int $size)
    {
        return __FUNCTION__ . "($size)";
    }

    public static function VARCHAR(int $size)
    {
        return __FUNCTION__ . "($size)";
    }

    /**
     * @param int $size
     */
    public static function YEAR()
    {
        return __FUNCTION__;
    }
}
