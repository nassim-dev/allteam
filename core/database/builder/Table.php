<?php

namespace core\database\builder;

/**
 * Class Table
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class Table implements \Stringable
{
    /**
     * Columns
     */
    private array $columns = [];

    /**
     * Primary index
     */
    private array $index = [];

    /**
     * Foreign keys
     */
    private array $relations = [];

    /**
     * Engine used
     */
    private string $engine = 'INNODB';

    /**
     * Charset used
     */
    private string $charset = 'utf8';

    /**
     * Collate used
     */
    private string $collate = 'utf8_bin';

    public function __construct(private string $name)
    {
    }

    public function addColum(string $name, string $type): Column
    {
        return $this->columns[$name] = new Column($name, $type, $this);
    }

    public function addPrimaryKey(string | Column $column): Index
    {
        if (is_string($column) && !isset($this->columns[$column])) {
            throw new BuilderException('You must create the column before.');
        }

        $column = (is_string($column)) ? $this->columns[$column] : $column;

        return   $this->index[] = new Index($column, $this);
    }

    public function addForeignKey(string | Column $column, string $targetTable): Relation
    {
        if (is_string($column) && !isset($this->columns[$column])) {
            throw new BuilderException('You must create the column before.');
        }

        $column = (is_string($column)) ? $this->columns[$column] : $column;

        return $this->relations[] = new Relation($column, $this, $targetTable);
    }

    public function __toString(): string
    {
        $tableName = $this->name;
        $engine    = $this->engine;
        $charset   = $this->charset;
        $collate   = $this->collate;

        $fields = '';
        /** @var Column $column */
        foreach ($this->columns as $column) {
            $fields .= $column->__toString();
        }

        $indexes = '';
        /** @var Index $index */
        foreach ($this->index as $index) {
            $indexes .= $index->__toString();
        }

        /** @var Relation $relation */
        foreach ($this->relations as $relation) {
            $indexes .= $relation->__toString();
        }

        $foreignKeys = '';
        /** @var Relation $relation */
        foreach ($this->relations as $relation) {
            $foreignKeys .= $relation->getForeignKey();
        }

        return (string) <<< SQL
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";
CREATE TABLE IF NOT EXISTS `{$tableName}` (
{$fields}
) ENGINE={$engine} DEFAULT CHARSET={$charset} COLLATE={$collate};

ALTER TABLE `{$tableName}`
{$indexes};

ALTER TABLE `{$tableName}`
{$foreignKeys};

SQL;
    }

    /**
     * Set the value of engine
     *
     * @PhpUnitGen\set("engine")
     *
     * @param mixed $engine
     */
    public function setEngine(string $engine): self
    {
        $this->engine = $engine;

        return $this;
    }

    /**
     * Set charset used
     *
     * @param string $charset Charset used
     *
     * @PhpUnitGen\set("charset")
     */
    public function setCharset(string $charset): self
    {
        $this->charset = $charset;

        return $this;
    }

    /**
     * Set collate used
     *
     * @param string $collate Collate used
     *
     * @PhpUnitGen\set("collate")
     */
    public function setCollate(string $collate): self
    {
        $this->collate = $collate;

        return $this;
    }

    /**
     * Get the value of name
     *
     * @PhpUnitGen\get("name")
     */
    public function getName(): string
    {
        return $this->name;
    }
}
