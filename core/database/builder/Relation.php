<?php

namespace core\database\builder;

/**
 * Class Relation
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class Relation implements \Stringable
{
    public function __construct(
        private Column $column,
        private Table $table,
        private string $tagetTable
    ) {
    }

    public function __toString(): string
    {
        $tableName   = $this->table->getName();
        $columnName  = $this->column->getName();
        $targetTable = $this->tagetTable;

        return (string) <<<SQL
    ADD KEY `fk_{$tableName}_{$targetTable}_{$columnName} ` (`{$columnName}`),
SQL;
    }

    public function getForeignKey()
    {
        $tableName   = $this->table->getName();
        $columnName  = $this->column->getName();
        $targetTable = $this->tagetTable;

        return <<<SQL
    ADD CONSTRAINT `fk_{$tableName}_{$targetTable}_'{$columnName}`  FOREIGN KEY (`{$columnName}`) REFERENCES `{$targetTable}` (`{$columnName}`),
SQL;
    }
}
