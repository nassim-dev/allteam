<?php

namespace core\database\mysql;

use core\config\Conf;
use core\database\mysql\lib\nette\CustomConventions;
use core\database\mysql\lib\nette\CustomNetteConnection;
use core\database\mysql\lib\nette\CustomNetteExplorer;
use core\database\mysql\lib\nette\CustomNetteSelection;
use core\DI\DI;
use core\entities\EntitieFactoryInterface;
use Nette\Bridges\DatabaseTracy\ConnectionPanel;
use Nette\Caching\Storage;
use Nette\Database\IStructure;
use Tracy\Debugger;

/**
 * Final class MysqlManager
 *
 * Represent mysql connection
 *
 * @todo create factory instead of store all instance in properties
 *
 * @category  Represent mysql connection
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class MysqlManager implements MysqlManagerInterface
{
    public function __construct(
        public Storage $storage,
        public CustomNetteConnection $connection,
        public CustomConventions $conventions,
        public IStructure $structure,
        public CustomNetteExplorer $explorer,
        EntitieFactoryInterface $entityFactory
    ) {
        $this->connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $this->explorer->setEntitieFactory($entityFactory);

        if (Conf::find('ENVIRONNEMENT') === 'development' && CONTEXT === 'APP' && !isset($_SERVER['x-requested-with'])) {
            $this->_initQueryPanel();
        }
    }

    public function getColumnsFor(string $table): array
    {
        return $this->structure->getColumns($table);
    }

    /**
     * Add specific foreign key association
     *
     * @return void
     */
    public function addAssociation(string $intialTable, string $field, string $targetTable)
    {
        $this->conventions->addAssociation($intialTable, $field, $targetTable);
    }

    public function getConnection(): CustomNetteConnection
    {
        return $this->connection;
    }

    public function getExplorer(): CustomNetteExplorer
    {
        return $this->explorer;
    }

    public function getIndexKeyFor(string $table): ?string
    {
        return $this->structure->getPrimaryAutoincrementKey($table);
    }

    public function getOrm(string $table): CustomNetteSelection
    {
        return $this->explorer->table($table);
    }

    /**
     * Init Tracy query panel
     *
     * @return void
     */
    private function _initQueryPanel()
    {
        /**
         * @var ConnectionPanel $panel
         */
        $panel             = DI::singleton(ConnectionPanel::class);
        $panel->maxQueries = 250;

        Debugger::getBar()->addPanel($panel);
    }

    /**
     * Get nette Structure objects
     *
     * @PhpUnitGen\get("structure")
     */
    public function getStructure(): IStructure
    {
        return $this->structure;
    }
}
