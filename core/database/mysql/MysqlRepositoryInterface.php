<?php

namespace core\database\mysql;

use core\database\RepositoryInterface;

/**
 * Interface MysqlRepositoryInterface
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
interface MysqlRepositoryInterface extends RepositoryInterface
{
    public const MYSQL_DATE_FORMAT = 'Y-m-d H:i:s';
    public const INCLUDE_RELATED   = true;
    public const CREATE_LOG        = true ;
    public const NO_LOG            = false;
    public const DESC              = 'DESC';
    public const ASC               = 'ASC';
}
