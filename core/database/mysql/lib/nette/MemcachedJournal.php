<?php

namespace core\database\mysql\lib\nette;

use Nette\Caching\Storages\Journal;
use Predis\NotSupportedException;

/**
 * Class MemcachedJournal
 *
 * Description
 *
 * @category  Description
 * @todo  implements
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class MemcachedJournal implements Journal
{
    public function __construct()
    {
        if (!extension_loaded('memcached')) {
            throw new NotSupportedException('MemcachedJournal requires PHP extension memcached which is not loaded.');
        }
    }

    /**
     * Cleans entries from journal.
     * @return array|null of removed items or null when performing a full cleanup
     */
    public function clean(array $conditions): ?array
    {
        return $conditions;
    }

    /**
     * Writes entry information into the journal.
     */
    public function write(string $key, array $dependencies): void
    {
    }
}
