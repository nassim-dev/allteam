<?php

namespace core\database\mysql\lib\nette;

use core\traits\EntitieFactoryTrait;
use Nette\Caching\Storage;
use Nette\Database\Explorer;
use Nette\SmartObject;

/**
 * Class CustomnetteExplorer
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class CustomNetteExplorer extends Explorer
{
    use EntitieFactoryTrait;
    use SmartObject;

    private ?\Nette\Caching\Storage $_parentCacheStorage;

    public function __construct(CustomNetteConnection $connection, CustomNetteStructure $structure, CustomConventions $conventions, ?Storage $cacheStorage = null)
    {
        parent::__construct($connection, $structure, $conventions, $cacheStorage);
        $this->_parentCacheStorage = $cacheStorage;
    }

    public function table(string $table): CustomNetteSelection
    {
        return new CustomNetteSelection($this, $this->getConventions(), $table, $this->_parentCacheStorage);
    }

    public function getConnection(): CustomNetteConnection
    {
        return parent::getConnection();
    }
}
