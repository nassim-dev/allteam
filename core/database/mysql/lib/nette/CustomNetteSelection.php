<?php

namespace core\database\mysql\lib\nette;

use core\database\datafilter\DataFilterInterface;
use core\database\RepositoryInterface;
use core\DI\DiProvider;
use core\DI\InjectionAttribute;
use core\entities\EntitieAbstract;
use core\entities\EntitieDefault;
use core\entities\EntitieInterface;
use Nette\Caching\Storage;
use Nette\Database\Conventions;
use Nette\Database\Explorer;
use Nette\Database\Table\IRow;
use Nette\Database\Table\Selection;
use Nette\SmartObject;

/**
 * Class CustomNetteSelection
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class CustomNetteSelection extends Selection
{
    use SmartObject;
    use DiProvider;
    /**
     * Creates filtered table representation.
     *
     * @param CustomNetteExplorer $explorer
     * @param CustomConventions   $conventions
     */
    #[InjectionAttribute(Explorer::class, build:InjectionAttribute::SINGLETON, implementation: CustomNetteExplorer::class)]
    #[InjectionAttribute(Conventions::class, build:InjectionAttribute::SINGLETON, implementation: CustomConventions::class)]
    public function __construct(
        Explorer $explorer,
        Conventions $conventions,
        string $tableName,
        Storage $cacheStorage = null
    ) {
        parent::__construct($explorer, $conventions, $tableName, $cacheStorage);
    }

    /**
     * Back compatibility
     *
     * @var CustomNetteExplorer
     */
    protected $context;

    /**
     * @var CustomNetteExplorer
     */
    protected $explorer;

    public function current(): \core\entities\EntitieInterface|false
    {
        return parent::current();
    }

    /**
     * Fetches single row object.
     *
     * @return null|EntitieInterface
     */
    public function fetch(): ?EntitieAbstract
    {
        return parent::fetch();
    }

    /**
     * Returns row specified by primary key.
     *
     * @param mixed $key primary key
     *
     * @return null|EntitieInterface
     */
    public function get($key): ?EntitieAbstract
    {
        return parent::get($key);
    }

    /**
     *  Returns referenced row.
     *
     * @param IRow $row
     */
    public function getReferencedTable($row, ?string $table, string $column = null): EntitieInterface | bool | null
    {
        return parent::getReferencedTable($row, $table, $column);
    }

    /**
     * Returns specified row.
     *
     * @param string $key
     *
     * @return EntitieInterface
     */
    public function offsetGet($key): EntitieAbstract
    {
        return parent::fetchoffsetGet($key);
    }

    /**
     * @return EntitieInterface
     */
    protected function createRow(array $row): EntitieAbstract
    {
        $this->getReferencedTable(new EntitieDefault([]), 'e');

        return ($this->explorer->getEntitieFactory() === null) ? new EntitieDefault($row, $this) : $this->context->getEntitieFactory()->create($this->name, [$row, $this]);
    }

    private function getRowsFromCache()
    {
        $rows = $this->cache?->getStorage()->read($this->getGeneralCacheKey());
        if (is_array($rows)) {
            $this->rows = $rows;
        }
    }

    protected function execute(): void
    {
        $this->getRowsFromCache();
        parent::execute();
        $this->saveCacheState();
    }

    /**
     * Applie custom filter
     */
    public function appliesFilter(string $filterClass, RepositoryInterface $repository, array $parameters = []): self
    {
        if (!in_array(DataFilterInterface::class, class_implements($filterClass))) {
            return $this;
        }

        /** @var DataFilterInterface $filter */
        $filter = $this->getDi()->singleton($filterClass);

        foreach ($parameters as $property => $value) {
            $setter = 'set' . ucfirst($property);
            if (method_exists($filter, $setter)) {
                call_user_func_array([$filter, $setter], [$property]);

                continue;
            }

            if (property_exists($filter, $property)) {
                $filter->{$property} = $value;
            }
        }

        return $filter->filter($this, $repository);
    }
}
