<?php

namespace core\database\mysql\lib\nette;

use Nette\Database\Connection;
use Nette\SmartObject;
use Nette\Utils\Arrays;
use PDOException;

/**
 * Class CustomNetteConnection
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class CustomNetteConnection extends Connection
{
    use SmartObject;

    /** @var string|null */
    private $sql;


    /**
     * Set Pdo attribute
     *
     * @return void
     */
    public function setAttribute(int $attribute, mixed $value)
    {
        $this->getPdo()->setAttribute($attribute, $value);
    }

    /**
     * Generates and executes SQL query.
     *
     * @param mixed ...$params
     */
    public function query(string $sql, ...$params): CustomNetteResultSet
    {
        [$this->sql, $params] = $this->preprocess($sql, ...$params);

        try {
            $result = new CustomNetteResultSet($this, $this->sql, $params);
        } catch (PDOException $e) {
            Arrays::invoke($this->onQuery, $this, $e);

            throw $e;
        }

        Arrays::invoke($this->onQuery, $this, $result);

        return $result;
    }

    public function queryArgs(string $sql, array $params): CustomNetteResultSet
    {
        return parent::queryArgs($sql, $params);
    }
}
