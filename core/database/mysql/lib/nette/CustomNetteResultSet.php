<?php

namespace core\database\mysql\lib\nette;

use core\entities\Entitie;
use core\DI\DiProvider;
use Nette\Database\IStructure;
use Nette\Database\ResultSet;
use Nette\SmartObject;
use Nette\Utils\DateTime;
use ReflectionClass;

/**
 * Class CustomNetteResultSet
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class CustomNetteResultSet extends ResultSet
{
    use SmartObject;
    use DiProvider;

    /** @var array */
    private static $propsTypesMapping = [];

    /** @var array */
    private $types;

    /**
     * @var ResultSetFormaterInterface[]
     */
    private static array $_customFormaters = [];

    /**
     * Set the value of customFormaters
     *
     * @PhpUnitGen\set("customFormaters")
     *
     * @param ResultSetFormaterInterface[] $customFormaters
     */
    public function addCustomFormaters(ResultSetFormaterInterface $customFormaters): self
    {
        self::$_customFormaters[] = $customFormaters;

        return $this;
    }

    /**
     * Set the value of customFormaters
     *
     * @PhpUnitGen\set("customFormaters")
     *
     * @param ResultSetFormaterInterface[] $customFormaters
     *
     * @return void
     */
    public static function addFormaters(ResultSetFormaterInterface $customFormaters)
    {
        self::$_customFormaters[] = $customFormaters;
    }

    /**
     * Normalizes result row.
     *
     * @return array
     */
    public function normalizeRow(array $row): array
    {
        if (null === $this->types) {
            $this->types = $this->getConnection()->getDriver()->getColumnTypes($this->getPdoStatement());
        }


        foreach ($this->types as $key => $type) {
            $value = $row[$key];
            if (null === $value || false === $value || IStructure::FIELD_TEXT === $type) {
                $this->_formatCustomPropsType($row, $key, $value);

                continue;
            }

            $this->_formatIntegerFields($row, $key, $value, $type);
            $this->_formatFloatFields($row, $key, $value, $type);
            $this->_formatBooleanFields($row, $key, $value, $type);
            $this->_formatDateFields($row, $key, $value, $type);
            $this->_formatTimeIntervalFields($row, $key, $value, $type);
            $this->_formatUnixTimeFields($row, $key, $value, $type);

            foreach (self::$_customFormaters as $formater) {
                $formater->format($row, $key, $value, $type);
            }
        }

        return $row;
    }

    /**
     * @param mixed $value
     *
     * @return void
     */
    private function _formatBooleanFields(array &$row, string $key, $value, string $type)
    {
        if (IStructure::FIELD_BOOL === $type) {
            $row[$key] = ((bool) $value) && 'f' !== $value && 'F' !== $value;
        }
    }

    /**
     * @param mixed $value
     *
     * @return void
     */
    private function _formatDateFields(array &$row, string $key, $value, string $type)
    {
        if (IStructure::FIELD_DATETIME === $type || IStructure::FIELD_DATE === $type || IStructure::FIELD_TIME === $type) {
            $row[$key] = new DateTime($value);
        }
    }

    /**
     * @param mixed $value
     *
     * @return void
     */
    private function _formatFloatFields(array &$row, string $key, $value, string $type)
    {
        if (IStructure::FIELD_FLOAT === $type) {
            if (is_string($value) && ($pos = strpos($value, '.')) !== false) {
                $value = rtrim(rtrim(0 === $pos ? "0$value" : $value, '0'), '.');
            }

            $float     = (float) $value;
            $row[$key] = (string) $float === $value ? $float : $value;
        }
    }

    /**
     * @param mixed $value
     *
     * @return void
     */
    private function _formatIntegerFields(array &$row, string $key, $value, string $type)
    {
        if (IStructure::FIELD_INTEGER === $type) {
            $row[$key] = is_float($tmp = $value * 1) ? $value : $tmp;
        }
    }

    /**
     * @param mixed $value
     *
     * @return void
     */
    private function _formatTimeIntervalFields(array &$row, string $key, $value, string $type)
    {
        if (IStructure::FIELD_TIME_INTERVAL === $type) {
            preg_match('#^(-?)(\d+)\D(\d+)\D(\d+)(\.\d+)?$#D', $value, $m);
            $row[$key]         = new \DateInterval("PT$m[2]H$m[3]M$m[4]S");
            $row[$key]->f      = isset($m[5]) ? (float) $m[5] : 0.0;
            $row[$key]->invert = (int) (bool) $m[1];
        }
    }

    /**
     * @param mixed $value
     *
     * @return void
     */
    private function _formatUnixTimeFields(array &$row, string $key, $value, string $type)
    {
        if (IStructure::FIELD_UNIX_TIMESTAMP === $type) {
            $row[$key] = DateTime::from($value);
        }
    }

    /**
     * Get actual db table
     *
     * @return string|null
     */
    private function getTable(): ?string
    {
        preg_match_all('/FROM [`\'"]([a-zA-Z\-_]*)/m', $this->getPdoStatement()->queryString, $matches, PREG_SET_ORDER, 0);

        return (isset($matches[0])) ? $matches[0][1] : null;
    }


    /**
     * Find custom type in entitie
     *
     * @return string|null
     */
    private function findCustomPropsType(string $name): ?string
    {
        $table = $this->getTable();
        if (null === $table) {
            return $table;
        }

        if (!isset(self::$propsTypesMapping[$table])) {
            self::$propsTypesMapping[$table] = [];

            try {
                $entitie = Entitie::create($table, []);
                if ($entitie !== null) {
                    $reflection = new ReflectionClass($entitie);
                    $properties = $reflection->getProperties();

                    foreach ($properties as $property) {
                        self::$propsTypesMapping[$table][$property->getName()] = ($property->getType()?->isBuiltin() ?? false) ? null : $property->getType()?->getName();
                    }
                }
            } catch (\Exception $e) {
            }
        }

        return self::$propsTypesMapping[$table][$name] ?? null;
    }

    private function _formatCustomPropsType(array &$row, string $key, $value)
    {
        $propType = $this->findCustomPropsType($key);
        if (!is_null($propType)) {
            $row[$key] = $this->getDi()->make($propType, $value);
        }
    }


    public function getConnection(): CustomNetteConnection
    {
        return parent::getConnection();
    }
}
