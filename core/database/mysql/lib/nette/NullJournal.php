<?php

namespace core\database\mysql\lib\nette;

use core\nullObjects\NullObject;
use Nette\Caching\Storages\Journal;

/**
 * Class NullJournal
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class NullJournal extends NullObject implements Journal
{
    public function clean(array $conditions): ?array
    {
        return null;
    }

    public function write(string $key, array $dependencies): void
    {
        //does nothing
    }
}
