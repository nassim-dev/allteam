<?php

namespace core\database\mysql\lib\nette;

/**
 * Interface ResultSetFormaterInterface
 *
 * Description
 *
 * @category  Description
 * @version   0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
interface ResultSetFormaterInterface
{
    /**
     * @param array  $row   this is the array to format
     * @param string $key   field name
     * @param mixed  $value row value
     * @param string $type  Database field type selector
     *
     * @return void
     */
    public function format(array &$row, string $key, $value, string $type);
}
