<?php

namespace core\database\mysql\lib\nette;

use Nette\Database\Conventions\StaticConventions;
use Nette\SmartObject;

/**
 * Class CustomConventions
 *
 * Description
 *
 * @category  Description
 * @version   0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class CustomConventions extends StaticConventions
{
    use SmartObject;

    private array $associations = [];

    /**
     * @param string $Rforeign
     */
    public function __construct(string $primary = 'id%1$s', string $foreign = 'id%1$s', string $table = '%s')
    {
        parent::__construct($primary, $foreign, $table);
    }

    public function getBelongsToReference(string $table, string $key): ?array
    {
        if (isset($this->associations[$table][$key])) {
            return [
                sprintf($this->table, $key, $this->associations[$table][$key]),
                sprintf($this->foreign, $key, $this->associations[$table][$key]),
            ];
        }

        return parent::getBelongsToReference($table, $key);
    }

    public function addAssociation(string $intialTable, string $field, string $targetTable)
    {
        if (!isset($this->associations[$intialTable])) {
            $this->associations[$intialTable] = [];
        }

        $this->associations[$intialTable][$field] = $targetTable;
    }
}
