<?php

namespace core\database\mysql\lib\nette;

use Nette\Caching\Storage;
use Nette\Database\Structure;
use Nette\SmartObject;

/**
 * Class CustomNetteStructure
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class CustomNetteStructure extends Structure
{
    use SmartObject;

    public function __construct(CustomNetteConnection $connection, Storage $cacheStorage)
    {
        parent::__construct($connection, $cacheStorage);
    }

    public function addPropertyType(string $dbType, string $class)
    {
    }

    /**
     * Init new table if not available
     *
     * @return void
     */
    public function initializeTable(string $tablename, array $columns = [])
    {
        if (in_array($tablename, $this->getTables())) {
            return;
        }

        $this->connection->query($this->getSqlRequestFor($tablename, $columns));

        $this->rebuild();
    }

    /**
     * Create sql request for creating a table
     */
    private function getSqlRequestFor(string $tablename, array $columns = []): string
    {
        if (str_contains($tablename, '_')) {
            $ids = explode('_', $tablename);
        }


        $sql = 'CREATE TABLE IF NOT EXISTS `' . $tablename . '` (';

        $newColumns = [];
        foreach ($ids as $column) {
            $newColumns[] = '`' . $column . '` int(11) NOT NULL';
        }

        foreach ($columns as $column => $attributes) {
            $newColumns[] = '`' . $column . '` ' . $attributes;
        }

        $sql .= implode(',', $newColumns);
        $sql .= ') ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8';


        return $sql;
    }
}
