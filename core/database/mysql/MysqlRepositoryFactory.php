<?php

namespace core\database\mysql;

use core\containers\ContainerInterface;
use core\database\RepositoryContainer;
use core\database\RepositoryFactoryInterface;
use core\database\RepositoryInterface;
use core\factory\AbstractFactory;
use core\factory\FactoryException;

/**
 * Class MysqlRepositoryFactory
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class MysqlRepositoryFactory extends AbstractFactory implements RepositoryFactoryInterface
{
    public function __construct()
    {
        $this->addNamespace('app\entities');
    }

    /**
     * Constructor
     */
    public function create(string $identifier, array $params = []): MysqlRepositoryInterface
    {
        $identifier = $this->_getClass($identifier);

        $instance = $this->getInstance($identifier);
        if (null === $instance) {
            $instance = $this->getDi()->singleton($identifier, $params);
            $this->register($instance, $identifier);
        }

        return $instance;
    }

    /**
     * Get registred elements
     */
    public function get(string $identifier): MysqlRepositoryInterface
    {
        return $this->getInstance($identifier) ?? $this->create($identifier);
    }

    /**
     * Return a Repository object
     *
     * @param string $table Table name
     *
     * @return null|MysqlRepositoryInterface
     */
    public function table(string $table, array $params = []): ?RepositoryInterface
    {
        return $this->create($table, $params);
    }

    /**
     * Get class name if exist ot throw error
     *
     * @throws FactoryException
     */
    private function _getClass(string $identifier): string
    {
        if (class_exists($identifier)) {
            return $identifier;
        }

        foreach ($this->getNamespaces() as $namespace) {
            $defaultIdentifier = $namespace . '\\' . ucfirst($identifier) . 'Repository';
            if (class_exists($defaultIdentifier)) {
                return $defaultIdentifier;
            }

            $defaultIdentifier = $namespace . '\\' . $identifier . '\\' . ucfirst($identifier) . 'Repository';
            if (class_exists($defaultIdentifier)) {
                return $defaultIdentifier;
            }
        }

        throw new FactoryException("$identifier can not be instanciate, because no class is mapped");
    }

    /**
     * Get all instances or filtered by callback
     *
     * @PhpUnitGen\get("instances")
     */
    public function getInstances(?callable $filter = null): ContainerInterface
    {
        $this->instances ??= new RepositoryContainer();
        if (!null === $filter) {
            $entries = array_filter($this->instances->toArray(), $filter);

            return new RepositoryContainer($entries);
        }

        return $this->instances;
    }
}
