<?php

namespace core\database\mysql;

use core\database\datafilter\AdminFilter;
use core\database\datafilter\DataFilterInterface;
use core\database\datafilter\DeletedFilter;
use core\database\DBM;
use core\database\mysql\lib\nette\CustomNetteSelection;
use core\database\mysql\traits\MysqlUtilsTrait;
use core\DI\DiProvider;
use core\entities\Entitie;
use core\entities\EntitieInterface;
use core\entities\interfaces\TimeableInterface;
use core\events\EventManagerInterface;
use core\secure\authentification\AuthServiceInterface;
use Nette\Utils\DateTime;

/**
 * Class MysqlRepository
 *
 * Parent class for mysql table access
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
abstract class MysqlRepository implements MysqlRepositoryInterface
{
    use MysqlUtilsTrait;
    use DiProvider;

    /**
     * Mysql query
     *
     * @var array
     */
    public static $query = [];

    /**
     * @var array
     */
    public static $test = [];

    private static ?array $_excludedTable = null;

    /**
     * Filter parameters
     *
     * @deprecated use _datafilters
     */
    private array $_filter = [];

    /**
     * IndexKey
     */
    private ?string $_indexkey = null;

    /**
     * Fields in Mysql Table
     */
    private array $_keys = [];

    /**
     * DataFilterInterface
     *
     * @var DataFilterInterface[]|array
     */
    private array $_datafilters = ['ALL' => []];

    public function __construct(
        protected MysqlManagerInterface $database,
        protected EventManagerInterface $eventManager,
        private string $table
    ) {
        $this->setKeys($this->database->getColumnsFor($table));
        $this->setIndexKey($this->database->getIndexKeyFor($table));
        $this->addFilter(DeletedFilter::class);
        $this->addFilter(AdminFilter::class);
    }


    /**
     * Add Object
     */
    public function add(EntitieInterface $object, array $options = []): ?int
    {
        $args                = $this->initArgs($object, $options);
        $object->updated_at  = DateTime::createFromFormat(self::MYSQL_DATE_FORMAT, date(self::MYSQL_DATE_FORMAT));
        $object->created_at  = $object->updated_at;
        $object->flag_delete = 0;

        $class = strtoupper($this->getTable());
        $args  = $this->eventManager->emit('PRE_CREATE_' . $class, $args);

        $datas = $this->getVarsKeysSql($object);
        unset($datas[$this->getIndexKey()]);

        if (property_exists($object, 'idcontext')) {
            $object->idcontext ??= $object->getAuth()->getContext()?->getIdcontext();
        }

        $row = $this->database->getOrm($this->getTable())
            ->insert($datas);

        $indexKey                       = (!is_object($row)) ? null : $row->getPrimary();
        $object->{$this->getIndexKey()} = $indexKey;

        $this->eventManager->emit('POST_CREATE_' . $class, $args);

        return $indexKey;
    }

    /**
     * Add multiples objects Object
     *
     * @param array|EntitieInterface[] $objects
     *
     * @return int number of row added
     */
    public function addMultiple(array $objects): int
    {
        foreach ($objects as $key => $val) {
            if (is_array($objects[$key])) {
                $objects[$key] = Entitie::create($this->getTable(), $objects[$key]);
            }

            $objects[$key]->updated_at  = DateTime::createFromFormat(self::MYSQL_DATE_FORMAT, date(self::MYSQL_DATE_FORMAT));
            $objects[$key]->created_at  = $objects[$key]->updated_at;
            $objects[$key]->flag_delete = false;
            if (property_exists($objects[$key], 'idcontext')) {
                $objects[$key]->idcontext = ($objects[$key]->idcontext != null) ? $objects[$key]->idcontext : $objects[$key]->getAuth()->getContext()?->getIdcontext();
            }

            $objects[$key] = $this->getVarsKeysSql($val);
            unset($objects[$key][$this->getIndexKey()]);
        }

        return $this->database->getOrm($this->getTable())->insert($objects);
    }

    /**
     * Copy an object with new params
     *
     * @todo rewrite
     */
    public function copy(
        EntitieInterface $oldObject,
        array $params = [],
        bool $mode = self::INCLUDE_RELATED,
        array $excludedTable = ['context']
    ): ?int {
        $excludedTable = array_merge(self::$_excludedTable, $excludedTable);
        $this->eventManager->disable();
        $object                         = clone $oldObject;
        $object->updated_at             = DateTime::createFromFormat(self::MYSQL_DATE_FORMAT, date(self::MYSQL_DATE_FORMAT));
        $object->created_at             = $object->updated_at;
        $object->flag_delete            = 0;
        $object->{$this->getIndexKey()} = null;
        $params['idcontext'] ??= null;
        $object = $object->hydrate($params);

        /**
         * Set initial new interval to add to all abject with start/end field
         */
        if (isset($params['INTERVAL'])) {
            if (is_subclass_of($object, TimeableInterface::class)) {
                /** @var TimeableInterface $object */
                if (!null === $object->start) {
                    $object->start = $object->getStart()->add($params['INTERVAL']);
                }

                if (!null === $object->end) {
                    $object->end = $object->getEnd()->add($params['INTERVAL']);
                }
            }
        } else {
            if (isset($params['START']) && in_array('start', $this->getKeys())) {
                if (is_subclass_of($oldObject, TimeableInterface::class)) {
                    /** @var TimeableInterface $oldObject */
                    if (!null === $oldObject->start) {
                        $params['INTERVAL'] = $oldObject->getStart()->diff($params['START']);
                    }

                    $object->start = $params['START'];
                    unset($params['START']);
                }
            }

            if (isset($params['END'])) {
                if (is_subclass_of($object, TimeableInterface::class)) {
                    /** @var TimeableInterface $object */
                    $object->end = $params['END'];
                    unset($params['END']);
                }
            }
        }

        if (!isset(self::$test[$oldObject->getTable()->getName()][$oldObject->{$this->getIndexKey()}])) {
            $indexKey = $this->add($object);

            self::$test[$oldObject->getTable()->getName()][$oldObject->{$this->getIndexKey()}] = $indexKey;
            $object->{$this->getIndexKey()}                                                    = $indexKey;
            $params[$this->getIndexKey()]                                                      = $indexKey;
        }

        if (!empty($oldObject::getLinkTables())) {
            foreach ($oldObject::getLinkTables() as $hasTable) {
                $hasTableLow = strtolower($hasTable);
                $indexKey    = 'id' . $hasTableLow;

                $indexs = $oldObject->findRelated($indexKey);

                if (empty($indexs)) {
                    continue;
                }
                $finalsIds = [];
                foreach ($indexs as $indexKey) {
                    if (!isset(self::$test[$hasTableLow][$indexKey])) {
                        $oldHasObject = DBM::table($hasTable)()->getById($indexKey);
                        $paramsCopy   = $params;
                        DBM::table($hasTable)->copy($oldHasObject, $paramsCopy);
                    }

                    $finalsIds[] = self::$test[$hasTableLow][$indexKey];
                }

                if (!is_bool($object)) {
                    $object->updateRelations($indexKey, $finalsIds);
                }
            }
        }


        $config = DBM::getTablelinked($this->getIndexKey());

        if (!isset($config['table']) || self::INCLUDE_RELATED !== $mode) {
            $this->eventManager->enable();

            return self::$test[$this->getTable()][$oldObject->{$this->getIndexKey()}];
        }


        foreach ($config['table'] as $table) {
            $subObjectIndex = DBM::table($table)->getIndexKey();
            if (in_array($table, $excludedTable)) {
                continue;
            }
            $subObjects = DBM::table($table)->getByParams([$this->getIndexKey() => $oldObject->{$this->getIndexKey()}]);
            foreach ($subObjects as $subObject) {
                $subObjectIndexKey = $subObject->getIndexKey();

                if (isset(self::$test[strtolower($table)][$subObjectIndexKey])) {
                    continue;
                }
                $paramsCopy = $params;
                $indexKey   = DBM::table($table)->copy($subObject, $paramsCopy);

                if (!null === $subObjectIndexKey && $subObjectIndexKey > 0) {
                    self::$test[strtolower($table)][$subObjectIndexKey] = $indexKey;
                }


                if (in_array($subObjectIndex, $this->getKeys())) {
                    $finalValue                = self::$test[strtolower($table)][$subObjectIndexKey] ?? null;
                    $object->{$subObjectIndex} = $finalValue;
                    $this->update($object);
                }
            }
        }

        $this->eventManager->enable();

        return self::$test[$this->getTable()][$oldObject->{$this->getIndexKey()}];
    }

    /**
     * Delete Object
     */
    public function delete(EntitieInterface $object): ?int
    {
        $args             = $this->initArgs($object);
        $args['indexKey'] = $object->getPrimary();

        return $this->deleteByParams([$this->getIndexKey() => $object->getPrimary()], $args);
    }

    /**
     * Delete Object By Id
     */
    public function deleteById(int $indexKey): ?int
    {
        $args             = $this->initArgs(null);
        $args['indexKey'] = $indexKey;

        return $this->deleteByParams([$this->getIndexKey() => $args['object']->getPrimary()], $args);
    }

    /**
     * Delete objects by parameters
     *
     * @param string|array $parameter
     */
    public function deleteByParams($parameter): ?int
    {
        $args             = $this->initArgs(null);
        $args['indexKey'] = null;
        $class            = strtoupper($this->getTable());

        $args = $this->eventManager->emit('PRE_DELETE_' . $class, $args);

        $return = $this->database->getOrm($this->getTable())
            ->where($parameter)
            ->delete();

        $args = $this->eventManager->emit('POST_DELETE_' . $class, $args);

        return $return;
    }

    /**
     *  Set flag_delete to TRUE
     *
     * @param EntitieInterface $object
     *
     * @return void
     */
    public function flagDelete(EntitieInterface $object)
    {
        $args                = $this->initArgs($object);
        $object->flag_delete = 1;
        $class               = strtoupper($this->getTable());

        $this->eventManager->emit('PRE_FLAGDELETE_' . $class, $args);
        $this->update($object, ['log' => self::NO_LOG]);
        $this->eventManager->emit('POST_FLAGDELETE_' . $class, $args);
    }

    /**
     * Flag delete by parmeter
     *
     * @param array $parameters
     *
     * @return void
     */
    public function flagDeleteByParams(array $parameters)
    {
        $args = $this->initArgs(null);
        $objs = $this->getByParams($parameters);

        foreach ($objs as $obj) {
            if (!$obj->flag_delete) {
                $this->flagDelete($obj, $args);
            }
        }
    }
    /**
     * Set flag_delete to FALSE
     *
     * @param EntitieInterface $object
     *
     * @return void
     */
    public function flagRestore(EntitieInterface $object)
    {
        $args                = $this->initArgs($object);
        $object->flag_delete = 0;
        $class               = strtoupper($this->getTable());

        $this->eventManager->emit('PRE_FLAGRESTORE_' . $class, $args);
        $this->update($object, ['log' => self::NO_LOG]);
        $this->eventManager->emit('POST_FLAGRESTORE_' . $class, $args);
    }

    /**
     * @param array $parameter
     */
    public function flagRestoreByParams(array $parameters)
    {
        $args = $this->initArgs(null);
        $objs = $this->getByParams($parameters);

        foreach ($objs as $obj) {
            if (!$obj->flag_delete) {
                $this->flagRestore($obj, $args);
            }
        }
    }

    /**
     * Get Object by id (index)
     *
     * @return EntitieInterface|mixed|bool
     */
    public function getById(?int $indexKey)
    {
        $object = $this->database->getOrm($this->getTable())
            ->get($indexKey);

        return $object ?? false;
    }
    /**
     * Get List filtered by parameters
     *
     * @param array|string $parameters
     * @param string|null  $orderParam
     * @param string       $order
     *
     * @return CustomNetteSelection
     */
    public function getByParams(
        array|string $parameters,
        ?string $orderParam = null,
        string $order = self::DESC
    ): CustomNetteSelection {
        $objects = $this->database->getOrm($this->getTable())->where($parameters);
        $objects = $this->useFilters($objects);

        return (!null === $orderParam) ? $objects->order("$orderParam $order") : $objects;
    }

    /**
     * Return PDO connection
     *
     * @return \PDO
     */
    public function getConnection(): \PDO
    {
        return $this->database->getConnection()->getPdo();
    }
    /**
     * Generate fake object
     *
     * @return EntitieInterface
     */
    public function getFakeObject(): EntitieInterface
    {
        return Entitie::create(ucfirst($this->getTable()), [$this->getIndexKey() => uniqid()]);
    }
    /**
     *  Get Index
     *
     * @return string
     */
    public function getIndexKey(): string
    {
        return $this->_indexkey;
    }
    /**
     * Get fields list
     *
     * @return array
     */
    public function getKeys(): array
    {
        return $this->_keys;
    }

    /**
     * Get list with filter (idcontext && flag_delete === 0)
     *
     * @param string|null $orderParam
     * @param array|null  $limit
     * @param [type] $order
     *
     * @return CustomNetteSelection
     */
    public function getList(
        ?string $orderParam = null,
        ?array $limit = null,
        string $order = self::DESC
    ): CustomNetteSelection {
        $objects = $this->database->getOrm($this->getTable());
        $objects = $this->useFilters($objects);

        if (!null === $orderParam) {
            $objects->order("$orderParam $order");
        }

        if (!null === $limit) {
            $objects->limit($limit['limit'], $limit['offset']);
        }

        return $objects;
    }

    /**
     * Cout rows
     *
     * @param string|null $column
     *
     * @return integer
     */
    public function count(?string $column = null): int
    {
        return $this->database->getOrm($this->getTable())
            ->count($column ?? $this->getIndexKey());
    }

    /**
     * Undocumented function
     *
     * @param string|null $table
     *
     * @return CustomNetteSelection
     */
    public function getOrm(?string $table = null): CustomNetteSelection
    {
        return $this->database->getOrm($table ?? $this->getTable(), $this->server);
    }

    /**
     * Parameters to filters objects, if user owns it, however he hasn't rights
     *
     * @param AuthServiceInterface $auth
     *
     * @return array|null
     */
    public function getOwnsFilters(AuthServiceInterface $auth): ?array
    {
        return null;
    }

    /**
     * Get smart list for Form select element
     *
     * @param string|null $key
     * @param string|null $val
     * @param string|null $orderParam
     *
     * @return array
     */
    public function getSmartList(
        ?string $key = null,
        ?string $val = null,
        ?string $orderParam = null
    ): array {
        $objects = $this->database->getOrm($this->getTable());
        $objects = $this->useFilters($objects);

        if (!null === $orderParam) {
            $objects->order($orderParam);
        }

        $return = [];
        /**
         * @var EntitieInterface
         */
        foreach ($objects as $object) {
            if (!null === $key && !null === $val) {
                $return[$object->{$key}] = $object->{$val};
            } else {
                $return = array_merge($return, $object->getOptionHtml(EntitieInterface::ARRAY_FORMAT));
            }
        }

        return $return;
    }

    /**
     * Get smart list filtered for Form select element
     *
     * @param string      $parameter
     * @param string      $paramValue
     * @param string|null $key
     * @param string|null $val
     * @param string|null $orderParam
     *
     * @return array
     */
    public function getSmartListByParam(
        string $parameter,
        string $paramValue,
        ?string $key = null,
        ?string $val = null,
        ?string $orderParam = null
    ): array {
        $objects = $this->database->getOrm($this->getTable())
            ->where($parameter, $paramValue);

        $objects = $this->useFilters($objects);

        if (!null === $orderParam) {
            $objects->order($orderParam);
        }

        $return = [];
        foreach ($objects as $object) {
            /** @var EntitieInterface $object */
            if (!null === $key && !null === $val) {
                $return[$object->{$key}] = $object->{$val};

                continue;
            }
            $return = array_merge($return, $object->getOptionHtml(EntitieInterface::ARRAY_FORMAT));
        }

        return $return;
    }
    /**
     * Get Table name
     *
     * @return string
     */
    public function getTable(): string
    {
        return $this->table;
    }

    /**
     * Test if ressource exist && return it
     *
     * @param mixed $indexKey indexKey
     *
     * @return boolean|mixed
     */
    public function isRessource($indexKey)
    {
        if (is_numeric($indexKey)) {
            $object = $this->getById($indexKey);
            if (!null === $object && !is_bool($object)) {
                return $object;
            }
        }

        return false;
    }

    /**
     * Set the value of filter
     *
     * @param array $filter
     *
     * @return self
     */
    public function setFilter(array $filter): self
    {
        $this->filter = $filter;

        return $this;
    }

    /**
     * Slide an object
     *
     * @param EntitieInterface $object
     * @param array            $params
     * @param bool             $mode
     * @param array            $excludedTable
     *
     * @return void
     */
    public function slide(
        EntitieInterface $object,
        array $params = [],
        bool $mode = self::INCLUDE_RELATED,
        array $excludedTable = []
    ) {
        $excludedTable = array_merge(self::$_excludedTable, $excludedTable);
        $this->eventManager->disable();

        /*
         * Set initial new interval to add to all abject with star/end field
         */

        if (isset($params['INTERVAL'])) {
            if (is_subclass_of($object, TimeableInterface::class)) {
                /** @var TimeableInterface $object */
                if (!null === $object->start) {
                    $object->start = $object->getStart()->add($params['INTERVAL']);
                    $update        = true;
                }

                if (in_array('end', $this->getKeys()) && !null === $object->end) {
                    $object->end = $object->getEnd()->add($params['INTERVAL']);
                    $update      = true;
                }
            }
        } else {
            $tmp = $params;
            if (is_subclass_of($object, TimeableInterface::class)) {
                /** @var TimeableInterface $object */
                if (isset($params['start'])) {
                    if (!null === $object->start) {
                        $params['INTERVAL'] = $object->getStart()->diff($params['start']);
                    }

                    $object->start = $params['start'];
                    $update        = true;

                    unset($params['start']);
                }

                if (isset($params['end'])) {
                    $object->end = $params['end'];
                    $update      = true;
                    unset($params['end']);
                }
            }

            $object = $object->hydrate($tmp);
        }

        if (!isset(self::$test[$object->getTable()->getName()][$object->{$this->getIndexKey()}]) && isset($update)) {
            $indexKey                                                                    = $this->update($object);
            self::$test[$object->getTable()->getName()][$object->{$this->getIndexKey()}] = $indexKey;
        }

        if (!$mode) {
            $this->eventManager->enable();

            return $object;
        }
        //TODO rewrite a method
        //$config = $this->database->getTablelinked($this->getIndexKey());
        $config = [];
        if (isset($config['table'])) {
            $this->eventManager->enable();

            return $object;
        }
        foreach ($config['table'] as $table) {
            if (in_array($table, $excludedTable)) {
                continue;
            }
            $subObjects = DBM::table($table)->getByParams([$this->getIndexKey() => $object->{$this->getIndexKey()}]);
            foreach ($subObjects as $subObject) {
                $subObjectIndexKey = $subObject->getIndexKey();
                $subObjectIndex    = DBM::table($table)->getIndexKey();
                if (!isset(self::$test[strtolower($table)][$subObjectIndexKey])) {
                    $indexKey = DBM::table($table)->slide($subObject, ['INTERVAL' => $params['INTERVAL']]);
                    if (!null === $subObjectIndexKey && $subObjectIndexKey > 0) {
                        self::$test[strtolower($table)][$subObjectIndexKey] = $indexKey;
                    }
                }

                if (in_array($subObjectIndex, $this->getKeys())) {
                    $finalValue                = self::$test[strtolower($table)][$subObjectIndexKey] ?? null;
                    $object->{$subObjectIndex} = $finalValue;
                }
            }
        }



        $this->eventManager->enable();

        return $object;
    }

    /**
     * Update object
     */
    public function update(EntitieInterface $object): ?int
    {
        $args               = $this->initArgs($object, func_get_args());
        $class              = strtoupper($this->getTable());
        $object->updated_at = DateTime::createFromFormat(self::MYSQL_DATE_FORMAT, date(self::MYSQL_DATE_FORMAT));

        $args   = $this->eventManager->emit('PRE_UPDATE_' . $class, $args);
        $return = $this->database->getOrm($this->getTable())
            ->where($this->getIndexKey(), $object->getPrimary())
            ->update($this->getVarsKeysSql($object));

        $this->eventManager->emit('POST_UPDATE_' . $class, $args);

        return $return;
    }

    /**
     * Set Index
     */
    protected function setIndexKey(string $indexKey)
    {
        $this->_indexkey = $indexKey;
    }

    /**
     * Set field list
     * @param mixed[] $keys
     */
    protected function setKeys(array $keys)
    {
        $this->_keys = $keys;
    }



    /**
     * Get Html for select field
     */
    public function getHtmlList(
        int $type = EntitieInterface::HTML_FORMAT,
        int $page = 1,
        ?string $order = null,
        array $filter = []
    ): string | array {
        $return = [];

        $objects = $this->getList($order)
            ->where($filter)
            ->page($page, 30);

        foreach ($objects as $object) {
            $return[] = $object->getOptionHtml($type);
        }

        return (EntitieInterface::HTML_FORMAT === $type) ? implode('', $return) : $return;
    }

    /**
     * Remove dataFilterInterface
     */
    public function removeFilter(string $filter, array $repository = []): self
    {
        if (!empty($repository)) {
            foreach ($repository as $class) {
                $key = array_search($filter, $this->_datafilters[$class]);

                if (!is_bool($key)) {
                    unset($this->_datafilters[$class][$key]);
                }
            }

            return $this;
        }

        $key = array_search($filter, $this->_datafilters['ALL']);
        if (!is_bool($key)) {
            unset($this->_datafilters['ALL'][$key]);
        }

        return $this;
    }

    /**
     * Set dataFilterInterface
     *
     * @param string $filter DataFilterInterface
     *
     * @return self
     */
    public function addFilter(string $filter, array $repository = []): self
    {
        if (empty($repository)) {
            $this->_datafilters['ALL'][] = $filter;

            return $this;
        }

        foreach ($repository as $class) {
            if (!isset($this->_datafilters[$class])) {
                $this->_datafilters[$class] = [];
            }

            $this->_datafilters[$class][] = $filter;
        }

        return $this;
    }

    /**
     * Use registred data filters
     */
    protected function useFilters(CustomNetteSelection $selection): CustomNetteSelection
    {
        foreach ($this->_datafilters['ALL'] as $filter) {
            /** @var DataFilterInterface $dataFilter */
            $dataFilter = $this->getDi()->singleton($filter);
            $selection  = $dataFilter->filter($selection, $this);
        }

        if (!isset($this->_datafilters[$this::class])) {
            return $selection;
        }

        foreach ($this->_datafilters[$this::class] as $filter) {
            /** @var DataFilterInterface $dataFilter */
            $dataFilter = $this->getDi()->singleton($filter);
            $selection  = $dataFilter->filter($selection, $this);
        }

        return $selection;
    }


    /**
     * Initialize arguments
     *
     * @param EntitieInterface|null $object
     * @param array                 $options
     *
     * @return array
     */
    private function initArgs(?EntitieInterface $object, array $options = []): array
    {
        $args['object'] = $object;
        $default        = ['log' => true];
        $options[1]     = (isset($options[1])) ? array_replace($default, $options[1]) : $default;

        return array_merge_recursive($args, $options[1]);
    }
}
