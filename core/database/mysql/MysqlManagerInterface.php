<?php

namespace core\database\mysql;

use core\database\DatabaseManagerInterface;
use core\database\mysql\lib\nette\CustomNetteConnection;
use core\database\mysql\lib\nette\CustomNetteExplorer;
use core\database\mysql\lib\nette\CustomNetteSelection;
use Nette\Database\IStructure;

/**
 * Interface MysqlManagerInterface
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
interface MysqlManagerInterface extends DatabaseManagerInterface
{
    public function getConnection(): CustomNetteConnection;

    public function getExplorer(): CustomNetteExplorer;

    public function getOrm(string $table): CustomNetteSelection;


    /**
     * Return database structure
     */
    public function getStructure(): IStructure;

    /**
     * Add specific foreign key association
     *
     * @return void
     */
    public function addAssociation(string $intialTable, string $field, string $targetTable);
}
