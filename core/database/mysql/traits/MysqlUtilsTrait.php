<?php

namespace core\database\mysql\traits;

use core\database\RepositoryFactoryProvider;

/**
 * Class MysqlUtilsTrait
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait MysqlUtilsTrait
{
    use RepositoryFactoryProvider;

    /**
     * Count Row with parameters
     *
     * @param array|string $param
     *
     * @return int
     */
    public function count(array|string $param): int
    {
        return $this->getOrm($this->getTable())->count($param);
    }

    /**
     * Check if field exist
     * TODO:: Rewrite a recursive function
     *
     * @return bool
     */
    public function fieldExist(string $field)
    {
        if (!is_string($field)) {
            return false;
        }

        $params = explode('-', $field);

        switch (count($params)) {
            case 2:
                $table  = ucfirst($params[0]);
                $fields = $this->getRepositoryFactory()
                    ->table($table)
                    ->getKeys();

                return in_array($params[1], $fields);

            case 3:
                $table1  = ucfirst($params[0]);
                $fields1 = $this->getRepositoryFactory()
                    ->table($table1)
                    ->getKeys();

                return (in_array('id' . $params[1], $fields1) && in_array('id' . $params[2], $fields1));

            default:
                return in_array($field, $this->getKeys());
        }
    }

    /**
     * Check if parameter is in field list
     */
    public function isParameter(string $parameter): bool
    {
        return (in_array($parameter, $this->getKeys()));
    }

    /**
     * Retrieve field name
     */
    public function realField(string|bool $field): bool|string
    {
        if (!is_string($field)) {
            return false;
        }

        if (in_array($field, $this->getKeys())) {
            return $field;
        }

        $params = explode('-', $field);

        return ($this->getTable() === $params[0]) ? $params[1] : 'id' . $params[0];
    }

    /**
     * Get datas of object for SQL request
     *
     * @param mixed $obj
     */
    protected function getVarsKeysSql($obj): array
    {
        $return = [];

        foreach ($this->getKeys() as $column) {
            //$method = 'get' . ucfirst($column['name']);
            $return[$column['name']] = is_array($obj) ? ($obj[$column['name']] ?? null) : $obj->{$column['name']};
            if (is_array($return[$column['name']])) {
                $return[$column['name']] = implode(';', $return[$column['name']]);
            }
        }

        return $return;
    }
}
