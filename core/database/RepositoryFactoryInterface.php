<?php

namespace core\database;

/**
 * Interface RepositoryFactoryInterface
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
interface RepositoryFactoryInterface
{
    /**
     * Constructor
     */
    public function create(string $identifier): RepositoryInterface;

    /**
     * Return a Manager object
     *
     * @param string $table Table name
     */
    public function table(string $table, array $params = []): ?RepositoryInterface;

    /**
     * Set the value of namespaces
     *
     * @param string $namespaces
     *
     * @return static
     */
    public function addNamespace(string $namespace);
}
