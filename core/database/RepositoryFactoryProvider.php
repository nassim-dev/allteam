<?php

namespace core\database;

use core\database\mongo\RepositoryFactory;
use core\DI\DiProvider;

/**
 * Trait RepositoryFactoryProvider
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait RepositoryFactoryProvider
{
    use DiProvider;

    /**
     * RepositoryFactory implémentation
     *
     * @var RepositoryFactoryInterface
     */
    private $repositoryFactory;

    /**
     * Return RepositoryFactory implémetation
     *
     * @return RepositoryFactoryInterface
     */
    public function getRepositoryFactory(): RepositoryFactoryInterface
    {
        return $this->repositoryFactory ??= $this->getDi()->singleton(RepositoryFactoryInterface::class);
    }
}
