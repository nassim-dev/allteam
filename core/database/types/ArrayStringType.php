<?php

namespace core\database\types;

class ArrayStringType implements PropertyTypeInterface
{
    public function __construct(private array|string|null $value = null)
    {
        $this->toPhp();
    }

    public function toPhp(): array
    {
        return $this->value = is_array($this->value) ? $this->value : explode('|', $this->value);
    }

    public function set($value)
    {
        $this->value = $value;
    }

    public function __toString(): string
    {
        return $this->value = is_string($this->value) ? $this->value : implode('|', $this->value);
    }

    public function __get($name)
    {
        return $this->toPhp();
    }
}
