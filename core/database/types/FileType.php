<?php

namespace core\database\types;

use core\services\storage\FileItem;

class FileType implements PropertyTypeInterface
{
    public function __construct(private array|string|null $value = null)
    {
        $this->toPhp();
    }

    public function toPhp(): array
    {
        if (is_string($this->value)) {
            $filepaths   = explode('|', $this->value);
            $this->value = [];
            foreach ($filepaths as $filepath) {
                $this->value[] = new FileItem($filepath);
            }
        }

        return $this->value;
    }

    public function set($value)
    {
        $this->value = $value;
    }

    public function __toString(): string
    {
        $return = [];
        if (is_array($this->value)) {
            /** @var FileItem $fileitem */
            foreach ($this->value as $fileitem) {
                $return[] = $fileitem->getPath();
            }
        }

        return is_string($this->value) ? $this->value : implode('|', $return);
    }

    public function __get($name)
    {
        return $this->toPhp();
    }
}
