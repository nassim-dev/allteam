<?php

namespace core\database\datafilter;

use core\database\mysql\lib\nette\CustomNetteSelection;
use core\database\RepositoryInterface;
use core\secure\authentification\AuthServiceInterface;
use core\secure\permissions\AdminPermission;

/**
 * Class AdminFilter
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class AdminFilter extends AbstractFilter implements DataFilterInterface
{
    public function __construct(private AuthServiceInterface $auth)
    {
    }

    public function filter(CustomNetteSelection $selection, RepositoryInterface $repository): CustomNetteSelection
    {
        $permission = new AdminPermission($this->auth->getContext()?->getIdcontext());

        foreach ($repository->getKeys() as $column) {
            if ($column['name'] === 'idcontext' && !$this->auth->getPermissionManager()->isAllowed($permission, $this->auth->getUser())) {
                $selection->where($repository->getTable() . '.idcontext', $this->auth->getContext()?->getIdcontext());

                return $selection;
            }
        }

        return $selection;
    }
}
