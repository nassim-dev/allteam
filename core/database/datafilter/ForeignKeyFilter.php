<?php

namespace core\database\datafilter;

use core\database\mysql\lib\nette\CustomNetteSelection;
use core\database\RepositoryInterface;
use core\entities\EntitieInterface;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class ForeignKeyFilter extends AbstractFilter implements DataFilterInterface
{
    public function __construct(private EntitieInterface $entitie)
    {
    }

    public function filter(CustomNetteSelection $selection, RepositoryInterface $repository): CustomNetteSelection
    {
        foreach ($repository->getKeys() as $column) {
            if ($column['name'] == $this->entitie->getPrimaryKey()) {
                $selection->where($repository->getTable() . '.' . $this->entitie->getPrimaryKey() . ' = ?', $this->entitie->getPrimary());

                return $selection;
            }
        }

        $selection->where($this->entitie->getTable()->getName() . '.' . $this->entitie->getPrimaryKey() . ' = ?', $this->entitie->getPrimary());

        return $selection;
    }

    /**
     * Set the value of entitie
     *
     * @PhpUnitGen\set("entitie")
     * @return self
     */
    public function setEntitie(EntitieInterface $entitie)
    {
        $this->entitie = $entitie;

        return $this;
    }
}
