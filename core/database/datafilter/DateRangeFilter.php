<?php

namespace core\database\datafilter;

use core\database\mysql\lib\nette\CustomNetteSelection;
use core\database\RepositoryInterface;
use DateTimeInterface;

/**
 * Class DateRangeFilter
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class DateRangeFilter extends AbstractFilter implements DataFilterInterface
{
    public function __construct(private ?DateTimeInterface $start, private ?DateTimeInterface $end)
    {
    }

    public function filter(CustomNetteSelection $selection, RepositoryInterface $repository): CustomNetteSelection
    {
        foreach ($repository->getKeys() as $column) {
            if ($column['name'] == 'start') {
                $selection->where($repository->getTable() . '.start >= ?', $this->start);
            }

            if ($column['name'] == 'end') {
                $selection->where($repository->getTable() . '.end <= ?', $this->end);
            }
        }


        return $selection;
    }

    /**
     * Set the value of start
     *
     * @PhpUnitGen\set("start")
     * @return self
     */
    public function setStart(?DateTimeInterface $start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Set the value of end
     *
     * @PhpUnitGen\set("end")
     * @return self
     */
    public function setEnd(?DateTimeInterface $end)
    {
        $this->end = $end;

        return $this;
    }
}
