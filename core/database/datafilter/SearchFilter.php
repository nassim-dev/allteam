<?php

namespace core\database\datafilter;

use core\database\mysql\lib\nette\CustomNetteSelection;
use core\database\RepositoryInterface;
use core\messages\request\HttpRequestInterface;
use Nette\Database\IStructure;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class SearchFilter extends AbstractFilter implements DataFilterInterface
{
    /**
     * Repository
     */
    private ?\core\database\RepositoryInterface $repository = null;

    public function __construct(private HttpRequestInterface $request)
    {
    }

    public function filter(CustomNetteSelection $selection, RepositoryInterface $repositoryInterface): CustomNetteSelection
    {
        $this->repository = $repositoryInterface;

        $query = $this->request->getContents('q');
        if (empty($query) || $query === 'null') {
            return $selection;
        }

        $params = $this->generateSearchParameters($this->request->getContents(), $query);

        return $selection->whereOr($params);
    }


    /**
     * @return mixed
     */
    private function generateSearchParameters(array $params, ?string $query = null)
    {
        /** Filter non existent fields */
        $keysRaw = $this->repository->getKeys();
        $keys    = array_map(fn ($element) => $element['name'], $keysRaw);

        foreach ($params as $field => $value) {
            if (!in_array($field, $keys)) {
                unset($params[$field]);
            }
        }

        if (null === $query) {
            return $params;
        }

        $query        = '%' . $query . '%';
        $searchFields = $this->repository->getFakeObject()->getSearchFields();

        /** @var IStructure $structure */
        $structure = $this->getDi()->singleton(IStructure::class);
        $tablesRaw = $structure->getTables();
        $tables    = array_map(fn ($element) => $element['name'], $tablesRaw);

        if (is_array($searchFields)) {
            foreach ($searchFields as $parameter) {
                if (preg_match('/->/', $parameter)) {
                    $filter     = '';
                    $properties = explode('->', $parameter);
                    foreach ($properties as $propertie) {
                        if (preg_match('/get/', $propertie)) {
                            $table = str_replace('get', '', $propertie);
                            $filter .= strtolower($table);
                        } else {
                            $filter .= (in_array($propertie, $tables)) ? strtolower($propertie) : '.' . strtolower($propertie);
                        }
                    }

                    $params['LOWER(' . $filter . ') LIKE LOWER(?)'] = $query;
                } else {
                    $params['LOWER(' . $this->repository->getTable() . '.' . $parameter . ') LIKE LOWER(?)'] = $query;
                }
            }


            return $params;
        }

        return $params['LOWER(' . $this->repository->getTable() . '.' . $searchFields . ') LIKE LOWER(?)'] = $query;
    }
}
