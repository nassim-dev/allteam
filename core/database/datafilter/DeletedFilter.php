<?php

namespace core\database\datafilter;

use core\database\mysql\lib\nette\CustomNetteSelection;
use core\database\RepositoryInterface;

/**
 * Class DeletedFilter
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class DeletedFilter extends AbstractFilter implements DataFilterInterface
{
    public function filter(CustomNetteSelection $selection, RepositoryInterface $repository): CustomNetteSelection
    {
        foreach ($repository->getKeys() as $column) {
            if ($column['name'] == 'flag_delete') {
                $selection->where($repository->getTable() . '.flag_delete = ? OR ' . $repository->getTable() . '.flag_delete ?', 0, null);

                return $selection;
            }
        }


        return $selection;
    }
}
