<?php

namespace core\calc\solver\types\decorator;

use core\calc\solver\Constraint;
use core\calc\solver\ObjectiveFunction;
use core\calc\solver\Solution;
use core\calc\solver\Variable;

/**
 * Undocumented interface
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
interface SolverInterface
{
    public function render(Variable|Solution|Constraint|ObjectiveFunction $object): string;
}
