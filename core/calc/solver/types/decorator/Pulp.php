<?php

namespace core\calc\solver\types\decorator;

use core\calc\solver\Constraint;
use core\calc\solver\ObjectiveFunction;
use core\calc\solver\Solution;
use core\calc\solver\Variable;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Pulp implements SolverInterface
{
    public function render(Variable|Solution|Constraint|ObjectiveFunction $object): string
    {
        switch ($object::class) {
            case Variable::class:
                return $this->renderVariable($object);
            case Solution::class:
                return $this->renderSolution($object);
            case Constraint::class:
                return $this->renderConstraint($object);
            case ObjectiveFunction::class:
                return $this->renderObjectiveFunction($object);
        }
    }
    /**
     * Render a variable
     *
     * @param Variable $variable
     *
     * @return string
     */
    private function renderVariable(Variable $variable): string
    {
        return str_replace('.', '_', $variable->getName()) . ' = LpVariable("' . $variable->getDescription() . '", ' . $variable->getLowerLimit() . ', ' . $variable->getUpperLimit() . ' , ' . $variable->getType() . ')';
    }

    /**
     * Render a solution
     *
     * @param Solution $solution
     *
     * @return string
     */
    private function renderSolution(Solution $solution): string
    {
        return $solution->getSign() . ' ' . $solution->getValue();
    }

    /**
     * Render a constraint
     *
     * @param Constraint $constraint
     *
     * @return string
     */
    private function renderConstraint(Constraint $constraint): string
    {
        $return = [];
        /** @var Variable $variable */
        foreach ($constraint->getVariables() as $variable) {
            $return[] .= '(' . $variable['coef'] . ' * ' . $variable['variable']->getName() . ')';
        }

        return implode(' + ', $return) . ' ' . $this->renderSolution($constraint->getSolution());
    }

    /**
     * Render an objective function
     *
     * @param ObjectiveFunction $function
     *
     * @return string
     */
    private function renderObjectiveFunction(ObjectiveFunction $function): string
    {
        $return = '
from pulp import *
prob = LpProblem("' . $function->getName() . '", ' . $function->getType()->getValue() . ')';

        foreach ($function->getVariables() as $variable) {
            $return .= '
' . $this->renderVariable($variable['variable']);
        }

        foreach ($function->getConstraints() as $constraint) {
            $return .= '
prob += ' . $this->renderConstraint($constraint['constraint']);
        }

        $return .= '
prob.writeLP("' . $function->getName() . '.lp")';

        $return .= '
prob.solve()';


        $return .= '
solution = prob.variables()
solution[objective] = value(prob.objective)
json_object = json.dumps(solution, indent=4):
with open("' . BASE_DIR . 'python/json/' . $function->getName() . '.json", "w") as outfile :
    outfile.write(json_object)';

        return $return;
    }
}
