<?php

namespace core\calc\solver\types\decorator;

use core\calc\solver\Constraint;
use core\calc\solver\ObjectiveFunction;
use core\calc\solver\Solution;
use core\calc\solver\Variable;

/**
 * Undocumented class
 *
 * https://developers.google.com/optimization/lp/lp_example#python_4
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Glop implements SolverInterface
{
    public function render(Variable|Solution|Constraint|ObjectiveFunction $object): string
    {
        switch ($object::class) {
            case Variable::class:
                return $this->renderVariable($object);
            case Solution::class:
                return $this->renderSolution($object);
            case Constraint::class:
                return $this->renderConstraint($object);
            case ObjectiveFunction::class:
                return $this->renderObjectiveFunction($object);
        }
    }
    /**
     * Render a variable
     *
     * @param Variable $variable
     *
     * @return string
     */
    private function renderVariable(Variable $variable): string
    {
        return str_replace('.', '_', $variable->getName());
    }

    /**
     * Render a solution
     *
     * @param Solution $solution
     *
     * @return string
     */
    private function renderSolution(Solution $solution): string
    {
        return $solution->getSign() . ' ' . $solution->getValue();
    }

    /**
     * Render a constraint
     *
     * @param Constraint $constraint
     *
     * @return string
     */
    private function renderConstraint(Constraint $constraint): string
    {
        $return = '';
        /** @var Variable $variable */
        foreach ($constraint->getVariables() as $variable) {
            $return .= ($variable['coef'] > 0) ? ' + ' . $variable['coef'] : ' ' . $variable['coef'] . ' * ' . $variable['variable']->getName();
        }

        return $return . ' ' . $this->renderSolution($constraint->getSolution());
    }

    /**
     * Render an objective function
     *
     * @param ObjectiveFunction $function
     *
     * @return string
     */
    private function renderObjectiveFunction(ObjectiveFunction $function): string
    {
        $return = '
from ortools.linear_solver import pywraplp

def LinearProgrammingExample():
    """Linear programming sample."""
    # Instantiate a Glop solver, naming it LinearExample.
    solver = pywraplp.Solver.CreateSolver(\'' . $function->getName() . '\')
    if not solver:
        return
';
        $objective = [];
        foreach ($function->getVariables() as $variable) {
            $return .= '
' . $variable['variable']->getName() . ' = solver.NumVar(' . $this->renderVariable($variable['variable']) . ')';
            $objetive[] = $variable['variable']->getName() . ' * ' . $variable['variable']->getCoef();
        }

        foreach ($function->getConstraints() as $constraint) {
            $return .= '
    solver.Add(' . $this->renderConstraint($constraint['constraint']) . ')';
        }

        $return .= '
    solver.' . str_replace('Lp', '', $function->getType()->getValue()) . '(' . implode(' + ', $objective) . ')';

        $return .= '
    status = solver.Solve()';


        $return .= '
    result = {}
    if status == pywraplp.Solver.OPTIMAL:
        result[OBJECTIVEVALUE] = solver.Objective().Value()';
        foreach ($function->getVariables() as $variable) {
            $return .= '
        result[' . $variable['variable']->getName() . '] = ' . $variable['variable']->getName() . '.solution_value()
    ';
        }

        $return .= '
    json_object = json.dumps(result, indent=4):
    with open("' . BASE_DIR . 'python/json/' . $function->getName() . '.json", "w") as outfile :
        outfile.write(json_object)';

        return $return;
    }
}
