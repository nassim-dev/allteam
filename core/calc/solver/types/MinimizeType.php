<?php

namespace core\calc\solver\types;

class MinimizeType implements TypeInterface
{
    private string $name = '';

    /**
     * Get the value of name
     *
     * @PhpUnitGen\get("name")
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @PhpUnitGen\set("name")
     * @return self
     */
    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getValue(): string
    {
        return 'LpMinimize';
    }
}
