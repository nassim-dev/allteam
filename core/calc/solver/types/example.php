<?php

use core\calc\solver\Constraint;
use core\calc\solver\ObjectiveFunction;
use core\calc\solver\Solution;
use core\calc\solver\SolverTask;
use core\calc\solver\types\MaximizeType;
use core\calc\solver\Variable;
use core\tasks\AsyncJob;

$a = new Variable('a');
$b = new Variable('b');
$c = new Variable('c');

$function = new ObjectiveFunction(new MaximizeType(), 'MyFunction');
//Objective function -1a + 2b + 5c
$function->addVariable($a, -1);
$function->addVariable($b, 2);
$function->addVariable($c, 5);

$constraintA = new Constraint('constraintA');
$constraintA->addVariable($a, 2);
$constraintA->addVariable($b, -4);
$constraintA->setSolution(new Solution('>=', 25));

$constraintB = new Constraint('constraintB');
$constraintB->addVariable($a, 8);
$constraintB->addVariable($c, 12);
$constraintB->setSolution(new Solution('>=', 37));

$function->addConstraint($constraintA);
$function->addConstraint($constraintB);

$function->solve((new AsyncJob('SolverTask'))->setCallable(SolverTask::class));
