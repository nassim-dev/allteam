<?php

namespace core\calc\solver\types;

/**
 * Undocumented interface
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
interface TypeInterface
{
    public function getName(): string;

    public function setName(string $name): static;

    public function getValue(): string;
}
