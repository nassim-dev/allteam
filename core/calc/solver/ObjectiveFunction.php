<?php

namespace core\calc\solver;

use Composer\DependencyResolver\Solver;
use core\calc\types\TypeInterface;
use core\tasks\AsyncJob;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class ObjectiveFunction
{
    /**
     * @var array
     */
    private array $variables = [];

    /**
     * @var array
     */
    private array $constraints = [];

    public function __construct(private TypeInterface $type, private string $name)
    {
    }

    public function solve(AsyncJob $asyncJob)
    {
        $asyncJob->setCallable(Solver::class);
        $asyncJob->setContext(['problem' => $this]);
        $asyncJob->save();
    }

    /**
     * Get the value of name
     *
     * @PhpUnitGen\get("name")
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @PhpUnitGen\set("name")
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }


    /**
     * Add new variable definition
     *
     * @param string $variableName
     *
     * @return Variable
     */
    public function addVariable(Variable $variableName, float $coef = 1.0): Variable
    {
        $variable          = ($variableName instanceof Variable) ? $variableName : new Variable($variableName);
        $this->variables[] = ['variable' => $variable, 'coef' => $coef];

        return $variable;
    }

    /**
     * Add new variable definition
     *
     * @return Constraint
     */
    public function addConstraint(Constraint|string $constraintName): Constraint
    {
        $constraint          = ($constraintName instanceof Constraint) ? $constraintName : new Constraint($constraintName);
        $this->constraints[] = ['constraint' => $constraint];

        return $constraint;
    }

    /**
     * Get the value of type
     *
     * @PhpUnitGen\get("type")
     *
     * @return TypeInterface
     */
    public function getType(): TypeInterface
    {
        return $this->type;
    }

    /**
     * Set the value of type
     *
     * @param mixed $type
     *
     * @PhpUnitGen\set("type")
     *
     * @return self
     */
    public function setType(TypeInterface $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get the value of variables
     *
     * @PhpUnitGen\get("variables")
     *
     * @return array
     */
    public function getVariables(): array
    {
        return $this->variables;
    }

    /**
     * Get the value of constraints
     *
     * @PhpUnitGen\get("constraints")
     *
     * @return array
     */
    public function getConstraints(): array
    {
        return $this->constraints;
    }
}
