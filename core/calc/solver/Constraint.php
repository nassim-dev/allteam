<?php

namespace core\calc\solver;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Constraint
{
    private array $variables = [];

    private ?Solution $solution = null;

    public function __construct(private string $name)
    {
    }


    /**
     * Get the value of name
     *
     * @PhpUnitGen\get("name")
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @PhpUnitGen\set("name")
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Add new variable definition
     *
     * @param string $variableName
     *
     * @return Variable
     */
    public function addVariable(string|Variable $variableName, float $coef = 1.0): Variable
    {
        $variable          = ($variableName instanceof Variable) ? $variableName : new Variable($variableName);
        $this->variables[] = ['variable' => $variable, 'coef' => $coef];

        return $variable;
    }


    public function setSolution(Solution $solution)
    {
        $this->solution = $solution;

        return $this;
    }


    /**
     * Get the value of variables
     *
     * @PhpUnitGen\get("variables")
     *
     * @return array
     */
    public function getVariables(): array
    {
        return $this->variables;
    }

    /**
     * Get the value of solution
     *
     * @PhpUnitGen\get("solution")
     *
     * @return mixed
     */
    public function getSolution()
    {
        return $this->solution;
    }
}
