<?php

namespace core\calc\solver;

use core\calc\solver\types\decorator\SolverInterface;
use core\tasks\TaskInterface;
use core\utils\Utils;

/**
 * Class SolverTask
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class SolverTask implements TaskInterface
{
    public const PYTHON_DIR = BASE_DIR . 'python/scripts/';

    public const JSON_DIR = BASE_DIR . 'python/json/';

    public function __construct(private SolverInterface $solver)
    {
    }
    public function run(array $context)
    {
        Utils::mkdir(self::PYTHON_DIR);
        Utils::mkdir(self::JSON_DIR);

        /** @var ObjectiveFunction $problem */
        $problem = $context['problem'];
        $json    = self::JSON_DIR . $problem->getName() . '.json';

        if (file_exists($json)) {
            unlink($json);
        }

        file_put_contents('nette.safe://' . self::PYTHON_DIR . $problem->getName() . '.py', $this->solver->render($problem));

        //$elapsedTime = 0;
        //while (!file_exists($json) && $elapsedTime < 360) {
        //    sleep(1);
        //    $elapsedTime++;
        //}
    }
}
