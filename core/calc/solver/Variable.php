<?php

namespace core\calc\solver;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Variable
{
    /**
     * Define a new variable
     *
     * @param string $name variable name
     */
    public function __construct(
        private string $name,
        private ?string $description = null,
        private ?float $lowerLimit = 0.0,
        private ?float $upperLimit = 0.0,
        private string $type = 'LpContinuous'
    ) {
    }

    /**
     * Get the value of name
     *
     * @PhpUnitGen\get("name")
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @PhpUnitGen\set("name")
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of description
     *
     * @PhpUnitGen\get("description")
     *
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * Set the value of description
     *
     * @PhpUnitGen\set("description")
     * @return self
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of upperLimit
     *
     * @PhpUnitGen\get("upperLimit")
     *
     * @return float|null
     */
    public function getUpperLimit(): ?float
    {
        return $this->upperLimit;
    }

    /**
     * Get the value of lowerLimit
     *
     * @PhpUnitGen\get("lowerLimit")
     *
     * @return float|null
     */
    public function getLowerLimit(): ?float
    {
        return $this->lowerLimit;
    }

    /**
     * Get the value of type
     *
     * @PhpUnitGen\get("type")
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }
}
