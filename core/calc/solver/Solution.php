<?php

namespace core\calc\solver;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Solution
{
    public function __construct(
        private string $sign,
        private float $value,
        private ?string $name = null
    ) {
    }

    /**
     * Get the value of sign
     *
     * @PhpUnitGen\get("sign")
     *
     * @return string
     */
    public function getSign(): string
    {
        return $this->sign;
    }

    /**
     * Set the value of sign
     *
     * @PhpUnitGen\set("sign")
     * @return self
     */
    public function setSign(string $sign)
    {
        $this->sign = $sign;

        return $this;
    }

    /**
     * Get the value of value
     *
     * @PhpUnitGen\get("value")
     *
     * @return float
     */
    public function getValue(): float
    {
        return $this->value;
    }

    /**
     * Set the value of value
     *
     * @PhpUnitGen\set("value")
     * @return self
     */
    public function setValue(float $value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get the value of name
     *
     * @PhpUnitGen\get("name")
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @PhpUnitGen\set("name")
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
