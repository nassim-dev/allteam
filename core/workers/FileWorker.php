<?php

namespace core\workers;

use core\config\Conf;
use core\logger\LogServiceInterface;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Nette\Neon\Neon;
use Nette\Utils\Finder;
use SplFileInfo;

/**
 * Just a file migration worker
 *
 * See Conf::saveInfile
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class FileWorker implements WorkerInterface
{
    /**
     * @var mixed
     */
    public bool $empty = true;

    /**
     * Is the worker running
     *
     * @var bool
     */
    public $isRunning = false;

    /**
     * @return mixed
     */
    public function __construct(
        private LogServiceInterface $logger,
        public bool $locked = true
    ) {
    }

    public function kill()
    {
        $this->logger->dump(
            [
                'data'    => 'Kill worker PID ' . getmypid(),
                'logfile' => ASYNC_LOGFILE
            ]
        );
        exec('kill -9 ' . getmypid());
    }

    /**
     * Run the worker loop
     *
     * @todo check if a task is throwing an error loop
     *
     * @return void
     */
    public function tick()
    {
        usleep(random_int(1000, 2000));
        if (!$this->isRunning) {
            $this->empty     = false;
            $this->isRunning = true;
            $return          = [];
            $streams         = [];
            //Find files migrations
            $files = Finder::find('*')->in(BASE_DIR . 'tmp/files_operations');
            foreach ($files as $key => $file) {
                /** @var SplFileInfo $file */
                $parts = explode('_', $file->getBasename());

                try {
                    $token = JWT::decode(
                        end($parts),
                        new Key(
                            Conf::find('SECURITY.JSONRPC.KEY'),
                            Conf::find('SECURITY.JSONRPC.METHOD')
                        )
                    );

                    $return[$token->file] ??= Neon::decode(file_get_contents('nette.safe://' . BASE_DIR . $token->file));
                    $this->set($return[$token->file], $token->keys, $token->value);
                } catch (\Throwable $th) {
                    //throw $th;
                } finally {
                    unlink($key);
                }
            }

            //Wrte changes
            foreach ($streams as $stream) {
                if (is_resource($stream)) {
                    fclose($stream);
                }
            }

            foreach ($return as $filename => $contents) {
                if (str_ends_with($filename, '.neon')) {
                    $contents = Neon::Encode($contents);
                }

                if (str_ends_with($filename, '.json')) {
                    $contents = json_encode($contents);
                }

                file_put_contents('nette.safe://' . BASE_DIR . $filename, $contents);
            }

            $this->empty     = true;
            $this->isRunning = false;
        }
    }

    private function set(array &$initialArray, array $keys, $valueToSet)
    {
        foreach ($initialArray as $key => $value) {
            if (count($keys) === 1) {
                return $initialArray = $valueToSet;
            }
            if ($key != $keys[0]) {
                continue;
            }
            array_shift($keys);

            return $this->set($value, $keys, $valueToSet);
        }
    }

    public function getLoopCondition(): bool
    {
        return count(scandir(BASE_DIR . 'tmp/files_operations')) > 2;
    }
}
