<?php

namespace core\workers;

use core\logger\LogServiceInterface;
use core\messages\queue\MessageQueueInterface;
use core\tasks\AsyncJob;
use core\tasks\TaskException;

class Worker implements WorkerInterface
{
    /**
     * @var mixed
     */
    public bool $empty = true;

    /**
     * Is the worker running
     *
     * @var bool
     */
    public $isRunning = false;

    /**
     * @return mixed
     */
    public function __construct(
        private MessageQueueInterface $queue,
        private LogServiceInterface $logger,
        public bool $locked = true
    ) {
    }

    public function kill()
    {
        $this->logger->dump(
            [
                'data'    => 'Kill worker PID ' . getmypid(),
                'logfile' => ASYNC_LOGFILE
            ]
        );
        exec('kill -9 ' . getmypid());
    }

    /**
     * Run the worker loop
     *
     * @todo check if a task is throwing an error loop
     *
     * @return void
     */
    public function tick()
    {
        usleep(random_int(1000, 2000));
        $this->queue->readThen(['queue.priority.high', 'queue.priority.normal', 'queue.priority.low'], [$this, 'handleResult']);
    }

    public function getLoopCondition(): bool
    {
        return $this->queue->getLoopCondition();
    }

    public function handleResult($trade)
    {
        if ($trade instanceof AsyncJob) {
            $this->empty     = false;
            $this->isRunning = true;
            $this->executeTask($trade);
        } else {
            if (!$this->empty) {
                $this->isRunning = false;
                $this->logger->dump(
                    [
                        'data'    => 'No trades registred on worker PID ' . getmypid() . ', or queue finished',
                        'logfile' => ASYNC_LOGFILE
                    ]
                );
            }

            $this->empty = true;
        }
    }

    private function executeTask(?AsyncJob $task)
    {
        if ($task === null) {
            $this->logger->dump(
                [
                    'data'    => 'Invalid task provided on worker PID ' . getmypid(),
                    'logfile' => ASYNC_LOGFILE
                ]
            );

            return;
        }

        $this->logger->dump(
            [
                'data'    => 'Executing ' . $task->getCallable() . ' on worker  PID ' . getmypid() . '...',
                'logfile' => ASYNC_LOGFILE
            ]
        );

        try {
            $task->run();
        } catch (TaskException $exception) {
            $this->logger->dump(
                [
                    'data'    => $exception->getMessage(),
                    'logfile' => ASYNC_LOGFILE
                ]
            );
        }
    }
}
