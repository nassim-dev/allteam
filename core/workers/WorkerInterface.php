<?php

namespace core\workers;

/**
 * Interface WorkerInterface
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
interface WorkerInterface
{
    /**
     * Kill worker process
     *
     * @return void
     */
    public function kill();

    /**
     * Run the worker loop
     *
     * @todo check if a taksk is throwing an error loop
     *
     * @return void
     */
    public function tick();


    /**
     * Condition for execute loop
     */
    public function getLoopCondition(): bool;
}
