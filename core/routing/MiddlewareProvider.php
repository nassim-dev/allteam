<?php

namespace core\routing;

use core\DI\DiProvider;
use core\middleware\MiddlewareDispatcher;

/**
 * Trait MiddlewareProvider
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait MiddlewareProvider
{
    use DiProvider;

    /**
     * MiddlewareDispatcher implémentation
     *
     * @var MiddlewareDispatcher
     */
    private $middleware;

    /**
     * Return MiddlewareDispatcher implémetation
     */
    public function getMiddleware(): MiddlewareDispatcher
    {
        return $this->middleware ??= $this->getDi()->singleton(MiddlewareDispatcher::class);
    }
}
