<?php

namespace core\routing;

use core\cache\CacheServiceInterface;
use core\cache\CacheUpdaterTrait;
use core\extensions\ExtensionLauncherServiceInterface;
use ReflectionClass;

/**
 * Class Linker
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class Linkifier
{
    use CacheUpdaterTrait;

    private ?array $links = null;

    public function __construct(private CacheServiceInterface $cache, private ExtensionLauncherServiceInterface $launcher)
    {
        $this->_fetchPropsFromCache(['links']);
        if (!is_array($this->links)) {
            $this->links = [];
        }
    }

    public function __destruct()
    {
        $this->_cacheMultiplesProps(['links']);
    }

    /**
     * Return new link
     */
    public function to(string $controller, string $method): Link
    {
        return new Link($controller, $method);
    }

    /**
     * Get Route url for controller
     */
    public function generate(Link $link, array $parameters = []): ?string
    {
        $identifier = $link->getSignature($parameters);
        if (isset($this->links[$identifier])) {
            $link->setMethod($this->links[$identifier]['method']);

            return $this->links[$identifier]['endpoint'];
        }

        if (!class_exists($link->class)) {
            return null;
        }

        $reflection = new ReflectionClass($link->class);
        $method     = $reflection->getMethod($link->method);
        $attributes = $method->getAttributes(RouteAttribute::class);
        foreach ($attributes as $attributeReflected) {
            /**
             * @var RouteAttribute $attribute
             */
            $attribute = $attributeReflected->newInstance();
            $attribute->decorateRoute($reflection->getNamespaceName(), $attribute->getBasePath($reflection->getNamespaceName(), $this->launcher));

            $link->setMethod($attribute->method);

            $this->links[$identifier] = [
                'endpoint' => $link->generateUrl($attribute->url, $parameters),
                'method'   => $attribute->method
            ];

            return $this->links[$identifier]['endpoint'];
        }

        throw new RouteException('Unable to generate an endpoint for requested link : ' . $link->class . '::' . $link->method . ' maybe because the method is innexisting or the method does not have an associated route');
    }
}
