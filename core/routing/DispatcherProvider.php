<?php

namespace core\routing;

use core\DI\DiProvider;

/**
 * Trait DispatcherProvider
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait DispatcherProvider
{
    use DiProvider;

    /**
     * DispatcherServiceInterface implémentation
     *
     * @var DispatcherServiceInterface
     */
    private $dispatcher;

    /**
     * Return DispatcherServiceInterface implémetation
     */
    public function getDispatcher(): DispatcherServiceInterface
    {
        return $this->dispatcher ??= $this->getDi()->singleton(DispatcherServiceInterface::class);
    }
}
