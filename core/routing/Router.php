<?php

namespace core\routing;

use app\api\controller\DefaultController as ApiDefaultController;
use app\controller\DefaultController;
use core\app\ApplicationServiceInterface;
use core\config\Conf;
use core\controller\ControllerInterface;
use core\controller\ControllerServiceInterface;
use core\DI\DiProvider;
use core\logger\router\TracyRouterPanel;
use core\messages\request\HttpRequestInterface;
use core\secure\authentification\AuthServiceInterface;
use Nette\Utils\Arrays;
use Tracy\Debugger;

/**
 * Class Router
 *
 * Represent Router object
 *
 * @category Represent Router object
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Router implements RouterServiceInterface
{
    use DiProvider;
    /**
     *  Contains all the routes
     *
     * @var array
     */
    public array $routes = [
        'GET'    => [],
        'POST'   => [],
        'PUT'    => [],
        'PATCH'  => [],
        'DELETE' => []
    ];

    private string $_endpoint = '/';

    /**
     * @var Route[]
     */
    private array $_matchingRoutes = [];

    private array $_defaultControllers = [
        'APP' => DefaultController::class,
        'API' => ApiDefaultController::class
    ];

    /**
     * Constructor
     */
    public function __construct(
        private HttpRequestInterface $request,
        private ControllerServiceInterface $controllerService
    ) {
        if (Conf::find('ENVIRONNEMENT') === 'development'
        && CONTEXT === 'APP'
        && !isset($_SERVER['x-requested-with'])) {
            Debugger::getBar()->addPanel(new TracyRouterPanel($this));
        }
    }

    /**
     * Add New Route
     *
     * @param  array   ...Route parameters
     */
    public function addRoute(): Route
    {
        $args  = func_get_args();
        $route = ($args[0] instanceof Route) ? $args[0] : new Route(...$args);

        return $this->insertRoute($route);
    }

    /**
     * Return default route
     */
    public function getDefaultRoute(): ?string
    {
        return $this->_endpoint;
    }

    /**
     * Run Router
     *
     * @return ControllerInterface|null
     */
    public function run(
        AuthServiceInterface $auth,
        ApplicationServiceInterface $app
    ): ?ControllerInterface {
        //Find the matching methods
        $this->_matchingRoutes = $this->routes[$this->request->getMethod()] ?? [];

        //In previous matching routes find the ones matching the pattern
        $this->_findMatchingPattern($this->_matchingRoutes, $this->request);

        //If no route match
        if (count((array) $this->_matchingRoutes) === 0) {
            $controller = $this->getDi()->singleton($this->_defaultControllers[CONTEXT]);
            $app->setController($controller);

            $this->getDi()->call($controller, 'errorAction', ['errorCode' => 404]);

            return null;
        }

        $route = $this->getMatchingRoute();
        $auth->allowAccess();

        //Check if requested route is allowed
        if (!$route->getPermissionSet()->process($auth->getUser(), $route)) {
            $auth->denyAccess();
            $elements = explode('::', $route->getControllerSignature());
            $route->setControllerSignature($elements[0] . '::errorAction');
            $route->setCallback(function () use ($elements) {
                $args       = func_get_args();
                $controller = $this->controllerService->create($elements[0], $args);

                $this->getDi()->call($controller, 'errorAction', ['errorCode' => 403]);

                return $controller;
            });
        }

        //Run first route founded
        /** @var ControllerInterface $controller */
        $controller = $route->run();
        $app->setController($controller);

        //Build main menu in private context
        if (!$controller->isPublic() && CONTEXT === 'APP' && !$this->request->isAjaxify()) {
            $this->controllerService->registerMenuActions(_('Core'), 'fa-cat');
        }

        return $controller;
    }

    /**
     * If nothing match the default route to apply
     *
     * @return void
     */
    public function setDefaultRoute(string $route)
    {
        $this->_endpoint = $route;
    }

    /**
     * Find matching pater in routes
     *
     * @param Route[] $routes
     *
     * @return void
     */
    private function _findMatchingPattern(array $routes, HttpRequestInterface $request)
    {
        //Reset the matching pattern array

        $this->_matchingRoutes = [];
        foreach ($routes as $route) {
            if ($route->patternMatches($request->getUrl())) {
                $this->_matchingRoutes[] = $route;
                $request->setParameters($route->getParam());

                return;
            }
        }
    }

    /**
     * Insert route in array depending regex size
     */
    private function insertRoute(Route $route): Route
    {
        $count = strlen($route->getPattern());
        if (!isset($this->routes[$route->getMethod()][$count])) {
            return $this->routes[$route->getMethod()][$count] = $route;
        }

        foreach ($this->routes[$route->getMethod()] as $index => $registredRoute) {
            $actualCount = strlen($registredRoute->getPattern());
            if ($actualCount < $count) {
                continue;
            }

            Arrays::insertBefore($this->routes[$route->getMethod()], $index, [$route]);

            return $route;
        }

        return $route;
    }

    /**
     * Get the value of _matchingRoutes
     */
    public function getMatchingRoute(): ?Route
    {
        return $this->_matchingRoutes[0] ?? null;
    }
}
