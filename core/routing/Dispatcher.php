<?php

namespace core\routing;

use core\cache\CacheServiceInterface;
use core\cache\CacheUpdaterTrait;
use core\extensions\ExtensionLauncherServiceInterface;
use core\messages\request\HttpRequestInterface;
use ReflectionClass;
use ReflectionMethod;
use Swoole\Http2\Request as RequestH2;
use Swoole\Http\Request;

/**
 * Class Dispatcher
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Dispatcher implements DispatcherServiceInterface
{
    use CacheUpdaterTrait;

    private array $_controllers = [];

    public function __construct(
        private RouterServiceInterface $router,
        private HttpRequestInterface $request,
        private CacheServiceInterface $cache,
        private ExtensionLauncherServiceInterface $launcher,
        private ?array $_mapping = []
    ) {
        $this->_fetchPropsFromCache(['_mapping', '_controllers']);
        if (!is_array($this->_mapping)) {
            $this->_mapping = [];
        }

        if (!is_array($this->_controllers)) {
            $this->_controllers = [];
        }
    }

    public function __destruct()
    {
        $this->_cacheMultiplesProps(['_mapping', '_controllers']);
    }

    /**
     * Register new namespace shortcut
     */
    public function registerNamespace(string $namespace, string $identifier): self
    {
        if (!array_key_exists($identifier, $this->_mapping ?? [])) {
            $this->_mapping[$identifier] = $namespace;
        }

        if ($this->_mapping[$identifier] !== $namespace) {
            throw new DispatcherException("Endpoint '$identifier' is already use by " . $this->_mapping[$identifier] . ', please choose an other one');
        }

        return $this;
    }

    /**
     * Register new controller
     */
    public function registerController(string $controller, string $basePath): string
    {
        if (!array_key_exists($controller, $this->_controllers)) {
            $basePath = str_replace('Extension', '', $basePath);
            $basePath = (str_contains($controller, '\\api\\')) ? "/api/e/$basePath/" : "/e/$basePath/";
            $basePath = strtolower($basePath);
            $basePath = str_replace(' ', '-', $basePath);

            $namespaceElements = explode('\\', $controller);

            array_pop($namespaceElements);
            $this->_controllers[$controller] = implode('\\', $namespaceElements);

            $this->registerNamespace($this->_controllers[$controller], $basePath);
        }

        return $this->_controllers[$controller];
    }

    /**
     * Register route for associated controller (find with request object)
     */
    public function dispatch(): RouterServiceInterface
    {
        $this->_registerRoutes($this->_findController());

        return $this->router;
    }

    /**
     * Find controller shortcut
     */
    private function _findController(): string
    {
        $context  = (CONTEXT === 'APP') ? '/' : '/api/';
        $baseName = $context . (($this->request->isExtension()) ? 'e/' : null) . $this->getExtensionPath();

        if ($baseName[-1] !== '/') {
            $baseName .= '/';
        }

        if (is_string($baseName) && isset($this->_mapping[$baseName])) {
            $controller = $this->request->getPage() ?? 'Default';

            return $this->_mapping[$baseName] . '\\' . ucfirst($controller) . 'Controller';
        }

        throw new DispatcherException('Unregistred controller for ' . $baseName);
    }

    /**
     * Create specific routes for controller methods
     *
     * @return void
     */
    private function _registerRoutes(string $controller)
    {
        $attributes = $this->listRoutes($controller);

        if (null !== $attributes) {
            foreach ($attributes as [$attribute, $method]) {
                /** @var RouteAttribute $attribute */
                /** @var ReflectionMethod $method */
                if ($this->request->getMethod() === $attribute->method) {
                    $attribute->buildRoute($method, $this->router, $this->launcher);
                }
            }
        }
    }

    /**
     * Return extension path if exists
     */
    private function getExtensionPath(): ?string
    {
        if ($this->request->isExtension()) {
            return (CONTEXT === 'APP') ? $this->request->getQuery()[1] : $this->request->getQuery()[3];
        }

        return null;
    }

    /**
     * List routes for a specific controller class
     */
    public function listRoutes(string $controller): ?array
    {
        if (!class_exists($controller)) {
            return null;
        }

        $reflection = new ReflectionClass($controller);
        //$extensionPath = $this->getExtensionPath();

        $routeAttributes = [];
        foreach ($reflection->getMethods() as $method) {
            $attributes = $method->getAttributes(RouteAttribute::class);
            foreach ($attributes as $attributeReflected) {
                /** @var RouteAttribute $attribute */
                $attribute = $attributeReflected->newInstance();
                //$attribute->decorateRoute($reflection->getNamespaceName(), $extensionPath);

                $routeAttributes[$reflection->getName() . '::' . $method->getName()] = [$attribute, $method];
            }
        }


        return $routeAttributes;
    }

    /**
     * Set the value of request
     *
     * @param Request|RequestH2|HttpRequestInterface $request
     *
     * @PhpUnitGen\set("request")
     *
     * @return self
     */
    public function setRequest(Request|RequestH2|HttpRequestInterface $request): self
    {
        $this->request->populate($request);

        return $this;
    }
}
