<?php

namespace core\routing;

use core\messages\request\HttpRequestInterface;
use Swoole\Http2\Request as RequestH2;
use Swoole\Http\Request;

/**
 * Interface DispatcherServiceInterface
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
interface DispatcherServiceInterface
{
    public function dispatch(): RouterServiceInterface;

    /**
     * Register new controller shortcut
     */
    public function registerNamespace(string $namespace, string $identifier): self;

    /**
     * Register new controller
     */
    public function registerController(string $controller, string $basePath): string;

    /**
     * Undocumented function
     */
    public function listRoutes(string $controller): ?array;

    /**
     * Set the value of request
     *
     * @param Request|RequestH2|HttpRequestInterface $request
     *
     * @PhpUnitGen\set("request")
     *
     * @return self
     */
    public function setRequest(Request|RequestH2|HttpRequestInterface $request): self;
}
