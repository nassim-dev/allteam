<?php

namespace core\routing;

use core\controller\ControllerInterface;
use core\secure\components\RessourceInterface;
use core\secure\components\RessourceTrait;
use core\secure\permissions\PermissionSetInterface;
use core\utils\Utils;

/**
 * Class Route
 *
 * Represents a route object for Router
 *
 * @category  Represents a route object for Router
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Route implements RessourceInterface
{
    use RessourceTrait;

    /**
     * Are parmeters encoded ?
     */
    private bool|string $_encode = false;

    /**
     * Parmeters founded in url
     */
    private array $_param = [];

    /**
     * Controller:method
     *
     * @@var string|null
     */
    private $controllerSignature = null;

    /**
     * Constructor
     *
     * @param callable $callback
     * @param string   $rights
     */
    public function __construct(
        private string $method,
        private string $pattern,
        private \Closure $_callback,
        private ?array $permissions = null
    ) {
    }

    public function getHash(): string
    {
        return Utils::hash([$this->method,  $this->patern]);
    }

    /**
     * Get are parmeters encoded ?
     *
     * @PhpUnitGen\get("_encode")
     */
    public function getEncode(): string
    {
        return $this->encode;
    }

    /**
     * Get callback to execute
     *
     * @PhpUnitGen\get("_function")
     */
    public function getFunction(): callable
    {
        return $this->callback;
    }

    /**
     * Get pOST | PUT | GET | DELETE
     *
     * @PhpUnitGen\get("_method")
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * Get parmeters founded in url
     *
     * @PhpUnitGen\get("_param")
     */
    public function getParam(): array
    {
        return $this->_param;
    }

    /**
     * Get patern to match
     *
     * @PhpUnitGen\get("_pattern")
     */
    public function getPattern(): string
    {
        return $this->pattern;
    }

    /**
     * Check params from URL
     */
    public function patternMatches(string $url): bool
    {
        //Extract URL params
        preg_match_all('@:([\w]+)@', $this->pattern, $paramNames, PREG_PATTERN_ORDER);
        $paramNames = $paramNames[1];

        //Convert URL params into regex patterns, construct a regex for this route
        $patternAsRegex = preg_replace_callback('@:[\w]+@', [$this, '_convertPatternToRegex'], $this->pattern);
        if (str_ends_with($this->pattern, '/')) {
            $patternAsRegex .= '?';
        }

        $patternAsRegex = '@^' . $patternAsRegex . '$@';

        //Save URL params' names and values if this route matches the current url
        if (preg_match($patternAsRegex, $url, $paramValues)) {
            array_shift($paramValues);

            foreach ($paramNames as $name) {
                if (isset($paramValues[$name])) {
                    $this->_param[$name] = Utils::decodeRawVar(urldecode($paramValues[$name]), $this->_encode);
                }
            }

            if (isset($this->_param['CUSTOM'])) {
                $this->_param = $this->_param['CUSTOM'];
                unset($this->_param['CUSTOM']);
            }

            return true;
        }

        return false;
    }

    /**
     * Execute callback function
     */
    public function run(): ?ControllerInterface
    {
        //Run the main callback
        if (is_callable($this->_callback)) {
            //return $this->di->callable($this->_callback, array_values($this->_param));
            return call_user_func_array($this->_callback, array_values($this->_param));
        }

        return null;
    }

    /**
     * Set are parmeters encoded ?
     *
     * @PhpUnitGen\set("_encode")
     *
     * @param string $_encode Are parmeters encoded ?
     */
    public function setEncode(string $encode): self
    {
        $this->encode = $encode;

        return $this;
    }

    /**
     * Set callback to execute
     *
     * @PhpUnitGen\set("_callback")
     *
     * @param callable $callback Callback to execute
     */
    public function setCallback(callable $callback): self
    {
        $this->_callback = $callback;

        return $this;
    }

    /**
     * Set pOST | PUT | GET | DELETE
     *
     * @PhpUnitGen\set("_method")
     *
     * @param string $method POST | PUT | GET | DELETE
     */
    public function setMethod(string $method): self
    {
        $this->method = $method;

        return $this;
    }

    /**
     * Set parmeters founded in url
     *
     * @PhpUnitGen\set("_param")
     *
     * @param array   $_param Parmeters founded in url
     * @param mixed[] $param
     */
    public function setParam(array $param): self
    {
        $this->_param = $param;

        return $this;
    }

    /**
     * Set patern to match
     *
     * @PhpUnitGen\set("_pattern")
     *
     * @param string $_pattern Patern to match
     */
    public function setPattern(string $pattern): self
    {
        $this->pattern = $pattern;

        return $this;
    }

    /**
     * Convert :var to real variables
     *
     * @return string
     */
    private function _convertPatternToRegex(array $matches)
    {
        $key = str_replace(':', '', $matches[0]);

        return '(?P<' . $key . '>[a-zA-Z0-9_\-\.\!\~\*\\\'\(\)\:\@\&\=\$\+,%]+)';
    }

    /**
     * Get the value of controllerSignature
     *
     * @PhpUnitGen\get("controllerSignature")
     */
    public function getControllerSignature(): ?string
    {
        return $this->controllerSignature;
    }

    /**
     * Set the value of controllerSignature
     *
     * @param mixed $controllerSignature
     *
     * @PhpUnitGen\set("controllerSignature")
     */
    public function setControllerSignature(?string $controllerSignature): self
    {
        $this->controllerSignature = $controllerSignature;

        return $this;
    }

    public function getOwner(): ?int
    {
        return null;
    }

    public static function buildDefaultPermissions(PermissionSetInterface $permissionSet): PermissionSetInterface
    {
        return $permissionSet;
    }

    /**
     * Get the value of _callback
     *
     * @PhpUnitGen\get("_callback")
     */
    public function getCallback(): \Closure
    {
        return $this->_callback;
    }
}
