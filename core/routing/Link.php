<?php

namespace core\routing;

use core\utils\Utils;

/**
 * Class Link
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class Link
{
    private array $parameters = [];

    /**
     * Route METHOD
     */
    private ?string $apimethod = null;

    /**
     * @param string method
     */
    public function __construct(
        public ?string $class,
        public ?string $method
    ) {
    }

    /**
     * Add one data attribute
     */
    public function addParameter(string $key, string $val): self
    {
        $this->parameters[$key] = $val;

        return $this;
    }

    /**
     * Add  parameters
     */
    public function addParameters(array $parameters): self
    {
        $this->parameters = array_merge($this->parameters, $parameters);

        return $this;
    }

    public function clearParameters()
    {
        $this->parameters = [];
    }

    /**
     * Get formatted url
     */
    public function getEndpoint(array $parameters = []): ?string
    {
        return ($this->class != null && $this->method != null) ? Linker::generate($this, array_merge($this->getParameters(), $parameters)) : null;
    }

    /**
     * Get link method deined in controller
     */
    public function getMethod(): string
    {
        if ($this->apimethod === null) {
            $this->getEndpoint();
        }

        return $this->apimethod;
    }

    /**
     * Get the value of parameters
     *
     * @PhpUnitGen\get("parameters")
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    /**
     * Return link signature
     */
    public function getSignature(array $parameters = []): string
    {
        return Utils::hash([$this->class, $this->method, $parameters]);
    }

    /**
     * Generate url
     *
     * @param string $pattern
     */
    public function generateUrl(string $url, array $parameters = []): string
    {
        if (empty($parameters)) {
            $url = preg_replace('/\/:[a-zA-Z_-]*/m', '', $url);
        }

        foreach ($parameters as $name => $value) {
            $parameter = ':' . $name;

            if (str_contains($url, $parameter)) {
                $url = str_replace($parameter, $value, $url);
            }
        }

        if (str_contains($url, ':')) {
            throw new RouteException("You must provide all required parameters for $url");
        }

        return $url;
    }

    /**
     * Set the value of method
     *
     * @PhpUnitGen\set("method")
     * @return self
     */
    public function setMethod(string $method)
    {
        $this->apimethod = $method;

        return $this;
    }
}
