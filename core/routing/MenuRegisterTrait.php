<?php

namespace core\routing;

use core\DI\DiProvider;
use core\extensions\ExtensionInterface;
use core\html\menu\MenuFactory;
use core\html\treeview\TreeviewFactory;
use core\secure\authentification\AuthServiceInterface;
use ReflectionMethod;

/**
 * Undocumented trait
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
trait MenuRegisterTrait
{
    use DiProvider;
    /**
     * Controller routes
     *
     * @var array|null
     */
    private $_routes;

    /**
     * Genrate routes to register in menu
     *
     * @return array
     */
    private function getRoutes()
    {
        if (is_array($this->_routes)) {
            return $this->_routes;
        }

        $this->_routes = [];
        /** @var DispatcherServiceInterface $dispatcher */
        $dispatcher = $this->getDi()->singleton(DispatcherServiceInterface::class);

        foreach ($this->getControllers() as $controllerName) {
            if (!str_contains($controllerName, 'app\\')) {
                continue;
            }

            $attributes = $dispatcher->listRoutes($controllerName);
            if ($attributes === null) {
                continue;
            }


            foreach ($attributes as [$attribute, $method]) {
                /** @var ReflectionMethod $method */
                /** @var RouteAttribute $attribute */
                if ($method->name === 'errorAction' || !$attribute->visible) {
                    continue;
                }

                if (!isset($this->_routes[$method->class])) {
                    $this->_routes[$method->class] = [];
                }

                $module = explode('\\', $method->class);

                $this->_routes[$method->class][$method->name] = [
                    $attribute->decorateRoute($method->getDeclaringClass()->getNamespaceName(), $this->getBasePath()),
                    str_replace('Controller', '', end($module)),
                    $attribute->permission,
                    $attribute->getDisplayName() ?? $method->name];
            }
        }

        return $this->_routes;
    }

    /**
     * Return base path for route
     */
    public function getBasePath(): ?string
    {
        return (in_array(ExtensionInterface::class, class_implements($this))) ? $this->getName() : null;
    }

    /**
     * Register a menu action with routes
     *
     * @return void
     */
    public function registerMenuActions(string $name, string $icon)
    {
        if (CONTEXT !== 'APP') {
            return;
        }

        if (!empty($this->getRoutes())) {
            /** @var MenuFactory $menuFactory */
            $menuFactory = $this->getDi()->singleton(MenuFactory::class);
            $menu        = $menuFactory->getMainMenu();
            if ($menu === null) {
                return;
            }

            $dropdown = $menu->addDropDown($name, '#', $icon);

            /** @var TreeviewFactory $treeviewFactory */
            $treeviewFactory = $this->getDi()->singleton(TreeviewFactory::class);
            $treeview        = $treeviewFactory->create($name);
            $treeview->setHtmlTemplate($treeview::getBaseTemplateDir() . 'treeview/TreeviewSideMenu.html');

            /** @var AuthServiceInterface $auth */
            $auth = $this->getDi()->singleton(AuthServiceInterface::class);

            /** @var RouteAttribute[] $attributes */
            foreach ($this->getRoutes() as $class => $attributes) {
                $categorie = $treeview->addCategorie($class);
                $categorie->addClasses(['menu-content', 'text-muted', 'mb-0', 'fs-6', 'fw-bold', 'text-uppercase']);

                foreach ($attributes as $method => [$url, $module, $permission, $displayName]) {
                    if ($auth->getPermissionManager()->isAllowed($permission[0] ?? null, $auth->getUser())) {
                        $categorie->setTitle($module);
                        $item = $categorie->addItem($displayName ?? $method);
                        $item->setHref($url);
                        $item->setHtmlTemplate($treeview::getBaseTemplateDir() . 'menu/SideMenuItem.html');
                    }
                }

                if (!$categorie->hasChild()) {
                    $treeview->removeChild($categorie);
                }
            }

            if ($treeview->hasChild()) {
                $dropdown->addTreeview($treeview);
            }

            if (!$dropdown->hasChild()) {
                $menu->removeChild($dropdown);
            }
        }
    }
}
