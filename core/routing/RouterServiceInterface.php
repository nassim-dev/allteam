<?php

namespace core\routing;

use core\app\ApplicationServiceInterface;
use core\controller\ControllerInterface;
use core\secure\authentification\AuthServiceInterface;

/**
 * Interface RouterServiceInterface
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
interface RouterServiceInterface
{
    /**
     * Add New Route
     *
     * @param array ...$parameters
     */
    public function addRoute(): Route;

    /**
     * Return default route
     */
    public function getDefaultRoute(): ?string;

    /**
     * Run Router
     *
     * @return ControllerInterface|null
     */
    public function run(AuthServiceInterface $auth, ApplicationServiceInterface $app): ?ControllerInterface;

    /**
     * Get the value of _matchingRoutes
     *
     * @return null|Route
     */
    public function getMatchingRoute(): ?Route;
}
