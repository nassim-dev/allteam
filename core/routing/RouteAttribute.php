<?php

namespace core\routing;

use core\controller\ControllerServiceInterface;
use core\extensions\ExtensionLauncherServiceInterface;
use core\DI\DI;
use core\secure\permissions\PermissionServiceInterface;
use ReflectionMethod;

/**
 * Class RouteAttribute
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
#[\Attribute(\Attribute::TARGET_METHOD, \Attribute::TARGET_CLASS)]
class RouteAttribute
{
    protected bool $isDecorated = false;

    public function __construct(
        public string $url,
        public string $method = 'GET',
        public ?array $permission = null,
        public ?array $roles = null,
        public bool $encoding = false,
        public bool $visible = true,
        public ?string $displayName = null,
        public ?string $description = null
    ) {
    }

    /**
     * Return diplay name
     */
    public function getDisplayName(): ?string
    {
        return $this->displayName;
    }

    /**
     * Buil a route object and add permissions
     *
     * @param string|null $routePrefix
     */
    public function buildRoute(ReflectionMethod $method, RouterServiceInterface $router, ExtensionLauncherServiceInterface $launcher): Route
    {
        $reflection = $method->getDeclaringClass();

        $this->decorateRoute($reflection->getNamespaceName(), $this->getBasePath($reflection->getNamespaceName(), $launcher));

        $route = $router->addRoute(
            $this->method,
            $this->url,
            function () use ($method) {
                $args              = func_get_args();
                $controllerFactory = DI::singleton(ControllerServiceInterface::class);
                $controller        = $controllerFactory->create($method->class, $args);
                DI::call($controller, $method->name);

                return $controller;
            },
            $this->permission
        );

        /** @var PermissionServiceInterface $pm */
        $pm = DI::singleton(PermissionServiceInterface::class);

        if (is_array($this->permission)) {
            foreach ($this->permission as $permission) {
                $route->getPermissionSet()->addRequiredCondition($pm->getPermissionClass($permission));
            }
        }

        if (is_array($this->roles)) {
            foreach ($this->roles as $role) {
                $role = $pm->getRoleClass($role);
                foreach ($role->getPermissions() as $permission) {
                    $route->getPermissionSet()->addPriorityPermission($permission);
                }
            }
        }

        $route->setControllerSignature($method->class . '::' . $method->name);

        return $route->setEncode($this->encoding);
    }

    /**
     * Decorate route url with custom prefix for extensions
     */
    public function decorateRoute(string $namespace, ?string $basePath = null): string
    {
        $isExension = (str_contains($namespace, 'extensions\\'));

        if ($isExension && null !== $basePath && !$this->isDecorated) {
            $isApi    = (str_contains($namespace, '\api'));
            $basePath = strtolower($basePath);
            $basePath = str_replace(' ', '-', $basePath);

            $this->isDecorated = true;

            return $this->url = ($isApi) ? str_replace('/api', "/api/e/$basePath", $this->url) : "/e/$basePath" . $this->url;
        }

        return $this->url;
    }

    public function getBasePath(string $namespace, ExtensionLauncherServiceInterface $launcher)
    {
        if (!str_contains($namespace, 'extensions\\')) {
            return null;
        }

        $elements      = explode('\\', $namespace);
        $extensionName = $elements[0] . '\\' . $elements[1] . '\\' . $elements[1] . 'Extension';

        return  $launcher->getExtensions()[$extensionName]?->getName() ?? null;
    }
}
