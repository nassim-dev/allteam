<?php namespace core\routing;

/**
 * Class Linker
 *
 *  @method static core\routing\Link to(string $controller, string $method)
 *  @method static string generate(core\routing\Link $link, array $parameters)
 *  @method static string getPrefix()
 *  @method static static setPrefix(string $__prefix)
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class Linker extends \core\facade\HelperFacade
{
    protected static ?string $serviceInterface = 'core\routing\Linkifier';
}
