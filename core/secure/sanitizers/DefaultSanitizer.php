<?php

namespace core\secure\sanitizers;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class DefaultSanitizer implements SanitizerInterface
{
    public const FILTERS = [
        'string'   => FILTER_UNSAFE_RAW,
        'string[]' => [
            'filter' => FILTER_UNSAFE_RAW,
            'flags'  => FILTER_REQUIRE_ARRAY
        ],
        'email' => FILTER_SANITIZE_EMAIL,
        'int'   => [
            'filter' => FILTER_SANITIZE_NUMBER_INT,
            'flags'  => FILTER_REQUIRE_SCALAR
        ],
        'int[]' => [
            'filter' => FILTER_SANITIZE_NUMBER_INT,
            'flags'  => FILTER_REQUIRE_ARRAY
        ],
        'float' => [
            'filter' => FILTER_SANITIZE_NUMBER_FLOAT,
            'flags'  => FILTER_FLAG_ALLOW_FRACTION
        ],
        'float[]' => [
            'filter' => FILTER_SANITIZE_NUMBER_FLOAT,
            'flags'  => FILTER_REQUIRE_ARRAY
        ],
        'url' => FILTER_SANITIZE_URL,
    ];

    /**
     * Santize function
     *
     * @param array $values to process
     * @param array $config configuration
     *
     * @return array
     */
    public function sanitize(array $values, array $config = []): array
    {
        $options = (empty($config)) ? FILTER_UNSAFE_RAW : array_map(fn ($field): int|array => self::FILTERS[$field] ?? FILTER_DEFAULT, $config);

        return filter_var_array($values, $options);
    }
}
