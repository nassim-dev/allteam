<?php

namespace core\secure\sanitizers;

use core\DI\DiProvider;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Sanitizer implements SanitizerServiceInterface
{
    use DiProvider;

    public function __construct(SanitizerInterface $defaultSanitizer)
    {
        $this->register($defaultSanitizer);
    }

    /**
     * Sanitizers
     *
     * @var SanitizerInterface[]
     */
    private $sanitizers = [];

    public function register(SanitizerInterface|string $sanitizer): static
    {
        $class = (is_string($sanitizer)) ? $sanitizer : $sanitizer::class;

        if (!isset($this->sanitizers[$class])) {
            $this->sanitizers[$class] = (is_string($sanitizer)) ? $this->getDi()->singleton($class) : $sanitizer;
        }

        return $this;
    }

    public function deregister(SanitizerInterface|string $sanitizer): static
    {
        $class = (is_string($sanitizer)) ? $sanitizer : $sanitizer::class;

        if (isset($this->sanitizers[$class])) {
            unset($this->sanitizers[$class]);
        }

        return $this;
    }

    /**
     * Santize function
     *
     * @param array $values to process
     * @param array $config configuration
     *
     * @return array
     */
    public function sanitize(array &$values, array $config = []): array
    {
        foreach ($this->sanitizers as $sanitizer) {
            $values = $sanitizer->sanitize($values, $config);
        }

        return $values;
    }

    /**
     * Return a registred sanitizer
     *
     * @return SanitizerInterface|null
     */
    public function getSanitizer(string $sanitizer): ?SanitizerInterface
    {
        return $this->sanitizers[$sanitizer] ?? null;
    }
}
