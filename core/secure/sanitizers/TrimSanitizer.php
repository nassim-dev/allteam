<?php

namespace core\secure\sanitizers;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class TrimSanitizer implements SanitizerInterface
{
    /**
     * Santize function
     *
     * @param array $values to process
     * @param array $config configuration
     *
     * @return array
     */
    public function sanitize(array $values, array $config = []): array
    {
        return $this->arrayTrim($values);
    }

    private function arrayTrim(array $datas): array
    {
        return array_map(function ($item) {
            if (is_string($item)) {
                return trim($item);
            }

            if (is_array($item)) {
                return $this->arrayTrim($item);
            }

            return $item;
        }, $datas);
    }
}
