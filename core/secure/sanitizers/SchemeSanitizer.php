<?php

namespace core\secure\sanitizers;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class SchemeSanitizer implements SanitizerInterface
{
    public const FILE_TYPE = 'file';

    public const FILE_TYPE_ARRAY = 'file[]';

    /**
     * Santize function
     *
     * @param array $values to process
     * @param array $config configuration
     *
     * @return array
     */
    public function sanitize(array $values, array $config = []): array
    {
        foreach ($config as $field => $type) {
            if ($type === self::FILE_TYPE) {
                $values[$field] = $this->removeProtocol($values[$field]);

                continue;
            }

            if ($type === self::FILE_TYPE_ARRAY) {
                foreach ($values[$field] as $key => $filename) {
                    $values[$field][$key] = $this->removeProtocol($filename);
                }
            }
        }

        return $values;
    }

    /**
     * Remove scheme protocol to prevent php injection
     *
     * @return string|null
     */
    private function removeProtocol(?string $value): ?string
    {
        if (str_contains($value, '://')) {
            $value = explode('://', $value);
            $value = $value[1];
        }

        return $value;
    }
}
