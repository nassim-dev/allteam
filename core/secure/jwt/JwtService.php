<?php

namespace core\secure\jwt;

use core\config\Conf;
use core\messages\request\HttpRequestInterface;
use core\secure\user\UserInterface;
use core\session\SessionInterface;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class JwtService implements JwtServiceInterface
{
    private ?string $token  = null;
    private ?array $decoded = null;

    public function __construct(
        private ?UserInterface $user,
        private ?HttpRequestInterface $request,
        private ?SessionInterface $session
    ) {
    }

    /**
     * Generate jwt
     *
     * @return string
     */
    public function getToken(): string
    {
        if ($this->token === null) {
            $this->token = $this->session?->get('USER.JWT') ?? $this->request?->getPost(self::TOKEN_NAME) ?? null;
            $this->token ??= JWT::encode(
                [
                    self::USERID_KEY     => $this->user?->getIduser() ?? -1,
                    self::USER_EMAIL_KEY => $this->user?->getEmail() ?? -1,
                    self::USER_PERM_KEY  => $this->user?->getPermissions()->__toString() ?? null,
                    'exp'                => time() + 60
                ],
                Conf::find('SECURITY.JSONRPC.KEY'),
                Conf::find('SECURITY.JSONRPC.METHOD')
            );
        }

        $this->session?->set('USER.JWT', $this->token);

        return $this->token;
    }

    public function __destruct()
    {
        $this->session?->set('USER.JWT', $this->token);
    }


    /**
     * Get the value of decodedJwt
     *
     * @PhpUnitGen\get("decodedJwt")
     *
     * @return array
     */
    public function getDecodedToken(): array
    {
        return $this->decoded ??= JWT::decode(
            $this->getToken(),
            new Key(
                Conf::find('SECURITY.JSONRPC.KEY'),
                Conf::find('SECURITY.JSONRPC.METHOD')
            )
        );
    }

    /**
     * Set the value of user
     *
     * @param UserInterface|null $user
     *
     * @PhpUnitGen\set("user")
     *
     * @return self
     */
    public function setUser(?UserInterface $user)
    {
        $this->user = $user;

        return $this;
    }
}
