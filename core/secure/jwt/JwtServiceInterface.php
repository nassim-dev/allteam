<?php

namespace core\secure\jwt;

use core\secure\user\UserInterface;

/**
 * Undocumented interface
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
interface JwtServiceInterface
{
    public const TOKEN_NAME     = '_jwt';
    public const USERID_KEY     = 'user_id';
    public const USER_EMAIL_KEY = 'user_email';
    public const USER_PERM_KEY  = 'user_perm';

    public function getToken(): string;
    public function getDecodedToken(): array;

    /**
     * Set the value of user
     *
     * @param UserInterface|null $user
     *
     * @PhpUnitGen\set("user")
     *
     * @return self
     */
    public function setUser(?UserInterface $user);
}
