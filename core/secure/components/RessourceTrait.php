<?php

namespace core\secure\components;

use core\secure\authentification\AuthProvider;
use core\secure\permissions\PermissionSet;
use core\secure\permissions\PermissionSetInterface;

/**
 * Trait RessourceTrait
 *
 * Provides implementation for RessourceInterface
 *
 * @category  provide implementation for RessourceInterface
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
trait RessourceTrait
{
    use AuthProvider;

    /**
     * @var PermissionSetInterface
     */
    private $__permissionSet;

    /**
     * Get PermissionSet
     *
     * @return PermissionSet
     */
    public function getPermissionSet(): PermissionSetInterface
    {
        return $this->__permissionSet ??= new PermissionSet();
    }
}
