<?php

namespace core\secure\components;

use core\secure\permissions\PermissionSetInterface;
use core\utils\Utils;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class DefaultRessource implements RessourceInterface
{
    use RessourceTrait;

    public function __construct(private string $name)
    {
    }

    public function getHash(): string
    {
        return Utils::hash($this->name);
    }

    public function getOwner(): ?int
    {
        return null;
    }

    public static function buildDefaultPermissions(PermissionSetInterface $permissionSet): PermissionSetInterface
    {
        return $permissionSet;
    }
}
