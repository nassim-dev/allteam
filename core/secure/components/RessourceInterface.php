<?php

namespace core\secure\components;

use core\secure\permissions\PermissionSet;
use core\secure\permissions\PermissionSetInterface;

/**
 * Interface RessourceInterface
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
interface RessourceInterface
{
    /**
     * Return hash to distinguish ressource of same class (ex: id)
     */
    public function getHash(): string;

    /**
     * Get PermissionSet
     */
    public function getPermissionSet(): PermissionSetInterface;

    /**
     * Return owner id
     */
    public function getOwner(): ?int;

    /**
     * Default permissions for class
     */
    public static function buildDefaultPermissions(PermissionSetInterface $permissionSet): PermissionSetInterface;
}
