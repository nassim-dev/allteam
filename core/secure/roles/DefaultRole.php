<?php

namespace core\secure\roles;

use core\secure\permissions\DefaultPermission;
use core\secure\permissions\PermissionSet;

/**
 * Class DefaultRole
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class DefaultRole extends AbstractRole
{
    public function __construct(private string $name, private ?string $context = null)
    {
        /** You could define here a set of permissions */
        /* $this->getPermissions()->attach(new PermissionSet(
             [
                 new DefaultPermission("name", 'context')
             ],
             [
                 new DefaultPermission("name", 'context')
             ]
         ));*/
    }


    /**
     * Return ressource:id or null if no context
     */
    public function getContext(): ?string
    {
        return $this->context;
    }

    /**
     * Return role name
     */
    public function getName(): string
    {
        return $this->name;
    }
}
