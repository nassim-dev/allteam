<?php

namespace core\secure\roles;

use core\secure\EntityLinker;

/**
 * Class AbstractRole
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
abstract class AbstractRole extends EntityLinker implements RoleInterface
{
    /**
     * Related UserInterface objects
     */
    private ?\core\secure\roles\RoleContainer $members = null;

    /**
     * Related PermissionInterface objects
     */
    private ?\core\secure\roles\RoleContainer $permissions = null;

    /**
     * @return void
     */
    final public function extendsFrom(RoleInterface $role)
    {
        $this->getMembers()->addAll($role->getMembers());
        $this->getPermissions()->addAll($role->getPermissions());
    }

    /**
     * Get members
     *
     * @PhpUnitGen\get("members")
     */
    final public function getMembers(): RoleContainer
    {
        return $this->members ??= new RoleContainer();
    }

    /**
     * Get permissions
     *
     * @PhpUnitGen\get("permissions")
     */
    final public function getPermissions(): RoleContainer
    {
        return $this->permissions ??= new RoleContainer();
    }
}
