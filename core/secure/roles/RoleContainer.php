<?php

namespace core\secure\roles;

use core\containers\ContainerInterface;
use core\containers\SplContainer;

/**
 * Class RoleContainer
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class RoleContainer extends SplContainer implements ContainerInterface
{
    /**
     * @param mixed $fieldId
     */
    public function findObject($fieldId): ?RoleInterface
    {
        return parent::findObject($fieldId);
    }

    /**
     * @param RoleInterface $object
     *
     * @return mixed
     */
    protected function getPropertie($object, string $propertieName)
    {
        if (property_exists($object, $propertieName)) {
            return $object->{$propertieName};
        }

        if (method_exists($object, $propertieName)) {
            return call_user_func_array([$object, $propertieName], []);
        }

        $methodName = 'get' . ucfirst($propertieName);
        if (method_exists($object, $methodName)) {
            return call_user_func_array([$object, $methodName], []);
        }

        return null;
    }

    /**
     * Find an element if callable return true
     *
     * @param string $function    (isDifferent , isSup, isEqual, isInf)
     * @param mixed  $testedValue
     */
    public function findIf(string $propertieName, string $function, $testedValue): ?RoleInterface
    {
        return parent::findIf($propertieName, $function, $testedValue);
    }
}
