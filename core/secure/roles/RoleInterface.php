<?php

namespace core\secure\roles;

use core\entities\EntitieInterface;
use core\secure\permissions\PermissionInterface;

/**
 * Interface RoleInterface
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
interface RoleInterface
{
    public function getEntity(): ?EntitieInterface;

    public function setEntity(?EntitieInterface $entityInterface);

    /**
     * @return void
     */
    public function extendsFrom(RoleInterface $role);

    /**
     * Return role name
     */
    public function getName(): string;

    /**
     * Get members
     *
     * @PhpUnitGen\get("members")
     */
    public function getMembers(): RoleContainer;

    /**
     * Return related permission
     *
     * @return PermissionInterface[]
     */
    public function getPermissions();

    /**
     * Return ressource:id or null if no context
     */
    public function getContext(): ?string;
}
