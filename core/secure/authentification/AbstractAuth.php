<?php

namespace core\secure\authentification;

use core\DI\DiProvider;
use core\secure\authentification\components\AuthComponentInterface;
use core\secure\context\ContextInterface;
use core\secure\permissions\PermissionServiceInterface;
use core\secure\user\UserInterface;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
abstract class AbstractAuth implements AuthServiceInterface
{
    use DiProvider;

    protected bool $allowed = true;

    /**
     * Controller mode
     */
    protected bool $public = false;

    /**
     * Logged user
     */
    protected ?UserInterface $user = null;

    /**
     * Actual context
     */
    protected ?ContextInterface $context = null;

    /**
     * Permission manager
     *
     * @var PermissionServiceInterface
     */
    protected PermissionServiceInterface $permissionManager;

    /**
     * Auth component
     *
     * @var AuthComponentInterface
     */
    protected AuthComponentInterface $auth;

    /**
     * Get the value of allowed
     *
     * @PhpUnitGen\get("allowed")
     */
    public function isAllowed(): bool
    {
        return $this->allowed;
    }

    /**
     * True if public controller
     *
     * @return boolean
     */
    public function isPublic(): bool
    {
        return $this->public;
    }

    /**
     * Define public context for controller
     *
     * @param boolean $public
     *
     * @return void
     */
    public function setControllerMode(bool $public)
    {
        $this->public = $public;
    }

    /**
     * Get logged user
     *
     * @PhpUnitGen\get("user")
     */
    public function getUser(): ?UserInterface
    {
        return $this->user;
    }

    /**
     * Get actual context
     *
     * @PhpUnitGen\get("context")
     */
    public function getContext(): ?ContextInterface
    {
        return $this->context;
    }

    /**
     * Get the value of permissionManager
     *
     * @PhpUnitGen\get("permissionManager")
     */
    public function getPermissionManager(): PermissionServiceInterface
    {
        return $this->permissionManager;
    }

    /**
     * Get the value of auth
     *
     * @PhpUnitGen\get("auth")
     */
    public function getAuth(): AuthComponentInterface
    {
        return $this->auth;
    }
}
