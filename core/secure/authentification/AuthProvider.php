<?php

namespace core\secure\authentification;

use core\DI\DiProvider;

/**
 * Trait AuthProvider
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait AuthProvider
{
    use DiProvider;

    /**
     * AuthServiceInterface implémentation
     *
     * @var AuthServiceInterface
     */
    private $auth;

    /**
     * Return auth cache implémetation
     */
    public function getAuth(): AuthServiceInterface
    {
        return $this->auth ??= $this->getDi()->singleton(AuthServiceInterface::class);
    }
}
