<?php

namespace core\secure\authentification;

use core\notifications\NotificationServiceInterface;
use core\secure\authentification\components\AuthComponentInterface;
use core\secure\context\ContextInterface;
use core\secure\permissions\PermissionServiceInterface;
use core\secure\user\UserInterface;

/**
 * Interface AuthServiceInterface
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
interface AuthServiceInterface
{
    public const IP_VALIDITY_TEST   = 'IP_VALIDITY';
    public const SESSION_AGE_TEST   = 'SESSION_AGE';
    public const SERVER_NAME_TEST   = 'SERVER_NAME';
    public const SERVER_ADRESS_TEST = 'SERVER_ADRESS_TEST';
    public const JWT_CHECK          = 'JWT_CHECK';
    public const CSRF_CHECK         = 'CSRF_CHECK';


    public function isPublic(): bool;

    public function setControllerMode(bool $public);

    /**
     * Login user
     *
     * @return void
     */
    public function login(NotificationServiceInterface $notifier);

    /**
     * Logout user
     *
     * @return void
     */
    public function logout(NotificationServiceInterface $notifier);

    /**
     * True if user is connected
     */
    public function isConnected(): bool;

    /**
     * True if client is allowed to acheve a request
     */
    public function isAllowed(): bool;

    /**
     * Allow access for request
     *
     * @return static
     */
    public function allowAccess();

    /**
     * Deny access for request
     *
     * @return static
     */
    public function denyAccess();

    /**
     * Get logged user
     *
     * @PhpUnitGen\get("user")
     */
    public function getUser(): ?UserInterface;

    /**
     * Get actual context
     *
     * @PhpUnitGen\get("context")
     */
    public function getContext(): ?ContextInterface;

    /**
     * Get the value of permissionManager
     *
     * @PhpUnitGen\get("permissionManager")
     */
    public function getPermissionManager(): PermissionServiceInterface;

    /**
     * Get the value of auth
     *
     * @PhpUnitGen\get("auth")
     */
    public function getAuth(): AuthComponentInterface;
}
