<?php

namespace core\secure\authentification;

use core\notifications\NotificationServiceInterface;
use core\secure\authentification\components\AuthComponentInterface;
use core\secure\context\DefaultContext;
use core\secure\permissions\PermissionServiceInterface;

/**
 * Class CommandLineAuthService
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class CommandLineAuthService extends AbstractAuth implements AuthServiceInterface
{
    public function __construct(
        private PermissionServiceInterface $permissionManager,
        private AuthComponentInterface $auth,
    ) {
        $this->user    = $this->auth->buildPublicUser();
        $this->allowed = true;
        $this->context = new DefaultContext();
    }

    /**
     * Login user
     *
     * @return void
     */
    public function login(NotificationServiceInterface $notifier)
    {
    }

    /**
     * Logout user
     *
     * @return void
     */
    public function logout(NotificationServiceInterface $notifier)
    {
    }

    /**
     * True if user is connected
     */
    public function isConnected(): bool
    {
        return true;
    }

    /**
     * Allow access for request
     */
    public function allowAccess(): self
    {
        $this->allowed = true;

        return $this;
    }

    /**
     * Deny access for request
     */
    public function denyAccess(): self
    {
        $this->allowed = false;

        return $this;
    }
}
