<?php

namespace core\secure\authentification;

use core\config\Conf;
use core\DI\DI;
use core\logger\auth\TracyAuthPanel;
use core\messages\HttpUtils;
use core\messages\request\HttpRequestInterface;
use core\messages\response\HttpResponseInterface;
use core\notifications\NotificationServiceInterface;
use core\secure\authentification\components\AuthComponentInterface;
use core\secure\authentification\middleware\CsrfMiddleware;
use core\secure\authentification\middleware\IpMiddleware;
use core\secure\authentification\middleware\JwtMiddleware;
use core\secure\authentification\middleware\ServerAddressMiddleware;
use core\secure\authentification\middleware\ServerNameMiddleware;
use core\secure\authentification\middleware\SessionAgeMiddleware;
use core\secure\context\ContextInterface;
use core\secure\context\DefaultContext;
use core\secure\permissions\PermissionServiceInterface;
use core\session\SessionInterface;
use core\utils\Pipeline;
use Tracy\Debugger;

/**
 * Class AuthService
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class AuthService extends AbstractAuth implements AuthServiceInterface
{
    public function __construct(
        private PermissionServiceInterface $permissionManager,
        private SessionInterface $session,
        private AuthComponentInterface $auth,
        private HttpRequestInterface $request,
        array $middlewares = [
            ServerAddressMiddleware::class,
            ServerNameMiddleware::class,
            IpMiddleware::class,
            SessionAgeMiddleware::class,
            CsrfMiddleware::class,
        ]
    ) {
        $this->user    = $this->auth->check() ?? $this->auth->buildPublicUser();
        $this->context = $this->initializeContext();

        if (Conf::find('ENVIRONNEMENT') === 'development') {
            if (CONTEXT === 'APP' && !isset($_SERVER['x-requested-with'])) {
                Debugger::getBar()->addPanel(new TracyAuthPanel($this));
            }
            DI::constructor()->call($this, 'login');
        }

        $jwt = (new Pipeline($middlewares))
            ->pipe(JwtMiddleware::class, ['user' => $this->user])
            ->execute();

        $this->allowed = match (true) {
            (!is_array($jwt))                                                        => false,
            ($jwt[JwtMiddleware::USERID_KEY] !== $this->session->get('USER.IDUSER')) => false,
            default                                                                  => true
        };

        if ($this->allowed) {
            $this->user->getPermissions()->addAll(
                $this->user->getPermissions()::rebuild(
                    $jwt[JwtMiddleware::USER_PERM_KEY]
                )
            );
        }

        if ($this->isConnected()) {
            $this->user = $this->session->get('USER.ENTITY');
            if ($this->user === null || !$this->isAllowed()) {
                DI::constructor()->call($this, 'logout');
            }
        } else {
            $this->user = $this->auth->check() ?? $this->auth->buildPublicUser();
        }
    }

    public function __destruct()
    {
        $this->auth->persist($this->getUser());
    }

    private function initializeContext(): ContextInterface
    {
        if (!$this->session->has('USER.CONTEXT')
         || $this->user === null
         || !is_object($this->session->get('USER.CONTEXT'))
         || !is_subclass_of($this->session->get('USER.CONTEXT'), ContextInterface::class)) {
            $context = new DefaultContext();
            $this->session->set('USER.CONTEXT', $context);
        }

        return $this->session->get('USER.CONTEXT');
    }

    /**
     * Login user
     *
     * @return void
     */
    public function login(NotificationServiceInterface $notifier)
    {
        if (!$this->session->has('USER.ENTITY')) {
            if ($this->user === null) {
                return;
            }

            $this->session->set('USER.IDUSER', $this->user->getIduser())
                ->set('USER.CONNECTED', true)
                ->set('USER.LAST_ACCESS', time())
                ->set('CONTEXT.SERVER_ADDR', $this->request->getServer('SERVER_ADDR'))
                ->set('CONTEXT.LANGUAGE', $this->user->language)
                ->set('CONTEXT.REMOTE_ADDR', $this->request->getServer('REMOTE_ADDR'))
                ->set('CONTEXT.SERVER_NAME', $this->request->getServer('SERVER_NAME'))
                ->set('CONTEXT.HTTP_USER_AGENT', $this->request->getServer('HTTP_USER_AGENT'));

            $this->auth->hydrate($this->user, $this->permissionManager);

            $this->session->set('USER.ENTITY', $this->user);
            $notifier->notify(_('Login success!'), $notifier::SUCCESS, '/panel');
        }
    }

    /**
     * Logout user
     *
     * @return void
     */
    public function logout(NotificationServiceInterface $notifier)
    {
        $this->session->clear();
        $notifier->notify(_('You are properly offline.'), $notifier::SUCCESS, '/');
    }

    /**
     * True if user is connected
     */
    public function isConnected(): bool
    {
        $status = $this->session->get('USER.CONNECTED') ?? false;
        $this->session->set('USER.CONNECTED', $status);

        return $status;
    }


    /**
     * Allow access for request
     */
    public function allowAccess(): self
    {
        $this->allowed = true;

        /** @var HttpResponseInterface $response */
        $response = $this->getDi()->singleton(HttpResponseInterface::class);
        $response->setCode(200)
            ->setData('error', false)
            ->setData('errorMessage', HttpUtils::HTTP_ERROR(200));

        return $this;
    }

    /**
     * Deny access for request
     */
    public function denyAccess(): self
    {
        $this->allowed = false;

        /** @var HttpResponseInterface $response */
        $response = $this->getDi()->singleton(HttpResponseInterface::class);
        $response->setCode(403)
            ->setData('error', true)
            ->setData('errorMessage', HttpUtils::HTTP_ERROR(403));

        return $this;
    }
}
