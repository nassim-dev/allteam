<?php namespace core\secure\authentification;

/**
 * Class Auth
 *
 *  @method static bool isPublic()
 *  @method static string setControllerMode(bool $public)
 *  @method static string getJsonWebToken()
 *  @method static bool getStatus(string $test)
 *  @method static string login(core\notifications\NotificationServiceInterface $notifier)
 *  @method static string logout(core\notifications\NotificationServiceInterface $notifier)
 *  @method static bool isConnected()
 *  @method static bool isAllowed()
 *  @method static string allowAccess()
 *  @method static string denyAccess()
 *  @method static core\secure\user\UserInterface getUser()
 *  @method static core\secure\context\ContextInterface getContext()
 *  @method static core\secure\permissions\PermissionServiceInterface getPermissionManager()
 *  @method static core\secure\authentification\components\AuthComponentInterface getAuth()
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class Auth extends \core\facade\HelperFacade
{
    protected static ?string $serviceInterface = 'core\secure\authentification\AuthServiceInterface';
}
