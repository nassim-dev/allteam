<?php

namespace core\secure\authentification\components;

use core\config\Conf;
use core\database\mysql\MysqlRepository;
use core\database\RepositoryFactoryInterface;
use core\DI\DiProvider;
use core\entities\EntitieInterface;
use core\messages\request\HttpRequestInterface;
use core\secure\permissions\PermissionServiceInterface;
use core\secure\user\FakeUser;
use core\secure\user\UserInterface;

/**
 * Class MysqlAuthComponent
 *
 * Manage authentification
 *
 * @category  Manage authentification
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class MysqlAuthComponent implements AuthComponentInterface
{
    use DiProvider;

    public function __construct(
        private RepositoryFactoryInterface $repositoryFactory,
        private HttpRequestInterface $request,
        private ?string $email = null,
        private ?string $password = null
    ) {
        if (Conf::find('ENVIRONNEMENT') === 'development'
            && $email === null
            && $password === null) {
            $this->setEmail('admin@allteam.io')
                ->setPassword('allteam');
        }
    }

    /**
     * Check authentification
     */
    public function check(): ?UserInterface
    {
        $this->email ??= $this->request->getPost('email');
        $this->password ??= $this->request->getPost('password');

        $user = $this->repositoryFactory->table('user')
            ->getByParams(['email' => $this->email])
            ->fetch();
        if (null === $user || is_bool($user)) {
            return null;
        }

        if (!password_verify($this->password, $user->password)) {
            return null;
        }

        $user->last_connection = date(MysqlRepository::MYSQL_DATE_FORMAT);
        $this->repositoryFactory->table('user')->update($user);

        return $user;
    }

    /**
     * Initialize permission container
     *
     * @param UserInterface|null         $user
     * @param PermissionServiceInterface $permissionService
     *
     * @return void
     */
    public function hydrate(?UserInterface $user, PermissionServiceInterface $permissionService)
    {
        if ($user === null) {
            return;
        }

        /** @var EntitieInterface|UserInterface $user */
        $permissions = $user->findRelated('permission');

        /** @var EntitieInterface[] $permissions */
        foreach ($permissions as $permission) {
            $newPermission = $permissionService->getPermissionClass($permission->name, $permission->context);
            $newPermission->setEntity($permission);
            $user->addPermission($newPermission);
        }

        $roles = $user->findRelated('role');

        /** @var EntitieInterface $role */
        foreach ($roles as $role) {
            $newRole = $permissionService->getRoleClass($role->name, $role->context);
            $newRole->setEntity($role);
            $user->addRole($newRole);
        }
    }

    /**
     *  Persist user permissions
     *
     * @param UserInterface|null $user
     *
     * @return void
     */
    public function persist(?UserInterface $user)
    {
        if ($user === null) {
            return;
        }

        $rolesToAdd       = [];
        $permissionsToAdd = [];

        /** @var PermissionInterface $permission */
        foreach ($user->getPermissions() as $permission) {
            if ($permission->getEntity() === null) {
                $permissionsToAdd[] = ['iduser' => $user->getIduser(), 'name' => $permission->getName()];
            }
        }

        /** @var RoleInterface $role */
        foreach ($user->getRoles() as $role) {
            if ($role->getEntity() === null) {
                $rolesToAdd[] = ['iduser' => $user->getIduser(), 'name' => $role->getName()];
            }
        }

        if (!empty($permissionsToAdd)) {
            $this->repositoryFactory->table('permission')
                ->addMultiple($permissionsToAdd);
        }

        if (!empty($rolesToAdd)) {
            $this->repositoryFactory->table('role')
                ->addMultiple($rolesToAdd);
        }
    }


    /**
     * Build public default user
     */
    public function buildPublicUser(): UserInterface
    {
        return  $this->getDi()->make(FakeUser::class, [
            'data' => [
                'iduser'     => 1,
                'password'   => '$2y$10$H2x1d/.ZY.17EEwyLhl0gOovVqqqXZKfv4BH1QUAKk6BPpJOwLI7y', // password_hash('allteam', PASSWORD_BCRYPT),
                'name'       => null,
                'first_name' => null,
                'email'      => '@public'
            ]
        ]);
    }


    /**
     * Set email
     */
    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Set password
     */
    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }
}
