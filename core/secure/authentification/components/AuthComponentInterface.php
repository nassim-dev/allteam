<?php

namespace core\secure\authentification\components;

use core\secure\permissions\PermissionServiceInterface;
use core\secure\user\UserInterface;

/**
 * Interface AuthComponentInterface
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
interface AuthComponentInterface
{
    /**
     * Check authentification
     */
    public function check(): ?UserInterface;

    /**
     * Set email
     *
     * @return self
     */
    public function setEmail(?string $email);

    /**
     * Set password
     *
     * @return self
     */
    public function setPassword(?string $password);

    /**
     * Build public default user
     */
    public function buildPublicUser(): UserInterface;

    /**
     * Initialize permission container
     *
     * @param UserInterface|null         $user
     * @param PermissionServiceInterface $permissionService
     *
     * @return void
     */
    public function hydrate(?UserInterface $user, PermissionServiceInterface $permissionService);

    /**
     *  Persist user permissions
     *
     * @param UserInterface|null $user
     *
     * @return void
     */
    public function persist(?UserInterface $user);
}
