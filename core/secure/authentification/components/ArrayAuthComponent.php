<?php

namespace core\secure\authentification\components;

use core\config\Conf;
use core\DI\DiProvider;
use core\messages\request\HttpRequestInterface;
use core\secure\permissions\AdminPermission;
use core\secure\permissions\PermissionServiceInterface;
use core\secure\user\FakeUser;
use core\secure\user\UserInterface;

/**
 * Class ArrayAuthComponent
 *
 * Manage authentification
 *
 * @category  Manage authentification
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class ArrayAuthComponent implements AuthComponentInterface
{
    use DiProvider;

    private array $credentials = [
        'admin@allteam.io' => [
            'iduser'     => 1,
            'password'   => '$2y$10$hMp0WSX.Tq3wtlp3dXRGQuJV9qHja9I5NNCEW2BwuBo5NutNpHjMq', // password_hash('allteam', PASSWORD_BCRYPT),
            'name'       => 'Ourami',
            'first_name' => 'Nassim',
            'email'      => 'admin@allteam.io'
        ]
    ];

    public function __construct(
        private HttpRequestInterface $request,
        private ?string $email = null,
        private ?string $password = null
    ) {
        if (Conf::find('ENVIRONNEMENT') === 'development'
            && $email === null
            && $password === null) {
            $this->setEmail('admin@allteam.io')
                ->setPassword('allteam');
        }
    }

    /**
     * Build public default user
     */
    public function buildPublicUser(): UserInterface
    {
        $user = $this->getDi()->make(FakeUser::class, [
            'data' => [
                'iduser'     => -1,
                'password'   => '$2y$10$H2x1d/.ZY.17EEwyLhl0gOovVqqqXZKfv4BH1QUAKk6BPpJOwLI7y', // password_hash('allteam', PASSWORD_BCRYPT),
                'name'       => null,
                'first_name' => null,
                'email'      => '@public'
            ]
        ]);

        //$user->addPermission($this->permissionService->getPermissionClass('read:panel'));

        return $user;
    }

    /**
     * Check authentification
     */
    public function check(): ?UserInterface
    {
        $this->email ??= $this->request->getPost('email');
        $this->password ??= $this->request->getPost('password');

        if (!isset($this->credentials[$this->email])
        || !password_verify($this->password, $this->credentials[$this->email]['password'])) {
            return null;
        }

        return $this->getDi()->make(FakeUser::class, ['data' => $this->credentials[$this->email]]);
    }


    /**
     * Initialize permission container
     *
     * @param UserInterface|null         $user
     * @param PermissionServiceInterface $permissionService
     *
     * @return void
     */
    public function hydrate(?UserInterface $user, PermissionServiceInterface $permissionService)
    {
        if ($user === null) {
            return;
        }

        //$user->addPermission($permissionService->getPermissionClass('read:panel'));
        //$user->addPermission($permissionService->getPermissionClass('read:user'));
        //$user->addPermission($permissionService->getPermissionClass('read:type'));
        //$user->addPermission($permissionService->getPermissionClass('create:type'));
        //$user->addPermission($permissionService->getPermissionClass('update:type'));
        //$user->addPermission($permissionService->getPermissionClass('create:todo'));
        //$user->addPermission($permissionService->getPermissionClass('create:address'));
        //$user->addPermission($permissionService->getPermissionClass('read:band'));
        //$user->addPermission($permissionService->getPermissionClass('read:compagny'));
        //$user->addPermission($permissionService->getPermissionClass('create:compagny'));
        $user->addPermission($permissionService->getPermissionClass(AdminPermission::class));
    }

    /**
     *  Persist user permissions
     *
     * @param UserInterface|null $user
     *
     * @return void
     */
    public function persist(?UserInterface $user)
    {
        return;
    }

    /**
     * Set the value of credentials
     *
     * @PhpUnitGen\set("credentials")
     * @param mixed[] $credentials
     */
    public function setCredentials(array $credentials): self
    {
        $this->credentials = $credentials;

        return $this;
    }

    /**
     * Set email
     */
    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Set password
     */
    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }
}
