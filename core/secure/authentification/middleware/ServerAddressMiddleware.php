<?php

namespace core\secure\authentification\middleware;

use core\messages\request\HttpRequestInterface;
use core\session\SessionInterface;
use core\utils\AbstractPipeline;
use core\utils\PipelineInterface;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class ServerNameMiddleware extends AbstractPipeline implements PipelineInterface
{
    public function __construct(
        private HttpRequestInterface $request,
        private SessionInterface $session
    ) {
    }



    public function execute(...$parameters)
    {
        $return = ($this->session->get('CONTEXT.SERVER_NAME') === $this->request->getServer('SERVER_NAME'));

        if (!$return) {
            $this->getParent()?->stop();
        }

        $this->result = $return;
    }
}
