<?php

namespace core\secure\authentification\middleware;

use core\config\Conf;
use core\session\SessionInterface;
use core\utils\AbstractPipeline;
use core\utils\PipelineInterface;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class SessionAgeMiddleware extends AbstractPipeline implements PipelineInterface
{
    public function __construct(
        private SessionInterface $session
    ) {
    }



    public function execute(...$parameters)
    {
        $return = ((time() - ($this->session->get('USER.LAST_ACCESS') ?? 0)) < Conf::find('PHP.SESSION_TIMEOUT'));

        if (!$return) {
            $this->getParent()?->stop();
        }

        $this->result = $return;
    }
}
