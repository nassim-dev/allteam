<?php

namespace core\secure\authentification\middleware;

use core\secure\jwt\JwtServiceInterface;
use core\secure\user\UserInterface;
use core\utils\AbstractPipeline;
use core\utils\PipelineInterface;
use LogicException;
use UnexpectedValueException;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class JwtMiddleware extends AbstractPipeline implements PipelineInterface
{
    public const TOKEN_NAME     = JwtServiceInterface::TOKEN_NAME;
    public const USERID_KEY     = JwtServiceInterface::USERID_KEY;
    public const USER_EMAIL_KEY = JwtServiceInterface::USER_EMAIL_KEY;
    public const USER_PERM_KEY  = JwtServiceInterface::USER_PERM_KEY;

    public function __construct(
        private JwtServiceInterface $jwt,
        private ?UserInterface $user
    ) {
    }


    public function execute(...$parameters)
    {
        try {
            $this->jwt->setUser($this->user);
            $result = $this->jwt->getDecodedToken();
        } catch (LogicException $e) {
            $this->getParent()?->stop();
        } catch (UnexpectedValueException $e
        ) {
            $this->getParent()?->stop();
        } finally {
            $this->result = $result ?? false;
        }
    }
}
