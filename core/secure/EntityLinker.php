<?php

namespace core\secure;

use core\entities\EntitieInterface;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
abstract class EntityLinker
{
    /**
     * Assocoated Entitie
     *
     * @var EntitieInterface|null
     */
    private ?EntitieInterface $entity = null;

    public function getEntity(): ?EntitieInterface
    {
        return $this->entity;
    }

    public function setEntity(?EntitieInterface $entityInterface)
    {
        $this->entity = $entityInterface;
    }
}
