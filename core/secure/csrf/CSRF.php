<?php

namespace core\secure\csrf;

use core\messages\request\HttpRequestInterface;
use core\session\SessionInterface;
use core\utils\Utils;

/**
 * Class CSRF
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class CSRF implements CsrfServiceInterface
{
    private array $_generated = [];

    public function __construct(
        protected HttpRequestInterface $request,
        protected SessionInterface $session
    ) {
    }

    /**
     * Constant-time string comparison.  This comparison function is timing-attack safe
     */
    public function compare(string $hasha, string $hashb): bool
    {
        $hashesAreNotEqual = strlen($hasha) ^ strlen($hashb);

        // compare the shortest of the two strings
        $length = min(strlen($hasha), strlen($hashb));
        $hasha  = substr($hasha, 0, $length);
        $hashb  = substr($hashb, 0, $length);

        // iterate through the hashes comparing them character by character
        // if a character does not match, then return true, so the hashes are not equal
        for ($i = 0; $i < strlen($hasha); ++$i) {
            $hashesAreNotEqual += (ord($hasha[$i]) !== ord($hashb[$i]));
        }

        // if not hashes are not equal, then hashes are equal
        return !$hashesAreNotEqual;
    }

    /**
     * Generate a Token
     */
    public function generateToken(): string
    {
        // generate a random token
        $tokenName        = $this->getPage();
        $sessionContainer = $this->session->getContainer(self::SESSION_CONTAINER);

        if (!isset($this->_generated[$tokenName])) {
            $salt = $this->request->getServer('REMOTE_ADDR') ?? uniqid();
            $sessionContainer->set($tokenName, sha1(uniqid(sha1($salt), true)));
            $this->_generated[$tokenName] = true;
        }

        return $sessionContainer[$tokenName];
    }

    /**
     * Return token as aray
     * @return array{name: string, value: string}
     */
    public function getArray(): array
    {
        return [
            'name'  => $this->getPage(),
            'value' => $this->getToken()
        ];
    }

    /**
     * Get a hidden input string with the token/token name in it.
     *
     * @param string $tokenName - defaults to the default token name
     */
    public function getHiddenInputString(): string
    {
        return '<input id="CSRF" type="hidden" name="' . $this->getPage() . '" value="' . $this->getToken() . '"/>';
    }

    /**
     * Get id Token (from url)
     */
    public function getPage(bool $encrypted = true): string
    {
        switch (CONTEXT) {
            case 'API':
                $uri = explode('/', $this->request->headers['HTTP_REFERER']);
                unset($uri[0], $uri[1], $uri[2]);

                $return = self::TOKEN_NAME . Utils::encrypt('/' . implode('/', $uri));
                if (!$encrypted) {
                    return '/' . implode('/', $uri);
                }

                break;
            case 'APP':
                if (!$encrypted) {
                    return $this->request->getUrl();
                }

                $return = self::TOKEN_NAME . Utils::encrypt($this->request->getUrl());

                break;
            case 'CRON':
                $return = self::TOKEN_NAME;

                break;
            default:
                $uri = explode('/', $this->request->getServer('HTTP_REFERER'));
                unset($uri[0], $uri[1], $uri[2]);
                $return = self::TOKEN_NAME . Utils::encrypt('/' . implode('/', $uri));

                if (!$encrypted) {
                    return '/' . implode('/', $uri);
                }

                break;
        }

        return preg_replace('/[^A-Za-z0-9_]/', '', $return);
    }

    /**
     * Get a query string mark-up with the token/token name in it.
     *
     * @param string $tokenName - defaults to the default token name
     */
    public function getQueryString(): string
    {
        $tokenName = $this->getPage();

        return sprintf('%s=%s', $tokenName, $this->getToken());
    }

    /**
     * Get the token.  If it's not defined, this will go ahead and generate one.
     *
     * @param string $tokenName - defaults to the default token name
     */
    public function getToken(): string
    {
        $tokenName        = $this->getPage();
        $sessionContainer = $this->session->getContainer(self::SESSION_CONTAINER);

        return $sessionContainer[$tokenName] ?? null;
    }

    /**
     * Get an array with the token (useful for form libraries, etc.)
     *
     * @param string $tokenName
     */
    public function getTokenAsArray(): array
    {
        $tokenName = $this->getPage();

        return [
            $tokenName => $this->getToken(),
            'name'     => $tokenName,
            'value'    => $this->getToken()
        ];
    }

    /**
     * Validate the token.  If there's not one yet, it will return false.
     *
     * @param array $requestData - your whole POST/GET array - will index in with the token name to get the token.
     */
    public function validate(array $requestData = []): bool
    {
        $tokenName        = $this->getPage();
        $sessionContainer = $this->session->getContainer(self::SESSION_CONTAINER);

        if (!isset($sessionContainer[$tokenName]) || !isset($requestData[$tokenName])) {
            return false;
        }

        if ($this->compare($requestData[$tokenName], $this->getToken())) {
            return true;
        }

        //unset($sessionContainer[$tokenName]);

        return false;
    }
}
