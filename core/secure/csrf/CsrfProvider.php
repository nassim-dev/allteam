<?php

namespace core\secure\csrf;

use core\DI\DiProvider;

/**
 * Trait CsrfProvider
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait CsrfProvider
{
    use DiProvider;

    /**
     * CsrfServiceInterface implémentation
     *
     * @var CsrfServiceInterface
     */
    private $csrf;

    /**
     * Return CsrfServiceInterface implémetation
     */
    public function getCsrf(): CsrfServiceInterface
    {
        return $this->csrf ??= $this->getDi()->singleton(CsrfServiceInterface::class);
    }
}
