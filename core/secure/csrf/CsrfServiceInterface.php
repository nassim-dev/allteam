<?php

namespace core\secure\csrf;

/**
 * Interface CsrfServiceInterface
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
interface CsrfServiceInterface
{
    /**
     * The default token name
     */
    public const SESSION_CONTAINER = 'CSRF';
    public const TOKEN_NAME        = 'CSRF';
    public const CSRF_ENABLED      = true;
    public const CSRF_DISABLED     = false;

    /**
     * Constant-time string comparison.  This comparison function is timing-attack safe
     *
     * @return bool
     */
    public function compare(string $hasha, string $hashb);

    /**
     * Generate a Token
     */
    public function generateToken(): string;

    /**
     * Return token as aray
     */
    public function getArray(): array;

    /**
     * Get a hidden input string with the token/token name in it.
     *
     * @param string $tokenName - defaults to the default token name
     */
    public function getHiddenInputString(): string;

    /**
     * Get id Token (from url)
     */
    public function getPage(bool $encrypted = true): string;

    /**
     * Get a query string mark-up with the token/token name in it.
     *
     * @param string $tokenName - defaults to the default token name
     */
    public function getQueryString(): string;

    /**
     * Get the token.  If it's not defined, this will go ahead and generate one.
     *
     * @param string $tokenName - defaults to the default token name
     */
    public function getToken(): string;

    /**
     * Get an array with the token (useful for form libraries, etc.)
     *
     * @param string $tokenName
     */
    public function getTokenAsArray(): array;

    /**
     * Validate the token.  If there's not one yet, it will return false.
     *
     * @param array  $requestData - your whole POST/GET array - will index in with the token name to get the token.
     * @param string $tokenName   - defaults to the default token name
     */
    public function validate(array $requestData = []): bool;
}
