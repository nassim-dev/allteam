<?php

namespace core\secure\user;

use core\secure\context\ContextInterface;
use core\secure\permissions\PermissionContainer;
use core\secure\permissions\PermissionInterface;
use core\secure\roles\RoleContainer;
use core\secure\roles\RoleInterface;

/**
 * Interface UserInterface
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
interface UserInterface
{
    /**
     * Add role to user
     */
    public function addRole(RoleInterface $role): self;

    /**
     * Add permission to user
     */
    public function addPermission(PermissionInterface $permission): self;

    /**
     * Remove permission to user
     */
    public function removePermission(PermissionInterface $permission): self;

    /**
     * Return user id
     */
    public function getIduser(): int;

    /**
     * Get the value of email
     *
     * @PhpUnitGen\get("email")
     */
    public function getEmail(): string;

    /**
     * Get the value of password
     *
     * @PhpUnitGen\get("password")
     */
    public function getPassword(): string;

    /**
     * Return user permissions
     */
    public function getPermissions(): PermissionContainer;

    /**
     * Return user roles
     *
     * @return PermissionContainer
     */
    public function getRoles(): RoleContainer;

    /**
     * Remove role to user
     *
     * @return void
     */
    public function removeRole(RoleInterface $role): self;

    /**
     * Check if user is administrator for specific context
     */
    public function isAdministrator(?ContextInterface $context = null): bool;
}
