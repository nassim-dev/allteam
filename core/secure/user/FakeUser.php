<?php

namespace core\secure\user;

use core\DI\DiProvider;
use core\secure\context\ContextInterface;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class FakeUser implements UserInterface
{
    use UserTrait;
    use DiProvider;

    public string $language = 'fr';


    /**
     * Constructor
     */
    public function __construct(array $data)
    {
        $this->hydrate($data);
    }

    /**
     * Check if user is admin
     */
    public function isAdministrator(?ContextInterface $context = null): bool
    {
        return true;
    }


    /**
     * Undocumented function
     *
     * @param string $column
     * @param mixed  $value
     *
     * @return void
     */
    public function __set($column, $value)
    {
        $this->{$column} = $value;
    }

    /**
     * Hydrate object with array
     */
    final public function hydrate(array $data): self
    {
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }

        return $this;
    }


    public function getIduser(): int
    {
        return $this->iduser;
    }

    /**
     * Get the value of email
     *
     * @PhpUnitGen\get("email")
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * Get the value of password
     *
     * @PhpUnitGen\get("password")
     */
    public function getPassword(): string
    {
        return $this->password;
    }
}
