<?php

namespace core\secure\user;

use core\DI\DI;
use core\secure\permissions\PermissionContainer;
use core\secure\permissions\PermissionInterface;
use core\secure\permissions\PermissionServiceInterface;
use core\secure\roles\RoleContainer;
use core\secure\roles\RoleInterface;

/**
 * Trait UserTrait
 *
 *  Provides implementation for UserInterface
 *
 * @category  Provides implementation for UserInterface
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
trait UserTrait
{
    /**
     * Roles
     *
     * @var RoleContainer
     */
    private $__roles;

    /**
     * Contains Permission
     *
     * @var PermissionContainer
     */
    private $__permissions;

    /**
     * Add role to role
     */
    public function addRole(RoleInterface $role): static
    {
        $this->getRoles()->attach($role, $role->getName());
        $role->getMembers()->attach($this, $this->getIduser());
        foreach ($role->getPermissions() as $permission) {
            $this->getPermissions()->attach($permission, $permission->getHash());
        }

        return $this;
    }

    /**
     * Reurn user roles
     */
    public function getRoles(): RoleContainer
    {
        return $this->__roles ??= new RoleContainer();
    }

    /**
     * Reurn user roles
     */
    public function getPermissions(): PermissionContainer
    {
        return $this->__permissions ??= new PermissionContainer();
    }

    /**
     * Remove role to role
     */
    public function removeRole(RoleInterface $role): static
    {
        if ($this->getRoles()->containsIdentifier($role->getName())) {
            $this->getRoles()->detachWithIdentifier($role->getName());
        }

        $role->getMembers()->detachWithIdentifier($this->getIduser());
        if ($role->getEntity() !== null) {
            $role->getEntity()->delete();
        }

        foreach ($role->getPermissions() as $permission) {
            $this->getPermissions()->detachWithIdentifier($permission->getHash());
        }

        return $this;
    }

    /**
     * Remove permission to role
     */
    public function removePermission(PermissionInterface $permission): static
    {
        if ($this->getPermissions()->containsIdentifier($permission->getHash())) {
            $this->getPermissions()->detachWithIdentifier($permission->getHash());
            if ($permission->getEntity() !== null) {
                $permission->getEntity()->delete();
            }
        }

        return $this;
    }

    /**
     * Add permission to user
     */
    public function addPermission(string | PermissionInterface $permission): self
    {
        if (is_string($permission)) {
            /** @var PermissionServiceInterface $permissionService */
            $permissionService = DI::singleton(PermissionServiceInterface::class);
            $permission        = $permissionService->getPermissionClass($permission);
        }

        $this->getPermissions()->attach($permission, $permission->getHash());



        return $this;
    }


    /**
     * @return array{data: mixed, roles: mixed, permissions: mixed}
     */
    private function serialize()
    {
        return [
            'data'        => $this->data,
            'roles'       => $this->__roles,
            'permissions' => $this->__permissions
        ];
    }

    private function unserialize(array $data)
    {
        $this->__permissions = $data['permissions'];
        $this->__roles       = $data['roles'];
        $this->data          = $data['data'];
    }
}
