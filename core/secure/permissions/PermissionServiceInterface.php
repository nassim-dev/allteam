<?php

namespace core\secure\permissions;

use core\secure\components\RessourceInterface;
use core\secure\roles\RoleInterface;
use core\secure\user\UserInterface;

/**
 * Interface PermissionServiceInterface
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
interface PermissionServiceInterface
{
    /**
     * Check if user has rights for ressource
     */
    public function isAllowed(null | string | RessourceInterface | PermissionInterface $ressource, ?UserInterface $user = null): bool;

    /**
     * Allow permission
     *
     * @return void
     */
    public function allow(PermissionInterface $permission, UserInterface $user);

    /**
     * Deny permission
     *
     * @return void
     */
    public function deny(PermissionInterface $permission, UserInterface $user);

    /**
     * Return Permission object from string
     *
     * @return PermissionInterface
     */
    public function getPermissionClass(string $permissionName, ?string $context = null): PermissionInterface;


    /**
     * Return Role object from string
     *
     * @return RoleInterface
     */
    public function getRoleClass(string $roleName, ?string $context = null): RoleInterface;
}
