<?php

namespace core\secure\permissions;

use core\secure\components\RessourceInterface;
use core\secure\EntityLinker;
use core\secure\user\UserInterface;
use core\utils\Utils;

/**
 * Class DefaultPermission
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class DefaultPermission extends EntityLinker implements PermissionInterface
{
    /**
     * @param string|null $name    permission name
     * @param string|null $context context string format : ressource:iduser
     */
    public function __construct(protected ?string $name = null, protected ?string $context = null)
    {
    }

    public function __sleep()
    {
        return ['name', 'context'];
    }

    /**
     * Custom function to execute to check permission
     */
    public function process(UserInterface $user, RessourceInterface $ressource): bool
    {
        /** @var PermissionInterface $permission */
        foreach ($user->getPermissions() as $permission) {
            if ($permission->getName() === $this->getName()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Return permission name
     */
    public function getName(): string
    {
        $context = ($this->getContext() !== null) ? 'IN ' . $this->context : null;

        return 'DefaultPermission(' . $this->name . $context . ')';
    }

    /**
     * Return hash
     */
    final public function getHash(): string
    {
        return Utils::hash($this->getName());
    }

    /**
     * Return ressource:id or null if no context
     */
    public function getContext(): ?string
    {
        return $this->context;
    }

    /**
     * Set the value of name
     *
     * @PhpUnitGen\set("name")
     * @return self
     */
    public function setName(?string $name)
    {
        $this->name = $name;

        return $this;
    }
}
