<?php

namespace core\secure\permissions;

use core\secure\components\RessourceInterface;
use core\secure\EntityLinker;
use core\secure\user\UserInterface;
use core\utils\Utils;

/**
 * Class AdminPermission
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class AdminPermission extends EntityLinker implements PermissionInterface
{
    /**
     * @param string|null $context context string format : ressource:iduser
     */
    public function __construct(private ?string $context = null)
    {
    }

    /**
     * Custom function to execute to check permission
     */
    public function process(UserInterface $user, RessourceInterface $ressource): bool
    {
        return $user->getPermissions()->containsIdentifier($this->getHash());
    }

    public function __sleep()
    {
        return ['context'];
    }


    /**
     * Return permission name
     */
    public function getName(): string
    {
        return 'AdminPermission(' . $this->context . ')';
    }

    /**
     * Return hash
     */
    final public function getHash(): string
    {
        return Utils::hash($this->getContext()) ?? '';
    }

    /**
     * Return ressource:id or null if no context
     */
    public function getContext(): ?string
    {
        return $this->context;
    }
}
