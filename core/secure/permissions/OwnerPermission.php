<?php

namespace core\secure\permissions;

use core\secure\components\RessourceInterface;
use core\secure\EntityLinker;
use core\secure\user\UserInterface;
use core\utils\Utils;

/**
 * Class OwnerPermission
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class OwnerPermission extends EntityLinker implements PermissionInterface
{
    /**
     * @param string      $name    permission name
     * @param string|null $context context string format : ressource:iduser
     */
    public function __construct(private string $name, private ?string $context = null)
    {
    }

    public function __sleep()
    {
        return ['name', 'context'];
    }

    /**
     * Custom function to execute to check permission
     */
    public function process(UserInterface $user, RessourceInterface $ressource): bool
    {
        if ($ressource->getOwner() === null) {
            return true;
        }

        return ($user->getIduser() === $ressource->getOwner());
    }


    /**
     * Return permission name
     */
    public function getName(): string
    {
        $context = ($this->getContext() !== null) ? 'IN ' . $this->context : null;

        return 'OwnerPermission(' . $this->name . $context . ')';
    }

    /**
     * Return hash
     */
    final public function getHash(): string
    {
        return Utils::hash($this->getName());
    }

    /**
     * Return ressource:id or null if no context
     */
    public function getContext(): ?string
    {
        return $this->context;
    }
}
