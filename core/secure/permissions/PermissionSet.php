<?php

namespace core\secure\permissions;

use core\secure\components\RessourceInterface;
use core\secure\EntityLinker;
use core\secure\SecurityException;
use core\secure\user\UserInterface;
use core\utils\Utils;

/**
 * Class PermissionSet
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class PermissionSet extends EntityLinker implements PermissionSetInterface
{
    /**
     * Required permission
     *
     * @var PermissionInterface[]
     */
    private array $required = [];

    /**
     * High level permission
     *
     * @var PermissionInterface[]
     */
    private array $priority = [];

    /**
     * Undocumented function
     *
     * @param PermissionInterface[]|null $requiredPermissions
     * @param PermissionInterface[]|null $priorityPermissions
     */
    public function __construct(?array $requiredPermissions = null, ?array $priorityPermissions = null)
    {
        if (is_array($requiredPermissions)) {
            foreach ($requiredPermissions as $permission) {
                $this->addRequiredCondition($permission);
            }
        }


        if (is_array($priorityPermissions)) {
            foreach ($priorityPermissions as $permission) {
                $this->addPriorityPermission($permission);
            }
        }

        $this->addPriorityPermission(new AdminPermission());
    }

    public function __sleep()
    {
        return ['required', 'priority'];
    }

    public function isEmpty(): bool
    {
        return (empty($this->required) && empty($this->priority));
    }

    /**
     * Add a required permission
     *
     * @return void
     */
    public function addRequiredCondition(PermissionInterface $permission)
    {
        $this->required[$permission->getName()] = $permission;
    }

    /**
     * Add an high level permission
     *
     * @return void
     */
    public function addPriorityPermission(PermissionInterface $permission)
    {
        $this->priority[$permission->getName()] = $permission;
    }

    /**
     * Custom function to execute to check permission
     */
    public function process(?UserInterface $user, ?RessourceInterface $ressource): bool
    {
        if ($user === null || $ressource === null) {
            return false;
        }

        foreach ($this->priority as $permission) {
            if ($permission->process($user, $ressource)) {
                return true;
            }
        }

        foreach ($this->required as $permission) {
            if (!$permission->process($user, $ressource)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Return permission name
     */
    public function getName(): string
    {
        $priority = (!empty($this->priority)) ? '(' . implode('||', array_keys($this->priority)) . ')' : null;
        $required = (!empty($this->required)) ? ' || (' . implode('&&', array_keys($this->required)) . ')' : null;

        return "PermissionSet:$priority$required";
    }

    /**
     * Return hash
     */
    final public function getHash(): string
    {
        return Utils::hash($this->getName());
    }

    public function getContext(): ?string
    {
        throw new SecurityException('Empty method. You must implements it');
    }
}
