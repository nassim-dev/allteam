<?php

namespace core\secure\permissions;

use core\factory\AbstractFactory;
use core\factory\FactoryInterface;
use core\utils\Utils;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class PermissionFactory extends AbstractFactory implements FactoryInterface
{
    public const ISPUBLIC = '@public';

    public const ISINTERNAL = '@internal';

    /**
     * @param string $identifier
     *
     * @return PermissionInterface
     */
    public function create(string $class, array $params = [])
    {
        $identifier = $class . '_' . Utils::hash($params);

        $instance = $this->getInstance($identifier);
        if (null === $instance) {
            $instance = $this->getDi()->make($class, $params);
        }

        return $this->register($instance, $identifier);
    }

    public function get(string $identifier): ?PermissionInterface
    {
        return $this->getInstance($identifier) ?? $this->create($identifier);
    }
}
