<?php

namespace core\secure\permissions;

use app\entities\permission\Permission;
use core\cache\CacheServiceInterface;
use core\cache\CacheUpdaterTrait;
use core\database\RepositoryFactoryInterface;
use core\secure\components\DefaultRessource;
use core\secure\components\RessourceInterface;
use core\secure\roles\DefaultRole;
use core\secure\roles\RoleInterface;
use core\secure\user\UserInterface;
use core\utils\Utils;

/**
 * Class PermissionManager
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 *
 * @todo find a way to use cache if needed
 */
class PermissionManager implements PermissionServiceInterface
{
    use CacheUpdaterTrait;

    private array $ressources = [];

    /**
     * Constructor
     */
    public function __construct(
        private CacheServiceInterface $cache,
        private RepositoryFactoryInterface $repositoryFactory,
        private PermissionFactory $permissionFactory
    ) {
        //$this->_fetchPropsFromCache(['ressources']);
    }

    public function __destruct()
    {
        //$this->_cacheMultiplesProps(['ressources']);
    }



    /**
     * Return Permission object from string
     *
     * @return PermissionInterface
     */
    public function getPermissionClass(string $permissionName, ?string $context = null): PermissionInterface
    {
        $class = $permissionName;
        if (!str_contains('\\', $permissionName) && !class_exists($class)) {
            $elements = explode(':', $permissionName);
            $elements = array_map(function ($element): string {
                return ucfirst($element);
            }, $elements);

            $class = 'core\\secure\\permissions\\' . implode('', $elements) . 'Permission';
        }


        $class = (class_exists($class)) ? $class : DefaultPermission::class;

        return $this->permissionFactory->create($class, ['name' => $permissionName, 'context' => $context]);
    }


    /**
     * Return Role object from string
     *
     * @return RoleInterface
     */
    public function getRoleClass(string $roleName, ?string $context = null): RoleInterface
    {
        $class = $roleName;
        if (!str_contains('\\', $roleName)) {
            $elements = explode(':', $roleName);
            $elements = array_map(function ($element): string {
                return ucfirst($element);
            }, $elements);

            $class = 'core\\secure\\roles\\' . implode('', $elements) . 'Role';
        }


        $class = (class_exists($class)) ? $class : DefaultRole::class;

        return $this->roleFactory->create($class, ['name' => $roleName, 'context' => $context]);
    }

    /**
     * Check if user has rights for ressource
     */
    public function isAllowed(null | string | RessourceInterface | PermissionInterface $ressource, ?UserInterface $user = null): bool
    {
        $key = null;
        if (null === $ressource) {
            return false;
        }

        if (is_object($ressource)) {
            if (in_array(PermissionInterface::class, class_implements($ressource))) {
                $ressource = $ressource->getName();
            } else {
                $key = [$ressource::class, $ressource->getPermissionSet()->getName()];
                $key = Utils::hash($key);
            }
        }

        if (is_string($ressource)) {
            $ressourceName = $ressource;
            $key           = Utils::hash($ressource);
            $ressource     = new DefaultRessource($ressource);
            $ressource->getPermissionSet()->addRequiredCondition($this->getPermissionClass($ressourceName));
        }


        if (null === $user) {
            return $ressource->getPermissionSet()->isEmpty();
        }

        return $this->ressources[$user->getIduser()][$key] ??= $ressource->getPermissionSet()->process($user, $ressource);
    }

    /**
     * Allow permission
     *
     * @return void
     */
    public function allow(PermissionInterface $permission, UserInterface $user)
    {
        $permissionParameters = [
            'iduser'  => $user->getIduser(),
            'name'    => $permission->getHash(),
            'context' => $permission->getContext()
        ];

        if (!$user->getPermissions()->containsIdentifier($permission->getHash())) {
            $this->repositoryFactory->table('permission')
                ->add(new Permission($permissionParameters));

            $user->getPermissions()->attach($permission, $permission->getHash());
        }
    }

    /**
     * Deny permission
     *
     * @return void
     */
    public function deny(PermissionInterface $permission, UserInterface $user)
    {
        if ($user->getPermissions()->containsIdentifier($permission->getHash())) {
            $this->repositoryFactory->table('permission')->deleteByParams(
                [
                    'iduser'  => $user->getIduser(),
                    'name'    => $permission->getHash(),
                    'context' => $permission->getContext()
                ]
            );
            $user->getPermissions()->detachWithIdentifier($permission->getHash());
        }
    }
}
