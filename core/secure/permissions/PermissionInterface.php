<?php

namespace core\secure\permissions;

use core\entities\EntitieInterface;
use core\secure\components\RessourceInterface;
use core\secure\user\UserInterface;

/**
 * Interface PermissionInterface
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
interface PermissionInterface
{
    public function getEntity(): ?EntitieInterface;

    public function setEntity(?EntitieInterface $entityInterface);

    /**
     * Custom function to execute to check permission
     */
    public function process(UserInterface $user, RessourceInterface $ressource): bool;

    /**
     * Return permission name
     */
    public function getName(): string;

    /**
     * Return hash
     */
    public function getHash(): string;

    /**
     * Return ressource:id or null if no context
     */
    public function getContext(): ?string;
}
