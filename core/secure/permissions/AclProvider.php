<?php

namespace core\secure\permissions;

use core\DI\DiProvider;

/**
 * Trait AclProvider
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait AclProvider
{
    use DiProvider;

    /**
     * PermissionServiceInterface implémentation
     *
     * @var PermissionServiceInterface
     */
    private $acl;

    /**
     * Return PermissionServiceInterface implémetation
     */
    public function getAcl(): PermissionServiceInterface
    {
        return $this->acl ??= $this->getDi()->singleton(PermissionServiceInterface::class);
    }
}
