<?php

namespace core\secure\permissions;

use core\secure\components\RessourceInterface;
use core\secure\user\UserInterface;

/**
 * Class ModalPermission
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class ModalPermission extends DefaultPermission implements PermissionInterface
{
    /**
     * Custom function to execute to check permission
     */
    public function process(UserInterface $user, RessourceInterface $ressource): bool
    {
        /** @var Modal $ressource */
        $elements = explode('_', $ressource->getIdAttribute() ?? '');

        if (!isset($elements[2])) {
            return true;
        }

        $this->setName($elements[1] . ':' . $elements[2]);


        /** @var PermissionInterface $permission */
        foreach ($user->getPermissions() as $permission) {
            if ($permission->getName() === $this->getName()) {
                return true;
            }
        }


        return false;
    }
}
