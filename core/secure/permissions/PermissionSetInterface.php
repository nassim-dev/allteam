<?php

namespace core\secure\permissions;

/**
 * Interface PermissionSetInterface
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
interface PermissionSetInterface extends PermissionInterface
{
    /**
     * Add a required permission
     *
     * @return void
     */
    public function addRequiredCondition(PermissionInterface $permission);

    /**
     * Add an high level permission
     *
     * @return void
     */
    public function addPriorityPermission(PermissionInterface $permission);

    /**
     * Check if it contains permissions
     */
    public function isEmpty(): bool;
}
