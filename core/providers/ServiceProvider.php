<?php

namespace core\providers;

use core\cache\CacheProvider;
use core\database\DatabaseManagerProvider;
use core\database\RepositoryFactoryProvider;
use core\DI\DiProvider;
use core\entities\EntitieFactoryProvider;
use core\events\EventManagerProvider;
use core\extensions\ExtensionInstallerProvider;
use core\extensions\ExtensionLauncherProvider;
use core\extensions\ExtensionUninstallerProvider;
use core\html\AssetsProvider;
use core\html\javascript\JavascriptProvider;
use core\logger\LogProvider;
use core\messages\request\RequestProvider;
use core\messages\response\ResponseProvider;
use core\notifications\NotificationProvider;
use core\routing\DispatcherProvider;
use core\routing\MiddlewareProvider;
use core\routing\RouterProvider;
use core\secure\authentification\AuthProvider;
use core\secure\csrf\CsrfProvider;
use core\secure\permissions\AclProvider;
use core\session\SessionProvider;
use core\transport\websocket\WebsocketProvider;
use core\view\ViewProvider;

/**
 * Class ServiceProvider
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class ServiceProvider implements ServiceProviderInterface
{
    use CacheProvider;
    use MiddlewareProvider;
    use AssetsProvider;
    use AuthProvider;
    use CsrfProvider;
    use DatabaseManagerProvider;
    use EntitieFactoryProvider;
    use EventManagerProvider;
    use ExtensionLauncherProvider;
    use ExtensionInstallerProvider;
    use ExtensionUninstallerProvider;
    use JavascriptProvider;
    use LogProvider;
    use NotificationProvider;
    use AclProvider;
    use ViewProvider;
    use SessionProvider;
    use RequestProvider;
    use ResponseProvider;
    use WebsocketProvider;
    use RouterProvider;
    use DispatcherProvider;
    use RepositoryFactoryProvider;
    use DiProvider;
}
