<?php

namespace core\providers;

use core\DI\DependencieInjectorInterface;

/**
 * Interface ServiceProviderInterface
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
interface ServiceProviderInterface
{
    /**
     * Get dependencyInjector
     *
     * @PhpUnitGen\get("di")
     */
    public function getDi(): DependencieInjectorInterface;

    /**
     * Set dependencyInjector
     *
     * @PhpUnitGen\set("di")
     *
     * @param DependencieInjectorInterface $di DependencyInjector
     *
     * @return static
     */
    public function setDi(DependencieInjectorInterface $di);
}
