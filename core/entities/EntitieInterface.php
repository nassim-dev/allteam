<?php

namespace core\entities;

use core\database\mysql\lib\nette\CustomNetteSelection;
use core\html\button\Button;
use Nette\Database\Table\IRow;

/**
 * Interface EntitieInterface
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
interface EntitieInterface extends \IteratorAggregate, IRow
{
    public const HTML_FORMAT = 1;

    public const ARRAY_JS = 2;

    public const ARRAY_FORMAT = 3;

    public const RETURN_SEARCH_FIELDS = 4;


    public function getPropertyVisibilities():array;

    /**
     * Get required field to record a row
     */
    public function getRequiredField(): string|null|array;

    /**
     * Return primary key
     */
    public function getPrimaryKey(): ?string;

    /**
     * Returns primary key value.
     *
     * @return mixed possible int, string, array, object (Nette\Utils\DateTime)
     */
    public function getPrimary(bool $throw = true);

    /**
     * Return associated table
     */
    public function getTable(): CustomNetteSelection;

    /**
     * Hydrate object with array
     *
     * @return static
     */
    public function hydrate(array $data);


    /**
     * Historical format
     */
    public function historical(): ?string;

    /**
     * Create new button
     */
    public function createButton(?string $name = null, array $parameters = []): Button;

    /**
     * Generate Html option for select list
     *
     * @param int         $format self::HTML_FORMAT or self::ARRAY_JS or self::ARRAY_FORMAT
     * @param array       $html   Displayed value of option
     * @param string|null $value  Value of option
     *
     * @return array|string|null
     */
    public function getOptionHtml(int $format = self::HTML_FORMAT, array $html = [], string $value = null);

    /**
     * Return list of hasTable
     */
    public static function getLinkTables(): ?array;

    /**
     * Return user id actually editing entitie
     */
    public function getLockerUser(): ?string;

    /**
     * Find related rows for specific field or table
     */
    public function findRelated(string | array $field, int $format = self::ARRAY_FORMAT, ?array $selector = null): array | CustomNetteSelection;

    /**
     * Update related rows for specific field or table
     *
     * @return void
     */
    public function updateRelations(string $tableName, array | string $values);

    /**
     * Chec if entitie is required
     */
    public function isRequired(): bool;

    /**
     * Return namespace
     */
    public function getNamespace(): string;

    /**
     * Get Only vars
     */
    public function getVars(): array;

    /**
     * Return array of properties
     */
    public function getVarsKeys(): array;

    /**
     * Return field use to process search
     */
    public function getSearchFields(): array;
}
