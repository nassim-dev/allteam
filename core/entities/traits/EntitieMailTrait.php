<?php

namespace core\entities\traits;

use core\utils\Utils;

trait EntitieMailTrait
{
    public function getFormattedDate(): string
    {
        return utf8_encode(strftime('%A %d %B ' . utf8_decode('à') . ' %Hh%M', strtotime($this->sendtime)));
    }

    /**
     * Return formatted email list
     *
     * @return string
     */
    public function getFormattedListId($showPersonalMail = false)
    {
        $repositoryFactory = null;
        $users             = $repositoryFactory->table('user')->getByParams(['iduser' => explode(';', $this->listid)]);
        $nameTo            = [];
        foreach ($users as $user) {
            if ($this->auth::getIduser() === $user->iduser && !$showPersonalMail) {
                $nameTo[] = _('Me') . ' <' . $this->auth::getUser()->getFormatEmail() . '>';
            } else {
                $nameTo[] = $user->getFormattedName() . ' <' . $user->getFormatEmail() . '>';
            }
        }

        if (empty($nameTo)) {
            $user = $repositoryFactory->table('user')->getById($this->idto);

            return ($this->auth::getIduser() === $user->iduser) ? _('Me') . ' <' . $this->auth::getUser()->getFormatEmail() . '>' : $user->getFormattedName() . ' <' . $user->getFormatEmail() . '>';
        }

        return implode(' ', $nameTo);
    }

    /**
     * Return mail header
     */
    public function getHeaders(): string
    {
        $header = '---------- Forwarded message ---------<br>';
        $header .= _('Subject') . ' : ' . $this->object . '<br>';
        $header .= _('Date') . ' : ' . Utils::formatDate($this->sendtime) . '<br>';
        $header .= _('Sender') . ' : ' . $this->getSender()->getFormattedName() . ' <' . $this->getSender()->getFormatEmail() . '><br>';
        $header .= _('Receivers') . ' : ' . $this->getFormattedListId(true);

        return $header;
    }

    /**
     * @param $icon
     *
     * @return mixed
     */
    public function getObjectFormatted($icon = true)
    {
        if ($icon) {
            $boldOpenTag  = (0 === $this->readed) ? '<b><i class="fas fa-envelope fa-fw"> </i> ' : '<i class="fas fa-envelope-o fa-fw"> </i> ';
            $boldCloseTag = (0 === $this->readed) ? '</b>' : null;
        } else {
            $boldOpenTag  = (0 === $this->readed) ? '<b> ' : null;
            $boldCloseTag = (0 === $this->readed) ? '</b>' : null;
        }

        return $boldOpenTag . $this->object . $boldCloseTag;
    }

    public function getPreview()
    {
        return substr($this->getTextFormat(), 0, 100) . '...';
    }

    public function getReceiver()
    {
        $repositoryFactory = null;

        return $repositoryFactory->table('user')->getById($this->getIdto());
    }

    /*   public function setChilds(treeviewCategorie $treeviewCategorie)
    {
    $childs = $this->getChilds();
    if (is_bool($childs)) {
    return $treeviewCategorie;
    }
    foreach ($childs as $child) {

    $actionBar = $child->getActionBar();

    $child->setExtraActions();
    $button = $child->createButton(_('Delete'));
    $button->setPage("button.html");
    $icon = (0 === $child->readed) ? "fa-envelope" : "fa-envelope-o";
    $treeviewCategorie->addItem(new treeviewItem('<div class="d-flex flex-row"><div class="p-2">' . $child->getObjectFormatted(false) . " - " . Utils::formatDate($child->sendtime) . $child->getPreview() . '</div><div class="p-2">' . $actionBar->getHtml(, "dropdownBar.html") . "</div></div>", $icon));

    $child->setChilds($treeviewCategorie);
    }
    return $treeviewCategorie;
    }

    public function getTreeview()
    {
    if (is_null($this->idreplyto) or $this->idreplyto === 0) {
    $treeview = new treeview();
    $icon = (0 === $this->readed) ? "fa-envelope" : "fa-envelope-o";
    $treeviewCategorie = new treeviewCategorie($this->getObjectFormatted(false) . $this->getPreview(), $icon);
    $treeviewCategorie = $this->setChilds($treeviewCategorie);
    $treeview->addCategorie($treeviewCategorie);

    return (is_null($treeview)) ? $this->getObjectFormatted() : $treeview->getHtml();
    }
    return $this->getObjectFormatted();
    }

    public function getUlList()
    {

    $childs = $this->getChilds();
    if (is_bool($childs)) {
    return $this->getObjectFormatted(false) . $this->getPreview();
    }
    $list = new UlElement($this->idinbox);
    foreach ($childs as $child) {

    $actionBar = $child->getActionBar();

    $child->setExtraActions();
    $button = $child->createButton(_('Delete'));
    $button->setPage("button.html");
    $icon = (0 === $child->readed) ? "fa-envelope" : "fa-envelope-o";

    $li = $list->addChild('<div class="d-flex w-100 justify-content-between"><div class="p-2">' . $child->getObjectFormatted(false) . " - " . Utils::formatDate($child->sendtime) . $child->getPreview() . '</div><div class="p-2">' . $actionBar->getHtml(null, "dropdownBar.html"));
    $li->setIcon($icon);
    //$list->addChild($child->getUlList());
    }
    return $list;
    }
     */

    public function getSender()
    {
        $repositoryFactory = null;

        return $repositoryFactory->table('user')->getById($this->getIdfrom());
    }

    public function getTextFormat(): string
    {
        return htmlspecialchars_decode(preg_replace('/\s+/', ' ', $this->text));
    }
}
