<?php

namespace core\entities\skeleton;

/**
 * Class Skeleton
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
#[\core\entities\EntitieAttribute(tableName:'skeleton')]
class Skeleton extends \core\entities\EntitieAbstract
{
    /**
     * Database's Fields used in view
     */
    private array $searchFields = [];

    public function __construct(array $data, ?\core\database\mysql\lib\nette\CustomNetteSelection $selection)
    {
        parent::__construct($data, $selection);
        $this->idcontext = ($this->idcontext > 0) ? $this->idcontext : $this->getAuth()->getContext()?->getIdcontext();
    }

    public function getPropertyVisibilities(): array
    {
        return [];
    }

    /**
     * Get database's Fields used in view
     *
     * @PhpUnitGen\get("searchFields")
     */
    public function getSearchFields(): array
    {
        return $this->searchFields;
    }
}
