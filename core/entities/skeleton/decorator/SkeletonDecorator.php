<?php

namespace core\entities\skeleton\decorator;

/**
 * Class SkeletonDecorator
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class SkeletonDecorator implements \core\entities\DecoratorInterface
{
    /**
     * Return skeleton
     *
     * @param \core\entities\skeleton\Skeleton $object
     */
    public function decorate(&$object, ?array $args = null): ?array
    {
        return null;
    }
}
