<?php

namespace core\entities\skeleton\decorator;

use core\entities\decorator\DatatableDecoratorBase;
use core\entities\DecoratorInterface;
use core\entities\skeleton\Skeleton;

/**
 * Class DatatableDecorator
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class DatatableDecorator extends DatatableDecoratorBase implements DecoratorInterface
{
    /**
     * Return datatable
     *
     * @param Skeleton $object
     */
    public function decorate(&$object, ?array $args = null): ?array
    {
        $row = parent::decorate($object, $args);
        /*__CONFIG__*/
        return $row;
    }
}
