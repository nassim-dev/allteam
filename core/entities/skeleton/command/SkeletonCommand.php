<?php

namespace core\entities\skeleton\command;

/**
 * Class FreeRoomCommand
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class SkeletonCommand implements \core\entities\CommandInterface
{
    /**
     * Execute action
     *
     * @param Skeleton $object
     */
    public function action(&$object, ?array $arguments = []): ?array
    {
        extract($arguments);

        return null;
    }
}
