<?php

namespace core\entities\widgets;

use core\controller\ControllerServiceInterface;
use core\entities\EntitieInterface;
use core\html\button\Button;
use core\html\form\command\DeleteCommand;
use core\routing\Link;

/**
 * Class DeleteButtonWidgetBase
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class DeleteButtonWidgetBase extends ButtonWidgetBase
{
    /**
     * Create widget
     *
     * @param EntitieInterface $object
     */
    public function create(&$object, ?array $arguments = null): Button
    {
        $controllerFactory = $this->getDi()->singleton(ControllerServiceInterface::class);
        $class             = $controllerFactory->getClass($object->getTable()->getName(), contextPriority:'api');
        $link              = new Link($class, 'deleteAction');
        $arguments['uuid'] ??= $object->getPrimary();


        $link->addParameter($object->getPrimaryKey(), $arguments['uuid']);

        $arguments['endpoint'] = [
            'endpoint'   => $link->getEndpoint(),
            'initParams' => $link->getParameters(),
            'method'     => $link->getMethod()
        ];



        $class = $object->getTable()->getName();
        $arguments['selector'] ??= 'btn_delete';
        $arguments['icon'] ??= 'fa-trash';
        $arguments['text'] ??= _('Delete');
        $arguments['contentsRights'] ??= 'del' . $class;
        $arguments['command'] ??= $this->commandFactory->create(DeleteCommand::class, $arguments);
        $arguments['attributes'] ??= [
            'apiMethod'              => 'DELETE',
            $object->getPrimaryKey() => $object->getPrimary(),
            'apiRessource'           => $class,
            'target'                 => 'form_delete_' . $class,
            'filter'                 => 'deleteEvent',
            'isRequired'             => $object->isRequired()
        ];

        return parent::create($object, $arguments);
    }
}
