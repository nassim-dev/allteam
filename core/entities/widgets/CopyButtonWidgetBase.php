<?php

namespace core\entities\widgets;

use core\controller\ControllerServiceInterface;
use core\entities\EntitieInterface;
use core\html\button\Button;
use core\html\form\command\CopyCommand;
use core\routing\Link;

/**
 * Class CopyButtonWidgetBase
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class CopyButtonWidgetBase extends ButtonWidgetBase
{
    /**
     * Create widget
     *
     * @param EntitieInterface $object
     */
    public function create(&$object, ?array $arguments = null): Button
    {
        $controllerFactory = $this->getDi()->singleton(ControllerServiceInterface::class);
        $class             = $controllerFactory->getClass($object->getTable()->getName(), contextPriority:'api');
        $link              = new Link($class, 'copyAction');
        $arguments['uuid'] ??= $object->getPrimary();
        $link->addParameter($object->getPrimaryKey(), $arguments['uuid']);

        $arguments['endpoint'] = [
            'endpoint'   => $link->getEndpoint(),
            'initParams' => $link->getParameters(),
            'method'     => $link->getMethod()
        ];


        $class = $object->getTable()->getName();
        $arguments['selector'] ??= 'btn_copy';
        $arguments['icon'] ??= 'fa-copy';
        $arguments['text'] ??= _('Copy');
        $arguments['contentsRights'] ??= 'create:' . $class;
        $arguments['indexKey'] = $object->getPrimaryKey();
        $arguments['command'] ??= $this->commandFactory->create(CopyCommand::class, $arguments);
        /*$arguments['attributes']     = $arguments['attributes'] ?? [
            'apiMethod'              => 'GET',
            $object->getPrimaryKey() => $object->getPrimary(),
            'apiRessource'           => $class,
            'target'                 => 'form_create_' . $class,
            'filter'                 => 'copyEvent'
        ];*/

        return parent::create($object, $arguments);
    }
}
