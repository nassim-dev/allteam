<?php

namespace core\entities\widgets;

use core\entities\EntitieInterface;
use core\html\button\Button;
use core\html\modal\Modal;

/**
 * Class AddButtonWidgetBase
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class AddButtonWidgetBase extends ButtonWidgetBase
{
    /**
     * Create widget
     *
     * @param EntitieInterface $object
     */
    public function create(&$object, ?array $arguments = null): Button
    {
        $arguments['icon'] ??= 'fa-plus';
        $arguments['text'] ??= _('New');

        $button = parent::create($object, $arguments);

        /** @var ModalFactory $modalFactory */
        $modalFactory = $this->getDi()->singleton(ModalFactory::class);

        $modalFactory->createThen('create_' . $object->getTable()->getName(), function (Modal $modal) use ($button): void {
            $button->addDataAttributes(
                [
                    'bs-toggle' => 'modal',
                    'bs-target' => '#' . $modal->getIdAttribute()
                ]
            );

            $button->enable();
        });

        $button->addClasses(
            [
                'btn_create' . $object->getTable()->getName(),
                'animated',
                'heartBeat',
                'delay-3s',
                'btn-light-success'
            ]
        )
            ->setIcon('fa-plus');

        return $button;
    }
}
