<?php

namespace core\entities\widgets;

use core\DI\DiProvider;
use core\entities\EntitieInterface;
use core\entities\WidgetInterface;
use core\html\bar\BarFactory;
use core\html\button\Button;
use core\html\button\command\RedirectCommand;
use core\html\CommandFactory;
use core\secure\authentification\AuthServiceInterface;

/**
 * Class ButtonWidgetBase
 *$
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class ButtonWidgetBase implements WidgetInterface
{
    use DiProvider;

    public function __construct(
        protected BarFactory $barFactory,
        protected AuthServiceInterface $auth,
        protected CommandFactory $commandFactory
    ) {
    }

    /**
     * Create widget
     *
     * @param EntitieInterface $object
     */
    public function create(&$object, ?array $arguments = null): Button
    {
        extract($arguments);

        $class       = $object->getTable()->getName();
        $cssSelector = $selector ?? null . '_' . $class;
        $actionBar   = $this->barFactory->createForEntity($object);
        $button      = $actionBar->addButton($text ?? uniqid());

        $button->addClasses([$colors ?? null, $cssSelector])
            ->setIcon($icon ?? '')
            ->addDataAttributes($attributes ?? []);

        $button->setHtmlTemplate($htmlTemplate ?? $button->getHtmlTemplate());

        $button->disable();

        if ($this->auth->getPermissionManager()->isAllowed($button, $this->auth->getUser()) && $arguments != null) {
            $button->on('click')
                ->command($command ?? $this->commandFactory->create(RedirectCommand::class, $arguments))
                ->addParameters(['parentNode' => $parentNode ?? null]);
            $button->enable();
        }

        return $button;
    }
}
