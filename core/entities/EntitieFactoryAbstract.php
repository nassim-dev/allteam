<?php

namespace core\entities;

use core\cache\CacheServiceInterface;
use core\cache\CacheUpdaterTrait;
use core\containers\ContainerInterface;
use core\containers\DefaultContainer;
use core\factory\AbstractFactory;
use core\factory\FactoryException;

/**
 * Abstract class EntitieFactoryAbstract
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
abstract class EntitieFactoryAbstract extends AbstractFactory implements EntitieFactoryInterface
{
    use CacheUpdaterTrait;

    protected $_mapping;

    public function __construct(private CacheServiceInterface $cache)
    {
        $this->_fetchPropsFromCache(['_mapping']);

        if (!is_array($this->_mapping)) {
            $this->_mapping = [];
        }
    }

    public function __destruct()
    {
        $this->_cacheMultiplesProps(['_mapping']);
    }

    /**
     * Return a entitie object
     */
    public function create(string $entitie, array $params = []): ?EntitieInterface
    {
        $data      = $params[0] ?? $params['data'] ?? [];
        $selection = $params[1] ?? $params['selection'] ?? null;

        $module = $this->getClass($entitie);
        if ($module === null) {
            return null;
        }

        return (class_exists($module)) ? new $module($data, $selection) : new EntitieDefault($data, $selection);
    }

    /**
     * Get all instances or filtered by callback
     *
     * @PhpUnitGen\get("instances")
     */
    public function getInstances(?callable $filter = null): ContainerInterface
    {
        $this->instances ??= new DefaultContainer();
        if (!null === $filter) {
            $entries = array_filter($this->instances->toArray(), $filter);

            return new DefaultContainer($entries);
        }

        return $this->instances;
    }

    /**
     * Add new entitie in mapping
     */
    public function registerEntitie(string $tableName, string $class): self
    {
        if (isset($this->_mapping[$tableName]) && $this->_mapping[$tableName] !== $class) {
            throw new FactoryException("$tableName is already used by " . $this->_mapping[$tableName]);
        }

        $this->_mapping[$tableName] = $class;

        return $this;
    }

    /**
     * Return namspace
     *
     * !!!Must be implemented in child class !!!
     */
    abstract public function getNameSpace(): string;

    /**
     * Get class name if exist ot throw error
     *
     * @throws FactoryException
     */
    private function getClass(string $identifier): ?string
    {
        if (class_exists($identifier)) {
            return $identifier;
        }

        $defaultIdentifier = $this->getNameSpace() . $identifier . '\\' . ucfirst($identifier);
        if (class_exists($defaultIdentifier)) {
            return $defaultIdentifier;
        }

        if (isset($this->_mapping[$identifier]) && class_exists($this->_mapping[$identifier])) {
            return $this->_mapping[$identifier];
        }

        $identifier = strtolower($identifier);
        if (isset($this->_mapping[$identifier]) && class_exists($this->_mapping[$identifier])) {
            return $this->_mapping[$identifier];
        }

        return null;
        //throw new FactoryException("$identifier can not be instanciate, because it's not a class");
    }
}
