<?php

namespace core\entities\command;

use core\database\DatabaseManagerInterface;
use core\database\mysql\lib\nette\CustomNetteSelection;
use core\entities\CommandException;
use core\entities\CommandInterface;
use core\entities\EntitieInterface;
use Nette\InvalidArgumentException;

/**
 * Class RelatedRowFinderCommandBase
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class RelatedRowFinderCommandBase implements CommandInterface
{
    public function __construct(protected DatabaseManagerInterface $database)
    {
    }

    /**
     * @param EntitieInterface $object
     * @param mixed[]|null     $arguments
     */
    public function action(&$object, ?array $arguments = []): array | CustomNetteSelection
    {
        if (!isset($arguments[0])) {
            throw new CommandException("You must provide a field. It could be a string or an array. ex: 'event' or ['event']");
        }

        return $this->_getRelatedRows(
            $object,
            $arguments[0],
            $arguments[1] ?? $object::ARRAY_FORMAT,
            $arguments[2] ?? null
        );
    }

    /**
     * Get related rows for$object
     *
     * @return array|CustomNetteSelection
     *
     * @throws InvalidArgumentException
     */
    private function _getRelatedRows(EntitieInterface $object, string | array $tableName, int $format, ?array $selector): array | CustomNetteSelection
    {
        /**
         * @var IStructure $structure
         */
        $structure = $this->database->getStructure();
        $type      = $object->getTable()->getName();


        if (!is_array($tableName)) {
            $targetTable = (str_starts_with($tableName, 'id')) ? substr($tableName, 2) : $tableName;
            $key         = 'id' . $targetTable;

            if (!in_array($key, $this->database->getColumnsFor($tableName))) {
                return $this->_getRowsReferingTo($object, $tableName, $format, $selector);
            }
        } else {
            $targetTable = (str_starts_with($tableName[0], 'id')) ? substr($tableName[0], 2) : $tableName[0];
            $key         = $tableName;
        }

        $firstTable  = $type . '_' . $tableName . '_link';
        $secondTable = $tableName . '_' . $type . '_link';

        $structure->initializeTable($firstTable);
        $structure->initializeTable($secondTable);

        $tables = [
            (in_array($firstTable, $structure->getTables())) ? $firstTable : null,
            (in_array($secondTable, $structure->getTables())) ? $secondTable : null,
        ];

        $objects = [];
        foreach ($tables as $table) {
            if ($table === null) {
                continue;
            }

            /**
             * @var CustomNetteSelection $database
             */
            $database = $this->database->getOrm($table);
            $objects  = $database->where([$object->getPrimaryKey() => $object->getPrimary()]);
            $objects  = (null !== $selector) ? $objects->where($selector) : $objects;
        }

        $relations = [];
        foreach ($objects as $tmp) {
            $relations[] = (EntitieInterface::ARRAY_FORMAT === $format) ? ((!is_array($key)) ? $tmp->$key : $tmp->toArray()) : $tmp->$targetTable;
        }

        return $relations;
    }

    /**
     * Generate related that refer to $object
     *
     * @return array|CustomNetteSelection
     */
    private function _getRowsReferingTo(EntitieInterface $object, string $tableName, int $format, ?array $selector): array | CustomNetteSelection
    {
        $targetTable = (str_starts_with($tableName, 'id')) ? substr($tableName, 2) : $tableName;
        $class       = $object->getTable()->getName();
        $key         = 'id' . $targetTable;

        $relations = [];


        /**
         * @var CustomNetteSelection $database
         */
        $database = $this->database->getOrm($targetTable);

        //$objects = $database->group($targetTable . '.' . $key)
        //->where(':' . $class . '.' . $object->getPrimaryKey(), $object->getPrimary());
        $objects = $database->where([$object->getPrimaryKey() => $object->getPrimary()]);

        if (null !== $selector) {
            $objects->where($selector);
        }

        $relations = [];
        foreach ($objects as $tmp) {
            $relations[] = (EntitieInterface::ARRAY_FORMAT === $format) ? $tmp->$key : $tmp;
        }

        return $relations;
    }
}
