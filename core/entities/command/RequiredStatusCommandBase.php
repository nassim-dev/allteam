<?php

namespace core\entities\command;

use core\cache\CacheServiceInterface;
use core\database\DatabaseManagerInterface;
use core\entities\CommandInterface;
use core\entities\EntitieInterface;

/**
 * Class RequiredStatusCommandBase
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class RequiredStatusCommandBase implements CommandInterface
{
    public function __construct(
        protected CacheServiceInterface $cache,
        protected DatabaseManagerInterface $database
    ) {
    }

    /**
     * @param EntitieInterface $object
     * @param mixed[]|null     $arguments
     */
    public function action(&$object, ?array $arguments = []): bool
    {
        $status = $this->cache->get('ressources.' . $object->getTable()->getName() . '.' . $object->getPrimary());
        if (null === $status) {
            $status = $this->findRelatedRows($object);
        }

        return $status;
    }

    /**
     * @todo implements
     */
    private function findRelatedRows(EntitieInterface $object): bool
    {
        return false;
    }
}
