<?php

namespace core\entities\command;

use core\DI\DiProvider;
use core\entities\CommandInterface;
use core\entities\EntitieInterface;
use core\html\bar\Bar;
use core\html\bar\BarFactory;
use core\utils\Utils;
use Nette\Utils\DateTime;

/**
 * Class OptionHtmlCommandBase
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class OptionHtmlCommandBase implements CommandInterface
{
    use DiProvider;

    public const HTML_FORMAT          = EntitieInterface::HTML_FORMAT;
    public const RETURN_SEARCH_FIELDS = EntitieInterface::RETURN_SEARCH_FIELDS;
    public const ARRAY_JS             = EntitieInterface::ARRAY_JS;
    public const ARRAY_FORMAT         = EntitieInterface::ARRAY_FORMAT;

    public function __construct(protected BarFactory $barFactory)
    {
    }

    /**
     * @param EntitieInterface $object
     * @param mixed[]|null     $arguments
     *
     * @return string|array|null
     */
    public function action(&$object, ?array $arguments = [])
    {
        $format = null;
        $value  = null;
        extract($arguments, EXTR_OVERWRITE);
        $html = $object->getSearchFields();
        if (self::RETURN_SEARCH_FIELDS === $format) {
            return $html;
        }

        $value   = $this->_processValueOption($value, $object);
        $html    = $this->_processHtmlOption($html, $object);
        $primary = $object->getPrimary();
        //$indexKey = Utils::hiddenId($primary);

        switch ($format) {
            case self::ARRAY_JS:

                /** @var Bar $actionBar */
                $actionBar = $this->barFactory->createDefaultButtons($object);
                $actionBar->setHtmlTemplate($actionBar::getBaseTemplateDir() . 'bar/bar.select2.html');

                $return = [
                    'value'     => $value,
                    'text'      => $html /*. $indexKey*/,
                    'id'        => $value,
                    'actionbar' => $this->getDi()->call($actionBar, 'getHtml')
                ];

                break;
            case self::HTML_FORMAT:
                $return = "<option value='$value'>$html.$indexKey</option>";

                break;
            case self::ARRAY_FORMAT:
                $return = [$html => $value];

                break;
            default:
                $return = null;

                break;
        }

        return $return;
    }

    /**
     * Genrate Html for getOptionHtml method
     *
     * @todo Refactor
     *
     * @param string|array|null $html
     *
     * @return string
     */
    private function _processHtmlOption($html, EntitieInterface $object): ?string
    {
        if (is_array($html) && !empty($html)) {
            return $this->_resolveArray($html, $object);
        }

        if (null === $html || empty($html)) {
            return $object->getPrimaryKey() . ':' . $object->getPrimary();
        }

        $method = 'get' . ucfirst($html);
        if (method_exists($object, $method)) {
            return $object->{$method}();
        }

        if (property_exists($object, $html)) {
            return $object->$html;
        }
    }

    /**
     * Resolve parmeters if an array is provided
     */
    private function _resolveArray(array $html, EntitieInterface $object): string
    {
        $method_value = [];
        foreach ($html as $parameter) {
            if (preg_match('/->/', $parameter)) {
                $value = $this->_resolveComplexParameter($object, $parameter);
                if ($value !== null) {
                    $method_value[] = $value;
                }

                continue;
            }

            if (property_exists($object, $parameter)) {
                if ($object->$parameter !== null) {
                    $method_value[] = $object->$parameter;
                }

                continue;
            }

            $method = 'get' . ucfirst($parameter);
            if (!null === $object->{$method}() && $object->{$method}() !== '' && !empty($object->{$method}())) {
                $this->_resolveSingleParameter($object, $method, $method_value);
            }
        }

        return implode(' - ', $method_value);
    }

    /**
     * Resolve single parameter
     */
    private function _resolveSingleParameter(EntitieInterface $object, string $method, array &$method_value): array
    {
        if (is_object($object->{$method}())) {
            $method_value[] = ($object->{$method}()::class === DateTime::class) ? $object->{$method}()->format('d/m/Y (H:i)') : $object->{$method}();
        } else {
            $method_value[] = $object->{$method}();
        }

        return $method_value;
    }

    /**
     * Resolve parameter in format getField->getOtherField
     */
    private function _resolveComplexParameter(EntitieInterface $object, string $parameter): ?string
    {
        $properties = explode('->', $parameter);
        $methodtmp  = (method_exists($object, $properties[0])) ? $object->{$properties[0]}() : $object->{$properties[0]};

        if (is_bool($methodtmp) || null === $methodtmp) {
            return  null;
        }

        unset($properties[0]);
        foreach ($properties as $propertie) {
            $methodtmp = (method_exists($methodtmp, $propertie)) ? $methodtmp->{$propertie}() : $methodtmp->{$propertie};
        }

        return $methodtmp;
    }

    /**
     * Process Values for getOptionHtml method
     *
     * @todo Refactor
     *
     * @param string|array|null $values
     *
     * @return string
     */
    private function _processValueOption($values, EntitieInterface $object)
    {
        if (is_array($values)) {
            $keys = [];
            foreach ($values as $parameter) {
                $method = 'get' . ucfirst($parameter);
                $keys[] = $object->{$method}();
            }

            return implode(' - ', $keys);
        }


        if (null === $values) {
            return $object->getPrimary();
        }

        $method = 'get' . ucfirst($values);

        return $object->{$method}();
    }
}
