<?php

namespace core\entities\command;

use core\database\DatabaseManagerInterface;
use core\database\mysql\lib\nette\CustomNetteSelection;
use core\entities\CommandInterface;
use core\entities\EntitieInterface;
use Nette\Database\IStructure;

/**
 * Class RelatedRowUpdaterCommandBase
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class RelatedRowUpdaterCommandBase implements CommandInterface
{
    public function __construct(protected DatabaseManagerInterface $database)
    {
    }

    /**
     * @param EntitieInterface $object
     * @param mixed[]|null     $arguments
     *
     * @return void
     */
    public function action(&$object, ?array $arguments = [])
    {
        $tableName = $arguments['tableName'] ?? '';
        $values    = $arguments['values'] ?? null;
        $type      = (str_starts_with($tableName, 'id')) ? substr($tableName, 2) : $tableName;
        if (!is_array($values) || empty($values)) {
            return;
        }

        /**
         * @var IStructure $structure
         */
        $structure = $this->database->getStructure();

        $firstTable  = $type . '_' . $tableName . '_link';
        $secondTable = $tableName . '_' . $type . '_link';

        $structure->initializeTable($firstTable);
        $structure->initializeTable($secondTable);

        $tables = [
            (in_array($firstTable, $structure->getTables())) ? $firstTable : null,
            (in_array($secondTable, $structure->getTables())) ? $secondTable : null,
        ];

        $objectsToAdd = $this->_generateNewRows($values, $object, $tableName);
        foreach ($tables as $table) {
            if ($table === null) {
                continue;
            }

            /**
             * @var CustomNetteSelection $database
             */
            $database = $this->database->getOrm($table);
            $database->where([$object->getPrimaryKey() => $object->getPrimary()])->delete();

            $database->insert($objectsToAdd);
        }
    }

    /**
     * Generate n,ew row to add
     */
    private function _generateNewRows(array $values, EntitieInterface $object, string $tableName): array
    {
        $objectsToAdd = [];

        foreach ($values as $value) {
            if (!is_int($object->getPrimary()) || null === $value || '' === $value) {
                continue;
            }

            if (is_object($value)) {
                $value[$object->getPrimaryKey()] = $object->getPrimary();
                $objectsToAdd[]                  = $value;

                continue;
            }

            $objectsToAdd[] = [
                $object->getPrimaryKey() => $object->getPrimary(),
                $tableName               => $value
            ];
        }

        return $objectsToAdd;
    }
}
