<?php

namespace core\entities\command;

use core\cache\CacheServiceInterface;
use core\database\RepositoryFactoryInterface;
use core\entities\CommandInterface;
use core\entities\EntitieInterface;
use core\secure\authentification\AuthServiceInterface;

/**
 * Class LockerCommandBase
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class LockerCommandBase implements CommandInterface
{
    public function __construct(
        protected CacheServiceInterface $cache,
        protected AuthServiceInterface $auth,
        protected RepositoryFactoryInterface $repositoryFactory
    ) {
    }

    /**
     * @param EntitieInterface $object
     * @param mixed[]|null     $arguments
     */
    public function action(&$object, ?array $arguments = []): ?string
    {
        // extract($arguments);

        $id     = $this->cache->get('entitieState.' . $object->getTable()->getName() . '.' . $object->getPrimary());
        $status = null;

        if (is_numeric($id)) {
            if ($this->auth->getUser()?->getIduser() === $id) {
                return null;
            }

            $user = $this->repositoryFactory->table('user')->getById($id);
            if (!is_bool($user)) {
                $status = $user->getFormattedName();
            }
        }

        return $status;
    }
}
