<?php

namespace core\entities;

use core\database\mysql\lib\nette\CustomNetteSelection;
use core\DI\DI;
use core\DI\DiProvider;
use core\entities\command\LockerCommandBase;
use core\entities\command\OptionHtmlCommandBase;
use core\entities\command\RelatedRowFinderCommandBase;
use core\entities\command\RelatedRowUpdaterCommandBase;
use core\entities\command\RequiredStatusCommandBase;
use core\entities\decorator\HistoricalDecoratorBase;
use core\entities\widgets\AddButtonWidgetBase;
use core\entities\widgets\ButtonWidgetBase;
use core\entities\widgets\CopyButtonWidgetBase;
use core\entities\widgets\DeleteButtonWidgetBase;
use core\entities\widgets\EditButtonWidgetBase;
use core\html\bar\Bar;
use core\html\bar\BarFactory;
use core\html\button\Button;
use core\html\traits\WithActions;
use core\secure\components\RessourceInterface;
use core\secure\components\RessourceTrait;
use core\secure\permissions\PermissionSetInterface;
use Nette\Database\Table\ActiveRow;
use ReflectionClass;

/**
 * Abstract class EntitieAbstract
 *
 * Represent Mysql Row
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
abstract class EntitieAbstract extends ActiveRow implements EntitieInterface, RessourceInterface
{
    use WithActions;
    use RessourceTrait;
    use DiProvider;

    private $_entitieConfig = null;

    /**
     * Constructor
     */
    public function __construct(array $data, ?CustomNetteSelection $selection = null)
    {
        $table = explode('\\', static::class);
        $this->di ??= DI::getContainer();
        $selection ??= $this->getDi()->make(CustomNetteSelection::class, ['tableName' => strtolower(end($table))]);
        parent::__construct($data, $selection);
        $this->hydrate($data); // Maybe it's useless
        $this->_initializeTraits();
    }


    public function getTableName(): string
    {
        $_entitieConfig = null;
        if ($this->_entitieConfig == null) {
            $reflection = new ReflectionClass($this);
            $attributes = $reflection->getAttributes(EntitieAttribute::class);
            foreach ($attributes as $attribute) {
                $_entitieConfig = $attribute->newInstance();

                continue;
            }
        }

        return $_entitieConfig->tableName;
    }


    /**
     * Return has many relations table
     */
    public static function getHasManyRelations(): ?array
    {
        return null;
    }


    /**
     * Get required field to record a row
     */
    public function getRequiredField(): string|null|array
    {
        return null;
    }

    /**
     * Return primary key
     */
    public function getPrimaryKey(): ?string
    {
        return $this->getTable()->getPrimary(false);
    }


    /**
     * Get Object ActionBar
     */
    public function getActionBar(): Bar
    {
        return $this->__actionBar ??= $this->getDi()->singleton(BarFactory::class)->createForEntity($this);
    }


    public function getHash(): string
    {
        return static::class . ':' . $this->getPrimary();
    }

    /**
     * Transistion method in order to have old getter working
     *
     * @param string $name
     * @param array  $arguments
     *
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        $propertie = strtolower(str_replace('get', '', $name));

        return $this->{$propertie};
    }

    /**
     * Undocumented function
     *
     * @param string $column
     * @param mixed  $value
     *
     * @return void
     */
    public function __set($column, $value)
    {
        $this->accessColumn($column);
        $this->{$column} = $value;
    }



    public function __serialize(): array
    {
        return $this->toArray();
    }


    /**
     * @return void
     */
    public function __unserialize(array $data)
    {
        $this->hydrate($data);
        $table = explode('\\', static::class);
        $this->di ??= DI::getContainer();
        $selection ??= $this->getDi()->make(CustomNetteSelection::class, ['tableName' => strtolower(end($table))]);
        $this->setTable($selection);
        $this->_initializeTraits();
    }



    /**
     * Historical format
     */
    final public function historical(): ?string
    {
        $class = (class_exists($this->getNamespace() . '\decorator\HistoricalDecorator')) ? $this->getNamespace() . '\decorator\HistoricalDecorator' : HistoricalDecoratorBase::class;

        /**
         * @var HistoricalDecoratorBase $decorator
         */
        $decorator = $this->getDi()->singleton($class);

        return $decorator->decorate($this, func_get_args());
    }

    /**
     * Create new button
     */
    public function createButton(?string $name = null, array $parameters = []): Button
    {
        $name ??= _('Add');
        /**
         * @var ButtonWidgetBase $widget
         */
        $widget = match ($name) {
            _('Add')     => $this->getDi()->singleton((class_exists($this->getNamespace() . '\widgets\AddButtonWidget')) ? $this->getNamespace() . '\widgets\AddButtonWidget' : AddButtonWidgetBase::class),
            _('Delete')  => $this->getDi()->singleton((class_exists($this->getNamespace() . '\widgets\DeleteButtonWidget')) ? $this->getNamespace() . '\widgets\DeleteButtonWidget' : DeleteButtonWidgetBase::class),
            _('Copy')    => $this->getDi()->singleton((class_exists($this->getNamespace() . '\widgets\CopyButtonWidget')) ? $this->getNamespace() . '\widgets\CopyButtonWidget' : CopyButtonWidgetBase::class),
            _('Edit')    => $this->getDi()->singleton((class_exists($this->getNamespace() . '\widgets\EditButtonWidget')) ? $this->getNamespace() . '\widgets\EditButtonWidget' : EditButtonWidgetBase::class),
            _('Default') => $this->getDi()->singleton((class_exists($this->getNamespace() . '\widgets\ButtonWidgetBase')) ? $this->getNamespace() . '\widgets\ButtonWidgetBase' : ButtonWidgetBase::class),
        };


        return $widget->create($this, $parameters);
    }

    /**
     * Generate Html option for select list
     *
     * @param int         $format self::HTML_FORMAT or self::ARRAY_JS or self::ARRAY_FORMAT
     * @param array       $html   Displayed value of option
     * @param string|null $value  Value of option
     *
     * @return array|string|null
     */
    final public function getOptionHtml(int $format = self::HTML_FORMAT, array $html = [], string $value = null)
    {
        $class = (class_exists($this->getNamespace() . '\command\OptionHtmlCommand')) ? $this->getNamespace() . '\command\OptionHtmlCommand' : OptionHtmlCommandBase::class;

        /**
         * @var OptionHtmlCommandBase $command
         */
        $command = $this->getDi()->singleton($class);

        return $command->action($this, ['format' => $format, 'html' => $html, 'value' => $value]);
    }

    /**
     * Return list of hasTable
     */
    public static function getLinkTables(): ?array
    {
        return null;
    }

    /**
     * Return user id actually editing entitie
     */
    final public function getLockerUser(): ?string
    {
        /**
         * @var LockerCommandBase $command
         */
        $command = $this->getDi()->singleton(LockerCommandBase::class);

        return $command->action($this, func_get_args());
    }

    /**
     * Find related rows for specific field or table
     */
    final public function findRelated(string | array $classOrIndexKey, int $format = self::ARRAY_FORMAT, ?array $selector = null): array | CustomNetteSelection
    {
        /**
         * @var RelatedRowFinderCommandBase $command
         */
        $command = $this->getDi()->singleton(RelatedRowFinderCommandBase::class);

        return $command->action($this, func_get_args());
    }

    /**
     * Update related rows for specific field or table
     *
     * @param  mixed[]|string|null $values
     * @return void
     */
    final public function updateRelations(string $tableName, null| array | string $values)
    {
        /**
         * @var RelatedRowUpdaterCommandBase $command
         */
        $command = $this->getDi()->singleton(RelatedRowUpdaterCommandBase::class);
        $command->action($this, ['tableName' => $tableName, 'values' => $values]);
    }

    /**
     * Chec if entitie is required
     */
    final public function isRequired(): bool
    {
        /**
         * @var RequiredStatusCommandBase $command
         */
        $command = $this->getDi()->singleton(RequiredStatusCommandBase::class);

        return  $command->action($this, func_get_args());
    }

    /**
     * Return namespace
     */
    public function getNamespace(): string
    {
        $elements = explode('\\', static::class);
        array_pop($elements);

        return implode('\\', $elements);
    }

    final public function getTable(): CustomNetteSelection
    {
        return parent::getTable();
    }


    /**
     * Hydrate object with array
     */
    final public function hydrate(array $data): self
    {
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }

        //$this->data = [...$this->data, ...$data];

        return $this;
    }

    /**
     * Initialize traits
     *
     * @return void
     */
    private function _initializeTraits()
    {
        $traits = class_uses($this);
        foreach ($traits as $trait) {
            $method = "init$trait";
            if (method_exists($this, $method)) {
                call_user_func_array([$this, $method], []);
            }
        }
    }

    /**
     * Get only properties values
     */
    public function getVars(): array
    {
        $tmp = $this->toArray();

        $ret = [];
        foreach ($tmp as $v) {
            $ret[] = $v;
        }

        return $ret;
    }

    /**
     * Return array of properties
     */
    public function getVarsKeys(): array
    {
        $tmp = $this->toArray();
        $ret = [];
        foreach ($tmp as $k => $v) {
            $v = (is_string($v)) ? htmlspecialchars_decode($v, ENT_QUOTES) : $v;
            if ('_' === $k[0]) {
                $k = substr($k, 1);
            }

            //if (str_contains('id', $k) && !is_integer($v)) {
            //    $v = null;
            //}
            $ret[$k] = $v;
        }

        return $ret;
    }

    /**
     * Return owner iduser
     */
    public function getOwner(): ?int
    {
        return (property_exists($this, 'iduser')) ? $this->iduser : null;
    }

    /**
     * Return default permission container
     */
    public static function buildDefaultPermissions(PermissionSetInterface $permissionSet): PermissionSetInterface
    {
        return $permissionSet;
    }
}
