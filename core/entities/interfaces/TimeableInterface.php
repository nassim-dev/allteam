<?php

namespace core\entities\interfaces;

use core\entities\EntitieInterface;
use DateTimeInterface;

/**
 * Interface TimeableInterface
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
interface TimeableInterface extends EntitieInterface
{
    public function getStart(): DateTimeInterface | string;

    public function getEnd(): DateTimeInterface | string;
}
