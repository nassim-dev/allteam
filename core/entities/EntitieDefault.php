<?php

namespace core\entities;

use core\database\mysql\lib\nette\CustomNetteSelection;

/**
 * Class EntitieDefault
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
#[EntitieAttribute(tableName:null)]
class EntitieDefault extends EntitieAbstract
{
    /**
     * Database's Fields used in view
     */
    private array $searchFields = [];

    /**
     * Constructor
     *
     * @param CustomNetteSelection|null $selection
     */
    public function __construct(array $data, CustomNetteSelection $selection = null)
    {
        parent::__construct($data, $selection);
    }

    public function getPropertyVisibilities(): array
    {
        return [];
    }
    /**
     * Get database's Fields used in view
     *
     * @PhpUnitGen\get("searchFields")
     */
    public function getSearchFields(): array
    {
        return $this->searchFields;
    }

    /**
     * Execute action
     *
     * @return mixed
     */
    public function action()
    {
        throw new EntitieException("Invalid method 'action' for this dafault entitie, you must create a specific entitie that extends EntitieAbstract with an EntitieActions");
    }

    /**
     * Decorate object
     *
     * @return mixed
     */
    public function decorate()
    {
        throw new EntitieException("Invalid method 'decorate' for this dafault entitie, you must create a specific entitie that extends EntitieAbstract with an EntitieDecorator");
    }

    /**
     * Related widgets
     *
     * @return mixed
     */
    public function widget()
    {
        throw new EntitieException("Invalid method 'widget' for this dafault entitie, you must create a specific entitie that extends EntitieAbstract with an EntitieWidget");
    }
}
