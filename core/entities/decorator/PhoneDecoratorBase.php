<?php

namespace core\entities\decorator;

use core\entities\DecoratorInterface;
use core\entities\EntitieInterface;

/**
 * Class PhoneDecoratorBase
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class PhoneDecoratorBase extends LinkDecoratorBase implements DecoratorInterface
{
    /**
     * Return phone decorated
     *
     * @param EntitieInterface $object
     * @param array|null       $args
     */
    public function decorate(&$object, array $args = null): string
    {
        return parent::decorate($object, ['link' => "tel:$object->phone", 'text' => $object->phone]);
    }
}
