<?php

namespace core\entities\decorator;

use core\entities\DecoratorInterface;
use core\entities\EntitieInterface;

/**
 * Class LinkDecoratorBase
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class LinkDecoratorBase implements DecoratorInterface
{
    /**
     * Return email decorated
     *
     * @param EntitieInterface $object
     * @param array|null       $args
     */
    public function decorate(&$object, array $args = null): string
    {
        return '<a data-router-disabled href="' . $args['link'] ?? '#' . '">' . $args['text'] ?? 'UNDEFINED TEXT' . '</a>';
    }
}
