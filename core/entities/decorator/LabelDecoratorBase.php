<?php

namespace core\entities\decorator;

use core\entities\DecoratorInterface;
use core\html\label\Label;

/**
 * Class LabelDecoratorBase
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class LabelDecoratorBase implements DecoratorInterface
{
    /**
     * Decorate
     *
     * @param EntitieInterface $object
     */
    public function decorate(&$object, ?array $arguments = []): string
    {
        $icon     = null;
        $type     = null;
        $labelDiv = null;
        extract($arguments);

        $icon ??= null;
        $type ??= Label::SUCCESS;
        $labelDiv ??= Label::ALIGN_CENTER;

        $label        = new Label($object->decorate()->historical(), $icon, $type);
        $label->datas = [
            'id'   => $object->getPrimary(),
            'href' => $object->getHref()
        ];

        return $label->getHtml();
    }
}
