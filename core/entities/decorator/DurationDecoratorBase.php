<?php

namespace core\entities\decorator;

use core\entities\DecoratorInterface;
use core\entities\interfaces\TimeableInterface;
use Nette\Utils\DateTime;

/**
 * Class DurationDecoratorBase
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class DurationDecoratorBase implements DecoratorInterface
{
    /**
     * Return skeleton
     *
     * @param TimeableInterface $object
     * @param array|null        $args
     */
    public function decorate(&$object, array $args = null): string
    {
        $format = $args['format'] ?? DateDecoratorBase::$DEFAULT_DATE_FORMAT;

        $object->start = (is_string($object->getStart())) ? DateTime::from($object->getStart()) : $object->getStart();
        $object->end   = (is_string($object->getEnd())) ? DateTime::from($object->getEnd()) : $object->getEnd();

        $interval = date_diff($object->getStart(), $object->getEnd());

        return (is_string($format)) ? $interval->format($format) : $interval;
    }
}
