<?php

namespace core\entities\decorator;

use core\entities\DecoratorInterface;
use core\entities\EntitieInterface;
use core\html\bar\BarFactory;
use core\html\tables\datatable\DatatableDecoratorInterface;

/**
 * Class DatatableDecoratorBase
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class DatatableDecoratorBase implements DecoratorInterface, DatatableDecoratorInterface
{
    public function __construct(private BarFactory $barFactory)
    {
    }

    /**
     * Decorate
     *
     * @param EntitieInterface $object
     */
    public function decorate(&$object, ?array $arguments = []): ?array
    {
        if (!isset($arguments['buttonsEnabled'])) {
            $arguments['buttonsEnabled'] = ['edit' => true, 'delete' => true, 'copy' => true];
        }

        $row = [];
        //$row['rowId']        = $object->getPrimary();
        $row[$object->getPrimaryKey()] = $object->getPrimary();
        $actionBar                     = $this->barFactory->createDefaultButtons($object, $arguments['buttonsEnabled']);
        $row['tableactions']           = $actionBar->getHtml();

        return $row;
    }
}
