<?php

namespace core\entities\decorator;

use core\entities\DecoratorInterface;
use core\entities\EntitieInterface;

/**
 * Class HistoricalDecoratorBase
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class HistoricalDecoratorBase implements DecoratorInterface
{
    /**
     * Decorate
     *
     * @param EntitieInterface $object
     */
    public function decorate(&$object, ?array $arguments = []): ?string
    {
        return null;
    }
}
