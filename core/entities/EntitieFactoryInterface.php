<?php

namespace core\entities;

use core\factory\FactoryInterface;

/**
 * Interface EntitieFactoryInterface
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
interface EntitieFactoryInterface extends FactoryInterface
{
    /**
     * Register all entities classes from attributes
     *
     * @return array
     */
    public function registerApplicationEntities(): array;

    /**
     * Return a entitie object
     */
    public function create(string $entitie, array $params = []): ?EntitieInterface;

    /**
     * Add new entitie in mapping
     *
     * @return static
     */
    public function registerEntitie(string $tableName, string $class);
}
