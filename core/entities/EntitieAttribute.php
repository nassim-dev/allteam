<?php

namespace core\entities;

use Attribute;

/**
 * Class EntitieAttribute
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
#[Attribute(Attribute::TARGET_CLASS)]
class EntitieAttribute
{
    public function __construct(public ?string $tableName = null)
    {
    }
}
