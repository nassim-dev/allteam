<?php

namespace core\entities;

use core\DI\DiProvider;

/**
 * Trait EntitieFactoryProvider
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait EntitieFactoryProvider
{
    use DiProvider;

    /**
     * EntitieFactoryInterface implémentation
     *
     * @var EntitieFactoryInterface
     */
    private $entitieFactory;

    /**
     * Return EntitieFactoryInterface implémetation
     */
    public function getEntitieFactory(): EntitieFactoryInterface
    {
        return $this->entitieFactory ??= $this->getDi()->singleton(EntitieFactoryInterface::class);
    }
}
