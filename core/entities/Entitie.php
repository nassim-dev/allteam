<?php

namespace core\entities;

/**
 * Class Entitie
 *
 *  @method static array registerApplicationEntities()
 *  @method static \core\entities\EntitieInterface create(string $entitie, array $params)
 *  @method static string registerEntitie(string $tableName, string $class)
 *  @method static string get(string $identifier)
 *  @method static core\containers\ContainerInterface getInstances(callable $filter)
 *  @method static string remove(string $identifier)
 *  @method static string registerClass(string $class)
 *  @method static string addNamespace(string $namespace)
 *  @method static string getNamespaces()
 *  @method static string register(string $object, string $identifier)
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class Entitie extends \core\facade\HelperFacade
{
    protected static ?string $serviceInterface = 'core\entities\EntitieFactoryInterface';
}
