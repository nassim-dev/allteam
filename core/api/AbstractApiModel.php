<?php

namespace core\api;

use core\DI\DiProvider;
use core\messages\request\RequestParameterTrait;
use core\model\ModelInterface;

/**
 * Class AbstractApiModel
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
abstract class AbstractApiModel implements ModelInterface
{
    use RequestParameterTrait;
    use DiProvider;

    /**
     * Get the value of tableName
     *
     * @PhpUnitGen\get("tableName")
     */
    abstract public function getTableName(): ?string;

    /**
     * Get the value of tableNameUcFirst
     *
     * @PhpUnitGen\get("tableNameUcFirst")
     */
    abstract public function getTableNameUcFirst(): ?string;

    /**
     * Get the value of tableNameUpper
     *
     * @PhpUnitGen\get("tableNameUpper")
     */
    abstract public function getTableNameUpper(): ?string;
}
