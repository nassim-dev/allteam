<?php

namespace core\api;

use core\controller\ControllerInterface;
use core\database\mysql\MysqlRepository;
use core\database\RepositoryFactoryInterface;
use core\database\RepositoryInterface;
use core\DI\DiProvider;
use core\DI\InjectionAttribute;
use core\entities\Entitie;
use core\entities\EntitieFactoryInterface;
use core\entities\EntitieInterface;
use core\html\AssetsServiceInterface;
use core\html\form\Form;
use core\html\form\FormFactory;
use core\html\FormatterInterface;
use core\messages\HttpUtils;
use core\messages\request\HttpRequestInterface;
use core\messages\request\RequestParameterTrait;
use core\messages\response\HttpResponseInterface;
use core\notifications\NotificationServiceInterface;
use core\secure\authentification\AuthServiceInterface;
use core\secure\csrf\CsrfServiceInterface;
use core\tasks\AsyncJob;
use core\tasks\DeepcopyTask;
use core\transport\TransportServiceInterface;
use core\utils\Utils;
use Nette\Utils\DateTime;

/**
 * Class AbstractApiController
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
abstract class AbstractApiController implements ControllerInterface
{
    use RequestParameterTrait;
    use DiProvider;

    /**
     * Return Encrypted Datas
     * @var bool
     */
    public $encryptedDatas = true;

    /**
     * Database service for related table
     *
     * @var null|RepositoryInterface
     */
    protected ?RepositoryInterface $dbManager;

    /**
     * Component to reload in view
     *
     * @var array
     */
    protected $reloadedComponents = [];

    /**
     * Form object
     *
     * @var Form|null
     */
    protected $form = null;

    /**
     * Is public controller ?
     *
     * @var boolean
     */
    protected $public = false;

    /**
     * Called table
     * @var string|null
     */
    protected $table = null;

    /**
     * Set redirect url
     *
     * @var boolean|string
     */
    protected $errorLocation = false;

    /**
     * Constructor
     */
    public function __construct(
        protected AuthServiceInterface $auth,
        protected HttpRequestInterface $request,
        protected HttpResponseInterface $response,
        protected RepositoryFactoryInterface $repositoryFactory,
    ) {
    }

    /**
     * Call a method
     *
     * @return mixed
     */
    public function call(string $method)
    {
        return $this->{$method}();
    }

    /**
     * Copy an entitie by key
     *
     * @return void
     */
    #[InjectionAttribute(AsyncJob::class, build:InjectionAttribute::UNIQUE)]
    public function copyAction(NotificationServiceInterface $notifier, AsyncJob $task)
    {
        if (null === $this->getParameter('id' . $this->getTable())) {
            $this->killServer(206);
        }


        $object = $this->getDbManager()->getById($this->getParameter('id' . $this->getTable()));
        if (is_bool($object) || null === $object) {
            $notifier->notify('Innexisting entitie', $notifier::DANGER, $this->errorLocation);
            $this->killServer(204);
        }

        $objectProperties = $object->getVarsKeys();
        $context          = [
            'form_vars' => $objectProperties,
            'ressource' => ucfirst($this->getTable()),
            'initialId' => $this->getParameter('id')
        ];

        if (isset($objectProperties['start'])) {
            $objectProperties['START'] = DateTime::from($objectProperties['start']);
            unset($objectProperties['start']);
        }

        if (isset($objectProperties['end'])) {
            $objectProperties['END'] = DateTime::from($objectProperties['end']);
            unset($objectProperties['end']);
        }

        $objectProperties['idcontext'] = $this->auth->getContext()?->getIdcontext();
        $context['form_vars_copy']     = $objectProperties;
        $context['idcontext']          = $objectProperties['idcontext'];

        $task->setCallable(DeepcopyTask::class);
        $task->setContext($context);
        $task->setSocketChannels(
            [
                'iduser' => $this->auth->getUser()?->getIduser(),
                'page'   => $this->getTable()
            ]
        );
        $task->save();
    }


    /**
     * Use to create entitie with just required fields
     *
     * @return void
     */
    public function createInlineAction(
        NotificationServiceInterface $notifier,
        EntitieFactoryInterface $entitieFactory,
        CsrfServiceInterface $csrf
    ) {
        if (!$csrf->validate($this->request->getContents())) {
            $this->killServer(206);
        }

        $object = $entitieFactory->create($this->getTable(), ['data' => []]);
        $tag    = Utils::secure($this->request->getContents('tag'));
        if (!is_string($object->getRequiredField())) {
            $message = sprintf(_('Inline creation for %s unavailable. Use the \'New\' button.'), $this->getTable());
            $notifier->notify($message, $notifier::WARNING, $this->errorLocation);
            $this->killServer(206);
        }

        $data = [$object->getRequiredField() => $tag];

        //RECORD
        /** @var EntitieInterface $object */
        $object->hydrate($data);
        $id = $this->dbManager->add($object);
        $object->hydrate([$object->getPrimaryKey() => $id]);


        //NOTIFICATION
        $message = sprintf(_('The %s has been recorded.'), $this->getTable());
        $notifier->notify($message, $notifier::SUCCESS, $this->errorLocation);
        $this->response->setData('data', ['id' => $id, 'tag' => $tag]);

        // $this->response->setData('data', $object->getVarsKeys());
        // $this->response->setData('setter', [$object->getOptionHtml(EntitieInterface::ARRAY_FORMAT)]);

        //RETURN HTTP CODE
        $this->response->setCode(201);
    }

    /**
     * Create an entitie
     *
     * @return void
     */
    public function createAction(
        NotificationServiceInterface $notifier,
        EntitieFactoryInterface $entitieFactory,
        FormFactory $formFactory,
        AsyncJob $task
    ) {
        //Check form && get Values
        $form = $formFactory->createFromWidget(ucfirst($this->getTable()), ['method' => 'add']);
        $csrf = $this->getDi()->singleton(CsrfServiceInterface::class);
        $this->validateForm($form, $notifier, $csrf);

        //CREATE OBJECT
        $object = $entitieFactory->create($this->getTable(), ['data' => $this->request->getContents()]);

        //RECORD
        $object = $this->dbManager->add($object);

        //NOTIFICATION
        $message = sprintf(_('The %s has been recorded.'), $this->getTable());
        $notifier->notify($message, $notifier::SUCCESS, $this->errorLocation);

        //RETURN HTTP CODE
        $this->response->setCode(201);

        if ($this->request->getContents('keepOpen') == 1) {
            $this->response->setData('error', true);
        }
    }

    /**
     * Delete an entitie by key
     *
     * @return void
     */
    public function deleteAction(NotificationServiceInterface $notifier)
    {
        $indexKey = $this->request->getContents($this->dbManager->getIndexKey());
        $object   = $this->dbManager->getById($indexKey);

        if (is_bool($object)) {
            $this->killServer(204);
        }

        $lockerUser = $object->getLockerUser();
        if ($lockerUser !== null) {
            $notifier->notify(_("Ressource is locked because it's edited by : ") . $lockerUser, $notifier::WARNING);
            $this->killServer(204);
        }

        //DELETE Object (just flag it)
        $this->getDbManager()->flagDelete($object);

        //NOTIFICATION
        $message = sprintf(_('The %s has been deleted.'), $this->getTable());
        $notifier->notify($message, $notifier::SUCCESS, $this->errorLocation);

        $this->response->setCode(200);
    }

    /**
     * Delete multiples entities by keys
     *
     * @return void
     */
    public function deleteMultipleAction(NotificationServiceInterface $notifier)
    {
        $ids = $this->request->getContents('keys');
        if (!is_array($ids)) {
            $this->killServer(206);
        }

        foreach ($ids as $id) {
            $object = $this->dbManager->getById($id);

            if (!is_bool($object)) {
                $lockerUser = $object->getLockerUser();

                if ($lockerUser !== null) {
                    $notifier->notify(
                        _("Ressource is locked because it's edited by : ") . $lockerUser,
                        $notifier::WARNING
                    );

                    return;
                }

                $deleted = true;
                $this->getDbManager()->flagDelete($object);
            }
        }

        if (!isset($deleted)) {
            $notifier->notify('Error', $notifier::WARNING, $this->errorLocation);
            $this->killServer(204);
        }

        //NOTIFICATION
        $message = sprintf(_('The %s has been deleted.'), $this->getTable());
        $notifier->notify($message, $notifier::SUCCESS, $this->errorLocation);

        $this->response->setCode(200);
    }

    /**
     * Display response content
     *
     * @return HttpResponseInterface
     */
    public function display(): HttpResponseInterface
    {
        return $this->response;
    }

    /**
     * Get all entities
     *
     * @return void
     */
    public function getAllAction()
    {
        $objects = $this->dbManager->getList();
        $datas   = [];
        /** @var EntitieInterface[] $objects */
        foreach ($objects as $object) {
            $datas[] = $this->preShowCallback($object)->getVarsKeys();
        }

        $this->response->setData('data', $datas);
        $this->response->setCode(200);
    }

    /**
     * Get entitie by key
     *
     * @return void
     */
    public function getByIdAction(NotificationServiceInterface $notifier)
    {
        $indexKey = $this->request->getParameters($this->dbManager->getIndexKey());
        /** @var EntitieInterface $object */
        $object = $this->dbManager->getById($indexKey);

        if (is_bool($object)) {
            $this->killServer(204);
        }

        // Pre action
        $object     = $this->preShowCallback($object);
        $lockerUser = $object->getLockerUser();

        if ($lockerUser !== null) {
            $notifier->notify(
                _("Ressource is locked because it's edited by : ") . $lockerUser,
                $notifier::WARNING
            );
            $this->response->setData('entitieLocked', true);
        }

        $this->response->setData('data', $object->getVarsKeys());
        $this->response->setCode(200);
    }

    /**
     * Get entities by parameters
     *
     * @return void
     */
    public function getByParametersAction()
    {
        $conditions = $this->request->getContents();
        if (isset($conditions['formatter'])) {
            $formatter = $conditions['formatter'];
            unset($conditions['formatter']);
        }

        $objects = $this->dbManager->getByParams($conditions);

        $datas = [];
        /** @var EntitieInterface[] $objects */
        foreach ($objects as $object) {
            $object = $this->preShowCallback($object);
            if (isset($formatter)) {
                $formatted = $this->findAndAppliesFormatters($formatter, $object, $conditions);
                if (null !== $formatted) {
                    $datas[] = $formatted;
                }

                continue;
            }

            $datas[] = $object->getVarsKeys();
        }

        $this->response->setData('data', $datas);
        $this->response->setCode(200);
    }

    /**
     * Applies formatters if available
     *
     * @param array|string     $formatter
     * @param EntitieInterface $object
     * @param array            $context
     *
     * @return array|null
     */
    protected function findAndAppliesFormatters(
        array|string $formatter,
        EntitieInterface $object,
        array $context = []
    ): ?array {
        $formatter = (is_string($formatter))
        ? 'core\html\\' . $formatter
        : 'extensions\\' . $formatter[0] . '\\formatters\\' . ($formatter[1] ?? '');

        /** @var FormatterInterface|null $formatter */
        $formatter = $this->getDi()->singleton($formatter);
        if (null != $formatter) {
            return $formatter->useContext($context)->format($object);
        }

        return null;
    }

    /**
     * Get Default components to reload in view
     */
    abstract public function getReloadedComponents(): array;

    /**
     * Get related entities by key
     *
     * @return void
     */
    public function relatedAction()
    {
        $this->response->setData('errorMessage', 'Method `relatedAction` is not implemented in class ' . static::class);
    }

    /**
     * Default method
     *
     * @return void
     */
    public function indexAction()
    {
        $this->response->setData('errorMessage', 'Method `indexAction` is not implemented in class ' . static::class);
    }

    /**
     * Get is that a public controller
     */
    public function isPublic(): bool
    {
        return $this->public;
    }

    /**
     * Kill server with HttpCode
     *
     * @param integer $realHttpCode
     * @param integer $sendHttpCode
     *
     * @return string
     */
    protected function killServer(
        int $realHttpCode = 403,
        int $sendHttpCode = 200
    ): string {
        $this->response->setCode($sendHttpCode);
        $this->response->setData('error', ($realHttpCode > 205));
        $this->response->setData('errorMessage', HttpUtils::HTTP_ERROR($realHttpCode));

        return $this->display()->getData($this->encryptedDatas);
    }

    /**
     *  Render the view
     *
     * @param AssetsServiceInterface    $javascript
     * @param TransportServiceInterface $transport
     *
     * @return void
     */
    public function render(
        AssetsServiceInterface $javascript,
        TransportServiceInterface $transport
    ): self {
        if (!empty($this->getReloadedComponents())) {
            $transport->send(
                $this->getReloadedComponents(),
                [
                    'type'   => $transport::RELOADCOMPONENTS,
                    'iduser' => $this->auth->getUser()?->getIduser()
                ]
            );
        }

        return $this;
    }

    /**
     * Update an entitie by key
     *
     * @return void
     */
    public function updateAction(
        NotificationServiceInterface $notifier,
        FormFactory $formFactory,
        AsyncJob $task
    ) {
        //Check form && get Values
        $form = $formFactory->createFromWidget($this->getTable(), ['method' => 'edit']);

        /** @var CsrfServiceInterface $csrf */
        $csrf = $this->getDi()->singleton(CsrfServiceInterface::class);

        $this->validateForm($form, $notifier, $csrf);

        $object = $this->dbManager->getById($this->request->getContents($this->dbManager->getIndexKey()));

        if (is_bool($object)) {
            $this->response->setCode(204);

            return;
        }

        $lockerUser = $object->getLockerUser();

        if ($lockerUser !== null) {
            $notifier->notify(
                _("Ressource is locked because it's edited by : ") . $lockerUser,
                $notifier::WARNING
            );
            $this->response->setCode(204);

            return;
        }

        $object->hydrate($this->request->getContents());
        $this->dbManager->update($object);

        //NOTI$notifier
        $message = sprintf(_('The %s has been updated.'), $this->getTable());
        $notifier->notify($message, $notifier::SUCCESS, $this->errorLocation);

        $this->response->setData('data', $object->getVarsKeys());
        $this->response->setData('setter', [$object->getPrimaryKey() => $object->getPrimary()]);

        //RETURN HTTP CODE
        $this->response->setCode(201);
    }

    /**
     * Get database service for related table
     *
     * @PhpUnitGen\get("dbManager")
     */
    protected function getDbManager(): MysqlRepository
    {
        return $this->dbManager;
    }

    /**
     * Get called table
     *
     * @PhpUnitGen\get("table")
     */
    protected function getTable(): ?string
    {
        return $this->table;
    }

    /**
     * Execute specific action  before show object
     *
     * @return EntitieInterface|
     */
    protected function preShowCallback(EntitieInterface $object): EntitieInterface
    {
        return $object;
    }

    /**
     * Set called table
     *
     * @PhpUnitGen\set("table")
     *
     * @param string|null $table Called table
     */
    protected function setTable($table): self
    {
        $this->table = $table;

        return $this;
    }

    /**
     * Validate form submission
     */
    protected function validateForm(
        Form $form,
        NotificationServiceInterface $notifier,
        CsrfServiceInterface $csrf
    ) {
        if (!$form->isSubmitted($this->request->getContents())) {
            $this->killServer(204);
        }

        $form->pre($this->request->getContents());

        if (!$form->isValid($this->request->getContents(), $csrf)) {
            $notifier->notify($form->getErrors(), $notifier::DANGER, $this->errorLocation);
            $this->killServer(206);
        }

        $this->request->contents = array_merge($this->request->getContents(), $form->getValues());
        $this->processExternalFields($form);
    }

    /**
     * @todo make checks
     *
     * @param Form $form
     *
     * @return void
     */
    protected function processExternalFields(Form $form)
    {
        $contents = $form->getValues(internal:false);

        $datas = [];
        foreach ($contents as $key => $value) {
            $elements                          = explode('__', $key);
            $datas[$elements[0]]               = $datas[$elements[0]] ?? [];
            $datas[$elements[0]][$elements[1]] = $value;
        }

        foreach ($datas as $table => $properties) {
            $repository = $this->repositoryFactory->table($table);

            $object = $repository->getById($properties['id' . $table] ?? -1);
            if ($object instanceof EntitieInterface) {
                $object->hydrate($properties);
                $repository->update($object);

                $this->request->addContent("id$table", $object->getPrimary());

                continue;
            }

            $object = Entitie::create($table, $properties);
            $this->request->addContent("id$table", $repository->add($object));
        }
    }

    /**
     * Get the value of response
     *
     * @PhpUnitGen\get("response")
     *
     * @return HttpResponseInterface
     */
    public function getResponse(): HttpResponseInterface
    {
        return $this->response;
    }
}
