<?php

namespace core\events;

/**
 * Interface EventManagerInterface
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
interface EventManagerInterface
{
    /**
     * Detach a listener
     *
     * @return static
     */
    public function detach(string $eventName);

    /**
     * Disable event manager
     *
     * @return void
     */
    public function disable();

    /**
     * Emit an event
     *
     * @return array
     */
    public function emit(string $eventName, array &$data = []): array;

    /**
     * Enable Event manager
     *
     * @return void
     */
    public function enable();

    /**
     * Register a listener
     *
     * @return static
     */
    public function on(string $eventName, array | callable $callback, ?array $dependencies = null);


    /**
     * Register class listeners
     *
     * @return void
     */
    public function registerListerners(array $listeners);
}
