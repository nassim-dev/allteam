<?php

namespace core\events;

use core\DI\DiProvider;

/**
 * Abstract class AbstractListener
 *
 * Description
 *
 * @category  Description
 * @version   0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
abstract class AbstractListener implements ListenerInterface
{
    use DiProvider;
}
