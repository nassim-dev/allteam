<?php

namespace core\events;

use app\tasks\EventListenerTask;
use core\cache\CacheServiceInterface;
use core\cache\CacheUpdaterTrait;
use core\DI\DiProvider;
use core\tasks\AsyncJob;
use MJS\TopSort\Implementations\StringSort;
use ReflectionAttribute;
use ReflectionClass;
use ReflectionMethod;

/**
 * Class EventManager
 *
 * Manage Callback events
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class EventManager implements EventManagerInterface
{
    use CacheUpdaterTrait;
    use DiProvider;

    /**
     * Disable all events
     *
     * @var boolean
     */
    public $disableEvents = false;

    /**
     * Defered all task, only for class not for callable
     *
     * @var boolean
     */
    public $AsyncJob = false;

    /**
     * Listeners Recorded
     */
    private array $_listeners = [];

    /**
     * Listeners order
     */
    private array $_dependencies = [];

    /**
     * Registred listeners
     */
    private array $mapping = [];

    public function __construct(private CacheServiceInterface $cache)
    {
        $this->_fetchPropsFromCache(['mapping', '_dependencies']);
    }

    public function __destruct()
    {
        $this->_cacheMultiplesProps(['mapping', '_dependencies']);
    }

    /**
     * Detach a listener
     *
     * @return static
     */
    public function detach(string $eventName): \core\events\EventManager
    {
        return $this->_deRegisterEvent($eventName);
    }

    /**
     * Disable event manager
     */
    public function disable()
    {
        $this->disableEvents = true;
    }

    /**
     * Emit an event
     *
     * @return array
     */
    public function emit(string $eventName, array &$data = []): array
    {
        $listeners = $this->getListenerForEvent($eventName);
        if (null === $listeners || $this->disableEvents) {
            return $data;
        }

        $this->_ordonnateDependencies($listeners, $eventName);
        $this->_executeListeners($eventName, $data);

        return $data;
    }

    /**
     * Undocumented function
     */
    private function _ordonnateDependencies(array $listeners, string $eventName)
    {
        $orderedListenerList = [];
        if (!isset($this->_dependencies[$eventName])) {
            $sorter = new StringSort();
            foreach ($listeners as $id => $listener) {
                if (null !== $listener['dependencies']) {
                    $sorter = $this->_parseDependencies($eventName, $listener['dependencies'], $id, $sorter);
                }
            }

            $this->_dependencies[$eventName] = $sorter->sort();
            $orderedListenerList[$eventName] = [];

            foreach ($this->_dependencies[$eventName] as $listener) {
                if (isset($this->_listeners[$eventName][$listener])) {
                    $orderedListenerList[] = $this->_listeners[$eventName][$listener];
                }
            }

            foreach ($this->_listeners[$eventName] as $listener) {
                if (is_callable($listener['callback'])) {
                    $orderedListenerList[$eventName] = $listener;
                }
            }



            $this->_listeners[$eventName] = $orderedListenerList;
        }
    }

    /**
     * Execute listeners for eventName
     */
    private function _executeListeners(string $eventName, array &$data = [])
    {
        if (!isset($this->_listeners[$eventName])) {
            return;
        }

        if ($this->AsyncJob) {
            /** @var AsyncJob $task */
            $task = $this->getDi()->singleton(AsyncJob::class, ['id' => EventListenerTask::class]);
            $task->setCallable(EventListenerTask::class);
        }

        foreach ($this->_listeners[$eventName] as $listener) {
            if (!isset($listener['callback'])) {
                continue;
            }

            if (is_array($listener['callback'])) {
                if ($this->AsyncJob) {
                    $listener['args'] = $data;
                    $task->setContext($listener)
                        ->save();

                    continue;
                }

                $this->getDi()->call($listener['callback']['class'], $listener['callback']['method'], ['args' => $data]);

                continue;
            }

            call_user_func($listener['callback'], $data);
        }
    }

    /**
     * Enable Event manager
     */
    public function enable()
    {
        $this->disableEvents = false;
    }

    /**
     * Return event related listeners
     */
    public function getListenerForEvent(string $event): ?array
    {
        return $this->_listeners[$event] ?? null;
    }

    /**
     * Get value of _listeners
     *
     * @PhpUnitGen\get("_listeners")
     */
    public function getListeners(): array
    {
        return $this->_listeners;
    }

    /**
     * Register a listener
     *
     * @return static
     */
    public function on(string $eventName, array | callable $callback, ?array $dependencies = null)
    {
        if (!$this->_checkCallbackValidity($callback)) {
            return false;
        }

        return $this->_registerEvent($eventName, $callback, $dependencies);
    }

    /**
     * @return static
     */
    private function _checkCallbackValidity(array $callback)
    {
        if (!isset($callback['class']) || !is_string($callback['class'])) {
            return false;
        }

        if (!isset($callback['method']) || !is_string($callback['method'])) {
            return false;
        }

        return true;
    }

    private function _deRegisterEvent(string $eventName): self
    {
        if (isset($this->_listeners[$eventName])) {
            unset($this->_listeners[$eventName]);
        }

        return $this;
    }

    /**
     * @param callable $callback
     */
    private function _registerEvent(string $eventName, callable | array $callback, ?array $dependencies): self
    {
        $eventName = trim($eventName);
        if (!isset($this->_listeners[$eventName])) {
            $this->_listeners[$eventName] = [];
        }

        $event = [
            'event_name'   => $eventName,
            'callback'     => $callback,
            'dependencies' => $dependencies,
        ];

        $id = (is_array($callback)) ? $event['callback']['class'] . '::' . $event['callback']['method'] : uniqid();

        $this->_listeners[$eventName][$id] = $event;

        return $this;
    }

    /**
     * Parse listener dependencies
     */
    private function _parseDependencies(string $eventName, ?array $dependencies, string $id, StringSort $sorter): StringSort
    {
        if (null === $dependencies) {
            return $sorter;
        }

        $sorter->add($id, $dependencies);

        foreach ($dependencies as $dependencie) {
            if (isset($this->_listeners[$eventName][$dependencie])) {
                $this->_parseDependencies(
                    $eventName,
                    $this->_listeners[$eventName][$dependencie]['dependencies'],
                    $dependencie,
                    $sorter
                );
            }
        }

        return $sorter;
    }

    /**
     * Register listener classes
     *
     * @return void
     */
    public function registerListerners(array $listeners)
    {
        foreach ($listeners as $listener) {
            if (!isset($this->mapping[$listener])) {
                $reflection = new ReflectionClass($listener);
                foreach ($reflection->getMethods() as $method) {
                    $attributes = $method->getAttributes(ListenerAttribute::class);
                    foreach ($attributes as $attributeReflected) {
                        $this->_registerFromReflection($listener, $attributeReflected, $method, $reflection);
                    }
                }

                continue;
            }

            $this->_registerFromCache($listener);
        }
    }

    /**
     * Register listeners from cache
     *
     * @return void
     */
    private function _registerFromCache(string $listener)
    {
        foreach ($this->mapping[$listener] as $events) {
            foreach ($events as $event) {
                $this->on(
                    $event['event'],
                    [
                        'class'  => $event['class'],
                        'method' => $event['method']
                    ],
                    $event['dependencies']
                );
            }
        }
    }

    /**
     * Register listeners from reflection elements
     *
     * @return void
     */
    private function _registerFromReflection(
        string $listener,
        ReflectionAttribute $attributeReflected,
        ReflectionMethod $method,
        ReflectionClass $reflection
    ) {
        /**
         * @var ListenerAttribute $attribute
         */
        $attribute = $attributeReflected->newInstance();
        $this->on(
            $attribute->getEvent(),
            [
                'class'  => $reflection->getName(),
                'method' => $method->getName()
            ],
            $attribute->getDependencies()
        );

        $this->mapping[$listener][$method->getName()][$attribute->getEvent()] = [
            'event'        => $attribute->getEvent(),
            'class'        => $reflection->getName(),
            'method'       => $method->getName(),
            'dependencies' => $attribute->getDependencies()
        ];
    }
}
