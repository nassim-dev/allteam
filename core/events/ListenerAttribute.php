<?php

namespace core\events;

/**
 * Class Listener
 *
 * Description
 *
 * @category  Description
 * @version   0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
#[\Attribute(\Attribute::TARGET_METHOD | \Attribute::IS_REPEATABLE)]
class ListenerAttribute
{
    /**
     * @param array|null $dependencies Execute theses methods before [ClassName => MethodName]
     */
    public function __construct(private string $event, private ?array $dependencies = null)
    {
    }

    /**
     * Get the value of event
     *
     * @PhpUnitGen\get("event")
     */
    public function getEvent(): string
    {
        return $this->event;
    }

    /**
     * Get the value of dependencies
     */
    public function getFormatedDependencies(): ?array
    {
        if (null === $this->dependencies) {
            return null;
        }

        $return = [];
        foreach ($this->dependencies as $class => $method) {
            $return[] = $class . '::' . $method;
        }

        return $return;
    }

    /**
     * Get the value of dependencies
     *
     * @PhpUnitGen\get("dependencies")
     */
    public function getDependencies(): ?array
    {
        return $this->dependencies;
    }
}
