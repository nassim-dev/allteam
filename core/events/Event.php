<?php namespace core\events;

/**
 * Class Event
 *
 *  @method static string detach(string $eventName)
 *  @method static string disable()
 *  @method static array emit(string $eventName, array $data)
 *  @method static string enable()
 *  @method static string on(string $eventName, callable|array $callback, array $dependencies)
 *  @method static string registerListerners(array $listeners)
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class Event extends \core\facade\HelperFacade
{
    protected static ?string $serviceInterface = 'core\events\EventManagerInterface';
}
