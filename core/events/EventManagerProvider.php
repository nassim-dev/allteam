<?php

namespace core\events;

use core\DI\DiProvider;

/**
 * Trait EventManagerProvider
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait EventManagerProvider
{
    use DiProvider;

    /**
     * EventManagerInterface implémentation
     *
     * @var EventManagerInterface
     */
    private $eventManager;

    /**
     * Return EventManagerInterface implémetation
     */
    public function getEventManager(): EventManagerInterface
    {
        return $this->eventManager ??= $this->getDi()->singleton(EventManagerInterface::class);
    }
}
