<?php

namespace core\containers;

/**
 * Interface ContainerInterface
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
interface ContainerInterface
{
    public function detachWithIdentifier(string $identifier);

    /**
     * Return element filtered by class
     */
    public function filterBy(string $class): ?array;

    /**
     * @param mixed $fieldId
     */
    public function findObject($fieldId): mixed;

    public function containsIdentifier(string $identifier): bool;

    /**
     * @inheritDoc
     */
    public function offsetGet($object);

    /**
     * Find an element if callable return true
     *
     * @param string $function    (isDifferent , isSup, isEqual, isInf)
     * @param mixed  $testedValue
     */
    public function findIf(string $propertieName, string $function, $testedValue): mixed;

    /**
     * Return filtered elememen, with conditions
     *
     * @param $conditions
     */
    public function findByParameters(array $conditions): ?array;

    public function toArray(): array;
}
