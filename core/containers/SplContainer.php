<?php

namespace core\containers;

use core\DI\DiProvider;
use core\utils\GeneratorProvider;
use core\utils\Utils;
use SplObjectStorage;

/**
 * Class SplContainer
 *
 * Description
 *
 * @category  html_component
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
abstract class SplContainer extends SplObjectStorage
{
    use GeneratorProvider;
    use DiProvider;

    protected array $map = [];

    /**
     * @param mixed[]|null $entries
     */
    public function __construct(?array $entries = null)
    {
        if (null !== $entries) {
            foreach ($entries as $key => $entrie) {
                $this->attach($entrie, $key);
            }
        }
    }

    /**
     * @return mixed
     */
    public function __invoke()
    {
        return $this->findObject(func_get_arg(1));
    }

    public function attach($object, $info = null): void
    {
        parent::attach($object, $info);
        if ($info !== null) {
            $this->map[$info] = $object;
        }
    }

    public function detachWithIdentifier(string $identifier)
    {
        $object = $this->findObject($identifier);
        if (null !== $object) {
            $this->detach($object);
        }
    }

    /**
     * Return element filtered by class
     */
    public function filterBy(string $class): ?array
    {
        $array = $this->toArray();
        $array = array_filter($array, fn ($element): bool => $element::class == $class
            || get_parent_class($element) === $class
            || in_array($class, class_implements($element)));

        return (empty($array)) ? null : $array;
    }

    /**
     * @param mixed $fieldId
     */
    public function findObject($fieldId): mixed
    {
        if (isset($this->map[$fieldId])) {
            return $this->map[$fieldId];
        }

        foreach ($this->getGenerator($this) as $object) {
            if ($fieldId === $this->getInfo()) {
                $this->map[$fieldId] = $object;

                return $object;
            }
        }

        return null;
    }

    public function containsIdentifier(string $identifier): bool
    {
        return ($this->findObject($identifier) !== null);
    }

    abstract protected function getPropertie(mixed $object, string $propertieName);


    /**
     * Find an element if callable return true
     *
     * @param string $function    (isDifferent , isSup, isEqual, isInf)
     * @param mixed  $testedValue
     */
    public function findIf(string $propertieName, string $function, $testedValue): mixed
    {
        foreach ($this as $object) {
            if (call_user_func_array([Utils::class, $function], [$this->getPropertie($object, $propertieName), $testedValue])) {
                return $object;
            }
        }

        return null;
    }

    /**
     * Return filtered elememen, with conditions
     *
     * @param $conditions
     */
    public function findByParameters(array $conditions): ?array
    {
        $that  = $this;
        $array = array_filter($this->toArray(), function ($element) use ($that, $conditions): bool {
            $return = true;
            foreach ($conditions as $propertie => $values) {
                if (!$return) {
                    continue;
                }

                $return = (in_array($that->getPropertie($element, $propertie), $values) && $return);
            }

            return $return;
        });

        return (empty($array)) ? null : $array;
    }

    public function toArray(): array
    {
        $return = [];
        foreach ($this as $element) {
            $return[] = $element;
        }

        return $return;
    }

    public function __toString(): string
    {
        /** @var SerializerServiceInterface $serializer */
        $serializer = $this->getDi()->singleton(SerializerServiceInterface::class);

        return $serializer->serialize($this);
    }

    public static function rebuild(string $serialized): static
    {
        /** @var SerializerServiceInterface $serializer */
        $serializer = DI::getContainer()->singleton(SerializerServiceInterface::class);

        return $serializer->unserialize($serialized);
    }
}
