<?php

namespace core\notifications;

use core\DI\DiProvider;
use core\session\SessionInterface;
use core\transport\TransportServiceInterface;

/**
 * Class Notificator
 *
 * Gestion des notifications renvoyés à l'utilisateur
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class Notificator implements NotificationServiceInterface
{
    use DiProvider;

    /**
     * Recorded Errors
     *
     * Prevent for multiple same errors
     */
    private static array $_existingErrors = [];

    /**
     * True if transport object are build
     *
     * @var boolean
     */
    private $isbuild = false;

    /**
     * Constructor
     */
    public function __construct(
        private SessionInterface $session,
        private array $transports = [],
    ) {
    }

    /**
     * Clean $_SESSIONS errors
     */
    public function clean()
    {
        $this->session->getContainer(self::SESSION_PREFIX)->clear();
    }

    /**
     * Attach transport method
     *
     * @return void
     */
    public function attachTransport(string|TransportServiceInterface $transport)
    {
        $this->transports[] = $transport;
        $this->isbuild      = false;
    }

    /**
     *  Add an error
     *
     * @param string       $error    Message to send
     * @param string       $type     Alert css class
     * @param boolean      $callback Page to redirect
     * @param null|integer $iduser   If in cron context, Iduser that will receive the error
     * @param bool         $toSocket true if send to web socket
     *
     * @return void
     */
    public function notify(
        string $error,
        string $type = self::DANGER,
        ?string $callback = null,
        ?int $iduser = null,
        bool $toTransport = self::TO_TRANSPORT
    ) {
        $newError = [
            'message'  => $error,
            'type'     => $type,
            'callback' => $callback ?? false
        ];

        if ($toTransport) {
            if (!$this->isbuild) {
                $transports = [];
                foreach ($this->transports as $transport) {
                    $transports[] = (is_string($transport)) ? $this->getDi()->singleton($transport) : $transport;
                }

                $this->transports = $transports;
                $this->isbuild    = true;
            }

            foreach ($this->transports as $transport) {
                /** @var TransportServiceInterface $transport */
                $transport->send($newError, ['type' => $transport::ERROR, 'iduser' => $iduser]);
            }
        }

        if (!$toTransport && !in_array($error, self::$_existingErrors)) {
            self::$_existingErrors[] = $error;

            $sessionContainer = $this->session->getContainer(self::SESSION_PREFIX);
            $sessionContainer->push('MODAL_ERRORS', $newError);
        }
    }
}
