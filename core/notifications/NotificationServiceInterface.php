<?php

namespace core\notifications;

use core\transport\TransportServiceInterface;

/**
 * Class NotificationsServiceInterface
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
interface NotificationServiceInterface
{
    public const DANGER            = 'error';
    public const INFO              = 'info';
    public const SUCCESS           = 'success';
    public const WARNING           = 'warning';
    public const TO_TRANSPORT      = true;
    public const WITHOUT_TRANSPORT = false;
    public const SESSION_PREFIX    = 'NOTIFICATIONS';

    /**
     * Clean $_SESSIONS errors
     */
    public function clean();

    /**
     *  Add an error
     *
     * @param string       $error    Message to send
     * @param string       $type     Alert css class
     * @param null|string  $callback Page to redirect
     * @param null|integer $iduser   If in cron context, Iduser that will receive the error
     * @param bool         $toSocket true if send to web socket
     *
     * @return void
     */
    public function notify(
        string $error,
        string $type,
        ?string $callback = null,
        ?int $iduser = null,
        bool $toTransport = self::TO_TRANSPORT
    );

    /**
     * Attach a transport object
     *
     * @return void
     */
    public function attachTransport(string|TransportServiceInterface $transport);
}
