<?php

namespace core\notifications;

use core\DI\DiProvider;

/**
 * Trait NotificationProvider
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait NotificationProvider
{
    use DiProvider;

    /**
     * NotificationServiceInterface implémentation
     *
     * @var NotificationServiceInterface
     */
    private $notification;

    /**
     * Return NotificationServiceInterface implémetation
     */
    public function getNotification(): NotificationServiceInterface
    {
        return $this->notification ??= $this->getDi()->singleton(NotificationServiceInterface::class);
    }
}
