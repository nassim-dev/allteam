<?php namespace core\notifications;

/**
 * Class Notify
 *
 *  @method static string clean()
 *  @method static string notify(string $error, string $type, string $callback, int $iduser, bool $toTransport)
 *  @method static string attachTransport(core\transport\TransportServiceInterface|string $transport)
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class Notify extends \core\facade\HelperFacade
{
    protected static ?string $serviceInterface = 'core\notifications\NotificationServiceInterface';
}
