<?php

namespace core\states;

/**
 * Class AbstractState
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
abstract class AbstractState implements StateInterface
{
    private static array $_instances = [];

    private function __construct(private string $specific)
    {
    }

    /**
     * Precise the state
     *
     * @param string|null $specific pre|main|post default is main
     *
     * @return static
     */
    public static function on(?string $specific = null)
    {
        $id = $specific ?? 'main';
        if (!isset(self::$_instances[$id])) {
            self::$_instances[$id] = new static($id);
        }

        return self::$_instances[$id];
    }

    /**
     * Resturn state Name
     */
    abstract public function get(): string;

    /**
     * Get the value of specific
     *
     * @PhpUnitGen\get("specific")
     */
    public function getSpecific(): string
    {
        return $this->specific;
    }
}
