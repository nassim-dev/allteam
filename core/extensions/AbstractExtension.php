<?php

namespace core\extensions;

use core\app\ApplicationServiceInterface;
use core\cache\CacheServiceInterface;
use core\cache\CacheUpdaterTrait;
use core\config\Conf;
use core\DI\DiProvider;
use core\entities\EntitieAttribute;
use core\routing\MenuRegisterTrait;
use core\utils\ClassFinder;
use Nette\Neon\Neon;
use ReflectionClass;

/**
 * Class AbstractExtension
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
abstract class AbstractExtension implements ExtensionInterface
{
    use CacheUpdaterTrait;
    use MenuRegisterTrait;
    use DiProvider;

    /**
     * Associated listeners
     */
    private ?array $_listeners = null;

    /**
     * Associated controllers
     */
    private ?array $_controllers = null;

    /**
     * Associated entities
     */
    private ?array $_entities = null;

    /**
     * Associated cron
     */
    private ?array $_cron = null;

    /**
     * Associated controllers
     */
    private ?array $_models = null;

    /**
     * Extensio nemespace
     */
    private ?string $_namespace = null;

    /**
     * Is extensions installed ?
     */
    private bool $_installed = false;

    /**
     * Is extensions installed ?
     */
    private bool $_initialized = false;

    /**
     * Is extension enabled ?
     */
    private bool $_enabled = false;

    /**
     * Config from Attribute
     */
    private ?\core\extensions\ExtensionConfig $config = null;

    public function __construct(protected CacheServiceInterface $cache)
    {
        $basedir = BASE_DIR . str_replace('\\', '//', $this->getNamespace());
        $class   = explode('\\', static::class);
        bindtextdomain(str_replace('Extension', '', end($class)), $basedir);

        $filename = $basedir . '/manifest.neon';
        if (file_exists($filename)) {
            $config = Neon::decodeFile('nette.safe://' . $filename);
            foreach ($config as $propertie => $value) {
                $this->{$propertie}                = $value;
                $this->__objectsCached[$propertie] = $value;
            }

            $this->_cacheMultiplesProps(['_listeners', '_controllers', '_entities', '_cron', '_models', '_namespace', '_routes', '_config']);

            return;
        }

        //$this->cache->invalidate(md5(static::class) . '._routes');
        $this->_fetchPropsFromCache(['_listeners', '_controllers', '_entities', '_cron', '_models', '_namespace', '_routes', '_config']);
    }

    public function getBaseDir(): string
    {
        return BASE_DIR . str_replace('\\', '/', $this->getNamespace());
    }

    public function __destruct()
    {
        if (Conf::find('ENVIRONNEMENT') === 'development') {
            //$this->compile();
        }

        $this->_cacheMultiplesProps(['_listeners', '_controllers', '_entities', '_cron', '_models', '_namespace', '_routes', '_config']);
    }

    protected function getIcon()
    {
        return $this->getConfig()->icon;
    }

    public function getEnvFile(): string
    {
        return  $this->getBaseDir() . '/config/env.neon';
    }

    /**
     * Boot extension
     *
     * @return void
     */
    abstract public function boot();

    /**
     * Build extension
     *
     * @return void
     */
    abstract public function build();

    /**
     * Destroy extension
     *
     * @return void
     */
    abstract public function destroy();

    /**
     * Install extension
     *
     * @return void
     */
    public function install()
    {
    }

    /**
     * Uninstall extension
     *
     * @return void
     */
    public function uninstall()
    {
    }

    /**
     * Compile extension
     *
     * @return void
     */
    public function compile()
    {
        $manifest = BASE_DIR . str_replace('\\', '//', $this->getNamespace()) . '/manifest.neon';
        if (file_exists($manifest)) {
            unlink($manifest);
        }


        $config = [
            '_listeners'   => $this->getListeners(),
            '_controllers' => $this->getControllers(),
            '_entities'    => $this->getEntities(),
            '_models'      => $this->getModels(),
            '_cron'        => $this->getCron(),
            '_namespace'   => $this->getNamespace(),
            '_routes'      => $this->getRoutes(),
            '_config'      => $this->getConfig()
        ];

        file_put_contents('nette.safe://' . $manifest, Neon::encode($config, blockMode:true));
    }

    /**
     * Initialize extension
     *
     * @return void
     */
    public function init(ApplicationServiceInterface $app)
    {
        if (method_exists($app, 'getController') && !$app->getController()?->isPublic()) {
            $this->registerMenuActions($this->getName(), $this->getIcon());
        }

        //Register locales
        $localesDir = BASE_DIR . str_replace('\\', '//', $this->getNamespace()) . '/locales';
        bindtextdomain($this->getName(), $localesDir);
        bind_textdomain_codeset($this->getName(), 'UTF-8');
    }

    /**
     * Get extension Name
     */
    final public function getName(): string
    {
        return $this->getConfig()->name;
    }

    private function getNamespace(): string
    {
        if (is_string($this->_namespace)) {
            return $this->_namespace;
        }

        $class = static::class;

        return $this->_namespace = substr($class, 0, strrpos($class, '\\'));
    }

    /**
     * Get associated ss
     *
     * @PhpUnitGen\get("listener")
     */
    final public function getListeners(): ?array
    {
        if (is_array($this->_listeners)) {
            return $this->_listeners;
        }

        /** @var ClassFinder $finder */
        $finder = $this->getDi()->singleton(ClassFinder::class);

        return $this->_listeners = $finder->findClasses($this->getNamespace() . '\event', ClassFinder::RECURSIVE_MODE) ?? [];
    }

    /**
     * Get associated controllers
     *
     * @PhpUnitGen\get("controllers")
     */
    final public function getControllers(): ?array
    {
        if (is_array($this->_controllers)) {
            return $this->_controllers;
        }

        /** @var ClassFinder $finder */
        $finder = $this->getDi()->singleton(ClassFinder::class);

        return $this->_controllers = $finder->findClasses($this->getNamespace() . '\controller', ClassFinder::RECURSIVE_MODE) ?? [];
    }

    /**
     * Get associated models
     *
     * @PhpUnitGen\get("models")
     */
    final public function getModels(): ?array
    {
        if (is_array($this->_models)) {
            return $this->_models;
        }

        /** @var ClassFinder $finder */
        $finder = $this->getDi()->singleton(ClassFinder::class);

        return $this->_models = $finder->findClasses($this->getNamespace() . '\model', ClassFinder::RECURSIVE_MODE) ?? [];
    }

    /**
     * Get associated entities
     *
     * @PhpUnitGen\get("entities")
     */
    final public function getEntities(): ?array
    {
        if (is_array($this->_entities)) {
            return $this->_entities;
        }

        /** @var ClassFinder $finder */
        $finder          = $this->getDi()->singleton(ClassFinder::class);
        $classes         = $finder->findClasses($this->getNamespace() . '\entities', ClassFinder::RECURSIVE_MODE) ?? [];
        $this->_entities = [];

        foreach ($classes as $class) {
            $reflection = new ReflectionClass($class);
            $attributes = $reflection->getAttributes(EntitieAttribute::class);
            if (empty($attributes)) {
                continue;
            }

            $this->_entities[] = $class;
        }

        return $this->_entities;
    }

    /**
     * Get associated cron
     *
     * @PhpUnitGen\get("cron")
     */
    final public function getCron(): ?array
    {
        if (is_array($this->_cron)) {
            return $this->_cron;
        }

        /** @var ClassFinder $finder */
        $finder      = $this->getDi()->singleton(ClassFinder::class);
        $classes     = $finder->findClasses($this->getNamespace() . '\cron', ClassFinder::RECURSIVE_MODE) ?? [];
        $this->_cron = [];

        foreach ($classes as $class) {
            $this->_cron[] = $class;
        }

        return $this->_cron;
    }

    /**
     * Get associated widget namespaces to register in factories
     */
    final public function getWidgetNamespaces(): string
    {
        return $this->getNamespace() . '\widgets';
    }

    /**
     * Execute cron tasks
     *
     * @return void
     */
    final public function executeCronTasks(string $context)
    {
        foreach ($this->_cron as $class) {
            $this->getDi()->call($class, $context);
        }
    }

    /**
     * Get is extension enabled ?
     *
     * @PhpUnitGen\get("isEnabled")
     */
    final public function isEnabled(): bool
    {
        return $this->_enabled;
    }

    /**
     * Get is extensions installed ?
     *
     * @PhpUnitGen\get("_installed")
     */
    final public function isInstalled(): bool
    {
        return $this->_installed;
    }

    /**
     * Get is extensions initialized ?
     *
     * @PhpUnitGen\get("_initialized")
     */
    final public function isInitialized(): bool
    {
        return $this->_initialized;
    }

    /**
     * Set is extensions installed ?
     */
    final public function validateInstallation(): self
    {
        $this->_installed = true;

        return $this;
    }

    /**
     * Set is extension enabled ?
     */
    final public function enable(): self
    {
        $this->_enabled = true;

        return $this;
    }

    /**
     * Get config from Attribute
     *
     * @PhpUnitGen\get("config")
     */
    public function getConfig(): ExtensionConfig
    {
        if ($this->config === null) {
            $reflection = new ReflectionClass($this);
            $attributes = $reflection->getAttributes(ExtensionConfig::class);
            foreach ($attributes as $attribute) {
                $this->config = $attribute->newInstance();
            }
        }

        return $this->config;
    }
}
