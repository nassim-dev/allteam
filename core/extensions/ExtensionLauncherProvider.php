<?php

namespace core\extensions;

use core\DI\DiProvider;

/**
 * Trait ExtensionLauncherProvider
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait ExtensionLauncherProvider
{
    use DiProvider;

    /**
     * ExtensionLauncherServiceInterface implémentation
     *
     * @var ExtensionLauncherServiceInterface
     */
    private $extensionLauncher;

    /**
     * Return ExtensionLauncherServiceInterface implémetation
     */
    public function getExtensionLauncher(): ExtensionLauncherServiceInterface
    {
        return $this->extensionLauncher ??= $this->getDi()->singleton(ExtensionLauncherServiceInterface::class);
    }
}
