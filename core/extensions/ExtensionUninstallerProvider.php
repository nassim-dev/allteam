<?php

namespace core\extensions;

use core\DI\DiProvider;

/**
 * Trait ExtensionUninstallerProvider
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait ExtensionUninstallerProvider
{
    use DiProvider;

    /**
     * ExtensionUninstallerServiceInterface implémentation
     *
     * @var ExtensionUninstallerServiceInterface
     */
    private $extensionUninstaller;

    /**
     * Return ExtensionUninstallerServiceInterface implémetation
     */
    public function getExtensionUninstaller(): ExtensionUninstallerServiceInterface
    {
        return $this->extensionUninstaller ??= $this->getDi()->singleton(ExtensionUninstallerServiceInterface::class);
    }
}
