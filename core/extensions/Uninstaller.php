<?php

namespace core\extensions;

use core\DI\DiProvider;
use core\tasks\AsyncJob;
use core\tasks\UninstallExtensionTask;

/**
 * Class Uninstaller
 *
 * Extensions uninstaller
 *
 * @category Extensions uninstaller
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Uninstaller implements ExtensionUninstallerServiceInterface
{
    use DiProvider;

    public function __construct(private ExtensionLauncherServiceInterface $launcher)
    {
    }

    public function destroy(ExtensionInterface $extension)
    {
        $this->launcher->disable($extension);

        if ($extension->isInstalled()) {
            $extension->destroy();
        }

        /** @var AsyncJob $task */
        $task = $this->getDi()->singleton(AsyncJob::class, ['id' => UninstallExtensionTask::class]);
        $task->setCallable(UninstallExtensionTask::class);
        $task->setContext(['extension' => $extension::class]);
        $task->save();

        $this->launcher->validateUninstallation($extension);
    }
}
