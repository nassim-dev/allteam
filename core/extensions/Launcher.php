<?php

namespace core\extensions;

use Clockwork\Support\Lumen\Controller;
use core\cache\CacheServiceInterface;
use core\cache\CacheUpdaterTrait;
use core\config\Conf;
use core\config\ConfigException;
use core\DI\DiProvider;
use core\entities\EntitieAttribute;
use core\logger\extensions\TracyExtensionsPanel;
use core\tasks\AsyncJob;
use core\tasks\InstallExtensionTask;
use MJS\TopSort\Implementations\StringSort;
use Nette\Neon\Decoder;
use ReflectionAttribute;
use ReflectionClass;
use Tracy\Debugger;

/**
 * Class Launcher
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class Launcher implements ExtensionLauncherServiceInterface
{
    use CacheUpdaterTrait;
    use DiProvider;

    public const CONFIG_FILE = 'extensions/manifest.neon';

    /**
     * Extension instance
     *
     * @var ExtensionInterface[]
     */
    private ?array $_extensions = null;

    /**
     * Confg
     */
    private ?array $_manifest = null;

    /**
     * ControllerMapping
     */
    private ?array $_controllers = null;

    /**
     * ControllerMapping
     */
    private ?array $_models = null;

    /**
     * Listener to register
     */
    private ?array $_listeners = null;

    /**
     * Entitie to register
     */
    private ?array $_entities = null;

    /**
     * Widget namespaces
     */
    private ?array $_widgetNamespace = null;

    /**
     * Boot sequence
     */
    private ?array $_bootSequence = null;

    public function __construct(
        protected CacheServiceInterface $cache
    ) {
        $this->_fetchPropsFromCache(
            [
                '_manifest',
                '_listeners',
                '_controllers',
                '_models',
                '_bootSequence',
                '_entities',
                '_widgetNamespace'
            ]
        );

        if (!is_array($this->_listeners)) {
            $this->_listeners = [];
        }

        if (!is_array($this->_controllers)) {
            $this->_controllers = [];
        }

        if (!is_array($this->_models)) {
            $this->_models = [];
        }

        if (!is_array($this->_entities)) {
            $this->_entities = [];
        }

        if (!is_array($this->_widgetNamespace)) {
            $this->_widgetNamespace = [];
        }


        $this->_decodeManifest(BASE_DIR . self::CONFIG_FILE);
        $this->_buildBootSequence(new StringSort());
        $this->_bootExtensions();

        if (Conf::find('ENVIRONNEMENT') === 'development' && CONTEXT === 'APP' && !isset($_SERVER['x-requested-with'])) {
            Debugger::getBar()->addPanel(new TracyExtensionsPanel($this));
        }
    }

    public function __destruct()
    {
        $this->_cacheMultiplesProps(
            [
                '_manifest',
                '_listeners',
                '_controllers',
                '_models',
                '_bootSequence',
                '_entities',
                '_widgetNamespace'
            ]
        );
    }

    /**
     * Get controllerMapping
     *
     * @PhpUnitGen\get("controllers")
     */
    public function getControllers(): array
    {
        return $this->_controllers ?? [];
    }

    /**
     * Add new Controller to register
     */
    public function addController(string $baseUrl, string $namespace): self
    {
        $this->_controllers[$baseUrl] = $namespace;

        return $this;
    }

    /**
     * Add new Model to register
     */
    public function addModel(string $namespace): self
    {
        $this->_models[$namespace] = $namespace;

        return $this;
    }


    /**
     * Add new listener to register
     */
    public function addListener(string $listenerClass): self
    {
        $this->_listeners[$listenerClass] = $listenerClass;

        return $this;
    }

    /**
     * Add new entitie to register
     */
    public function addEntitie(string $entitieClass): self
    {
        $reflection = new ReflectionClass($entitieClass);

        /** @var ReflectionAttribute[] $attributes */
        $attributes = $reflection->getAttributes(EntitieAttribute::class);
        foreach ($attributes as $reflectedAttribute) {
            /** @var EntitieAttribute $attribute */
            $attribute                              = $reflectedAttribute->newInstance();
            $this->_entities[$attribute->tableName] = $entitieClass;
        }

        return $this;
    }

    /**
     * Decode Manifest file
     */
    private function _decodeManifest(string $directory): array
    {
        if (is_array($this->_manifest)) {
            return $this->_manifest;
        }

        if (!file_exists($directory)) {
            throw new ConfigException('Innexisting config file ' . $directory, 1);
        }

        return $this->_manifest = $this->getDi()->singleton(Decoder::class)->decode(file_get_contents('nette.safe://' . $directory));
    }

    /**
     * Ordonate boot order
     */
    private function _buildBootSequence(StringSort $resolver): array
    {
        if (is_array($this->_bootSequence)) {
            return $this->_bootSequence;
        }

        foreach ($this->_manifest as $extensionClass => $config) {
            $reflection = new ReflectionClass($extensionClass);

            /** @var ReflectionAttribute[] $attributes */
            $attributes = $reflection->getAttributes(ExtensionConfig::class);
            foreach ($attributes as $reflectedAttribute) {
                /** @var ExtensionConfig $attribute */
                $attribute = $reflectedAttribute->newInstance();
                $resolver->add($extensionClass, $attribute->depends);
            }
        }

        return $this->_bootSequence = $resolver->sort();
    }

    private function _bootExtensions()
    {
        /** @var AsyncJob $task */
        $task = $this->getDi()->singleton(AsyncJob::class, ['id' => '_bootExtensions']);
        $task->setCallable(InstallExtensionTask::class);
        $resetManifest = false;
        foreach ($this->_bootSequence as &$extensionClass) {
            if ($extensionClass === '') {
                unset($extensionClass);

                continue;
            }

            /** @var ExtensionInterface $extension */
            $extension                          = $this->getDi()->singleton($extensionClass);
            $this->_extensions[$extensionClass] = $extension;

            Conf::appendConfigFile($extension->getEnvFile());

            $enabled = $this->isEnabled($extension);
            if ($enabled) {
                $extension->enable();
            }

            if (!$this->isInstalled($extension)) {
                if (CONTEXT === 'APP') {
                    $task->setContext(['extension' => $extension::class]);
                    $task->save();
                    $resetManifest = true;
                }

                continue;
            }

            $this->validateInstallation($extension);
            if ($this->isEnabled($extension)) {
                $extListeners = $extension->getListeners();
                if (null !== $extListeners) {
                    foreach ($extListeners as $listener) {
                        $this->addListener($listener);
                    }
                }

                $extControllers = $extension->getControllers();
                if (null !== $extControllers) {
                    foreach ($extControllers as $namespace) {
                        $this->addController($namespace, $extension->getName());
                    }
                }

                $extModels = $extension->getModels();
                if (null !== $extModels) {
                    foreach ($extModels as $namespace) {
                        $this->addModel($namespace);
                    }
                }

                $extEntities = $extension->getEntities();
                if (null !== $extEntities) {
                    foreach ($extEntities as $entitie) {
                        $this->addEntitie($entitie);
                    }
                }

                $this->_widgetNamespace = array_merge($this->_widgetNamespace ?? [], [$extension->getWidgetNamespaces() => $extension->getWidgetNamespaces()]);

                $this->getDi()->call($extension, 'boot');
                $this->getDi()->call($extension, 'init');
            }
        }

        if ($resetManifest) {
            $this->_manifest = null;
            $this->_decodeManifest(BASE_DIR . self::CONFIG_FILE);
        }
    }

    /**
     * Disable extension
     *
     * @return void
     */
    final public function disable(ExtensionInterface $extension)
    {
        $this->_manifest[$extension::class]['isEnabled'] = false;
    }

    /**
     * Enable extension
     *
     * @return void
     */
    final public function enable(ExtensionInterface $extension)
    {
        $this->_manifest[$extension::class]['isEnabled'] = true;
    }

    /**
     * Is extension enabled ?
     */
    final public function isEnabled(ExtensionInterface $extension): bool
    {
        return $this->_manifest[$extension::class]['isEnabled'] ?? false;
    }

    /**
     * Check if extension is installed
     */
    final public function isInstalled(ExtensionInterface $extension): bool
    {
        return $this->_manifest[$extension::class]['isInstalled'] ?? false;
    }

    /**
     * Check if extension is initialized
     */
    final public function isInitialized(ExtensionInterface $extension): bool
    {
        return $this->getDi()->has($extension::class);
    }

    /**
     * Validate installation
     *
     * @return void
     */
    final public function validateInstallation(ExtensionInterface $extension)
    {
        $extension->validateInstallation();
        $this->_manifest[$extension::class]['isInstalled'] = true;
    }


    /**
     * Validate uninstallation
     *
     * @return void
     */
    final public function validateUninstallation(ExtensionInterface $extension)
    {
        $this->_manifest[$extension::class]['isInstalled'] = false;
    }

    /**
     * Get entitie to register
     *
     * @PhpUnitGen\get("entities")
     */
    public function getEntities(): ?array
    {
        return $this->_entities ?? [];
    }

    /**
     * Get boot sequence
     *
     * @PhpUnitGen\get("bootSequence")
     */
    public function getBootSequence(): array
    {
        return $this->_bootSequence ?? [];
    }

    /**
     * Get controllerMapping
     *
     * @PhpUnitGen\get("models")
     */
    public function getModels(): array
    {
        return $this->_models ?? [];
    }

    /**
     * Get widget namespaces
     *
     * @PhpUnitGen\get("_widgetNamespace")
     */
    public function getWidgetNamespace(): array
    {
        return $this->_widgetNamespace ?? [];
    }

    /**
     * Get extension instance
     *
     * @PhpUnitGen\get("_extensions")
     *
     * @return ExtensionInterface[]
     */
    public function getExtensions(): array
    {
        return $this->_extensions ?? [];
    }

    /**
     * Get listener to register
     *
     * @PhpUnitGen\get("listeners")
     */
    public function getListeners(): array
    {
        return $this->_listeners ?? [];
    }
}
