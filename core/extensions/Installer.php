<?php

namespace core\extensions;

use core\config\Config;
use core\database\DatabaseManagerInterface;
use core\DI\DiProvider;
use core\tasks\AsyncJob;
use core\tasks\InstallExtensionTask;

/**
 * Class Installer
 *
 * Extensions installer
 *
 * @category Extensions installer
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Installer implements ExtensionInstallerServiceInterface
{
    use DiProvider;

    public array $phinxConfig = [];

    public function __construct(
        private ExtensionLauncherServiceInterface $launcher,
        private DatabaseManagerInterface $databaseManager,
        private Config $config
    ) {
        $this->phinxConfig = [
            'paths' => [
                'migrations' => BASE_DIR . 'config/db/migrations',
                'seeds'      => BASE_DIR . 'config/db/seeds'
            ],
            'environments' => [
                'default_migration_table' => 'phinxlog',
                'default_environment'     => 'main',
                'main'                    => [
                    'adapter'    => 'mysql',
                    'connection' => $databaseManager->getConnection()->getPdo(),
                    'name'       => $config->find('MYSQL.DATABASE'),
                ]
            ]
        ];
    }

    public function install(ExtensionInterface $extension): ExtensionInterface
    {
        if (!$extension->isInstalled()) {
            $extension->build();
        }

        /** @var AsyncJob $task */
        $task = $this->getDi()->singleton(AsyncJob::class, ['id' => InstallExtensionTask::class]);
        $task->setCallable(InstallExtensionTask::class);
        $task->setContext(['extension' => $extension::class]);
        $task->save();

        $this->launcher->validateInstallation($extension);
        $this->launcher->enable($extension);

        return $extension;
    }
}
