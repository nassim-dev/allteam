<?php

namespace core\extensions;

use core\DI\DiProvider;

/**
 * Trait ExtensionInstallerProvider
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait ExtensionInstallerProvider
{
    use DiProvider;

    /**
     * ExtensionInstallerServiceInterface implémentation
     *
     * @var ExtensionInstallerServiceInterface
     */
    private $extensionInstaller;

    /**
     * Return ExtensionInstallerServiceInterface implémetation
     */
    public function getExtensionInstaller(): ExtensionInstallerServiceInterface
    {
        return $this->extensionInstaller ??= $this->getDi()->singleton(ExtensionInstallerServiceInterface::class);
    }
}
