<?php

namespace core\extensions;

use core\app\ApplicationServiceInterface;

/**
 * Interface ExtensionInterface
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
interface ExtensionInterface
{
    /**
     * Return base directory
     *
     * @return string
     */
    public function getBaseDir(): string;

    /**
     * Compile extension
     *
     * @return void
     */
    public function compile();

    /**
     * Get associated widget namespaces to register in factories
     */
    public function getWidgetNamespaces(): string;

    /**
     * Boot extension
     *
     * @return void
     */
    public function boot();

    /**
     * Return ENV file
     *
     * @return string
     */
    public function getEnvFile(): string;

    /**
     * Build extension
     *
     * @return void
     */
    public function build();

    /**
     * Destroy extension
     *
     * @return void
     */
    public function destroy();

    /**
     * Install extension
     *
     * @return void
     */
    public function install();

    /**
     * Uninstall extension
     *
     * @return void
     */
    public function uninstall();

    /**
     * Get extension Name
     */
    public function getName(): string;


    /**
     * Initialize extension
     *
     * @return void
     */
    public function init(ApplicationServiceInterface $app);

    /**
     * Execute cron tasks
     *
     * @return void
     */
    public function executeCronTasks(string $context);


    /**
     * Return event listeners
     */
    public function getListeners(): ?array;

    /**
     * Get associated controllers
     *
     * @PhpUnitGen\get("controllers")
     */
    public function getControllers(): ?array;

    /**
     * Get associated entities
     *
     * @PhpUnitGen\get("entities")
     */
    public function getEntities(): ?array;

    /**
     * Get associated models
     *
     * @PhpUnitGen\get("models")
     */
    public function getModels(): ?array;

    /**
     * Get is extension enabled ?
     *
     * @PhpUnitGen\get("isEnabled")
     */
    public function isEnabled(): bool;

    /**
     * Get is extensions installed ?
     *
     * @PhpUnitGen\get("_installed")
     */
    public function isInstalled(): bool;

    /**
     * Get is extensions initialized ?
     *
     * @PhpUnitGen\get("_initialized")
     */
    public function isInitialized(): bool;

    /**
     * Set is extensions installed ?
     *
     * @return self
     */
    public function validateInstallation();

    /**
     * Set is extension enabled ?
     *
     * @return self
     */
    public function enable();
}
