<?php

namespace core\extensions;

/**
 * Interface ExtensionLauncherServiceInterface
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
interface ExtensionLauncherServiceInterface
{
    /**
     * Get extension instance
     *
     * @PhpUnitGen\get("_extensions")
     *
     * @return ExtensionInterface[]
     */
    public function getExtensions(): array;

    /**
     * Get widget namespaces
     *
     *  @example [extensions\namespace]
     */
    public function getWidgetNamespace(): array;

    /**
     * Return controller namespace to register
     *
     * @example ['/baseurl' => 'extensions\namespace]
     */
    public function getControllers(): array;

    /**
     * Return controller namespace to register
     *
     * @example [extensions\namespace]
     */
    public function getModels(): array;

    /**
     * Return list of class listeners
     */
    public function getListeners(): array;

    /**
     * Get entitie to register
     *
     * @PhpUnitGen\get("entities")
     */
    public function getEntities(): ?array;

    /**
     * Disable extension
     *
     * @return void
     */
    public function disable(ExtensionInterface $extension);

    /**
     * Enable extension
     *
     * @return void
     */
    public function enable(ExtensionInterface $extension);

    /**
     * Is extension enabled ?
     */
    public function isEnabled(ExtensionInterface $extension): bool;

    /**
     * Check if extension is installed
     */
    public function isInstalled(ExtensionInterface $extension): bool;

    /**
     * Check if extension is initialized
     */
    public function isInitialized(ExtensionInterface $extension): bool;

    /**
     * Validate installation
     *
     * @return void
     */
    public function validateInstallation(ExtensionInterface $extension);

    /**
     * Validate uninstallation
     *
     * @return void
     */
    public function validateUninstallation(ExtensionInterface $extension);

    /**
     * Get boot sequence
     *
     * @PhpUnitGen\get("bootSequence")
     */
    public function getBootSequence(): array;
}
