<?php

namespace core\extensions;

/**
 * Class ExtensionConfig
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class ExtensionConfig
{
    public function __construct(
        public string $name,
        public string $description,
        public array $depends = [],
        public string $icon = 'fa-square'
    ) {
    }
}
