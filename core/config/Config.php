<?php

namespace core\config;

use core\cache\CacheServiceInterface;
use core\cache\CacheUpdaterTrait;
use core\utils\Utils;
use Firebase\JWT\JWT;
use Nette\Neon\Decoder;
use Nette\Neon\Entity;
use Nette\Neon\Neon;

/**
 * Class Config
 *
 * Manage configurations
 *
 * @category  Manage configurations
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Config
{
    use CacheUpdaterTrait;

    private ?array $_config      = null;
    private array $_decodedFiles = [];
    private ?Decoder $decoder    = null;
    private bool $init           = false;

    /**
     * Contruct config from neon file
     */
    public function __construct(private string $envFile, private CacheServiceInterface $cache)
    {
    }

    private function init()
    {
        if ($this->init) {
            return;
        }
        $this->init = true;
        $this->_fetchPropsFromCache(['_config', '_decodedFiles']);
        $this->decoder = new Decoder();
        if (!is_array($this->_config) || count($this->_config) <= 1) {
            $this->decodeConfigFile($this->envFile);
        }
    }

    /**
     * Append config file
     *
     * @return void
     */
    public function appendConfigFile(string $file)
    {
        $this->init();
        if (file_exists($file)) {
            $this->decodeConfigFile($file);
        }
    }

    public function __destruct()
    {
        $this->_cacheMultiplesProps(['_config', '_decodedFiles']);
    }

    /**
     * Get parmeter from this format key1.key2.key3 or just key
     *
     * @return mixed
     */
    public function find(string $element)
    {
        $this->init();

        $keys = explode('.', $element);
        if (count($keys) <= 1 && !isset($this->_config[$element])) {
            if (method_exists($this, $element)) {
                return $this->{$element}();
            }

            throw new ConfigException("Innexisting config parameter $element", 1);
        }

        $return = $this->_config;
        foreach ($keys as $key) {
            if (!array_key_exists($key, $return)) {
                throw new ConfigException("Innexisting config parameter $element", 1);
            }

            $return = $return[$key];
        }

        return $return;
    }

    public function findInFile(string $neonFile, string $element)
    {
        $this->init();

        if (!file_exists(BASE_DIR . $neonFile)) {
            throw new ConfigException('Innexisting config file ' . BASE_DIR . $neonFile, 1);
        }

        $return = $this->decoder->decode(file_get_contents('nette.safe://' . BASE_DIR . $neonFile));
        $keys   = explode('.', $element);
        foreach ($keys as $key) {
            if (!array_key_exists($key, $return)) {
                return null;
            }

            $return = $return[$key];
        }

        return $return;
    }

    public function saveInFile(string $file, string $element, mixed $newValue)
    {
        $this->init();

        Utils::mkdir(BASE_DIR . 'tmp/files_operations');
        $filename = microtime(true) . '_' . uniqid() . '_file_migration';
        $jsonrpc  = JWT::encode(
            [
                'keys'  => explode('.', $element),
                'file'  => $file,
                'value' => $newValue,
                'exp'   => time() + 60
            ],
            self::find('SECURITY.JSONRPC.KEY'),
            self::find('SECURITY.JSONRPC.METHOD')
        );

        file_put_contents('nette.safe://' . BASE_DIR . 'tmp/files_operations/' . $filename . '_' . $jsonrpc, '');
    }

    private static function parseDotedArray(array $keys): string
    {
        $return = '';
        foreach ($keys as $key) {
            $return .= '[\'' . $key . '\']';
        }

        return $return;
    }

    /**
     * Return custom colors
     */
    public function getCustomCssColors(): string
    {
        $this->init();

        return '';
    }

    /**
     * Set a new config for instance
     *
     * @param mixed $value
     */
    public function set(string $key, $value): self
    {
        $this->_config[$key] = $value;

        return $this;
    }

    /**
     * @return void
     */
    private function decodeConfigFile(string $envFile)
    {
        if (!in_array($envFile, $this->_decodedFiles)) {
            if (!file_exists(BASE_DIR . $envFile)) {
                throw new ConfigException('Innexisting config file ' . BASE_DIR . $envFile, 1);
            }

            $this->_decodedFiles[] = $envFile;
            $oldConfig             = (is_array($this->_config)) ? $this->_config : [];

            $this->_config = $this->decoder->decode(file_get_contents('nette.safe://' . BASE_DIR . $envFile));
            $this->_config = Utils::array_map_recursive(
                function ($element) {
                    if (is_string($element)) {
                        return self::env($element);
                    }
                    if ($element instanceof Entity) {
                        return self::entity($element);
                    }
                },
                $this->_config
            );
            $this->_config = array_merge($oldConfig, $this->_config);
        }
    }

    private static function entity(Entity $entity)
    {
        if (class_exists($entity->value)) {
            $class    = $entity->value;
            $type     = 'static';
            $property = null;

            extract($entity->attributes, EXTR_OVERWRITE);
            if ($property === null) {
                throw new ConfigException('You must provide a property paramerter for entity', 1);
            }
            $regex = '/([a-zA-Z_\-]*)/m';
            preg_match_all($regex, $property, $matches, PREG_SET_ORDER, 0);
            $property = array_pop($matches);

            $return = ($type === 'constant') ? constant("$class::$property") : $class::$$property;

            return (isset($key)) ? $return[$key] : $return;
        }

        if ($entity->value === 'concat') {
            $result = '';
            foreach ($entity->attributes as $type => $value) {
                $result .= ($type === 'var') ? $$value : $value;
            }

            return $result;
        }

        return $entity;
    }

    /**
     * Return environement variable
     *
     * @return mixed
     */
    private static function env(string $key)
    {
        $regex = '/%env:([0-9a-zA-Z_]*)/m';
        preg_match_all($regex, $key, $matches, PREG_SET_ORDER, 0);

        if (empty($matches)) {
            return $key;
        }

        $matches = array_pop($matches);
        if (!isset($matches[1])) {
            throw new ConfigException("Invalid config parameter $key", 1);
        }

        //Try to find docker secret
        if (file_exists("/run/secrets/$key")) {
            return  trim(file_get_contents("/run/secrets/$key"));
        }

        //Else try to find env
        return getenv($matches[1]) ?? null;
    }

    public function __debugInfo()
    {
        $this->init();

        return ['Hidden datas'];
    }
}
