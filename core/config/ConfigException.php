<?php

namespace core\config;

use Exception;

/**
 * Class ConfigException
 *
 * Provide ClassDb exception
 *
 * @category Provide ClassDb exception
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class ConfigException extends Exception
{
}
