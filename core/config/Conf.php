<?php

namespace core\config;

/**
 * Class Conf
 *
 *  @method static string appendConfigFile(string $file)
 *  @method static mixed find(string $element)
 *  @method static string findInFile(string $neonFile, string $element)
 *  @method static string saveInFile(string $file, string $element, mixed $newValue)
 *  @method static string getCustomCssColors()
 *  @method static self set(string $key, string $value)
 *  @method static string getPrefix()
 *  @method static static setPrefix(string $__prefix)
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class Conf extends \core\facade\HelperFacade
{
    protected static ?string $serviceInterface = 'core\config\Config';
    protected static ?Config $object           = null;

    /**
     * Get object
     *
     * @return mixed
     */
    protected static function __getSingleton(): mixed
    {
        return self::$object ??= parent::__getSingleton();
    }

    /**
     * Set object to use for static call
     *
     * @param callable|Config $object Object to use for static call
     *
     * @return void
     */
    public static function setObject(Config|callable $object)
    {
        self::$object = (is_callable($object)) ? $object() : $object;
    }
}
