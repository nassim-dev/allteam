<?php

namespace core\app;

use core\config\Config;
use core\controller\ControllerInterface;
use core\messages\response\HttpResponseInterface;
use core\providers\ServiceProviderInterface;

/**
 * Interface ApplicationServiceInterface
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
interface ApplicationServiceInterface
{
    /**
     * Get dependencies after construction to prevent cyclic-dependencies
     *
     * @return static
     */
    public function build(): self;

    /**
     * Startup action
     *
     * @return static
     */
    public function boot(): self;

    /**
     * Finalize process
     *
     * @return null|HttpResponseInterface
     */
    public function process(): ?HttpResponseInterface;

    /**
     * Shutdown callbacks, generaly destroy all objects
     *
     * @return void
     */
    public function shutdown();

    /**
     * Return ServiceProvider
     */
    public function getServiceProvider(): ServiceProviderInterface;

    /**
     * Set controllerObject
     *
     * @param ControllerInterface $controller ControllerObject
     *
     * @return static
     */
    public function setController(ControllerInterface $controller);


    /**
     * Return array of registred factories
     *
     * @return array
     */
    public function getFactories(): array;

    /**
     * Get config
     *
     * @PhpUnitGen\get("config")
     */
    public function getConfig(): Config;

    /**
     * Get controllerObject
     *
     * @PhpUnitGen\get("_controller")
     */
    public function getController(): ?ControllerInterface;
}
