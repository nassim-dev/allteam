<?php

namespace core\app;

use core\cache\CacheServiceInterface;
use core\config\Conf;
use core\config\Config;
use core\database\mysql\lib\nette\CustomNetteConnection;
use core\DI\DependencieInjectorInterface;
use core\DI\DependenciesInjector;
use core\DI\DI;
use core\events\EventManagerInterface;
use core\extensions\Launcher;
use core\logger\FileLogger;
use core\logger\LogServiceInterface;
use core\logger\TracyLogger;
use core\middleware\MiddlewareDispatcher;
use core\middleware\MiddlewareInterface;
use Nette\Bridges\DatabaseTracy\ConnectionPanel;
use Nette\Caching\Storage;
use Nette\Caching\Storages\MemcachedStorage;
use Nette\Database\Connection;
use Nette\Utils\SafeStream;
use PDO;
use Tracy\Debugger;
use Tracy\Dumper;

/**
 * Class Bootstrap
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Bootstrap implements BootstrapInterface
{
    /**
     * Config
     */
    private ?\core\config\Config $_config = null;

    /**
     * DependencyInjector
     */
    private ?\core\DI\DependencieInjectorInterface $_di = null;

    private array $cacheConfig = [];

    /**
     * @param string $container
     */
    public function __construct(
        private string $applicationClass,
        private array $container,
        private string $configClass,
        private array $eventListeners = [],
        private array $middlewares = []
    ) {
        SafeStream::register();
    }

    /**
     * Startup function
     *
     * @return void
     */
    public function startup(array $context)
    {
        $cache = $this->_buildMainCacheInstance();
        $cache->setCachePrefix($context['CONTEXT']);

        //$cache->invalidate(md5(Config::class) . '._config');
        //$cache->clearAll();
        //$cache->invalidate(md5(DependenciesInjector::class) . '.implementations');
        //$cache->invalidate(md5(Launcher::class) . '._mode');
        //$cache->invalidate(md5(Launcher::class) . '._widgetNamespace');
        //$cache->invalidate(md5(Launcher::class) . '._models');
        //$cache->invalidate(md5(Launcher::class) . '._mode');

        $this->_initConfig($context, $cache);

        $this->_di = $this->_buildDiContainer($context, $cache)
            ->addInstance($cache, [CacheServiceInterface::class])
            ->addInstance($this)
            ->addInstance($this->_config)
            ->affectArray($context);

        foreach ($this->cacheConfig as $element) {
            if (is_object($element)) {
                if ($element instanceof DependencieInjectorInterface) {
                    continue;
                }
                $namespace = explode($element::class, '\\');
                if (!isset($namespace[0]) || strlen($namespace[0]) <= 1) {
                    continue;
                }
                echo $element::class;

                $this->_di->addInstance($element, class_implements($element));

                $properties = get_object_vars($element);
                foreach ($properties as $property) {
                    if ($property instanceof DependencieInjectorInterface) {
                        continue;
                    } $namespace = explode($property::class, '\\');
                    if (!isset($namespace[0]) || strlen($namespace[0]) <= 1) {
                        continue;
                    }

                    if (is_object($property)) {
                        $this->_di->addInstance($property, class_implements($property));
                    }
                }
            }
        }

        $this->_configureDatabaseCache();
        $this->_initDatabaseCredencial();
        $this->_initDebug();

        SafeStream::register();
    }

    public function addListener(string $class): self
    {
        if (!in_array($class, $this->eventListeners)) {
            $this->eventListeners[] = $class;
        }

        return $this;
    }

    public function buildApplication(array $context = []): ApplicationServiceInterface
    {
        /** @var ApplicationServiceInterface $app */
        $app = $this->_di->singleton($this->applicationClass, $context);

        /** @var EventManagerInterface $em */
        $em = $this->_di->singleton(EventManagerInterface::class);
        $em->registerListerners($this->eventListeners);

        if (!empty($this->middlewares)) {
            /** @var MiddlewareDispatcher $dispatcher */
            $dispatcher = $this->_di->singleton(MiddlewareDispatcher::class);

            foreach ($this->middlewares as $class) {
                /** @var MiddlewareInterface $middleware */
                $middleware = $this->_di->singleton($class);
                $dispatcher->pipe($middleware);
            }

            $em->on(CONTEXT . '_PRE_BOOT', fn (): \core\messages\response\HttpResponseInterface => $dispatcher->process());
        }

        return $app->build(); //Build services
    }

    /**
     * Get config
     *
     * @PhpUnitGen\get("config")
     */
    public function getConfig(): Config
    {
        return $this->_config;
    }

    /**
     * Get dependencyInjector
     *
     * @PhpUnitGen\get("di")
     */
    public function getDi(): DependencieInjectorInterface
    {
        return $this->_di;
    }



    /**
     * Build dependency injection container
     */
    private function _buildDiContainer(
        array $context,
        CacheServiceInterface $cache
    ): DependencieInjectorInterface {
        $configFile = 'config/config_' . strtolower($context['CONTEXT']) . '.neon';
        $container  = $this->container['class'];

        /** @var DependencieInjectorInterface $container */
        $container = new $container($configFile, $this->_config, $cache);
        $container->map(CacheServiceInterface::class, $this->container['cache']);
        $container->affect('envFile', $context['envFile']);
        $container->affect('CONFIG_FILE', $configFile);
        $container->affect('CONTEXT', $context['CONTEXT']);

        return $container;
    }

    /**
     * Build  main cache implementation
     */
    private function _buildMainCacheInstance(): CacheServiceInterface
    {
        $this->cacheConfig = $this->container['cache']::defaultConfig();

        return new $this->container['cache'](...$this->cacheConfig);
    }

    /**
     * Configure database cache
     *
     * @return void
     */
    private function _configureDatabaseCache()
    {
        $parameters = [
            'host'    => getenv('MEMCACHED_SERVER'),
            'port'    => (is_int(getenv('MEMCACHED_PORT'))) ? getenv('MEMCACHED_PORT') : 0,
            'prefix'  => 'DB',
            'journal' => null
        ];

        $this->_di->affectArray($parameters, Storage::class);
        $this->_di->affectArray($parameters, MemcachedStorage::class);
    }

    private function _initConfig(array $context, CacheServiceInterface $cache): Config
    {
        $this->_config = new Config($context['envFile'], $cache);
        Conf::setObject($this->_config);
        $this->_config->set('ENVIRONNEMENT', 'development');
        //$this->_config->set('ENVIRONNEMENT', 'production');

        return $this->_config;
    }

    /**
     * Initalize database config
     *
     * @return void
     */
    private function _initDatabaseCredencial()
    {
        $dsn = ($this->_config->find('ENVIRONNEMENT') === 'development')
        ? 'sqlite:' . BASE_DIR . 'config/db/' . $this->_config->find('MYSQL.DATABASE') . '.sqlite3'
        : 'mysql:' . $this->_config->find('MYSQL.SERVER') . ';dbname=' . $this->_config->find('MYSQL.DATABASE');

        $config = [
            'dsn'      => $dsn,
            'user'     => $this->_config->find('MYSQL.USER'),
            'password' => $this->_config->find('MYSQL.PASSWORD'),
            'options'  => []
        ];

        if (str_starts_with($dsn, 'mysql')) {
            $config['options'] = [
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8mb4',
                PDO::FETCH_DEFAULT           => true,
                PDO::ATTR_PREFETCH           => true,
                PDO::ATTR_PERSISTENT         => true,
            ];
        }
        $this->_di->affectArray($config, CustomNetteConnection::class);
        $this->_di->affectArray($config, Connection::class);
    }

    /**
     * Initialize debug tool
     *
     * @return void
     */
    private function _initDebug()
    {
        session_start();

        // Disable Tracy for ajax request
        if (isset($_SERVER['HTTP_X_TRACY_AJAX'])) {
            unset($_SERVER['HTTP_X_TRACY_AJAX']);
        }

        $log = $this->_di->singleton(LogServiceInterface::class);

        if (CONTEXT === 'CLI' || CONTEXT === 'HOOK' || CONTEXT === 'API') {
            $log->registerLogger($this->_di->singleton(FileLogger::class));

            return;
        }

        //Choose log configuration
        $log->registerLogger($this->_di->singleton(TracyLogger::class));

        if ($this->_config->find('ENVIRONNEMENT') === 'development'
            && CONTEXT === 'APP'
            && !isset($_SERVER['x-requested-with'])) {
            ini_set('display_startup_errors', 1);
            ini_set('display_errors', '1');
            ini_set('log_errors', '1');
            error_reporting(E_ALL);

            Debugger::getBar()->addPanel(
                new ConnectionPanel(
                    $this->_di->singleton(CustomNetteConnection::class),
                    Debugger::getBlueScreen()
                )
            );

            Debugger::$showLocation = Dumper::LOCATION_SOURCE | Dumper::LOCATION_CLASS | Dumper::LOCATION_LINK;

            Debugger::$maxDepth           = 5;
            Debugger::$reservedMemorySize = 10000;
            Debugger::$showBar            = true;

            if (!is_dir(BASE_DIR . 'logs')) {
                mkdir(BASE_DIR . 'logs', 777, true);
            }

            Debugger::enable(Debugger::DEVELOPMENT, BASE_DIR . 'logs');
        } else {
            Debugger::$logSeverity = E_NOTICE | E_WARNING;
            Debugger::$email       = $this->_config->find('CONTEXT.ADMIN_EMAIL');
            Debugger::enable(Debugger::PRODUCTION, BASE_DIR . 'logs');
        }
    }
}
