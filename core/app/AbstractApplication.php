<?php

namespace core\app;

use core\config\Config;
use core\controller\ControllerFactory;
use core\controller\ControllerInterface;
use core\database\RepositoryFactoryInterface;
use core\database\RepositoryInterface;
use core\DI\DiProvider;
use core\entities\EntitieFactoryInterface;
use core\events\EventManagerInterface;
use core\extensions\ExtensionLauncherServiceInterface;
use core\messages\response\HttpResponseInterface;
use core\model\ModelFactory;
use core\providers\ServiceProviderInterface;
use core\routing\DispatcherServiceInterface;
use core\utils\ClassFinder;
use ReflectionClass;

/**
 * Class AbstractApplication
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
abstract class AbstractApplication
{
    use DiProvider;

    /**
     * App repository class
     *
     * @var null|array
     */
    protected $_repository;

    /**
     * Get dependencies after construction to prevent cyclic-dependencies
     */
    public function build(): self
    {
        /** @var EventManagerInterface $eventManager */
        $eventManager = $this->getDi()->singleton(EventManagerInterface::class);

        $this->getServiceProvider()->setDi($this->getDi());

        //Build dispatcher and register core controller
        /** @var DispatcherServiceInterface $dispatcher */
        $dispatcher = $this->getDi()->singleton(DispatcherServiceInterface::class);
        $dispatcher->registerNamespace('app\controller', '/');
        $dispatcher->registerNamespace('app\api\controller', '/api/');

        /** @var ControllerFactory $controllerFactory */
        $controllerFactory = $this->getDi()->singleton(ControllerFactory::class);

        /** @var ModelFactory $modelFactory */
        $modelFactory = $this->getDi()->singleton(ModelFactory::class);

        //Build launcher extensions
        /** @var ExtensionLauncherServiceInterface $launcher */
        $launcher = $this->getDi()->singleton(ExtensionLauncherServiceInterface::class);

        //Register extensions controllers
        foreach ($launcher->getControllers() as $controller => $basePath) {
            $namespace = $dispatcher->registerController($controller, $basePath);
            $controllerFactory->addNamespace($namespace);
        }

        //Register models
        foreach ($launcher->getModels() as $namespace) {
            $namespaceElements = explode('\\', $namespace);

            array_pop($namespaceElements);
            $modelFactory->addNamespace(implode('\\', $namespaceElements));
        }

        //Register HtmlElements widgets namespace in factories
        foreach ($this->getFactories() as $factoryClass) {
            $this->getDi()->affectArray(['namespaces' => $launcher->getWidgetNamespace()], $factoryClass);
        }

        //Register event listeners
        $eventManager->registerListerners($launcher->getListeners());

        //Register entities
        /** @var EntitieFactoryInterface $entitieFactory */
        $entitieFactory = $this->getDi()->singleton(EntitieFactoryInterface::class);
        foreach ($launcher->getEntities() as $tableName => $entitieClass) {
            $entitieFactory->registerEntitie($tableName, $entitieClass);
        }

        return $this;
    }


    /**
     * Startup action
     */
    public function boot(): self
    {
        return $this;
    }

    /**
     * Get controllerObject
     *
     * @PhpUnitGen\get("_controller")
     */
    public function getController(): ?ControllerInterface
    {
        return null;
    }

    /**
     * Render view
     */
    public function process(): ?HttpResponseInterface
    {
        return null;
    }

    /**
     * Register extensions repository
     *
     * @return void
     */
    protected function registerRepository(
        RepositoryFactoryInterface $repositoryFactory,
        ExtensionLauncherServiceInterface $launcher,
        string $namespace
    ) {
        if (!is_array($this->_repository)) {
            /** @var ClassFinder $finder */
            $finder            = $this->getDi()->singleton(ClassFinder::class);
            $classes           = $finder->findClasses($namespace . '\entities', ClassFinder::RECURSIVE_MODE) ?? [];
            $this->_repository = [];
            foreach ($classes as $class) {
                $reflection = new ReflectionClass($class);
                if ($reflection->implementsInterface(RepositoryInterface::class)) {
                    $this->_repository[] = $reflection->getNamespaceName();
                }
            }
        }

        $repositoryFactory->addNamespace($namespace . '\\entities');
        // foreach ($this->_repository as $namespace) {
        //     $repositoryFactory->addNamespace($namespace);
        // }

        foreach ($launcher->getWidgetNamespace() as $extension) {
            $repositoryFactory->addNamespace(str_replace('widgets', 'entities', $extension));
        }
    }

    /**
     * Get config
     *
     * @PhpUnitGen\get("config")
     */
    abstract public function getConfig(): Config;

    /**
     * Get serviceProvider
     *
     * @PhpUnitGen\get("serviceProvider")
     */
    abstract public function getServiceProvider(): ServiceProviderInterface;

    /**
     * Return array of registred factories
     */
    abstract public function getFactories(): array;
}
