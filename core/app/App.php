<?php namespace core\app;

/**
 * Class App
 *
 *  @method static self build()
 *  @method static self boot()
 *  @method static core\messages\response\HttpResponseInterface process()
 *  @method static string shutdown()
 *  @method static core\providers\ServiceProviderInterface getServiceProvider()
 *  @method static string setController(core\controller\ControllerInterface $controller)
 *  @method static array getFactories()
 *  @method static core\config\Config getConfig()
 *  @method static core\controller\ControllerInterface getController()
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class App extends \core\facade\HelperFacade
{
    protected static ?string $serviceInterface = 'core\app\ApplicationServiceInterface';
}
