<?php

namespace core\app;

use core\config\Config;
use core\DI\DependencieInjectorInterface;

/**
 * .
 *
 * Description
 *
 * @category  Description
 * @version   0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
interface BootstrapInterface
{
    /**
     * Startup function
     *
     * @return void
     */
    public function startup(array $context);

    public function addListener(string $class): self;

    public function buildApplication(array $context = []): ApplicationServiceInterface;

    public function getConfig(): Config;

    public function getDi(): DependencieInjectorInterface;
}
