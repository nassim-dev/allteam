<?php

namespace core\session;

use ArrayAccess;
use core\services\serializer\DescriptionInterface;
use core\services\serializer\SerializerServiceInterface;

class SessionContainer implements ArrayAccess
{
    public function __construct(
        private string $name,
        private SerializerServiceInterface $serializer,
        array $initialDatas = []
    ) {
        if (!isset($_SESSION[PhpSession::PREFIX][$this->name])) {
            $_SESSION[PhpSession::PREFIX][$this->name] = [];
        }

        $initialDatas = array_merge($_SESSION[PhpSession::PREFIX][$this->name], $initialDatas);
        foreach ($initialDatas as $key => $value) {
            $this[$key] = $value;
        }
    }

    public function offsetUnset($offset): void
    {
        if (isset($_SESSION[PhpSession::PREFIX][$this->name][$offset])) {
            unset($_SESSION[PhpSession::PREFIX][$this->name][$offset]);
        }
    }

    public function offsetSet($offset, $value): void
    {
        $_SESSION[PhpSession::PREFIX][$this->name][$offset] = (is_subclass_of($value, DescriptionInterface::class) || $this->isSimpleType($value)) ? $value : $this->serializer->describe($value);
    }

    private function isSimpleType(mixed $value): bool
    {
        return (!is_object($value) && !is_resource($value) && !is_array($value));
    }

    public function offsetGet($offset): mixed
    {
        $default = $_SESSION[PhpSession::PREFIX][$this->name][$offset] ?? null;

        return (is_subclass_of($default, DescriptionInterface::class)) ? $this->serializer->rebuild($default) : $default;
    }

    public function offsetExists($offset): bool
    {
        return isset($_SESSION[PhpSession::PREFIX][$this->name][$offset]);
    }

    public function push(string $key, $value)
    {
        if (!isset($this[$key])) {
            $this[$key] = [];
        }

        $this[$key][] = $value;
    }

    public function has($key): bool
    {
        return $this->offsetExists($key);
    }

    public function get($key)
    {
        return $this->offsetGet($key);
    }

    public function set(string $key, $value)
    {
        $this->offsetSet($key, $value);
    }

    public function remove(string $key)
    {
        $this->offsetUnset($key);
    }

    /**
     * Get the value of name
     *
     * @PhpUnitGen\get("name")
     */
    public function getName(): string
    {
        return $this->name;
    }

    public function clear()
    {
        $_SESSION[PhpSession::PREFIX][$this->name] = [];
    }

    public function count(): int
    {
        return is_countable($_SESSION[PhpSession::PREFIX][$this->name]) ? count($_SESSION[PhpSession::PREFIX][$this->name]) : 0;
    }

    public function all()
    {
        return $_SESSION[PhpSession::PREFIX][$this->name];
    }
}
