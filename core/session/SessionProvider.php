<?php

namespace core\session;

use core\DI\DiProvider;

/**
 * Trait SessionProvider
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait SessionProvider
{
    use DiProvider;

    /**
     * SessionInterface implémentation
     *
     * @var SessionInterface
     */
    private $session;

    /**
     * Return SessionInterface implémetation
     */
    public function getSession(): SessionInterface
    {
        if (null === $this->session) {
            $this->session = $this->getDi()->singleton(SessionInterface::class);
            $this->session->start();
        }

        return $this->session;
    }
}
