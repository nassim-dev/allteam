<?php namespace core\session;

/**
 * Class Session
 *
 *  @method static string all()
 *  @method static string clear()
 *  @method static string push(string $element, string $value)
 *  @method static int count()
 *  @method static bool destroy()
 *  @method static string get(string $key)
 *  @method static array getCookieParams()
 *  @method static string getId()
 *  @method static string getName()
 *  @method static array getOptions()
 *  @method static bool has(string $key)
 *  @method static bool isStarted()
 *  @method static bool regenerateId()
 *  @method static string remove(string $key)
 *  @method static string save()
 *  @method static string set(string $key, string $value)
 *  @method static string setCookieParams(int $lifetime, string $path, string $domain, bool $secure, bool $httpOnly)
 *  @method static string setId(string $id)
 *  @method static string setName(string $name)
 *  @method static string setOptions(array $config)
 *  @method static bool start()
 *  @method static core\session\SessionContainer getContainer(string $key)
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class Session extends \core\facade\HelperFacade
{
    protected static ?string $serviceInterface = 'core\session\SessionInterface';
}
