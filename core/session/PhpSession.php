<?php

namespace core\session;

use ArrayAccess;
use core\services\serializer\Description;
use core\services\serializer\DescriptionInterface;
use core\services\serializer\SerializerServiceInterface;
use RuntimeException;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class PhpSession implements SessionInterface, ArrayAccess
{
    public const PREFIX = '__CONTAINERS__';

    private array $containers = [];

    public function __construct(private SerializerServiceInterface $serializer)
    {
        $this->start();

        if (!isset($_SESSION[self::PREFIX])) {
            $_SESSION[self::PREFIX] = [];
        }

        foreach ($_SESSION[self::PREFIX] as $containerName => $content) {
            $this->containers[$containerName] = new SessionContainer($containerName, $this->serializer, $content);
        }
    }


    public function offsetUnset(mixed $offset): void
    {
        if (isset($_SESSION[$offset])) {
            unset($_SESSION[$offset]);
        }
    }

    public function offsetSet(mixed $offset, mixed $value): void
    {
        $this->start();

        $_SESSION[$offset] = (is_subclass_of($value, DescriptionInterface::class) || $this->isSimpleType($value)) ? $value : $this->serializer->describe($value);
        session_write_close();
    }

    private function isSimpleType(mixed $value): bool
    {
        return (!is_object($value) && !is_resource($value) && !is_callable($value));
    }

    public function offsetGet($offset): mixed
    {
        $default = $_SESSION[$offset] ?? null;

        return (is_subclass_of($default, DescriptionInterface::class)) ? $this->serializer->rebuild($default) : $default;
    }

    public function offsetExists(mixed $offset): bool
    {
        return isset($_SESSION[$offset]);
    }

    /**
     * {@inheritdoc}
     */
    public function all()
    {
        return $_SESSION;
    }

    /**
     * {@inheritdoc}
     */
    public function clear()
    {
        $_SESSION = [];
    }

    /**
     * {@inheritdoc}
     */
    public function count(): int
    {
        return count($_SESSION);
    }

    /**
     * {@inheritdoc}
     */
    public function destroy(): bool
    {
        $this->clear();

        if (ini_get('session.use_cookies')) {
            $params = session_get_cookie_params();
            setcookie(
                $this->getName(),
                '',
                [
                    'expires'  => time() - 42000,
                    'path'     => $params['path'],
                    'domain'   => $params['domain'],
                    'secure'   => $params['secure'],
                    'httponly' => $params['httponly']
                ]
            );
        }

        if ($this->isStarted()) {
            session_destroy();
            session_unset();
        }

        session_write_close();

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function getCookieParams(): array
    {
        return session_get_cookie_params();
    }

    /**
     * {@inheritdoc}
     */
    public function getId(): string
    {
        return session_id() ?? '';
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return session_name();
    }


    /**
     * {@inheritdoc}
     */
    public function isStarted(): bool
    {
        return session_status() === PHP_SESSION_ACTIVE;
    }

    /**
     * {@inheritdoc}
     */
    public function regenerateId(): bool
    {
        return session_regenerate_id(true);
    }


    /**
     * {@inheritdoc}
     */
    public function getOptions(): array
    {
        $config = [];

        foreach (ini_get_all('session') as $key => $value) {
            $config[substr($key, 8)] = $value['local_value'];
        }

        return $config;
    }


    /**
     * {@inheritdoc}
     */
    public function save()
    {
        session_write_close();
    }

    /**
     * {@inheritdoc}
     */
    public function setCookieParams(int $lifetime, string $path = null, string $domain = null, bool $secure = true, bool $httpOnly = true)
    {
        session_set_cookie_params($lifetime, $path ?? '/', $domain, $secure, $httpOnly);
    }

    /**
     * {@inheritdoc}
     */
    public function setId(string $id)
    {
        if ($this->isStarted()) {
            throw new RuntimeException('Cannot change session id when session is active');
        }

        session_id($id);
    }

    /**
     * {@inheritdoc}
     */
    public function setName(string $name)
    {
        if ($this->isStarted()) {
            throw new RuntimeException('Cannot change session name when session is active');
        }

        session_name($name);
    }

    /**
     * {@inheritdoc}
     */
    public function setOptions(array $config)
    {
        foreach ($config as $key => $value) {
            ini_set('session.' . $key, $value);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function start(): bool
    {
        return (!$this->isStarted()) ? session_start() : true;
    }



    /**
     * {@inheritdoc}
     */
    public function has(string $key): bool
    {
        if (!$this->isInGobalContainer($key)) {
            $elements  = explode('.', $key);
            $container = $this->getContainer($elements[0]);
            unset($elements[0]);
            $realKey = implode('.', $elements);

            return isset($container[$realKey]);
        } else {
            return isset($this[$key]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function get(string $key)
    {
        if (!$this->isInGobalContainer($key)) {
            $elements  = explode('.', $key);
            $container = $this->getContainer($elements[0]);
            unset($elements[0]);
            $realKey = implode('.', $elements);

            return $container->offsetGet($realKey) ?? null;
        } else {
            return $this[$key] ?? null;
        }
    }


    /**
     * @param mixed $value
     */
    public function push(string $key, $value)
    {
        if (!$this->isInGobalContainer($key)) {
            $elements  = explode('.', $key);
            $container = $this->getContainer($elements[0]);
            unset($elements[0]);
            $realKey = implode('.', $elements);

            if (!isset($container[$realKey])) {
                $container[$realKey] = [];
            }

            $container[$realKey][] = $value;
        } else {
            if (!isset($this[$key])) {
                $this[$key] = [];
            }

            $this[$key][] = $value;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function remove(string $key)
    {
        if (!$this->isInGobalContainer($key)) {
            $elements  = explode('.', $key);
            $container = $this->getContainer($elements[0]);
            unset($elements[0]);
            $realKey = implode('.', $elements);
            unset($container[$realKey]);
        } else {
            unset($this[$key]);
        }
    }


    /**
     * {@inheritdoc}
     */
    public function set(string $key, $value): static
    {
        if (!$this->isInGobalContainer($key)) {
            $elements  = explode('.', $key);
            $container = $this->getContainer($elements[0]);
            unset($elements[0]);
            $realKey = implode('.', $elements);
            $this->start();
            $container->offsetSet($realKey, $value);
            session_write_close();
        } else {
            $this[$key] = $value;
        }

        return $this;
    }

    private function isInGobalContainer(string $key): bool
    {
        return (!preg_match('/\./m', $key));
    }

    public function getContainer(string $key): SessionContainer
    {
        return $this->containers[$key] ??= new SessionContainer($key, $this->serializer);
    }
}
