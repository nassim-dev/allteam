<?php

namespace core\DI;

use Closure;
use ReflectionFunction;

/**
 * Class ClosureSignature
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class ClosureSignature
{
    /**
     * @var Implementation[]
     */
    private array $dependencies = [];

    /**
     * ReflectionFunction
     */
    private ?\ReflectionFunction $method;

    /**
     * @var Parameter[]
     */
    private array $parameters = [];

    /**
     * Replace class name
     *
     * @var string|null
     */
    public ?string $class = null;

    /**
     * Replace name name
     *
     * @var string|null
     */
    public ?string $name = null;


    /**
     * Get the value of isPublic
     *
     * @PhpUnitGen\get("isPublic")
     *
     * @return mixed
     */
    public function isPublic(): bool
    {
        return $this->isPublic;
    }

    public function __construct(
        private Closure $closure,
        ?ReflectionFunction &$reflection = null
    ) {
        $this->method = $reflection ?? $this->_createReflectionFunction($closure);
        $this->_parseParameters();
        $this->name = 'closure';
    }

    /**
     * @return array{parameters: \core\DI\Parameter[], class: string|null, name: string|null}
     */
    public function __serialize(): array
    {
        return [
            'parameters' => $this->parameters,
            'class'      => $this->class,
            'name'       => $this->name,
        ];
    }

    /**
     * @return void
     */
    public function __unserialize(array $data)
    {
        $this->parameters = $data['parameters'];
        $this->class      = $data['class'];
        $this->name       = $data['name'];
    }

    /**
     * @return mixed
     */
    public function call(Context &$context)
    {
        return $this->closure->__invoke($this->getRealParameters($context));
    }

    public function getRealParameters(Context &$context): array
    {
        return array_map(
            function ($parameter) use ($context) {
                if (!is_object($parameter)) {
                    return $parameter;
                }

                if ($parameter::class === Implementation::class) {
                    /** @var Implementation $parameter */
                    $buildMode              = $context->getBuildModeFor($this, $parameter);
                    $specificImplementation = $context->getSpecificImplementation($this, $parameter) ?? $parameter->getClass();
                    $object                 = $context->getRegistredObject($specificImplementation);

                    if ($buildMode === Context::SINGLETON && null !== $object) {
                        return $object;
                    }

                    $parameter->prepare('__construct', $context);

                    $identifier = $specificImplementation . $context->getIdentifier($specificImplementation);

                    return $parameter->build($context, $identifier);
                }

                if (is_object($parameter)) {
                    return $parameter;
                }

                throw new DiException('Invalid parameter');
            },
            $context->getConstructor($this->class)
        );
    }

    /**
     * Undocumented function
     *
     * @return array
     */
    public function prepare(Context &$context): array
    {
        $constructor = [];
        foreach ($this->parameters as $parameter) {
            if (isset($context->config[$parameter->name])) {
                $constructor[$parameter->name] = $context->config[$parameter->name];

                continue;
            }

            $implementation = $parameter->prepareInjection($context);

            if (null !== $implementation) {
                $this->dependencies[$parameter->name] = $implementation;
                $constructor[$parameter->name]        = $implementation;
            } else {
                $constructor[$parameter->name] = $parameter->resolveDefaultValue($this->class, $context);
            }
        }

        $context->registerConstructor($this->class, $constructor);

        return $this->dependencies;
    }

    private function _createReflectionFunction(Closure $closure): ?ReflectionFunction
    {
        return new ReflectionFunction($closure);
    }

    /**
     * @return void
     */
    private function _parseParameters()
    {
        if (empty($this->parameters) && null !== $this->method) {
            $closuresignatureElements = $this->method->getParameters();
            $closureRef               = [];
            foreach ($closuresignatureElements as $parameter) {
                $this->parameters[$parameter->getName()] = new Parameter($parameter);
                $closureRef[]                            = $parameter->getName() . '::' . $this->parameters[$parameter->getName()]->type;
            }

            $this->class = md5(implode('|', $closureRef));
        }
    }
}
