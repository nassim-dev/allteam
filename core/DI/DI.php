<?php namespace core\DI;

/**
 * Class DI
 *
 *  @method static WeakReference getWeakReference()
 *  @method static core\DI\DependencieInjectorInterface get()
 *  @method static string map(string $interface, string $class)
 *  @method static core\DI\ProxyFactory getProxyFactory()
 *  @method static core\DI\DependencieInjectorInterface clone()
 *  @method static core\DI\Implementation addImplementation(string $requestedClass, core\DI\Context $context)
 *  @method static string addInstance(string $object, array $interfacesShortcuts)
 *  @method static string affect(string $key, string $value, string $class)
 *  @method static string affectArray(array $store, string $class)
 *  @method static string bind(string $interface, string $class)
 *  @method static string call(string $object, string $methodName, array $parameters)
 *  @method static string callInside(string $object, string $methodName, array $parameters)
 *  @method static string checkImplementation(string $interface, string $class)
 *  @method static core\cache\CacheServiceInterface getCache()
 *  @method static array getImplementations()
 *  @method static array getMapping()
 *  @method static array getStore(string $key)
 *  @method static string make(string $class, array $parameters)
 *  @method static string setCache(core\cache\CacheServiceInterface $cache)
 *  @method static bool has(string $class)
 *  @method static string singleton(string $class, array $parameters)
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class DI extends \core\facade\HelperFacade
{
    protected static ?string $serviceInterface = 'core\DI\DependencieInjectorInterface';
    protected static ?DependencieInjectorInterface $object = null;

    /**
     * Get object
     *
     * @return mixed
     */
    protected static function __getSingleton(): mixed
    {
        return self::getContainer();
    }

    /**
     * Get object
     *
     * @PhpUnitGen\get("object")
     */
    public static function getContainer(): ?DependencieInjectorInterface
    {
        if (null === self::$object) {
            throw new \core\facade\FacadeException('You must provide a class to construct with registerContainer()');
        }

        return self::$object;
    }

    /**
     * Return a cloned container to use in constructor methods
     */
    public static function constructor(): ?DependencieInjectorInterface
    {
        if (null === self::$object) {
            throw new \core\facade\FacadeException('You must provide a class to construct with registerContainer()');
        }

        return self::$object->getWeakReference()->get();
    }

    /**
     * Set object to use for static call
     *
     * @param callable|DependencieInjectorInterface $object Object to use for static call
     *
     * @return void
     */
    public static function registerContainer(DependencieInjectorInterface|callable $object)
    {
        self::$object = (is_callable($object)) ? $object() : $object;
    }
}
