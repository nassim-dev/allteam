<?php

namespace core\DI;

use Nette\SmartObject;
use ReflectionClass;

/**
 * Class Implementation
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Implementation
{
    private \ReflectionClass $reflection;

    private ?string $interface = null;

    /**
     * Methods signatures
     *
     * @var Signature[]
     */
    private ?array $signatures = null;

    /**
     * Is a smart object
     */
    private bool $isSmartObject;

    /**
     * Constructor signature
     *
     * @var array|null
     */
    private ?array $constructor = null;

    public function __construct(private ?string $class, Context &$context)
    {
        $this->reflection = $this->_createReflectionClass($class);
        if ($this->reflection->isInterface() && \interface_exists($class)) {
            $this->interface  = $class;
            $this->class      = $this->findImplementation($context);
            $this->reflection = $this->_createReflectionClass($this->class);
        }

        if ($this->constructor === null) {
            foreach ($this->reflection->getConstructor()?->getParameters() ?? [] as $parameter) {
                $this->constructor[$parameter->getName()] = match ($parameter->getType()::class) {
                    \ReflectionUnionType::class => null,
                    \ReflectionNamedType::class => $parameter->getType()->getName(),
                };
            }
        }

        $this->isSmartObject = (in_array(SmartObject::class, class_uses($this->class, false)));
    }

    /**
     * @return array $array = [
     *               'isSmartObject' => 'bool',
     *               'interface' => 'string|null',
     *               'signatures' => \'core\DI\Signature[]|null',
     *               'class' => 'string|null']
     */
    public function __serialize(): array
    {
        return [
            'isSmartObject' => $this->isSmartObject,
            'interface'     => $this->interface,
            'signatures'    => $this->signatures,
            'class'         => $this->class,
            'constructor'   => $this->constructor
        ];
    }

    /**
     * @return void
     */
    public function __unserialize(array $data)
    {
        $this->isSmartObject = $data['isSmartObject'];
        $this->interface     = $data['interface'];
        $this->signatures    = $data['signatures'];
        $this->class         = $data['class'];
        $this->constructor   = $data['constructor'];
    }

    /**
     * @return mixed
     */
    public function build(Context &$context, string $identifier)
    {
        $class = $this->class;
        if (empty($context->getConstructor($class)) || !isset($this->signatures['__construct'])) {
            $this->prepare('__construct', $context);
        }

        $constructor = $this->signatures['__construct']->getRealParameters($context);

        //Todo Fix ProxyClass
        //$proxyClass = $context->getDi()->getProxyFactory()->getProxyFor($class);
        //$object     = new $proxyClass($constructor);
        $object = new $class(...$constructor);

        if (!$this->isSmartObject) {
            if (method_exists($object, 'setDiWrapper')) {
                $object->setDiWrapper(Context::wrapperClosure());
            }
            if (method_exists($object, 'setDi')) {
                $object->setDi($context->getDi());
            }
        }

        $mode = $context->getBuildModeFor($this->signatures['__construct'], $this);

        return $context->registerObject($object, $mode, [$identifier, $this->getInterface()]);
    }

    /**
     * @return mixed
     */
    public function call(string $method, Context &$context): mixed
    {
        if (!isset($this->signatures[$method])) {
            $this->prepare($method, $context);
        }

        return $this->signatures[$method]->call($context, $this);
    }

    public function findImplementation(Context &$context): string
    {
        $mapping = $context->getMapping();
        if (isset($context->config[Context::__BUILD_MODE__]['mapping'])) {
            $mapping = array_merge($mapping, $context->config[Context::__BUILD_MODE__]['mapping']);
        }

        if (!isset($mapping[$this->interface])) {
            throw new DiException("You must provide an implementation for $this->interface");
        }

        return $mapping[$this->interface];
    }

    /**
     * Get the value of class
     *
     * @PhpUnitGen\get("reflection")
     */
    public function getReflection(): ?ReflectionClass
    {
        return $this->reflection;
    }

    /**
     * Get the value of class
     *
     * @PhpUnitGen\get("class")
     */
    public function getClass(): ?string
    {
        return $this->class;
    }

    /**
     * Get the value of constructor
     *
     * @PhpUnitGen\get("constructor")
     */
    public function getConstructor(): ?array
    {
        return $this->constructor;
    }

    /**
     * Get the value of interface
     *
     * @PhpUnitGen\get("interface")
     */
    public function getInterface(): ?string
    {
        return $this->interface;
    }

    public function getSignature(string $method): ?Signature
    {
        return $this->signatures[$method] ?? null;
    }

    public function prepare(string $method, Context &$context, bool $resolve = true): Signature
    {
        if (!isset($this->signatures[$method])) {
            $reflection                = ('__constuct' === $method) ? $this->reflection->getConstructor() : null;
            $signature                 = new Signature($method, $this->class, $reflection);
            $this->signatures[$method] = $signature;
        }

        /**
         * @var Implementation[] $dependencies
         */
        $dependencies = $context->getPreparedSignatures($this->signatures[$method], $this);
        if ($resolve) {
            $context->resolver->add($this->class, $dependencies);
        }

        return $this->signatures[$method];
    }

    private function _createReflectionClass(string $requestedClass): ReflectionClass
    {
        return new ReflectionClass($requestedClass);
    }
}
