<?php

namespace core\DI;

use WeakReference;

/**
 * Class Context
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Context
{
    public const IN_ALL         = 'ALL';
    public const SINGLETON      = 'SINGLETON';
    public const UNIQUE         = 'UNIQUE';
    public const __BUILD_MODE__ = '__BUILD_MODE__';
    public const __MAPPING__    = '__MAPPING__';

    private string $_buildModeDependencies = self::SINGLETON;
    private string $_buildModeMain         = self::SINGLETON;
    private array $_buildModeSpecific      = [];
    private ?string $_identifier           = null;
    private array $constructors            = [];

    /**
     * Store prepared signature for specofic context
     */
    private array $preparedSignatures = [];

    public function __construct(
        public string $requestedClass,
        private WeakReference $di,
        public array $config = [],
        public ?Resolver $resolver = null,
        ?string $identifier = null,
        private array $tmpContainer = []
    ) {
        $this->initBuildModes($this->config);
        $this->setIdentifier($identifier);
        $this->getDi()->contextContainer = &$this->tmpContainer;
    }




    /**
     * Undocumented function
     *
     * @return void
     */
    public function build(?array $dependencies = null)
    {
        $this->getDi()->dependencies[$this->requestedClass] = $dependencies ?? $this->resolver->sort();

        /**
         * @var Implementation $dependencies
         */
        foreach ($this->getDi()->dependencies[$this->requestedClass] as $classToBuild) {
            //$identifier = $this->getIdentifier($this->requestedClass);
            $implementation = $this->getDi()->getImplementations()[$classToBuild] ?? $this->_createImplementation($this->requestedClass);

            $identifier = $implementation->getClass() . $this->getIdentifier($implementation->getClass());
            if (null === $this->getRegistredObject($identifier)) {
                $implementation->build($this, $identifier);
            }
        }
    }

    /**
     * Find registred object if exists
     */
    public function getRegistredObject(string $class): ?object
    {
        return  $this->getDi()->container[$class] ?? $this->tmpContainer[$class] ?? null;
    }

    /**
     * Register object into di container
     */
    public function registerObject(object &$object, string $mode, array $alias = []): object
    {
        foreach ($alias as $identifier) {
            if (null === $identifier || !is_string($identifier)) {
                continue;
            }

            //if (str_contains($identifier, '@anonymous')) {
            //    continue;
            //}
            if ($mode === self::UNIQUE) {
                if (!str_contains($identifier, '.')) {
                    $identifier .= $this->getIdentifier($identifier);
                }

                $this->tmpContainer[$identifier] = $object;

                continue;
            }

            $this->getDi()->container[$identifier] = $object;
        }

        return $object;
    }

    /**
     * Return Di store parameters
     */
    public function getStore(?string $key = null): array
    {
        $return = $this->getDi()->getStore();

        return ($key == null) ? $this->getDi()->getStore() : ($return[$key] ?? []);
    }

    /**
     * Return di class mapping
     */
    public function getMapping(): array
    {
        return  $this->getDi()->getMapping();
    }

    /**
     * Undocumented function
     *
     * @return mixed
     */
    public function call(Implementation &$implementation)
    {
        return $implementation->call($this->config['method'], $this);
    }

    /**
     * Undocumented function
     */
    public static function extendsFrom(Context &$context, string &$requestedClass): Implementation
    {
        //$scopedContext = $context->getDi()->container[ContextFactory::class]->createFrom($context, $requestedClass);
        $scopedContext = new self(
            $requestedClass,
            $context->di,
            $context->config,
            $context->resolver,
            $context->getBaseIdentifier(),
            $context->getTmpContainer()
        );
        //$scopedContext->preparedSignatures = $context->preparedSignatures;
        //$scopedContext->constructors       = $context->constructors;
        $scopedContext->config['method'] = '__construct';

        return $scopedContext->prepare();
    }

    /**
     * Get the value of identifier
     *
     * @PhpUnitGen\get("identifier")
     */
    public function getBaseIdentifier(): ?string
    {
        return $this->_identifier;
    }

    /**
     * Return buildMode
     */
    public function getBuildModeFor(Signature|ClosureSignature &$signature, Implementation &$implementation): string
    {
        if ($signature->class === $this->requestedClass || $implementation->getInterface() === $this->requestedClass) {
            return $this->_buildModeMain;
        }

        $identifier = "$signature->class::$signature->name";

        if (isset($this->_buildModeSpecific[$identifier])
            && !empty($this->_buildModeSpecific[$identifier])
            && isset($this->_buildModeSpecific[$identifier][$implementation->getClass()])
        ) {
            return $this->_buildModeSpecific[$identifier][$implementation->getClass()][self::__BUILD_MODE__];
        }

        return $this->_buildModeDependencies;
    }

    /**
     * Get specific implementation
     */
    public function getSpecificImplementation(Signature|ClosureSignature &$signature, Implementation &$implementation): ?string
    {
        if ($signature->class === $this->requestedClass) {
            return null;
        }

        $identifier = "$signature->class::$signature->name";

        if (isset($this->_buildModeSpecific[$identifier])
            && !empty($this->_buildModeSpecific[$identifier])
            && isset($this->_buildModeSpecific[$identifier][$implementation->getClass()])
        ) {
            return $this->_buildModeSpecific[$identifier][$implementation->getClass()][self::__MAPPING__];
        }

        return null;
    }

    public function getConstructor(string $class): array
    {
        return $this->constructors[$class] ?? [];
    }

    /**
     * Get the value of identifier
     *
     * @PhpUnitGen\get("identifier")
     */
    public function getIdentifier(string $class): ?string
    {
        return (($class === $this->requestedClass && self::UNIQUE === $this->_buildModeMain)
            || ($class !== $this->requestedClass && self::UNIQUE === $this->_buildModeDependencies))
        ? '.' . $this->_identifier
        : null;
    }

    /**
     * Create wrapper Closure
     * @static
     */
    public static function wrapperClosure(): \Closure
    {
        return function (Context $context, Signature $signature) {
            $methodName = $signature->name;

            return $this->{$methodName}(...$signature->getRealParameters($context));
        };
    }

    /**
     * Create wrapper Closure
     * @deprecated use static call Context::wrapperClosure()
     */
    public function getWrapperClosure(): \Closure
    {
        return self::wrapperClosure();
    }

    /**
     * Undocumented function
     */
    public function prepare(): Implementation
    {
        return $this->_createImplementation($this->requestedClass);
    }

    /**
     * @return void
     */
    public function registerConstructor(string $class, array $parameters)
    {
        $this->constructors[$class] = $parameters;
    }

    /**
     * Update specific build mode config
     *
     * @return void
     */
    public function updateSpecificConfig(array $config)
    {
        $this->_buildModeSpecific = array_merge_recursive($this->_buildModeSpecific, $config);
    }

    /**
     * @param null|string $identifier
     */
    private function _createImplementation(string $class): Implementation
    {
        $implementation = $this->getDi()->addImplementation($class, $this);
        $implementation->prepare($this->config['method'] ?? '__construct', $this);

        return $implementation;
    }

    /**
     * @return void
     */
    public function initBuildModes(array &$config)
    {
        if (isset($config[self::__BUILD_MODE__])) {
            if (isset($config[self::__BUILD_MODE__]['main'])) {
                $this->_buildModeMain = $config[self::__BUILD_MODE__]['main'];
            }

            if (isset($config[self::__BUILD_MODE__]['dependencies'])) {
                $this->_buildModeDependencies = $config[self::__BUILD_MODE__]['dependencies'];
            }

            if (isset($config[self::__BUILD_MODE__]['specific'])) {
                $this->updateSpecificConfig($config[self::__BUILD_MODE__]['specific']);
            }
        }
    }

    /**
     * Get the value of tmpContainer
     *
     * @PhpUnitGen\get("tmpContainer")
     */
    public function getTmpContainer(): array
    {
        return $this->tmpContainer;
    }

    /**
     * Get dependency injector
     *
     * @PhpUnitGen\get("di")
     */
    public function getDi(): DependencieInjectorInterface
    {
        return $this->di->get();
    }


    /**
     * Set the value of tmpContainer
     *
     * @param mixed[] $tmpContainer
     *
     * @PhpUnitGen\set("tmpContainer")
     */
    public function setTmpContainer(array $tmpContainer): self
    {
        $this->tmpContainer = $tmpContainer;

        return $this;
    }

    /**
     * Set the value of _identifier
     *
     * @param mixed $_identifier
     *
     * @PhpUnitGen\set("_identifier")
     *
     * @return self
     */
    public function setIdentifier(?string $identifier)
    {
        $this->_identifier = $identifier ?? uniqid();

        return $this;
    }

    /**
     * Get store prepared signature for specofic context
     *
     * @PhpUnitGen\get("preparedSignatures")
     */
    public function getPreparedSignatures(Signature &$signature, Implementation &$implementation): array
    {
        $id = spl_object_id($signature);
        $this->preparedSignatures[$id] ??= $signature->prepare($implementation, $this);

        return $this->preparedSignatures[$id];
    }
}
