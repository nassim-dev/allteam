<?php

namespace core\DI;

use core\cache\CacheServiceInterface;

/**
 * Interface DependencieInjectorInterface
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
interface DependencieInjectorInterface
{
    public function getWeakReference(): \WeakReference;

    public function get(): DependencieInjectorInterface;

    /**
     * Return true if object is a singleton
     *
     * @param object $object
     *
     * @return boolean
     */
    public function isSingleton(object $object): bool;

    /**
     * Map an implementation to an interface
     *
     * @throws DiException
     * @return void
     */
    public function map(string $interface, string $class);


    public function getProxyFactory(): ProxyFactory;

    /**
     * Clone object with reference to old one
     */
    public function clone(): DependencieInjectorInterface;

    /**
     * @return Implementation
     */
    public function addImplementation(string $requestedClass, Context $context): Implementation;

    /**
     * Add existing instance to container
     *
     * @param mixed $object
     *
     * @return static
     */
    public function addInstance($object, array $interfacesShortcuts = []);

    /**
     * Affect value to contructor paremeter
     *
     * @todo fix null value are not available in parameter, for now create NullObject if possible
     * @param mixed $value
     */
    public function affect(string $key, $value, string $class = Context::IN_ALL);

    /**
     * Affect multiples values
     *
     * @return static
     */
    public function affectArray(array $store, string $class = Context::IN_ALL);

    /**
     * Bind implementation to interface
     *
     * @return static
     */
    public function bind(string $interface, string $class);

    /**
     * Call a method $methodName for $object
     * @todo fix null value are not available in parameter, for now create NullObject if possible
     * @param string|mixed $object
     * @param array        $parameters ['key' => 'value, classNameOrInterface::class => 'value , classNameOrInterface::class => function(){}]
     */
    public function call($object, string $methodName, array $parameters = []);

    /**
     * Call a method inside  $object
     *
     *  Usage inside an object with facade : DI::callInside($this, 'privateMethod');
     *                                      DI::callInside($this, 'protectedMethod');
     *                                      DI::callInside($this, 'publicMethod');
     * Usage outside an object with facade: DI:callInside($object, 'publicMethod');
     *
     * @todo fix null value are not available in parameter, for now create NullObject if possible
     * @todo check debug_backtrace usage, maybe not secure
     * @param string|mixed $object
     * @param string       $methodName
     * @param array        $parameters ['key' => 'value, classNameOrInterface::class => 'value , classNameOrInterface::class => function(){}]
     */
    public function callInside($object, string $methodName, array $parameters = []);

    /**
     * Check if class implement interface
     *
     * @param  string      $service
     * @throws DiException
     * @return void
     */
    public static function checkImplementation(string $interface, string $class);

    /**
     * Get cache service
     *
     * @PhpUnitGen\get("cache")
     */
    public function getCache(): ?CacheServiceInterface;

    /**
     * Get the value of implementations
     *
     * @PhpUnitGen\get("implementations")
     *
     * @return Implementation[]|Implementation
     */
    public function getImplementations(?string $specific = null): array|Implementation;

    /**
     * Get interface mapping
     *
     * @PhpUnitGen\get("mapping")
     */
    public function getMapping(): array;

    /**
     * Get parameters store
     */
    public function getStore(?string $key = null): array;

    /**
     * Make new instance of $class
     *
     * @todo fix null value are not available in parameter, for now create NullObject if possible
     * @param  array  $parameters   ['key' => 'value, classNameOrInterface::class => 'value , classNameOrInterface::class => function(){}]
     * @param  string $depSingleton
     * @return mixed
     */
    public function make(string $class, array $parameters = []);

    /**
     * Set cache service
     *
     * @PhpUnitGen\set("cache")
     *
     * @param CacheServiceInterface $cache Cache service
     *
     * @return static
     */
    public function setCache(CacheServiceInterface $cache);

    /**
     * Return true if class is registred
     *
     * @param string $class
     *
     * @return boolean
     */
    public function has(string $class): bool;

    /**
     * Create new singleton with dependencies as singleton
     *
     * @todo fix null value are not available in parameter, for now create NullObject if possible
     * @param  array  $parameters           ['key' => 'value, classNameOrInterface::class => 'value , classNameOrInterface::class => function(){}]
     * @param  string $singletonDependencie self::SINGLETON
     * @return mixed
     */
    public function singleton(string $class, array $parameters = []);
}
