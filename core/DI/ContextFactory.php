<?php

namespace core\DI;

use core\factory\AbstractFactory;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class ContextFactory extends AbstractFactory
{
    private array $instances = [];

    /**
     * Undocumented function
     *
     * @return Context|null
     */
    public function create(string $requestClass, array $params = [])
    {
        $instance = $this->getInstance($requestClass);
        if (null !== $instance) {
            /** @var Context $instance  */
            if (!isset($params['resolver'])) {
                $instance->resolver->reset();
            } else {
                $instance->resolver = $params['resolver'];
            }

            if (isset($params['tmpContainer'])) {
                $instance->setTmpContainer($params['tmpContainer']);
            }

            $instance->setIdentifier($params['identifier'] ?? null);
            $tmp = $params['build_modes'] ?? [];
            $instance->initBuildModes($tmp);

            return $instance;
        }

        $instance = new Context(
            $requestClass,
            $params['di'],
            $params['build_modes'],
            $params['resolver'] ?? new Resolver(),
            $params['identifier'] ?? null,
            $params['tmpContainer'] ?? []
        ) ;

        return $this->register($instance, $requestClass);
    }

    /**
     * @return mixed
     */
    public function getInstance(string $identifier)
    {
        return $this->instances[$identifier] ?? null;
    }

    public function createFrom(
        Context $context,
        string $requestedClass,
        array $parameters = []
    ): Context {
        $parameters = [
            'di'           => $context->getDi(),
            'build_modes'  => $context->config,
            'resolver'     => $context->resolver,
            'identifier'   => $context->getBaseIdentifier(),
            'tmpContainer' => $context->getTmpContainer()
        ];
        $instance = $this->create($requestedClass, $parameters);


        return $instance;
    }

    /**
     * Undocumented function
     *
     * @return Context|null
     */
    public function get(string $identifier)
    {
    }
}
