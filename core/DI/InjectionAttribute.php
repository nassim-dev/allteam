<?php

namespace core\DI;

/**
 * Class InjectionAttribute
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
#[\Attribute(\Attribute::TARGET_METHOD | \Attribute::IS_REPEATABLE)]
class InjectionAttribute
{
    public const SINGLETON = Context::SINGLETON;

    public const UNIQUE = Context::UNIQUE;

    public function __construct(
        private string $class,
        private string $build = Context::SINGLETON,
        private ?string $implementation = null
    ) {
    }

    /**
     * Get the value of class
     *
     * @PhpUnitGen\get("class")
     */
    public function getClass(): string
    {
        return $this->class;
    }

    /**
     * Get the value of build
     *
     * @PhpUnitGen\get("build")
     */
    public function getBuild(): string
    {
        return $this->build;
    }

    /**
     * Get the value of implementation
     *
     * @PhpUnitGen\get("implementation")
     */
    public function getImplementation(): ?string
    {
        return $this->implementation;
    }
}
