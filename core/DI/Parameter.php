<?php

namespace core\DI;

use ReflectionParameter;

/**
 * Class Parameter
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Parameter
{
    public const BUILDINTYPE = ['string', 'int', 'array', 'bool', 'callable', 'float', 'mixed'];

    public const UNDEFINED = 'UNDEFINED';

    /**
     * @var bool
     */
    public $allowsNull;

    /**
     * @var mixed
     */
    public $defaultValue;

    /**
     * @var bool
     */
    public $isBuiltinType;

    /**
     * @var bool
     */
    public $isDefaultValueAvailable;

    /**
     * @var bool
     */
    public $isOptional;

    /**
     * @var bool
     */
    public $isPassedByRef;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string|null
     */
    public $type;

    public function __construct(ReflectionParameter &$reflection)
    {
        $this->name                    = $reflection->getName();
        $this->type                    = $this->getType($reflection);
        $this->isBuiltinType           = ($this->type !== null) ? (in_array($this->type, self::BUILDINTYPE)) : false;
        $this->isDefaultValueAvailable = $reflection->isDefaultValueAvailable();
        $this->allowsNull              = $reflection->allowsNull();
        $this->isOptional              = $reflection->isOptional();
        $this->defaultValue            = ($reflection->isDefaultValueAvailable()) ? $reflection->getDefaultValue() : self::UNDEFINED;
        $this->isPassedByRef           = $reflection->isPassedByReference();
    }

    private function getType(ReflectionParameter $parameter)
    {
        if ($parameter->getType() == null) {
            return null;
        }

        return match ($parameter->getType()::class) {
            \ReflectionUnionType::class => null,
            \ReflectionNamedType::class => $parameter->getType()->getName(),
        };
    }



    /**
     * @return array{name: string, type: string|null, isBuiltinType: bool, isDefaultValueAvailable: bool, allowsNull: bool, isOptional: bool, defaultValue: mixed, isPassedByRef: bool}
     */
    public function __serialize(): array
    {
        return [
            'name'                    => $this->name,
            'type'                    => $this->type,
            'isBuiltinType'           => $this->isBuiltinType,
            'isDefaultValueAvailable' => $this->isDefaultValueAvailable,
            'allowsNull'              => $this->allowsNull,
            'isOptional'              => $this->isOptional,
            'defaultValue'            => $this->defaultValue,
            'isPassedByRef'           => $this->isPassedByRef
        ];
    }

    /**
     * @return void
     */
    public function __unserialize(array $data)
    {
        $this->name                    = $data['name'];
        $this->type                    = $data['type'];
        $this->isBuiltinType           = $data['isBuiltinType'];
        $this->isDefaultValueAvailable = $data['isDefaultValueAvailable'];
        $this->allowsNull              = $data['allowsNull'];
        $this->isOptional              = $data['isOptional'];
        $this->defaultValue            = $data['defaultValue'];
        $this->isPassedByRef           = $data['isPassedByRef'];
    }

    public function prepareInjection(Context &$context): ?Implementation
    {
        if ($this->isBuiltinType || null === $this->type || $this->allowsNull) {
            return null;
        }

        return Context::extendsFrom($context, $this->type);
    }

    /**
     * Get Parameter default value
     *
     * @param  Context $affectations
     * @return mixed
     */
    public function resolveDefaultValue(string $calledClass, Context &$context)
    {
        if (isset($context->config[$this->name])) {
            return $context->config[$this->name];
        }

        if (isset($context->getStore($calledClass)[$this->name])) {
            return $context->getStore($calledClass)[$this->name];
        }

        if (isset($context->getStore($context->requestedClass)[$this->name])) {
            return $context->getStore($context->requestedClass)[$this->name];
        }

        if (isset($context->getStore(Context::IN_ALL)[$this->name])) {
            return $context->getStore(Context::IN_ALL)[$this->name];
        }

        if ($this->isDefaultValueAvailable) {
            return $this->defaultValue;
        }

        if ($this->allowsNull) {
            return null;
        }

        throw new DiException('Missing parameter $' . $this->name . ' of type ' . $this->type . ' for class ' . $context->requestedClass);
    }
}
