<?php

namespace core\DI;

use ReflectionMethod;

/**
 * Class Signature
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Signature
{
    /**
     * @var Implementation[]
     */
    private array $dependencies = [];

    private bool $isPublic = false;

    /**
     * ReflectionMethod
     */
    private ?\ReflectionMethod $method;

    /**
     * @var Parameter[]
     */
    private array $parameters = [];

    /**
     * Specific build optionb
     *
     * @var InjectionAttribute[]
     */
    private array $attributes = [];

    /**
     * Get the value of isPublic
     *
     * @PhpUnitGen\get("isPublic")
     *
     * @return mixed
     */
    public function isPublic(): bool
    {
        return $this->isPublic;
    }

    public function __construct(
        public string $name,
        public string $class,
        ?ReflectionMethod &$reflection = null
    ) {
        $this->method   = $reflection ?? $this->_createReflectionMethod();
        $this->isPublic = ($this->method) ? $this->method->isPublic() : false;
        $this->_parseParameters();
        $this->_parseInjectionAttributes();
    }

    /**
     * @return array{isPublic: bool, name: string, class: string, attributes: \core\DI\InjectionAttribute[], parameters: \core\DI\Parameter[]}
     */
    public function __serialize(): array
    {
        return [
            'isPublic'   => $this->isPublic,
            'name'       => $this->name,
            'class'      => $this->class,
            'attributes' => $this->attributes,
            'parameters' => $this->parameters
        ];
    }

    /**
     * @return void
     */
    public function __unserialize(array $data)
    {
        $this->isPublic   = $data['isPublic'];
        $this->name       = $data['name'];
        $this->class      = $data['class'];
        $this->attributes = $data['attributes'];
        $this->parameters = $data['parameters'];
    }

    /**
     * @return mixed
     */
    public function call(Context &$context, Implementation &$implementation)
    {
        $classname = $implementation->getClass() ?? $implementation->getInterface();
        if (null === $classname) {
            throw new DiException('Implementation must have at least interface or implementation');
        }

        $object = $context->getRegistredObject($classname);
        if (null === $object) {
            throw new DiException('Function Signature::call has been called before parent Implementation::build.');
        }

        return call_user_func_array([$object, $context->config['method']], $this->getRealParameters($context));
    }

    public function getRealParameters(Context &$context): array
    {
        $class = $this->class;
        $context->updateSpecificConfig(["$this->class::$this->name" => $this->attributes]);

        return array_map(
            function ($parameter) use ($context) {
                if (!is_object($parameter)) {
                    return $parameter;
                }

                if ($parameter::class === Implementation::class) {
                    /** @var Implementation $parameter */
                    $buildMode              = $context->getBuildModeFor($this, $parameter);
                    $specificImplementation = $context->getSpecificImplementation($this, $parameter) ?? $parameter->getClass();
                    $object                 = $context->getRegistredObject($specificImplementation);

                    if ($buildMode === Context::SINGLETON && null !== $object) {
                        return $object;
                    }

                    $parameter->prepare('__construct', $context);

                    $identifier = $specificImplementation . $context->getIdentifier($specificImplementation);

                    return $parameter->build($context, $identifier);
                }

                if (is_object($parameter)) {
                    return $parameter;
                }

                throw new DiException('Invalid parameter');
            },
            $context->getConstructor($class)
        );
    }

    /**
     * @return Implementation[]
     */
    public function prepare(Implementation &$parent, Context &$context): array
    {
        $constructor = [];

        foreach ($this->parameters as $parameter) {
            if (isset($context->config[$parameter->name])) {
                $constructor[$parameter->name] = $context->config[$parameter->name];

                continue;
            }

            $implementation = $parameter->prepareInjection($context);

            if (null !== $implementation) {
                $this->dependencies[$parameter->name] = $implementation;
                $constructor[$parameter->name]        = $implementation;

                continue;
            }
            $constructor[$parameter->name] = $parameter->resolveDefaultValue($parent->getClass(), $context);
        }

        $context->registerConstructor($parent->getClass(), $constructor);

        return $this->dependencies;
    }

    private function _createReflectionMethod(): ?ReflectionMethod
    {
        return (method_exists($this->class, $this->name)) ? new ReflectionMethod($this->class, $this->name) : null;
    }

    /**
     * @return void
     */
    private function _parseParameters()
    {
        if (empty($this->parameters) && null !== $this->method) {
            $signatureElements = $this->method->getParameters();
            foreach ($signatureElements as $parameter) {
                $this->parameters[$parameter->getName()] = new Parameter($parameter);
            }
        }
    }

    /**
     * @return void
     */
    private function _parseInjectionAttributes()
    {
        if (empty($this->attributes) && null !== $this->method) {
            $attributes = $this->method->getAttributes(InjectionAttribute::class);
            foreach ($attributes as $attribute) {
                /**
                 * @var InjectionAttribute $injectionAttribute
                 */
                $injectionAttribute                                = $attribute->newInstance();
                $this->attributes[$injectionAttribute->getClass()] = [
                    Context::__BUILD_MODE__ => $injectionAttribute->getBuild(),
                    Context::__MAPPING__    => $injectionAttribute->getImplementation()
                ];
            }
        }
    }

    /**
     * Get the value of parameters
     *
     * @PhpUnitGen\get("parameters")
     *
     * @return Parameter[]
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }
}
