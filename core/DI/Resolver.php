<?php

namespace core\DI;

use MJS\TopSort\ElementNotFoundException;
use MJS\TopSort\Implementations\StringSort;

/**
 * Class Resolver
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Resolver extends StringSort
{
    private static $instance = null;

    public static function get(): static
    {
        return self::$instance ??= new self();
    }

    /**
     * @param $element
     * @param $parents
     */
    protected function visit($element, &$parents = null)
    {
        $this->throwCircularExceptionIfNeeded($element, $parents);

        if (!$element->visited) {
            $parents[$element->id] = true;

            $element->visited = true;

            foreach ($element->dependencies as $dependency) {
                /**
                 * @var Implementation $dependency
                 */
                if (isset($this->elements[$dependency->getClass()])) {
                    $newParents = $parents;
                    $this->visit($this->elements[$dependency->getClass()], $newParents);
                } else {
                    throw ElementNotFoundException::create($element->id, $dependency->getClass());
                }
            }

            $this->addToList($element);
        }
    }

    /**
     * Reset resolver
     */
    public function reset(): self
    {
        $this->elements = [];
        $this->sorted   = [];

        return $this;
    }
}
