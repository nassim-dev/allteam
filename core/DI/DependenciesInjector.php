<?php

namespace core\DI;

use Closure;
use core\cache\CacheServiceInterface;
use core\cache\CacheUpdaterTrait;
use core\config\Config;
use core\config\ConfigException;
use core\logger\di\TracyDiPanel;
use Nette\Neon\Decoder;
use SplObjectStorage;
use Tracy\Debugger;

/**
 * Class DependenciesInjector
 *
 * The singleton is pass ass propertie obj::di for each created object
 *
 * Dependencies injection
 *
 * @todo it's actualy impossible to call singleton() or make() inside a constructor becaause it creates infinite loop
 *
 * @category  Dependencies_Injection
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class DependenciesInjector implements DependencieInjectorInterface
{
    use CacheUpdaterTrait;
    /**
     * @var array
     */
    public $container = [];

    /**
     * @var Implementation[]
     */
    public $implementations;


    /**
     * @var array
     */
    public $dependencies;

    /**
     * Interface mapping
     *
     * @var array
     */
    public $mapping;

    /**
     * Parameters store
     *
     * @var array
     */
    public $store = [];

    /**
     * Context container
     *
     * @var array
     */
    public $contextContainer;

    /**
     * Proxy factory
     */
    private ?ProxyFactory $proxyFactory = null;

    /**
     * Weak reference
     */
    private ?\WeakReference $weakRef = null;

    /**
     * Contructor
     *
     * @param array|string $config
     *
     * @return void
     */
    public function __construct(
        array|string $mapping,
        private Config $config,
        private ?CacheServiceInterface $cache = null
    ) {
        DI::registerContainer($this);

        //Get mapping && signatures from cache
        $this->_fetchPropsFromCache(['implementations', 'mapping', 'dependencies']);

        //Or get from files (or array)
        if (!is_array($this->mapping)) {
            $this->mapping = (is_array($mapping)) ? $this->_getConfigFromArray($mapping) : $this->_getConfigFromFile($mapping);
        }

        if (!is_array($this->implementations)) {
            $this->implementations = [];
        }

        if (!is_array($this->dependencies)) {
            $this->dependencies = [];
        }

        $this->addInstance($config);

        if ($this->config->find('ENVIRONNEMENT') === 'development' && CONTEXT === 'APP' && !isset($_SERVER['x-requested-with'])) {
            Debugger::getBar()->addPanel(new TracyDiPanel($this));
        }
    }

    /**
     * Return true if object is a singleton
     *
     * @param object $object
     *
     * @return boolean
     */
    public function isSingleton(object $object): bool
    {
        if (!isset($this->container[$object::class])) {
            return false;
        }

        return spl_object_hash($this->container[$object::class]) === spl_object_hash($object);
    }

    /**
     * Map an implementation to an interface
     *
     * @throws DiException
     * @return void
     */
    public function map(string $interface, string $class)
    {
        $this->checkImplementation($interface, $class);
        $this->mapping[$interface] = $class;
    }

    public function getProxyFactory(): ProxyFactory
    {
        if (null === $this->proxyFactory) {
            $this->proxyFactory = new ProxyFactory();
            $this->addInstance($this->proxyFactory);
        }

        return $this->proxyFactory;
    }

    public function clone(): DependencieInjectorInterface
    {
        $cloned                   = clone $this;
        $cloned->container        = &$this->container;
        $cloned->implementations  = &$this->implementations;
        $cloned->mapping          = &$this->mapping;
        $cloned->store            = &$this->store;
        $cloned->contextContainer = &$this->contextContainer;

        return $cloned;
    }


    /**
     * Update constructors signatures cache
     *
     * @return void
     */
    public function __destruct()
    {
        $this->_cacheMultiplesProps(['implementations', 'mapping', 'dependencies']);
        $cacheClass = new SplObjectStorage();
        foreach ($this->container as $class) {
            if (!method_exists($class, '__destruct')) {
                continue;
            }

            if (!in_array(CacheServiceInterface::class, class_implements($class))) {
                $class->__destruct();
            }

            if (!$cacheClass->contains($class)) {
                $cacheClass->attach($class);
            }
        }

        foreach ($cacheClass as $class) {
            $class->__destruct();
        }
    }

    public function addImplementation(string $requestedClass, Context $context): Implementation
    {
        return $this->implementations[$requestedClass] ??= new Implementation($requestedClass, $context);
    }

    /**
     * Add existing instance to container $context->registerObject($object,$mode, [$class]);
     *
     * @param mixed        $object
     * @param mixed[]|null $interfacesShortcuts
     */
    public function addInstance($object, ?array $interfacesShortcuts = null): self
    {
        //$this->container[$this->_getImplementation(get_class($object))] = $object;

        $this->container[$object::class] = $object;

        if (method_exists($object, 'setDiWrapper')) {
            $object->setDiWrapper(Context::wrapperClosure());
        }
        if (method_exists($object, 'setDi')) {
            $object->setDi($this);
        }

        if (!is_null($interfacesShortcuts)) {
            foreach ($interfacesShortcuts as $interface) {
                $this->container[$interface] = $object;
            }
        }

        return $this;
    }

    /**
     * Affect value to contructor paremeter
     *
     * @todo fix null value are not available in parameter, for now create NullObject if possible
     *
     * @param mixed $value
     */
    public function affect(string $key, $value, string $class = Context::IN_ALL): self
    {
        $this->store[$class][$key] = $value;

        if (Context::IN_ALL !== $class) {
            $this->store[$this->_getImplementation($class)][$key] = $value;
        }

        return $this;
    }

    /**
     * Affect multiples values
     */
    public function affectArray(array $store, string $class = Context::IN_ALL): self
    {
        if (Context::IN_ALL !== $class && !interface_exists($class) && !class_exists($class) && 1 !== $class) {
            throw new DiException("$class does not exist");
        }

        foreach ($store as $key => $value) {
            $this->affect($key, $value, $class);
        }

        return $this;
    }

    /**
     * Bind implementation to interface
     */
    public function bind(string $interface, string $class): self
    {
        self::checkImplementation($interface, $class);
        $this->mapping[$interface] = $class;

        return $this;
    }


    /**
     * Autowiring for callable
     *
     * @return void
     */
    public function callable(callable $callable, array $parameters = [])
    {
        $signature                           = new ClosureSignature((new Closure())->fromCallable($callable));
        $parameters[Context::__BUILD_MODE__] = [
            'main'         => Context::SINGLETON,
            'dependencies' => Context::SINGLETON
        ];
        $context = new Context($signature->class, $this->getWeakReference(), $parameters, new Resolver());
        $signature->prepare($context);

        return $signature->call($context);
    }

    /**
     * Call a method $methodName for $object
     *
     * @todo fix null value are not available in parameter, for now create NullObject if possible
     *
     * @param  string|mixed $object
     * @param  array        $parameters ['key' => 'value, classNameOrInterface::class => 'value , classNameOrInterface::class => function(){}]
     * @return mixed
     */
    public function call($object, string $methodName, array $parameters = [])
    {
        if (!is_object($object)) {
            $object = $this->singleton($object, $parameters);
        }

        //$this->affectArray($parameters, $object::class);

        $parameters[Context::__BUILD_MODE__] = [
            'main'         => Context::SINGLETON,
            'dependencies' => Context::SINGLETON
        ];
        $parameters['method'] = $methodName;

        $context        = new Context($object::class, $this->getWeakReference(), $parameters, new Resolver());
        $implementation = $context->prepare();
        $signature      = $implementation->getSignature($methodName);

        if (!$signature->isPublic()) {
            throw new DiException('Method ' . $methodName . ' is not public in ' . $object::class . ', please use DI::callInside($this,"' . $methodName . '") if your trying to call inside scope.');
        }

        $mode = $context->getBuildModeFor($signature, $implementation);
        $context->registerObject($object, $mode, [$object::class]);

        return $implementation->call($methodName, $context);
    }

    /**
     * Call a method inside  $object
     *
     * Usage inside an object with facade : DI::callInside($this, 'privateMethod');
     *                                      DI::callInside($this, 'protectedMethod');
     *                                      DI::callInside($this, 'publicMethod');
     * Usage outside an object with facade: DI:callInside($object, 'publicMethod');
     *
     * @todo fix null value are not available in parameter, for now create NullObject if possible
     * @todo check debug_backtrace usage, maybe not secure
     *
     * @param string|mixed $object
     * @param string       $methodName
     * @param array        $parameters ['key' => 'value, classNameOrInterface::class => 'value , classNameOrInterface::class => function(){}]
     *
     * @return mixed
     */
    public function callInside($object, string $methodName, array $parameters = [])
    {
        if (!is_object($object)) {
            $object = $this->singleton($object, $parameters);
        }

        //$this->affectArray($parameters, $object::class);

        if (!method_exists($object, 'getDiWrapper')) {
            throw new DiException('Method \'callInside\' not available for class ' . $object::class . ' because DiProvider is not used by class');
        }

        $parameters[Context::__BUILD_MODE__] = [
            'main'         => Context::SINGLETON,
            'dependencies' => Context::SINGLETON
        ];
        $parameters['method'] = $methodName;

        $context        = new Context($object::class, $this->getWeakReference(), $parameters, new Resolver());
        $implementation = $context->prepare();
        $signature      = $implementation->getSignature($methodName);

        $backtraces    = debug_backtrace();
        $backtrace     = end($backtraces);
        $caller        = $backtrace['object'] ?? null;
        $callerInScope = $backtraces[1]['object'] ?? null;

        if ((!$signature->isPublic()) && ($caller !== $object && $callerInScope !== $object)) {
            throw new DiException('Invalid caller ' . $caller::class . ' for  ' . $object::class . '::' . $methodName . '. You must call this method in object scope.');
        }

        $mode = $context->getBuildModeFor($signature, $implementation);
        $context->registerObject($object, $mode, [$object::class]);

        return $object->getDiWrapper()->call($object, $context, $signature);
    }

    /**
     * Check if class implement interface
     *
     * @param string $service
     *
     * @throws DiException
     * @return void
     */
    public static function checkImplementation(string $interface, string $class)
    {
        if (!class_exists($class)) {
            throw new DiException("Class $class does not exist");
        }

        if (!class_exists($interface)) {
            if (!interface_exists($interface)) {
                throw new DiException("Interface $interface does not exist");
            }

            if (!in_array($interface, class_implements($class))) {
                throw new DiException("Class $class does not implement $interface");
            }
        } else {
            if (empty(array_intersect(class_implements($class), class_implements($interface)))) {
                throw new DiException("Class $interface is not compatible with $class");
            }
        }
    }

    /**
     * Get cache service
     *
     * @PhpUnitGen\get("cache")
     */
    public function getCache(): ?CacheServiceInterface
    {
        return $this->cache;
    }


    /**
     * Get the value of implementations
     *
     * @PhpUnitGen\get("implementations")
     *
     * @return Implementation[]|Implementation
     */
    public function getImplementations(?string $specific = null): array|Implementation
    {
        return  $this->implementations[$specific] ?? $this->implementations;
    }

    /**
     * Get interface mapping
     *
     * @PhpUnitGen\get("mapping")
     */
    public function getMapping(): array
    {
        return $this->mapping;
    }

    /**
     * Get registred parameter
     *
     * @return mixed|null
     */
    public function getParameter(string $parameter)
    {
        return $this->store[Context::IN_ALL][$parameter] ?? null;
    }

    /**
     * Get parameters store
     */
    public function getStore(?string $key = null): array
    {
        return $this->store[$key ?? 'NOKEY'] ?? $this->store;
    }

    /**
     * Make new instance of $class
     *
     * @todo fix null value are not available in parameter, for now create NullObject if possible
     *
     * @param  array $parameters ['key' => 'value, classNameOrInterface::class => 'value , classNameOrInterface::class => function(){}]
     * @return mixed
     */
    public function make(string $class, array $parameters = [])
    {
        $defaultParameters                          = [];
        $defaultParameters[Context::__BUILD_MODE__] = [
            'main'         => Context::UNIQUE,
            'dependencies' => Context::SINGLETON
        ];

        $parameters = array_replace_recursive($defaultParameters, $parameters);

        $context = new Context($class, $this->getWeakReference(), $parameters, new Resolver());


        $context->prepare();
        $context->build($this->dependencies[$class] ?? null);

        $scopedContainer = $context->getTmpContainer();

        return $scopedContainer[$class . $context->getIdentifier($class)] ?? $this->container[$class . $context->getIdentifier($class)] ?? null;
    }

    /**
     * Set cache service
     *
     * @PhpUnitGen\set("cache")
     *
     * @param CacheServiceInterface $cache Cache service
     */
    public function setCache(CacheServiceInterface $cache): self
    {
        $this->cache = $cache;

        return $this;
    }

    /**
     * Return true if class is registred
     *
     * @param string $class
     *
     * @return boolean
     */
    public function has(string $class): bool
    {
        return isset($this->container[$class]);
    }



    /**
     * Create new singleton with dependencies as singleton
     *
     * @param  array $parameters ['key' => 'value, classNameOrInterface::class => 'value , classNameOrInterface::class => function(){}]
     * @return mixed
     */
    public function singleton(string $class, array $parameters = [])
    {
        if (isset($this->container[$class])) {
            return $this->container[$class];
        }

        if (isset($this->mapping[$class], $this->container[$this->mapping[$class]])) {
            return $this->container[$this->mapping[$class]];
        }

        $defaultParameters                          = [];
        $defaultParameters[Context::__BUILD_MODE__] = [
            'main'         => Context::SINGLETON,
            'dependencies' => Context::SINGLETON
        ];
        $parameters = array_replace_recursive($defaultParameters, $parameters);

        $parameters['method'] = '__construct';

        $context = new Context($class, $this->getWeakReference(), $parameters, new Resolver());
        $context->prepare();
        $context->build($this->dependencies[$class] ?? null);

        return $this->container[$class] ?? $this->container[$this->mapping[$class]];
    }

    /**
     * Check if config is valid
     */
    private function _checkConfigValidity(array $config): array
    {
        if (isset($config['store'])) {
            foreach ($config['store'] as $class => $store) {
                $this->affectArray($store);
            }
        }

        if (isset($config['mapping'])) {
            foreach ($config['mapping'] as $interface => $class) {
                static::checkImplementation($interface, $class);
            }

            return $config['mapping'];
        }

        return [];
    }

    /**
     * Generate base config from file
     *
     * config_app.neon App context
     * config_api.neon Api context
     */
    private function _getBaseConfig(): array
    {
        $file = BASE_DIR . 'config/config_app.neon';
        if (!file_exists($file)) {
            return [];
        }

        $decoder    = new Decoder();
        $baseConfig = $decoder->decode(file_get_contents("nette.safe://$file"));

        return $this->_checkConfigValidity($baseConfig);
    }

    /**
     * Generate config from array
     */
    private function _getConfigFromArray(array $config): array
    {
        return array_merge($this->_getBaseConfig(), $this->_checkConfigValidity($config));
    }

    /**
     * Generate config form file
     *
     * @throws ConfigException
     * @return array           generated Config
     */
    private function _getConfigFromFile(?string $config): array
    {
        if (null === $config) {
            throw new DiException('Missing argument $config in getConfigFromFile()');
        }

        $file = BASE_DIR . $config;
        if (!file_exists(BASE_DIR . $config)) {
            throw new ConfigException("Innexisting config file $file", 1);
        }

        $decoder = new Decoder();
        $config  = $decoder->decode(file_get_contents("nette.safe://$file"));

        return array_merge($this->_getBaseConfig(), $this->_checkConfigValidity($config));
    }

    /**
     * @return mixed
     */
    private function _getImplementation(string $class): ?string
    {
        //$context = $this->container[ContextFactory::class]->create($class, ['build_modes' => [], 'di' => $this]);
        $context        = new Context($class, $this->getWeakReference(), [], new Resolver());
        $implementation = $context->prepare();

        return $implementation->getClass();
    }

    public function get(): self
    {
        return $this;
    }

    public function getWeakReference(): \WeakReference
    {
        return $this->weakRef ??= \WeakReference::create($this);
    }
}
