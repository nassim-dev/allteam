<?php

namespace core\DI;

use WeakReference;

/**
 * Trait DiProvider
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait DiProvider
{
    /**
     * DependencyInjector container
     *
     * @var WeakReference
     */
    private $di;

    /**
     * Closure method defined on construct
     *
     * @var \Closure|null
     */
    private ?\Closure $diWrapper = null;

    /**
     * Get dependencyInjector
     *
     * @PhpUnitGen\get("di")
     */
    public function getDi(): DependencieInjectorInterface
    {
        $di = $this->di ?? DI::getContainer()->getWeakReference();

        return $di->get();
    }

    /**
     * Set dependencyInjector
     *
     * @PhpUnitGen\set("di")
     *
     * @param null|DependencieInjectorInterface $di DependencyInjector
     *
     * @return static
     */
    public function setDi(?DependencieInjectorInterface $di)
    {
        $this->di = $di?->getWeakReference();

        return $this;
    }

    /**
     * Get closure method defined on construct
     *
     * @PhpUnitGen\get("diWrapper")
     *
     * @return \Closure|null
     */
    public function getDiWrapper(): ?\Closure
    {
        return $this->diWrapper;
    }

    /**
     * Set closure method defined on construct
     *
     * @param \Closure|null $diWrapper Closure method defined on construct
     *
     * @PhpUnitGen\set("diWrapper")
     *
     * @return self
     */
    public function setDiWrapper(?\Closure $diWrapper)
    {
        $this->diWrapper = $diWrapper;

        return $this;
    }
}
