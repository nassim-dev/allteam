<?php

namespace core\DI;

use core\utils\Utils;
use Nette\PhpGenerator\ClassType;
use ReflectionClass;
use ReflectionNamedType;
use ReflectionUnionType;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class ProxyFactory
{
    public const FINAL_CLASS = '/*PARENT CLASS IS A FINAL CLASS*/';

    private array $loaded = [];

    public function __construct(private string $directory = 'tmp/proxy/', private string $_proxyNamespace = 'proxy')
    {
        if (!is_dir(BASE_DIR . $this->directory)) {
            mkdir(BASE_DIR . $this->directory);
        }
    }

    public function clearCache()
    {
        $files = glob(BASE_DIR . $this->directory . '*');
        foreach ($files as $file) {
            if (is_file($file)) {
                unlink($file);
            }
        }
    }

    public function getProxyFor(string $class, bool $force = false)
    {
        $proxy = $this->getProxyClassName($class);
        if (class_exists($proxy)) {
            return $proxy;
        }

        if (!isset($this->loaded[$class])) {
            $this->loaded[$class] = $this->buildProxyClass($class, $force);
            if ($this->loaded[$class]['filename'] !== self::FINAL_CLASS) {
                require $this->loaded[$class]['filename'];
            }
        }

        return $this->loaded[$class]['className'];
    }

    private function getProxyClassName(string $class)
    {
        $elements       = explode('\\', $class);
        $className      = array_pop($elements);
        $proxyClassName = $className . 'Proxy';
        $namespace      = $this->_proxyNamespace . '\\' . implode('\\', $elements);

        return  $namespace . '\\' . $proxyClassName;
    }

    /**
     * Build class proxy
     *
     * @param string $filename
     */
    private function buildProxyClass(string $class, bool $force = false): array
    {
        $elements       = explode('\\', $class);
        $className      = array_pop($elements);
        $proxyClassName = $className . 'Proxy';
        $namespace      = $this->_proxyNamespace . '\\' . implode('\\', $elements);
        $directory      = BASE_DIR . $this->directory . implode('/', $elements) . '/';
        $proxyFileName  = $directory . $proxyClassName . '.php';

        if (!file_exists($proxyFileName) || $force) {
            $reflection = new ReflectionClass($class);
            Utils::mkdir($directory);

            if ($reflection->isInternal()) {
                return ['filename' => self::FINAL_CLASS, 'className' => $class];
            }

            if ($reflection->isFinal()) {
                file_put_contents('nette.safe://' . $proxyFileName, self::FINAL_CLASS);

                return ['filename' => self::FINAL_CLASS, 'className' => $class];
            }

            $this->_generateProxyClass($reflection, $proxyClassName, $proxyFileName, $namespace, self::$_proxyClassTemplate);
        }

        if (!file_exists($proxyFileName) || filesize($proxyFileName) === strlen(self::FINAL_CLASS)) {
            return ['filename' => self::FINAL_CLASS, 'className' => $class];
        }

        return ['filename' => $proxyFileName, 'className' => $namespace . '\\' . $proxyClassName];
    }


    /**
     * Generates a proxy class file.
     * Substitutes certain parameters like class name and methods in a template
     * kept at the end of this file. The class source code is saved in a file in the directory specified.
     * Testing this method is usually not a problem since the directory is easily configurable.
     *
     * @param string $originalClassName
     * @param string $file              The path of the file to write to.
     */
    private function _generateProxyClass(
        ReflectionClass $class,
        string $proxyClassName,
        string $fileName,
        string $namespace,
        string $file
    ) {
        if ($class->isAbstract() || $class->isInternal() || !$class->isInstantiable()) {
            return;
        }
        $methods     = $this->_generateMethods($class);
        $sleepImpl   = $this->_generateSleep($class);
        $constructor = $this->_generateConstructor($class);

        $placeholders = [
            '<namespace>',
            '<proxyClassName>',
            '<className>',
            '<methods>',
            '<sleepImpl>',
            '<constructor>'
        ];

        $className    = (substr($class->name, 0, 1) == '\\') ? substr($class->name, 1) : $class->name;
        $replacements = [
            $namespace,
            $proxyClassName,
            $className,
            $methods,
            $sleepImpl,
            $constructor
        ];

        $file = str_replace($placeholders, $replacements, $file);

        file_put_contents('nette.safe://' . $fileName, $file);
    }

    /**
     * Generate constructor body
     */
    private function _generateConstructor(ReflectionClass $class): string
    {
        if ($class->getConstructor() !== null) {
            //return PHP_EOL . '        parent::__construct(...$__constructor__);' . PHP_EOL;
            $constructor = PHP_EOL . '        $this->__isInitialized__ = function () {' . PHP_EOL;
            $constructor .= '            parent::__construct(...$this->__constructor__);' . PHP_EOL;
            $constructor .= '            $this->__constructor__ = null;' . PHP_EOL;

            return $constructor .= '        };' . PHP_EOL;
        }

        return PHP_EOL . '        $this->__isInitialized__ = true;' . PHP_EOL;
    }

    /**
     * Generates the methods of a proxy class.
     * All methods are overridden to call _load() before execution.
     *
     * @return string The code of the generated methods.
     */
    private function _generateMethods(ReflectionClass $class)
    {
        $methods    = '';
        $classTypes = [ClassType::from($class->getName(), true)];
        $reflection = $class->getParentClass();
        if (!is_bool($reflection) && $reflection->isInternal()) {
            $reflection = null;
        }
        while ($reflection instanceof ReflectionClass) {
            $classTypes[] = ClassType::from($reflection->getName(), true);
            $reflection   = $reflection->getParentClass();

            if (is_bool($reflection) || null === $reflection || $reflection->isInternal()) {
                $reflection = null;
            }
        }

        foreach ($class->getMethods() as $method) {
            if ($method->isConstructor() || strtolower($method->getName()) == '__sleep' || strtolower($method->getName()) == '__destruct') {
                continue;
            }

            if ($method->isPublic() && !$method->isFinal() && !$method->isStatic()) {
                $methods .= PHP_EOL . '    public function ';
                if ($method->returnsReference()) {
                    $methods .= '&';
                }

                $methods .= $method->getName() . '(';
                $parameterString = $argumentString = [];
                foreach ($method->getParameters() as $param) {
                    $parameterString[] = $this->formatParameter($param);
                    $argumentString[]  = ($param->isVariadic() ? '...' : '') . '$' . $param->getName();
                }

                $return = null;
                $finded = false;
                foreach ($classTypes as $classType) {
                    if ($finded) {
                        continue;
                    }

                    if (!$classType->hasMethod($method->getName())) {
                        continue;
                    }

                    $methodType = $classType->getMethod($method->getName());
                    if ($methodType->getBody() === null) {
                        continue;
                    }

                    $return = (str_contains($methodType->getBody(), 'return ')) ? 'return ' : null;
                    $finded = true;
                }

                if ($finded && $method->hasReturnType()) {
                    $returnType = $method->getReturnType();
                    $return     = 'return ';
                    if (method_exists($returnType, 'getName') && str_contains($returnType->getName(), 'void')) {
                        $return = null;
                    }
                }

                $methods .= implode(', ', $parameterString) . ')' . $this->formatMethodReturn($method);
                $methods .= PHP_EOL . '    {' . PHP_EOL;
                $methods .= '        $this->_load();' . PHP_EOL;
                $methods .= '        ' . $return . 'parent::' . $method->getName() . '(' . implode(', ', $argumentString) . ');';
                $methods .= PHP_EOL . '    }' . PHP_EOL;
            }
        }

        return $methods;
    }

    /**
     * Format method return type
     */
    private function formatMethodReturn(\ReflectionMethod $method): string
    {
        $return = '';
        $type   = $method->getReturnType();
        if ($type !== null) {
            $return = ':';
            $return .= match ($type::class) {
                ReflectionUnionType::class => $this->formatUnionType($type, null),
                ReflectionNamedType::class => $this->formatNamedType($type, null),
            };
        }

        return $return;
    }

    /**
     * Format parameter
     */
    private function formatParameter(\ReflectionParameter $param): string
    {
        $return = '';
        $type   = $param->getType();
        if ($type !== null) {
            $return = match ($type::class) {
                ReflectionUnionType::class => $this->formatUnionType($type, $param),
                ReflectionNamedType::class => $this->formatNamedType($type, $param),
            };
            $return .= ' ';
        }

        if ($param->isVariadic()) {
            $return .= '...';
        }

        if ($param->isPassedByReference()) {
            $return .= '&';
        }

        $return .= '$' . $param->getName();

        if ($param->isDefaultValueAvailable()) {
            $return .= ' = ' . var_export($param->getDefaultValue(), true);
        }

        return $return;
    }

    /**
     * Format parameter type
     */
    private function formatNamedType(\ReflectionNamedType $param, ?\ReflectionParameter $reflection, bool $operator = true): string
    {
        $type         = $param->getName();
        $nullOperator = (($reflection?->allowsNull() || $param->allowsNull()) && $type !== 'mixed') ? '?' : '';
        $nullOperator = ($operator) ? $nullOperator : '';

        return ($param->isBuiltin() || $type == 'static' || $type == 'self') ? $nullOperator . $type : $nullOperator . '\\' . $type;
    }

    /**
     * Format parameter union type
     */
    private function formatUnionType(\ReflectionUnionType $param, ?\ReflectionParameter $reflection): string
    {
        $types = [];
        foreach ($param->getTypes() as $namedType) {
            $types[] = $this->formatNamedType($namedType, $reflection, false);
        }

        return implode('|', $types);
    }

    /**
     * Generates the code for the __sleep method for a proxy class.
     * The __sleep() method is used in case of serialization, which should
     * not include service objects referenced by proxies like the Entity Manager.
     *
     * @return string
     */
    private function _generateSleep(ReflectionClass $class)
    {
        $sleepImpl = '';

        if ($class->hasMethod('__sleep')) {
            $sleepImpl .= 'return parent::__sleep();';
        } else {
            $sleepImpl .= 'return [';
            $first = true;

            foreach ($class->getProperties() as $propertie) {
                if ($first) {
                    $first = false;
                } else {
                    $sleepImpl .= ', ';
                }

                $sleepImpl .= "'" . $propertie->getName() . "'";
            }

            $sleepImpl .= '];';
        }

        return $sleepImpl;
    }

    /** Proxy class code template */
    private static string $_proxyClassTemplate = '<?php

namespace <namespace>;

/**
 * THIS CLASS WAS GENERATED BY DEPENDENCIES INJECTOR CONTAINER. DO NOT EDIT THIS FILE.
 */
class <proxyClassName> extends \<className> implements \core\DI\ProxyInterface
{

    public $__isInitialized__;
    public $diWrapper;
    public $di;

    public function __construct(private ?array $__constructor__)
    {
        <constructor>
    }

    public static function _getParentClass():string
    {
        return parent::class;
    }


    private function _load()
    {
        if (is_callable($this->__isInitialized__)) {
            $callable                = $this->__isInitialized__;
            $this->__isInitialized__ = true;
            $callable->call($this);
        }
    }

    <methods>

    public function __sleep()
    {
        if ($this->__isInitialized__ !== true) {
            return [];
            //throw new \core\DI\DiException("Not fully loaded proxy can not be serialized.");
        }
        <sleepImpl>
    }
}';
}
