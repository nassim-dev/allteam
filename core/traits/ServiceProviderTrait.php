<?php

namespace core\traits;

use core\DI\DiProvider;
use core\providers\ServiceProviderInterface;

/**
 * Trait ServiceProviderTrait
 *
 * Provide getter for service provider
 *
 * @category Provide getter for service provider
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
trait ServiceProviderTrait
{
    use DiProvider;
    /**
     * Return ServiceProvider instance
     */
    public function getServiceProvider(): ServiceProviderInterface
    {
        return $this->getDi()->singleton(ServiceProviderInterface::class);
    }
}
