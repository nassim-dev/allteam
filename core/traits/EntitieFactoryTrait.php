<?php

namespace core\traits;

use core\entities\EntitieFactoryInterface;

/**
 * Trait EntitieFactoryTrait
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait EntitieFactoryTrait
{
    /**
     * Entitie Factory
     *
     * @var EntitieFactoryInterface
     */
    private $entitieFactory;

    /**
     * Get entitie Factory
     *
     * @PhpUnitGen\get("entitieFactory")
     */
    public function getEntitieFactory(): EntitieFactoryInterface
    {
        return $this->entitieFactory;
    }

    /**
     * Set entitie Factory
     *
     * @PhpUnitGen\set("entitieFactory")
     *
     * @param EntitieFactoryInterface $entitieFactory Entitie Factory
     *
     * @return static
     */
    public function setEntitieFactory(EntitieFactoryInterface $entitieFactory)
    {
        $this->entitieFactory = $entitieFactory;

        return $this;
    }
}
