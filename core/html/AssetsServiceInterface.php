<?php

namespace core\html;

/**
 * Interface AssetsServiceInterface
 *
 * Manage CSS && javascript files
 *
 * @category service_interface
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
interface AssetsServiceInterface
{
    public function addCss(string $file, bool $inBody = true);

    public function addCssInline(string $style, bool $inBody = true);

    public function addJs(string $file, bool $inBody = true);

    public function addJsInline(string $script, bool $inBody = true);

    /**
     * Get the value of css
     *
     * @PhpUnitGen\get("css")
     */
    public function getCss(): array;

    /**
     * Get the value of js
     *
     * @PhpUnitGen\get("js")
     */
    public function getJs(): array;

    /**
     * Return all assets concatenate for body tag
     */
    public function getBody(): string;

    /**
     * Return all assets concatenate for head tag
     */
    public function getHead(): string;
}
