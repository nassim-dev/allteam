<?php

namespace core\html\javascript;

use core\states\StateInterface;

/**
 * Interface JavascriptCommandInterface
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
interface JavascriptCommandInterface extends StateInterface
{
    public const DEFAULT_CONTEXT = 'default';

    public function getCallback(): string;

    public function setCallback(string $callback);

    /**
     * Return array of properties
     */
    public function toArray(): array;

    /**
     * Get command js file name
     */
    public function get(): string;


    /**
     * Return command context
     */
    public function getContext(): string;
}
