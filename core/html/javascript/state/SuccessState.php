<?php

namespace core\html\javascript\state;

use core\html\javascript\JavascriptStateInterface;
use core\states\AbstractState;

/**
 * Class SuccessState
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class SuccessState extends AbstractState implements JavascriptStateInterface
{
    private $elementId;

    public function __construct(string|HtmlElementInterface $element)
    {
        $this->elementId = (is_string($element)) ? $element : $element->getIdAttribute();
    }
    /**
     * Undocumented function
     */
    public function get(): string
    {
        return 'action.success.' . $this->elementId;
    }
}
