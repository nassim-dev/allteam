<?php

namespace core\html\javascript;

use core\html\HtmlElementContainer;
use core\secure\csrf\CsrfServiceInterface;
use core\session\SessionInterface;
use core\transport\websocket\Websocket;
use core\transport\websocket\WebsocketServiceInterface;

/**
 * Représente la configuration générale javascript à appliquer
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class JavascriptConfig implements JavascriptServiceInterface
{
    /**
     * Registred Trigger
     */
    private \core\html\HtmlElementContainer $objects;

    public function __construct(
        protected CsrfServiceInterface $csrf,
        protected WebsocketServiceInterface $websocket,
        protected SessionInterface $session
    ) {
        $this->objects = new HtmlElementContainer();
    }


    /**
     * Clear Config
     */
    public function clearConfig(): self
    {
        $this->session->set('JS_CONFIG.IS_CACHED', false);
        $this->session->set('JS_CONFIG.CONFIG', []);

        return $this;
    }

    /**
     * Add New jsObject
     */
    public function detach(JavascriptObjectInterface $jsObject): self
    {
        $this->objects->detach($jsObject);

        return $this;
    }

    /**
     * Return Config
     */
    public function getConfig(bool $hotConfig = false): array
    {
        if ($hotConfig) {
            $return = [];

            foreach ($this->objects as $jsObject) {
                /**
                 * @var JavascriptObjectInterface $jsObject
                 */
                $return[$jsObject->getViewReference()][$jsObject->getIdAttribute()] = $jsObject->toArray();
            }

            return $return;
        }


        return $this->session->get('JS_CONFIG.CONFIG') ?? [];
    }

    /**
     * Get jsObject
     */
    public function getObject(JavascriptObjectInterface | string $object): ?JavascriptObjectInterface
    {
        $ref = (is_object($object)) ? $object->getIdAttribute() : $object;

        if (isset($this->objects[$ref])) {
            return $this->objects[$ref];
        }

        if (is_object($object)) {
            return $this->register($object);
        }

        return null;
    }

    /**
     * Add New jsObject
     */
    public function register(JavascriptObjectInterface $object, bool $classObject = false): JavascriptObjectInterface
    {
        $object->classObject = $classObject;
        $this->objects->attach($object, $object->getIdAttribute());

        return $object;
    }


    /**
     * Save Config to Session
     */
    public function saveConfig(): array
    {
        $sessionContainer = $this->session->getContainer('JS_CONFIG');
        $return           = $sessionContainer['CONFIG'] ?? [];

        foreach ($this->objects as $jsObject) {
            /**
             * @var JavascriptObjectInterface $jsObject
             */
            if (!isset($sessionContainer['CONFIG'][$jsObject->getViewReference()][$jsObject->getIdAttribute()])) {
                $sessionContainer['IS_CACHED']                                      = false;
                $return[$jsObject->getViewReference()][$jsObject->getIdAttribute()] = $jsObject->toArray();
            }
        }

        //Update config with websocket
        if (!empty($return)) {
            $this->websocket->updateJavascriptConfig($this, $return);
        }

        $return['LANGUAGE']         = ('fr' === $this->session->get('CONTEXT.LANGUAGE')) ? 'fr-FR' : 'en-US';
        $sessionContainer['CONFIG'] = array_merge($sessionContainer['CONFIG'] ?? [], $return);

        return $return;
    }
}
