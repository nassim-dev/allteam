<?php

namespace core\html\javascript\utils;

class JsUtils
{
    /**
     * Mark code to be executed by client
     */
    public static function Js(string $string): string
    {
        return '<<<JS' . $string . 'JS;';
    }
}
