<?php

namespace core\html\javascript;

use core\html\traits\Identifiable;

/**
 * Trait WithJavascript
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait WithJavascript
{
    use Identifiable;

    /**
     * Action to do on specific state
     * @var array
     */
    private $_actions = [];

    /**
     * Object build config
     * @var array
     */
    private $_config = [];

    /**
     * Listener to register
     * @var array
     */
    private $_listeners = [];

    /**
     * Add Post Action
     */
    public function addAction(string $action, ?JavascriptStateInterface $onState = null): Action
    {
        return $this->_actions[$action] ??= new Action($action, $onState);
    }


    /**
     * Add config to execute
     */
    public function addConfig(string $identifier): Config
    {
        return $this->_config[$identifier] ??= new Config($identifier);
    }

    /**
     * Remove config to execute
     *
     * @return void
     */
    public function removeConfig(string $identifier)
    {
        if (isset($this->_config[$identifier])) {
            unset($this->_config[$identifier]);
        }
    }

    /**
     * Add Event listener
     */
    public function on(array | string $event): Event
    {
        $id = (is_array($event)) ? $event[Event::EVENT]->get() : $event;

        return $this->_listeners[$id] ??= new Event($event, $this);
    }


    /**
     * Reset jsConfig
     *
     * @return void
     */
    public function reset()
    {
        $this->_actions   = [];
        $this->_listeners = [];
    }

    /**
     * Deffered build
     *
     * @param string $type deferred or onViewport
     *
     * @return self
     */
    public function buildOn(string $event, string|JavascriptObjectInterface $selector, string $type = 'deferred')
    {
        $this->addConfig('buildOn')->addParameters(
            [
                'selector' => (is_string($selector)) ? $selector : '#' . $selector->getIdAttribute(),
                'event'    => $event
            ]
        )
            ->value($type);

        if ($this->getChilds() !== null) {
            foreach ($this->getChilds() as $child) {
                if (method_exists($child, 'buildOn')) {
                    $child->buildOn($event, $selector, $type);
                }
            }
        }


        return $this;
    }

    /**
     * Reset deffered build to default build
     *
     *  @return self
     */
    public function restoreDefaultBuildMode()
    {
        $this->removeConfig('buildOn');
        if ($this->getChilds() !== null) {
            foreach ($this->getChilds() as $child) {
                if (method_exists($child, 'buildOn')) {
                    $child->removeConfig('buildOn');
                }
            }
        }

        return $this;
    }


    /**
     * Return an Array for object jsConfig
     */
    public function _toArrayJavascript(): array
    {
        $configArray = [
            'actions'   => [],
            'listeners' => [],
            'config'    => []
        ];
        foreach ($this->_actions as $obj) {
            /**
             * @var Action $obj
             */
            $configArray['actions'][$obj->getId()] = $obj->toArray();
        }

        $configArray['listeners'] = [];
        foreach ($this->_listeners as $obj) {
            /**
             * @var Event $obj
             */
            $configArray['listeners'][$obj->getId()] = $obj->toArray();
        }

        $configArray['config'] = [];
        foreach ($this->_config as $obj) {
            /**
             * @var Action $obj
             */
            $configArray[$obj->getId()] = $obj->toArray();
        }

        return $configArray;
    }
}
