<?php

namespace core\html\javascript;

use core\html\traits\Identifiable;
use core\html\traits\Tunable;

/**
 * Class AbstractParameter
 *
 * Represent AbstractParameter
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
abstract class AbstractParameter
{
    use Identifiable;
    use Tunable;

    /**
     * Action Type
     *
     * @var string FUNCTION_EVAL|VALUE|FIELD_VALUE|null|CALLABLE
     */
    private string $type = 'CALLABLE';

    public function __construct(string $id, private ?JavascriptStateInterface $state = null)
    {
        $this->setId($id);
    }

    /**
     * Return an Array of object
     * @return array{id: string|null, parameters: mixed[]}
     */
    public function toArray(): array
    {
        return [
            'id'         => $this->getId(),
            'parameters' => $this->getParameters(),
        ];
    }
}
