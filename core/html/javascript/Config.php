<?php

namespace core\html\javascript;

use core\utils\Utils;

/**
 * Class Config
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Config extends AbstractParameter
{
    /**
     * Fixed valude
     */
    private mixed $value = null;

    /**
     * Set a fixed value
     */
    public function value(string | bool | array | null $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Add dynamic value, setted by callback function
     *
     * @return void
     */
    public function dynamicValue(JavascriptCommandInterface $command): self
    {
        $this->value = $command;

        return $this;
    }

    /**
     * Return an Array of object
     * @return array{id: string|null, value: mixed, parameters: mixed[]}
     */
    public function toArray(): array
    {
        return [
            'id'         => $this->getId(),
            'value'      => Utils::arrayPrinter($this->value),
            'parameters' => $this->getParameters(),
        ];
    }
}
