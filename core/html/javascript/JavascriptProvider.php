<?php

namespace core\html\javascript;

use core\DI\DiProvider;

/**
 * Trait JavascriptProvider
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait JavascriptProvider
{
    use DiProvider;

    /**
     * JavascriptServiceInterface implémentation
     *
     * @var JavascriptServiceInterface
     */
    private $javascript;

    /**
     * Return JavascriptServiceInterface implémetation
     */
    public function getJavascript(): JavascriptServiceInterface
    {
        return $this->javascript ??= $this->getDi()->singleton(JavascriptServiceInterface::class);
    }
}
