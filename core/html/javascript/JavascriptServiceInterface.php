<?php

namespace core\html\javascript;

/**
 * Interface JavascriptServiceInterface
 *
 * Manage javascript config for widget
 *
 * @category  Manage CSS && javascript files
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
interface JavascriptServiceInterface
{
    /**
     * Clear Config
     *
     * @return static
     */
    public function clearConfig();

    /**
     * Return Config
     */
    public function getConfig(bool $hotConfig = false): array;

    /**
     * Get jsObject
     */
    public function getObject(JavascriptObjectInterface | string $object): ?JavascriptObjectInterface;

    /**
     * Save Config to Session
     */
    public function saveConfig(): array;
}
