<?php

namespace core\html\javascript\command;

use core\html\HtmlElementInterface;
use core\html\javascript\JavascriptCommandInterface;

/**
 * Class ChangeAttributeCommand
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class ChangeAttributeCommand extends AbstractCommand
{
    /**
     * Targeted id element
     *
     * @var string
     */
    public $target;

    public function __construct(
        string | HtmlElementInterface $target,
        public string $attribute,
        public string | JavascriptCommandInterface $value,
        public array $parameters = ['targetIsValuable' => false]
    ) {
        $this->target = (is_string($target)) ? $target : '#' . $target->getIdAttribute();
    }


    public function get(): string
    {
        return 'command/ChangeAttributeCommand';
    }
}
