<?php

namespace core\html\javascript\command;

use core\html\javascript\JavascriptCommandInterface;

class CommandSet extends AbstractCommand
{
    private array $commands = [];

    /**
     * Constuct a set of command
     *
     * @param JavascriptCommandInterface[] $commands
     */
    public function __construct(array $commands)
    {
        foreach ($commands as $command) {
            $this->add($command);
        }
    }

    public function add(JavascriptCommandInterface $command)
    {
        $this->commands[$command->get()] = $command;
    }


    public function get(): string
    {
        return 'command/CommandSet';
    }

    /**
     * Get the value of commands
     *
     * @PhpUnitGen\get("commands")
     */
    public function getCommands(): array
    {
        return $this->commands;
    }

    public function toArray(): array
    {
        $return = [];
        foreach ($this->commands as $key => $command) {
            $return[$key] = $command->toArray();
        }

        return $return;
    }
}
