<?php

namespace core\html\javascript\command;

use core\html\javascript\JavascriptObjectInterface;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class ExportWidgetCommand extends AbstractCommand
{
    public function __construct(public JavascriptObjectInterface $target)
    {
    }

    public function get(): string
    {
        return 'command/ExportWidgetCommand';
    }
}
