<?php

namespace core\html\javascript\command;

/**
 * Class EvalCommand
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class EvalCommand extends AbstractCommand
{
    public function __construct(public ?string $evalCode = null, public array $parameters = [])
    {
    }

    public function get(): string
    {
        return 'command/EvalCommand';
    }
}
