<?php

namespace core\html\javascript\command;

use core\html\javascript\JavascriptCommandInterface;
use core\utils\Utils;

/**
 * Class AbstractCommand
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
abstract class AbstractCommand implements JavascriptCommandInterface
{
    /**
     * @var string
     */
    protected $callback;

    /**
     * Get the value of callback
     *
     * @PhpUnitGen\get("callback")
     */
    public function getCallback(): string
    {
        return $this->callback;
    }

    /**
     * Set the value of callback
     *
     * @PhpUnitGen\set("callback")
     */
    public function setCallback(string $callback): self
    {
        $this->callback = $callback;

        return $this;
    }

    /**
     * Return path to command
     */
    abstract public function get(): string;

    public function toArray(): array
    {
        return Utils::arrayPrinter(get_object_vars($this), ['di', 'diWrapper']);
    }

    /**
     * Return command context
     */
    public function getContext(): string
    {
        return self::DEFAULT_CONTEXT;
    }
}
