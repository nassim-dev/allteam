<?php

namespace core\html\javascript;

use core\html\HtmlElementInterface;

/**
 * Interfacte JavascriptObjectInterface
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
interface JavascriptObjectInterface extends HtmlElementInterface
{
    /**
     * Add Post Action
     */
    public function addAction(string $action, ?JavascriptStateInterface $onState = null): Action;

    /**
     * Add config to execute
     */
    public function addConfig(string $identifier): Config;


    /**
     * Add Event listener
     */
    public function on(array | string $event): Event;

    /**
     * Reset jsConfig
     *
     * @return void
     */
    public function reset();



    /**
     * Return an Array for object jsConfig
     */
    public function toArray(): array;
}
