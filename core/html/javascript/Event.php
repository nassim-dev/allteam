<?php

namespace core\html\javascript;

use core\html\traits\Identifiable;
use core\html\traits\Tunable;

/**
 * Class Event
 *
 * Represent js event
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class Event
{
    use Identifiable;
    use Tunable;

    public const COMPONENT = 'component';

    public const EVENT = 'event';

    /**
     * Event Callback
     */
    private ?string $callback = null;

    /**
     * Event Command
     */
    private ?\core\html\javascript\JavascriptCommandInterface $command = null;

    /**
     * Event Trigger
     */
    private string $event = 'click';

    /**
     * Selector HTML
     */
    private ?string $selector = null;

    /**
     * Event target
     */
    private ?string $target = null;

    /**
     * Should be trig on shown
     */
    private bool $trigger = false;

    /**
     * Action Type
     *
     * @var string FUNCTION_EVAL|VALUE|FIELD_VALUE|null|CALLABLE
     */
    private string $type = 'CALLABLE';

    /**
     * @return static
     */
    public function __construct(array | string $event, private JavascriptObjectInterface $parent)
    {
        if (is_string($event)) {
            $this->setId($event);
            $this->on($event);
        }

        if (is_array($event)) {
            if (!isset($event[self::EVENT])) {
                throw new JavascriptComponentException('You must provide an array with these 2 keys : Event::EVENT (and optional Event::COMPONENT)');
            }

            if (!in_array(JavascriptStateInterface::class, class_implements($event[self::EVENT]))) {
                throw new JavascriptComponentException('Event::EVENT must be an object implementing ' . JavascriptStateInterface::class);
            }

            if (!in_array(JavascriptObjectInterface::class, class_implements($event[self::COMPONENT]))) {
                throw new JavascriptComponentException('Event::COMPONENT must be an object implementing ' . JavascriptObjectInterface::class);
            }

            /**
             * @var JavascriptObjectInterface $component
             */
            $component = $event[self::COMPONENT];
            $this->target($component);

            /**
             * @var JavascriptStateInterface $event
             */
            $event = $event[self::EVENT];
            $this->setId($event->get());
            $this->on($event->get());
        }
    }


    public function command(JavascriptCommandInterface $command): self
    {
        $this->callback = $command->get();
        $this->command  = $command;

        return $this;
    }

    public function on(string $event): self
    {
        $this->event = $event;

        return $this;
    }

    public function selector(?string $selector): self
    {
        $this->selector = $selector;

        return $this;
    }

    public function target(JavascriptObjectInterface | string $target): self
    {
        $this->target = (is_string($target)) ? $target : "#$target->getIdAttribute()";

        return $this;
    }

    /**
     * Return an Array of object
     */
    public function toArray(): array
    {
        return [
            'id'         => $this->getId(),
            'event'      => $this->event,
            'callback'   => $this->callback,
            'target'     => $this->target ?? '#' . $this->parent->getIdAttribute(),
            'parameters' => $this->getParameters(),
            'selector'   => $this->selector ?? '#' . $this->parent->getIdAttribute(),
            'trigger'    => $this->trigger,
            'command'    => $this->command->toArray()
        ];
    }

    public function trigger(bool $trigger): self
    {
        $this->trigger = $trigger;

        return $this;
    }
}
