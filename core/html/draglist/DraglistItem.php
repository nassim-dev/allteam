<?php

namespace core\html\draglist;

use core\html\HtmlElement;
use core\html\HtmlElementInterface;
use core\html\traits\Collapsable;
use core\html\traits\Stylizable;
use core\html\traits\WithActions;

/**
 * Class DraglistItem
 *
 * Description
 *
 * @category html_component
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class DraglistItem extends HtmlElement implements HtmlElementInterface
{
    use WithActions;
    use Stylizable;
    use Collapsable;

    public $due_date;

    public $type;

    public $title;

    public $file;

    public $performance;



    /**
     * List of allowed childs elements
     * could be a class, an abstract class or an interface
     */
    protected static function getAllowedChilds(): array
    {
        return [];
    }
}
