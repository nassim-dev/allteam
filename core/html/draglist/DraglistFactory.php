<?php

namespace core\html\draglist;

use core\factory\FactoryInterface;
use core\html\HtmlElementFactory;

/**
 * Undocumented class
 *
 * Description
 *
 * @example
 *
 * $element         = $this->create('test_draglist');
 *
 * $todo     = $element->addColumn(_('To do'), 'todo');
 * $todo->addClass('info');
 * $progress = $element->addColumn(_('Payment in progress'), 'progress');
 * $progress->addClass('warning');
 * $review = $element->addColumn(_('To review'), 'review');
 * $review->addClass('danger');
 * $done = $element->addColumn(_('Done'), 'done');
 * $done->addClass('success');
 *
 * $element->setTitle('My draglist');
 * $element->setActionBar($bar);
 * $status  = ['todo', 'progress', 'review', 'done'];
 * $items   = [];
 * $items[] = ['title' => $faker->email, 'content' => $faker->firstNameFemale, 'checked' => $status[rand(0, 3)]];
 * $items[] = ['title' => $faker->email, 'content' => $faker->firstNameFemale, 'checked' => $status[rand(0, 3)]];
 * $items[] = ['title' => $faker->email, 'content' => $faker->firstNameFemale, 'checked' => $status[rand(0, 3)]];
 * $items[] = ['title' => $faker->email, 'content' => $faker->firstNameFemale, 'checked' => $status[rand(0, 3)]];
 * $items[] = ['title' => $faker->email, 'content' => $faker->firstNameFemale, 'checked' => $status[rand(0, 3)]];
 * $items[] = ['title' => $faker->email, 'content' => $faker->firstNameFemale, 'checked' => $status[rand(0, 3)]];
 * $items[] = ['title' => $faker->email, 'content' => $faker->firstNameFemale, 'checked' => $status[rand(0, 3)]];
 * foreach ($items as $item) {
 * switch ($item['checked']) {
 * case 'todo':
 * $it = $todo->addItem($item);
 * $it->setActionBar($bar);
 *
 * break;
 * case 'progress':
 * $progress->addItem($item);
 *
 * break;
 * case 'review':
 * $review->addItem($item);
 *
 * break;
 * case 'done':
 * $done->addItem($item);
 *
 * break;
 * default:
 * }
 * }
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class DraglistFactory extends HtmlElementFactory implements FactoryInterface
{
    /**
     * Return element class to build
     */
    protected function getElementClass(): string
    {
        return Draglist::class;
    }

    /**
     * Return suffix for widget classes
     */
    protected function getWidgetSuffix(): string
    {
        return 'DraglistWidget';
    }

    /**
     * Create new instance or return singleton if exist
     *
     * @PhpUnitGen\assertInstanceOf("Draglist::class")
     */
    public function create(string $identifier, array $params = []): ?Draglist
    {
        $params['title'] ??= $identifier;

        return parent::create($identifier, $params);
    }

    /**
     * Get registred elements
     */
    public function get(string $identifier): ?Draglist
    {
        return $this->getInstance($identifier) ?? $this->create($identifier);
    }
}
