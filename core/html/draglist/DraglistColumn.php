<?php

namespace core\html\draglist;

use core\html\AbstractHtmlElement;

;
use core\html\traits\Identifiable;
use core\html\traits\Stylizable;
use core\html\traits\Titleable;

/**
 * Class DraglistColumn
 *
 * Drgalist column
 *
 * @category Drgalist column
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class DraglistColumn extends AbstractHtmlElement
{
    use Stylizable;
    use Identifiable;
    use Titleable;

    /**
     * Column data-attr html
     *
     * @var string
     */
    public $column = '';

    /**
     * Constructor
     *
     * @return static
     */
    public function __construct(string $title, string $identifier)
    {
        $this->setTitle($title);
        $this->setId($identifier . '_' . uniqid());
        $this->column = $identifier;
    }

    /**
     * Add new item
     *
     * @param array $item ["type" => EntitieInterface::class, "due_date" => DateTime formated, "title" => string, "content" => string]
     */
    public function addItem(array $item): DraglistItem
    {
        $item = new DraglistItem($item);

        return $this->appendChild($item);
    }

    /**
     * Return Id attribute for html element
     */
    public function getIdAttribute(): string
    {
        return $this->getViewReference() . '_' . $this->getId();
    }

    /**
     * List of allowed childs elements
     * could be a class, an abstract class or an interface
     */
    protected static function getAllowedChilds(): array
    {
        return [DraglistItem::class];
    }

    /**
     * Return all childs ids concatenated
     */
    public function getChildsIds(): string
    {
        $return = '';

        /** @var DraglistItem $child */
        foreach ($this->getChilds() as $child) {
            $return .= $child->getIdAttribute() . ' ';
        }

        return $return;
    }
}
