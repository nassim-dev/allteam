<?php

namespace core\html\draglist;

use core\html\AbstractHtmlElement;
use core\html\javascript\JavascriptObjectInterface;
use core\html\javascript\WithJavascript;
use core\html\traits\AjaxSourced;
use core\html\traits\Collapsable;
use core\html\traits\Identifiable;
use core\html\traits\Titleable;
use core\html\traits\WithActions;

/**
 * Class Draglist
 *
 * DraglistElement
 *
 * @category DraglistElement
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Draglist extends AbstractHtmlElement implements JavascriptObjectInterface
{
    use WithJavascript;
    use Titleable;
    use Identifiable;
    use WithActions;
    use Collapsable;
    use AjaxSourced;

    /**
     * Related columns
     *
     * @var array
     */
    public $elements = [];

    /**
     * Template for items
     *
     * @var string
     */
    public $template = 'todo';

    /**
     * Constructor
     *
     * @PhpUnitGen\assertInstanceOf("Draglist::class")
     *
     * @return static
     */
    public function __construct(string $title)
    {
        //Assets::addDragula();
        $this->setTitle($title);
        $this->setId($title);
    }

    /**
     * Add new column
     */
    public function addColumn(string $title, string $id): DraglistColumn
    {
        $draglistItem = new DraglistColumn($title, $id);

        return $this->appendChild($draglistItem);
    }

    /**
     * Return Id attribute for html element
     */
    public function getIdAttribute(): string
    {
        return $this->getViewReference() . '_' . $this->getId();
    }

    /**
     * List of allowed childs elements
     * could be a class, an abstract class or an interface
     */
    protected static function getAllowedChilds(): array
    {
        return [DraglistColumn::class];
    }
}
