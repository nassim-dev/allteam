<?php

namespace core\html\draglist\formatters;

use core\entities\EntitieInterface;
use core\html\AbstractFormatter;
use core\html\FormatterInterface;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class DraglistFormatter extends AbstractFormatter implements FormatterInterface
{
    public function format(EntitieInterface $entitie)
    {
        return null;
    }
}
