<?php

namespace core\html\carousel;

use core\html\AbstractHtmlElement;
use core\html\traits\Activeable;
use core\html\traits\Captionable;
use core\html\traits\Identifiable;
use core\html\traits\Stylizable;

/**
 * Class CarouselItem
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class CarouselItem extends AbstractHtmlElement
{
    use Identifiable;
    use Stylizable;
    use Activeable;
    use Captionable;

    /**
     * Alt attribute
     */
    private ?string $alt = null;


    /**
     * Constructor
     */
    public function __construct(private string $src, int $id, bool $active = false)
    {
        $this->setId($id);
        $this->active = $active;
    }



    /**
     * Get alt attribute
     *
     * @PhpUnitGen\get("alt")
     */
    public function getAlt(): string
    {
        return $this->alt;
    }



    /**
     * Return Id attribute for html element
     */
    public function getIdAttribute(): string
    {
        return $this->getViewReference() . '_' . $this->getId();
    }

    /**
     * Get boy element
     *
     * @PhpUnitGen\get("src")
     */
    public function getSrc(): string
    {
        return $this->src;
    }


    /**
     * Set alt attribute
     *
     * @PhpUnitGen\set("alt")
     *
     * @param string $alt Alt attribute
     */
    public function setAlt(string $alt): self
    {
        $this->alt = $alt;

        return $this;
    }



    /**
     * Set source element
     *
     * @PhpUnitGen\set("src")
     *
     * @param string $src Source element
     */
    public function setSrc(string $src): self
    {
        $this->src = $src;

        return $this;
    }

    /**
     * List of allowed childs elements
     * could be a class, an abstract class or an interface
     */
    protected static function getAllowedChilds(): array
    {
        return [];
    }
}
