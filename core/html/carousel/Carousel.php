<?php

namespace core\html\carousel;

use core\html\AbstractHtmlElement;
use core\html\traits\Identifiable;
use core\html\traits\Stylizable;

/**
 * Class Carousel
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class Carousel extends AbstractHtmlElement
{
    use Identifiable;
    use Stylizable;

    private int $ids = 0;

    private bool $buttons = false;

    public function __construct(string $title)
    {
        $this->title = $title;
        $this->setId(uniqid());
    }

    public function addSlide(string $src, string $caption, string $alt, bool $active = false): CarouselItem
    {
        $item = new CarouselItem($src, $this->ids++, $active);
        $item->setCaption($caption)
            ->setAlt($alt);

        return $this->appendChild($item);
    }

    /**
     * Return Id attribute for html element
     */
    public function getIdAttribute(): string
    {
        return $this->getViewReference() . '_' . $this->getId();
    }

    /**
     * List of allowed childs elements
     * could be a class, an abstract class or an interface
     */
    protected static function getAllowedChilds(): array
    {
        return [CarouselItem::class];
    }

    /**
     * Get the value of buttons
     *
     * @PhpUnitGen\get("buttons")
     */
    public function hasButtons(): bool
    {
        return $this->buttons;
    }

    public function activateButtons()
    {
        $this->buttons = true;
    }

    public function deactivateButtons()
    {
        $this->buttons = false;
    }
}
