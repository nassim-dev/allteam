<?php

namespace core\html\carousel;

use core\factory\FactoryInterface;
use core\html\HtmlElementFactory;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class CarouselFactory extends HtmlElementFactory implements FactoryInterface
{
    /**
     * Return element class to build
     */
    protected function getElementClass(): string
    {
        return Carousel::class;
    }

    /**
     * Return suffix for widget classes
     */
    protected function getWidgetSuffix(): string
    {
        return 'CarouselWidget';
    }

    /**
     * Create new instance or return singleton if exist
     *
     * @PhpUnitGen\assertInstanceOf("Carousel::class")
     */
    public function create(string $identifier, array $params = []): ?Carousel
    {
        $params['title'] ??= $identifier;
        $params['method'] = $params['title'] ?? $identifier;

        return parent::create($identifier, $params);
    }

    /**
     * Get registred elements
     */
    public function get(string $identifier): ?Carousel
    {
        return $this->getInstance($identifier) ?? $this->create($identifier);
    }
}
