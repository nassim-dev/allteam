<?php

namespace core\html\label;

use core\html\AbstractHtmlElement;
use core\html\javascript\JavascriptObjectInterface;
use core\html\javascript\WithJavascript;
use core\html\traits\DataAttribute;
use core\html\traits\Iconable;
use core\html\traits\Identifiable;
use core\html\traits\Stylizable;

/**
 * Class Label
 *
 * Represents Html label
 *
 * @category  Represents Html label
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Label extends AbstractHtmlElement implements JavascriptObjectInterface
{
    use WithJavascript;
    use Iconable;
    use Identifiable;
    use DataAttribute;
    use Stylizable;

    public const DANGER = 'danger';

    public const INFO = 'info';

    public const SUCCESS = 'success';

    public const WARNING = 'warning';

    public const ALIGN_LEFT = 'ALIGN_LEFT';

    public const ALIGN_RIGHT = 'ALIGN_RIGHT';

    public const ALIGN_CENTER = 'ALIGN_CENTER';

    public const NO_ICON = null;

    /**
     * Allow click action
     *
     * @var boolean
     */
    public $allowClick = false;

    /**
     * Constructor
     *
     * @PhpUnitGen\assertInstanceOf("Label::class")
     *
     * @return static
     */
    public function __construct(public ?string $text = null, ?string $icon = null, public ?string $type = self::SUCCESS)
    {
        $this->setIcon($icon);
        $this->setId(uniqid());
    }



    /**
     * Return Id attribute for html element
     */
    public function getIdAttribute(): string
    {
        return $this->getViewReference() . '_' . $this->getId();
    }

    /**
     * enerate a label
     *
     * @param string|null $text
     */
    public static function helper(string $text = null, ?string $icon = null, ?string $type = self::SUCCESS, ?array $attributes = null): string
    {
        $label = new self(
            $text ?? _('Undefined'),
            $icon,
            $type
        );
        if ($attributes !== null) {
            $label->addDataAttributes($attributes);
        }

        return $label->getHtml();
    }

    /**
     * Return jsRef
     *
     * @return string
     */
    public function jsRef()
    {
        return (!$this->classObject) ? $this->getIdAttribute() : $this->classObject;
    }

    /**
     * List of allowed childs elements
     * could be a class, an abstract class or an interface
     */
    protected static function getAllowedChilds(): array
    {
        return [];
    }
}
