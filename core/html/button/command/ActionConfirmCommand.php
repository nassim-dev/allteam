<?php

namespace core\html\button\command;

use core\html\javascript\command\AbstractCommand;

/**
 * Class ActionConfirmCommand
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class ActionConfirmCommand extends AbstractCommand
{
    public function __construct(public array $parameters = [])
    {
    }

    public function get(): string
    {
        return 'components/button/ActionConfirmCommand';
    }

    public function getContext(): string
    {
        return 't-button';
    }
}
