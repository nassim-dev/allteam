<?php

namespace core\html\button;

use core\html\AbstractHtmlElement;
use core\html\javascript\JavascriptObjectInterface;
use core\html\javascript\WithJavascript;
use core\html\traits\DataAttribute;
use core\html\traits\Deactivable;
use core\html\traits\Iconable;
use core\html\traits\Identifiable;
use core\html\traits\Labelable;
use core\html\traits\Stylizable;

/**
 * Class Button
 *
 * Represent html button
 *
 * @category  html_component
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Button extends AbstractHtmlElement implements JavascriptObjectInterface
{
    use WithJavascript;
    use Deactivable;
    use Iconable;
    use Labelable;
    use Identifiable;
    use DataAttribute;
    use Stylizable;

    /**
     * All Ids
     */
    private static int $ids = 0;

    /**
     * Constructor Action
     *
     * @return static
     */
    public function __construct(string $label, ?string $idbar = null)
    {
        $this->setId($idbar . '_' . uniqid());
        $this->setLabel($label);
        ++self::$ids;
    }




    /**
     * Return Id attribute for html element
     */
    public function getIdAttribute(): string
    {
        return $this->getViewReference() . '_' . $this->getId();
    }



    /**
     * List of allowed childs elements
     * could be a class, an abstract class or an interface
     */
    protected static function getAllowedChilds(): array
    {
        return [];
    }

    public function toArray(): array
    {
        $array         = parent::toArray();
        $array['icon'] = $this->getIcon();

        return $array;
    }
}
