<?php

namespace core\html;

use Exception;

/**
 * Class AssetException
 *
 * Trable exception
 *
 * @category Trable exception
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class AssetException extends Exception
{
}
