<?php

namespace core\html\lists;

use core\html\AbstractHtmlElement;
use core\html\traits\Iconable;
use core\html\traits\Identifiable;
use core\html\traits\Stylizable;

/**
 * Clas LiElement
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class LiElement extends AbstractHtmlElement
{
    use Stylizable;
    use Iconable;
    use Identifiable;

    /**
     * Badge content
     *
     * @var string
     */
    public $badge = null;

    /**
     * Childs Elements
     *
     * @var mixed
     */
    public $content = null;

    /**
     * @return static
     */
    public function __construct()
    {
        $this->setId(uniqid());
    }

    /**
     * Get badge content
     */
    public function getBadge(): string
    {
        return $this->badge;
    }

    /**
     * Get childs Elements
     */
    public function getContent(): mixed
    {
        return $this->content;
    }

    /**
     * Return Id attribute for html element
     */
    public function getIdAttribute(): string
    {
        return $this->getViewReference() . '_' . $this->getId();
    }

    /**
     * Set badge content
     *
     * @param string $badge Badge content
     */
    public function setBadge(string $badge): self
    {
        $this->badge = $badge;

        return $this;
    }

    /**
     * Set childs Elements
     *
     * @param mixed $content Childs Elements
     */
    public function setContent(mixed $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * List of allowed childs elements
     * could be a class, an abstract class or an interface
     */
    protected static function getAllowedChilds(): array
    {
        return [];
    }
}
