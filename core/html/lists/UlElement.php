<?php

namespace core\html\lists;

use core\html\AbstractHtmlElement;
use core\html\HtmlElementContainer;
use core\html\traits\Identifiable;
use core\html\traits\Stylizable;

/**
 * Clas UlElement
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class UlElement extends AbstractHtmlElement
{
    use Stylizable;
    use Identifiable;

    /**
     * @return static
     */
    public function __construct(string $identifier)
    {
        $this->setId($identifier);
    }

    /**
     * Set childs Elements
     *
     * @param array $liElements Childs Elements
     */
    public function addChild($content): LiElement
    {
        $element = new LiElement();
        $element->setContent($content);

        return $this->appendChild($element);
    }

    /**
     * Return Id attribute for html element
     */
    public function getIdAttribute(): string
    {
        return $this->getViewReference() . '_' . $this->getId();
    }

    /**
     * Get childs Elements
     */
    public function getLiElements(): HtmlElementContainer
    {
        return $this->getChilds();
    }

    /**
     * List of allowed childs elements
     * could be a class, an abstract class or an interface
     */
    protected static function getAllowedChilds(): array
    {
        return [LiElement::class];
    }
}
