<?php

namespace core\html\menu;

use core\html\AbstractHtmlElement;
use core\html\traits\Clickable;
use core\html\traits\DataAttribute;
use core\html\traits\Deactivable;
use core\html\traits\Iconable;
use core\html\traits\Identifiable;
use core\html\traits\Titleable;

/**
 * Class AbstractMenuElement
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
abstract class AbstractMenuElement extends AbstractHtmlElement
{
    use Iconable;
    use Titleable;
    use DataAttribute;
    use Deactivable;
    use Identifiable;
    use Clickable;

    public const CENTER = 2;

    public const LEFT = 1;

    public const RIGHT = 3;



    public function __construct(string $title, string $href = '#', ?string $icon = null)
    {
        $this->setTitle($title);
        $this->setIcon($icon);
        $this->href = $href;

        $this->setId(uniqid());
    }


    /**
     * Return Id attribute for html element
     */
    public function getIdAttribute(): string
    {
        return $this->getViewReference() . '_' . $this->getId();
    }
}
