<?php

namespace core\html\menu;

use core\factory\FactoryException;
use core\factory\FactoryInterface;
use core\html\HtmlElementFactory;

/**
 * Class MenuFactory
 *
 * Menu Factory
 *
 * @category  Menu Factory
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class MenuFactory extends HtmlElementFactory implements FactoryInterface
{
    public const SIDETOP = Menu::SIDETOP;

    public const SIDEBOTTOM = Menu::SIDEBOTTOM;

    public const TOPCENTER = Menu::TOPCENTER;

    public const TOPLEFT = Menu::TOPLEFT;

    public const TOPRIGHT = Menu::TOPRIGHT;

    /**
     * Return element class to build
     */
    protected function getElementClass(): string
    {
        return Menu::class;
    }

    /**
     * Return suffix for widget classes
     */
    protected function getWidgetSuffix(): string
    {
        return 'MenuWidget';
    }

    /**
     * Create new instance or return singleton if exist
     *
     * @PhpUnitGen\assertInstanceOf("Menu::class")
     */
    public function create(string $identifier, array $params = []): ?Menu
    {
        $params['identifier'] ??= $identifier;
        if (!isset($params['location'])) {
            throw new FactoryException("You must specify a 'location' in parameters");
        }

        if ($params['location'] === self::SIDEBOTTOM || $params['location'] === self::SIDETOP) {
            throw new FactoryException('Available location are : Menu::TOPRIGHT, Menu::TOPLEFT, Menu::TOPCENTER');
        }

        return parent::create($identifier, $params);
    }

    public function getMainMenu(): ?Menu
    {
        $element = $this->getInstance('__main__');
        if (null === $element) {
            /** @var Menu $element */
            $element = $this->createThen('__main__', function ($element): void {
                $element->setLocation(self::SIDETOP);
            }, ['identifier' => '__main__', 'location' => self::SIDETOP]);
        }

        return $element;
    }

    public function createThen(string $identifier, callable $callback, array $params = [])
    {
        $instance = parent::create($identifier, $params);

        if ($instance !== null) {
            $callback($instance);

            return $instance;
        }

        return null;
    }


    /**
     * @return mixed
     */
    public function getShortcutsMenu(): ?Menu
    {
        $element = $this->getInstance('__shortcuts__');
        if (null === $element) {
            /** @var Menu $element */
            $element  = parent::create('__shortcuts__', ['identifier' => '__shortcuts__', 'location' => self::SIDEBOTTOM]);
            $dropdown = $element->addDropDown(_('Shortcuts'), '#', 'fa-star');

            $dropdown->setTitle(_('Bookmarks'));
            $dropdown->addItem(_('Mark this page'), '#', 'fa-plus-square');
        }

        return $element;
    }

    public function getUserMenu(): ?Menu
    {
        $element = $this->getInstance('__user__');
        if (null === $element) {
            /** @var Menu $element */
            $element  = parent::create('__user__', ['identifier' => '__user__', 'location' => self::TOPRIGHT]);
            $dropdown = $element->addDropDown(_('User menu'), '#', 'fa-user');

            $dropdown->setTitle(_('Hello') . ' ' . $this->auth->getUser()?->first_name);
            $dropdown->addItem(_('Network'), '#', 'fa-network-wired');
            $dropdown->addItem(_('Settings'), '#', 'fa-cogs');
            $dropdown->addItem(_('Profile'), '#', 'fa-address-card');
            $dropdown->addItem(_('Logout'), '/logout', 'fa-power-off');
        }

        return $element;
    }

    public function getMessengerMenu(): ?Menu
    {
        $element = $this->getInstance('__messenger__');
        if (null === $element) {
            /** @var Menu $element */
            $element = parent::create('__messenger__', ['identifier' => '__messenger__', 'location' => self::TOPRIGHT]);
            $item    = $element->addItem('', '#', 'fa-comments');
            $item->setHtmlTemplate($item::getBaseTemplateDir() . 'menu/MenuItemTitle.html');
            $item->setId('__messenger__');
        }

        return $element;
    }

    /**
     * Get registred elements
     */
    public function get(string $identifier): ?Menu
    {
        return $this->getInstance($identifier) ?? $this->create($identifier);
    }
}
