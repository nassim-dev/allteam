<?php

namespace core\html\menu;

use core\html\AbstractHtmlElement;
use core\html\traits\Identifiable;

/**
 * Class Menu
 *
 * Menu element
 *
 * @category  Menu element
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Menu extends AbstractHtmlElement
{
    use Identifiable;

    public const SIDETOP = 5;

    public const SIDEBOTTOM = 6;

    public const TOPCENTER = 7;

    public const TOPLEFT = 8;

    public const TOPRIGHT = 9;

    private array $dropdowns = [];

    private array $items = [];

    /**
     * Menu constructor.
     *
     * @PhpUnitGen\assertInstanceOf("Menu::class")
     */
    public function __construct(string $identifier, private int $location = self::TOPCENTER)
    {
        $this->htmlTemplate = self::getBaseTemplateDir() . 'menu/Menu.html';
        $this->setId($identifier);
    }

    /**
     * Add Dropdown element
     */
    public function addDropDown(string $title, string $href = '#', ?string $icon = null): MenuDropDown
    {
        $this->dropdowns[$title] ??= new MenuDropDown($title, $href, $icon);
        $this->dropdowns[$title]->setLocation($this->getLocation());

        return $this->appendChild($this->dropdowns[$title]);
    }

    /**
     * Add Item element
     */
    public function addItem(string $title, string $href = '#', ?string $icon = null): MenuItem
    {
        $this->items[$title] ??= new MenuItem($title, $href, $icon);
        $this->items[$title]->setLocation($this->getLocation());

        return $this->appendChild($this->items[$title]);
    }

    /**
     * Return Id attribute for html element
     */
    public function getIdAttribute(): string
    {
        return $this->getViewReference() . '_' . $this->getId();
    }

    /**
     * Get menu location
     *
     * @PhpUnitGen\get("location")
     */
    public function getLocation(): int
    {
        return $this->location;
    }


    /**
     * Set menu Elements
     *
     * @PhpUnitGen\set("_elements")
     *
     * @param array $elements Menu Elements
     */
    public function setElements(array $elements): self
    {
        foreach ($elements as $element) {
            $element->setLocation($this->getLocation());
            $this->appendChild($element);
        }

        return $this;
    }

    /**
     * Set menu location
     *
     * @PhpUnitGen\set("location")
     *
     * @param int $location Menu location
     */
    public function setLocation(int $location): self
    {
        $sideEmplacements = [self::SIDETOP, self::SIDEBOTTOM];
        if (in_array($location, $sideEmplacements)) {
            $this->setHtmlTemplate(self::getBaseTemplateDir() . 'menu/SideMenu.html');
        } else {
            $this->setHtmlTemplate(self::getBaseTemplateDir() . 'menu/Menu.html');
        }

        foreach ($this->getChilds() as $child) {
            $child->setLocation($location);
        }

        $this->location = $location;

        return $this;
    }

    /**
     * List of allowed childs elements
     * could be a class, an abstract class or an interface
     */
    protected static function getAllowedChilds(): array
    {
        return [MenuDropDown::class, MenuItem::class];
    }
}
