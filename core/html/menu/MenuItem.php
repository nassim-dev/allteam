<?php

namespace core\html\menu;

use core\html\traits\Deactivable;
use core\html\traits\Iconable;
use core\html\traits\Labelable;

/**
 * Class MenuItem
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class MenuItem extends AbstractMenuElement
{
    use Deactivable;
    use Labelable;
    use Iconable;


    public function __construct(string $title, string $href = '#', ?string $icon = null)
    {
        $this->htmlTemplate = self::getBaseTemplateDir() . 'menu/MenuItem.html';

        parent::__construct($title, $href, $icon);
    }

    /**
     * List of allowed childs elements
     * could be a class, an abstract class or an interface
     */
    protected static function getAllowedChilds(): array
    {
        return [];
    }

    /**
     * Set menu location
     *
     * @PhpUnitGen\set("location")
     *
     * @param int $location Menu location
     */
    public function setLocation(int $location): self
    {
        $sideEmplacements = [Menu::SIDETOP, Menu::SIDEBOTTOM];
        if (in_array($location, $sideEmplacements)) {
            $this->setHtmlTemplate(self::getBaseTemplateDir() . 'menu/SideMenuItem.html');
        } else {
            $this->setHtmlTemplate(self::getBaseTemplateDir() . 'menu/MenuItem.html');
        }

        return $this;
    }
}
