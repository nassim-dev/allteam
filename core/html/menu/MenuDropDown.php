<?php

namespace core\html\menu;

use core\html\treeview\Treeview;

/**
 * Class MenuDropDown
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class MenuDropDown extends AbstractMenuElement
{
    public const SIDETOP = Menu::SIDETOP;

    public const SIDEBOTTOM = Menu::SIDEBOTTOM;

    public const TOPCENTER = Menu::TOPCENTER;

    public const TOPLEFT = Menu::TOPLEFT;

    public const TOPRIGHT = Menu::TOPRIGHT;

    private $location = null;

    private array $items = [];

    private array $treeviews = [];


    public function __construct(string $title, string $href = '#', ?string $icon = null)
    {
        $this->htmlTemplate = self::getBaseTemplateDir() . 'menu/MenuDropDown.html';

        parent::__construct($title, $href, $icon);
    }

    /**
     * Add Item element
     */
    public function addItem(string $title, string $href = '#', ?string $icon = null): MenuItem
    {
        $this->items[$title] ??= new MenuItem($title, $href, $icon);
        $this->items[$title]->setLocation($this->getLocation());

        return $this->appendChild($this->items[$title]);
    }

    public function addTreeview(Treeview $treeview): Treeview
    {
        $this->appendChild($treeview);

        return $treeview;
    }



    /**
     * Set menu Elements
     *
     * @PhpUnitGen\set("_elements")
     *
     * @param array $elements Menu Elements
     */
    public function setElements(array $elements): self
    {
        foreach ($elements as $element) {
            $element->setLocation($this->getLocation());
            $this->appendChild($element);
        }

        return $this;
    }

    /**
     * Set menu location
     *
     * @param int $location Menu location
     */
    public function setLocation(int $location): self
    {
        if ($location === Menu::SIDETOP) {
            $this->setHtmlTemplate(self::getBaseTemplateDir() . 'menu/SideMenuDropDown.html');
        } elseif ($location === Menu::SIDEBOTTOM) {
            $this->setHtmlTemplate(self::getBaseTemplateDir() . 'menu/SideMenuDropDownBottom.html');
        } else {
            $this->setHtmlTemplate(self::getBaseTemplateDir() . 'menu/MenuDropDown.html');
        }

        foreach ($this->getChilds() as $child) {
            if ($child instanceof Treeview) {
                continue;
            }

            $child->setLocation($location);
        }

        $this->location = $location;

        return $this;
    }

    /**
     * List of allowed childs elements
     * could be a class, an abstract class or an interface
     */
    protected static function getAllowedChilds(): array
    {
        return [MenuItem::class, Treeview::class];
    }

    /**
     * Get menu location
     *
     * @PhpUnitGen\get("location")
     */
    public function getLocation(): int
    {
        return $this->location;
    }
}
