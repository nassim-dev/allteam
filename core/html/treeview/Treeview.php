<?php

namespace core\html\treeview;

use core\html\AbstractHtmlElement;
use core\html\traits\Identifiable;
use core\html\traits\Stylizable;
use core\html\traits\Titleable;
use core\html\traits\WithActions;

/**
 *  Treeview
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class Treeview extends AbstractHtmlElement
{
    use Titleable;
    use WithActions;
    use Stylizable;
    use Identifiable;

    /**
     * Constructor
     */
    public function __construct(string $title = '')
    {
        $this->setTitle($title);
        $this->setId(uniqid());
    }

    public function addCategorie(string $title, ?string $icon = null): TreeviewCategorie
    {
        $categorie = new TreeviewCategorie($title, $icon);

        return $this->appendChild($categorie);
    }

    /**
     * Return Id attribute for html element
     */
    public function getIdAttribute(): string
    {
        return $this->getViewReference() . '_' . $this->getId();
    }

    /**
     * List of allowed childs elements
     * could be a class, an abstract class or an interface
     */
    protected static function getAllowedChilds(): array
    {
        return [TreeviewCategorie::class];
    }
}
