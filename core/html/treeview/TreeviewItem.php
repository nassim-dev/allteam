<?php

namespace core\html\treeview;

use core\html\AbstractHtmlElement;
use core\html\HtmlElement;
use core\html\HtmlElementInterface;
use core\html\HtmlException;
use core\html\traits\Clickable;
use core\html\traits\Deactivable;
use core\html\traits\Iconable;
use core\html\traits\Identifiable;
use core\html\traits\Titleable;

/**
 *  TreeviewItem
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class TreeviewItem extends AbstractHtmlElement
{
    use Titleable;
    use Iconable;
    use Identifiable;
    use Deactivable;
    use Clickable;

    /**
     * Constructor
     */
    public function __construct(string $title, ?string $icon = null)
    {
        $this->setTitle($title);
        $this->setIcon($icon);
        $this->setId(uniqid());
    }

    public function addItem(string $title, ?string $icon = null): TreeviewItem
    {
        $item = new TreeviewItem($title, $icon);

        return $this->appendChild($item);
    }

    /**
     * Add content
     *
     * @return HtmlElementInterface|null
     * @throws HtmlException
     */
    public function addContent(string | HtmlElementInterface $element)
    {
        $element = (is_string($element)) ? new HtmlElement(['content' => $element]) : $element;
        //$element->setHtmlTemplate(self::getBaseTemplateDir() . 'treeview/TreeviewContent.html');
        if (self::class) {
            return $this->appendChild($element);
        }
    }

    /**
     * Return Id attribute for html element
     */
    public function getIdAttribute(): string
    {
        return $this->getViewReference() . '_' . $this->getId();
    }

    /**
     * List of allowed childs elements
     * could be a class, an abstract class or an interface
     */
    protected static function getAllowedChilds(): array
    {
        return [self::class, HtmlElementInterface::class];
    }
}
