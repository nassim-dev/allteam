<?php

namespace core\html\treeview;

use core\html\AbstractHtmlElement;
use core\html\traits\Iconable;
use core\html\traits\Identifiable;
use core\html\traits\Stylizable;
use core\html\traits\Titleable;

/**
 *  TreeviewCategorie
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class TreeviewCategorie extends AbstractHtmlElement
{
    use Titleable;
    use Iconable;
    use Stylizable;
    use Identifiable;

    /**
     * Constructor
     */
    public function __construct(string $title, ?string $icon = null)
    {
        $this->setTitle($title);
        $this->setIcon($icon);
        $this->setId(uniqid());
    }

    public function addItem(string $title, ?string $icon = null): TreeviewItem
    {
        $item = new TreeviewItem($title, $icon);

        return $this->appendChild($item);
    }

    /**
     * Return Id attribute for html element
     */
    public function getIdAttribute(): string
    {
        return $this->getViewReference() . '_' . $this->getId();
    }

    /**
     * List of allowed childs elements
     * could be a class, an abstract class or an interface
     */
    protected static function getAllowedChilds(): array
    {
        return [TreeviewItem::class];
    }
}
