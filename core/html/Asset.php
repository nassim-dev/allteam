<?php namespace core\html;

/**
 * Class Asset
 *
 *  @method static string addCss(string $file, bool $inBody)
 *  @method static string addCssInline(string $style, bool $inBody)
 *  @method static string addJs(string $file, bool $inBody)
 *  @method static string addJsInline(string $script, bool $inBody)
 *  @method static array getCss()
 *  @method static array getJs()
 *  @method static string getBody()
 *  @method static string getHead()
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class Asset extends \core\facade\HelperFacade
{
    protected static ?string $serviceInterface = 'core\html\AssetsServiceInterface';
}
