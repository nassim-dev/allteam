<?php

namespace core\html\tables\priceTable;

use core\html\AbstractHtmlElement;
use core\html\traits\Stylizable;
use core\utils\Utils;

/**
 * Class PriceTableElement
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class PriceTableElement extends AbstractHtmlElement
{
    use Stylizable;

    private ?string $duration = null;

    private ?array $link = null;

    private bool $popular = false;

    private ?float $price = null;

    public function __construct(private string $subtitle)
    {
    }

    /**
     * Add new element
     *
     * @param string      $quantity Number
     * @param string      $feature  Text
     * @param string|null $link     Add link
     */
    public function addElement(string $quantity, string $feature, ?string $link = null): self
    {
        $feature = new PriceTableFeature($quantity, $feature, $link);
        $this->appendChild($feature);

        return $this;
    }

    /**
     * Get the value of duration
     *
     * @PhpUnitGen\get("duration")
     */
    public function getDuration(): string
    {
        return $this->duration;
    }

    /**
     * Return Id attribute for html element
     */
    public function getIdAttribute(): string
    {
        return $this->getViewReference() . '_' . Utils::hash($this->subtitle);
    }

    /**
     * Get the value of link
     *
     * @PhpUnitGen\get("link")
     */
    public function getLink(): ?array
    {
        return $this->link;
    }

    /**
     * Get the value of popular
     *
     * @PhpUnitGen\get("popular")
     */
    public function getPopular(): bool
    {
        return $this->popular;
    }

    /**
     * Get the value of price
     *
     * @PhpUnitGen\get("price")
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * Get the value of subtitle
     *
     * @PhpUnitGen\get("subtitle")
     */
    public function getSubtitle(): string
    {
        return $this->subtitle;
    }

    /**
     * Set the value of duration
     *
     * @PhpUnitGen\set("duration")
     */
    public function setDuration(?string $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Set the value of link
     *
     * @PhpUnitGen\set("link")
     * @param mixed[]|null $link
     */
    public function setLink(?array $link): self
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Set the value of popular
     *
     * @PhpUnitGen\set("popular")
     */
    public function setPopular(bool $popular): self
    {
        $this->popular = $popular;

        return $this;
    }

    /**
     * Set the value of price
     *
     * @PhpUnitGen\set("price")
     */
    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Set the value of subtitle
     *
     * @PhpUnitGen\set("subtitle")
     */
    public function setSubtitle(string $subtitle): self
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    /**
     * List of allowed childs elements
     * could be a class, an abstract class or an interface
     */
    protected static function getAllowedChilds(): array
    {
        return [PriceTableFeature::class];
    }
}
