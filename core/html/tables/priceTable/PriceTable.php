<?php

namespace core\html\tables\priceTable;

use core\html\AbstractHtmlElement;
use core\html\traits\Identifiable;
use core\html\traits\Titleable;

/**
 * Class PriceTable
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class PriceTable extends AbstractHtmlElement
{
    use Identifiable;
    use Titleable;

    /**
     * PriceTable constructor.
     */
    public function __construct(string $title, ?string $identifier = null)
    {
        $this->setTitle($title);
        $this->setId($identifier ?? uniqid());
        //Assets::addPriceTable();
    }

    /**
     * Add new
     */
    public function addPill(string $title): PriceTableElement
    {
        $object = new PriceTableElement($title);

        return $this->appendChild($object);
    }

    /**
     * Return Id attribute for html element
     */
    public function getIdAttribute(): string
    {
        return $this->getViewReference() . '_' . $this->getId();
    }

    /**
     * List of allowed childs elements
     * could be a class, an abstract class or an interface
     */
    protected static function getAllowedChilds(): array
    {
        return [PriceTableElement::class];
    }
}
