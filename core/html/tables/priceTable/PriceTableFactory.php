<?php

namespace core\html\tables\priceTable;

use core\factory\FactoryInterface;
use core\html\HtmlElementFactory;

/**
 * Class PricePriceTableFactory
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class PriceTableFactory extends HtmlElementFactory implements FactoryInterface
{
    /**
     * Return element class to build
     */
    protected function getElementClass(): string
    {
        return PriceTable::class;
    }

    /**
     * Return suffix for widget classes
     */
    protected function getWidgetSuffix(): string
    {
        return 'PriceTableWidget';
    }

    /**
     * Create new instance or return singleton if exist
     *
     * @PhpUnitGen\assertInstanceOf("PriceTable::class")
     */
    public function create(string $identifier, array $params = []): ?PriceTable
    {
        $params['title'] ??= $identifier;

        return parent::create($identifier, $params);
    }

    /**
     * Get registred elements
     */
    public function get(string $identifier): ?PriceTable
    {
        return $this->getInstance($identifier) ?? $this->create($identifier);
    }
}
