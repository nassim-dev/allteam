<?php

namespace core\html\tables\priceTable;

use core\html\AbstractHtmlElement;
use core\utils\Utils;

/**
 * Class PriceTableFeature
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class PriceTableFeature extends AbstractHtmlElement
{
    public function __construct(private string $quantity, private string $feature, private ?string $link)
    {
    }

    /**
     * Get feature
     *
     * @PhpUnitGen\get("feature")
     */
    public function getFeature(): string
    {
        return $this->feature;
    }

    /**
     * Return Id attribute for html element
     */
    public function getIdAttribute(): string
    {
        return $this->getViewReference() . '_' . Utils::hash($this->feature);
    }

    /**
     * Get link
     *
     * @PhpUnitGen\get("link")
     */
    public function getLink(): ?string
    {
        return $this->link;
    }

    /**
     * Get quantity
     *
     * @PhpUnitGen\get("quantity")
     */
    public function getQuantity(): string
    {
        return $this->quantity;
    }

    /**
     * Set feature
     *
     * @PhpUnitGen\set("feature")
     *
     * @param string $feature Feature
     */
    public function setFeature(string $feature): self
    {
        $this->feature = $feature;

        return $this;
    }

    /**
     * Set link
     *
     * @PhpUnitGen\set("link")
     *
     * @param string|null $link Link
     */
    public function setLink(?string $link): self
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Set quantity
     *
     * @PhpUnitGen\set("quantity")
     *
     * @param string $quantity Quantity
     */
    public function setQuantity(string $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * List of allowed childs elements
     * could be a class, an abstract class or an interface
     */
    protected static function getAllowedChilds(): array
    {
        return [];
    }
}
