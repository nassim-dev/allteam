<?php

namespace core\html\tables\traits;

/**
 * Trait DatatableEditorTrait
 *
 * Function needed to editor pluggin
 *
 * @category  Function needed to editor pluggin
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
trait DatatableEditorTrait
{
    /**
     * Ajax Url
     *
     * @var string
     */
    public $ajaxUrl;

    /**
     * Editor fields
     *
     * @var array
     */
    public $fields = [];

    /**
     * Table Id
     *
     * @var string
     */
    public $table;

    /**
     * @return mixed
     */
    public function disableEditor(bool $removeButtons = true)
    {
        $this->ajaxUrl = null;
        $this->fields  = [];
        $this->table   = null;

        if ($removeButtons) {
            $this->addConfig('buttons')->value(
                [
                    [
                        'container' => [
                            'className' => 'dt-buttons dropdown'
                        ],
                        'extend'  => 'collection',
                        'text'    => '<i class="fas fa-ellipsis-v"></i>',
                        'fade'    => true,
                        'buttons' => [
                            [
                                'extend'    => 'excelHtml5',
                                'text'      => '<i class="fas fa-file-excel"></i> Export Excel',
                                'titleAttr' => 'Excel',
                                'filename'  => $this->getTitle() . ' - ' . date('D M d Y'),
                                'tag'       => 'a',
                                'className' => ' dropdown-item'
                            ],
                            [
                                'extend'    => 'pdfHtml5',
                                'text'      => '<i class="fas fa-file-pdf"></i> Export PDF',
                                'titleAttr' => 'PDF',
                                'filename'  => $this->getTitle() . ' - ' . date('D M d Y'),
                                'tag'       => 'a',
                                'className' => ' dropdown-item'
                            ]
                        ]
                    ]
                ]
            );
        }

        return $this;
    }

    /**
     * Get ajax Url
     *
     * @PhpUnitGen\get("ajaxUrl")
     *
     * @return string|null
     */
    public function getAjaxUrl(): ?array
    {
        return $this->ajaxUrl;
    }

    /**
     * Get editor fields
     *
     * @PhpUnitGen\get("fields")
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * Get table Id
     *
     * @PhpUnitGen\get("table")
     */
    public function getTable(): string
    {
        return $this->table;
    }

    /**
     * Set ajax Url
     *
     * @PhpUnitGen\set("ajaxUrl")
     *
     * @param array $ajaxUrl Ajax Url
     *
     * @return static
     */
    public function setAjaxUrl(array $ajaxUrl)
    {
        $this->ajaxUrl = $ajaxUrl;

        return $this;
    }

    /**
     * Set table Id
     *
     * @return static
     */
    public function setEditor()
    {
        $this->table = $this->getIdAttribute();

        return $this;
    }

    /**
     * Set editor fields
     *
     * @PhpUnitGen\set("fields")
     *
     * @param array $fields Editor fields
     *
     * @return static
     */
    public function setFields(array $fields)
    {
        $this->fields = $fields;

        return $this;
    }
}
