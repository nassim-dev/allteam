<?php

namespace core\html\tables\traits;

use core\DI\DI;
use core\html\bar\Bar;
use core\html\bar\BarFactory;
use core\html\CommandFactory;
use core\html\javascript\command\ChangeAttributeCommand;
use core\html\javascript\utils\JsUtils;
use core\html\tables\datatable\command\DeleteRowCommand;
use core\html\tables\datatable\command\UpdateRowCommand;

/**
 * Trait DatatableLocalTrait
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait DatatableLocalTrait
{
    /**
     * Return actionBar for datatable list
     *
     * @todo clean method && rename variables && change signature
     *
     * @param sting|int $id
     *
     * @return void
     */
    public static function getBarDatatable(string | int $id, string $type, string $caller, string $mode = 'Add')
    {
        $ressource = explode('\\', self::class)[2];
        $ressource = strtolower($ressource);
        $table     = strtolower($caller);
        $suffix    = $table . '_' . $type . '_ids';
        $mode      = explode('_', $mode);
        $mode      = ucfirst($mode[0]);

        /**
         * @var Bar $actionBar
         */
        $actionBar = DI::singleton(BarFactory::class)->create($table . '-' . $type . '-' . $id);
        $button    = $actionBar->addButton(_('Delete'));

        $button->addClass('btn_delete_' . $suffix . '_' . $mode)
            ->setIcon('fa-trash')
            ->addDataAttributes(
                [
                    'id'    => $id,
                    'table' => $mode . '_' . $suffix
                ]
            );

        /** @var CommandFactory $commandFactory */
        $commandFactory = DI::singleton(CommandFactory::class);

        $button->on('click')
            ->selector('.btn_delete_' . $suffix . '_' . $mode)
            ->command($commandFactory->create(DeleteRowCommand::class, ['target' => $this]));

        $button->enable();

        $button = $actionBar->addButton(_('Edit'));

        $button->addClass('btn_update_' . $suffix . '_' . $mode)
            ->setIcon('fa-edit')
            ->addDataAttributes(
                [
                    'apiMethod'    => 'GET',
                    'id'           => $id,
                    'apiRessource' => $ressource,
                    'target'       => 'form_update_' . $type . '_ids',
                    'table'        => $mode . '_' . $suffix
                ]
            );

        $button->on('click')
            ->selector('.btn_update_' . $suffix . '_' . $mode)
            ->command($commandFactory->create(UpdateRowCommand::class, ['target' => $this]));

        $button->on('click')
            ->selector('.btn_update_' . $suffix . '_' . $mode)
            ->command($commandFactory->create(
                ChangeAttributeCommand::class,
                [
                    'target'    => $this->getIdAttribute() . '_ids',
                    'attribute' => 'table',
                    'value'     => JsUtils::Js('this.value')]
            ));

        $button->enable();

        return $actionBar;
    }
}
