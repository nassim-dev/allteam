<?php

namespace core\html\tables\traits;

use core\config\Conf;
use PhpOffice\PhpWord\Element\Cell;
use PhpOffice\PhpWord\Element\Section;
use PhpOffice\PhpWord\Element\Table;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\SimpleType\JcTable;
use SimpleXMLElement;

/**
 * Trait DatatableToDoc
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait DatatableToDoc
{
    /**
     * Title Style
     *
     * @var array
     */
    public static $header = ['size' => 14, 'bold' => true];

    /**
     * Table style options
     *
     * @var array
     */
    public $options = [];

    /**
     * PhpWord Object
     *
     * @var PhpWord|null
     */
    public $phpWord = null;

    /**
     * Section object
     *
     * @var Section|null
     */
    public $section = null;

    /**
     * Table object
     *
     * @var Table|null
     */
    public $table = null;

    /**
     * @return static
     */
    public function generateContents(int $height = 900, int $width = 2000)
    {
        foreach ($this->getRows() as $rows) {
            $this->table->addRow($height);
            foreach ($rows as $row) {
                $this->_formatData($row, $width);
            }
        }

        return $this;
    }

    /**
     * @return static
     */
    public function generateHeader(int $height = 900, int $width = 2000)
    {
        $this->table->addRow($height);
        foreach ($this->getThead() as $row) {
            if ('Action' !== $row) {
                $this->table->addCell($width, $this->options['TableFirstRowStyle'])->addText(trim(strip_tags($row)), $this->options['TableFontStyle']);
            }
        }

        return $this;
    }

    /**
     * Init Element
     *
     * @return static
     */
    public function initDocument(Section $section, PhpWord $phpWord)
    {
        $this->phpword = $phpWord;
        $this->section = $section;
        $this->section->addTitle($this->getTitle(), 2);
        $this->section->addTextBreak(1);

        return $this;
    }

    /**
     * Init table with options
     *
     * @return static
     */
    public function initTable(array $options = [])
    {
        $default                       = [];
        $default['TableStyleName']     = 'Fancy Table';
        $default['TableStyle']         = ['layout' => \PhpOffice\PhpWord\Style\Table::LAYOUT_AUTO, 'borderSize' => 6, 'borderColor' => '1c2331', 'cellMargin' => 80, 'alignment' => JcTable::CENTER, 'cellSpacing' => 50];
        $default['TableFirstRowStyle'] = ['bgColor' => '1c2331', 'tblHeader' => true, 'color' => 'ffffff', 'valign' => 'center', 'alignment' => JcTable::CENTER];
        $default['TableCellStyle']     = ['borderSize' => 6, 'borderColor' => '1c2331', 'valign' => 'center'];
        $default['TableCellBtlrStyle'] = ['valign' => 'center', 'textDirection' => Cell::TEXT_DIR_BTLR];
        $default['TableFontStyle']     = ['bold' => true];

        $this->options = (!empty($options)) ? array_replace($default, $options) : $default;
        $this->table   = $this->section->addTable($this->options['TableStyle']);

        return $this;
    }

    /**
     * Format content to displau
     *
     * @return Celle|null
     */
    private function _formatData(array $row, $width): ?Cell
    {
        switch ($row['type']) {
            case 'text':
                return $this->table->addCell($width, $this->options['TableCellStyle'])->addText(trim(strip_tags($row['data'])), $this->options['TableFontStyle']);
            case 'form':
                return $this->table->addCell($width, $this->options['TableCellStyle'])->addText('', $this->options['TableFontStyle']);
            case 'element':
                return $this->table->addCell($width, $this->options['TableCellStyle'])->addText('', $this->options['TableFontStyle']);
            case 'label':
                return $this->table->addCell($width, $this->options['TableCellStyle'])->addText($row['data']->text, $this->options['TableFontStyle']);
            case 'list':

                foreach ($row['data']->getLiElements() as $item) {
                    $content = preg_match('/href=\"/iU', $item->getContent()) ? null : $item->getContent();
                    if (!isset($cell)) {
                        $cell = $this->table->addCell($width, $this->options['TableCellStyle']);
                    }

                    if (null === $content) {
                        $link = new SimpleXMLElement($item->getContent());
                        $cell->addLink(Conf::find('PHP.HTTP_PROTOCOL') . '://' . Conf::find('PHP.SERVER_NAME') . $link['href'], $link[0]);
                    } else {
                        $cell->addText($item->getContent(), $this->options['TableFontStyle']);
                    }

                    $cell->addTextBreak(1);
                }

                return $cell;
        }
    }
}
