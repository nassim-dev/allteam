<?php

namespace core\html\tables\traits;

/**
 * Undocumented trait
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
trait VueExcelEditorTrait
{
    private $vueProps = [
        'v-model'               => 'data',
        '@delete'               => 'onDelete',
        '@update'               => 'onUpdate',
        '@select'               => 'onSelect',
        '@change'               => 'onChange',
        'no-paging'             => false,
        'no-num-col'            => false,
        'filter-row'            => false,
        'no-footer'             => false,
        'no-finding'            => false,
        'no-finding-next'       => false,
        'free-select'           => false,
        'autocomplete'          => true,
        'readonly'              => false,
        'readonly-style'        => '',
        'remember'              => true,
        'allow-add-col'         => false,
        'no-header-edit'        => true,
        'spellcheck'            => true,
        'new-if-bottom'         => false,
        'disable-panel-setting' => false,
        'disable-panel-filter'  => false
    ];

    /**
     * Return vue properties
     *
     * @return array
     */
    protected function _toArrayVue(): array
    {
        return ['vue-config' => $this->vueProps];
    }

    /**
     * Set a vue propertie
     *
     * @return static
     */
    protected function setVueProp(string $key, mixed $value): static
    {
        $this->vueProps[$key] = $value;

        return $this;
    }
}
