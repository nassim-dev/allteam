<?php

namespace core\html\tables\traits;

/**
 * VueExcelColumnTrait
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
trait VueExcelColumnTrait
{
    private $vueProps = [
        'invisible'             => false,
        'sticky'                => false,
        '@change'               => 'onChange',
        '@validate'             => 'onValidate',
        'autocomplete'          => true,
        'readonly'              => false,
        'type'                  => 'string',
        'text-transform'        => '', //uppercase or lowercase
        'summary'               => '',  // 'sum', 'avg', 'max', 'min'
        'allow-add-col'         => false,
        'no-header-edit'        => false,
        'spellcheck'            => true,
        'new-if-bottom'         => false,
        'disable-panel-setting' => false,
        'disable-panel-filter'  => false
    ];

    /**
     * Return vue properties
     *
     * @return array
     */
    protected function _toArrayVue(): array
    {
        $this->vueProps['field']    = $this->getName();
        $this->vueProps['label']    = $this->getTitle();
        $this->vueProps[':options'] = 'getOptions(' . json_encode($this->options) . ')';
        $this->vueProps['readonly'] = $this->readonly ?? false;

        return ['vue-config' => $this->vueProps];
    }

    /**
     * Set a vue propertie
     *
     * @return static
     */
    protected function setVueProp(string $key, mixed $value): static
    {
        $this->vueProps[$key] = $value;

        return $this;
    }

    /**
     * Set collumn as summary
     *
     * @return static
     */
    protected function setVueSummary(string $value): static
    {
        $this->vueProps['summary'] = (!in_array($value, ['sum', 'avg', 'max', 'min'])) ? '' : $value;

        return $this;
    }

    /**
     * Set column as textTransform
     *
     * @return static
     */
    protected function setVueTextTransform(string $value): static
    {
        $this->vueProps['text-transform'] = (!in_array($value, ['uppercase', 'lowercase'])) ? '' : $value;

        return $this;
    }

    /**
     * Set Column type for vue plugin
     *
     * @return static
     */
    protected function setVueType(string $value): static
    {
        $this->vueProps['type'] = (!in_array($value, ['string', 'number', 'select', 'map', 'check10', 'checkYN', 'checkTF', 'date', 'datetime'])) ? 'string' : $value;

        return $this;
    }
}
