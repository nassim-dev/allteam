<?php

namespace core\html\tables\tableStriped;

use core\html\AbstractHtmlElement;
use core\html\form\elements\interfaces\FieldInterface;
use core\html\form\Form;
use core\html\HtmlElement;
use core\html\traits\Identifiable;
use core\html\traits\Stylizable;
use core\html\traits\Titleable;

/**
 * Class TableStriped
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class TableStriped extends AbstractHtmlElement
{
    use Titleable;
    use Stylizable;
    use Identifiable;

    /**
     * TableStriped constructor.
     */
    public function __construct(string $title, string $identifier, private array $_thead)
    {
        $this->setTitle($title);
        $this->setId(ucfirst($identifier));
    }

    /**
     * Add Element to form
     *
     * @return static
     */
    public function addElement(FieldInterface $element): ?\core\html\HtmlElementInterface
    {
        return $this->appendChild($element);
    }

    /**
     * Add Row
     *
     * @return static
     */
    public function addRow(array $row): ?\core\html\HtmlElementInterface
    {
        $item = new HtmlElement($row);
        $item->setHtmlTemplate(self::getBaseTemplateDir() . 'tables/tableStriped/TableRow.html');

        return $this->appendChild($item);
    }

    /**
     * Get value of _elements;
     * @PhpUnitGen\get("_elements")
     */
    public function getElements(): array
    {
        return $this->getChilds()->filterBy(FieldInterface::class);
    }

    /**
     * Return Id attribute for html element
     */
    public function getIdAttribute(): string
    {
        return $this->getViewReference() . '_' . $this->getId();
    }


    /**
     * Get value of _rows
     *
     * @PhpUnitGen\get("_rows")
     */
    public function getRows(): array
    {
        return $this->getChilds()->filterBy(HtmlElement::class);
    }

    /**
     * Get value of _thead
     *
     * @PhpUnitGen\get("_thead")
     */
    public function getThead(): array
    {
        return $this->_thead;
    }

    /**
     * List of allowed childs elements
     * could be a class, an abstract class or an interface
     */
    protected static function getAllowedChilds(): array
    {
        return [HtmlElement::class, FieldInterface::class];
    }
}
