<?php

namespace core\html\tables\tableStriped;

use core\factory\FactoryInterface;
use core\html\HtmlElementFactory;

/**
 * Class TableStripedFactory
 *
 * TableStriped
 *
 * @category  TableStriped
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class TableStripedFactory extends HtmlElementFactory implements FactoryInterface
{
    /**
     * Return element class to build
     */
    protected function getElementClass(): string
    {
        return TableStriped::class;
    }

    /**
     * Return suffix for widget classes
     */
    protected function getWidgetSuffix(): string
    {
        return 'TableWidget';
    }

    /**
     * Create new instance or return singleton if exist
     *
     * @PhpUnitGen\assertInstanceOf("TableStriped::class")
     */
    public function create(string $identifier, array $params = []): ?TableStriped
    {
        $params['thead'] ??= [];
        $params['title'] ??= $identifier;
        $params['identifier'] ??= $identifier;

        return parent::create($identifier, $params);
    }

    /**
     * Get registred elements
     */
    public function get(string $identifier): ?TableStriped
    {
        return $this->getInstance($identifier) ?? $this->create($identifier);
    }
}
