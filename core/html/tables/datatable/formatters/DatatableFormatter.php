<?php

namespace core\html\tables\datatable\formatters;

use core\entities\decorator\DatatableDecoratorBase;
use core\entities\EntitieInterface;
use core\html\AbstractFormatter;
use core\html\FormatterInterface;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class DatatableFormatter extends AbstractFormatter implements FormatterInterface
{
    public function format(EntitieInterface $entitie): ?array
    {
        $class = (class_exists($entitie->getNamespace() . '\decorator\DatatableDecorator')) ? $entitie->getNamespace() . '\decorator\DatatableDecorator' : DatatableDecoratorBase::class;

        /**
         * @var DatatableDecoratorBase $decorator
         */
        $decorator = $this->getDi()->singleton($class);

        return $decorator->decorate($entitie, func_get_args());
    }
}
