<?php

namespace core\html\tables\datatable;

use core\factory\FactoryInterface;
use core\html\button\Button;
use core\html\CommandFactory;
use core\html\HtmlElementFactory;
use core\html\modal\command\ModalShowCommand;
use core\html\modal\Modal;
use core\html\modal\ModalFactory;

/**
 * Class DatatableFactory
 *
 * Table Factory*
 *
 * @category  Table Factory
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class DatatableFactory extends HtmlElementFactory implements FactoryInterface
{
    /**
     * Return element class to build
     */
    protected function getElementClass(): string
    {
        return Datatable::class;
    }

    /**
     * Return suffix for widget classes
     */
    protected function getWidgetSuffix(): string
    {
        return 'DatatableWidget';
    }

    /**
     * Create new instance or return singleton if exist
     *
     * @PhpUnitGen\assertInstanceOf("Datatable::class")
     */
    public function create(string $identifier, array $params = []): ?Datatable
    {
        $params['title'] ??= $identifier;
        $params['identifier'] ??= $identifier;
        $params['thead'] ??= [];

        return parent::create($identifier, $params);
    }

    /**
     * Create from WidgetInterface
     */
    public function createFromWidget(string $className, array $params): ?Datatable
    {
        return parent::createFromWidget($className, $params);
    }

    /**
     *  Add a button
     *
     * @PhpUnitGen\assertInstanceOf("Button::class")
     */
    public function createButonFor(Datatable $table, string $name, ?string $label = null): ?Button
    {
        $moduleName = explode('_', $name);
        $page       = $moduleName[0] . ucfirst($moduleName[1]);
        $label ??= _('New');

        $button = $table->addButton($label);
        $button->addClasses(
            [
                'btn_' . $moduleName[0] . '_' . strtolower($moduleName[1]),
                'animated',
                'heartBeat',
                'delay-3s',
                'btn-light-success'
            ]
        )
            ->setIcon('fa-plus');

        $button->disable();


        /** @var ModalFactory $modalFactory */
        $modalFactory = $this->getDi()->singleton(ModalFactory::class);

        $modalFactory->createThen($moduleName[0] . '_' . strtolower($moduleName[1]), function (Modal $modal) use ($button, $name, $table): void {
            /** @var CommandFactory $commandFactory */
            $commandFactory = $this->getDi()->singleton(CommandFactory::class);

            $button->addDataAttributes(
                [
                    'bs-toggle' => 'modal',
                    'bs-target' => '#' . $modal->getIdAttribute()
                ]
            );

            $button->on('click')
                ->command($commandFactory->create(
                    ModalShowCommand::class,
                    [
                        'target'     => $modal,
                        'parameters' => [
                            'previous' => $table->getTitle(),
                            'table'    => 'table_' . $name]
                    ]
                ));

            $button->enable();
        });


        return $button;
    }

    public function get(string $identifier): ?Datatable
    {
        return $this->getInstance($identifier) ?? $this->create($identifier);
    }
}
