<?php

namespace core\html\tables\datatable;

use core\controller\ControllerServiceInterface;
use core\messages\request\HttpRequestInterface;

/**
 * Class AbstractTableTemplate
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 *
 * @PhpUnitGen\mock("\\app\\http\\HttpRequest", "request")
 */
class AbstractTableTemplate
{
    /**
     * Mode pills
     *
     * @var bool
     */
    protected static $_pills = false;

    /**
     * Pills Id
     *
     * @var int
     */
    protected static $_pillsId = 0;

    /**
     * Parameters
     *
     * @var array
     */
    public $params = [];

    public function __construct(
        public HttpRequestInterface $request,
        protected ControllerServiceInterface $controllerService,
        array $params = []
    ) {
        $this->setParams(array_merge($this->request->getParameters(), $params));
    }

    public static function getDefaultTemplateDir()
    {
        $elements = explode('\\', static::class);
        unset($elements[-1]);

        return BASE_DIR . implode('/', $elements) . '/../../templates/';
    }

    /**
     * Get parameters
     *
     * @PhpUnitGen\get("params")
     */
    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * Set parameters
     *
     * @param array $params Parameters
     *
     * @PhpUnitGen\set("params")
     */
    public function setParams(array $params): self
    {
        $this->params = $params;

        return $this;
    }

    /**
     * Get mode pills
     *
     * @PhpUnitGen\get("_pills")
     */
    public function getPills(): bool
    {
        return $this->_pills;
    }

    /**
     * Get pills Id
     *
     * @PhpUnitGen\get("_pillsId")
     */
    public function getPillsId(): int
    {
        return $this->_pillsId;
    }

    /**
     * Set mode pills
     *
     * @PhpUnitGen\set("_pills")
     *
     * @param bool $_pills Mode pills
     */
    public function setPills(bool $_pills): self
    {
        $this->_pills = $_pills;

        return $this;
    }

    /**
     * Set pills Id
     *
     * @PhpUnitGen\set("_pillsId")
     *
     * @param int $_pillsId Pills Id
     */
    public function setPillsId(int $_pillsId): self
    {
        $this->_pillsId = $_pillsId;

        return $this;
    }

    /**
     * Check if a parameter is registred then return it or null
     *
     * @return mixed|null
     */
    protected function getParameter(string $key)
    {
        return $this->params[$key] ?? null;
    }

    /**
     * Create widget
     *
     * @param mixed $object
     */
    public function create(&$object, ?array $arguments = null): Datatable
    {
        if (!isset($arguments['method'])) {
            throw new DatatableException('You must provide a parameter `method`');
        }

        if (!method_exists($this, $arguments['method'])) {
            throw new DatatableException('Invalid method `' . $arguments['method'] . '`');
        }

        return  $this->getDi()->call($this, $arguments['method'], $arguments);
    }
}
