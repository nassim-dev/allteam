<?php

namespace core\html\tables\datatable;

use core\app\ApiException;
use core\html\bar\Bar;
use core\html\bar\BarFactory;
use core\html\form\FormFactory;
use core\html\javascript\command\ChangeAttributeCommand;
use core\html\javascript\utils\JsUtils;
use core\html\tables\datatable\command\DeleteRowCommand;
use core\html\tables\datatable\command\UpdateRowCommand;
use core\DI\DiProvider;

/**
 * Trait DatatableHandlerTrait
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
trait DatatableHandlerTrait
{
    use DiProvider;

    /**
     * Return actionBar for datatable list
     *
     * @todo clean method && rename variables && change signature
     */
    public function getBarDatatable(int|\sting $id, string $type, string $caller, string $mode): Bar
    {
        $ressource = explode('\\', self::class)[2];
        $ressource = strtolower($ressource);
        $table     = strtolower($caller);
        $suffix    = $table . '_' . $type . '_ids';
        $mode      = ucfirst($mode);

        /**
         * @var Bar $actionBar
         */
        $actionBar = $this->getDi()->singleton(BarFactory::class)->create($table . '-' . $type . '-' . $id);
        $button    = $actionBar->addButton(_('Delete'));

        $cssDeleteClass = 'btn_delete_' . $suffix . '_' . $mode;

        $button->addClass($cssDeleteClass)
            ->setIcon('fa-trash')
            ->addDataAttributes(
                [
                    'id'    => $id,
                    'table' => $mode . '_' . $suffix
                ]
            );
        /** @var CommandFactory $commandFactory */
        $commandFactory = $this->getDi()->singleton(CommandFactory::class);

        $button->on('click')
            ->selector(".$cssDeleteClass")
            ->command($commandFactory->create(DeleteRowCommand::class, ['target' => $this]));

        $button->enable();

        $button = $actionBar->addButton(_('Edit'));

        $cssUpdateClass = 'btn_update_' . $suffix . '_' . $mode;

        $button->addClass($cssUpdateClass)
            ->setIcon('fa-edit')
            ->addDataAttributes(
                [
                    'apiMethod'    => 'GET',
                    'id'           => $id,
                    'apiRessource' => $ressource,
                    'target'       => 'form_update_' . $type . '_ids',
                    'table'        => $mode . '_' . $suffix
                ]
            );

        $button->on('click')
            ->selector(".$cssUpdateClass")
            ->command($commandFactory->create(UpdateRowCommand::class, ['target' => $this]));

        $button->on('click')
            ->selector(".$cssUpdateClass")
            ->command($commandFactory->create(
                ChangeAttributeCommand::class,
                [
                    'target'    => '#table_form_' . strtolower($mode) . '_' . $type . '_ids',
                    'attribute' => 'table',
                    'value'     => JsUtils::Js('this.command.execute(this, "saveContents");')
                ]
            ));

        $button->enable();

        return $actionBar;
    }

    /**
     * Add new row
     *
     * @return void
     */
    private function addRow(string $formName)
    {
        $form = $this->getFormFactory()->createFromWidget($this->getTable(), [$formName]);
        $this->validateForm($form);

        return $this->generateRow('create', $this->request->getContents('table'));
    }

    /**
     * Return FormFactory instance
     */
    private function getFormFactory(): FormFactory
    {
        return $this->getDi()->singleton(FormFactory::class);
    }

    /**
     * Edit row
     *
     * @return void
     */
    private function editRow(string $formName)
    {
        $form = $this->getFormFactory()->createFromWidget($this->getTable(), [$formName]);
        $this->validateForm($form);

        return $this->generateRow('edit', $this->request->getContents('table'));
    }

    /**
     * Generate row contents
     *
     * @return void
     */
    private function generateRow(string $action, string $table)
    {
        $idrow = ('edit' === $action) ? $this->request->getContents('temporary_id') : uniqid();

        $datasTmp = $this->getFormattedRow($idrow, $action);

        $this->response->setData(
            'localStorage',
            [
                'data'      => (count($datasTmp) > 1) ? $datasTmp : [$datasTmp],
                'datatable' => strtolower($table),
                'action'    => ucfirst($action)
            ]
        );

        $this->response->setCode(201);
    }

    /**
     * Return formatted row for datatable as form element
     *
     * @param mixed $idrow
     */
    private function getFormattedRow($idrow, string $mode): array
    {
        throw new ApiException('Method `getFormattedRow` must be implemented in ' . static::class);
    }
}
