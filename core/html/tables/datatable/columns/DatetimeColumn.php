<?php

namespace core\html\tables\datatable\columns;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class DatetimeColumn extends AbstractColumn implements DatatableColumnInterface
{
    public function __construct(string $title, array $options = [])
    {
        $options['format'] = $options['format'] ?? 'DD/MM/YYYY HH24:MI';
        $options['time']   = $options['time'] ?? 1;
        $options['today']  = $options['today'] ?? 0;

        parent::__construct($title);
        $this->setAlign('center')
            ->setOptions($options)
            ->setType('calendar');
    }
}
