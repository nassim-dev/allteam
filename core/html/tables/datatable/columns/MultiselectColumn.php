<?php

namespace core\html\tables\datatable\columns;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class MultiselectColumn extends AbstractColumn implements DatatableColumnInterface
{
    protected ?string $url = null;

    protected ?bool $autocomplete = true;

    protected ?bool $multiple = false;

    protected ?array $items = [];

    public function __construct(string $title, array $options = [])
    {
        parent::__construct($title);
        $this->setMultiple((isset($options['multiple'])) ? $options['multiple'] : false)
            ->setAutocomplete((isset($options['autocomplete'])) ? $options['autocomplete'] : true)
            ->setAlign('center')
            ->setOptions($options)
            ->setType('dropdown');

        if (isset($options['items'])) {
            $this->setItems($options['items'])->setUrl(null);
        }

        if (isset($options['url'])) {
            $this->setUrl($options['url'])->setItems(null);
        }
    }

    public function toArray(): array
    {
        $config                 = parent::toArray();
        $config['url']          = $this->url;
        $config['autocomplete'] = $this->autocomplete;
        $config['multiple']     = $this->multiple;
        $config['source']       = $this->items;

        return $config;
    }


    /**
     * Set the value of autocomplete
     *
     * @PhpUnitGen\set("autocomplete")
     * @return static
     */
    public function setAutocomplete(bool $autocomplete)
    {
        $this->autocomplete = $autocomplete;

        return $this;
    }


    /**
     * Set the value of url
     *
     * @PhpUnitGen\set("url")
     * @return static
     */
    public function setUrl(?string $url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Set the value of multiple
     *
     * @PhpUnitGen\set("multiple")
     * @return static
     */
    public function setMultiple(bool $multiple)
    {
        $this->multiple = $multiple;

        return $this;
    }

    /**
     * Set the value of items
     *
     * @PhpUnitGen\set("items")
     * @return self
     * @param  mixed[]|null $items
     */
    public function setItems(?array $items)
    {
        $this->items = $items;

        return $this;
    }
}
