<?php

namespace core\html\tables\datatable\columns;

use core\html\AbstractHtmlElement;
use core\html\form\elements\traits\WithMask;
use core\html\tables\traits\VueExcelColumnTrait;
use core\html\traits\Namable;
use core\html\traits\Titleable;
use core\utils\Utils;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class AbstractColumn extends AbstractHtmlElement
{
    use Namable;
    use Titleable;
    use WithMask;
    use VueExcelColumnTrait;

    protected ?string $defaultValue = null;

    protected bool $readonly = false;

    protected string $align = 'center';

    protected ?array $options = [];

    protected string $type = 'text';

    protected bool $wordWrap = false;

    protected ?int $width = 100;

    protected bool $stripHTML = true;


    public function __construct(string $title)
    {
        $this->setName(Utils::slugify($title));
        $this->setTitle($title);
        $this->htmlTag = 'vue-excel-column';
    }

    /**
     * Return an Array for object jsConfig
     * @return array{name: string|null, title: string|null, mask: string|null, defaultValue: string|null, readonly: bool, align: string, options: mixed[]|null, type: string, wordWrap: bool, stripHTML: bool, width?: int}
     */
    public function toArray(): array
    {
        $return = [
            'name'         => $this->getName(),
            'title'        => $this->getTitle(),
            'mask'         => $this->getMask(),
            'defaultValue' => $this->defaultValue,
            'readonly'     => $this->readonly,
            'align'        => $this->align,
            'options'      => $this->options,
            'type'         => $this->type,
            'wordWrap'     => $this->wordWrap,
            'stripHTML'    => $this->stripHTML,
        ];

        if ($this->width != null) {
            $return['width'] = $this->width;
        }

        return $return;
    }

    /**
     * List of allowed childs elements
     * could be a class, an abstract class or an interface
     */
    protected static function getAllowedChilds(): array
    {
        return [];
    }


    /**
     * Return Id attribute for html element
     */
    public function getIdAttribute(): string
    {
        return $this->getViewReference() . '_' . uniqid();
    }



    /**
     * Set column is readlonly
     *
     * @param boolean $readonly Column is readlonly
     *
     * @PhpUnitGen\set("readonly")
     *
     * @return self
     */
    public function setReadonly(bool $readonly)
    {
        $this->readonly = $readonly;

        return $this;
    }

    /**
     * Set doesn't have a value.
     *
     * @param string $defaultValue doesn't have a value.
     *
     * @PhpUnitGen\set("defaultValue")
     *
     * @return self
     */
    public function setDefaultValue(string $defaultValue)
    {
        $this->defaultValue = $defaultValue;

        return $this;
    }

    /**
     * Set 'center' | 'left' | 'right'
     *
     * @param string $align 'center' | 'left' | 'right'
     *
     * @PhpUnitGen\set("align")
     *
     * @return self
     */
    public function setAlign(string $align)
    {
        $this->align = $align;

        return $this;
    }

    /**
     * Set the value of options
     *
     * @PhpUnitGen\set("options")
     * @return self
     * @param  mixed[]|null $options
     */
    public function setOptions(?array $options)
    {
        $this->options = $options;

        return $this;
    }


    /**
     * Set the value of type
     *
     * @PhpUnitGen\set("type")
     * @return self
     */
    public function setType(string $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Set the value of wordWrap
     *
     * @PhpUnitGen\set("wordWrap")
     * @return self
     */
    public function setWordWrap(bool $wordWrap)
    {
        $this->wordWrap = $wordWrap;

        return $this;
    }

    /**
     * Set the value of width
     *
     * @PhpUnitGen\set("width")
     * @return self
     */
    public function setWidth(?int $width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Set the value of stripHTML
     *
     * @PhpUnitGen\set("stripHTML")
     * @return self
     */
    public function setStripHTML(bool $stripHTML)
    {
        $this->stripHTML = $stripHTML;

        return $this;
    }
}
