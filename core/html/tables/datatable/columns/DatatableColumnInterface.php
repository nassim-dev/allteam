<?php

namespace core\html\tables\datatable\columns;

use core\html\HtmlElementInterface;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
interface DatatableColumnInterface extends HtmlElementInterface
{
    /**
     * Get name for action
     *
     * @PhpUnitGen\get("name")
     */
    public function getName(): ?string;

    /**
     * Return an Array for object jsConfig
     */
    public function toArray(): array;

    /**
     * Set name for action
     *
     * @PhpUnitGen\set("name")
     *
     * @param string $name Name for action
     *
     * @return static
     */
    public function setName(string $name);

    /**
     * Set the value of stripHTML
     *
     * @PhpUnitGen\set("stripHTML")
     * @return self
     */
    public function setStripHTML(bool $stripHTML);

    /**
     * Set title for action
     *
     * @PhpUnitGen\set("title")
     *
     * @param string $title Title for action
     *
     * @return static
     */
    public function setTitle(string $title);

    /**
     * Set column is readlonly
     *
     * @param boolean $readonly Column is readlonly
     *
     * @PhpUnitGen\set("readonly")
     *
     * @return self
     */
    public function setReadonly(bool $readonly);

    /**
     * Set doesn't have a value.
     *
     * @param string $defaultValue doesn't have a value.
     *
     * @PhpUnitGen\set("defaultValue")
     *
     * @return self
     */
    public function setDefaultValue(string $defaultValue);

    /**
     * Set 'center' | 'left' | 'right'
     *
     * @param string $align 'center' | 'left' | 'right'
     *
     * @PhpUnitGen\set("align")
     *
     * @return self
     */
    public function setAlign(string $align);

    /**
     * Set the value of options
     *
     * @PhpUnitGen\set("options")
     * @return self
     */
    public function setOptions(?array $options);

    /**
     * Set the value of type
     *
     * @PhpUnitGen\set("type")
     * @return self
     */
    public function setType(string $type);


    /**
     * Set the value of wordWrap
     *
     * @PhpUnitGen\set("wordWrap")
     * @return self
     */
    public function setWordWrap(bool $wordWrap);

    /**
     * Set the value of width
     *
     * @PhpUnitGen\set("width")
     * @return self
     */
    public function setWidth(?int $width);


    /**
     * Set value opf $_mask
     *
     * @PhpUnitGen\set("_mask")
     *
     * @return static
     */
    public function mask(?string $var);
}
