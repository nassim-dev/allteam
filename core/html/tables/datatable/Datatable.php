<?php

namespace core\html\tables\datatable;

use core\html\AbstractHtmlElement;
use core\html\HtmlElement;
use core\html\HtmlElementInterface;
use core\html\javascript\JavascriptObjectInterface;
use core\html\javascript\WithJavascript;
use core\html\tables\datatable\columns\CheckboxColumn;
use core\html\tables\datatable\columns\ColorColumn;
use core\html\tables\datatable\columns\DatatableColumnInterface;
use core\html\tables\datatable\columns\DateColumn;
use core\html\tables\datatable\columns\DatetimeColumn;
use core\html\tables\datatable\columns\DisabledColumn;
use core\html\tables\datatable\columns\MultiselectColumn;
use core\html\tables\datatable\columns\NumberColumn;
use core\html\tables\datatable\columns\RichtextColumn;
use core\html\tables\datatable\columns\SelectColumn;
use core\html\tables\datatable\columns\TextColumn;
use core\html\tables\datatable\columns\TimeColumn;
use core\html\tables\traits\DatatableEditorTrait;
use core\html\tables\traits\DatatableToDoc;
use core\html\tables\traits\VueExcelEditorTrait;
use core\html\traits\AjaxSourced;
use core\html\traits\Identifiable;
use core\html\traits\Stylizable;
use core\html\traits\Titleable;
use core\html\traits\WithActions;
use core\html\traits\WithHelper;
use core\routing\Link;

/**
 * Class Datatable
 *
 * Datatable element
 *
 * @category  Datatable element
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Datatable extends AbstractHtmlElement implements JavascriptObjectInterface
{
    use WithJavascript;
    use DatatableToDoc;
    use DatatableEditorTrait;
    use Titleable;
    use AjaxSourced;
    use WithActions;
    use Identifiable;
    use Stylizable;
    use WithHelper;
    use VueExcelEditorTrait;

    public const NO_COL_ACTION = false;
    public const NO_CHECKBOX   = false;

    /**
     * Header
     *
     * @var array
     */
    protected $columns = [];

    protected bool $draggable = false;

    /**
     * Undocumented function
     */
    public function __construct(string $title, string $identifier, array $thead, private bool $_hasColAction = true)
    {
        $this->setTitle($title);
        $this->setId(strtolower($identifier));
        $this->_generateColActionColumn($_hasColAction);
        $this->_generateThead($thead);
        $this->htmlTag = 'vue-excel-editor';
    }


    public function addColumn(string $type, string $name, array $options = []): DatatableColumnInterface
    {
        if (isset($this->columns[$name])) {
            return $this->columns[$name];
        }

        if (class_exists($type) && in_array(DatatableColumnInterface::class, class_implements($type, false))) {
            return $this->columns[$name] = new $type($name, $options);
        }

        return $this->columns[$name] = match ($type) {
            'fileinput'     => (new TextColumn($name, $options))->setStripHTML(false),
            'text'          => new TextColumn($name, $options),
            'phone'         => (new TextColumn($name, $options))->mask('## ## ## ## ##'),
            'password'      => new TextColumn($name, $options),
            'email'         => new TextColumn($name, $options),
            'richtext'      => new RichtextColumn($name, $options),
            'disabled'      => new DisabledColumn($name, $options),
            'number'        => new NumberColumn($name, $options),
            'select'        => new SelectColumn($name, $options),
            'autocomplete'  => new MultiselectColumn($name, $options),
            'multiselect'   => new MultiselectColumn($name, $options),
            'date'          => new DateColumn($name, $options),
            'datetime'      => new DatetimeColumn($name, $options),
            'time'          => new TimeColumn($name, $options),
            'daterange'     => new DateColumn($name, $options),
            'datetimerange' => new DatetimeColumn($name, $options),
            'timerange'     => new TimeColumn($name, $options),
            'color'         => new ColorColumn($name, $options),
            'checkbox'      => new CheckboxColumn($name, $options),
            'html'          => (new TextColumn($name, $options))->setStripHTML(false),
        };
    }

    /**
     * Add new row
     */
    public function addRow(\core\html\HtmlElementInterface|string $element): HtmlElementInterface
    {
        $element = (is_string($element)) ? new HtmlElement(['value' => $element]) : $element;
        $element = (is_array($element)) ? new HtmlElement($element) : $element;

        if ($element::class === HtmlElement::class) {
            $element->setHtmlTemplate(self::getBaseTemplateDir() . 'tables/tableStriped/TableRow.html');
        }

        $this->appendChild($element);

        return $element;
    }

    /**
     * Add new rows
     *
     * @return void
     */
    public function addRows(array $contents)
    {
        foreach ($contents as $element) {
            $this->addRow($element);
        }
    }



    /**
     * Return Id attribute for html element
     */
    public function getIdAttribute(): string
    {
        return $this->getViewReference() . '_' . $this->getId();
    }

    /**
     * Get column name from composed name like compagny-user -> iduser
     *
     * @param $columnName
     *
     * @return mixed
     */
    public function getRealColumnName($columnName)
    {
        if (!empty($this->_associatedColumns)) {
            if (array_key_exists($columnName, $this->_associatedColumns)) {
                return $this->_associatedColumns[$columnName];
            }

            return false;
        }

        $elements = explode('-', $columnName);

        return (is_array($elements)) ? end($elements) : $elements;
    }

    /**
     * Applies getRealColumn on all columns
     */
    public function getRealColumnsNames(): array
    {
        $columnsId  = $this->_columnId;
        $columnName = [];
        foreach ($columnsId as $column) {
            $columnName[] = $this->getRealColumnName($column['name']);
        }

        return $columnName;
    }

    /**
     * Get value of thead
     *
     * @PhpUnitGen\get("columns")
     */
    public function getThead(): array
    {
        return $this->columns;
    }

    /**
     * Get true if table has action column
     *
     * @PhpUnitGen\get("_hasColAction")
     */
    public function hasColAction(): bool
    {
        return $this->_hasColAction;
    }


    /**
     * Set header
     *
     * @PhpUnitGen\set("columns")
     *
     * @param array $thead Header
     */
    public function setThead(array $thead): self
    {
        $this->columns = $thead;

        return $this;
    }

    /**
     * Return an Array for object jsConfig
     */
    public function toArray(): array
    {
        $configArray            = parent::toArray();
        $configArray['columns'] = [];

        /** @var DatatableColumnInterface $column */
        foreach ($this->columns as $column) {
            $configArray['columns'][] = $column->toArray();
        }

        $configArray['data'] = $this->getChilds()?->toArray() ?? [];

        if (empty($configArray['data'])) {
            unset($configArray['data'], $configArray['childs']);
        }

        return $configArray;
    }

    /**
     * List of allowed childs elements
     * could be a class, an abstract class or an interface
     */
    protected static function getAllowedChilds(): array
    {
        return [];
    }

    /**
     * Generate colAction column if $colAction is true
     *
     * @return integer
     */
    private function _generateColActionColumn(bool $colAction)
    {
        if ($colAction) {
            $this->addColumn('html', 'tableActions')
                ->setReadonly(true)
                ->setStripHTML(false);
        }
    }

    /**
     * Generate Thead
     */
    private function _generateThead(array $thead)
    {
        foreach ($thead as $columName => $options) {
            if (!is_array($options)) {
                $options = [];
            }


            if (array_key_exists('url', $options) && $options['url'] === null) {
                unset($thead[$columName]);

                continue;
            }


            $options['type'] ??= 'text';
            /** @var DatatableColumnInterface $column */
            $column = $this->addColumn($options['type'] ?? 'text', $options['label'] ?? $columName, $options);
            $column->setName($columName);
        }
    }


    /**
     * Set the value of draggable
     *
     * @param mixed $draggable
     *
     * @PhpUnitGen\set("draggable")
     *
     * @return self
     */
    public function setDraggable($draggable)
    {
        $this->draggable = $draggable;

        return $this;
    }

    public function getColumns(): array
    {
        return $this->columns;
    }
}
