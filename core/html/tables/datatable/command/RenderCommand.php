<?php

namespace core\html\tables\datatable\command;

use core\html\javascript\command\AbstractCommand;
use core\html\tables\datatable\Datatable;

/**
 * Class RenderCommand
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class RenderCommand extends AbstractCommand
{
    public function __construct(public Datatable $target, public array $parameters = [])
    {
        //'$("#' . $table->getIdAttribute() . '").DataTable().columns.adjust();'
    }


    public function get(): string
    {
        return 'components/tables/datatable/RenderCommand';
    }

    public function getContext(): string
    {
        return 't-datatable';
    }
}
