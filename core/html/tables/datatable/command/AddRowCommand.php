<?php

namespace core\html\tables\datatable\command;

use core\html\javascript\command\AbstractCommand;
use core\html\tables\datatable\Datatable;

/**
 * Class AddRowCommand
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class AddRowCommand extends AbstractCommand
{
    public function __construct(public Datatable $target, public array $parameters = [])
    {
    }


    public function get(): string
    {
        return 'components/tables/datatable/AddRowCommand';
    }

    public function getContext(): string
    {
        return 't-datatable';
    }
}
