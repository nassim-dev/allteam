<?php

namespace core\html\tables\datatable\command;

use core\html\javascript\command\AbstractCommand;
use core\html\tables\datatable\Datatable;

/**
 * Class AddFilterCommand
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class AddFilterCommand extends AbstractCommand
{
    public function __construct(
        public Datatable $target,
        public string $column,
        public string $condition,
        public string $value,
        public array $parameters = []
    ) {
    }


    public function get(): string
    {
        return 'components/tables/datatable/AddFilterCommand';
    }

    public function getContext(): string
    {
        return 't-datatable';
    }
}
