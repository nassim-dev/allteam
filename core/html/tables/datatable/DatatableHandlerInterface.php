<?php

namespace core\html\tables\datatable;

use core\notifications\NotificationServiceInterface;

/**
 * Interface DatatableHandlerInterface
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
interface DatatableHandlerInterface
{
    /**
     * Api action to create new row
     *
     * @return void
     */
    public function createRowAction(NotificationServiceInterface $notifier);

    /**
     * Api action to edit a row
     *
     * @return void
     */
    public function updateRowAction(NotificationServiceInterface $notifier);
}
