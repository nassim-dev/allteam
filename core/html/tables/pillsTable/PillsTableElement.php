<?php

namespace core\html\tables\pillsTable;

use core\html\AbstractHtmlElement;
use core\html\calendar\Calendar;
use core\html\carousel\Carousel;
use core\html\chart\ChartInterface;
use core\html\diagram\Diagram;
use core\html\draglist\Draglist;
use core\html\flow\Flow;
use core\html\form\elements\interfaces\FieldInterface;
use core\html\form\Form;
use core\html\HtmlElementInterface;
use core\html\javascript\JavascriptObjectInterface;
use core\html\javascript\WithJavascript;
use core\html\tables\datatable\Datatable;
use core\html\tables\tableStriped\TableStriped;
use core\html\timeline\Timeline;
use core\html\traits\Colorable;
use core\html\traits\Iconable;
use core\html\traits\Identifiable;
use core\html\traits\Stylizable;
use core\html\traits\Titleable;
use core\html\treeview\Treeview;

/**
 * Class PillsTableElement
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class PillsTableElement extends AbstractHtmlElement implements JavascriptObjectInterface
{
    use Iconable;
    use Identifiable;
    use Titleable;
    use Colorable;
    use Stylizable;
    use WithJavascript;


    private bool $_default = false;

    /**
     * PillsTableElement constructor
     */
    public function __construct(string $id, string $title, ?string $icon = null, ?string $color = null)
    {
        $this->setTitle($title);
        $this->setIcon($icon);
        $this->setId($id);
        $this->setColor($color);
    }

    /**
     * @return Calendar[]|null
     */
    public function getCalendar(): ?array
    {
        return $this->getChilds()->filterBy(Calendar::class);
    }

    /**
     * @return ChartInterface[]|null
     */
    public function getChart(): ?array
    {
        return $this->getChilds()->filterBy(ChartInterface::class);
    }

    /**
     * @return Form[]|null
     */
    public function getForm(): ?Form
    {
        return $this->getChilds()->filterBy(Form::class);
    }

    /**
     * Get if form is explode in multiple pills
     *
     * @return FieldInterface[]|null
     */
    public function getFormElements(): ?array
    {
        return $this->getChilds()->filterBy(FieldInterface::class);
    }

    /**
     * Return Id attribute for html element
     */
    public function getIdAttribute(): string
    {
        return $this->getViewReference() . '_' . $this->getId();
    }

    /**
     * @return Datatable[]|null
     */
    public function getTable(): ?array
    {
        return $this->getChilds()->filterBy(Datatable::class);
    }

    /**
     * @return TableStriped[]|null
     */
    public function getTableStriped(): ?array
    {
        return $this->getChilds()->filterBy(TableStriped::class);
    }


    public function appendChild(?HtmlElementInterface &$child, ?string $identifier = null)
    {
        parent::appendChild($child, $identifier);
        if (method_exists($child, 'buildOn')) {
            $child->buildOn('shown.bs.tab', $this, 'deferred');
        }

        return $child;
    }

    /**
     * List of allowed childs elements
     * could be a class, an abstract class or an interface
     */
    protected static function getAllowedChilds(): array
    {
        return [
            Form::class,
            Datatable::class,
            ChartInterface::class,
            TableStriped::class,
            FieldInterface::class,
            Calendar::class,
            Draglist::class,
            Diagram::class,
            Carousel::class,
            TableStriped::class,
            Treeview::class,
            Timeline::class,
            Flow::class,
        ];
    }

    /**
     * Set the value of _default
     *
     * @PhpUnitGen\set("_default")
     */
    public function setDefault(bool $_default): self
    {
        $this->_default = $_default;

        if ($_default) {
            $this->restoreDefaultBuildMode();
        }

        return $this;
    }

    /**
     * Get the value of _default
     *
     * @PhpUnitGen\get("_default")
     */
    public function isDefault(): bool
    {
        return $this->_default;
    }
}
