<?php

namespace core\html\tables\pillsTable;

use core\DI\DI;
use core\html\AbstractHtmlElement;
use core\html\bar\Bar;
use core\html\HtmlElementContainer;
use core\html\traits\Stylizable;
use core\html\traits\Titleable;
use core\html\traits\WithActions;
use core\secure\permissions\PermissionServiceInterface;
use core\secure\permissions\PermissionSetInterface;
use core\utils\Utils;

/**
 * Class PillsTable
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class PillsTable extends AbstractHtmlElement
{
    use Titleable;
    use Stylizable;
    use WithActions;

    public const DROPDOWN = 'DROPDOWN';

    public const INLINE = 'INLINE';

    private string $menu = self::DROPDOWN;

    private $dropdownBar = null;

    /**
     * PillsTable constructor.
     */
    public function __construct(string $title)
    {
        $this->setTitle($title);
    }

    /**
     * Undocumented function
     */
    public function addPill(string $id, string $title, ?string $icon = null, ?string $color = null): PillsTableElement
    {
        $element = new PillsTableElement($id, $title, $icon, $color);

        return $this->appendChild($element, $id);
    }

    public function setDefault(string|PillsTableElement $element)
    {
        if (!is_string($element)) {
            $element->setDefault(true);

            return;
        }

        $element = $this->getChild($element);
        if ($element === null) {
            return;
        }

        /** @var PillsTableElement $element */
        $element->setDefault(true);
    }

    /**
     * Return Id attribute for html element
     */
    public function getIdAttribute(): string
    {
        return $this->getViewReference() . '_' . Utils::hash($this->getTitle());
    }

    public function getPills(): HtmlElementContainer
    {
        return $this->getChilds();
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function getPillsByid($id)
    {
        return $this->getChilds()->findObject($id);
    }

    /**
     * List of allowed childs elements
     * could be a class, an abstract class or an interface
     */
    protected static function getAllowedChilds(): array
    {
        return [PillsTableElement::class];
    }

    /**
     * Set the value of menu
     */
    public function setInlineMenu(): self
    {
        $this->menu = self::INLINE;

        return $this;
    }

    /**
     * Set the value of menu
     */
    public function setDropdownMenu(): self
    {
        $this->menu = self::DROPDOWN;

        return $this;
    }

    /**
     * Get the value of menu
     *
     * @PhpUnitGen\get("menu")
     */
    public function getMenu(): string
    {
        return $this->menu;
    }

    /**
     * Return action bar with pills menu
     */
    public function getDropdownBar(): Bar
    {
        if ($this->dropdownBar === null) {
            $this->dropdownBar = $this->getActionBar();
            $this->dropdownBar->setHtmlTemplate($this->dropdownBar::getBaseTemplateDir() . 'bar/bar.dropdown.html');

            /** @var PillsTableElement $pill */
            foreach ($this->getChilds() as $pill) {
                if (!$pill->hasChild()) {
                    continue;
                }

                $button = $this->dropdownBar->addButton($pill->getTitle());
                $button->setIcon($pill->getIcon());
                $button->addDataAttribute('bs-toggle', 'tab');
                $button->addDataAttribute('href', '#' . $pill->getIdAttribute());
                $button->addClass('nav-link');
                $button->enable();
            }

            $this->dropdownBar->enable();
        }

        return $this->dropdownBar;
    }

    /**
     * Default permissions for class
     */
    public static function buildDefaultPermissions(PermissionSetInterface $permissionSet): PermissionSetInterface
    {
        /** @var PermissionServiceInterface $permissionService */
        $permissionService = DI::singleton(PermissionServiceInterface::class);
        $permissionSet->addRequiredCondition($permissionService->getPermissionClass('@public'));


        return $permissionSet;
    }
}
