<?php

namespace core\html\tables\pillsTable;

use core\factory\FactoryInterface;
use core\html\HtmlElementFactory;

/**
 * Class PillsPillsTableFactory
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class PillsTableFactory extends HtmlElementFactory implements FactoryInterface
{
    /**
     * Return element class to build
     */
    protected function getElementClass(): string
    {
        return PillsTable::class;
    }

    /**
     * Return suffix for widget classes
     */
    protected function getWidgetSuffix(): string
    {
        return 'PillsTableWidget';
    }

    /**
     * Create new instance or return singleton if exist
     *
     * @PhpUnitGen\assertInstanceOf("PillsTable::class")
     */
    public function create(string $identifier, array $params = []): ?PillsTable
    {
        $params['title'] ??= $identifier;

        return parent::create($identifier, $params);
    }

    /**
     * Get registred elements
     */
    public function get(string $identifier): ?PillsTable
    {
        return $this->getInstance($identifier) ?? $this->create($identifier);
    }
}
