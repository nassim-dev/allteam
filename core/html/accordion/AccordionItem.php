<?php

namespace core\html\accordion;

use core\html\AbstractHtmlElement;
use core\html\HtmlElement;
use core\html\traits\Activeable;
use core\html\traits\Collapsable;
use core\html\traits\Identifiable;
use core\html\traits\Titleable;

/**
 * Class AccordionItem
 *
 * Description
 *
 * @category html_component
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class AccordionItem extends AbstractHtmlElement
{
    use Titleable;
    use Identifiable;
    use Collapsable;
    use Activeable;

    private static int $ids = 0;

    public function __construct(string $title, array $body, bool $active)
    {
        $this->title  = $title;
        $this->active = $active;
        $this->setCollapsable(true);
        $this->setBody($body);
        $this->id = self::$ids++;
    }

    /**
     * Return Id attribute for html element
     */
    public function getIdAttribute(): string
    {
        return $this->getViewReference() . '_' . $this->id;
    }

    /**
     * Set boy element
     *
     * @PhpUnitGen\set("body")
     *
     * @param array $body Boy element
     */
    public function setBody(array $body): self
    {
        $element = new HtmlElement($body);
        $this->appendChild($element);

        return $this;
    }

    /**
     * List of allowed childs elements
     * could be a class, an abstract class or an interface
     */
    protected static function getAllowedChilds(): array
    {
        return [HtmlElement::class];
    }
}
