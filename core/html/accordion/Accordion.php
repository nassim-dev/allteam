<?php

namespace core\html\accordion;

use core\html\AbstractHtmlElement;
use core\html\traits\Collapsable;
use core\html\traits\Deactivable;
use core\html\traits\Identifiable;
use core\html\traits\Stylizable;
use core\html\traits\Titleable;
use core\html\traits\WithActions;

/**
 * Class Accordion
 *
 * Representation off bootstrap accordion
 *
 * @category html_component
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Accordion extends AbstractHtmlElement
{
    use Stylizable;
    use Identifiable;
    use Titleable;
    use Deactivable;
    use Collapsable;
    use WithActions;
    public const IS_ACTIVE = true;

    /**
     * Vidéo
     */
    private ?string $video = null;

    /**
     * Accordion constructor.
     */
    public function __construct(string $id, ?string $title = null)
    {
        $this->setTitle($title ?? $id);
        $this->setId($id);
    }

    /**
     * @param array $body ['title' => "", "text" => ""]
     */
    public function addPill(string $title, array $body, bool $active = false): AccordionItem
    {
        $child = new AccordionItem($title, $body, $active);
        $this->appendChild($child);

        return $child;
    }

    /**
     * Return Id attribute for html element
     */
    public function getIdAttribute(): string
    {
        return $this->getViewReference() . '_' . $this->id;
    }

    /**
     * Get vidéo
     *
     * @PhpUnitGen\get("video")
     */
    public function getVideo(): ?string
    {
        return $this->video;
    }

    /**
     * Set vidéo
     *
     * @PhpUnitGen\set("video")
     *
     * @param string $video Vidéo
     */
    public function setVideo(string $video): self
    {
        $this->video = $video;

        return $this;
    }

    /**
     * List of allowed childs elements
     * could be a class, an abstract class or an interface
     */
    protected static function getAllowedChilds(): array
    {
        return [AccordionItem::class];
    }
}
