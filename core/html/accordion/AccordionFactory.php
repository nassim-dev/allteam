<?php

namespace core\html\accordion;

use core\factory\FactoryException;
use core\factory\FactoryInterface;
use core\html\HtmlElementFactory;

/**
 * Class AccordionFactory
 *
 * Description
 *
 * @example
 *
 * $element          = $this->create('test_accordion');
 *
 * $element->addPill('Title - ' . uniqid(), ['title' => 'MyTitle', 'text' => 'MyText'], $element::IS_ACTIVE);
 * $element->addPill('Title - ' . uniqid(), ['title' => 'MyTitle', 'text' => 'MyText']);
 *
 * $element->setTitle('My Accordion');
 *
 * $this->view->assignWidget($element);
 *
 * @category  html_component_factory
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class AccordionFactory extends HtmlElementFactory implements FactoryInterface
{
    /**
     * Return element class to build
     */
    protected function getElementClass(): string
    {
        return Accordion::class;
    }

    /**
     * Return suffix for widget classes
     */
    protected function getWidgetSuffix(): string
    {
        return 'AccordionWidget';
    }

    /**
     * Create new instance
     *
     * @param array $parameters
     *
     * @throws FactoryException
     */
    public function create(string $identifier, array $params = []): ?Accordion
    {
        $params['title'] ??= null;
        $params['id'] ??= $identifier;

        return parent::create($identifier, $params);
    }

    /**
     * Get registred elements
     */
    public function get(string $identifier): ?Accordion
    {
        return $this->getInstance($identifier) ?? $this->create($identifier);
    }
}
