<?php

namespace core\html;

use core\html\traits\DataAttribute;
use core\html\traits\Identifiable;

/**
 * Class HtmlElement
 *
 * Description
 *
 * @category html_component
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class HtmlElement extends AbstractHtmlElement implements HtmlElementInterface
{
    use Identifiable;
    use DataAttribute;

    private mixed $contents = null;

    public function __construct(array $properties)
    {
        foreach ($properties as $prop => $val) {
            $this->{$prop} = $val;
        }

        $this->setId(uniqid());
    }

    /**
     * Get the value of contents
     *
     * @PhpUnitGen\get("contents")
     */
    public function getContents(): mixed
    {
        return $this->contents;
    }

    /**
     * Return Id attribute for html element
     */
    public function getIdAttribute(): string
    {
        return $this->getViewReference() . '_' . $this->getId();
    }

    /**
     * Set the value of contents
     *
     * @PhpUnitGen\set("contents")
     *
     * @param mixed $contents
     */
    public function setContents($contents): self
    {
        $this->contents = $contents;

        return $this;
    }

    /**
     * List of allowed childs elements
     * could be a class, an abstract class or an interface
     */
    protected static function getAllowedChilds(): array
    {
        return [HtmlElementInterface::class];
    }
}
