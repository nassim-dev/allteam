<?php

namespace core\html\timeline;

use core\factory\FactoryInterface;
use core\html\HtmlElementFactory;

/**
 * Class Timelineactory
 *
 * Timeline Factory
 *
 * @category  Factory
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class TimelineFactory extends HtmlElementFactory implements FactoryInterface
{
    public const DATAID = 'dataId';

    public const NAME = 'name';

    /**
     * Return element class to build
     */
    protected function getElementClass(): string
    {
        return Timeline::class;
    }

    /**
     * Return suffix for widget classes
     */
    protected function getWidgetSuffix(): string
    {
        return 'TimelineWidget';
    }

    /**
     * Create new instance or return singleton if exist
     *
     * @PhpUnitGen\assertInstanceOf("Timeline::class")
     */
    public function create(string $identifier, array $params = []): ?Timeline
    {
        $params[self::NAME] ??= 'Planning';
        $params['identifier'] ??= $identifier;
        $params[self::DATAID] ??= '0';

        return parent::create($identifier, $params);
    }

    /**
     * Get registred elements
     */
    public function get(string $identifier): ?Timeline
    {
        return $this->getInstance($identifier) ?? $this->create($identifier);
    }
}
