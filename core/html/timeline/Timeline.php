<?php

namespace core\html\timeline;

use core\html\AbstractHtmlElement;
use core\html\form\elements\DaterangeElement;
use core\html\javascript\JavascriptObjectInterface;
use core\html\javascript\WithJavascript;
use core\html\traits\AjaxSourced;
use core\html\traits\Collapsable;
use core\html\traits\Identifiable;
use core\html\traits\Namable;
use core\html\traits\WithActions;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Timeline extends AbstractHtmlElement implements JavascriptObjectInterface
{
    use WithJavascript;
    use WithActions;
    use Namable;
    use Identifiable;
    use Collapsable;
    use AjaxSourced;


    /**
     * @param string $identifier The identifier
     * @param string $name       The name
     * @param string $dataid     The dataid
     */
    public function __construct(string $identifier, string $name = 'Timeline')
    {
        $this->setId($identifier);
        $this->setName($name);
    }

    /**
     * Add field selector to change the view
     *
     * @return self
     */
    public function addDateSelector()
    {
        $selector = $this->addField('dateSelector', DaterangeElement::class);

        $config = $this->addConfig('dateSelector');
        $config->addParameter('id', $selector->getIdAttribute());

        return $this;
    }

    /**
     * Add button to fit all items in view
     *
     * @return self
     */
    public function addFitSelector()
    {
        $selector = $this->addButton(_('Fit all items'));
        $selector->addClasses([' btn-light-success']);
        $config = $this->addConfig('fitSelector');
        $config->addParameter('id', $selector->getIdAttribute());

        return $this;
    }

    /**
     * Add button to change to today
     *
     * @return self
     */
    public function addTodaySelector()
    {
        $selector = $this->addButton(_('Today'));
        $selector->addClasses([' btn-light-success']);
        $config = $this->addConfig('todaySelector');
        $config->addParameter('id', $selector->getIdAttribute());

        return $this;
    }

    /**
     * Return Id attribute for html element
     */
    public function getIdAttribute(): string
    {
        return $this->getViewReference() . '_' . $this->getId();
    }


    /**
     * List of allowed childs elements
     * could be a class, an abstract class or an interface
     */
    protected static function getAllowedChilds(): array
    {
        return [];
    }
}
