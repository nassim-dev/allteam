<?php

namespace core\html\progressBar;

use core\html\AbstractHtmlElement;
use core\html\traits\Identifiable;
use core\html\traits\Titleable;

/**
 * Class ProgressBar
 *
 * Generate Bootstrap Progress bar
 *
 * @category Generate Bootstrap Progress bar
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class ProgressBar extends AbstractHtmlElement
{
    use Identifiable;
    use Titleable;

    /**
     * Constructor
     *
     * @param int         $percent Percent of complete
     * @param string      $type    Color
     * @param string|null $title   Title
     */
    public function __construct(private int $percent, private string $type = 'info', ?string $title = null, private ?string $subtitle = null)
    {
        $this->setId(uniqid());
        $this->title = $title;
    }

    /**
     * Return Id attribute for html element
     */
    public function getIdAttribute(): string
    {
        return $this->getViewReference() . '_' . $this->getId();
    }


    /**
     * @PhpUnitGen\get("_percent")
     */
    public function getPercent(): int
    {
        return $this->percent;
    }

    /**
     * @PhpUnitGen\get("_subtitle")
     */
    public function getSubtitle(): ?string
    {
        return $this->subtitle;
    }

    /**
     * @PhpUnitGen\get("_type")
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * List of allowed childs elements
     * could be a class, an abstract class or an interface
     */
    protected static function getAllowedChilds(): array
    {
        return [];
    }
}
