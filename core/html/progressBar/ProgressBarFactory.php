<?php

namespace core\html\progressBar;

use core\factory\FactoryInterface;
use core\html\HtmlElementFactory;

/**
 * Class ProgressBarFactory
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class ProgressBarFactory extends HtmlElementFactory implements FactoryInterface
{
    public const PERCENTAGE = 'percent';

    public const SUBTITLE = 'subtitle';

    public const TYPE = 'type';

    public const TITLE = 'title';

    /**
     * Return element class to build
     */
    protected function getElementClass(): string
    {
        return ProgressBar::class;
    }

    /**
     * Return suffix for widget classes
     */
    protected function getWidgetSuffix(): string
    {
        return 'ProgressBarWidget';
    }

    /**
     * Create new instance or return singleton if exist
     *
     * @PhpUnitGen\assertInstanceOf("ProgressBar::class")
     */
    public function create(string $identifier, array $params = []): ?ProgressBar
    {
        $params[self::PERCENTAGE] ??= 0;
        $params[self::TYPE] ??= 'primary';
        $params[self::SUBTITLE] ??= null;
        $params[self::TITLE] ??= null;

        return parent::create($identifier, $params);
    }

    /**
     * Get registred elements
     */
    public function get(string $identifier): ?ProgressBar
    {
        return $this->getInstance($identifier) ?? $this->create($identifier);
    }
}
