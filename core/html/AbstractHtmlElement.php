<?php

namespace core\html;

use core\DI\DI;
use core\DI\DiProvider;
use core\DI\InjectionAttribute;
use core\html\traits\Contenable;
use core\secure\components\RessourceInterface;
use core\secure\components\RessourceTrait;
use core\secure\permissions\PermissionSetInterface;
use core\utils\ClassFinder;
use core\utils\Utils;
use core\view\ViewServiceInterface;

/**
 * Class AbstractHtmlElement
 *
 * Represent an html element && provides default method to link objects
 * It's use to perform factories getInstances(filter());
 *
 * @category  Description
 * @version   Release: 0.1
 * @package Allteam
 *
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 */
abstract class AbstractHtmlElement implements HtmlElementInterface, RessourceInterface
{
    use DiProvider;
    use Contenable;
    use RessourceTrait;

    /**
     * Html template to use
     *
     * @var string|null
     */
    protected $htmlTemplate = null;

    /**
     * Html Tag
     */
    protected bool|string|null $htmlTag = true;

    /**
     * Is Element locked
     */
    protected bool $lock = false;

    /**
     * True if element is a design container
     *
     * @var boolean
     */
    protected bool $isContainer = false;

    /**
     * True if element is a design container
     *
     * @var boolean
     */
    protected bool $isInsideContainer = false;

    /**
     * View reference string
     *
     * @var array
     */
    protected static $viewReference = [];

    /**
     * Clone form
     *
     * @PhpUnitGen\assertNotNull()
     *
     * @return static
     */
    public function __clone()
    {
        if ($this->childs !== null) {
            $childs       = $this->childs;
            $this->childs = null;
            foreach ($childs as $child) {
                /** @var HtmlElementInterface $child */
                $cloned = clone $child;
                $cloned->setParent(null);
                $this->appendChild($cloned, $cloned->getReference());
            }
        }
    }

    public function getReference(): ?string
    {
        return $this->getIdAttribute();
    }

    public function __invoke(): string
    {
        return $this->getDi()->call($this, 'getHtml');
    }


    /**
     * Return template directory
     */
    public static function getBaseTemplateDir(): string
    {
        return BASE_DIR . 'templates/components/';
    }

    /**
     * Return Html attribute for customElement
     */
    public static function getDefaultHtmlTag(): string
    {
        $path = explode('\\', static::class);

        return 't-' . strtolower(array_pop($path));
    }

    /**
     * @return mixed
     */
    public function getHash(): string
    {
        return $this->getIdAttribute();
    }

    /**
     * Return as html
     */
    #[InjectionAttribute(ViewServiceInterface::class, build:InjectionAttribute::UNIQUE)]
    public function getHtml(?ViewServiceInterface $view = null, ?string $template = null): string
    {
        $view ??= $this->getDi()->make(ViewServiceInterface::class);
        if ($template != null) {
            $template = BASE_DIR . $template;
        }

        return ($this->isLocked()) ? '' : preg_replace('/\s+/', ' ', $view->renderToString(($template ?? $this->getHtmlTemplate()), ['element' => $this]));
    }

    public function getHtmlTag(): ?string
    {
        return ($this->htmlTag && !is_string($this->htmlTag)) ? static::getDefaultHtmlTag() : ($this->htmlTag ?? null);
    }

    /**
     * Get html template to use
     *
     * @PhpUnitGen\get("htmlTemplate")
     */
    public function getHtmlTemplate(): string
    {
        return $this->htmlTemplate ?? $this->getDefaultTemplate();
    }

    /**
     * Return Id attribute for html element
     */
    abstract public function getIdAttribute(): string;

    /**
     * Reference for smarty assignment
     */
    public function getViewReference(): string
    {
        if (!isset(static::$viewReference[static::class])) {
            $class                                = static::class;
            $class                                = explode('\\', $class);
            static::$viewReference[static::class] = strtolower(end($class));
        }

        return static::$viewReference[static::class];
    }



    public function isVueComponent(): bool
    {
        return method_exists($this, '_toArrayVue');
    }

    /**
     * Parse vue config if exists
     *
     * @return array
     */
    public function getVueConfig(): array
    {
        if (!$this->isVueComponent()) {
            return [];
        }

        $vueConfig = $this->_toArrayVue();
        foreach ($vueConfig['vue-config'] as $key => $value) {
            if (is_bool($value)) {
                $vueConfig['vue-config'][$key] = $value;
            }
        }

        return $vueConfig['vue-config'];
    }

    /**
     * Return true if class is instance of$class
     */
    public function isA(string $class): bool
    {
        $path = explode('\\', $this::class);

        return (strtolower(array_pop($path)) === strtolower($class));
    }

    /**
     * Reset element html tag
     *
     * @return void
     */
    public function isBuildInElement(): void
    {
        $this->htmlTag = false;
    }


    /**
     * Get is Element locked
     *
     * @PhpUnitGen\get("lock")
     *
     * @return boolean
     */
    public function isLocked(): bool
    {
        return $this->lock;
    }

    /**
     * Lock element
     */
    public function lock(): self
    {
        $this->lock = true;

        return $this;
    }

    /**
     * Return class prefix
     *
     * @return string
     */
    public static function prefix(): string
    {
        $class = explode('\\', static::class);

        return strtolower(end($class)) . '_';
    }

    /**
     * Set html template to use
     *
     * @PhpUnitGen\set("htmlTemplate")
     *
     * @param string $htmlTemplate Html template to use
     */
    public function setHtmlTemplate(string $htmlTemplate): self
    {
        $this->htmlTemplate = $htmlTemplate;

        return $this;
    }

    /**
     * Return an object as array
     */
    public function toArray(): array
    {
        $default = Utils::arrayPrinter(
            object:get_object_vars($this),
            filtered:[
                'di',
                'diWrapper',
                'htmlTemplate',
                'htmlTag',
                '_permissionSet',
                'auth'
            ]
        );

        $return = [
            'id'   => '#' . $this->getIdAttribute(),
            'type' => $this->getViewReference()
        ];

        $methods = DI::singleton(ClassFinder::class)->getTraitsMethods($this, '_toArray');

        foreach ($methods as $method) {
            $return = array_merge($return, $this->{$method}());
        }

        $return['parent'] = $this->getParent()?->getIdAttribute() ?? null;

        return array_merge_recursive($default, $return);
    }

    /**
     * Unlock element
     */
    public function unlock(): self
    {
        $this->lock = false;

        return $this;
    }


    private function getDefaultTemplate(): string
    {
        $path  = explode('\\', $this::class);
        $class = array_pop($path);

        $directory = array_splice($path, 2, count($path));
        $directory = implode('/', $directory);
        $directory = (null === $directory) ? $directory : $directory . '/';

        return self::getBaseTemplateDir() . "$directory$class.html";
    }

    public static function buildDefaultPermissions(PermissionSetInterface $permissionSet): PermissionSetInterface
    {
        return $permissionSet;
    }

    public function getOwner(): ?int
    {
        return null;
    }

    /**
     * Get true if element is a design container
     *
     * @PhpUnitGen\get("isContainer")
     *
     * @return bool
     */
    public function isContainer(): bool
    {
        return $this->isContainer;
    }

    /**
     * Get true if element is inside a design container
     *
     * @PhpUnitGen\get("isInsideContainer")
     *
     * @return bool
     */
    public function isInsideContainer(): bool
    {
        return $this->isInsideContainer;
    }

    /**
     * Define as container
     *
     * @return static
     */
    public function setHasContainer()
    {
        $this->isContainer = true;

        return $this;
    }

    /**
     * Define as container
     *
     * @return static
     */
    public function setInsideContainer()
    {
        $this->isInsideContainer = true;

        return $this;
    }
}
