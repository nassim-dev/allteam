<?php

namespace core\html\diagram;

use core\config\Conf;
use core\html\AbstractHtmlElement;
use core\html\javascript\JavascriptObjectInterface;
use core\html\javascript\WithJavascript;
use core\html\traits\Collapsable;
use core\html\traits\Identifiable;
use core\html\traits\Titleable;
use core\html\traits\WithActions;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Diagram extends AbstractHtmlElement implements JavascriptObjectInterface
{
    use WithJavascript;
    use Identifiable;
    use Titleable;
    use WithActions;
    use Collapsable;

    /**
     * @var mixed
     */
    private $format = null;

    private array $initialDatas = [];

    /**
     * @return static
     */
    public function __construct(string $identifier, string $format = 'conceptMap')
    {
        $this->setId($identifier);
        $this->setFormat($format);
    }

    /**
     * @return mixed
     */
    public function addNode(array $node): self
    {
        if (!isset($this->initialDatas['nodeDataArray'])) {
            $this->initialDatas['nodeDataArray'] = [];
        }

        $this->initialDatas['nodeDataArray'][] = $node;

        return $this;
    }

    /**
     * Get the value of format
     *
     * @PhpUnitGen\get("format")
     */
    public function getFormat(): string
    {
        return $this->format;
    }

    /**
     * Return Id attribute for html element
     */
    public function getIdAttribute(): string
    {
        return $this->getViewReference() . '_' . $this->getId();
    }

    /**
     * Get the value of initialDatas
     *
     * @PhpUnitGen\get("initialDatas")
     */
    public function getInitialDatas(): array
    {
        return $this->initialDatas;
    }

    /**
     * @return mixed
     */
    public function linkNodes(string $nodeIdFrom, string $nodeIdTo, string $text = ''): self
    {
        if (!isset($this->initialDatas['linkDataArray'])) {
            $this->initialDatas['linkDataArray'] = [];
        }

        $this->initialDatas['linkDataArray'][] = [
            'form' => $nodeIdFrom,
            'to'   => $nodeIdTo,
            'text' => $text
        ];

        return $this;
    }

    /**
     * Set the value of format
     *
     * @PhpUnitGen\set("format")
     */
    public function setFormat(string $format): self
    {
        $this->format = $format;

        return $this;
    }

    /**
     * Set the value of initialDatas
     *
     * @PhpUnitGen\set("initialDatas")
     * @param mixed[] $initialDatas
     */
    public function setInitialDatas(array $initialDatas): self
    {
        $this->initialDatas = $initialDatas;

        return $this;
    }

    /**
     * Return an Array for object jsConfig
     */
    public function toArray(): array
    {
        $configArray                 = parent::toArray();
        $configArray['initialDatas'] = $this->getInitialDatas();
        $configArray['format']       = $this->getFormat();

        return $configArray;
    }

    /**
     * List of allowed childs elements
     * could be a class, an abstract class or an interface
     */
    protected static function getAllowedChilds(): array
    {
        return [];
    }
}

/*

Example with moudles pages
$diagramHelp = DiagramFactory::createDiagram("faq");

$tableLinked = Conf::find('TABLE_LINKED', "Sys");
$nodeDataArray = [];
$linkDataArray = [];

$passedNodes = [];
$passedLinks = [];
$lastNode = false;
foreach ($tableLinked as $indexKey => $values) {
if (!empty($values)) {
     $mainTable = (str_starts_with($indexKey, 'id')) ? substr($indexKey, 2) : $indexKey;
$idunique = uniqid();
if (defined("MENU_TITLE_" . strtoupper($mainTable))) {
$constant =  constant("MENU_TITLE_" . strtoupper($mainTable));
$intialTable = $mainTable;
$mainTable = $mainTable . $idunique;

if ($intialTable !== "instance") {

$nodeDataArray[] = ['key' =>  $mainTable, "text" => $constant];

foreach ($values["table"] as $table) {
if (defined("MENU_TITLE_" . strtoupper($table))) {
$constant =  constant("MENU_TITLE_" . strtoupper($table));
if ($table !== "instance") {
$table = $table . $idunique;
$nodeDataArray[] = ['key' => $table, "text" => $constant];

if (!in_array($mainTable . $table, $passedLinks)) {
$linkDataArray[] = ["from" => $table, "text" => "owns", "to" => $mainTable,];
$passedLinks[] = $mainTable . $table;
}
}
}
}
$object = Entitie::create(ucfirst($intialTable), []);
foreach ($object::getLinkTables() as $table) {
$table = strtolower($table);
$constant =  constant("MENU_TITLE_" . strtoupper($table));
if ($table !== "instance") {
$table = $table . $idunique;
$nodeDataArray[] = ['key' => $table, "text" => $constant];

if (!in_array($mainTable . $table, $passedLinks)) {
$linkDataArray[] = ["from" => $mainTable, "text" => "hasMultiples", "to" => $table];
$passedLinks[] = $mainTable . $table;
}
}
}
if ($lastNode !== false) {
if (!in_array($mainTable . $lastNode, $passedLinks)) {
$linkDataArray[] = ["from" => $lastNode, "text" => "owns", "to" => $mainTable];
}
}
$lastNode = $mainTable;
}
}
}
}

$diagramHelp->setInitialDatas(["nodeDataArray" => $nodeDataArray, "linkDataArray" => $linkDataArray]);
$diagramHelp->setTitle("TEST");

$this->view->assign("diagram", $diagramHelp);*/
