<?php

namespace core\html\diagram;

use core\factory\FactoryInterface;
use core\html\HtmlElementFactory;

class DiagramFactory extends HtmlElementFactory implements FactoryInterface
{
    /**
     * Return element class to build
     */
    protected function getElementClass(): string
    {
        return Diagram::class;
    }

    /**
     * Return suffix for widget classes
     */
    protected function getWidgetSuffix(): string
    {
        return 'DiagramWidget';
    }

    /**
     * Create new instance or return singleton if exist
     *
     * @PhpUnitGen\assertInstanceOf("Diagram::class")
     */
    public function create(string $identifier, array $params = []): ?Diagram
    {
        $params['identifier'] ??= $identifier;

        return parent::create($identifier, $params);
    }

    public function get(string $identifier): ?Diagram
    {
        return $this->getInstance($identifier) ?? $this->create($identifier);
    }
}
