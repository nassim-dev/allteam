<?php

namespace core\html\form\elements;

use core\html\form\Form;
use core\html\traits\AjaxSourced;

/**
 * Class MentionableElement
 *
 * Jquery mentionable field
 *
 * @category Jquery mentionable field
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class MentionableElement extends FormElement
{
    use AjaxSourced;

    /**
     * Api ressource
     *
     * @var string|null
     */
    protected $ressource;

    /**
     * @PhpUnitGen\assertInstanceOf("MentionableElement::class")
     */
    public function __construct(?Form $form = null)
    {
        //Assets::addJqueryMentionable();
        $this->_type = 'mentionable';
        parent::__construct($form);
    }

    /**
     * Set api ressource
     *
     * @return static
     */
    public function setRessource(string $ressource)
    {
        $this->ressource = $ressource;
        //$ressource       = (class_exists($ressource)) ? $ressource : '\app\api\controller\\' . ucfirst($ressource) . 'Controller';

        return $this;
    }
}
