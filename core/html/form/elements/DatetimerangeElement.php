<?php

namespace core\html\form\elements;

use core\html\form\elements\traits\Regexable;
use core\html\form\elements\traits\WithMask;
use core\html\form\elements\traits\WithPlaceholder;
use core\html\form\Form;
use core\utils\Utils;

/**
 * Class DatetimerangeElement
 *
 * Datetimerange field
 *
 * @category  Datetimerange field
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class DatetimerangeElement extends FormElement
{
    use Regexable;
    use WithMask;
    use WithPlaceholder;

    /**
     * @PhpUnitGen\assertInstanceOf("DatetimerangeElement::class")
     */
    public function __construct(?Form $form = null)
    {
        $this->_type = 'datetimerange';
        $this->placeholder('JJ/MM/AAAA HH:MM');
        $this->mask("'mask': '99/99/9999 99:99 aa 99/99/9999 99:99'");
        $this->setAutocomplete(false);
        $this->setIcon('fa-calendar');
        //Assets::addDatetimePicker();
        $this->setHtmlTemplate(self::getBaseTemplateDir() . 'form/elements/DateElement.html');

        parent::__construct($form);
    }

    /**
     * Get one element value
     */
    public function getElementValue(array &$out): array
    {
        if (isset($out[$this->getName()]) && !isset($out[$this->getName() . '_start']) && !isset($out[$this->getName() . '_end'])) {
            $elements = explode(' to ', $out[$this->getName()]);

            $out[$this->getName() . '_start'] = Utils::createDateTime($elements[0]);
            $out[$this->getName() . '_end']   = Utils::createDateTime($elements[1]);
        }

        return $out;
    }

    /**
     * Check if field is valid
     */
    public function isValid(array $array): bool
    {
        $array = $this->getElementValue($array);
        if (!$this->isRequired() || (isset($array[$this->getName() . '_start'], $array[$this->getName() . '_end']))) {
            return true;
        }

        $this->_errors[] = $this->getLabel();

        return false;
    }
}
