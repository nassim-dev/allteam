<?php

namespace core\html\form\elements;

use core\html\form\elements\traits\SelectTrait;
use core\html\form\elements\traits\VueTagsInputTrait;
use core\html\form\Form;

/**
 * Class TagElement
 *
 * Select multiple field
 *
 * @category  Select multiple field
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class TagElement extends FormElement
{
    use VueTagsInputTrait;
    use SelectTrait;

    /**
     * @PhpUnitGen\assertInstanceOf("TagElement::class")
     */
    public function __construct(?Form $form = null)
    {
        $this->_type = 'tag';

        parent::__construct($form);
        $this->htmlTag = 'vue-tags-input';
    }
}
