<?php

namespace core\html\form\elements;

use core\html\form\elements\traits\WithMask;
use core\html\form\Form;

/**
 * Class PasswordElement
 *
 * Passwoird field
 *
 * @category Passwoird field
 * @version 0.1
 * @PhpUnitGen\assertClassHasAttribute("'_mask'")
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 * @since File available since Release 0.1
 */
class PasswordElement extends FormElement
{
    use WithMask;

    /**
     * @PhpUnitGen\assertInstanceOf("PasswordElement::class")
     */
    public function __construct(?Form $form = null)
    {
        $this->_type = 'password';
        $this->setIcon('fa-lock');

        parent::__construct($form);
    }
}
