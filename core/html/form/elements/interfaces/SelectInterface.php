<?php

namespace core\html\form\elements\interfaces;

use core\routing\Link;

/**
 * Interface SelectInterface
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
interface SelectInterface extends FieldInterface
{
    public const NO_BUTTON = false;

    public const CREATE_BUTTON = true;



    /**
     * Add buttons to open modals
     *
     * @return static
     */
    public function btn(string $ressource, array $options = [], ?string $idfield = null);

    /**
     * Add Select2Cascade
     *
     * @return static
     */
    public function cascadeOn(
        SelectInterface $selectChild,
        array $params = [
            'action'    => 'formList',
            'ressource' => null,
            'trigger'   => true
        ]
    );

    /**
     * Set option for select2 create tag
     *
     * @PhpUnitGen\set("createInline")
     *
     * @param array $createInline Option for select2 create tag
     *
     * @return static
     */
    public function createInline(array $createInline);

    /**
     * Get value of _btn
     *
     * @PhpUnitGen\get("_btn")
     */
    public function getBtn(): ?string;

    /**
     * Get option for select2 create tag
     *
     * @PhpUnitGen\get("createInline")
     *
     * @return array|null:
     */
    public function getCreateInline(): ?array;

    /**
     * Get value of _defaultMultiChoice
     *
     * @PhpUnitGen\get("_defaultMultiChoice")
     *
     * @return mixed
     */
    public function getDefaultMultiChoice();

    /**
     * Get value of _options
     *
     * @PhpUnitGen\get("_options")
     */
    public function getOptions(): ?array;

    /**
     * Get si on utilise des catégories
     */
    public function getSelectFilter(): ?array;

    /**
     * Get html Template for select
     *
     * @PhpUnitGen\get("_selectTemplate")
     */
    public function getSelectTemplate(): ?string;

    /**
     * Link select with selectChild
     *
     * @param SelectInbterface $selectChild
     *
     * @todo rewrite valid jav ascript code
     *
     * @return static
     */
    public function linkTo(SelectInterface $selectChild);

    /**
     * Add event onChange to trig autosave popup
     */
    public function isAutoSaved(): bool;

    /**
     * Set value of _options
     *
     * @PhpUnitGen\set("_options")
     *
     * @return static
     */
    public function options(?array $options);

    /**
     * Set html Template for select
     *
     * @PhpUnitGen\set("_selectTemplate")
     *
     * @param string|null $_selectTemplate Html Template for select
     *
     * @return static
     */
    public function selectTemplate(?string $_selectTemplate);

    /**
     * Set select without ajax
     *
     * @return static
     */
    public function setBasicSelect();

    /**
     * Set api ressource
     *
     * @return static
     */
    public function setRessource(string $ressource);

    /**
     * Set si on utilise des catégories
     *
     * @PhpUnitGen\set("selectFilter")
     *
     * @param array $selectFilter Si on utilise des catégories
     *
     * @return static
     */
    public function setSelectFilter(array $selectFilter);
}
