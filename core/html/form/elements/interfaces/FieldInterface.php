<?php

namespace core\html\form\elements\interfaces;

use core\html\javascript\JavascriptObjectInterface;

/**
 * Interface FieldInterface
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
interface FieldInterface extends JavascriptObjectInterface
{
    /**
     * Get default sanitizer
     *
     * @PhpUnitGen\get("sanitizer")
     *
     * @return string
     */
    public function getSanitizer(): string;

    public function setSanitizer(string $sanitizer): static;

    /**
     * Get element value
     */
    public function getElementValue(array &$out): array;

    /**
     * Check if field is external
     *
     * @return boolean
     */
    public function isExternal(): bool;

    /**
     * Define element as external
     *
     * @return void
     */
    public function setExternal(?string $entitieClass);

    /**
     * Get id
     *
     * @PhpUnitGen\get("id")
     */
    public function getId(): ?string;

    /**
     * Get label for action
     *
     * @PhpUnitGen\get("label")
     */
    public function getLabel(): ?string;

    /**
     * Get name for action
     *
     * @PhpUnitGen\get("name")
     */
    public function getName(): ?string;

    /**
     * Get value for action
     *
     * @PhpUnitGen\get("value")
     */
    public function getValue(): mixed;


    /**
     * Set id
     *
     * @PhpUnitGen\set("id")
     *
     * @param string $id Id for action
     *
     * @return static
     */
    public function setId(string $id);

    /**
     * Set label for action
     *
     * @PhpUnitGen\set("label")
     *
     * @param string $label Label for action
     *
     * @return static
     */
    public function setLabel(string $label);

    /**
     * Set value of _required
     *
     * @PhpUnitGen\set("_required")
     *
     * @return static
     */
    public function required(bool $required);

    /**
     * Set name for action
     *
     * @PhpUnitGen\set("name")
     *
     * @param string $name Name for action
     *
     * @return static
     */
    public function setName(string $name);

    /**
     * Set name for action
     *
     * @PhpUnitGen\set("value")
     *
     * @param string $value Value for action
     *
     * @return static
     */
    public function setValue(string $value);

    /**
     * Disale element
     *
     * @return static
     */
    public function disable();

    /**
     * @return static
     */
    public function enable();

    /**
     * Check if element is disabled
     *
     * @return boolean
     */
    public function isDisabled(): bool;

    /**
     * Check if element is enabled
     *
     * @return boolean
     */
    public function isEnabled(): bool;
}
