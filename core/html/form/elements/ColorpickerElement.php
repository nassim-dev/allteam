<?php

namespace core\html\form\elements;

use core\html\form\elements\traits\Regexable;
use core\html\form\elements\traits\WithMask;
use core\html\form\Form;

/**
 * Class ColorpickerElement
 *
 * Colopicker field
 *
 * @category Colopicker field
 * @version 0.1
 * @PhpUnitGen\assertClassHasAttribute("_regex")
 * @PhpUnitGen\assertClassHasAttribute("_mask")
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 * @since File available since Release 0.1
 */
class ColorpickerElement extends FormElement
{
    use Regexable;
    use WithMask;

    /**
     * @PhpUnitGen\assertInstanceOf("CguElement::class")
     */
    public function __construct(?Form $form = null)
    {
        //Assets::addColorPicker();
        $this->_type = 'colorpicker';
        $this->setIcon('fa-paint-brush');

        parent::__construct($form);
    }
}
