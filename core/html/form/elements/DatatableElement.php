<?php

namespace core\html\form\elements;

use core\DI\DI;
use core\html\CommandFactory;
use core\html\form\Form;
use core\html\form\FormFactory;
use core\html\javascript\command\ChangeAttributeCommand;
use core\html\javascript\utils\JsUtils;
use core\html\modal\command\ModalShowCommand;
use core\html\modal\Modal;
use core\html\modal\ModalFactory;
use core\html\tables\datatable\Datatable;

/**
 * Class DatatableElement
 *
 * Datatable as field
 *
 * @PhpUnitGen\assertClassHasAttribute("_forms")
 *
 * @category  Datatable as field
 * @version   0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class DatatableElement extends FormElement
{
    /**
     * DatatableElement constructor.
     *
     * @param string|null $form
     *
     * @return static
     */
    public function __construct(?Form $form = null)
    {
        $this->_type = 'datatable';

        parent::__construct($form);
    }

    /**
     * Set registred forms
     *
     * @param Form $form Registred forms
     */
    public function addForm(Form $form): self
    {
        $this->appendChild($form);

        return $this;
    }

    /**
     * Get one element value
     *
     * @return void
     */
    public function getElementValue(array &$out): array
    {
        $out[$this->getName()] = json_decode(htmlspecialchars_decode($this->getValue()), true, 512, JSON_THROW_ON_ERROR);
        $temp                  = [];
        if (!is_array($out[$this->getName()])) {
            $out[$this->getName()] = [];

            return $out;
        }

        foreach ($out[$this->getName()] as $row) {
            $tempRow = [];
            foreach ($row as $key => $val) {
                if (!in_array($key, ['DT_RowId', 'checkbox', 'tableactions', 'temporary_id'])) {
                    $tempRow[$key] = (is_array($val)) ? $val[0] : trim($val);
                }
            }

            $temp[] = $tempRow;
        }

        $out[$this->getName()] = $temp;

        return $out;
    }



    /**
     * Link to datatable element
     */
    public function table(Datatable $table, string $type): self
    {
        $table->addConfig('localStorage')
            ->value(true);
        $table->addConfig('data')
            ->value([]);

        $table->disableEditor();

        $this->addTableButtons($table, $type);
        $this->addFormsModals();
        $this->setValue($table->getIdAttribute());
        $this->appendChild($table);

        return $this;
    }

    /**
     * List of allowed childs elements
     * could be a class, an abstract class or an interface
     */
    protected static function getAllowedChilds(): array
    {
        return [Form::class, Datatable::class];
    }


    /**
     * Create forms && modals
     *
     * @return void
     */
    private function addFormsModals()
    {
        /**
         * @var Form
         */
        $parent      = $this->getParent();
        $ressource   = explode('_', $parent->getName() ?? '');
        $ucRessource = ucfirst($ressource[1] ?? $ressource[0]);

        $formAdd = DI::singleton(FormFactory::class)->create($ucRessource, ['add' . ucfirst($this->getName())]);
        $formAdd->add(HiddenElement::class)
            ->name('table')
            ->value(0);

        $this->addForm($formAdd);

        $modalFactory = DI::singleton(ModalFactory::class);
        $modal        = $modalFactory->create('create_' . $this->getName());
        $modal->addForm($formAdd);

        $formEdit = DI::singleton(FormFactory::class)->create($ucRessource, ['edit' . ucfirst($this->getName())]);
        $formEdit->add(HiddenElement::class)
            ->name('table')
            ->value(0);

        $this->addForm($formEdit);

        $modal = $modalFactory->create('update_' . $this->getName());
        $modal->addForm($formEdit);
    }

    private function addTableButtons(Datatable $table, string $type)
    {
        /** @var ModalFactory $modalFactory */
        $modalFactory = DI::singleton(ModalFactory::class);

        $modalFactory->createThen('create_' . $this->getName(), function (Modal $modal) use ($table, $type): void {
            /**
             * @var Form
             */
            $parent    = $this->getParent();
            $ressource = explode('_', $parent->getName() ?? '');
            $addButton = $table->addButton(_('Add'));
            $addButton->addDataAttributes(
                [
                    'table' => ucfirst($type) . '_' . end($ressource) . '_' . $this->getName()
                ]
            )
                ->setIcon('fa-plus')
                ->addClasses([' btn-light-success', 'heartBeat', 'delay-3s']);

            $addButton->enable();


            /** @var CommandFactory $commandFactory */
            $commandFactory = DI::singleton(CommandFactory::class);

            $addButton->on('click')
                ->command($commandFactory->create(ModalShowCommand::class, ['target' => $modal]));
            //->addParameters(['form' => 'form_create_' . $this->getName()])
            //->target('modal_create_' . $this->getName());

            $addButton->on('click')
                ->command($commandFactory->create(
                    ChangeAttributeCommand::class,
                    [
                        'target'    => $addButton,
                        'attribute' => 'table',
                        'value'     => JsUtils::Js('this.command.execute(this, "saveContents");')
                    ]
                ));
        });
    }
}
