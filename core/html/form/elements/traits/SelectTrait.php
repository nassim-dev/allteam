<?php

namespace core\html\form\elements\traits;

use core\controller\ControllerServiceInterface;
use core\DI\DI;
use core\html\bar\Bar;
use core\html\bar\BarFactory;
use core\html\CommandFactory;
use core\html\form\command\DeleteCommand;
use core\html\form\command\LockCommand;
use core\html\form\command\PopulateCommand;
use core\html\form\command\UnlockCommand;
use core\html\form\elements\interfaces\SelectInterface;
use core\html\form\Form;
use core\html\form\FormFactory;
use core\html\javascript\command\EvalCommand;
use core\html\modal\Modal;
use core\html\modal\ModalFactory;
use core\html\traits\AjaxSourced;
use core\html\traits\Identifiable;
use core\routing\Link;
use core\secure\authentification\AuthServiceInterface;
use core\secure\permissions\PermissionServiceInterface;
use core\utils\Utils;
use core\view\View;

/**
 * Trait SelectTrait
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait SelectTrait
{
    use AjaxSourced;
    use Identifiable;

    /**
     * Show CRUD buttons
     * If === string then $this->_btn = api ressource
     *
     * @var string|null
     */
    protected $_btn = null;

    /**
     * Le choix par défaut de l'élément de formulaire
     *
     * @var array
     */
    protected $_defaultMultiChoice = [];

    /**
     * Liste d'options pour les select
     *
     * @var array|null
     */
    protected $_options = null;

    /**
     * Html Template for select
     *
     * @var string|null
     */
    protected $_selectTemplate = null;

    /**
     * Option for select2 create tag
     *
     * @var array|null
     */
    protected $createInline = null;

    /**
     * Si on utilise des catégories
     *
     * @var array|null
     */
    protected $selectFilter = null;

    /**
     * Elements Action Bar
     *
     * @var array
     */
    protected $actionBar = [];

    /**
     * Api ressource
     *
     * @var string|null
     */
    protected $ressource;

    /**
     * Generate action bar for each option
     *
     * @return null
     */
    private function _generateActionsBar(string $ressource, BarFactory $barFactory, Form $form)
    {
        if (null === $this->getOptions()) {
            return;
        }

        foreach ($this->getOptions() as $value) {
            $value     = (is_array($value)) ? $value['value'] : $value;
            $actionBar = $barFactory->create('slct_' . $ressource . '_' . $value);

            //Update button
            $button = $actionBar->addButton(_('Edit'));
            $button->addClass("btn_update_$ressource")
                ->setIcon('fa-edit')
                ->addDataAttributes(
                    [
                        'apiMethod'    => 'GET',
                        "id$ressource" => $value,
                        'apiRessource' => $ressource,
                        'target'       => $form->getIdAttribute()
                    ]
                );

            /** @var CommandFactory $commandFactory */
            $commandFactory = DI::singleton(CommandFactory::class);

            $button->on('click')
                ->selector(".btn_update_$ressource")
                ->command($commandFactory->create(PopulateCommand::class, ['target' => $form, 'indexKey' => "id$ressource", 'uuid' => (int) $value]));

            $button->enable();
            $button->setHtmlTemplate('buttonIcon.html');

            //Delete button
            $button = $actionBar->addButton(_('Delete'));
            $button->addClass("btn_delete_$ressource")
                ->setIcon('fa-trash')
                ->addDataAttributes(
                    [
                        'apiMethod'    => 'DELETE',
                        "id$ressource" => $value,
                        'apiRessource' => $ressource,
                    ]
                );

            $button->on('click')
                ->selector(".btn_delete_$ressource")
                ->command($commandFactory->create(DeleteCommand::class, ['target' => $ressource, 'indexKey' => "id$ressource", 'uuid' => (int) $value]));

            $button->enable();
            $button->setHtmlTemplate('buttonIcon.html');

            $this->addActionBar($value, $actionBar);
        }
    }

    /**
     * Set the value of _actionBar
     *
     * @return static
     */
    public function addActionBar(string $key, Bar $actionBar): static
    {
        $this->actionBar[$key] = $actionBar;

        return $this;
    }


    /**
     * Create UpdateWidget (Form && Modal)
     *
     * @param PermissionServiceInterface $acl
     * @param AuthServiceInterface       $auth
     *
     * @return void
     */
    private function _createUpdateWidget(
        string $ressource,
        FormFactory $formFactory,
        ModalFactory $modalFactory,
        BarFactory $barFactory,
        CommandFactory $commandFactory,
        ?string $idfield
    ) {
        $modalFactory->createThen('update_' . strtolower($ressource), function (Modal $modal) use ($formFactory, $commandFactory, $ressource, $barFactory, $idfield): void {
            $this->_idfield = $idfield;

            $this->setRessource($ressource);
            $this->_btn = $ressource;
            $form       = $formFactory->createFromWidget($ressource, ['method' => 'edit']);
            $modal->addForm($form);

            $this->_generateActionsBar($ressource, $barFactory, $form);

            $modal->on('shown.bs.modal')
                ->addParameters(['ressource' => $ressource])
                ->command($commandFactory->create(LockCommand::class, ['endpoint' => $form->getRoute()?->getEndpoint() ?? '']));

            $modal->on('hidden.bs.modal')
                ->addParameters(['ressource' => $ressource])
                ->command($commandFactory->create(UnlockCommand::class, ['endpoint' => $form->getRoute()?->getEndpoint() ?? '']));

            View::assignWidget($modal);
        });
    }

    /**
     * Create CreateWidget (Form && Modal)
     *
     * @param PermissionServiceInterface $acl
     * @param AuthServiceInterface       $auth
     *
     * @return void
     */
    private function _createCreateWidget(
        string $ressource,
        array $options,
        FormFactory $formFactory,
        ModalFactory $modalFactory,
        CommandFactory $commandFactory,
        ?string $idfield
    ) {
        $modalFactory->createThen('create_' . $ressource, function (Modal $modal) use ($formFactory, $commandFactory, $ressource, $options, $idfield): void {
            $this->_idfield = $idfield;
            $this->setRessource($ressource);
            $this->_btn = $ressource;

            $form = $formFactory->createFromWidget($ressource, ['method' => 'add', 'options' => $options]);
            $modal->addForm($form);

            View::assignWidget($modal);
        });
    }

    /**
     * Add buttons to open modals
     *
     * @return static
     */
    public function btn(string $ressource, array $options = [], ?string $idfield = null): static
    {
        $ressource = strtolower($ressource);

        //$this->getDi()->callInside($this, '_createUpdateWidget', ['ressource' => $ressource, 'idfield' => $idfield]);
        //$this->getDi()->callInside($this, '_createCreateWidget', ['ressource' => $ressource, 'idfield' => $idfield, 'options' => $options]);

        return $this;
    }

    /**
     * Add Select2Cascade
     *
     * @return static
     */
    public function cascadeOn(
        SelectInterface $selectChild,
        array $params = [
            'action'    => 'formList',
            'ressource' => null,
            'trigger'   => true
        ]
    ): static {
        $selectChild->setRoute(null);
        $params['trigger'] ??= true;
        $params['action'] = '/' . $params['action'];

        $this->addConfig('cascadeOn')
            ->value($selectChild->getIdAttribute())
            ->addParameters($params);

        $selectChild->addConfig('childField')
            ->value($this->getIdAttribute());

        return $this;
    }

    /**
     * Check var validity ?
     */
    public function checkVar(array $array)
    {
        $value = @$array[$this->getName()];
        if (isset($value)) {
            $this->value($value);
            $this->defaultMultiChoice($value);
        }
    }

    /**
     * Set option for select2 create tag
     *
     * @PhpUnitGen\set("createInline")
     *
     * @param array $createInline Option for select2 create tag
     *
     * @return static
     */
    public function createInline(array $createInline): static
    {
        $this->createInline = $createInline;

        return $this;
    }

    /**
     * Set value of _defaultMultiChoice
     *
     * @PhpUnitGen\set("_defaultMultiChoice")
     *
     * @return static
     */
    public function defaultMultiChoice(array|string $default): static
    {
        $this->_defaultMultiChoice = (is_array($default)) ? $default : explode(';', $default);

        return $this;
    }

    /**
     * Get value of _btn
     *
     * @PhpUnitGen\get("_btn")
     */
    public function getBtn(): ?string
    {
        return $this->_btn;
    }

    /**
     * Get option for select2 create tag
     *
     * @PhpUnitGen\get("createInline")
     */
    public function getCreateInline(): ?array
    {
        return $this->createInline;
    }

    /**
     * Get value of _defaultMultiChoice
     *
     * @PhpUnitGen\get("_defaultMultiChoice")
     */
    public function getDefaultMultiChoice(): array
    {
        return $this->_defaultMultiChoice;
    }

    /**
     * Get value of _options
     *
     * @PhpUnitGen\get("_options")
     */
    public function getOptions(): ?array
    {
        return $this->_options;
    }

    /**
     * Get si on utilise des catégories
     */
    public function getSelectFilter(): ?array
    {
        if (null === $this->selectFilter) {
            return null;
        }

        $return = [];
        foreach ($this->selectFilter as $key => $value) {
            $return[$key] = [
                'group'    => $value,
                'constant' => $value
            ];
        }

        return $return;
    }

    /**
     * Get html Template for select
     *
     * @PhpUnitGen\get("_selectTemplate")
     */
    public function getSelectTemplate(): ?string
    {
        return $this->_selectTemplate;
    }

    /**
     * Link select with selectChild
     *
     * @param SelectInbterface $selectChild
     *
     * @todo rewrite valid jav ascript code
     *
     * @return static
     */
    public function linkTo(SelectInterface $selectChild): static
    {
        $selectChild->setRoute(null);

        $idAttribute = $selectChild->getIdAttribute();

        $change = <<< JS
        let selectChild = this.app.componentd.get("{$idAttribute}");
        setTimeout(() => {

            let options =  this.wrapper.find(":selected");

            $("#{$idAttribute}").empty();
            let clonedOptionsTmp = options.clone()

            let clonedOptions = [];
            $.each( clonedOptionsTmp, function() {
                clonedOptions.push(new Option(this.text, this.value));
            });

            selectChild.mainObject.val(null);
            selectChild.mainObject.append(clonedOptions).trigger("change");
            this.wrapper.trigger('select2:childchange');

            let container = $("#{$idAttribute}_div").find(".select2-container--default");
            container.addClass("animated flash");
            setTimeout(() =>
            {
                container.removeClass("animated flash");
            }, 1000);
        }, 500);
JS;
        /** @var CommandFactory $commandFactory */
        $commandFactory = DI::singleton(CommandFactory::class);

        $command = $commandFactory->create(EvalCommand::class, ['callback' => $change]);

        $this->on('select2:select')
            ->command($command);

        $this->on('select2:unselect')
            ->command($command);

        $selectChild->addConfig('childField')
            ->value($this->getIdAttribute());

        $selectChild->addConfig('isParentField')
            ->value($selectChild->getIdAttribute());

        return $this;
    }

    /**
     * Set value of _name
     *
     * @PhpUnitGen\set("_name")
     */
    public function setName(string $name): static
    {
        if (null === $this->getOptions()) {
            $ressource = $name;
            if (str_contains($ressource, '___')) {
                $elmt      = explode('___', $ressource);
                $ressource = $elmt[1] ?? '';
            }

            /** @var ControllerServiceInterface $controllerFactory */
            $controllerFactory = DI::singleton(ControllerServiceInterface::class);
            $ressource         = (str_starts_with($ressource, 'id')) ? substr($ressource, 2) : $ressource;
            $ressource         = (str_ends_with($ressource, '_parent')) ? str_replace('_parent', '', $ressource) : $ressource;

            //$this->setRessource($controllerFactory->getClass($this->getRessource() ?? $ressource, contextPriority:'api'));
            $class = $controllerFactory->getClass($this->getRessource() ?? $ressource, contextPriority:'api');
            if ($class === null) {
                $this->getParent()->removeChild($this);

                return $this;
            }

            $link = new Link($class, 'htmlListAction');
            $this->addEndpoint('readData', $link);

            $link = new Link($class, 'htmlListByIdAction');
            $this->addEndpoint('readById', $link);

            $link = new Link($class, 'createInlineAction');
            $this->addEndpoint('createData', $link);
        }

        return parent::setName($name);
    }


    /**
     * Set value of _options
     *
     * @PhpUnitGen\set("_options")
     *
     * @return static
     */
    public function options(?array $options)
    {
        $this->_options = $options;

        return $this;
    }

    /**
     * Set html Template for select
     *
     * @PhpUnitGen\set("_selectTemplate")
     *
     * @param string|null $_selectTemplate Html Template for select
     *
     * @return static
     */
    public function selectTemplate(?string $_selectTemplate)
    {
        $this->_selectTemplate = $_selectTemplate;

        return $this;
    }

    /**
     * Set select without ajax
     *
     * @return static
     */
    public function setBasicSelect()
    {
        $this->selectTemplate(null);

        return $this;
    }

    /**
     * Set api ressource
     *
     * @return static
     */
    public function setRessource(string $ressource)
    {
        $this->ressource = $ressource;
        //$ressource       = (class_exists($ressource)) ? $ressource : '\app\api\controller\\' . ucfirst($ressource) . 'Controller';

        return $this;
    }

    /**
     * Set si on utilise des catégories
     *
     * @PhpUnitGen\set("selectFilter")
     *
     * @param array $selectFilter Si on utilise des catégories
     *
     * @return static
     */
    public function setSelectFilter(array $selectFilter)
    {
        $this->selectFilter = $selectFilter;
        Utils::array_sort_by_column($this->_options, 'group');

        return $this;
    }

    /**
     * Get api ressource
     *
     * @PhpUnitGen\get("ressource")
     */
    public function getRessource(): ?string
    {
        return $this->ressource;
    }
}
