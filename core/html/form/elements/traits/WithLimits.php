<?php

namespace core\html\form\elements\traits;

/**
 * Trait WithLimits
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait WithLimits
{
    /**
     * Maximum
     *
     * @var null|int
     */
    protected $maximum;

    /**
     * Minimum
     *
     * @var null|int
     */
    protected $minimum;

    /**
     * Get maximum
     *
     * @PhpUnitGen\get("maximum")
     */
    public function getMaximum(): ?int
    {
        return $this->maximum;
    }

    /**
     * Get minimum
     *
     * @PhpUnitGen\get("minimum")
     */
    public function getMinimum(): ?int
    {
        return $this->minimum;
    }

    /**
     * Set maximum
     *
     * @PhpUnitGen\set("maximum")
     *
     * @param null|int $maximum Maximum
     *
     * @return static
     */
    public function setMaximum(?int $maximum)
    {
        $this->maximum = $maximum;

        return $this;
    }

    /**
     * Set minimum
     *
     * @PhpUnitGen\set("minimum")
     *
     * @param null|int $minimum Minimum
     *
     * @return static
     */
    public function setMinimum(?int $minimum)
    {
        $this->minimum = $minimum;

        return $this;
    }
}
