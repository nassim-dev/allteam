<?php

namespace core\html\form\elements\traits;

/**
 * Trait WithPlaceholder
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait WithPlaceholder
{
    /**
     * Html Attribute placeholder
     */
    private $_placeholder = null;

    /**
     * Gett value of _placeholder
     *
     * @PhpUnitGen\get("_placeholder")
     */
    public function getPlaceholder(): ?string
    {
        return $this->_placeholder;
    }

    /**
     * Set value opf $_placeholder
     *
     * @PhpUnitGen\set("_placeholder")
     *
     * @return static
     */
    public function placeholder(?string $var)
    {
        $this->_placeholder = $var;

        return $this;
    }
}
