<?php

namespace core\html\form\elements\traits;

/**
 * Trait WithMask
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait WithMask
{
    /**
     * Html Attribute mask
     */
    private $_mask = null;

    /**
     * Gett value of _mask
     *
     * @PhpUnitGen\get("_mask")
     */
    public function getMask(): ?string
    {
        return $this->_mask;
    }

    /**
     * Set value opf $_mask
     *
     * @PhpUnitGen\set("_mask")
     *
     * @return static
     */
    public function mask(?string $var)
    {
        $this->_mask = $var;

        return $this;
    }
}
