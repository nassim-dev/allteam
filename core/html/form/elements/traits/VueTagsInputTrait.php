<?php

namespace core\html\form\elements\traits;

/**
 * Undocumented trait
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
trait VueTagsInputTrait
{
    private $vueProps = [
        'v-model'       => 'tag',
        ':tags'         => 'tags',
        ':validation'   => 'validation',
        '@tags-changed' => 'onChange',

    ];

    /**
     * Return vue properties
     *
     * @return array
     */
    protected function _toArrayVue(): array
    {
        return ['vue-config' => $this->vueProps];
    }

    /**
     * Set a vue propertie
     *
     * @return static
     */
    protected function setVueProp(string $key, mixed $value): static
    {
        $this->vueProps[$key] = $value;

        return $this;
    }
}
