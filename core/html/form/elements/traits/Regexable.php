<?php

namespace core\html\form\elements\traits;

use core\html\form\FormException;

/**
 * Trait Regexable
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait Regexable
{
    /**
     * La regex vérifiant la validité du contenu de l'élément de formulaire
     * @var string|null
     */
    private $_regex;

    /**
     * Set value of _regex
     *
     * @PhpUnitGen\set("_regex")
     *
     * @return static
     */
    public function regex(?string $regex)
    {
        @preg_match($regex, null);
        if (preg_last_error() !== PREG_NO_ERROR) {
            throw new FormException('Invalid regex provided for field ' . $this->getName());
        }

        $this->_regex = $regex;

        return $this;
    }
}
