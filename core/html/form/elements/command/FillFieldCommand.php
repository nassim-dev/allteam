<?php

namespace core\html\form\elements\command;

use core\html\form\elements\interfaces\FieldInterface;
use core\html\javascript\command\AbstractCommand;

/**
 * Class FillFieldCommand
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class FillFieldCommand extends AbstractCommand
{
    public function __construct(public FieldInterface $target, public array $parameters = [])
    {
    }

    public function get(): string
    {
        return 'components/form/elements/FillFieldCommand';
    }
}
