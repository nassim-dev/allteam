<?php

namespace core\html\form\elements;

use core\html\form\Form;

/**
 * Class CguElement
 *
 * CGU field
 *
 * @category CGU fields
 * @version 1.0.
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 * @since File available since Release 0.1
 */
class CguElement extends FormElement
{
    /**
     * @PhpUnitGen\assertInstanceOf("CguElement::class")
     */
    public function __construct(?Form $form = null)
    {
        $this->_type = 'cgu';

        parent::__construct($form);
    }
}
