<?php

namespace core\html\form\elements;

use app\data\Dataset;
use core\DI\DI;
use core\html\form\Form;
use core\services\storage\ObjectStorageInterface;
use core\utils\Utils;

/**
 * Class FileinputElement
 *
 * Fileinput field
 *
 * @PhpUnitGen\assertClassHasAttribute("_defaultChoice")
 * @PhpUnitGen\assertClassHasAttribute("_filetype")
 * @category Fileinput field
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class FileinputElement extends FormElement
{
    /**
     * @var mixed
     */
    private $_defaultChoice = null;

    /**
     * Apply filetype fileter to fielinput
     */
    private array|string $_filetype = [];

    /**
     * @PhpUnitGen\assertInstanceOf("FileinputElement::class")
     */
    public function __construct(?Form $form = null)
    {
        $this->_filetype = implode(',', Dataset::get('fileExtensions', Dataset::REVERSE));
        $this->_type     = 'fileinput';
        $this->setSanitizer('file');
        parent::__construct($form);
    }


    /**
     * @param $array
     */
    public function checkVar($array)
    {
        $this->setValue(null);
        $files = $array[$this->getName()] ?? null;
        if (is_array($files)) {
            $this->setValue($files);
        }
    }

    /**
     * Set value of _defaultChoice
     *
     * @PhpUnitGen\set("_defaultChoice")
     *
     * @param string|null $default
     */
    public function defaultChoice($default): self
    {
        $this->_defaultChoice = $default;

        return $this;
    }

    /**
     * Set value of _filetype
     *
     * @PhpUnitGen\set("_filetype")
     */
    public function filetype(array $var): self
    {
        $this->_filetype = implode(',', $var);

        return $this;
    }

    /**
     * Get value of _defaultChoice
     *
     * @PhpUnitGen\get("_defaultChoice")
     */
    public function getDefaultChoice(): ?string
    {
        return $this->_defaultChoice;
    }

    /**
     * Get value of _filetype
     *
     * @PhpUnitGen\get("_filetype")
     */
    public function getFiletype(): string|null|array
    {
        return $this->_filetype;
    }

    /**
     * Check if is valid
     */
    public function isValid(array $array): bool
    {
        if ($this->isRequired() && $this->getValue() === null) {
            $this->errors[] = $this->getLabel();

            return false;
        }

        return true;
    }

    /**
     * Get element value
     */
    public function getElementValue(array &$out): array
    {
        if ($this->getValue() === null) {
            return $out;
        }

        $files = [];
        /** @var ObjectStorageInterface $storage */
        $storage = DI::singleton(ObjectStorageInterface::class);
        foreach ($this->getValue() as $key) {
            $filePath = Utils::decrypt($key);
            $file     = $storage->get($filePath);
            if ($file === null) {
                continue;
            }

            if (str_contains($file->getPath(), $storage::TMP_DIR)) {
                $file = $storage->move($file, '/' . date('Y'));
            }

            $fles[] = $file->getEncyptedKey();
        }


        $out[$this->getName()] = implode(';', $files);

        return $out;
    }
}
