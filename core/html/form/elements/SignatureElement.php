<?php

namespace core\html\form\elements;

use core\html\form\Form;

/**
 * Class SignatureElement
 *
 * Signature field
 *
 * @category Signature field
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class SignatureElement extends FormElement
{
    /**
     * @PhpUnitGen\assertInstanceOf("SignatureElement::class")
     */
    public function __construct(?Form $form = null)
    {
        //Assets::addSignature();
        $this->_type = 'signature';

        parent::__construct($form);
    }
}
