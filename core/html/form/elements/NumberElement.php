<?php

namespace core\html\form\elements;

use core\html\form\elements\traits\WithLimits;
use core\html\form\elements\traits\WithMask;
use core\html\form\Form;

/**
 * Class NumberElement
 *
 * Number field
 *
 * @category Number field
 * @version 0.1
 * @PhpUnitGen\assertClassHasAttribute("'_max'")
 * @PhpUnitGen\assertClassHasAttribute("'_min'")
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 * @since File available since Release 0.1
 */
class NumberElement extends FormElement
{
    use WithLimits;
    use WithMask;

    /**
     * @PhpUnitGen\assertInstanceOf("NumberElement::class")
     */
    public function __construct(?Form $form = null)
    {
        $this->_type = 'number';
        $this->setMaximum(365);
        $this->setMinimum(-365);
        $this->setSanitizer('int');

        parent::__construct($form);
    }
}
