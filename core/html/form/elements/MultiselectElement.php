<?php

namespace core\html\form\elements;

use core\html\form\elements\interfaces\SelectInterface;
use core\html\form\elements\traits\SelectTrait;
use core\html\form\Form;

/**
 * Class MultiselectElement
 *
 * Select multiple field
 *
 * @category  Select multiple field
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class MultiselectElement extends FormElement implements SelectInterface
{
    use SelectTrait;

    /**
     * @PhpUnitGen\assertInstanceOf("MultiselectElement::class")
     */
    public function __construct(?Form $form = null)
    {
        $this->selectTemplate('base.handlebars');
        $this->_type = 'multiselect';
        $this->setSanitizer('int[]');

        parent::__construct($form);
    }
}
