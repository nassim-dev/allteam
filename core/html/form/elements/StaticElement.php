<?php

namespace core\html\form\elements;

use core\html\form\elements\traits\WithMask;
use core\html\form\Form;

/**
 * Class StaticElement
 *
 * Static text field
 *
 * @category Static text field
 * @version 0.1
 * @PhpUnitGen\assertClassHasAttribute("'_mask'")
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 * @since File available since Release 0.1
 */
class StaticElement extends FormElement
{
    use WithMask;

    /**
     * @PhpUnitGen\assertInstanceOf("StaticElement::class")
     */
    public function __construct(?Form $form = null)
    {
        $this->_type = 'static';

        parent::__construct($form);
    }
}
