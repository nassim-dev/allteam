<?php

namespace core\html\form\elements;

use core\html\form\elements\traits\Regexable;
use core\html\form\Form;

/**
 * Class MultipleemailElement
 *
 * Tag field for email
 *
 * @category Tag field for email
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class MultipleemailElement extends FormElement
{
    use Regexable;

    /**
     * @PhpUnitGen\assertInstanceOf("MultipleemailElement::class")
     */
    public function __construct(?Form $form = null)
    {
        $this->_type = 'multipleemail';

        parent::__construct($form);
    }

    /**
     * Check if valid
     */
    public function isValid(array $array): bool
    {
        if ($this->isRequired()) {
            $this->regex('/^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$/');
        }

        if (!empty($this->_regex)) {
            $emails = str_replace(' ', '', $array[$this->getName()]);
            $emails = str_replace('<', '', $emails);
            $emails = str_replace('>', '', $emails);
            $emails = explode(';', $emails);

            foreach ($emails as $email) {
                if (!preg_match($this->_regex, $email)) {
                    $this->_errors[] = $this->getLabel();

                    return false;
                }
            }
        }

        return parent::isValid($array);
    }
}
