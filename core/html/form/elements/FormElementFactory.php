<?php

namespace core\html\form\elements;

use core\factory\FactoryInterface;
use core\html\form\elements\interfaces\FieldInterface;
use core\html\HtmlElementFactory;
use core\utils\Utils;

/**
 * Class FormElementFactory
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class FormElementFactory extends HtmlElementFactory implements FactoryInterface
{
    /**
     * Return element class to build
     */
    protected function getElementClass(): string
    {
        return FieldInterface::class;
    }

    /**
     * Return suffix for widget classes
     */
    protected function getWidgetSuffix(): string
    {
        return 'Fieldidget';
    }

    /**
     * Create new instance or return singleton if exist
     *
     * @PhpUnitGen\assertInstanceOf("FieldInterface::class")
     */
    public function create(string $class, array $params = []): ?FieldInterface
    {
        $method = $params[0] ?? $params['method'] ?? null;
        $method = (!is_array($method)) ? $method : null;

        $identifier = $class . '_' . $method . Utils::hash($params);

        $instance = $this->getInstance($identifier);
        if (null === $instance) {
            $instance = $this->getDi()->make($class, $params);
        }

        return $this->register($instance, $identifier);
    }

    /**
     * @return FieldInterface
     */
    public function get(string $identifier): ?FieldInterface
    {
        return $this->create($identifier);
    }
}
