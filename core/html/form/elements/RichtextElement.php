<?php

namespace core\html\form\elements;

use core\html\form\Form;

/**
 * Class RichtextElement
 *
 * Textarea field
 *
 * @category Textarea field
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class RichtextElement extends FormElement
{
    /**
     * @PhpUnitGen\assertInstanceOf("RichtextElement::class")
     */
    public function __construct(?Form $form = null)
    {
        //Assets::addTextarea();
        $this->_type = 'richtext';

        parent::__construct($form);
    }

    public function setLabel(?string $label)
    {
        $this->label = null;

        return $this;
    }
}
