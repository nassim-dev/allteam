<?php

namespace core\html\form\elements;

use core\html\form\Form;

/**
 * Class ButtonElement
 *
 * Button Field
 *
 * @category Button Field
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class ButtonElement extends FormElement
{
    /**
     * @PhpUnitGen\assertInstanceOf("ButtonElement::class")
     */
    public function __construct(?Form $form = null)
    {
        $this->_type = 'button';
        $this->addClass(' btn-light-primary');

        parent::__construct($form);
    }
}
