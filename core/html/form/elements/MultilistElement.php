<?php

namespace core\html\form\elements;

use core\html\form\elements\interfaces\SelectInterface;
use core\html\form\elements\traits\SelectTrait;
use core\html\form\Form;
use core\routing\Link;

/**
 * Class MultilistElement
 *
 * Multilist field
 *
 * @category Multilist field
 * @version 0.1
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @deprecated
 *
 * @link https://allteam.io
 * @since File available since Release 0.1
 */
class MultilistElement extends FormElement implements SelectInterface
{
    use SelectTrait;

    /**
     * @PhpUnitGen\assertInstanceOf("MultilistElement::class")
     */
    public function __construct(?Form $form = null)
    {
        $this->selectTemplate('base.handlebars');
        $this->_type = 'multilist';
        $this->setSanitizer('string[]');
        parent::__construct($form);
    }
}
