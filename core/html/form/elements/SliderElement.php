<?php

namespace core\html\form\elements;

use core\html\form\elements\traits\WithLimits;
use core\html\form\Form;

/**
 * Class SliderElement
 *
 * Slider field
 *
 * @category Slider field
 * @version 0.1
 * @PhpUnitGen\assertClassHasAttribute("'_max'")
 * @PhpUnitGen\assertClassHasAttribute("'_min'")
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 * @since File available since Release 0.1
 */
class SliderElement extends FormElement
{
    use WithLimits;

    /**
     * @PhpUnitGen\assertInstanceOf("SliderElement::class")
     */
    public function __construct(?Form $form = null)
    {
        //Assets::addSlider();
        $this->_type = 'slider';
        $this->setMinimum(-365);
        $this->setMaximum(365);
        $this->setSanitizer('int');

        parent::__construct($form);
    }
}
