<?php

namespace core\html\form\elements;

use core\html\form\elements\traits\Regexable;
use core\html\form\elements\traits\WithMask;
use core\html\form\elements\traits\WithPlaceholder;
use core\html\form\Form;

/**
 * Class TimeElement
 *
 * Time field
 *
 * @category Time field
 * @version 0.1
 * @PhpUnitGen\assertClassHasAttribute("'_regex'")
 * @PhpUnitGen\assertClassHasAttribute("'_mask'")
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 * @since File available since Release 0.1
 */
class TimeElement extends FormElement
{
    use Regexable;
    use WithMask;
    use WithPlaceholder;

    /**
     * @PhpUnitGen\assertInstanceOf("TimeElement::class")
     */
    public function __construct(?Form $form = null)
    {
        //Assets::addDatetimePicker();
        $this->mask("'mask':'99:99'");
        $this->placeholder('HH/MM');
        $this->setAutocomplete(false);
        $this->setIcon('fa-clock');
        $this->_type = 'time';
        $this->setHtmlTemplate(self::getBaseTemplateDir() . 'form/elements/DateElement.html');

        parent::__construct($form);
    }
}
