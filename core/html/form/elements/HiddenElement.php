<?php

namespace core\html\form\elements;

use core\html\form\Form;

/**
 * Class HiddenElement
 *
 * Hidden field
 *
 * @category Hidden field
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class HiddenElement extends FormElement
{
    /**
     * @PhpUnitGen\assertInstanceOf("HiddenElement::class")
     */
    public function __construct(?Form $form = null)
    {
        $this->_type = 'hidden';
        parent::__construct($form);
    }
}
