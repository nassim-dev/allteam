<?php

namespace core\html\form\elements;

use core\html\form\elements\traits\Regexable;
use core\html\form\elements\traits\WithMask;
use core\html\form\elements\traits\WithPlaceholder;
use core\html\form\Form;
use core\utils\Utils;

/**
 * Class DatetimeElement
 *
 * Datetime field
 *
 * @category  Datetime field
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class DatetimeElement extends FormElement
{
    use Regexable;
    use WithMask;
    use WithPlaceholder;

    public function __construct(?Form $form = null)
    {
        $this->_type = 'datetime';
        $this->placeholder('JJ/MM/AAAA HH:MM');
        $this->setAutocomplete(false);
        $this->mask("'mask': '99/99/9999 99:99'");
        $this->setIcon('fa-calendar');
        $this->setHtmlTemplate(self::getBaseTemplateDir() . 'form/elements/DateElement.html');
        //Assets::addDatetimePicker();
        parent::__construct($form);
    }

    /**
     * Get one element value
     */
    public function getElementValue(array &$out): array
    {
        $out[$this->getName()] = Utils::createDateTime($this->getValue());

        return $out;
    }
}
