<?php

namespace core\html\form\elements;

use core\html\form\elements\traits\Regexable;
use core\html\form\elements\traits\WithMask;
use core\html\form\Form;

/**
 * Class EmailElement
 *
 * Email field
 *
 * @category Email field
 * @version 0.1
 * @PhpUnitGen\assertClassHasAttribute("_mask")
 * @PhpUnitGen\assertClassHasAttribute("_regex")
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 * @since File available since Release 0.1
 */
class EmailElement extends FormElement
{
    use WithMask;
    use Regexable;

    /**
     * @PhpUnitGen\assertInstanceOf("EmailElement::class")
     */
    public function __construct(?Form $form = null)
    {
        $this->_type = 'email';
        $this->setIcon('fa-at');
        $this->mask("'alias' : 'email'");
        $this->setSanitizer('email');
        parent::__construct($form);
    }

    /**
     * Check validity
     */
    public function isValid(array $array): bool
    {
        if ($this->isRequired()) {
            $this->regex('/^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$/');
        }

        if (!empty($this->_regex)) {
            $emails = str_replace(' ', '', $array[$this->getName()]);
            $emails = str_replace('<', '', $emails);
            $emails = str_replace('>', '', $emails);
            $emails = explode(';', $emails);

            foreach ($emails as $email) {
                if (!preg_match($this->_regex, $email)) {
                    $this->_errors[] = $this->getLabel();

                    return false;
                }
            }
        }

        return parent::isValid($array);
    }
}
