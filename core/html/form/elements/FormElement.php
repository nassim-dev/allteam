<?php

namespace core\html\form\elements;

use core\DI\DI;
use core\html\AbstractHtmlElement;
use core\html\CommandFactory;
use core\html\form\elements\command\FillFieldCommand;
use core\html\form\elements\interfaces\FieldInterface;
use core\html\form\Form;
use core\html\javascript\JavascriptObjectInterface;
use core\html\javascript\WithJavascript;
use core\html\traits\AjaxSourced;
use core\html\traits\Autocomplete;
use core\html\traits\DataAttribute;
use core\html\traits\Deactivable;
use core\html\traits\Focusable;
use core\html\traits\Iconable;
use core\html\traits\Identifiable;
use core\html\traits\Labelable;
use core\html\traits\Namable;
use core\html\traits\Stylizable;
use core\html\traits\Valuable;
use core\html\traits\WithHelper;

/**
 * Class FormElement
 *
 * Main Form Element class
 *
 * @category  Main Form Element class
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class FormElement extends AbstractHtmlElement implements JavascriptObjectInterface, FieldInterface
{
    use WithJavascript;
    use Identifiable;
    use Deactivable;
    use Iconable;
    use Labelable;
    use Namable;
    use Stylizable;
    use DataAttribute;
    use Focusable;
    use Autocomplete;
    use Valuable;
    use WithHelper;
    use AjaxSourced;

    public const REQUIRED = true;

    public const UNREQUIRED = false;

    /**
     * Triggers parameters to execute on load
     *
     * @var array
     */
    public $triggerParms = [];

    /**
     * Les erreurs de non-validité rencontrées lors de la soumisson du formulaire
     *
     * @var array
     */
    protected $errors = [];

    /**
     * Indique si l'élément de formulaire est indispensable
     *
     * @var bool
     */
    protected $required = false;

    /**
     * True if change must be observed and saved
     *
     * @var boolean
     */
    protected $saveChange = true;

    /**
     * When Activate switch button, define the group id
     *
     * @var int|null
     */
    protected $switchGroup = null;

    /**
     * Le type d'élément
     *
     * @var string
     */
    protected $type;

    /**
     * If field is external, then represents the Entitie class name
     *
     * @var string|null
     */
    protected ?string $entitieClass = null;

    /**
     * Default sanitizer
     *
     * @var string
     */
    protected string $sanitizer = 'string';


    /**
     * FormElement constructor.
     *
     * @PhpUnitGen\assertInstanceOf("FormElement::class")
     */
    public function __construct(?Form $form = null)
    {
        $this->setId(uniqid());
        if (is_object($form)) {
            $this->appendTo($form, $this->getName());
        }
    }

    /**
     * Reset element on __clone()
     *
     * @return void
     */
    public function __clone()
    {
        $this->reset();
        parent::__clone();
    }


    public function getReference(): ?string
    {
        return $this->getName();
    }

    public function setSanitizer(string $sanitizer): static
    {
        $this->sanitizer = $sanitizer;

        return $this;
    }

    /**
     * Define element as external
     *
     * @return void
     */
    public function setExternal(?string $entitieClass)
    {
        $this->entitieClass = $entitieClass;
    }

    /**
     * Check if field is external
     *
     * @return boolean
     */
    public function isExternal(): bool
    {
        return $this->entitieClass != null;
    }

    /**
     *  Add trigger in element
     *
     * $parameters['trigger']   Doing the call at init ?
     * $parameters['action'] Api action
     * $parameters['ressource'] Api controller
     * $parameters['tree'] Methods to execute on related ressource
     * $parameters['fields'] = [[id => idfield, form => formid , type => fieldType]] Fields to populate
     *
     * @param array $parameters
     */
    public function addOnChangeApiCall(array $parameters = []): self
    {
        $parameters['trigger'] ??= true;
        $parameters['action'] ??= 'formList';
        $parameters['ressource'] ??= ((str_starts_with($$this->getName(), 'id')) ? substr($$this->getName(), 2) : $$this->getName());
        $parameters['tree'] ??= [];
        $parameters['fields'] ??= [
            ['id'      => $this->getId(),
                'form' => $this->getIdform(),
                'type' => $this->getType()
            ]];

        //Save parameters for __clone() method
        $this->triggerParms[] = $parameters;

        $this->on('change')
            ->target($parameters['ressource'])
            ->trigger($parameters['trigger'])
            ->addParameters(
                [
                    'ressource' => $parameters['ressource'],
                    'formId'    => $this->getIdform(),
                    'name'      => $this->getName(),
                    'key'       => '$("#' . $this->getId() . '").val()',
                    'action'    => "'/" . $parameters['action'] . "'"
                ]
            );

        return $this;
    }

    /**
     * Check var
     *
     * @return void
     */
    public function checkVar(array $array)
    {
        $value = $array[$this->getName()];
        if (isset($value)) {
            $this->setValue($value);
        }
    }

    /**
     * Static constructor
     *
     * @deprecated use FormElementFactory
     *
     * @return static
     */
    public static function create(string $type, ?Form $form = null)
    {
        $class   = __NAMESPACE__ . ucfirst($type . 'Element');
        $factory = DI::singleton(FormElementFactory::class);

        return $factory->create($class, ['form' => $form]);
    }

    /**
     * Get the value of _actionBar
     *
     * @PhpUnitGen\get("_actionBar")
     *
     * @return Button|null
     */
    public function getActionBar($key)
    {
        return $this->actionBar[$key] ?? null;
    }

    /**
     * Get element value
     *
     * @return array
     */
    public function getElementValue(array &$out): array
    {
        $out[$this->getName()] = ($this->getValue() === 'UNDEFINED_VALUE' || $this->getValue() == '') ? null : $this->getValue();
        if (is_array($this->getValue()) && in_array('UNDEFINED_VALUE', $this->getValue())) {
            $flip = array_flip($this->getValue());
            unset($flip['UNDEFINED_VALUE']);
            $out[$this->getName()] = array_flip($flip);
        }

        return $out;
    }

    /**
     * Get erros if exists
     *
     * @return boolean|string
     */
    public function getError(): bool|string
    {
        if (empty($this->errors)) {
            return false;
        }

        $out = [];
        foreach ($this->errors as $field) {
            $out[] = ' <li><b>' . $field . '</b></li>';
        }

        return implode(', ', $out);
    }

    /**
     * Return Id attribute for html element
     */
    public function getIdAttribute(): string
    {
        return ($this->hasParent()) ? $this->getParent()->getIdAttribute() . '_' . $this->getName() : $this->getViewReference() . '_' . $this->getId();
    }

    /**
     * Get value of _idform
     *
     * @PhpUnitGen\get("_idform")
     */
    public function getIdform(): ?string
    {
        return ($this->hasParent()) ? $this->getParent()->getIdAttribute() : null;
    }

    /**
     * Get ressource name of form
     */
    public function getRessourceName(): string
    {
        $elements = explode('_', $this->getId());

        return end($elements);
    }

    /**
     * Get the value of _switchGroup
     *
     * @PhpUnitGen\get("_switchGroup")
     */
    public function getSwitchGroup(): ?int
    {
        return $this->switchGroup;
    }

    public function getSwitchGroupClass(): ?string
    {
        return (null !== $this->getSwitchGroup()) ? 'groupToHide group' . $this->getSwitchGroup() : null;
    }

    /**
     * Get value of _type
     *
     * @PhpUnitGen\get("_type")
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * Gtet value of _required
     *
     * @PhpUnitGen\get("_required")
     */
    public function isRequired(): bool
    {
        return $this->required;
    }

    /**
     * Check validity
     *
     * @param array $array L'array dans lequel les valeurs doivent tre recherches et valides
     */
    public function isValid(array $array): bool
    {
        if ($this->required && (!isset($array[$this->getName()]) || empty($array[$this->getName()]) || 'UNDEFINED_VALUE' === $array[$this->getName()])) {
            $this->errors[] = $this->getLabel();

            return false;
        }

        return true;
    }

    /**
     * Set value of _name
     *
     * @PhpUnitGen\set("_name")
     */
    public function setName(string $name): self
    {
        $parent = $this->getParent();
        if ($parent !== null) {
            $this->removeFrom($parent);
        }

        $this->setId($name . '_' . $parent?->getIdAttribute());
        $this->name = $name;

        if ($parent !== null) {
            $parent->appendChild($this, $name);
        }

        return $this;
    }

    /**
     * Add event onChange to trig autosave popup
     */
    public function isAutoSaved(): bool
    {
        return $this->saveChange;
    }

    /**
     * Set value of _required
     *
     * @PhpUnitGen\set("_required")
     */
    public function required(bool $required): self
    {
        $this->required = $required;

        return $this;
    }

    /**
     * Set true if change must be observed and saved
     *
     * @PhpUnitGen\set("saveChange")
     *
     * @param bool $saveChange True if change must be observed and saved
     */
    public function saveChange(bool $saveChange): self
    {
        $this->saveChange = $saveChange;
        $this->addDataAttribute('save', ($saveChange) ? 'on' : 'false');

        return $this;
    }



    /**
     * Set the value of _switchGroup
     *
     * @PhpUnitGen\set("_switchGroup")
     */
    public function switchGroup(?int $switchGroup): self
    {
        $this->switchGroup = $switchGroup;

        return $this;
    }

    /**
     * Return an Array for object jsConfig
     */
    public function toArray(): array
    {
        $configArray         = parent::toArray();
        $configArray['name'] = $this->getName();

        return $configArray;
    }

    /**
     * Add trigger in element
     *
     * @deprecated use on()
     *
     * @param array $parms
     */
    public function trigger($parms = ['action' => 'formList', 'ressource' => null, 'trigger' => true]): self
    {
        $parms['trigger'] ??= true;
        $this->triggerParms[] = $parms;

        /** @var CommandFactory $commandFactory */
        $commandFactory = DI::singleton(CommandFactory::class);

        $this->on('change')
            ->command($commandFactory->create(FillFieldCommand::class, ['target' => $this]))
            ->target($parms['ressource'])
            ->trigger($parms['trigger'])
            ->addParameters(
                [
                    'ressource' => $parms['ressource'],
                    'formId'    => $this->getIdform(),
                    'name'      => $this->getName(),
                    'key'       => '$("#' . $this->getIdAttribute() . '").val()',
                    'action'    => "'/" . $parms['action'] . "'"
                ]
            );

        return $this;
    }

    /**
     * List of allowed childs elements
     * could be a class, an abstract class or an interface
     */
    protected static function getAllowedChilds(): array
    {
        return [];
    }

    /**
     * Return Html attribute for customElement
     */
    public static function getDefaultHtmlTag(): string
    {
        $tag = 'i-' . strtolower(str_replace('Element', '', static::class));
        $tag = str_replace(__NAMESPACE__, '', $tag);

        return str_replace('\\', '', $tag);
    }

    /**
     * Get default sanitizer
     *
     * @PhpUnitGen\get("sanitizer")
     *
     * @return string
     */
    public function getSanitizer(): string
    {
        return $this->sanitizer;
    }
}
