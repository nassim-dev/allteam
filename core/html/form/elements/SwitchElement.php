<?php

namespace core\html\form\elements;

use core\html\form\Form;

/**
 * Class SwitchElement
 *
 * Switch field
 *
 * @category Switch field
 * @version 0.1
 * @PhpUnitGen\assertClassHasAttribute("'_defaultChoice'")
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 * @since File available since Release 0.1
 */
class SwitchElement extends FormElement
{
    /**
     * Le choix par défaut de l'élément de formulaire, si c'est une checkBox, radio, ou liste de sélection
     * @var null
     */
    private $_defaultChoice = null;

    /**
     * @PhpUnitGen\assertInstanceOf("SwitchElement::class")
     */
    public function __construct(?Form $form = null)
    {
        $this->_type = 'switch';

        parent::__construct($form);
    }

    /**
     * @PhpUnitGen\set("_defaultChoice")
     *
     * @param $default
     */
    public function defaultChoice($default): self
    {
        $this->_defaultChoice = $default;

        return $this;
    }

    /**
     * @PhpUnitGen\get("_defaultChoice")
     *
     * @return null|int
     */
    public function getDefaultChoice()
    {
        return $this->_defaultChoice;
    }

    /**
     * @PhpUnitGen\get("_value")
     *
     * @return null
     */
    public function getValue(): ?string
    {
        return $this->_defaultChoice;
    }

    /**
     * Check validity
     *
     * @PhpUnitGen\assertTrue()
     */
    public function isValid(array $array): bool
    {
        return true;
    }
}
