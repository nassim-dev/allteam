<?php

namespace core\html\form\elements;

use core\html\form\elements\traits\Regexable;
use core\html\form\elements\traits\WithMask;
use core\html\form\elements\traits\WithPlaceholder;
use core\html\form\Form;
use core\utils\Utils;

/**
 * Class DateElement
 *
 * Date field
 *
 * @category Date field
 * @version 0.1
 * @PhpUnitGen\assertClassHasAttribute("_mask")
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 * @since File available since Release 0.1
 */
class DateElement extends FormElement
{
    use WithMask;
    use Regexable;
    use WithPlaceholder;

    /**
     * @PhpUnitGen\assertInstanceOf("DateElement::class")
     */
    public function __construct(?Form $form = null)
    {
        $this->_type = 'date';
        $this->mask("'mask': '99/99/9999'");
        $this->placeholder('JJ/MM/AAAA');
        $this->setAutocomplete(false);
        $this->setIcon('fa-calendar');
        //Assets::addDatetimePicker();

        parent::__construct($form);
    }

    /**
     * Get one element value
     */
    public function getElementValue(array &$out): array
    {
        $out[$this->getName()] = Utils::createDateTime($this->getValue());

        return $out;
    }
}
