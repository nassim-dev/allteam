<?php

namespace core\html\form\elements;

use core\html\form\Form;

/**
 * Class TextareaElement
 *
 * Textarea field
 *
 * @category Textarea field
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class TextareaElement extends FormElement
{
    /**
     * @PhpUnitGen\assertInstanceOf("TextareaElement::class")
     */
    public function __construct(?Form $form = null)
    {
        //Assets::addTextarea();
        $this->_type = 'textarea';

        parent::__construct($form);
    }
}
