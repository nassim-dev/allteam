<?php

namespace core\html\form\elements;

use core\html\form\Form;

/**
 * Class AddressElement
 *
 * Address field
 *
 * @category Address field
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class AddressElement extends FormElement
{
    /**
     * @PhpUnitGen\assertInstanceOf("AddressElement::class")
     */
    public function __construct(?Form $form = null)
    {
        $this->_type = 'address';
        $this->setIcon('fa-search-location');

        parent::__construct($form);
    }
}
