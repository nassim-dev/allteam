<?php

namespace core\html\form\elements;

use core\html\form\elements\traits\Regexable;
use core\html\form\elements\traits\WithMask;
use core\html\form\elements\traits\WithPlaceholder;
use core\html\form\Form;
use core\utils\Utils;

/**
 * Class TimerangeElement
 *
 * Timerange field
 *
 * @category Timerange field
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class TimerangeElement extends FormElement
{
    use WithPlaceholder;
    use WithMask;
    use Regexable;

    /**
     * @PhpUnitGen\assertInstanceOf("TimerangeElement::class")
     */
    public function __construct(?Form $form = null)
    {
        //Assets::addDatetimePicker();
        $this->_type = 'timerange';
        $this->mask("'mask':'99:99'");
        $this->placeholder('HH:MM');
        $this->setAutocomplete(false);
        $this->setIcon('fa-clock');
        $this->setHtmlTemplate(self::getBaseTemplateDir() . 'form/elements/DateElement.html');

        parent::__construct($form);
    }

    /**
     * Get one element value
     */
    public function getElementValue(array &$out): array
    {
        if (isset($out[$this->getName()]) && !isset($out[$this->getName() . '_start']) && !isset($out[$this->getName() . '_end'])) {
            $elements = explode(' to ', $out[$this->getName()]);

            $out[$this->getName() . '_start'] = Utils::createDateTime($elements[0]);
            $out[$this->getName() . '_end']   = Utils::createDateTime($elements[1]);
        }

        return $out;
    }

    /**
     * Chgeck validity
     */
    public function isValid(array $array): bool
    {
        $array = $this->getElementValue($array);
        if (!$this->isRequired() || (isset($array[$this->getName() . '_start'], $array[$this->getName() . '_end']))) {
            return true;
        }

        $this->_errors[] = $this->getLabel();

        return false;
    }
}
