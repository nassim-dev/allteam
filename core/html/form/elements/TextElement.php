<?php

namespace core\html\form\elements;

use core\html\form\elements\traits\Regexable;
use core\html\form\elements\traits\WithLimits;
use core\html\form\elements\traits\WithMask;
use core\html\form\Form;

/**
 * Class TextElement
 *
 * Text field
 *
 * @category Text field
 * @version 0.1
 * @PhpUnitGen\assertClassHasAttribute("'_mask'")
 * @PhpUnitGen\assertClassHasAttribute("'_maxlength'")
 * @PhpUnitGen\assertClassHasAttribute("'_minlength'")
 * @PhpUnitGen\assertClassHasAttribute("'_regex'")
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 * @since File available since Release 0.1
 */
class TextElement extends FormElement
{
    use WithMask;
    use Regexable;
    use WithLimits;

    /**
     * @PhpUnitGen\assertInstanceOf("TextElement::class")
     */
    public function __construct(?Form $form = null)
    {
        $this->_type = 'text';

        parent::__construct($form);
    }

    /**
     * Check validity
     */
    public function isValid(array $array): bool
    {
        if (!parent::isValid($array)) {
            return false;
        }

        if ($this->isRequired() && (
            (!null === $this->getMinimum() && strlen($array[$this->getName()]) < $this->getMinimum()) ||
            (!null === $this->getMaximum() && strlen($array[$this->getName()]) > $this->getMaximum()) ||
            (!empty($this->_regex) && !preg_match($this->_regex, $array[$this->getName()]))
        )) {
            $this->_errors[] = $this->getLabel();

            return false;
        }

        return true;
    }
}
