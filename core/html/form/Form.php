<?php

namespace core\html\form;

use core\html\AbstractHtmlElement;
use core\html\calendar\Calendar;
use core\html\CommandFactory;
use core\html\container\Collapse;
use core\html\container\ContenableInterface;
use core\html\form\elements\CheckboxElement;
use core\html\form\elements\command\FillFieldCommand;
use core\html\form\elements\FormElement;
use core\html\form\elements\interfaces\FieldInterface;
use core\html\form\elements\interfaces\SelectInterface;
use core\html\javascript\command\ChangeAttributeCommand;
use core\html\javascript\JavascriptObjectInterface;
use core\html\javascript\utils\JsUtils;
use core\html\javascript\WithJavascript;
use core\html\modal\command\ModalShowCommand;
use core\html\modal\ModalFactory;
use core\html\tables\datatable\Datatable;
use core\html\traits\AjaxSourced;
use core\html\traits\Collapsable;
use core\html\traits\Deactivable;
use core\html\traits\Iconable;
use core\html\traits\Identifiable;
use core\html\traits\Namable;
use core\html\traits\Stylizable;
use core\html\traits\Titleable;
use core\html\traits\Valuable;
use core\html\traits\WithActions;
use core\secure\csrf\CsrfServiceInterface;
use core\secure\sanitizers\SanitizerServiceInterface;

/**
 * Class Form
 *
 * Represent html form
 *
 * @PhpUnitGen\construct(["TestForm","POST", "#"])
 *
 * @category  Represent html form
 * @version   0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 hCOnttps://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class Form extends AbstractHtmlElement implements JavascriptObjectInterface
{
    use WithJavascript;
    use AjaxSourced;
    use Titleable;
    use Namable;
    use Valuable;
    use Deactivable;
    use Stylizable;
    use Identifiable;
    use WithActions;
    use Collapsable;
    use Iconable;

    /**
     * True if validate
     *
     * @var boolean
     */
    public static $validate = false;

    /**
     * Static alerte to show on top of the form
     */
    private ?string $alerte = null;

    /**
     * Method to use with api
     */
    private string $apiMethod = 'GET';

    /**
     * Custom Api ressouce
     */
    private ?string $apiRessource = null;

    /**
     * Add bottom message to form
     */
    private ?string $bottomMessage = null;

    /**
     * Registred externals fiedls
     *
     * @var array
     */
    private array $externalsFields = [];

    /**
     * Attached Calendar
     */
    private array $calendars = [];

    /**
     * Attached Datatable
     */
    private array $datatables = [];

    /**
     * Keep Modal Open
     */
    private bool|FieldInterface $keepOpen = false;

    /**
     * Set Switch form
     */
    private ?array $switchGroup = null;

    /**
     * Is captcha enabled;
     */
    private bool $captcha = false;

    /**
     * Internal form error
     */
    private ?string $error = null;

    /**
     * Form Values
     *
     * @var array|null
     */
    private ?array $values = null;

    /**
     * Clone form
     *
     * @PhpUnitGen\assertNotNull()
     *
     * @return static
     */
    public function __clone()
    {
        parent::__clone();
        $this->keepOpen   = (is_object($this->keepOpen)) ? clone $this->keepOpen : false;
        $this->calendars  = [];
        $this->datatables = [];
    }

    public function __construct(
        string $name,
        private string $method = 'POST',
        private string $action = '#'
    ) {
        $this->setName($name);
        $this->apiMethod($method);
    }

    /**
     * Add new Element
     *
     * @return FieldInterface
     */
    public function add(string $element): FieldInterface
    {
        $formElement = $this->getDi()->make($element, ['form' => $this]);

        return $this->appendChild($formElement);
    }

    /**
     * Use form in place of foreignKey field
     *
     * @param Form       $form        form to use (only childs are used)
     * @param string     $entityClass entitiy name
     * @param array|null $filter      array of reched field
     * @param string     $callback    see Helper::class
     *
     * @return FieldInterface
     */
    public function addFormAsField(Form $form, string $entityClass, ?array $filter = null, string $callback = 'OneToMany'): FieldInterface
    {
        $container = new Collapse();
        $this->appendChild($container);

        /** @var CommandFactory $commandFactory */
        $commandFactory = $this->getDi()->singleton(CommandFactory::class);
        $foreignKey     = call_user_func_array([Helper::class, $callback], [$entityClass, $this]);
        $indexKey       = 'id' . $entityClass;

        /** @var FieldInterface $formElement */
        foreach ($form->getChilds() as $formElement) {
            if ((is_array($filter) && !in_array($formElement->getName(), $filter) && $formElement->getName() != $indexKey) || $formElement->getName() === 'keepOpen') {
                continue;
            }

            $field = $this->addExternalField($formElement, $formElement->getName(), $entityClass, $container);
            if ($formElement->getName() === $indexKey) {
                $foreignKey->on('change')->command($commandFactory->create(
                    ChangeAttributeCommand::class,
                    [
                        'target'    => $field,
                        'attribute' => 'value',
                        'value'     => JsUtils::Js('this.getValue();')
                    ]
                ));
            }
        }

        return $foreignKey;
    }

    /**
     * Add new external Element
     *
     * @return FieldInterface
     */
    public function addExternalField(FieldInterface|string $element, string $columnName, string $entityClass, ?ContenableInterface $container = null): FieldInterface
    {
        $elements = explode('\\', $entityClass);
        $entity   = strtolower(end($elements));

        $formElement = (is_object($element)) ? clone $element : $this->getDi()->make($element, ['form' => $this]);
        $formElement->setParent($this);
        $formElement->setName($entity . '___' . $columnName);
        $formElement->setExternal($entityClass);

        return ($container === null) ? $this->appendChild($formElement) : $container->appendChild($formElement);
    }

    /**
     * Add Calendar to form
     *
     * @return void
     */
    public function addCalendar(Calendar $calendar): self
    {
        $this->calendars[$calendar->getIdAttribute()] = $calendar;
        $calendar->getActionBar()->removeAction(_('Detach'));
        $calendar->getActionBar()->removeAction(_('Export'));

        return $this;
    }

    /**
     * Add Datatable to form
     *
     * @return void
     */
    public function addDatatable(Datatable $datatable): self
    {
        $this->datatables[$datatable->getIdAttribute()] = $datatable;

        return $this;
    }

    /**
     * Add onChange event on all activated FormElement
     */
    public function addOnChangeEvents(): int
    {
        $observedElements = 0;
        foreach ($this->getChilds() as $element) {
            if ($element instanceof ContenableInterface) {
                continue;
            }

            if ($element->isAutoSaved()) {
                ++$observedElements;
            }
        }

        return $observedElements;
    }

    /**
     * Set the value of _alerte
     *
     * @PhpUnitGen\set("_alerte")
     * @param string $alerte
     */
    public function alerte(array $alerte): self
    {
        $this->alerte = $alerte;

        return $this;
    }

    /**
     * Set the value of _apiMethod
     *
     * @PhpUnitGen\set("_apiMethod")
     */
    public function apiMethod(string $apiMethod): self
    {
        $this->apiMethod = $apiMethod;

        return $this;
    }

    /**
     * Set custom Api ressouce
     *
     * @PhpUnitGen\set("_apiRessource")
     */
    public function apiRessource(?string $apiRessource): self
    {
        $this->apiRessource = $apiRessource;

        return $this;
    }

    /**
     * Set the value of _bottomMessage
     *
     * @PhpUnitGen\set("_bottomMessage")
     */
    public function bottomMessage(?string $bottomMessage): self
    {
        $this->bottomMessage = $bottomMessage;

        return $this;
    }


    /**
     * Get the value of _action
     *
     * @PhpUnitGen\get("_action")
     */
    public function getAction(): ?string
    {
        return $this->action;
    }

    /**
     * Get the value of _alerte
     *
     * @PhpUnitGen\get("_alerte")
     */
    public function getAlerte(): ?string
    {
        return $this->alerte;
    }

    /**
     * Get the value of _apiMethod
     *
     * @PhpUnitGen\get("_apiMethod")
     */
    public function getApiMethod(): ?string
    {
        return $this->apiMethod;
    }

    /**
     * Get the value of _bottomMessage
     *
     * @PhpUnitGen\get("_bottomMessage")
     */
    public function getBottomMessage(): ?string
    {
        return $this->bottomMessage;
    }

    /**
     * Get attached Calendar
     */
    public function getCalendar($calendarId): ?Calendar
    {
        return $this->calendars[$calendarId] ?? null;
    }

    /**
     * Get attached Datatable
     */
    public function getDatatable(string $datatableId): ?Datatable
    {
        return $this->datatables[$datatableId] ?? null;
    }

    /**
     * Appelle la méthode getError de chaque objet FormElement
     *
     * @return array|string Les erreurs rencontrées à la soumission du formulaire
     */
    public function getErrors(string $type = 'string'): array|string
    {
        if ($this->error !== null) {
            return   ('array' === $type) ? [$this->error] : $this->error;
        }

        $out = [];

        foreach ($this->getChilds() as $obj) {
            $out[] = $obj->getError();
        }

        $start = _('The required field ');
        $end   = _(' is empty or incorrectly filled');
        if (isset($out[1])) {
            $start = _('The required fields ');
            $end   = _(' are empty or incorrectly filled');
        }

        return ('array' === $type) ? $out : $start . '<ul>' . implode('', $out) . '</ul>' . $end ;
    }


    /**
     * Return Id attribute for html element
     */
    public function getIdAttribute(): string
    {
        return $this->getViewReference() . '_' . $this->getId();
    }

    /**
     * Return jsConfig
     */
    public function getJavascriptConfig(): array
    {
        return [];
    }

    /**
     * Get keep Open switch button
     *
     * @PhpUnitGen\get("keepOpen")
     */
    public function getKeepOpen(): bool|FieldInterface
    {
        return $this->keepOpen;
    }

    /**
     * Get value of _method
     *
     * @PhpUnitGen\get("_method")
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * Get the value of _switchGroup
     *
     * @PhpUnitGen\get("_switchGroup")
     */
    public function getSwitchGroup(): ?array
    {
        return $this->switchGroup;
    }



    /**
     * Get All values of form elements
     *
     * @return array
     */
    public function getValues(bool $internal = true): array
    {
        if ($this->values === null) {
            $this->values = [];
            $options      = [];
            /** @var FieldInterface $field */
            foreach ($this->getChilds() as $field) {
                if (($field->isExternal() && $internal) || (!$field->isExternal() && !$internal)) {
                    continue;
                }

                $this->values               = $field->getElementValue($this->values);
                $options[$field->getName()] = $field->getSanitizer();
            }

            /** @var SanitizerServiceInterface $sanitizer */
            $sanitizer = $this->getDi()->singleton(SanitizerServiceInterface::class);
            $sanitizer->sanitize($this->values, $options);
        }

        return $this->values;
    }


    /**
     *  Renvoie TRUE si le formulaire a été soumis, FALSE dans le cas contraire
     */
    public function isSubmitted(?array $array = null): bool
    {
        return (is_array($array) && array_key_exists($this->getIdAttribute(), $array));
    }

    /**
     * Renvoie TRUE si le formulaire a été soumis et s'il est conforme
     *
     * @var array $array L'array dans lequel chercher et vérifier la validité des valeurs
     */
    public function isValid(array $array, CsrfServiceInterface $csrf): bool
    {
        if (!$this->isSubmitted($array)) {
            $this->error = _('Form is not properly submitted.');

            return false;
        }

        if (!$csrf->validate($array)) {
            $this->error = _('Failed to check csrf, try to reload the page.');

            return false;
        }

        $noError = true;
        foreach ($this->getChilds() as $field) {
            if (!$field->isValid($array)) {
                $noError = false;
            }
        }

        return $noError;
    }

    /**
     * Keep Open switchButton
     *
     * @PhpUnitGen\params("'true'")
     *
     * @return static
     */
    public function keepOpen(bool $state = true)
    {
        if ($state && !$this->keepOpen) {
            $name = explode('_', $this->getName() ?? '');

            $this->keepOpen = new CheckboxElement($this);
            $this->keepOpen->setName('keepOpen')
                ->setLabel(sprintf(_('Create another %s ?'), end($name)))
                ->setId($this->getId() . '_' . uniqid());

            return $this;
        }

        $this->keepOpen = false;

        return $this;
    }

    /**
     * Pré-rempli les champs du formulaires avec les données de l'array
     *
     * @param array $values L'array d'où les valeurs sortent
     */
    public function pre(array $values)
    {
        if ($this->getChilds() === null) {
            return $this;
        }

        foreach ($this->getChilds() as $field) {
            $field->checkVar($values);
        }

        return $this;
    }

    /**
     * Set value of _action
     *
     * @PhpUnitGen\set("action")
     */
    public function setAction(string $var): self
    {
        $this->action = $var;

        return $this;
    }

    public function setCaptcha(bool $state): self
    {
        $this->captcha = $state;

        return $this;
    }

    /**
     * Set value of _method
     *
     * @PhpUnitGen\set("_method")
     */
    public function setMethod(string $var): self
    {
        $this->method = $var;

        return $this;
    }

    /**
     * Set Form name
     *
     * @PhpUnitGen\set("_name")
     */
    public function setName(string $name): self
    {
        $name = strtolower($name);

        $this->setId($name);
        $this->addClass($this->getIdAttribute());

        foreach ($this->getChilds() as $field) {
            /**
             * @var FormElement $field
             */
            $field->appendTo($this);
            if (in_array(SelectInterface::class, class_implements($field))) {
                /** @var SelectInterface $field */
                $this->addElementEvent($field);
            }
        }

        return $this;
    }

    /**
     * Set the value of _switchGroup
     *
     * @PhpUnitGen\set("_switchGroup")
     * @PhpUnitGen\set("_switchGroup")
     */
    public function switchGroup(?array $switchGroup = null): self
    {
        if (!is_array($switchGroup)) {
            $switchGroup = [
                [
                    'icon' => 'fa-database',
                    'name' => _('Database')
                ],
                [
                    'icon' => 'fa-pencil',
                    'name' => _('Manual')
                ]
            ];
        }

        $this->switchGroup = $switchGroup;

        return $this;
    }

    /**
     * List of allowed childs elements
     * could be a class, an abstract class or an interface
     */
    protected static function getAllowedChilds(): array
    {
        return [FieldInterface::class, FormElement::class, ContenableInterface::class];
    }

    /**
     * Add events on fields with button add
     *
     * @return void
     */
    private function addElementEvent(SelectInterface $element)
    {
        $element->setId($element->getName() . '_' . $this->getId());

        /** @var CommandFactory $commandFactory */
        $commandFactory = $this->getDi()->singleton(CommandFactory::class);

        /**
         * @var Form
         */
        $parent = $element->getParent();
        if ($element->getBtn() !== null) {
            /** @var ModalFactory $modalFactory */
            $modalFactory = $this->getDi()->singleton(ModalFactory::class);
            $modalCreate  = $modalFactory->get('create_' . $element->getBtn());
            $modalUpdate  = $modalFactory->get('update_' . $element->getBtn());

            /** @var FormFactory $formFactory */
            $formFactory = $this->getDi()->singleton(FormFactory::class);

            $formCreate = $formFactory->createFromWidget($element->getBtn(), ['method' => 'add']);
            $modalCreate->addForm($formCreate);
            $formUpdate = $formFactory->createFromWidget($element->getBtn(), ['method' => 'edit']);
            $modalUpdate->addForm($formUpdate);

            $element->on('click')
                ->selector('.' . $element->getId() . '_btn_create')
                ->addParameters(['previous' => $parent->getTitle()])
                ->command($commandFactory->create(ModalShowCommand::class, ['target' => $modalCreate]));

            $element->on('click')
                ->selector('.' . $element->getId() . '_btn_update')
                ->addParameters(['previous' => $parent->getTitle()])
                ->command($commandFactory->create(ModalShowCommand::class, ['target' => $modalUpdate]));

            $formCreate->on('success')
                ->command($commandFactory->create(FillFieldCommand::class, ['target' => $element]));

            $formUpdate->on('success')
                ->command($commandFactory->create(FillFieldCommand::class, ['target' => $element]));
        }

        /* if (!empty($element->triggerParms)) {

             foreach ($element->triggerParms as $params) {
                 $element->trigger($params);
             }
         }*/
    }

    /**
     * Get is captcha enabled;
     *
     * @PhpUnitGen\get("captcha")
     */
    public function getCaptcha(): bool
    {
        return $this->captcha;
    }
}
