<?php

namespace core\html\form\command;

use core\html\javascript\command\AbstractCommand;
use core\routing\Link;

/**
 * Class DeleteCommand
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class DeleteCommand extends AbstractCommand
{
    public function __construct(public ?string $target, public ?int $uuid = null, Link|array $endpoint = [])
    {
        if (!is_array($endpoint)) {
            $this->endpoint = [
                'endpoint'   => $endpoint->getEndpoint(),
                'initParams' => $endpoint->getParameters(),
                'method'     => $endpoint->getMethod()
            ];

            return;
        }

        $this->endpoint = $endpoint;
    }

    public function get(): string
    {
        return 'components/form/DeleteCommand';
    }

    public function getContext(): string
    {
        return 't-form';
    }
}
