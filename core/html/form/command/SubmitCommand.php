<?php

namespace core\html\form\command;

use core\html\form\Form;
use core\html\javascript\command\AbstractCommand;
use core\routing\Link;

/**
 * Class SubmitCommand
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class SubmitCommand extends AbstractCommand
{
    public function __construct(
        public Form $target,
        public string $method,
        public string $endpoint,
        public array $parameters = []
    ) {
    }

    public function get(): string
    {
        return 'components/form/SubmitCommand';
    }

    public function getContext(): string
    {
        return 't-form';
    }
}
