<?php

namespace core\html\form\command;

use core\html\form\Form;
use core\html\javascript\command\AbstractCommand;
use core\routing\Link;

/**
 * Class PopulateCommand
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class PopulateCommand extends AbstractCommand
{
    public $endpoint;


    public function __construct(string $indexKey, public string|Form $target, public ?int $uuid = null, Link|array $endpoint = [])
    {
        if (!is_array($endpoint)) {
            $this->endpoint = ['endpoint' => $endpoint->getEndpoint([$indexKey => $uuid]),
                'initParams'              => $endpoint->getParameters(),
                'method'                  => $endpoint->getMethod()];

            return;
        }

        $this->endpoint = $endpoint;
    }

    public function get(): string
    {
        return 'components/form/PopulateCommand';
    }

    public function getContext(): string
    {
        return 't-form';
    }
}
