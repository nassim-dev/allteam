<?php

namespace core\html\form;

use core\html\AbstractHtmlElement;
use core\html\form\command\SubmitStepCommand;
use core\html\HtmlElementContainer;
use core\html\javascript\JavascriptObjectInterface;
use core\html\javascript\WithJavascript;
use core\html\traits\Identifiable;

/**
 * Class Stepper
 *
 * Stepper to associate multiple forms
 *
 * @PhpUnitGen\assertClassHasAttribute("'_id'")
 * @PhpUnitGen\assertClassHasAttribute("'_lastStep'")
 * @PhpUnitGen\assertClassHasAttribute("'_steps'")
 *
 * @category  Stepper to associate multiple forms
 * @version   0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class Stepper extends AbstractHtmlElement implements JavascriptObjectInterface
{
    use WithJavascript;
    use Identifiable;

    /**
     * @PhpUnitGen\assertInstanceOf("Stepper::class")
     *
     * @param string $id
     *
     * @return static
     */
    public function __construct(string $identifier)
    {
        $this->setId($identifier);
    }

    /**
     * Add new step
     */
    public function addStep(?Form $step): self
    {
        if (null === $step) {
            return $this;
        }

        /** @var CommandFactory $commandFactory */
        $commandFactory = $this->getDi()->singleton(CommandFactory::class);

        $step = clone $step;

        $step->setName('stepper_' . $step->getName());
        $step->on('submit')
            ->command($commandFactory->create(SubmitStepCommand::class, ['target' => $step]));

        $this->appendChild($step);
        $this->_createStepEvents($step);

        return $this;
    }

    /**
     * Return Id attribute for html element
     */
    public function getIdAttribute(): string
    {
        return $this->getViewReference() . '_' . $this->getId();
    }

    /**
     * Get the value of _steps
     *
     * @PhpUnitGen\get("_steps")
     */
    public function getSteps(): HtmlElementContainer
    {
        return $this->getChilds();
    }

    /**
     * Get formatted steps as JS
     */
    public function getStepsJs(): array
    {
        return array_keys($this->getChilds()->toArray());
    }

    /**
     * Return an Array for object jsConfig
     */
    public function toArray(): array
    {
        $configArray          = parent::toArray();
        $configArray['steps'] = $this->getStepsJs();

        return $configArray;
    }

    /**
     * List of allowed childs elements
     * could be a class, an abstract class or an interface
     */
    protected static function getAllowedChilds(): array
    {
        return [Form::class];
    }

    /**
     * Add javascript event
     *
     * @return Form
     */
    private function _createStepEvents(Form &$step)
    {
        $current = $this->getChilds()->current();
        while ($current !== $step) {
            $current = $this->getChilds()->current();
        }

        $this->getChilds()->rewind();
        $previous = $this->getChilds()->current();

        if (($this->getChilds() === null ? 0 : count($this->getChilds())) > 1) {
            $step->addConfig('previousStep')->value($previous->getId());
            $previous->addConfig('nextStep')->value($step->getId());
        }

        return $step;
    }
}
