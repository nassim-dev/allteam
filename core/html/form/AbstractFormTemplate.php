<?php

namespace core\html\form;

use core\database\RepositoryFactoryInterface;
use core\DI\DiProvider;
use core\html\form\elements\FormElement;
use core\html\form\elements\HiddenElement;
use core\html\form\elements\interfaces\SelectInterface;
use core\html\form\elements\SelectElement;
use core\html\form\elements\StaticElement;
use core\messages\request\HttpRequestInterface;
use core\secure\authentification\AuthServiceInterface;

/**
 * Class AbstractFormTemplate
 *
 * Description
 *
 * @category  Description
 * @version   0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
abstract class AbstractFormTemplate
{
    use DiProvider;

    /**
     * Parameters
     *
     * @var array
     */
    public $params = [];

    public function __construct(
        protected AuthServiceInterface $auth,
        protected HttpRequestInterface $request,
        protected RepositoryFactoryInterface $repositoryFactory,
        array $params = []
    ) {
        $this->setParams(array_merge($this->request->getParameters(), $params));
    }

    public static function getDefaultTemplateDir()
    {
        $elements = explode('\\', static::class);
        array_pop($elements);

        return BASE_DIR . implode('/', $elements) . '/../../templates/';
    }

    /**
     * Return IndexField form Element
     *
     * index = 0;
     * label = uniqid();
     * table = $label;
     * addButon = SelectInterface::NO_BUTTON;
     * required = FormElement::REQUIRED;
     * type = SelectElement::class;
     * switchGroup = null;
     *
     * @param Form  $form   Parent Form
     * @param array $params paremeters Deafault
     *
     * @return FormElement
     */
    public function getIndexField(Form $form, array $params = [])
    {
        /**
         * Default Parameters
         */
        $index       = 0;
        $label       = uniqid();
        $table       = $label;
        $addButon    = SelectInterface::NO_BUTTON;
        $required    = FormElement::UNREQUIRED;
        $type        = SelectElement::class;
        $switchGroup = null;

        extract($params, EXTR_OVERWRITE);

        $indexName = 'id' . strtolower($table);

        if ($index > 0) {
            $object = $this->repositoryFactory->table($table)->getById($index);

            /** @var StaticElement $field */
            $field = $form->add(StaticElement::class);
            $field->setName($indexName . '_name')
                ->setLabel($label)
                ->setValue((is_object($object)) ? $object->name : _('No selection'));

            if (!null === $switchGroup) {
                $field->switchGroup($switchGroup);
            }

            /** @var HiddenElement $field */
            $field = $form->add(HiddenElement::class);
            $field->setName($indexName)
                ->setLabel($label)
                ->setValue($index);
            if (!null === $switchGroup) {
                $field->switchGroup($switchGroup);
            }

            return $field;
        }

        $field = $form->add($type)
            ->setName($indexName)
            ->setLabel($label);
        if ($addButon && in_array(SelectInterface::class, class_implements($field))) {
            /** @var SelectInterface $field */
            $field->btn(strtolower($table));
        }

        if ($required) {
            $field->required(true);
        }

        if (!null === $switchGroup) {
            $field->switchGroup($switchGroup);
        }

        return $field;
    }

    /**
     * Get parameters
     *
     * @PhpUnitGen\get("params")
     */
    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * Set parameters
     *
     * @PhpUnitGen\set("params")
     *
     * @param array $params Parameters
     */
    public function setParams(array $params): self
    {
        $this->params = $params;

        return $this;
    }

    /**
     * Check if a parameter is registred then return it or null
     *
     * @return mixed|null
     */
    protected function getParameter(string $key)
    {
        return $this->params[$key] ?? null;
    }

    /**
     * Return entitie/ressource name
     */
    public function getRessource(): string
    {
        $className = explode('\\', static::class);
        $ressource = str_replace('FormWidget', '', end($className));

        return strtolower($ressource);
    }

    /**
     * Create widget
     *
     * @param mixed $object
     */
    public function create(&$object, ?array $arguments = null): Form
    {
        if (!isset($arguments['method'])) {
            throw new FormException('You must provide a parameter `method`');
        }

        if (!method_exists($this, $arguments['method'])) {
            throw new FormException('Invalid method `' . $arguments['method'] . '`');
        }

        return  $this->getDi()->call($this, $arguments['method'], $arguments);
    }
}
