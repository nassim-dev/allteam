<?php

namespace core\html\form;

use core\html\form\elements\ColorpickerElement;
use core\html\form\elements\DateElement;
use core\html\form\elements\DaterangeElement;
use core\html\form\elements\DatetimeElement;
use core\html\form\elements\DatetimerangeElement;
use core\html\form\elements\FileinputElement;
use core\html\form\elements\HiddenElement;
use core\html\form\elements\MultiselectElement;
use core\html\form\elements\NumberElement;
use core\html\form\elements\SelectElement;
use core\html\form\elements\TextElement;
use core\html\form\elements\TimeElement;
use core\html\form\elements\TimerangeElement;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Helper
{
    public static function title(Form $form): TextElement
    {
        /** @var TextElement $field */
        $field = $form->add(TextElement::class);
        $field->setName('title')
            ->setLabel(_('Title'));

        return $field;
    }

    public static function OneToMany(string $table, Form $form): MultiselectElement
    {
        /** @var MultiselectElement $field */
        $field = $form->add(MultiselectElement::class);
        $field->setName('id' . $table)
            ->setLabel($table)
            ->btn($table);

        return $field;
    }

    public static function ManyToMany(string $table, Form $form): MultiselectElement
    {
        /** @var MultiselectElement $field */
        $field = $form->add(MultiselectElement::class);
        $field->setName('id' . $table)
            ->setLabel($table)
            ->btn($table);

        return $field;
    }

    public static function OneToOne(string $table, Form $form): SelectElement
    {
        /** @var SelectElement $field */
        $field = $form->add(SelectElement::class);
        $field->setName('id' . $table)
            ->setLabel($table)
            ->btn($table);

        return $field;
    }

    public static function imageInput(Form $form, string $name = 'img'): FileinputElement
    {
        /** @var FileinputElement $field */
        $field = $form->add(FileinputElement::class);
        $field->filetype(['.jpg', '.JPG', '.png', '.PNG', '.jpeg', '.JPEG', '.GIF', '.gif'])
            ->setLabel(_('Visual'))
            ->setName($name);

        return $field;
    }

    public static function phone(Form $form): TextElement
    {
        /** @var TextElement $field */
        $field = $form->add(TextElement::class);
        $field->setName('phone')
            ->setLabel(_('Phone'))
            ->mask("'mask': '99 99 99 99 99'")
            ->regex('/^(\+\d)*\s*(\(\d{3}\)\s*)*\d{3}(-{0,1}|\s{0,1})\d{2}(-{0,1}|\s{0,1})\d{2}$/')
            ->setIcon('fa-phone')
            ->setMinimum('10')
            ->setMaximum('10')
            ->setAutocomplete('tel');

        return $field;
    }

    public static function url(Form $form): TextElement
    {
        /** @var TextElement $field */
        $field = $form->add(TextElement::class);
        $field->setName('url')
            ->setLabel(_('Url'))
            ->setIcon('fa-link');

        return $field;
    }

    public static function currency(Form $form): NumberElement
    {
        /** @var NumberElement $field */
        $field = $form->add(NumberElement::class);
        $field->setName('price')
            ->setLabel(_('Price'))
            ->setIcon('fa-euro');

        return $field;
    }

    public static function datetime(Form $form): DatetimeElement
    {
        /** @var DatetimeElement $field */
        $field = $form->add(DatetimeElement::class);
        $field->setName('datetime')
            ->setLabel(_('Date'))
            ->setIcon('fa-calendar-clock');

        return $field;
    }

    public static function date(Form $form): DateElement
    {
        /** @var DateElement $field */
        $field = $form->add(DateElement::class);
        $field->setName('date')
            ->setLabel(_('Date'))
            ->setIcon('fa-calendar');

        return $field;
    }

    public static function time(Form $form): TimeElement
    {
        /** @var TimeElement $field */
        $field = $form->add(TimeElement::class);
        $field->setName('time')
            ->setLabel(_('Time'))
            ->setIcon('fa-clock');

        return $field;
    }

    public static function datetimerange(Form $form): DatetimerangeElement
    {
        /** @var DatetimerangeElement $field */
        $field = $form->add(DatetimerangeElement::class);
        $field->setLabel(_('Datetime'))
            ->setName('range')
            ->setIcon('fa-calendar-clock');

        return $field;
    }

    public static function daterange(Form $form): DaterangeElement
    {
        /** @var DaterangeElement $field */
        $field = $form->add(DaterangeElement::class);
        $field->setLabel(_('Date'))
            ->setName('range')
            ->setIcon('fa-calendar');

        return $field;
    }

    public static function timerange(Form $form): TimerangeElement
    {
        /** @var TimerangeElement $field */
        $field = $form->add(TimerangeElement::class);
        $field->setLabel(_('Time'))
            ->setName('range')
            ->setIcon('fa-clock');

        return $field;
    }

    public static function hidden(Form $form, string $name, mixed $value = null): HiddenElement
    {
        /** @var HiddenElement $field */
        $field = $form->add(HiddenElement::class);
        $field->setName($name)
            ->setValue($value);

        return $field;
    }

    public static function color(Form $form): ColorpickerElement
    {
        /** @var ColorpickerElement $field */
        $field = $form->add(ColorpickerElement::class);
        $field->setLabel(_('Color'))
            ->setName('color')
            ->setIcon('fa-palette');

        return $field;
    }
}
