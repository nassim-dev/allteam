<?php

namespace core\html\form;

use core\factory\FactoryInterface;
use core\html\HtmlElementFactory;

/**
 * Class FormFactory
 *
 * Form factory
 *
 * @category  Form factory
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class FormFactory extends HtmlElementFactory implements FactoryInterface
{
    /**
     * Return element class to build
     */
    protected function getElementClass(): string
    {
        return Form::class;
    }

    /**
     * Return suffix for widget classes
     */
    protected function getWidgetSuffix(): string
    {
        return 'FormWidget';
    }

    /**
     * Create new instance or return singleton if exist
     *
     * @PhpUnitGen\assertInstanceOf("Form::class")
     */
    public function create(string $identifier, array $params = []): ?Form
    {
        $params['name'] ??= $identifier;

        return parent::create($identifier, $params);
    }

    /**
     * Create from WidgetInterface
     */
    public function createFromWidget(string $className, array $params): ?Form
    {
        return parent::createFromWidget($className, $params);
    }

    public function get(string $identifier): ?Form
    {
        return $this->getInstance($identifier) ?? $this->create($identifier);
    }
}
