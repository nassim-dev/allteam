<?php

namespace core\html;

use core\DI\DiProvider;

/**
 * Trait AssetsProvider
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait AssetsProvider
{
    use DiProvider;

    /**
     * AssetsServiceInterface implémentation
     *
     * @var AssetsServiceInterface
     */
    private $assets;

    /**
     * Return AssetsServiceInterface implémetation
     */
    public function getAssets(): AssetsServiceInterface
    {
        return $this->assets ??= $this->getDi()->singleton(AssetsServiceInterface::class);
    }
}
