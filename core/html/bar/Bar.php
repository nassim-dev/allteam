<?php

namespace core\html\bar;

use core\html\AbstractHtmlElement;
use core\html\button\Button;
use core\html\form\elements\FormElementFactory;
use core\html\form\elements\interfaces\FieldInterface;
use core\html\form\elements\TextElement;
use core\html\HtmlElementInterface;
use core\html\traits\Deactivable;
use core\html\traits\Identifiable;
use core\html\traits\Labelable;
use core\html\traits\Stylizable;
use core\utils\Utils;

/**
 * Class Bar
 *
 * Generate toolbars
 *
 * @category html_component
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Bar extends AbstractHtmlElement
{
    use Stylizable;
    use Identifiable;
    use Labelable;
    use Deactivable;

    /**
     * ParentElement
     */
    private ?\core\html\HtmlElementInterface $_parentElement = null;

    /**
     * Constructor
     *
     * @return static
     */
    public function __construct(?string $identifier = null, ?string $label = null)
    {
        $this->id    = $identifier ?? uniqid();
        $this->label = $label;
    }


    /**
     * Add a new button
     */
    public function addButton(string $label): Button
    {
        $child = $this->getChild($label);
        if ($child !== null) {
            return $child;
        }

        $child = new Button($label, $this->id);

        $child->buildOn('shown.bs.dropdown', '#' . $this->getIdAttribute() . '_dropdown', 'deferred');
        $this->appendChild($child, $label);

        return $child;
    }


    /**
     * Add a new field
     *
     * @return FieldInterface
     */
    public function addField(string $label, string $fieldType = TextElement::class): FieldInterface
    {
        $factory = $this->getDi()->singleton(FormElementFactory::class);
        $child   = $factory->create($fieldType);
        //  $child->setLabel($label);
        $child->setName(Utils::hash($label));

        $this->appendChild($child);

        return $child;
    }

    public function setDropdown()
    {
        $this->setHtmlTemplate(static::getBaseTemplateDir() . '/bar/bar.dropdown.html');
    }

    /**
     * Empty buttons && fields
     */
    public function emptyBar(): self
    {
        $this->childs = [];

        return $this;
    }


    /**
     * Return child button element
     *
     * @return Button|null
     */
    public function getButton(string $label): ?Button
    {
        $buttons = $this->getButtons();
        if ($buttons === null) {
            return null;
        }

        $elements = array_filter($buttons, fn ($element): bool => $element->getLabel() === $label);

        return $elements[0] ?? null;
    }

    /**
     * Return registred buttons
     *
     * @return array|null
     */
    public function getButtons(): ?array
    {
        return $this->getChilds()->filterBy(Button::class);
    }


    /**
     * Return child field element
     *
     * @return FieldInterface|null
     */
    public function getField(string $label): ?FieldInterface
    {
        $elements = array_filter($this->getFields(), fn ($element): bool => $element->getLabel() === $label);

        return $elements[0] ?? null;
    }

    /**
     * Return registred fields
     *
     * @return array|null
     */
    public function getFields(): ?array
    {
        return $this->getChilds()->filterBy(FieldInterface::class);
    }

    /**
     * Return Id attribute for html element
     *
     * @return string
     */
    public function getIdAttribute(): string
    {
        return $this->getViewReference() . '_' . $this->getId();
    }

    /**
     * Has registred buttons ?
     *
     * @return boolean
     */
    public function hasButtons(): bool
    {
        $buttons = $this->getButtons();

        return ($this->hasChild()) ? ($buttons !== null && count($buttons) > 0) : false;
    }

    /**
     * Has registred buttons ?
     *
     * @return bool
     */
    public function hasFields(): bool
    {
        $fields = $this->getFields();

        return ($this->hasChild()) ? ($fields !== null && count($fields) > 0) : false;
    }


    /**
     * Remove bar action
     *
     * @return void
     */
    public function removeAction(string $label, string $type = 'Button')
    {
        $element = ('Button' === $type) ? $this->getButton($label) : $this->getField($label);
        if (null !== $element) {
            $this->removeChild($element);
        }
    }

    /**
     * Replace existing button
     */
    public function replaceButton(string $label, ?Button $button): Button
    {
        $registredButton = $this->getButton($label);
        if (null !== $registredButton) {
            $this->removeChild($registredButton);
        }

        return (null !== $button) ? $this->appendChild($button) : $this->addButton($label);
    }

    /**
     * List of allowed childs elements
     * could be a class, an abstract class or an interface
     */
    protected static function getAllowedChilds(): array
    {
        return [Button::class, FieldInterface::class];
    }

    /**
     * Get parentElement
     *
     * @PhpUnitGen\get("_parentElement")
     */
    public function getParentElement(): ?HtmlElementInterface
    {
        return $this->_parentElement;
    }

    /**
     * Set parentElement
     *
     * @param HtmlElementInterface|null $_parentElement ParentElement
     *
     * @PhpUnitGen\set("_parentElement")
     */
    public function setParentElement(?HtmlElementInterface $_parentElement): self
    {
        $this->_parentElement = $_parentElement;

        return $this;
    }
}
