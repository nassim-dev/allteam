<?php

namespace core\html\bar;

use core\entities\EntitieInterface;
use core\factory\FactoryInterface;
use core\html\HtmlElementFactory;
use core\html\HtmlElementInterface;

/**
 * Undocumented class
 *
 * Description
 *
 * @example
 *
 * $element    = $this->create('test_bar');
 *
 * $element->setHtmlTemplate($element::getBaseTemplateDir() . 'bar/bar.dropdown.html');
 * $element->enable();
 *
 * $button = $element->addButton('Add');
 * $button->setIcon('fa-plus');
 * $button->addClass(' btn-light-primary');
 * $button->enable();
 *
 * $field = $element->addField('Test field');
 * $field->isBuildInElement();
 *
 * $this->view->assignWidget($element);
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class BarFactory extends HtmlElementFactory implements FactoryInterface
{
    /**
     * Return element class to build
     */
    protected function getElementClass(): string
    {
        return Bar::class;
    }

    /**
     * Return suffix for widget classes
     */
    protected function getWidgetSuffix(): string
    {
        return 'BarWidget';
    }

    /**
     * Create new instance or return singleton if exist
     *
     * @PhpUnitGen\assertInstanceOf("Bar::class")
     */
    public function create(string $identifier, array $params = []): ?Bar
    {
        $params['label'] ??= null;

        return parent::create($identifier, $params);
    }

    public function createForEntity(EntitieInterface $object): ?Bar
    {
        $identifier = $object::class . '_' . $object->getPrimary();

        return $this->create($identifier);
    }

    public function createForHtmlElement(HtmlElementInterface $object): ?Bar
    {
        $identifier = $object->getIdAttribute() . '_' . Bar::prefix();

        return $this->create($identifier);
    }

    /**
     * Get registred elements
     */
    public function get(string $identifier): ?Bar
    {
        return $this->getInstance($identifier) ?? $this->create($identifier);
    }

    /**
     * Create default Buttons
     */
    public function createDefaultButtons(EntitieInterface $object, array $buttons = ['edit' => true, 'delete' => true, 'copy' => true]): Bar
    {
        $actionBar = $object->getActionBar();
        $actionBar->setHtmlTemplate($actionBar::getBaseTemplateDir() . 'bar/bar.dropdown.html');

        $lockerUser = $object->getLockerUser();
        if (!is_bool($lockerUser)) {
            //return $actionBar->lock();
        }

        if ($buttons['edit']) {
            $button = $object->createButton(_('Edit'), ['target' => 'form_update_' . $object->getTable()->getName(), 'indexKey' => $object->getPrimaryKey()]);
            $button->setHtmlTemplate($button::getBaseTemplateDir() . 'button/button.dropdown.html');
        }

        if ($buttons['delete']) {
            $button = $object->createButton(_('Delete'), ['target' => $object->getPrimary()]);
            $button->setHtmlTemplate($button::getBaseTemplateDir() . 'button/button.dropdown.html');
        }

        if ($buttons['copy']) {
            $button = $object->createButton(_('Copy'), ['target' => $object->getPrimary()]);
            $button->setHtmlTemplate($button::getBaseTemplateDir() . 'button/button.dropdown.html');
        }

        return $actionBar;
    }
}
