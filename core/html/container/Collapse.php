<?php

namespace core\html\container;

use core\html\AbstractHtmlElement;
use core\html\HtmlElementInterface;
use core\html\traits\Identifiable;
use core\html\traits\Namable;

/**
 * Class Collapse
 *
 * Description
 *
 * @category html_component
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Collapse extends AbstractHtmlElement implements HtmlElementInterface, ContenableInterface
{
    use Identifiable;
    use Namable;

    public function __construct()
    {
        $this->setHasContainer();
        $this->setId(uniqid());
    }

    /**
     * Return Id attribute for html element
     */
    public function getIdAttribute(): string
    {
        return $this->getViewReference() . '_' . $this->getId();
    }


    /**
     * List of allowed childs elements
     * could be a class, an abstract class or an interface
     */
    protected static function getAllowedChilds(): array
    {
        return [HtmlElementInterface::class];
    }
}
