<?php

namespace core\html\container;

use core\html\HtmlElementInterface;

interface ContenableInterface extends HtmlElementInterface
{
}
