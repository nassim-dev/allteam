<?php

namespace core\html\modal;

use core\DI\DI;
use core\html\AbstractHtmlElement;
use core\html\accordion\Accordion;
use core\html\calendar\Calendar;
use core\html\CommandFactory;
use core\html\diagram\Diagram;
use core\html\form\command\LockCommand;
use core\html\form\command\UnlockCommand;
use core\html\form\Form;
use core\html\HtmlElement;
use core\html\javascript\JavascriptObjectInterface;
use core\html\javascript\WithJavascript;
use core\html\tables\datatable\Datatable;
use core\html\tables\pillsTable\PillsTable;
use core\html\tables\tableStriped\TableStriped;
use core\html\traits\DataAttribute;
use core\html\traits\Identifiable;
use core\html\traits\Stylizable;
use core\html\treeview\Treeview;
use core\secure\permissions\ModalPermission;
use core\secure\permissions\PermissionServiceInterface;
use core\secure\permissions\PermissionSetInterface;

/**
 * Class Modal
 *
 * Modal element
 *
 * @category  Modal element
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Modal extends AbstractHtmlElement implements JavascriptObjectInterface
{
    use WithJavascript;
    use Identifiable;
    use DataAttribute;
    use Stylizable;

    public const DEFERRED = true;

    public const NOT_DEFERRED = false;

    public const CHECK_PERMISSION = true;

    public const DONT_CHECK_PERMISSION = false;

    public const AUTOSHOW = true;

    public const DONTSHOW = false;

    public const PUBLIC_CSS = ['animate', 'animate__animated', 'animate__fast', 'animate__bounceInLeft'];

    public const PRIVATE_CSS = ['modal-boxed', 'animate', 'animate__animated', 'animate__fast', 'animate__bounceInLeft'];

    /**
     * If true, disable close & previous buttons
     */
    private bool $static = false;

    /**
     * Modal constructor.
     *
     * @PhpUnitGen\assertInstanceOf("Modal::class")
     */
    public function __construct(string $identifier, ?Form $form = null, private bool $autoshow = self::DONTSHOW)
    {
        $this->setId($identifier);

        $this->buildOn('show.bs.modal', $this);
        if (null !== $form) {
            $this->addForm($form);
        }

        $this->addDataAttribute('save', 'true');
    }


    public static function buildDefaultPermissions(PermissionSetInterface $permissionSet): PermissionSetInterface
    {
        /** @var PermissionServiceInterface $permissionService */
        $permissionService = DI::singleton(PermissionServiceInterface::class);
        $permissionSet->addRequiredCondition($permissionService->getPermissionClass(ModalPermission::class));

        return $permissionSet;
    }

    /**
     * Set value of _accordion
     *
     * @PhpUnitGen\set("_accordion")
     */
    public function addAccordion(?Accordion $accordion): self
    {
        if (null !== $accordion) {
            $this->appendChild($accordion);
        }

        return $this;
    }

    /**
     * Set value of _calendar
     *
     * @PhpUnitGen\set("_calendar")
     *
     * @return statics
     */
    public function addCalendar(?Calendar $calendar): self
    {
        if (null !== $calendar) {
            $this->appendChild($calendar);
            $calendar->buildOn('shown.bs.modal', $this, 'deferred');
        }

        return $this;
    }

    /**
     * Set value of _diagram
     *
     * @PhpUnitGen\set("_diagram")
     */
    public function addDiagram(?Diagram $diagram): self
    {
        if (null !== $diagram) {
            $diagram->appendTo($this);
            $diagram->buildOn('shown.bs.modal', $this, 'deferred');
        }

        return $this;
    }

    /**
     * Set value of _form
     */
    public function addForm(?Form $form, bool $deferred = self::DEFERRED): self
    {
        if (null !== $form) {
            $form->appendTo($this);
            if ($deferred) {
                $form->buildOn('shown.bs.modal', $this, 'deferred');
            }

            $form->addOnChangeEvents();
        }

        return $this;
    }

    /**
     * Set value of _table
     *
     * @PhpUnitGen\set("_table")
     */
    public function addTable(?Datatable $table): self
    {
        if (null !== $table) {
            $table->appendTo($this);
            $table->buildOn('shown.bs.modal', $this, 'deferred');
        }

        return $this;
    }

    /**
     * Set value of _text
     *
     * @PhpUnitGen\set("_text")
     */
    public function addText(string $content, ?string $title = null): self
    {
        $element = new HtmlElement(['content' => $content, 'title' => $title]);
        $element->setHtmlTemplate(self::getBaseTemplateDir() . 'block/Text.html');
        $this->appendChild($element);

        return $this;
    }

    /**
     * Get value of _accordion
     *
     * @return Accordion[]|null
     */
    public function getAccordions(): ?array
    {
        return $this->getChilds()->filterBy(Accordion::class);
    }


    /**
     * Get value of _calendar
     *
     * @return Calendar[]|null
     */
    public function getCalendars(): ?array
    {
        return $this->getChilds()->filterBy(Calendar::class);
    }

    /**
     * Get diagram object inside modal
     *
     * @return Diagram[]|null
     */
    public function getDiagrams(): ?array
    {
        return $this->getChilds()->filterBy(Diagram::class);
    }

    /**
     * Get value of _form
     *
     * @return Form[]|null
     */
    public function getForms(): ?array
    {
        return $this->getChilds()->filterBy(Form::class);
    }

    /**
     * Return Id attribute for html element
     */
    public function getIdAttribute(): string
    {
        return $this->getViewReference() . '_' . $this->getId();
    }

    /**
     * Get value of _table
     *
     * @PhpUnitGen\get("_table")
     *
     * @return Datatable[]|null
     */
    public function getTables(): ?array
    {
        return $this->getChilds()->filterBy(Datatable::class);
    }

    /**
     * Get value of _text
     *
     * @PhpUnitGen\get("_text")
     *
     * @return HtmlElement[]|null
     */
    public function getTexts(): ?array
    {
        return $this->getChilds()->filterBy(HtmlElement::class);
    }

    /**
     * Get value of autoShow
     *
     * @PhpUnitGen\get("autoShow")
     */
    public function isAutoshow(): bool
    {
        return $this->autoshow;
    }

    /**
     * Get the value of static
     *
     * @PhpUnitGen\get("static")
     */
    public function isStatic(): bool
    {
        return $this->static;
    }

    /**
     * Register event to prevent multiple user edition
     */
    public function registerLockEvents(?string $ressource)
    {
        if ($ressource === null) {
            return;
        }

        /** @var CommandFactory $commandFactory */
        $commandFactory = DI::singleton(CommandFactory::class);

        $this->on('shown.bs.modal')
            ->addParameters(['ressource' => $ressource])
            ->command($commandFactory->create(LockCommand::class, ['endpoint' => "/api/$ressource/lock/:id"]));

        $this->on('hidden.bs.modal')
            ->addParameters(['ressource' => $ressource])
            ->command($commandFactory->create(UnlockCommand::class, ['endpoint' => "/api/$ressource/unlock/:id"]));
    }


    /**
     * Set the value of autoShow
     *
     * @PhpUnitGen\set("autoShow")
     */
    public function setAutoShow(bool $autoShow): self
    {
        $this->autoShow = $autoShow;

        return $this;
    }


    /**
     * Set the value of static
     *
     * @PhpUnitGen\set("static")
     */
    public function setStatic(bool $static): self
    {
        $this->static = $static;

        $this->removeDataAttribute('keyboard');
        $this->addDataAttribute('backdrop', 'false');

        if ($static) {
            $this->addDataAttributes(['backdrop' => 'static', 'keyboard' => 'false']);
        }

        return $this;
    }

    /**
     * List of allowed childs elements
     * could be a class, an abstract class or an interface
     */
    protected static function getAllowedChilds(): array
    {
        return [
            HtmlElement::class,
            Datatable::class,
            Form::class,
            PillsTable::class,
            Calendar::class,
            Accordion::class,
            TableStriped::class,
            Treeview::class
        ];
    }
}
