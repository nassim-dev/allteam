<?php

namespace core\html\modal;

use core\factory\FactoryInterface;
use core\html\form\Form;
use core\html\HtmlElementFactory;

/**
 * Class ModalFactory
 *
 * Modal Factory
 *
 * @category  Modal Factory
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class ModalFactory extends HtmlElementFactory implements FactoryInterface
{
    public const CHECK_PERMISSION = Modal::CHECK_PERMISSION;

    public const DONT_CHECK_PERMISSION = Modal::DONT_CHECK_PERMISSION;

    public const AUTOSHOW = Modal::AUTOSHOW;

    public const DONTSHOW = Modal::DONTSHOW;


    /**
     * Return element class to build
     */
    protected function getElementClass(): string
    {
        return Modal::class;
    }

    /**
     * Return suffix for widget classes
     */
    protected function getWidgetSuffix(): string
    {
        return 'ModalWidget';
    }

    /**
     * Create new instance or return singleton if exist
     *
     * @PhpUnitGen\assertInstanceOf("Modal::class")
     */
    public function create(string $identifier, array $params = []): ?Modal
    {
        $params['data']       = (isset($params[0]) && $params[0] instanceof Form) ? $params[0] : null;
        $params['autoshow']   = (!isset($params[1]) || !is_bool($params[1])) ? self::DONTSHOW : $params[1];
        $params['authcheck']  = (!isset($params[2]) || !is_bool($params[2])) ? self::CHECK_PERMISSION : $params[2];
        $params['identifier'] = $identifier;

        $modal = parent::create($identifier, $params);
        if ($modal !== null) {
            /** @var Modal $modal */
            ($this->auth->isPublic()) ? $modal->addClasses(Modal::PUBLIC_CSS) : $modal->addClasses(Modal::PRIVATE_CSS);
        }

        return $modal;
    }

    /**
     * Create from WidgetInterface
     */
    public function createFromWidget(string $className, array $params): ?Modal
    {
        return parent::createFromWidget($className, $params);
    }

    /**
     * Get registred elements
     */
    public function get(string $identifier): ?Modal
    {
        return $this->getInstance($identifier) ?? $this->create($identifier);
    }
}
