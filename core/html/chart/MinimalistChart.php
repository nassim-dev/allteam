<?php

namespace core\html\chart;

use core\html\AbstractHtmlElement;
use core\html\javascript\JavascriptObjectInterface;
use core\html\javascript\WithJavascript;
use core\html\traits\Colorable;
use core\html\traits\Labelable;

/**
 * Class MinimalistChart
 *
 * Description
 *
 *  Usage
 *
 * $chart = ChartFactory::create("Minimalist", "New2");
 *
 * $chart = ChartFactory::create("Doughnut", "New3");
 * $chart->setLabels(array("Red", "Green", "Yellow", "Grey", "Dark Grey"));
 * $dataset = $chart->addDataset(new ChartDataset());
 * $dataset->setData(array(300, 50, 100, 40, 120))
 *     ->setBackgroundColor(array("#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360"))
 *     ->setHoverBackgroundColor(array("#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5", "#616774"));
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class MinimalistChart extends AbstractHtmlElement implements JavascriptObjectInterface, ChartInterface
{
    use WithJavascript;
    use ChartTrait;
    use Colorable;
    use Labelable;

    /**
     * @var string
     */
    public $labelColor = 'green';

    /**
     * @var string
     */
    public $labelIcon = 'fa-arrow-circle-up';

    /**
     * @var int
     */
    public $percent = 100;

    /**
     * @param $identifier
     *
     * @return static
     */
    public function __construct(string $identifier)
    {
        $this->setColor('#4caf50');
        $this->setId($identifier);
        $this->setFormat('minimalist');
    }

    /**
     * Get the value of labelColor
     *
     * @PhpUnitGen\get("labelColor")
     */
    public function getLabelColor(): string
    {
        return $this->labelColor;
    }

    /**
     * Get the value of labelIcon
     *
     * @PhpUnitGen\get("labelIcon")
     */
    public function getLabelIcon(): string
    {
        return $this->labelIcon;
    }

    /**
     * Get the value of percent
     *
     * @PhpUnitGen\get("percent")
     */
    public function getPercent(): int
    {
        return $this->percent;
    }

    /**
     * Set the value of labelColor
     *
     * @PhpUnitGen\set("labelColor")
     */
    public function setLabelColor(string $labelColor): self
    {
        $this->labelColor = $labelColor;

        return $this;
    }

    /**
     * Set the value of labelIcon
     *
     * @PhpUnitGen\set("labelIcon")
     */
    public function setLabelIcon(string $labelIcon): self
    {
        $this->labelIcon = $labelIcon;

        return $this;
    }

    /**
     * Set the value of percent
     *
     * @PhpUnitGen\set("percent")
     */
    public function setPercent(int $percent): self
    {
        $this->percent = $percent;

        return $this;
    }
}
