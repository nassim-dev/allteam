<?php

namespace core\html\chart;

use core\html\AbstractHtmlElement;
use core\html\javascript\JavascriptObjectInterface;
use core\html\javascript\WithJavascript;

/**
 * Class BarChart
 *
 * Description
 *
 * @category  Description
 * @version 0.1
 * Usage
 * $chart = ChartFactory::create("Bar", "New");
 * $chart->setLabels(array("Red", "Green", "Yellow", "Grey", "Dark Grey"));
 * $dataset = $chart->addDataset(new ChartDataset());
 * $dataset->setData(array(12, 19, 3, 5, 2, 3))
 *     ->setBackgroundColor(array(
 *         'rgba(255, 99, 132, 0.2)',
 *         'rgba(54, 162, 235, 0.2)',
 *         'rgba(255, 206, 86, 0.2)',
 *         'rgba(75, 192, 192, 0.2)',
 *         'rgba(153, 102, 255, 0.2)',
 *         'rgba(255, 159, 64, 0.2)'))
 *     ->setBorderColor(array('rgba(255,99,132,1)',
 *         'rgba(54, 162, 235, 1)',
 *         'rgba(255, 206, 86, 1)',
 *         'rgba(75, 192, 192, 1)',
 *         'rgba(153, 102, 255, 1)',
 *         'rgba(255, 159, 64, 1)'));
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 * @since File available since Release 0.1
 */
class BarChart extends AbstractHtmlElement implements JavascriptObjectInterface, ChartInterface
{
    use WithJavascript;
    use ChartTrait;

    private int $width = 500;

    /**
     * @return static
     */
    public function __construct(string $identifier)
    {
        $this->setId($identifier);
        $this->setFormat('bar');
    }

    /**
     * Get the value of width
     *
     * @PhpUnitGen\get("width")
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * Set the value of width
     *
     * @PhpUnitGen\set("width")
     */
    public function setWidth(int $width): self
    {
        $this->width = $width;

        return $this;
    }
}
