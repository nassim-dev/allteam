<?php

namespace core\html\chart;

use core\factory\FactoryException;
use core\factory\FactoryInterface;
use core\html\HtmlElementFactory;

/**
 * Class ChartFactory
 *
 * Chart factory
 *
 * @category  Chart factory
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class ChartFactory extends HtmlElementFactory implements FactoryInterface
{
    /**
     * Return element class to build
     */
    protected function getElementClass(): string
    {
        return ChartInterface::class;
    }

    /**
     * Return suffix for widget classes
     */
    protected function getWidgetSuffix(): string
    {
        return 'ChartWidget';
    }

    /**
     * Create new instance or return singleton if exist
     *
     * @PhpUnitGen\assertInstanceOf("ChartInterface::class")
     */
    public function create(string $identifier, array $params = []): ?ChartInterface
    {
        if (!isset($params['type'])) {
            throw new FactoryException('You must define $params[\'type\']');
        }

        return parent::create($identifier, $params);
    }

    public function get(string $identifier): ?ChartInterface
    {
        return $this->getInstance($identifier) ?? $this->create($identifier);
    }
}
