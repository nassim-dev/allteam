<?php

namespace core\html\chart;

use core\html\HtmlElementInterface;
use core\html\javascript\JavascriptObjectInterface;

/**
 * Interface ChartInterface
 *
 * Description
 *
 * @todo interface must be smaller
 *
 * @category  Description
 * @version   0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
interface ChartInterface extends HtmlElementInterface, JavascriptObjectInterface
{
    public function addDataset(ChartDataset $dataset): ChartDataset;


    /**
     * Get the value of data
     */
    public function getData(): array;

    /**
     * Get the value of datasets
     */
    public function getDatasets(): array;

    /**
     * Get the value of format
     */
    public function getFormat(): ?string;

    /**
     * Get the value of id
     */
    public function getId(): ?string;

    /**
     * Return  jsConfig
     */
    public function getJavascriptConfig(): array;

    /**
     * Get the value of labels
     */
    public function getLabels(): array;

    /**
     * Get the value of options
     */
    public function getOptions(): array;



    /**
     * Set data attributes
     *
     * @param  string|null|array $val
     * @return void
     */
    public function setData(string $key, $val = null);

    /**
     * Set the value of format
     *
     * @return static
     */
    public function setFormat(string $format);

    /**
     * Set the value of id
     *
     * @return static
     */
    public function setId(string $identifier);

    /**
     * Set the value of labels
     *
     * @return static
     */
    public function setLabels(array $labels);

    /**
     * Undocumented function
     *
     * @param  string|null|array $val
     * @return void
     */
    public function setOptions(string $key, $val = null);
}
