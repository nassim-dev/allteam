<?php

namespace core\html\chart;

use core\html\AbstractHtmlElement;
use core\html\javascript\JavascriptObjectInterface;
use core\html\javascript\WithJavascript;

/**
 * Class PieChart
 *
 * Description
 *
 * Usage
 *
 * $chart = ChartFactory::create("Pie", "New4");
 * $chart->setLabels(array("Red", "Green", "Yellow", "Grey", "Dark Grey"));
 * $dataset = $chart->addDataset(new ChartDataset());
 * $dataset->setData(array(300, 50, 100, 40, 120))
 * ->setBackgroundColor(array("#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360"))
 * ->setHoverBackgroundColor(array("#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5", "#616774"));
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class PieChart extends AbstractHtmlElement implements JavascriptObjectInterface, ChartInterface
{
    use WithJavascript;
    use ChartTrait;

    /**
     * @return static
     */
    public function __construct(string $identifier)
    {
        $this->setId($identifier);
        $this->setFormat('pie');
    }
}
