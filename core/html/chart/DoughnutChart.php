<?php

namespace core\html\chart;

use core\html\AbstractHtmlElement;
use core\html\javascript\JavascriptObjectInterface;
use core\html\javascript\WithJavascript;

/**
 * Class DoughnutChart
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class DoughnutChart extends AbstractHtmlElement implements JavascriptObjectInterface, ChartInterface
{
    use WithJavascript;
    use ChartTrait;

    /**
     * @return static
     */
    public function __construct(string $identifier)
    {
        $this->setId($identifier);
        $this->setFormat('doughnut');
    }
}
