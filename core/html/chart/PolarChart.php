<?php

namespace core\html\chart;

use core\html\AbstractHtmlElement;
use core\html\javascript\JavascriptObjectInterface;
use core\html\javascript\WithJavascript;

/**
 * Class PolarChart
 *
 * Description
 *
 *  Usage
 *
 * $chart = ChartFactory::create("Polar", "New5");
 * $chart->setLabels(array("Red", "Green", "Yellow", "Grey", "Dark Grey"));
 * $dataset = $chart->addDataset(new ChartDataset());
 * $dataset->setData(array(300, 50, 100, 40, 120))
 *     ->setBackgroundColor(array("rgba(219, 0, 0, 0.1)", "rgba(0, 165, 2, 0.1)", "rgba(255, 195, 15, 0.2)", "rgba(55, 59, 66, 0.1)", "rgba(0, 0, 0, 0.3)"))
 *     ->setHoverBackgroundColor(array("rgba(219, 0, 0, 0.2)", "rgba(0, 165, 2, 0.2)", "rgba(255, 195, 15, 0.3)", "rgba(55, 59, 66, 0.1)", "rgba(0, 0, 0, 0.4)"));
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class PolarChart extends AbstractHtmlElement implements JavascriptObjectInterface, ChartInterface
{
    use WithJavascript;
    use ChartTrait;

    /**
     * @return static
     */
    public function __construct(string $identifier)
    {
        $this->setId($identifier);
        $this->setFormat('polar');
    }
}
