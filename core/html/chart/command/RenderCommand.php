<?php

namespace core\html\chart\command;

use core\html\chart\ChartInterface;
use core\html\javascript\command\AbstractCommand;

/**
 * Class RenderCommand
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class RenderCommand extends AbstractCommand
{
    public function __construct(private ChartInterface $target, private array $parameters = [])
    {
    }

    public function get(): string
    {
        return 'components/chart/RenderCommand';
    }

    public function getContext(): string
    {
        return 't-chart';
    }
}
