<?php

namespace core\html\chart;

use core\html\AbstractHtmlElement;
use core\html\javascript\JavascriptObjectInterface;
use core\html\javascript\WithJavascript;

/**
 * Class RadarChart
 *
 * Description
 *
 * $chart = ChartFactory::create("Radar", "New6");
 * $chart->setLabels(array("Red", "Green", "Yellow", "Grey", "Dark Grey"));
 * $dataset = $chart->addDataset(new ChartDataset());
 * $dataset->setData(array(300, 50, 100, 40, 120))
 *     ->setLabel("One")
 *     ->setBackgroundColor(array("rgba(219, 0, 0, 0.1)"))
 *     ->setHoverBackgroundColor(array('rgba(200, 99, 132, .7)'));
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class RadarChart extends AbstractHtmlElement implements JavascriptObjectInterface, ChartInterface
{
    use WithJavascript;
    use ChartTrait;

    /**
     * @return static
     */
    public function __construct(string $identifier)
    {
        $this->setId($identifier);
        $this->setFormat('radar');
    }
}
