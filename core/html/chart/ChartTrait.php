<?php

namespace core\html\chart;

use core\html\traits\AjaxSourced;
use core\html\traits\Identifiable;
use core\html\traits\Titleable;

/**
 * Trait ChartTrait
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait ChartTrait
{
    use Identifiable;
    use Titleable;
    use AjaxSourced;

    /**
     * @var array
     */
    public $data = [];

    /**
     * @var array
     */
    public $datasets = [];

    /**
     * @var string|null
     */
    public $format = null;

    /**
     * @var array
     */
    public $labels = [];

    /**
     * @var array
     */
    public $options = ['responsive' => true];



    /**
     * Set the value of datasets
     */
    public function addDataset(ChartDataset $dataset): ChartDataset
    {
        return $this->datasets[] = $dataset;
    }



    /**
     * Get the value of data
     *
     * @PhpUnitGen\get("data")
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * Get the value of datasets
     *
     * @PhpUnitGen\get("datasets")
     */
    public function getDatasets(): array
    {
        return $this->datasets;
    }

    /**
     * Get the value of format
     *
     * @PhpUnitGen\get("format")
     */
    public function getFormat(): ?string
    {
        return $this->format;
    }

    /**
     * Return Id attribute for html element
     */
    public function getIdAttribute(): string
    {
        return $this->getViewReference() . '_' . $this->getId();
    }

    /**
     * Return jsConfig
     */
    public function getJavascriptConfig(): array
    {
        $jsConfig = [];
        $tmp      = get_object_vars($this);
        unset($tmp['jsConfig'], $tmp['jsPreActions'], $tmp['jsPostActions'], $tmp['jsListeners'], $tmp['id']);




        foreach ($tmp as $k => $v) {
            $jsConfig[$k] = $v;
        }

        return $jsConfig;
    }

    /**
     * Get the value of labels
     *
     * @PhpUnitGen\get("labels")
     */
    public function getLabels(): array
    {
        return $this->labels;
    }

    /**
     * Get the value of options
     *
     * @PhpUnitGen\get("options")
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * Set data attributes
     *
     * @param  string|null|array $val
     * @return void
     */
    public function setData(string $key, $val = null)
    {
        $this->data[$key] = $val;
        if (null === $val && is_array($key)) {
            $this->data = (empty($this->data)) ? $key : array_merge($this->data, $key);
        }

        return $this;
    }

    /**
     * Set the value of format
     *
     * @PhpUnitGen\set("format")
     *
     * @return static
     */
    public function setFormat(string $format)
    {
        $this->format = $format;

        return $this;
    }

    /**
     * Set the value of labels
     *
     * @PhpUnitGen\set("labels")
     *
     * @return static
     */
    public function setLabels(array $labels)
    {
        $this->labels = $labels;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @param  string|null|array $val
     * @return void
     */
    public function setOptions(string $key, $val = null)
    {
        if (null === $val && is_array($key)) {
            $this->options = (empty($this->options)) ? $key : array_merge($this->options, $key);
        } else {
            $this->options[$key] = $val;
        }

        return $this;
    }

    /**
     * List of allowed childs elements
     * could be a class, an abstract class or an interface
     */
    protected static function getAllowedChilds(): array
    {
        return [];
    }
}
