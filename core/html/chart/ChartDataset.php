<?php

namespace core\html\chart;

use core\html\traits\Labelable;

/**
 * Class ChartDataset
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class ChartDataset
{
    use Labelable;
    /**
     * @var array
     */
    public $backgroundColor = ['rgba(0, 137, 132, .2)'];

    /**
     * @var array
     */
    public $borderColor = ['rgba(0, 10, 130, .7)'];

    /**
     * @var int
     */
    public $borderWidth = 2;

    /**
     * @var array
     */
    public $data = [28, 48, 40, 19, 86, 27, 90];

    /**
     * @var array
     */
    public $hoverBackgroundColor = ['#FF5A5E', '#5AD3D1', '#FFC870', '#A8B3C5', '#616774'];

    /**
     * Get the value of backgroundColor
     *
     * @PhpUnitGen\get("backgroundColor")
     */
    public function getBackgroundColor(): array
    {
        return $this->backgroundColor;
    }

    /**
     * Get the value of borderColor
     *
     * @PhpUnitGen\get("borderColor")
     */
    public function getBorderColor(): array
    {
        return $this->borderColor;
    }

    /**
     * Get the value of borderWidth
     *
     * @PhpUnitGen\get("borderWidth")
     */
    public function getBorderWidth(): int
    {
        return $this->borderWidth;
    }

    /**
     * Get the value of data
     *
     * @PhpUnitGen\get("data")
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * Get the value of hoverBackgroundColor
     *
     * @PhpUnitGen\get("hoverBackgroundColor")
     */
    public function getHoverBackgroundColor(): array
    {
        return $this->hoverBackgroundColor;
    }

    /**
     * Set the value of backgroundColor
     *
     * @PhpUnitGen\set("backgroundColor")
     *
     * @param mixed[] $backgroundColor
     */
    public function setBackgroundColor(array $backgroundColor): self
    {
        $this->backgroundColor = $backgroundColor;

        return $this;
    }

    /**
     * Set the value of borderColor
     *
     * @PhpUnitGen\set("borderColor")
     *
     * @param mixed[] $borderColor
     */
    public function setBorderColor(array $borderColor): self
    {
        $this->borderColor = $borderColor;

        return $this;
    }

    /**
     * Set the value of borderWidth
     *
     * @PhpUnitGen\set("borderWidth")
     */
    public function setBorderWidth(int $borderWidth): self
    {
        $this->borderWidth = $borderWidth;

        return $this;
    }

    /**
     * Set the value of data
     *
     * @PhpUnitGen\set("data")
     *
     * @param mixed[] $data
     */
    public function setData(array $data): self
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Set the value of hoverBackgroundColor
     *
     * @PhpUnitGen\set("hoverBackgroundColor")
     *
     * @param mixed[] $hoverBackgroundColor
     */
    public function setHoverBackgroundColor(array $hoverBackgroundColor): self
    {
        $this->hoverBackgroundColor = $hoverBackgroundColor;

        return $this;
    }
}
