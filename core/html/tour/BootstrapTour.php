<?php

namespace core\html\tour;

use core\html\AbstractHtmlElement;
use core\html\HtmlElement;
use core\html\javascript\JavascriptObjectInterface;
use core\html\javascript\WithJavascript;
use core\html\traits\Identifiable;
use core\html\traits\Namable;
use core\messages\request\HttpRequestInterface;

/**
 * Class BootstrapTour
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class BootstrapTour extends AbstractHtmlElement implements JavascriptObjectInterface
{
    use WithJavascript;
    use Identifiable;
    use Namable;

    /**
     * BootstrapTour constructor
     *
     * @param string $identifier The identifier
     * @param string $name       The name
     * @param array  $steps      The steps
     */
    public function __construct(string $identifier, string $name = 'Tour', array $steps = [])
    {
        $this->setId($identifier);
        $this->setName($name);
        foreach ($steps as $step) {
            $element = new HtmlElement($step);
            $this->appendChild($element);
        }


        //Assets::addBootstrapTour();
    }

    /**
     * Add a step
     */
    public function addStep(array $step, HttpRequestInterface $request): self
    {
        //$step["backdrop"] = true;
        //$step["backdropContainer"] = "body";
        //$step["backdropPadding"] = true;
        //$step["smartPlacement"] = true;
        //$step["animation"] = true;
        //$step["placement"] = "auto";
        //$step["autoscroll"] = true;
        $step['path'] = $request->getPage();
        $element      = new HtmlElement($step);
        $this->appendChild($element);

        return $this;
    }

    /**
     * Add a steps
     */
    public function addSteps(array $steps): self
    {
        foreach ($steps as $step) {
            $element = new HtmlElement($step);
            $this->appendChild($element);
        }

        return $this;
    }

    /**
     * Autostart tour
     */
    public function autotart(): self
    {
        $this->addConfig('autostart')->value(true);

        return $this;
    }

    /**
     * Return Id attribute for html element
     */
    public function getIdAttribute(): string
    {
        return $this->getViewReference() . '_' . $this->getId();
    }

    /**
     * Return an Array for object jsConfig
     */
    public function toArray(): array
    {
        $configArray          = parent::toArray();
        $configArray['steps'] = $this->getChilds();

        return $configArray;
    }

    /**
     * List of allowed childs elements
     * could be a class, an abstract class or an interface
     */
    protected static function getAllowedChilds(): array
    {
        return [HtmlElement::class];
    }
}
