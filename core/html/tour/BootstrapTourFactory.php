<?php

namespace core\html\tour;

use core\factory\FactoryException;
use core\html\HtmlElementFactory;
use core\messages\request\HttpRequestInterface;

/**
 * Class BootstrapTourFactory
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class BootstrapTourFactory extends HtmlElementFactory
{
    public const NAME  = 'name';
    public const STEPS = 'steps';

    /**
     * Return element class to build
     */
    protected function getElementClass(): string
    {
        return BootstrapTour::class;
    }

    /**
     * Return suffix for widget classes
     */
    protected function getWidgetSuffix(): string
    {
        return 'BootstrapTourWidget';
    }

    /**
     * Create new instance or return singleton if exist
     *
     * @PhpUnitGen\assertInstanceOf("BootstrapTour::class")
     */
    public function create(string $identifier, array $params = []): ?BootstrapTour
    {
        $params[self::NAME] ??= 'TOUR';
        $params['identifier'] ??= $identifier;
        if (isset($params[self::STEPS]) && !is_array($params[self::STEPS])) {
            throw new FactoryException('$params[\'steps\'] must be an array');
        }

        return parent::create($identifier, $params);
    }

    /**
     * Return singleton
     */
    public function getPageTour(HttpRequestInterface $request): ?BootstrapTour
    {
        return $this->create($request->getPage());
    }

    public function get(string $identifier): ?BootstrapTour
    {
        return $this->getInstance($identifier) ?? $this->create($identifier);
    }
}
