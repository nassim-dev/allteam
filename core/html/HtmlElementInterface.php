<?php

namespace core\html;

/**
 * Interface HtmlElementInterface
 *
 * Description
 *
 * @category html_component_interface
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
interface HtmlElementInterface
{
    /**
     * Append child element
     */
    public function appendChild(?HtmlElementInterface &$child, ?string $identifier = null);

    /**
     * Append element to parent
     */
    public function appendTo(HtmlElementInterface &$parent, ?string $identifier = null);

    /**
     * Return template directory
     */
    public static function getBaseTemplateDir(): string;

    /**
     * Get of HtmlElementInterface
     *
     * @PhpUnitGen\get("childs")
     */
    public function getChilds(): ?HtmlElementContainer;

    /**
     * Return html template
     */
    public function getHtmlTemplate(): string;

    /**
     * Return Id attribute for html element
     */
    public function getIdAttribute(): string;

    /**
     * Get the value of parent
     *
     * @PhpUnitGen\get("parent")
     */
    public function getParent(): ?HtmlElementInterface;

    /**
     * Get var name in view
     */
    public function getViewReference(): string;

    /**
     * Reference to retrieve component in child list
     */
    public function getReference(): ?string;

    public function hasParent(): bool;

    /**
     * Remove chidl element
     */
    public function removeChild(HtmlElementInterface $child);

    /**
     * Remove element from its parent
     */
    public function removeFrom(HtmlElementInterface $parent);

    /**
     * Set html template
     *
     * @return static
     */
    public function setHtmlTemplate(string $template);

    /**
     * Set the value of parent
     *
     * @PhpUnitGen\set("parent")
     *
     * @return static
     */
    public function setParent(?HtmlElementInterface $parent);

    /**
     * Return an object as array
     */
    public function toArray(): array;
}
