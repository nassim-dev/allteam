<?php

namespace core\html;

use core\cache\CacheServiceInterface;
use core\cache\CacheUpdaterTrait;
use core\config\Config;

/**
 * Class Includes
 *
 * Description
 *
 * @category service
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Assets implements AssetsServiceInterface
{
    use CacheUpdaterTrait;

    public const ASSETS_PATH = 'dist/assets';

    public const IN_HEAD = false;

    public const IN_BODY = true;

    private array $css = ['body' => [], 'head' => []];

    private array $js = ['body' => [], 'head' => []];

    private ?array $manifest = null;

    public function __construct(private Config $config, private CacheServiceInterface $cache)
    {
        //$this->_fetchPropsFromCache(['manifest']);
    }

    public function __destruct()
    {
        //$this->_cacheMultiplesProps(['manifest']);
    }

    public function addCss(string $file, bool $inBody = true)
    {
        $this->css[($inBody) ? 'body' : 'head'][] = $this->getFileInfos($file);
    }

    public function addCssInline(string $style, bool $inBody = true)
    {
        $this->css[($inBody) ? 'body' : 'head'][] = $style;
    }

    public function addJs(string $file, bool $inBody = true)
    {
        $this->js[($inBody) ? 'body' : 'head'][] = $this->getFileInfos($file);
    }

    public function addJsInline(string $script, bool $inBody = true)
    {
        $this->js[($inBody) ? 'body' : 'head'][] = $script;
    }

    public function addMainAssets()
    {
        if ($this->config->find('ENVIRONNEMENT') === 'development') {
            $script = <<< HTML
            <script crossorigin type="module" src="https://localhost:3000/assets/@vite/client"></script>
            <script crossorigin type="module">
                import RefreshRuntime from "https://localhost:3000/assets/@react-refresh"
                RefreshRuntime.injectIntoGlobalHook(window)
                window.\$RefreshReg$ = () => {}
                window.\$RefreshSig$ = () => (type) => type
                window.__vite_plugin_react_preamble_installed__ = true
            </script>
HTML;
            $this->addJsInline($script, self::IN_HEAD);

            $script = <<< HTML
            <script type="module" src="https://localhost:3000/assets/index.js"></script>
HTML;
            $this->addJsInline($script, self::IN_BODY);
        }

        if ($this->config->find('ENVIRONNEMENT') === 'production') {
            $manifest = $this->getManifest();
            $indexJs  = $manifest['index.js']['file'];
            $script   = <<< HTML
            <script type="module" src="/assets/{$indexJs}"></script>
HTML;
            $this->addJsInline($script, self::IN_BODY);

            if (isset($manifest['index.js']['css'])) {
                foreach ($manifest['index.js']['css'] as $css) {
                    $this->addCss('/assets/' . $css, self::IN_HEAD);
                }
            }
        }
    }

    /**
     * Return all assets concatenate for body tag
     */
    public function getBody(): string
    {
        return $this->getHtmlTagsFor('body');
    }

    /**
     * Return all assets concatenate for head tag
     */
    public function getHead(): string
    {
        return $this->getHtmlTagsFor('head');
    }

    /**
     * Get the value of css
     *
     * @PhpUnitGen\get("css")
     */
    public function getCss(): array
    {
        return $this->css;
    }

    /**
     * Get the value of js
     *
     * @PhpUnitGen\get("js")
     */
    public function getJs(): array
    {
        return $this->js;
    }

    private function getFileInfos(string $file)
    {
        if (!file_exists(BASE_DIR . 'public' . $file)) {
            throw new AssetException('Innexisting file ' . BASE_DIR . 'public ' . $file, 1);
        }

        $version = $this->config->find('VERSION');

        return [
            'key'       => $file,
            'realpath'  => realpath($file) . "?v=$version",
            'directory' => dirname($file)
        ];
    }


    private function getHtmlTagsFor(string $tag): string
    {
        $return = '';

        foreach ($this->css[$tag] as $css) {
            if (str_contains($css['key'], '<')) {
                $return .= $css['key'];

                continue;
            }

            $return .= '<link rel="stylesheet" type="text/css" href="' . $css['key'] . '">';
        }

        foreach ($this->js[$tag] as $js) {
            if (str_contains($js, '<')) {
                $return .= $js;

                continue;
            }

            $return .= '<script src="' . $js . '"></script>';
        }

        return $return;
    }


    /**
     * Get manifest
     */
    private function getManifest(): array
    {
        if (!is_array($this->manifest)) {
            $fileName       = /*$this->config->find('ASSETS.MANIFEST') ??*/ BASE_DIR . 'public/assets/manifest.json';
            $this->manifest = json_decode(file_get_contents($fileName), true, 512, JSON_THROW_ON_ERROR);
        }

        return $this->manifest;
    }
}
