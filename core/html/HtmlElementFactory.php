<?php

namespace core\html;

use core\cache\CacheServiceInterface;
use core\cache\CacheUpdaterTrait;
use core\containers\ContainerInterface;
use core\factory\AbstractFactory;
use core\factory\FactoryException;
use core\nullObjects\NullObject;
use core\secure\authentification\AuthServiceInterface;
use core\secure\components\RessourceInterface;
use core\utils\Utils;

/**
 * Class HtmlElementFactory
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
abstract class HtmlElementFactory extends AbstractFactory
{
    use CacheUpdaterTrait;

    /**
     * Undocumented function
     */
    public function __construct(
        protected AuthServiceInterface $auth,
        protected CacheServiceInterface $cache,
        array $namespaces = []
    ) {
        $this->_fetchPropsFromCache(['_namespaces']);

        if (!is_array($this->_namespaces) || empty($this->_namespaces)) {
            $this->_namespaces = [];
            $namespaces        = array_merge(['app\widgets'], $namespaces);

            foreach ($namespaces as $namespace) {
                $this->addNamespace($namespace);
            }
        }
    }

    public function __destruct()
    {
        $this->_cacheMultiplesProps(['_namespaces']);
    }

    final public function addNamespace(string $namespace): self
    {
        $widgetDirectory = strtolower(str_replace('Widget', '', $this->getWidgetSuffix()));

        return parent::addNamespace($namespace . '\\' . $widgetDirectory);
    }

    /**
     * Create new instance or return singleton if exist
     */
    public function create(string $identifier, array $params = []): ?HtmlElementInterface
    {
        $method     = $params[0] ?? $params['method'] ?? null;
        $method     = (!is_array($method)) ? $method : null;
        $identifier = $identifier . '_' . $method . Utils::hash($params);
        $identifier = strtolower($identifier);

        $instance = $this->getInstance($identifier);
        if (null === $instance) {
            /** @var RessourceInterface $instance  */
            $instance = $this->getDi()->make($this->getElementClass(), $params);
            $instance::buildDefaultPermissions($instance->getPermissionSet());
        }

        /** @var RessourceInterface $instance */
        if (!$this->auth->getPermissionManager()->isAllowed($instance, $this->auth->getUser())) {
            if ($this->getInstances()->contains($instance)) {
                $this->getInstances()->detach($instance);
            }

            return null;
        }

        return $this->register($instance, $identifier);
    }

    public function createThen(string $identifier, callable $callback, array $params = [])
    {
        $instance = $this->create($identifier, $params);

        if ($instance !== null) {
            //$this->di->callable($callback, ['instance' => $instance]);
            $callback($instance);

            return $instance;
        }

        return null;
    }

    /**
     * Create from WidgetInterface
     */
    public function createFromWidget(string $className, array $params): ?HtmlElementInterface
    {
        $widgetClass = $this->_getClass($className);

        $method     = $params[0] ?? $params['method'] ?? 'create';
        $identifier = $widgetClass . '_' . $method . Utils::hash($params);
        $identifier = strtolower($identifier);

        $instance = $this->getInstance($identifier);

        if (null !== $instance) {
            return $instance;
        }

        $widgetClass = $this->getDi()->singleton($widgetClass, $params);

        if (!method_exists($widgetClass, $method)) {
            throw new FactoryException("Innexisting method '$method', in " . $widgetClass::class);
        }

        $object = NullObject::get();

        $instance = $this->getDi()->call($widgetClass, $method, ['object' => $object, 'arguments' => $widgetClass->getParams()]);

        return $this->register($instance, $identifier);
    }

    /**
     * Get all instances or filtered by callback
     *
     * @PhpUnitGen\get("instances")
     *
     * @return HtmlElementContainer
     */
    public function getInstances(?callable $filter = null): ContainerInterface
    {
        $this->instances ??= new HtmlElementContainer();
        if (!null === $filter) {
            $entries = array_filter($this->instances->toArray(), $filter);

            return new HtmlElementContainer($entries);
        }

        return $this->instances;
    }

    /**
     * Return element class to build
     */
    abstract protected function getElementClass(): string;

    /**
     * Return suffix for widget classes
     */
    abstract protected function getWidgetSuffix(): string;

    /**
     * Get class name if exist ot throw error
     *
     * @throws FactoryException
     */
    private function _getClass(string $identifier): string
    {
        if (class_exists($identifier)) {
            return $identifier;
        }

        foreach ($this->getNamespaces() as $namespace) {
            $defaultIdentifier = $namespace . '\\' . ucfirst($identifier) . $this->getWidgetSuffix();
            if (class_exists($defaultIdentifier)) {
                return $defaultIdentifier;
            }
        }

        throw new FactoryException("$identifier can not be instanciate, because it's not a class");
    }
}
