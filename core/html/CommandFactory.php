<?php

namespace core\html;

use core\factory\AbstractFactory;
use core\html\javascript\JavascriptCommandInterface;
use core\utils\Utils;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class CommandFactory extends AbstractFactory
{
    private array $commandFiles = [];

    /**
     * Create new instance or return singleton if exist
     *
     * @PhpUnitGen\assertInstanceOf("JavascriptCommandInterface::class")
     *
     * @param string $identifier
     */
    public function create(string $command, array $params = []): ?JavascriptCommandInterface
    {
        $method     = $params[0] ?? $params['method'] ?? null;
        $method     = (!is_array($method)) ? $method : null;
        $identifier = $command . Utils::hash($params);

        $instance = $this->getInstance($identifier);
        if (null === $instance) {
            /** @var JavascriptCommandInterface $instance */
            $instance = $this->getDi()->make($command, $params);

            if (!in_array($instance->get(), $this->commandFiles)) {
                $baseName = explode('\\', $instance::class);

                $this->commandFiles[$instance->getContext()][end($baseName)] = $instance->get();
            }
        }

        return $this->register($instance, $identifier);
    }

    /**
     * /**
     * Get registred elements
     */
    public function get(string $identifier): ?JavascriptCommandInterface
    {
        return $this->getInstance($identifier) ?? $this->create($identifier);
    }

    /**
     * Get the value of commandFiles
     *
     * @PhpUnitGen\get("commandFiles")
     *
     * @return mixed
     */
    public function getCommandFiles(): array
    {
        return $this->commandFiles;
    }
}
