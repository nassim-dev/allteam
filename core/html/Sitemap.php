<?php

namespace core\html;

use XMLWriter;

/**
 * Sitemap
 *
 * This class used for generating Google Sitemap files
 *
 * @package    Sitemap
 *
 * @author     Osman Üngür <osmanungur@gmail.com>
 * @copyright  2009-2015 Osman Üngür
 * @copyright  2012-2015 Evert Pot (http://evertpot.com/)
 * @license    http://opensource.org/licenses/MIT MIT License
 *
 * @link       http://github.com/evert/sitemap-php
 */
class Sitemap
{
    public const DEFAULT_PRIORITY = 0.5;

    public const EXT = '.xml';

    public const INDEX_SUFFIX = 'index';

    public const ITEM_PER_SITEMAP = 50000;

    public const SCHEMA = 'http://www.sitemaps.org/schemas/sitemap/0.9';

    public const SEPERATOR = '-';

    private int $current_item = 0;

    private int $current_sitemap = 0;

    private mixed $domain = null;

    private string $filename = 'sitemap';

    private mixed $path = null;

    private ?\XMLWriter $writer = null;

    /**
     * @param string $domain
     */
    public function __construct($domain)
    {
        $this->setDomain($domain);
    }

    /**
     * Adds an item to sitemap
     *
     * @param string          $loc        URL of the page. This value must be less than 2,048 characters.
     * @param string|null     $priority   The priority of this URL relative to other URLs on your site. Valid values range from 0.0 to 1.0.
     * @param string|null     $changefreq How frequently the page is likely to change. Valid values are always, hourly, daily, weekly, monthly, yearly and never.
     * @param string|int|null $lastmod    The date of last modification of url. Unix timestamp or any English textual datetime description.
     */
    public function addItem($loc, $priority = self::DEFAULT_PRIORITY, $changefreq = null, $lastmod = null): self
    {
        if (($this->getCurrentItem() % self::ITEM_PER_SITEMAP) === 0) {
            if ($this->getWriter() instanceof XMLWriter) {
                $this->endSitemap();
            }

            $this->startSitemap();
            $this->incCurrentSitemap();
        }

        $this->incCurrentItem();
        $this->getWriter()->startElement('url');
        $this->getWriter()->writeElement('loc', $this->getDomain() . $loc);
        if (null !== $priority) {
            $this->getWriter()->writeElement('priority', $priority);
        }

        if ($changefreq) {
            $this->getWriter()->writeElement('changefreq', $changefreq);
        }

        if ($lastmod) {
            $this->getWriter()->writeElement('lastmod', $this->getLastModifiedDate($lastmod));
        }

        $this->getWriter()->endElement();

        return $this;
    }

    /**
     * Writes Google sitemap index for generated sitemap files
     *
     * @param string     $loc     Accessible URL path of sitemaps
     * @param string|int $lastmod The date of last modification of sitemap. Unix timestamp or any English textual datetime description.
     */
    public function createSitemapIndex($loc, int|string $lastmod = 'Today')
    {
        $this->endSitemap();
        $indexwriter = new XMLWriter();
        $indexwriter->openURI($this->getPath() . $this->getFilename() . self::SEPERATOR . self::INDEX_SUFFIX . self::EXT);
        $indexwriter->startDocument('1.0', 'UTF-8');
        $indexwriter->setIndent(true);
        $indexwriter->startElement('sitemapindex');
        $indexwriter->writeAttribute('xmlns', self::SCHEMA);
        for ($index = 0; $index < $this->getCurrentSitemap(); $index++) {
            $indexwriter->startElement('sitemap');
            $indexwriter->writeElement('loc', $loc . $this->getFilename() . ($index ? self::SEPERATOR . $index : '') . self::EXT);
            $indexwriter->writeElement('lastmod', $this->getLastModifiedDate($lastmod));
            $indexwriter->endElement();
        }

        $indexwriter->endElement();
        $indexwriter->endDocument();
    }

    /**
     * Sets root path of the website, starting with http:// or https://
     */
    public function setDomain(string $domain): self
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * Sets filename of sitemap file
     */
    public function setFilename(string $filename): self
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Sets paths of sitemaps
     */
    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Finalizes tags of sitemap XML document.
     */
    private function endSitemap()
    {
        if (!$this->getWriter()) {
            $this->startSitemap();
        }

        $this->getWriter()->endElement();
        $this->getWriter()->endDocument();
    }

    /**
     * Returns current item count
     */
    private function getCurrentItem(): int
    {
        return $this->current_item;
    }

    /**
     * Returns current sitemap file count
     */
    private function getCurrentSitemap(): int
    {
        return $this->current_sitemap;
    }

    /**
     * Returns root path of the website
     */
    private function getDomain(): string
    {
        return $this->domain;
    }

    /**
     * Returns filename of sitemap file
     */
    private function getFilename(): string
    {
        return $this->filename;
    }

    /**
     * Prepares given date for sitemap
     *
     * @param string $date Unix timestamp or any English textual datetime description
     *
     * @return string Year-Month-Day formatted date.
     */
    private function getLastModifiedDate(string $date): string
    {
        if (ctype_digit($date)) {
            return date('Y-m-d', $date);
        } else {
            $date = strtotime($date);

            return date('Y-m-d', $date);
        }
    }

    /**
     * Returns path of sitemaps
     */
    private function getPath(): string
    {
        return $this->path;
    }

    /**
     * Returns XMLWriter object instance
     */
    private function getWriter(): XMLWriter
    {
        return $this->writer;
    }

    /**
     * Increases item counter
     */
    private function incCurrentItem()
    {
        $this->current_item += 1;
    }

    /**
     * Increases sitemap file count
     */
    private function incCurrentSitemap()
    {
        $this->current_sitemap += 1;
    }

    /**
     * Assigns XMLWriter object instance
     */
    private function setWriter(XMLWriter $writer)
    {
        $this->writer = $writer;
    }

    /**
     * Prepares sitemap XML document
     */
    private function startSitemap()
    {
        $this->setWriter(new XMLWriter());
        if ($this->getCurrentSitemap()) {
            $this->getWriter()->openURI($this->getPath() . $this->getFilename() . self::SEPERATOR . $this->getCurrentSitemap() . self::EXT);
        } else {
            $this->getWriter()->openURI($this->getPath() . $this->getFilename() . self::EXT);
        }

        $this->getWriter()->startDocument('1.0', 'UTF-8');
        $this->getWriter()->setIndent(true);
        $this->getWriter()->startElement('urlset');
        $this->getWriter()->writeAttribute('xmlns', self::SCHEMA);
    }
}
