<?php

namespace core\html\calendar;

use core\config\Conf;
use core\services\pdf\PhpSpreadsheetToPdf;
use core\utils\Utils;
use Nette\Utils\Arrays;
use Nette\Utils\DateTime;
use PhpOffice\PhpSpreadsheet\RichText\RichText;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpWord\Element\Section;
use PhpOffice\PhpWord\PhpWord;

/**
 * Trait CalendarToDoc
 *
 * Transform calendar to be injected in .doc file
 *
 * @category Transform calendar to be injected in .doc file
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
trait CalendarToDoc
{
    /**
     * Registred columns
     *
     * @var array
     */
    public static $columnsRefs = [];

    /**
     * Time intervals
     *
     * @var array
     */
    public static $rowsY = [];

    /**
     * Fecteched Events
     *
     * @var array
     */
    private $events = [];

    /**
     * First Event of serie
     *
     * @var Event
     */
    private $firstEvent = null;

    /**
     * Represent Spreadsheet grid
     *
     * @var array
     */
    private $matrice = [];

    /**
     * Table style options
     *
     * @var array
     */
    private $options = [];

    /**
     * PhpWord Object
     *
     * @var PhpWord
     */
    private $phpWord = null;

    /**
     * Section object
     *
     * @var Section
     */
    private $section = null;

    /**
     * Table object
     *
     * @var Spreadsheet
     */
    private $spreadsheet = null;

    /**
     * Init Element
     *
     * @return static
     */
    public function initDocument(Section $section, PhpWord $phpWord)
    {
        $this->phpword = $phpWord;
        $this->section = $section;
        $this->section->addTitle($this->getName(), 2);
        $this->section->addTextBreak(1);
        $this->spreadsheet = new Spreadsheet();
        $this->spreadsheet->getDefaultStyle()->getAlignment()->setWrapText(true);

        //Get Events form API
        $this->events = $this->_getEvents();

        //Init Matrice
        $this->_setYRows();
        $this->_setXRows();
        $this->_dispatchEvents();
        $this->_clearMatrice();
        $this->_appendNewColumns();

        //Convert to spreasheet
        $this->_matriceToSpreasheet();

        //Save as file
        $writer = new Xls($this->spreadsheet);
        $writer->save(BASE_DIR . Conf::find('PHP.ROOT_PUBLIC_FILES') . 'context_' . $this->auth->getContext()?->getIdcontext() . '/route/calendar.' . Utils::hash($this->getName()) . '.xls');

        $pdf = new PhpSpreadsheetToPdf($this->spreadsheet);
        $pdf->render(BASE_DIR . Conf::find('PHP.ROOT_PUBLIC_FILES') . 'context_' . $this->auth->getContext()?->getIdcontext() . '/route/calendar.' . Utils::hash($this->getName()) . '.pdf');

        return $this;
    }

    /**
     * Append new columns if multiple events en same interval
     *
     * @return void
     */
    private function _appendNewColumns()
    {
        $count = count($this->matrice);
        $x     = 1;

        while ($x <= $count) {
            if (isset($this->matrice[$x]) && $this->matrice[$x][0]['colspan'] >= 2) {
                //Copy current contents
                $inserted  = array_replace([], $this->matrice[$x]);
                $lastIndex = $x + $this->matrice[$x][0]['colspan'] - 1;

                //Insert New columns
                while ($x !== $lastIndex) {
                    $this->matrice = Arrays::insertAfter($this->matrice, $x, [$inserted]);
                    $count         = count($this->matrice);
                    ++$x;
                }
            }

            ++$x;
        }
    }

    /**
     * Clear empty matrice row and columns
     *
     * @return void
     */
    private function _clearMatrice()
    {
        foreach ($this->matrice as $x => $values) {
            //Remove empty elements
            if ($x > 0) {
                foreach ($values as $y => $elements) {
                    if (!isset($elements['data']) && $y > 0) {
                        unset($this->matrice[$x][$y]);
                    }
                }
            }

            //Remove empty dates
            if (count($this->matrice[$x]) <= 1) {
                unset($this->matrice[$x]);
            }
        }

        $this->matrice = array_values($this->matrice);
    }

    /**
     * Place Events on matrice
     *
     * @return void
     */
    private function _dispatchEvents()
    {
        foreach ($this->events as $event) {
            $startTime = $event->getStart()->format('H:i');
            $endTime   = $event->getEnd()->format('H:i');
            $startDate = $event->getStart()->format('d/m/Y');
            $endDate   = $event->getEnd()->format('d/m/Y');

            $x       = 1;
            $y       = 1;
            $rowspan = [];

            while ($x < 367) {
                if ($this->matrice[$x][0]['date'] >= $startDate && $this->matrice[$x][0]['date'] <= $endDate) {
                    while ($y < 99) {
                        if ($this->matrice[0][$y]['date'] >= $startTime && $this->matrice[0][$y]['date'] <= $endTime) {
                            if (!isset($rowspan[$event->getIdevent()])) {
                                //Record event object

                                $rowspan[$event->getIdevent()]                           = ['value' => 1, 'firstRow' => ['x' => $x, 'y' => $y + 1]];
                                $this->matrice[$x][$y + 1]['data'][$event->getIdevent()] = ['event' => $event, 'first' => true, 'rowspan' => $rowspan[$event->getIdevent()]['value']];
                            } else {
                                //Define number of row for event
                                $this->matrice[$x][$y]['data'][$event->getIdevent()] = ['event' => 'REGISTRED_EVENT'];

                                $xCoordonate = $rowspan[$event->getIdevent()]['firstRow']['x'];
                                $yCoordonate = $rowspan[$event->getIdevent()]['firstRow']['y'];
                                ++$rowspan[$event->getIdevent()]['value'];

                                if ($endTime > $this->matrice[0][$y]['date']) {
                                    $this->matrice[$x][$y + 1]['data'][$event->getIdevent()] = ['event' => 'REGISTRED_EVENT'];
                                    ++$rowspan[$event->getIdevent()]['value'];
                                }

                                $this->matrice[$xCoordonate][$yCoordonate]['data'][$event->getIdevent()]['rowspan'] = $rowspan[$event->getIdevent()]['value'];
                            }

                            //Define number of column for header

                            if (isset($this->matrice[$x][$y])) {
                                $numberColumn = count($this->matrice[$x][$y]['data']);
                                if ($numberColumn > $this->matrice[$x][0]['colspan']) {
                                    $this->matrice[$x][0]['colspan'] = $numberColumn;
                                }
                            }

                            //Define column number from $x
                            if (isset($this->matrice[$x][$y]['data'][$event->getIdevent()]['event'])) {
                                $this->matrice[$x][$y]['data'][$event->getIdevent()]['column']     = $numberColumn;
                                $this->matrice[$x][$y - 1]['data'][$event->getIdevent()]['column'] = $numberColumn;
                            }
                        }

                        ++$y;
                    }
                }

                ++$x;
            }
        }
    }

    /**
     * Explode event in multiple event if more than 1 day
     *
     * @param Event $event
     *
     * @return void
     */
    private function _explodeEvent($event)
    {
        //Create DateTime Objects
        $endTime   = DateTime::from($event->getEnd())->format('H:i');
        $startDate = DateTime::from($event->getStart());
        $endDate   = DateTime::from($event->getEnd())->format('d/m/Y');
        $event->setStart(DateTime::from($event->getStart()));
        $event->setEnd(DateTime::from($event->getEnd()));

        //Check interval between end and start
        $interval = date_diff($event->getStart(), $event->getEnd());
        $dayDiff  = $interval->format('%a');

        if ($dayDiff > 0) {
            $formatDate = clone $startDate;

            $event->setEnd(DateTime::createFromFormat('d/m/Y H:i', $formatDate->format('d/m/Y') . ' 23:59'));

            $this->events[] = $event;

            //Generate events on other days
            while ($dayDiff > 0) {
                $eventTmp = clone $event;
                $eventTmp->setIdevent(uniqid());
                $startDate->modify('+1day');

                $formatDate = clone $startDate;
                $formatDate->format('d/m/Y');

                $eventTmp->setStart(DateTime::createFromFormat('d/m/Y H:i', $formatDate->format('d/m/Y') . ' 00:01'));
                $eventTmp->setEnd(DateTime::createFromFormat('d/m/Y H:i', $formatDate->format('d/m/Y') . ' 23:59'));

                --$dayDiff;

                if (0 === $dayDiff) {
                    $eventTmp->setEnd(DateTime::createFromFormat('d/m/Y H:i', $formatDate->format('d/m/Y') . ' ' . $endTime));
                }

                $this->events[] = $eventTmp;
            }
        } else {
            $this->events[] = $event;
        }

        return $this->events;
    }

    /**
     * Return column letter from number
     *
     * @param string $column
     */
    private static function _getColumnLetter($column): string
    {
        $character = '';
        while ($column >= 0) {
            $character = chr($column % 26 + 65) . $character;
            $column    = floor($column / 26) - 1;
        }

        return $character;
    }

    /**
     * Get Events from API
     */
    private function _getEvents(): array
    {
        $jsConfig = $this->toArray();
        foreach ($jsConfig['config']['urls']['parameters'] as $url) {
            $tmp      = explode('/', $url);
            $elements = explode('-', $tmp[3]);
            $params   = ['idperformance' => $elements[2]];
            if ($elements[1] > 0) {
                $params['idtype'] = $elements[1];
            }

            $eventRows = $repositoryFactory->table('event')->getByParams($params);
            foreach ($eventRows as $event) {
                $this->_explodeEvent($event);

                if (null === $this->firstEvent) {
                    $this->firstEvent = $event->start;
                }
            }
        }

        return $this->events;
    }

    /**
     * Return RichText Object
     *
     * @param mixed $data
     * @param array $options
     *
     * @return RichText
     */
    private function _getRichText($data, $options = ['line' => 0, 'column' => 'A']): RichText
    {
        $richtext = new RichText();
        $content  = $richtext->createTextRun($data);
        $content->getFont()->setSize(18);
        if (0 === $options['line'] || 'A' === $options['column']) {
            $content->getFont()->setBold(true);
            $content->getFont()->setSize(14);
        }

        return $richtext;
    }

    /**
     * Transform matrice to fill spreasheet
     *
     * @return void
     */
    private function _matriceToSpreasheet()
    {
        $lastColumn   = null;
        $rendedEvents = [];
        $rendedDates  = [];
        $activeSheet  = $this->spreadsheet->getActiveSheet();

        foreach ($this->matrice as $key => $columnsDatas) {
            $column     = self::_getColumnLetter($key);
            $lastColumn = $column;

            $activeSheet->getColumnDimension($column)->setAutoSize(true);

            foreach ($columnsDatas as $line => $values) {
                $realLine   = $line + 1;
                $lastColumn = $column;

                if (isset($values['data'])) {
                    foreach ($values['data'] as $element) {
                        if (isset($element['event']) && 'REGISTRED_EVENT' !== $element['event'] && !in_array($element['event']->getIdevent(), $rendedEvents)) {
                            $data           = $element['event']->getTitle();
                            $rendedEvents[] = $element['event']->getIdevent();

                            //Transform column in letter
                            if ($element['column'] >= 2) {
                                $column     = self::_getColumnLetter($key + $element['column'] - 1);
                                $lastColumn = $column;
                            }

                            //Format Contents
                            $richtext = $this->_getRichText($data, ['line' => $line, 'column' => $column]);
                            $activeSheet->setCellValue($column . $realLine, $richtext);
                            $this->_setStyleForEvent($column . $realLine, $element['event']);

                            //Merge rows
                            if (isset($element['rowspan']) && $element['rowspan'] >= 2) {
                                $finalLine = $realLine + $element['rowspan'] - 1;
                                $activeSheet->mergeCells($column . $realLine . ':' . $column . $finalLine);
                            }
                        }
                    }
                } elseif (isset($values['date'])) {
                    if (!in_array($values['date'], $rendedDates)) {
                        $data          = $values['date'];
                        $rendedDates[] = $values['date'];

                        //Format Contents
                        $richtext = $this->_getRichText($data, ['line' => $line, 'column' => $column]);
                        $activeSheet->setCellValue($column . $realLine, $richtext);
                        $this->_setBorders($column . $realLine);

                        //Merge Title Row
                        if (isset($values['colspan']) && $values['colspan'] >= 2) {
                            $columnMerged = self::_getColumnLetter($key + $values['colspan'] - 1);
                            $lastColumn   = $columnMerged;
                            $activeSheet->mergeCells($column . $realLine . ':' . $columnMerged . $realLine);
                        }

                        if (isset($values['rowspan'])) {
                            $finalLine = $realLine + 1;

                            $activeSheet->mergeCells($column . $realLine . ':' . $column . $finalLine);
                        }
                    }
                } else {
                    //Format Contents
                    $richtext = $this->_getRichText($values, ['line' => $line, 'column' => $column]);
                    $activeSheet->setCellValue($column . $realLine, $richtext);
                    $this->_setBorders($column . $realLine);
                }
            }
        }

        $activeSheet->getColumnDimension('A')->setWidth(250);
        $activeSheet->getRowDimension('1')->setRowHeight(30);
        $this->_setPrintArea(['from' => 'A1', 'to' => $lastColumn . '97']);
    }

    /**
     * Activate Borders && Center alignment
     *
     * @param string $colum
     *
     * @return void
     */
    private function _setBorders($colum, $bstyle = Border::BORDER_THIN, $color = null)
    {
        $borders = ['borderStyle' => $bstyle];
        if (!null === $color) {
            $borders['color'] = ['rgb' => $color];
        }

        $style = $this->spreadsheet->getActiveSheet()->getStyle($colum);

        $style->getAlignment()->setVertical(Alignment::VERTICAL_CENTER)->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $style->getBorders()->applyFromArray([
            'bottom' => $borders,
            'top'    => $borders,
            'left'   => $borders,
            'right'  => $borders
        ]);
    }

    /**
     * Configure Print area
     *
     * @return void
     */
    private function _setPrintArea(array $options)
    {
        $worksheet = $this->spreadsheet->getActiveSheet();
        $worksheet->getPageSetup()->setPrintArea($options['from'] . ':' . $options['to']);
        $worksheet->freezePane('B2');
    }

    /**
     * Generate Row Style
     *
     * @param string $colum
     * @param Event  $event
     *
     * @return void
     */
    private function _setStyleForEvent($colum, $event)
    {
        $color = ltrim($event->getColor(), '#');
        $style = $this->spreadsheet->getActiveSheet()->getStyle($colum);
        $this->_setBorders($colum, $color, Border::BORDER_THICK);
        $style->getFont()->getColor()->setRGB($color);
    }

    /**
     * Get Date rows intervals
     */
    private function _setXRows(): array
    {
        $start = DateTime::from($this->firstEvent)->modify('-160days');
        $end   = $start->modifyClone('+365days');

        $i = 1;

        while ($start <= $end) {
            $date                   = clone $start;
            $this->matrice[$i++][0] = ['date' => $date->format('d/m/Y'), 'colspan' => 1];
            $start->modify('+1day');
        }

        return $this->matrice;
    }

    /**
     * Get Hour rows intervals
     */
    private function _setYRows(): array
    {
        $start = DateTime::from('00:00:00');
        $end   = $start->modifyClone('+1 day');

        $this->matrice[0][0] = _('Hour');

        $i = 1;

        while ($start <= $end) {
            $date = clone $start;

            $this->matrice[0][$i++] = ['date' => $date->format('H:i'), 'rowspan' => true];
            $this->matrice[0][$i++] = ['date' => ''];
            $start->modify('+30minutes');
        }

        return $this->matrice;
    }
}
