<?php

namespace core\html\calendar;

use core\factory\FactoryInterface;
use core\html\HtmlElementFactory;

/**
 * Class CalendarFactory
 *
 * Calendar Factory
 *
 * @category  Factory
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class CalendarFactory extends HtmlElementFactory implements FactoryInterface
{
    public const DATAID = 'dataId';

    public const NAME = 'name';

    /**
     * Return element class to build
     */
    protected function getElementClass(): string
    {
        return Calendar::class;
    }

    /**
     * Return suffix for widget classes
     */
    protected function getWidgetSuffix(): string
    {
        return 'CalendarWidget';
    }

    /**
     * Create new instance or return singleton if exist
     *
     * @PhpUnitGen\assertInstanceOf("Calendar::class")
     */
    public function create(string $identifier, array $params = []): ?Calendar
    {
        $params[self::NAME] ??= 'Planning';
        $params['identifier'] ??= $identifier;
        $params[self::DATAID] ??= '0';

        return parent::create($identifier, $params);
    }

    /**
     * Get registred elements
     */
    public function get(string $identifier): ?Calendar
    {
        return $this->getInstance($identifier) ?? $this->create($identifier);
    }
}
