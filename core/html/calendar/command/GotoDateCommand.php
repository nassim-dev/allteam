<?php

namespace core\html\calendar\command;

use core\html\calendar\Calendar;
use core\html\javascript\command\AbstractCommand;
use Nette\Utils\DateTime;

/**
 * Class GotoDateCommand
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class GotoDateCommand extends AbstractCommand
{
    public function __construct(public Calendar $target, public DateTime $date, public array $parameters = [])
    {
    }

    public function get(): string
    {
        return 'components/calendar/GotoDateCommand';
    }

    public function getContext(): string
    {
        return 't-calendar';
    }
}
