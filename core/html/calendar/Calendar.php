<?php

namespace core\html\calendar;

use core\html\AbstractHtmlElement;
use core\html\button\Button;
use core\html\form\elements\interfaces\FieldInterface;
use core\html\form\elements\MultiselectElement;
use core\html\form\Form;
use core\html\javascript\JavascriptObjectInterface;
use core\html\javascript\WithJavascript;
use core\html\traits\AjaxSourced;
use core\html\traits\Collapsable;
use core\html\traits\Exportable;
use core\html\traits\Identifiable;
use core\html\traits\Namable;
use core\html\traits\Titleable;
use core\html\traits\Widgetable;
use core\html\traits\WithActions;

/**
 * Class for calendar.
 */
class Calendar extends AbstractHtmlElement implements JavascriptObjectInterface
{
    use WithJavascript;
    use CalendarToDoc;
    use Namable;
    use Identifiable;
    use WithActions;
    use Widgetable;
    use Exportable;
    use Titleable;
    use Collapsable;
    use AjaxSourced;

    /**
     * Api Url Filters parameters
     */
    private ?array $filters = null;

    /**
     * Selector field
     */
    private ?Form $selector = null;

    /**
     * @param string $identifier The identifier
     * @param string $name       The name
     * @param string $dataid     The dataid
     */
    public function __construct(string $identifier, string $name = 'Planning', private string $dataid = '0')
    {
        $this->setId($identifier);
        $this->setName($name);
        $this->addDetachButton();
    }

    /**
     * Add New Button to Bar
     */
    public function addButton(string $action, string $contentCheck): Button
    {
        return $this->getActionBar()->addButton($action);

        // return ($this->auth->hasRights($contentCheck)) ? $button->enable() : $button->disable();
    }

    /**
     * Add new selector in header
     */
    public function addSelector(string $type = MultiselectElement::class): FieldInterface
    {
        $actionBar = $this->getActionBar();

        return $actionBar->addField(uniqid(), $type);
    }

    /**
     * @return string
     */
    public function getDataId()
    {
        return $this->dataid;
    }

    /**
     * Get api url filters
     */
    public function getFilters(): ?array
    {
        return $this->filters;
    }

    /**
     * Return Id attribute for html element
     */
    public function getIdAttribute(): string
    {
        return $this->getViewReference() . '_' . $this->getId();
    }

    /**
     * Get the value of _selector
     */
    public function getSelector()
    {
        return $this->selector;
    }

    /**
     * Remove Button from Bar
     *
     * @return void
     */
    public function removeButton(string $action)
    {
        $this->getActionBar()->removeAction($action, 'Button');
    }

    /**
     * @param $var
     */
    public function setDataId($var)
    {
        $this->dataid = $var;
    }

    /**
     * Set api url filters
     *
     * @return void
     * @param  mixed[]|null $filters
     */
    public function setFilters(?array $filters): self
    {
        $this->filters = $filters;

        return $this;
    }

    /**
     * Return an Array for object jsConfig
     */
    public function toArray(): array
    {
        $configArray            = parent::toArray();
        $configArray['filters'] = $this->getFilters();

        return $configArray;
    }

    /**
     * List of allowed childs elements
     * could be a class, an abstract class or an interface
     */
    protected static function getAllowedChilds(): array
    {
        return [];
    }
}
