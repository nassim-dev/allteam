<?php

namespace core\html\calendar;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class CalendarHelper
{
    /**
     * Javascript function for day click
     */
    public static function getDayClickAsFormElementEvent(array $modalForm): string
    {
        $ucAdd  = ucfirst($modalForm[0]);
        $ucEdit = ucfirst($modalForm[1]);

        return <<< JS

        var element = jsConfigObj.getElement("range_form_{$modalForm[0]}");
        if(typeof element !== "undefined"){
            var promiseMain = new Promise(function (resolve) {
                                        if (!element.constructed) {
                                            element.instance();
                                        }
                                        resolve(true);
                                    });
            promiseMain.then(function (res) {
                if (typeof element.startPicker !== "undefined") {
                    element.startPicker.val(moment(bindParameters.start).format("DD/MM/YYYY HH:mm"));
                }

                if (typeof element.endPicker !== "undefined") {
                    element.endPicker.val(moment(bindParameters.end).format("DD/MM/YYYY HH:mm"));
                }
            });

        }
        $("#modal_{$modalForm[0]}").modal('show');
        if (typeof bindParameters.resource !== "undefined") {
            $("#idroom_form_{$modalForm[0]}").val(bindParameters.resource.id);
        }
        $("#modal_{$modalForm[0]}").modal('show');
        $("#table_form_{$modalForm[0]}").val("{$ucAdd}");
        $("#table_form_{$modalForm[1]}").val("{$ucEdit}");
JS;
    }

    /**
     * Javascript function for day click
     */
    public static function getDayClickEvent(string $modalForm): string
    {
        return <<< JS
        var element = jsConfigObj.getElement("range_form_{$modalForm}");
        if(typeof element !== "undefined"){
            var promiseMain = new Promise(function (resolve) {
                                        if (!element.constructed) {
                                            element.instance();
                                        }
                                        resolve(true);
                                    });
            promiseMain.then(function (res) {
                if (typeof element.startPicker !== "undefined") {
                    element.startPicker.val(moment(bindParameters.start).format("DD/MM/YYYY HH:mm"));
                }

                if (typeof element.endPicker !== "undefined") {
                    element.endPicker.val(moment(bindParameters.end).format("DD/MM/YYYY HH:mm"));
                }
        });

        }
        $("#modal_{$modalForm}").modal('show');
        if (typeof bindParameters.resource !== "undefined") {
            $("#idroom_form_{$modalForm}").val(bindParameters.resource.id);
        }
        $("#modal_{$modalForm}").modal('show');
JS;
    }

    /**
     * Javascript function for day click with popup
     */
    public static function getDayClickSwitchEvent(array $modalForm, array $params = []): string
    {
        $title     = null;
        $content   = null;
        $firstBtn  = null;
        $idplace   = null;
        $secondBtn = null;
        $thirdBtn  = null;
        extract($params);

        return <<< JS
        popupConfig = {
            title: "{$title}",
            content: "{$content}",
            animation: "zoom",
            closeAnimation: "scale",
            escapeKey: true,
            backgroundDismiss: true,
            theme: "supervan",
            scrollToPreviousElement: false,
            scrollToPreviousElementAnimate: false,
            buttons: {
                "{$firstBtn}": {
                    btnClass: " btn-light-success",
                    action: function () {

                                var element = jsConfigObj.getElement("range_form_{$modalForm[0]}");
                                if(typeof element !== "undefined"){
                                     var promiseMain = new Promise(function (resolve) {
                                        if (!element.constructed) {
                                            element.instance();
                                        }
                                        resolve(true);
                                    });
                                    promiseMain.then(function (res) {
                                        if (typeof element.startPicker !== "undefined") {
                                            element.startPicker.val(moment(bindParameters.start).format("DD/MM/YYYY HH:mm"));
                                        }

                                        if (typeof element.endPicker !== "undefined") {
                                            element.endPicker.val(moment(bindParameters.end).format("DD/MM/YYYY HH:mm"));
                                        }

                                    });
                                    if (typeof bindParameters.resource !== "undefined") {
                                        $("#idplace_form_{$modalForm[0]}").val({$idplace};)
                                    }
                                    $("#modal_{$modalForm[0]}").modal('show');
                                }

                            }
                    },
                "{$secondBtn}": {
                    btnClass: " btn-light-info",
                    action: function () {
                                var element = jsConfigObj.getElement("range_form_{$modalForm[1]}");
                                if(typeof element !== "undefined"){
                                    var promiseMain = new Promise(function (resolve) {
                                        if (!element.constructed) {
                                            element.instance();
                                        }
                                        resolve(true);
                                    });
                                    promiseMain.then(function (res) {
                                        if (typeof element.startPicker !== "undefined") {
                                            element.startPicker.val(moment(bindParameters.start).format("DD/MM/YYYY HH:mm"));
                                        }

                                        if (typeof element.endPicker !== "undefined") {
                                            element.endPicker.val(moment(bindParameters.end).format("DD/MM/YYYY HH:mm"));
                                        }

                                    });
                                    if (typeof bindParameters.resource !== "undefined") {
                                        $("#idplace_form_{$modalForm[1]}").val({$idplace};)
                                    }
                                    $("#modal_{$modalForm[1]}").modal('show');
                                 }
                            }
                },
                "{$thirdBtn}": {
                    btnClass: " btn-light-warning",
                    action: function () {
                                var element = jsConfigObj.getElement("range_form_{$modalForm[2]}");
                                if(typeof element !== "undefined"){
                                    var promiseMain = new Promise(function (resolve) {
                                        if (!element.constructed) {
                                            element.instance();
                                        }
                                        resolve(true);
                                    });
                                    promiseMain.then(function (res) {
                                        if (typeof element.startPicker !== "undefined") {
                                            element.startPicker.val(moment(bindParameters.start).format("DD/MM/YYYY HH:mm"));
                                        }

                                        if (typeof element.endPicker !== "undefined") {
                                            element.endPicker.val(moment(bindParameters.end).format("DD/MM/YYYY HH:mm"));
                                        }

                                    });
                                    $("#modal_{$modalForm[2]}").modal('show');
                                }
                            }
                },
                Cancel: {
                    btnClass: " btn-light-danger",
                    action: function () {
                    }
                }
            }
        };
        $.confirm(popupConfig);
JS;
    }

    /**
     * Javascript function for edit && resize
     */
    public static function getEditEvent(): string
    {
        return <<< JS
        var delta = (typeof bindParameters.delta === "undefined") ? bindParameters.dayDelta : bindParameters.delta;
        var params = delta.id.split("_");
        var contents = {};

        contents.id = params[1];
        contents.start = delta.start.format("DD-MM-YYYY HH:mm");
        contents.end = (delta.end) ? delta.end.format("DD-MM-YYYY HH:mm") : contents.start;
        contents.ressourceId = (typeof delta.resourceId !== "undefined") ? delta.resourceId : null;

        var url = "/api/"+params[0]+"/"+contents.id+"/resize/" + TOKEN[0].name + "-" + TOKEN[0].value;

        jsAjax.put(url, contents);
        new Promise(function (resolve) {
            var slug = slugify(url);
            document.addEventListener("ajax:" + slug + ":done", function (e)
            {
                resolve(jsAjax.getResponse(slug));
                document.removeEventListener("ajax:" + slug + ":done", doesNothing);
            });
        });

JS;
    }

    /**
     * Javascript function for render
     */
    public static function getRenderEvent(array $params = []): string
    {
        $idplace = null;
        $iduser  = null;
        extract($params);

        $edit      = _('Edit');
        $copy      = _('Copy');
        $delete    = _('Delete');
        $deleteall = _('Delete') . ' ' . _('All');
        $new       = _('New');

        $idplace ??= 0;
        $iduser ??= 0;

        return <<< JS

        var type = (typeof bindParameters.event.type !== "undefined") ? bindParameters.event.type : "";
        var room = (typeof bindParameters.event.room !== "undefined") ? bindParameters.event.room : "";
        var content = (typeof bindParameters.event.content !== "undefined") ? "<br>" + bindParameters.event.content : "";
        var title;
        var params = (typeof bindParameters.event.id !== "undefined") ? bindParameters.event.id : bindParameters.event.idevent;
        if(typeof params !== "undefined"){
            params = params.split("_");

            if(bindParameters.event.dropdown){
                switch (params[0]) {
                    case "template":
                        title = '<div class="d-flex justify-content-end"><div class="flex-grow-1"><input type="checkbox" id="cb'+params[1]+'" value="'+bindParameters.event.title+'"/>' + bindParameters.event.title + '</div><div class="flex-shrink-1"><div class="btn-group dropdown"><button type="button" class="btn  btn-light-primary btn-sm px-3  btn-popover" data-bs-toggle="dropdown"><i class="fas fa-ellipsis-v"></i></button><div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item  btn_create_template " href="#"  data-router-disabled aria-label="{$new}" data-toogle="tooltip" title="{$new}" data-placement="top" data-apimethod="GET" data-id="'+bindParameters.event.idreference_to+'" data-apiressource="performance" data-target="form_create_performance" data-idreference_to="'+bindParameters.event.idreference_to+'" data-start="'+bindParameters.event.startf+'" data-end="'+bindParameters.event.endf+'" data-name="'+bindParameters.event.title+'" data-idplace="{$idplace}" data-iduser="{$iduser}"><i class="fas fa-plus"></i>{$new}</a><a class="dropdown-item  btn_deleteone_'+params[0]+' " href="#"  data-router-disabled aria-label="{$delete}" data-toogle="tooltip" title="{$delete}" data-placement="top" data-apimethod="PUT" data-id="'+params[1]+'" data-idtemplate="'+params[1]+'" data-apiressource="template" data-idreference_to="'+bindParameters.event.idreference_to+'" data-start="'+bindParameters.event.startf+'" data-end="'+bindParameters.event.endf+'"><i class="fas fa-trash"></i> {$delete}</a><a class="dropdown-item  btn_delete_'+params[0]+' " href="#"  data-router-disabled aria-label="{$deleteall}" data-toogle="tooltip" title="{$deleteall}" data-placement="top" data-apimethod="DELETE" data-id="'+params[1]+'" data-id'+params[0]+'="'+params[1]+'" data-apiressource="'+params[0]+'" data-target="form_delete_'+params[0]+'"><i class="fas fa-trash"></i> {$deleteall}</a></div></div></div></div>';
                        break;
                    case "performance":
                        title = '<div class="d-flex justify-content-end"><div class="flex-grow-1"><input type="checkbox" id="cb'+params[1]+'" value="'+params[1]+'"/>' + bindParameters.event.title + '</div><div class="flex-shrink-1"><div class="btn-group dropdown"><button type="button" class="btn  btn-light-primary btn-sm px-3  btn-popover" data-bs-toggle="dropdown"><i class="fas fa-ellipsis-v"></i></button><div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item  btn_update_'+params[0]+' " href="#"  data-router-disabled aria-label="{$edit}" data-toogle="tooltip" title="{$edit}" data-placement="top" data-apimethod="GET" data-id="'+params[1]+'" data-id'+params[0]+'="'+params[1]+'" data-apiressource="'+params[0]+'" data-filterid="'+params[1]+'" data-target="form_update_'+params[0]+'"><i class="fas fa-edit"></i> {$edit}</a><a class="dropdown-item  btn_delete_'+params[0]+' " href="#"  data-router-disabled aria-label="{$delete}" data-toogle="tooltip" title="{$delete}" data-placement="top" data-apimethod="DELETE" data-id="'+params[1]+'" data-id'+params[0]+'="'+params[1]+'" data-apiressource="'+params[0]+'" data-target="form_delete_'+params[0]+'"><i class="fas fa-trash"></i> {$delete}</a><a class="dropdown-item  btn_copy_'+params[0]+' " href="#"  data-router-disabled aria-label="{$copy}" data-toogle="tooltip" title="{$copy}" data-placement="top" data-apimethod="GET" data-id="'+params[1]+'" data-id'+params[0]+'="'+params[1]+'" data-apiressource="'+params[0]+'" data-target="form_create_'+params[0]+'" data-filter="copyEvent"><i class="fas fa-copy"></i> {$copy}</a></div></div></div></div>';
                        break;
                    default:
                        title = '<div class="d-flex justify-content-end"><div class="flex-grow-1"><input type="checkbox" id="cb'+params[1]+'" value="'+params[1]+'"/>' + bindParameters.event.title + '</div><div class="flex-shrink-1"><div class="btn-group dropdown"><button type="button" class="btn  btn-light-primary btn-sm px-3  btn-popover" data-bs-toggle="dropdown"><i class="fas fa-ellipsis-v"></i></button><div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item  btn_update_'+params[0]+' " href="#"  data-router-disabled aria-label="{$edit}" data-toogle="tooltip" title="{$edit}" data-placement="top" data-apimethod="GET" data-id="'+params[1]+'" data-id'+params[0]+'="'+params[1]+'" data-apiressource="'+params[0]+'" data-filterid="'+params[1]+'" data-target="form_update_'+params[0]+'"><i class="fas fa-edit"></i> {$edit}</a><a class="dropdown-item  btn_delete_'+params[0]+' " href="#"  data-router-disabled aria-label="{$delete}" data-toogle="tooltip" title="{$delete}" data-placement="top" data-apimethod="DELETE" data-id="'+params[1]+'" data-id'+params[0]+'="'+params[1]+'" data-apiressource="'+params[0]+'" data-target="form_delete_'+params[0]+'"><i class="fas fa-trash"></i> {$delete}</a><a class="dropdown-item  btn_copy_'+params[0]+' " href="#"  data-router-disabled aria-label="{$copy}" data-toogle="tooltip" title="{$copy}" data-placement="top" data-apimethod="GET" data-id="'+params[1]+'" data-id'+params[0]+'="'+params[1]+'" data-apiressource="'+params[0]+'" data-target="form_create_'+params[0]+'" data-filter="copyEvent"><i class="fas fa-copy"></i> {$copy}</a></div></div></div></div>';
                        break;
                }

                bindParameters.element.popover({
                    animation: true,
                    title: title,
                    html:true,
                    content:type + " "+ room + content,
                    trigger:"manual",
                    placement:"auto",
                    container:"body"
                }).click(function(e) {
                    if(!$(this).hasClass('btn-popover')){
                        $(this).popover('toggle');
                    }
                });

                bindParameters.element.on("dragstart", function(){
                    bindParameters.element.popover("disable");
                    bindParmeters.element.popover("hide");
                });
                bindParameters.element.on("dragend", function(){
                    bindParameters.element.popover("enable");
                });
                //if (!confirm("Are you sure about this change?")) {
                //revertFunc();
                //}
            }
        }
JS;
    }

    public static function getSimpleRenderEvent()
    {
        return <<< JS
        var type = "";
if (typeof bindParameters.event.type !== "undefined") {
    type = bindParameters.event.type;
}
var room = "";
if (typeof bindParameters.event.room !== "undefined") {
    room = bindParameters.event.room;
}
var patt = new RegExp("[a-zA-Z]");
if(!patt.test(bindParameters.event.idevent)){
    bindParameters.element.popover({
        animation: true,
        title: bindParameters.event.title,html:true,content:type + " "+ room + "<br>" + bindParameters.event.content,trigger:"click",placement:"auto",container:"body"
    });

    bindParameters.element.on("dragstart", function(){
        bindParameters.element.popover("disable");
        bindParmeters.element.popover("hide");
    });
     bindParameters.element.on("dragend", function(){
        bindParameters.element.popover("enable");
    });
}
JS;
    }
}
