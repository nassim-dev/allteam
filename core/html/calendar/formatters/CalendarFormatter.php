<?php

namespace core\html\calendar\formatters;

use core\entities\EntitieInterface;
use core\entities\interfaces\TimeableInterface;
use core\html\AbstractFormatter;
use core\html\calendar\CalendarDecoratorInterface;
use core\html\FormatterInterface;
use core\utils\Utils;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class CalendarFormatter extends AbstractFormatter implements FormatterInterface
{
    /**
     * Undocumented function
     */
    protected function isFormattable(EntitieInterface $entitie): bool
    {
        return is_subclass_of($entitie, TimeableInterface::class);
    }

    /**
     * Undocumented function
     */
    public function format(EntitieInterface $entitie): ?array
    {
        if (!$this->isFormattable($entitie)) {
            return null;
        }

        $class = (class_exists($entitie->getNamespace() . '\decorator\CalendarDecorator')) ? $entitie->getNamespace() . '\decorator\CalendarDecorator' : '';

        /**
         * @var CalendarDecoratorInterface|null $decorator
         */
        $decorator = $this->getDi()->singleton($class);
        if ($decorator !== null) {
            $this->useContext($decorator->decorate($entitie));
        }

        $row = [
            'allDay'      => $this->context['allDay'] ?? false,
            'ressourceId' => $this->context['ressourceId'] ?? '0',
            'editable'    => $this->context['editable'] ?? false,
            'dropdown'    => $this->context['dropdown'] ?? false,
            'overlap'     => $this->context['overlap'] ?? false
        ];


        $row[$entitie->getPrimaryKey()] = $entitie->getPrimary();
        $row['id']                      = $entitie->getTable()->getName() . '_' . $entitie->getPrimary();
        if (isset($this->context['id'])) {
            $row['id'] .= '_' . $this->context['id'];
        }

        /** @var TimeableInterface $entitie */
        $row['start'] = Utils::formatDate($entitie->getStart());
        $row['end']   = Utils::formatDate($entitie->getEnd());

        $row = array_map(fn ($item) => (is_string($item)) ? htmlspecialchars_decode($item, ENT_QUOTES) : $item, $row);


        if (isset($this->context['options'])) {
            $this->context['options'] = array_map(fn ($item) => (is_string($item)) ? htmlspecialchars_decode($item, ENT_QUOTES) : $item, $this->context['options']);
        }

        $row = (is_array($this->context['options'])) ? array_merge($row, $this->context['options']) : $row;

        return $row;
    }
}
