<?php

namespace core\html\calendar;

use core\utils\Utils;

/**
 * Trait CalendarHandlerTrait
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
trait CalendarHandlerTrait
{
    /**
     * Generate Calendar events
     */
    protected function getEventsCalendar(array $variables = [], array $parameters = [], array $calendarParameters = []): array
    {
        foreach ($parameters as $parameter) {
            $variables[$parameter] = $this->getParameter($parameter);
            if (null === $this->getParameter($parameter)) {
                $this->killServer(204);
            }
        }

        if (null === $this->getParameter('start') || null === $this->getParameter('end')) {
            $this->killServer(204);
        }

        $start = Utils::createDateTime($this->getParameter('start'));
        $end   = Utils::createDateTime($this->getParameter('end'));

        $objects = $this->getDbManager()
            ->getByParams($variables)
            ->where('start >= ?  OR end <= ?', $start, $end);

        if (empty($objects)) {
            $this->killServer(204);
        }

        $datas         = [];
        $passedObjects = [];
        $firstDate     = false;
        foreach ($objects as $object) {
            if (!in_array($object->{$this->getDbManager()->getIndexKey()}, $passedObjects)) {
                $row = $object->calendar($calendarParameters);
                if (!null === $row) {
                    if (!$firstDate) {
                        $firstDate = $row['start'];
                    }

                    $datas[]         = $row;
                    $passedObjects[] = $object->{$this->getDbManager()->getIndexKey()};
                }
            }
        }

        $this->response->setData('data', $datas);
        $this->response->setData('currentDate', $firstDate);

        return $datas;
    }

    /**
     * Generate Calendar events
     */
    protected function getRessourcesCalendar(string $ressourceKey, array $variables = [], array $parameters = []): array
    {
        foreach ($parameters as $parameter) {
            $variables[$parameter] = $this->getParameter($parameter);
            if (null === $this->getParameter($parameter)) {
                $this->killServer(204);
            }
        }

        if (null === $this->getParameter('start') || null === $this->getParameter('end')) {
            $this->killServer(204);
        }

        $start = Utils::createDateTime($this->getParameter('start'));
        $end   = Utils::createDateTime($this->getParameter('end'));

        $objects = $this->getDbManager()
            ->getByParams($variables)
            ->where('start >= ?  OR end <= ?', $start, $end);

        if (empty($objects)) {
            $this->killServer(204);
        }

        $datas = [
            [
                'id'    => 0,
                'title' => _('No selection')
            ]
        ];

        $passedObjects = [];

        foreach ($objects as $object) {
            $ressource = $object->{$ressourceKey};
            if (is_object($ressource) && !in_array($ressource->getPrimary(), $passedObjects)) {
                $datas[] = [
                    'id'    => $ressource->getPrimary(),
                    'title' => $ressource->getHistoricalFormat()];

                $passedObjects[] = $ressource->getPrimary();
            }
        }

        $this->response->setData('data', $datas);

        return $datas;
    }

    /**
     * Resize an event with drag'n drop
     *
     * @return void
     */
    protected function resizeEvent(?string $newRessourceKey = null)
    {
        if (null === $this->getParameter('start') ||
            null === $this->getParameter('end') ||
            null === $this->getParameter($this->getDbManager()->getIndexKey())) {
            $this->killServer(204);
        }

        $object = $this->getDbManager()->getById($this->getParameter($this->getDbManager()->getIndexKey()));

        if (!$object) {
            $this->killServer(204);
        }

        $params = [
            'start' => Utils::createDateTime($this->getParameter('start')),
            'end'   => Utils::createDateTime($this->getParameter('end'))];

        if (null !== $this->getParameter($newRessourceKey)) {
            $params[$newRessourceKey] = $this->getParameter($newRessourceKey);
        }

        $object->hydrate($params);
        $this->getDbManager()->update($object);

        $this->response->setCode(200);
    }
}
