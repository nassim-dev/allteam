<?php

namespace core\html\traits;

/**
 * Trait Collapsable
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait Collapsable
{
    /**
     * True if collapsable
     *
     * @var bool
     */
    private $collapsable = false;

    /**
     * True if expanded by default
     *
     * @var boolean
     */
    private $expanded = true;

    /**
     * Get true if collapsable
     *
     * @PhpUnitGen\get("collapsable")
     */
    public function isCollapsable(): bool
    {
        return $this->collapsable;
    }

    /**
     * Set true if collapsable
     *
     * @PhpUnitGen\set("collapsable")
     *
     * @param bool $collapsable True if collapsable
     *
     * @return static
     */
    public function setCollapsable(bool $collapsable)
    {
        $this->collapsable = $collapsable;

        return $this;
    }

    /**
     * Get true if expanded by default
     *
     * @PhpUnitGen\get("expanded")
     */
    public function isExpanded(): bool
    {
        return $this->expanded;
    }

    /**
     * Set true if expanded by default
     *
     * @param bool $expanded True if expanded by default
     *
     * @PhpUnitGen\set("expanded")
     *
     * @return self
     */
    public function setExpanded(bool $expanded)
    {
        $this->expanded = $expanded;

        return $this;
    }
}
