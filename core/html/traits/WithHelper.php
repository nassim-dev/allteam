<?php

namespace core\html\traits;

/**
 * Trait WithHelper
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait WithHelper
{
    /**
     * Help text
     *
     * @var string|null
     */
    private $help = null;

    /**
     * Get help text
     *
     * @PhpUnitGen\get("help")
     *
     * @return string|null|bool
     */
    public function getHelp()
    {
        return $this->help;
    }



    /**
     * Set help text
     *
     * @PhpUnitGen\set("help")
     *
     * @param string|null $help Help text
     *
     * @return static
     */
    public function setHelp(?string $help)
    {
        $this->help = $help;

        return $this;
    }
}
