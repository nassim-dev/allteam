<?php

namespace core\html\traits;

use core\html\HtmlElementContainer;
use core\html\HtmlElementInterface;
use core\html\HtmlException;

/**
 * Trait Contenable
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait Contenable
{
    /**
     * @var HtmlElementContainer HtmlElementInterface
     */
    private $childs = null;

    /**
     * @var bool
     */
    private $isInsideElement = false;

    /**
     * @var HtmlElementInterface
     */
    private $parent = null;

    /**
     * Append child element
     *
     * @return HtmlElementInterface|null
     */
    public function appendChild(?HtmlElementInterface &$child, ?string $identifier = null)
    {
        if ($child === null) {
            return null;
        }

        if (empty(static::getAllowedChilds())) {
            throw new HtmlException('You cannot use this method ' . __METHOD__ . ' in ' . $this::class . ' because static:$allowedChilds returns an empty array');
        }

        if ($this === $child->getParent()) {
            return $child;
        }

        if (in_array($child::class, static::getAllowedChilds())
            || in_array(get_parent_class($child), static::getAllowedChilds())
            || count(array_intersect(class_implements($child), static::getAllowedChilds())) > 0) {
            if (!$this->isContainer()) {
                $child->getParent()?->removeChild($child);
                $child->setParent($this);
            } else {
                $child->setInsideContainer();
            }

            $this->initializeChildsContainer();
            $this->childs->attach($child, $identifier ?? $child->getReference() ?? null);

            return $child;
        }

        throw new HtmlException('Trying to attach ' . $child::class . ' in ' . $this::class . '. You can only add child instance of these classes : ' . implode(' , ', static::getAllowedChilds()));
    }

    /**
     * Append element to parent
     *
     * @return HtmlElementInterface
     */
    public function appendTo(HtmlElementInterface &$parent, ?string $identifier = null)
    {
        return $parent->appendChild($this, $identifier);
    }

    /**
     * Get of HtmlElementInterface
     *
     * @PhpUnitGen\get("childs")
     */
    public function getChilds(): ?HtmlElementContainer
    {
        $this->initializeChildsContainer();

        return $this->childs;
    }

    /**
     * Return specific child
     */
    public function getChild(string $identifier): ?HtmlElementInterface
    {
        $this->initializeChildsContainer();

        return $this->childs->findObject($identifier) ?? null;
    }

    /**
     * Get the value of parent
     *
     * @PhpUnitGen\get("parent")
     */
    public function getParent(): ?HtmlElementInterface
    {
        return $this->parent;
    }

    public function hasChild(): bool
    {
        return (null !== $this->childs && $this->childs->count() !== 0);
    }

    public function hasParent(): bool
    {
        return $this->parent !== null;
    }

    /**
     * Remove chidl element
     *
     * @return void
     */
    public function removeChild(HtmlElementInterface $child)
    {
        if (empty(static::getAllowedChilds())) {
            throw new HtmlException('You cannot use this method ' . __METHOD__ . ' in ' . self::class);
        }

        $child->setParent(null);

        if ($this->childs !== null && $this->childs->contains($child)) {
            $this->childs->detach($child);
        }
    }

    /**
     * Remove element from its parent
     *
     * @return void
     */
    public function removeFrom(HtmlElementInterface $parent)
    {
        $parent->removeChild($this);
    }

    /**
     * Set the value of parent
     *
     * @PhpUnitGen\set("parent")
     *
     * @return static
     */
    public function setParent(?HtmlElementInterface $parent)
    {
        $this->parent          = $parent;
        $this->isInsideElement = (null !== $parent);

        return $this;
    }

    /**
     * List of allowed childs elements
     * could be a class, an abstract class or an interface
     */
    abstract protected static function getAllowedChilds(): array;

    private function initializeChildsContainer()
    {
        if (empty(static::getAllowedChilds())) {
            return  $this->childs = null;
        }

        if (null === $this->childs) {
            $this->childs = new HtmlElementContainer();
        }
    }

    /**
     * @return array{parent: string|null, childs: string[]}
     */
    public function _toArrayContenable(): array
    {
        $return = ['parent' => null, 'childs' => []];
        if ($this->hasChild()) {
            foreach ($this->getChilds() as $child) {
                if (is_object($child) && !$child->isContainer()) {
                    $return['childs'][] = '#' . $child->getIdAttribute();
                }
            }
        }

        if ($this->hasParent()) {
            $return['parent'] = '#' . $this->parent?->getIdAttribute();
        }

        return $return;
    }
}
