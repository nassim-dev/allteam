<?php

namespace core\html\traits;

/**
 * Undocumented trait
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
trait Clickable
{
    /**
     * Link location
     *
     * @var string
     */
    private $href = '#';

    /**
     * Get the value of href
     *
     * @PhpUnitGen\get("href")
     */
    public function getHref(): string
    {
        return $this->href;
    }

    /**
     * Set the value of href
     *
     * @PhpUnitGen\set("href")
     */
    public function setHref(string $href): self
    {
        $this->href = $href;

        return $this;
    }
}
