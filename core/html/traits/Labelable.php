<?php

namespace core\html\traits;

use core\html\label\Label;

/**
 * Trait Labelable
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait Labelable
{
    /**
     * Label for action
     *
     * @var string|null
     */
    private $label = null;

    /**
     * Build a label in html
     *
     * @return static
     */
    public function buildHtmlLabel(?string $text, ?string $icon, string $color = Label::SUCCESS)
    {
        $label = new Label($text, $icon, $color);
        $this->setLabel($label->getHtml());

        return $this;
    }

    /**
     * Get label for action
     *
     * @PhpUnitGen\get("label")
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * Set label for action
     *
     * @PhpUnitGen\set("label")
     *
     * @param string|null $label Label for action
     *
     * @return static
     */
    public function setLabel(?string $label)
    {
        $this->label = $label;

        return $this;
    }
}
