<?php

namespace core\html\traits;

/**
 * Trait Titleable
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait Titleable
{
    /**
     * Title
     *
     * @var string|null
     */
    private $title = null;

    /**
     * Get title
     *
     * @PhpUnitGen\get("title")
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * Set title
     *
     * @PhpUnitGen\set("title")
     *
     * @param string $title Title for action
     *
     * @return static
     */
    public function setTitle(string $title)
    {
        $this->title = $title;

        return $this;
    }
}
