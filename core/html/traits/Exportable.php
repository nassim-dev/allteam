<?php

namespace core\html\traits;

use core\html\button\Button;
use core\html\CommandFactory;
use core\html\javascript\command\ExportWidgetCommand;
use core\html\javascript\JavascriptObjectInterface;

/**
 * Trait Exportable
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait Exportable
{
    use Iconable;
    use Deactivable;
    use WithActions;

    public function addExportButton(): ?Button
    {
        if (class_implements(JavascriptObjectInterface::class)) {
            /** @var Button $exportButton */
            $exportButton = $this->addButton(_('Export'), 'create_' . strtolower($this->getId()));

            $exportButton->addClasses(
                [
                    'btn_export_' . $this->getId(),
                    //'animated',
                    //'heartBeat',
                    //'delay-3s',
                    ' btn-light-success'
                ]
            )
                ->setIcon('fa-file-export')
                ->enable();

            /** @var CommandFactory $commandFactory */
            $commandFactory = $this->getDi()->singleton(CommandFactory::class);

            $exportButton->on('click')
                ->command($commandFactory->create(ExportWidgetCommand::class, ['target' => $this]));

            $exportButton->disable();

            return $exportButton;
        }

        return null;
    }
}
