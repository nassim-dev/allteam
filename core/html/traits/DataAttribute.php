<?php

namespace core\html\traits;

/**
 * Trait DataAttribute
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait DataAttribute
{
    /**
     * Extra  dataAttribute
     *
     * @var array
     */
    private $dataAttribute = [];

    private $formatedDataAttributes = null;

    /**
     * Add one data attribute
     *
     * @return static
     */
    public function addDataAttribute(string $key, string $val)
    {
        //if (!array_key_exists($key, $this->dataAttribute)) {
        $this->dataAttribute[$key] = $val;
        //}

        return $this;
    }

    /**
     * Add  dataAttributes
     *
     * @return static
     */
    public function addDataAttributes(array $dataAttributes)
    {
        foreach ($dataAttributes as $key => $val) {
            $this->addDataAttribute($key, $val);
        }


        return $this;
    }

    public function clearDataAttributes()
    {
        $this->dataAttribute = [];
    }

    /**
     * @return mixed
     */
    public function getDataAttributes(): array
    {
        return $this->dataAttribute;
    }


    /**
     * Return data attributes formated
     * [data-attribute => value]
     */
    public function getFormatedDataAttributes(): array
    {
        if ($this->formatedDataAttributes !== null) {
            return $this->formatedDataAttributes;
        }

        $this->formatedDataAttributes = [];

        foreach ($this->dataAttribute as $key => $value) {
            if ($value !== null && $key !== null) {
                $this->formatedDataAttributes["data-$key"] = $value;
            }
        }

        return $this->formatedDataAttributes;
    }

    /**
     * Add one  dataAttribute
     *
     * @return static
     */
    public function removeDataAttribute(string $dataAttribute)
    {
        $key = array_search($dataAttribute, $this->dataAttribute);
        if ($key) {
            unset($this->dataAttribute[$key]);
        }

        return $this;
    }

    /**
     * Remove  dataAttributes
     *
     * @return static
     */
    public function removeDataAttributes(array $dataAttributes)
    {
        $this->dataAttribute = array_diff($this->dataAttribute, $dataAttributes);

        return $this;
    }

    /**
     * @return array{dataAttributes: mixed}
     */
    public function _toArrayDataAttibute(): array
    {
        return [
            'dataAttributes' => $this->dataAttribute,
        ];
    }
}
