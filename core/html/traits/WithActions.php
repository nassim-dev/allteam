<?php

namespace core\html\traits;

use core\html\bar\Bar;
use core\html\bar\BarFactory;
use core\html\button\Button;
use core\html\form\elements\FormElement;
use core\DI\DiProvider;

/**
 * Trait WithActions
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait WithActions
{
    use DiProvider;

    private $__actionBar;

    /**
     * Get Object ActionBar
     */
    public function getActionBar(): Bar
    {
        return $this->__actionBar ??= $this->getDi()->singleton(BarFactory::class)->createForHtmlElement($this);
    }

    /**
     * Set the value of __actionBar
     *
     * @PhpUnitGen\set("__actionBar")
     * @return self
     */
    public function setActionBar(?Bar $actionBar)
    {
        if ($actionBar !== null) {
            if ($actionBar->getParentElement() !== null && method_exists($actionBar->getParentElement(), 'setActionBar')) {
                $actionBar->getParentElement()->setActionBar(null);
            }

            $actionBar->setParentElement($this);
        }

        $this->__actionBar = $actionBar;

        return $this;
    }

    /**
     * Remove Button from Bar
     *
     * @return void
     */
    public function removeButton(string $action)
    {
        $this->getActionBar()->removeAction($action);
    }


    /**
     * @return array{actionBar: mixed}
     */
    public function _toArrayWithAction(): array
    {
        return [
            'actionBar' => $this->getActionBar()->getIdAttribute(),
        ];
    }

    /**
     * Add New Button to Bar
     *
     * @param string $contentCheck
     */
    public function addButton(string $action): Button
    {
        return $this->getActionBar()->addButton($action);
    }

    /**
     * Add form element to actionBar
     *
     * @param string|null $idBar
     * @param string      $type
     *
     * @return FormElement
     */
    public function addField(string $label, string $fieldType = TextElement::class)
    {
        $field = $this->getActionBar()->addField($label, $fieldType);

        return $field->enable();
    }
}
