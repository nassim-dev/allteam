<?php

namespace core\html\traits;

/**
 * Trait Colorable
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait Colorable
{
    /**
     * Color for action
     *
     * @var string|null
     */
    private $color = null;

    /**
     * Get color for action
     *
     * @PhpUnitGen\get("color")
     */
    public function getColor(): ?string
    {
        return $this->color;
    }

    /**
     * Set color for action
     *
     * @PhpUnitGen\set("color")
     *
     * @param string|null $color Color for action
     *
     * @return static
     */
    public function setColor(?string $color)
    {
        $this->color = $color;

        return $this;
    }
}
