<?php

namespace core\html\traits;

/**
 * Trait Valuable
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait Valuable
{
    /**
     * Value
     *
     * @var string|null
     */
    private $value = null;

    /**
     * Get value
     *
     * @PhpUnitGen\get("value")
     */
    public function getValue(): mixed
    {
        return $this->value;
    }

    /**
     * Set value
     *
     * @PhpUnitGen\set("value")
     *
     * @param string $value Value for action
     *
     * @return static
     */
    public function setValue(mixed $value)
    {
        $this->value = $value;

        return $this;
    }
}
