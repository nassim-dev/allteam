<?php

namespace core\html\traits;

/**
 * Trait Identifiable
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait Identifiable
{
    /**
     * Id
     *
     * @var string|null
     */
    private $id = null;

    /**
     * Get id
     *
     * @PhpUnitGen\get("id")
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * Set id
     *
     * @PhpUnitGen\set("id")
     *
     * @param string $id Id for action
     *
     * @return static
     */
    public function setId(string $id)
    {
        $this->id = $id;

        return $this;
    }
}
