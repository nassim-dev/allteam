<?php

namespace core\html\traits;

/**
 * Trait Activeable
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait Activeable
{
    /**
     * Active
     *
     * @var string|null
     */
    private $active = null;

    /**
     * Get pill is active ?
     *
     * @PhpUnitGen\get("active")
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * Set pill is active ?
     *
     * @PhpUnitGen\set("active")
     *
     * @param bool $active Pill is active ?
     */
    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }
}
