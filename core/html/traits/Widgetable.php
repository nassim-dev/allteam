<?php

namespace core\html\traits;

use core\DI\DI;
use core\html\button\Button;
use core\html\calendar\command\DetachCommand;
use core\html\CommandFactory;
use core\html\javascript\JavascriptObjectInterface;

/**
 * Trait Widgetable
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait Widgetable
{
    use WithActions;
    use Deactivable;
    use Iconable;

    public function addDetachButton(): ?Button
    {
        if (class_implements(JavascriptObjectInterface::class)) {
            /** @var Button $detachButton */
            $detachButton = $this->addButton(_('Detach'), 'create_' . strtolower($this->getId()));

            $detachButton->addClasses(
                [
                    'btn_detach_' . $this->getId(),
                    'animated',
                    'heartBeat',
                    'delay-5s',
                    ' btn-light-success'
                ]
            )
                ->setIcon('fa-external-link-alt')
                ->enable();

            /** @var CommandFactory $commandFactory */
            $commandFactory = DI::singleton(CommandFactory::class);

            $detachButton->on('click')
                ->command($commandFactory->create(DetachCommand::class, ['target' => $this]))
                ->addParameters(
                    [
                        'type' => 'calendar',
                        'id'   => 'calendar-' . $this->getId(),
                        'page' => 'widget'
                    ]
                );
            $detachButton->disable();

            return $detachButton;
        }

        return null;
    }
}
