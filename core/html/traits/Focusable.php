<?php

namespace core\html\traits;

/**
 * Trait Focusable
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait Focusable
{
    /**
     *  La valeur défaut de l'élément
     *
     * @var string|null
     */
    private $autofocus = null;

    /**
     * Get value of autofocus
     */
    public function getAutofocus(): ?string
    {
        return $this->autofocus;
    }

    /**
     * Dos element has autofocus ?
     */
    public function hasAutofocus(): bool
    {
        return (null !== $this->autofocus);
    }

    /**
     * Set la valeur défaut de l'élément
     *
     * @PhpUnitGen\set("autofocus")
     *
     * @param bool $autofocus La valeur défaut de l'élément
     *
     * @return static
     */
    public function setAutofocus(bool $autofocus)
    {
        $this->autofocus = ($autofocus) ? 'autofocus' : null;

        return $this;
    }
}
