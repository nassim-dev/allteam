<?php

namespace core\html\traits;

/**
 * Trait Deactivable
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait Deactivable
{
    /**
     * Status disabled or enabled
     *
     * @var bool
     */
    private $state = true;

    /**
     * @return static
     */
    public function disable()
    {
        $this->state = false;

        return $this;
    }


    /**
     * @return static
     */
    public function enable()
    {
        $this->state = true;

        return $this;
    }

    /**
     * Undocumented function
     */
    public function isDisabled(): bool
    {
        return !$this->state;
    }

    public function isEnabled(): bool
    {
        return $this->state;
    }
}
