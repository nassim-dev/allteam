<?php

namespace core\html\traits;

use core\routing\Link;

/**
 * Trait AjaxSourced
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait AjaxSourced
{
    /**
     * Endpoints
     *
     * @var Link[]
     */
    private array $endpoints = [];


    /**
     * Add url endpoint
     *
     * @return Link
     */
    public function addEndpoint(string $widgetAction, Link $link): Link
    {
        return  $this->endpoints[$widgetAction] ??= $link;
    }


    public function _toArrayAjaxSourced(): array
    {
        $return              = [];
        $return['endpoints'] = [];

        /** @var Link $link */
        foreach ($this->endpoints as $action => $link) {
            $return['endpoints'][$action] = [
                'endpoint'   => $link->getEndpoint(),
                'initParams' => $link->getParameters(),
                'method'     => $link->getMethod()
            ];
        }

        return $return;
    }

    /**
     * Get the value of endpoints
     *
     * @PhpUnitGen\get("endpoints")
     *
     * @return array
     */
    public function getEndpoints(): array
    {
        return $this->endpoints;
    }
}
