<?php

namespace core\html\traits;

/**
 * Trait Stylizable
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait Stylizable
{
    /**
     * Extra CSS class
     *
     * @var array
     */
    private $class = [];

    /**
     * Add one css class
     *
     * @return static
     */
    public function addClass(string $class)
    {
        if (!in_array($class, $this->class)) {
            $this->class[] = $class;
        }

        return $this;
    }

    /**
     * Add css classes
     *
     * @return static
     */
    public function addClasses(array $classes)
    {
        foreach ($classes as $class) {
            if (!is_string($class)) {
                continue;
            }

            $this->addClass($class);
        }

        return $this;
    }



    public function getClasses(): array
    {
        return $this->class;
    }

    /**
     * Add one css class
     *
     * @return static
     */
    public function removeClass(string $class)
    {
        $key = array_search($class, $this->class);
        if ($key) {
            unset($this->class[$key]);
        }

        return $this;
    }

    /**
     * Remove css classes
     *
     * @return static
     */
    public function removeClasses(array $classes)
    {
        $this->class = array_diff($this->class, $classes);

        return $this;
    }

    /**
     * Return classes as string
     */
    public function stringifyClasses(): string
    {
        return implode(' ', $this->class);
    }
}
