<?php

namespace core\html\traits;

/**
 * Trait Tunable
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait Tunable
{
    /**
     * Extra  parameter
     *
     * @var array
     */
    private $parameter = [];

    /**
     * Add one data attribute
     *
     * @return static
     */
    public function addParameter(string $key, string $val)
    {
        $this->parameter[$key] = $val;

        return $this;
    }

    /**
     * Add  parameters
     *
     * @return static
     */
    public function addParameters(array $parameters)
    {
        $this->parameter = array_merge($this->parameter, $parameters);

        return $this;
    }

    public function clearParameters()
    {
        $this->parameter = [];
    }

    /**
     * @return mixed
     */
    public function getParameters(): array
    {
        return $this->parameter;
    }


    /**
     * Add one  parameter
     *
     * @return static
     */
    public function removeParameter(string $parameter)
    {
        $key = array_search($parameter, $this->parameter);
        if ($key) {
            unset($this->parameter[$key]);
        }

        return $this;
    }

    /**
     * Remove  parameters
     *
     * @return static
     */
    public function removeParameters(array $parameters)
    {
        $this->parameter = array_diff($this->parameter, $parameters);

        return $this;
    }
}
