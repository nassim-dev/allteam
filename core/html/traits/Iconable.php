<?php

namespace core\html\traits;

/**
 * Trait Iconable
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait Iconable
{
    /**
     * Icon for action
     *
     * @var string|null
     */
    private $icon = null;

    /**
     * Get icon for action
     *
     * @PhpUnitGen\get("icon")
     */
    public function getIcon(): ?string
    {
        return $this->icon;
    }

    /**
     * Set icon for action
     *
     * @PhpUnitGen\set("icon")
     *
     * @param string|null $icon Icon for action
     *
     * @return static
     */
    public function setIcon(?string $icon)
    {
        $this->icon = $icon;

        return $this;
    }
}
