<?php

namespace core\html\traits;

/**
 * Trait Captionable
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait Captionable
{
    /**
     * Caption
     *
     * @var string|null
     */
    private ?string $caption = null;

    /**
     * Set caption attribute
     *
     * @PhpUnitGen\set("caption")
     *
     * @param string $caption Caption attribute
     */
    public function setCaption(string $caption): self
    {
        $this->caption = $caption;

        return $this;
    }

    /**
     * Get caption attribute
     *
     * @PhpUnitGen\get("caption")
     */
    public function getCaption(): string
    {
        return $this->caption;
    }
}
