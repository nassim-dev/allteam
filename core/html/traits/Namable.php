<?php

namespace core\html\traits;

/**
 * Trait Namable
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait Namable
{
    /**
     * Name for action
     *
     * @var string|null
     */
    private $name = null;

    /**
     * Get name for action
     *
     * @PhpUnitGen\get("name")
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set name for action
     *
     * @PhpUnitGen\set("name")
     *
     * @param string $name Name for action
     *
     * @return static
     */
    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }
}
