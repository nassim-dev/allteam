<?php

namespace core\html;

use core\DI\DiProvider;
use core\entities\EntitieInterface;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
abstract class AbstractFormatter
{
    use DiProvider;

    protected $context;

    public function useContext(array $context): static
    {
        $this->context = $context;

        return $this;
    }

    protected function isFormattable(EntitieInterface $entitie): bool
    {
        return true;
    }
}
