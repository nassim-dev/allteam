<?php

namespace core\factory;

use core\containers\ContainerInterface;
use core\containers\DefaultContainer;
use core\DI\DiProvider;
use ReflectionClass;

/**
 * Class AbstractFactory
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
abstract class AbstractFactory
{
    use DiProvider;

    /**
     * Container
     * @var ContainerInterface;
     */
    private ?\core\containers\ContainerInterface $instances = null;

    /**
     * Available namespaces
     *
     * @var array
     */
    protected $_namespaces = [];

    /**
     * Get the value of namespaces
     *
     * @PhpUnitGen\get("namespaces")
     *
     * @return array
     */
    final public function getNamespaces(): array
    {
        return $this->_namespaces;
    }

    /**
     * Add new class to build
     */
    public function registerClass(string $class): self
    {
        $reflection = new ReflectionClass($class);

        return $this->addNamespace($reflection->getNamespaceName());
    }

    /**
     * Add new namespace
     *
     * @param string $namespaces
     */
    public function addNamespace(string $namespace): self
    {
        if (!in_array($namespace, $this->_namespaces)) {
            $this->_namespaces[] = $namespace;
        }

        return $this;
    }

    /**
     * Create new instance
     *
     * @return mixed
     */
    abstract public function create(string $identifier, array $params = []);

    /**
     * Get registred elements
     *
     * @return mixed|null
     */
    abstract public function get(string $identifier);

    /**
     * @return mixed
     */
    public function getInstance(string $identifier)
    {
        return $this->getInstances()->findObject($identifier) ?? null;
    }

    /**
     * Get all instances or filtered by callback
     *
     * @PhpUnitGen\get("instances")
     *
     * @return ContainerInterface;
     */
    public function getInstances(?callable $filter = null): ContainerInterface
    {
        $this->instances ??= new DefaultContainer();
        if (!null === $filter) {
            $entries = array_filter($this->instances->toArray(), $filter);

            return new ContainerInterface($entries);
        }

        return $this->instances;
    }

    /**
     * Add constructed object
     *
     * @param mixed $object
     */
    public function register($object, string $identifier): mixed
    {
        if (!$this->getInstances()->contains($object)) {
            $this->getInstances()->attach($object, $identifier);
        }

        return $object;
    }

    /**
     * Remove constructed object
     */
    public function remove(string $identifier): self
    {
        $this->getInstances()->detachWithIdentifier($identifier);

        return $this;
    }
}
