<?php

namespace core\factory;

use core\containers\ContainerInterface;

/**
 * Class FactoryInterface
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
interface FactoryInterface
{
    /**
     * Create new instance
     *
     * @return mixed
     */
    public function create(string $identifier, array $parameters = []);

    /**
     * Get registred elements
     *
     * @return mixed|null
     */
    public function get(string $identifier);

    /**
     * Get container
     */
    public function getInstances(?callable $filter = null): ContainerInterface;

    /**
     * Remove constructed object
     *
     * @return void
     */
    public function remove(string $identifier);

    /**
     * Add new class to build
     *
     * @return static
     */
    public function registerClass(string $class);

    /**
     * Add new namespace
     *
     * @param string $namespaces
     *
     * @return static
     */
    public function addNamespace(string $namespace);

    /**
     * Get the value of namespaces
     *
     * @PhpUnitGen\get("namespaces")
     *
     * @return array
     */
    public function getNamespaces();

    /**
     * Add constructed object
     *
     * @param mixed $object
     *
     * @return mixed
     */
    public function register($object, string $identifier);
}
