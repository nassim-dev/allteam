<?php

namespace core\view\latte;

use core\cache\CacheBuffer;
use core\cache\CacheServiceInterface;
use core\services\storage\FileException;
use core\utils\Utils;
use Latte\Engine;
use Latte\Macro;
use Latte\MacroNode;
use Latte\PhpWriter;

/**
 * Class LatteCacheMacro
 *
 * Usage in a latte template
 * {doll_cache identifier, $invalider_1, invalider_2}
 *      {doll_cache identifier, $invalider_1, invalider_2}
 *
 *      {/doll_cache}
 * {/doll_cache}
 *
 * Childs invaliders && keys used in template (in children to) are used to generate the key
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class LatteCacheMacro implements Macro
{
    /**
     * Cache adapter
     */
    private static \core\cache\CacheServiceInterface $cache;

    /**
     * Latte engine
     */
    private static \Latte\Engine $engine;

    /**
     * Latte nodeTree
     */
    private static \core\view\latte\LatteNodeTree $nodeTree;

    /**
     * Undocumented function
     */
    public function __construct(CacheServiceInterface $cache, LatteNodeTree $nodeTree, Engine $engine)
    {
        self::$cache    = $cache;
        self::$nodeTree = $nodeTree;
        self::$engine   = $engine;
    }

    /**
     * Starts the output buffer cache
     *
     * @return void
     */
    public static function createCache(string $template, string $nodeId, callable $callable, array $args = [], ?array $upgrade = null)
    {
        $lastModified = date('YmdHis', filemtime($template));
        $key          = self::generateKey($lastModified, $template, $args, $upgrade);

        // bdump('1 - ' . $key);
        //Return cache if exist
        $cache = self::$cache->get($key);
        if (\Memcached::RES_NOTFOUND !== $cache) {
            //bdump('Return cache : ' . $args[0]);
            echo $cache;

            return true;
        }

        //bdump('Generate cache : ' . $args[0]);

        //ob_start()
        $buffer = new CacheBuffer(self::$cache);
        $keys   = $callable();
        self::$nodeTree->setKeys($nodeId, $keys);

        //Generate cache key
        $upgradedKey = (null !== $keys) ? array_values($keys) : null;
        $key         = self::generateKey($lastModified, $template, $args, $upgradedKey);

        //ob_get_flush()
        $buffer->close($key);

        self::replaceNodes(self::$engine->getCacheFile($template), $nodeId);
    }

    public function finalize()
    {
    }

    public function initialize()
    {
    }

    public function nodeClosed(MacroNode $node)
    {
        $node->closingCode = PhpWriter::using($node)
            ->write('<?php
            $keys = $this->global->fn->getKeys[0]::getKeys();
            $this->global->fn->setKeys[0]::setKeys([]);
            return $keys;
        }); ?>');
    }

    public function nodeOpened(MacroNode $node)
    {
        $node->empty = false;
        self::$nodeTree->addNode($node);
        $node->openingCode = PhpWriter::using($node)
            ->write('<?php $that = &$this; core\view\latte\LatteCacheMacro::createCache($this->getName(), %node.array, /*node:%node.id*/null, %node.id, function() use($that) { extract($that->params); ?>');
    }

    /**
     * @param mixed[]|null $upgrade
     */
    private static function generateKey(string $lastModified, string $template, array $args = [], ?array $upgrade = null): ?string
    {
        $args    = (is_array($args) && !empty($args)) ? Utils::hash($args) : '';
        $upgrade = (is_array($upgrade) && !empty($upgrade)) ? Utils::hash($upgrade) : '';
        $baseKey = "Templating.Cache.$template.$lastModified";

        return Utils::hash([$baseKey, $args, $upgrade]);
    }

    /**
     * @param string $contents
     */
    private static function replaceNodes(string $file, string $nodeId)
    {
        if (!file_exists($file)) {
            throw new FileException("Innexisting template $file");
        }

        $contents    = \file_get_contents($file);
        $relatedNode = self::$nodeTree->getRelatedNode($nodeId);

        //Replace node:id comment by childs's keys
        if (null !== $relatedNode) {
            $replacementKeys = (null !== $relatedNode->getKeys()) ? '[' . implode(',', array_keys($relatedNode->getKeys())) . ']' : 'null';

            $regex    = '/\/\*node:\'' . $nodeId . '\'\*\/null/m';
            $contents = preg_replace($regex, $replacementKeys, $contents);
        }

        //Save time access to prevent cache invalidation
        $modificationTime = filemtime($file);

        //Save directly in template php after compilation
        if (file_put_contents("$file.tmp", $contents) !== strlen($contents) || !rename("$file.tmp", $file)) {
            @unlink("$file.tmp"); // @ - file may not exist
            touch($file, $modificationTime);
        }

        if (function_exists('opcache_invalidate')) {
            @opcache_invalidate($file, true); // @ can be restricted
        }

        return $contents;
    }
}
