<?php

namespace core\view\latte;

use core\utils\Utils;
use Latte\MacroNode;

/**
 * Class LatteNode
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class LatteNode
{
    /**
     * Keys
     */
    private array $keys = [];

    /**
     * @var mixed
     */
    private $parent = null;

    public function __construct(public MacroNode $node, /**
     * Latte nodeTree
     */
    private LatteNodeTree $nodeTree)
    {
        if (null !== $node->parentNode) {
            $parentNode   = $nodeTree->getRelatedNode(Utils::hash(spl_object_hash($node->parentNode)));
            $this->parent = $parentNode ?? $nodeTree->addNode($node->parentNode);
        }
    }

    /**
     * Get keys
     *
     * @PhpUnitGen\get("keys")
     *
     * @return array
     */
    public function getKeys(): ?array
    {
        return $this->keys ?? null;
    }

    /**
     * Set keys
     *
     * @PhpUnitGen\set("keys")
     *
     * @param array $keys Keys
     *
     * @return static
     */
    public function setKeys(array $keys)
    {
        $this->keys = array_merge($this->keys, $keys);
        if (null !== $this->parent) {
            $this->parent->setKeys($this->keys);
        }

        return $this;
    }
}
