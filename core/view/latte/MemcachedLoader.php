<?php

namespace core\view\latte;

use core\cache\CacheServiceInterface;
use core\utils\Utils;
use Latte\Loader;

/**
 * Class MemcachedLoader
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class MemcachedLoader implements Loader
{
    /**
     * @param $key
     */
    public function __construct(private CacheServiceInterface $cache, $key)
    {
    }

    /**
     * Returns template source code.
     *
     * @param $name
     */
    public function getContent($name): string
    {
        //bdump($this->getUniqueId($name));
        if (!$this->cache->hasKey($this->getUniqueId($name))) {
            throw new \RuntimeException("Missing template file '$name'.");
        }

        return $this->cache->get($this->getUniqueId($name));
    }

    /**
     * @param $file
     * @param $referringName
     */
    public function getReferredName($file, $referringFile): string
    {
        if ($this->baseDir || !preg_match('#/|\\\\|[a-z][a-z0-9+.-]*:#iA', $file)) {
            $file = static::normalizePath($referringFile . '/../' . $file);
        }

        return $file;
    }

    /**
     * @param $name
     */
    public function getUniqueId($name): string
    {
        return Utils::hash(['Templating', 'Cache', 'Global', $name]);
    }

    /**
     * @param $name
     * @param $time
     */
    public function isExpired($name, $time): bool
    {
        return true;
    }

    protected static function normalizePath(string $path): string
    {
        $res = [];
        foreach (explode('/', strtr($path, '\\', '/')) as $part) {
            if ('..' === $part && $res && end($res) !== '..') {
                array_pop($res);
            } elseif ('.' !== $part) {
                $res[] = $part;
            }
        }

        return implode('.', $res);
    }
}
