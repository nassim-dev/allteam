<?php

namespace core\view\latte;

use Latte\MacroNode;

/**
 * Class LatteNodeTree
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class LatteNodeTree
{
    /**
     * Nodes ids
     * @var array
     */
    public $ids = [];

    /**
     * Initial nodes
     *
     * @var MacroNode[]
     */
    private array $initialNode = [];

    /**
     * Generated nodes
     * @var LatteNode[]
     */
    private array $nodes = [];

    public function addNode(MacroNode $node): LatteNode
    {
        $id                     = Utils::hash(spl_object_hash($node));
        $this->initialNode[$id] = $node;
        $this->ids[]            = $id;

        return $this->nodes[$id] = new LatteNode($node, $this);
    }

    public function getRelatedInitialNode(string $id): ?MacroNode
    {
        return $this->initialNode[$id] ?? null;
    }

    public function getRelatedNode(string $id): ?LatteNode
    {
        return $this->nodes[$id] ?? null;
    }

    public function setKeys(string $nodeId, array $keys)
    {
        if (isset($this->nodes[$nodeId])) {
            $this->nodes[$nodeId]->setKeys($keys);
        }
    }
}
