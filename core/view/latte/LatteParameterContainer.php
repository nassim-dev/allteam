<?php

namespace core\view\latte;

use core\html\HtmlException;

/**
 * Class LatteParameterContainer
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class LatteParameterContainer
{
    /**
     * Node tree
     */
    private static \core\view\latte\LatteNodeTree $_NODE_TREE_;

    private static array $_USED_KEYS_ = [];

    public function __construct(private array $_PARAMETERS_, ?LatteNodeTree $latteNodeTree)
    {
        self::$_NODE_TREE_ = $latteNodeTree;
    }

    /**
     * @function
     * @param $name
     */
    public function get(string $name)
    {
        if (isset($this->_PARAMETERS_[$name])) {
            self::$_USED_KEYS_['($this->global->fn->get)(\'' . $name . '\')'] = $this->_PARAMETERS_[$name];

            return $this->_PARAMETERS_[$name];
        }

        throw new HtmlException("Innexisting parameter $name");
    }

    /**
     * Get the value of _USED_KEYS_
     *
     * @PhpUnitGen\get("_USED_KEYS_")
     * @function
     */
    public static function getKeys(): array
    {
        return self::$_USED_KEYS_;
    }

    /**
     * Set the value of _USED_KEYS_
     *
     * @PhpUnitGen\set("_USED_KEYS_")
     * @function
     *
     * @return static
     * @param  mixed[] $_USED_KEYS_
     */
    public static function setKeys(array $_USED_KEYS_)
    {
        self::$_USED_KEYS_ = $_USED_KEYS_;
    }
}
