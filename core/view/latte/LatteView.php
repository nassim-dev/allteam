<?php

namespace core\view\latte;

use core\cache\CacheServiceInterface;
use core\cache\CacheUpdaterTrait;
use core\config\Conf;
use core\DI\DiProvider;
use core\html\AssetsServiceInterface;
use core\html\HtmlElementInterface;
use core\logger\view\TracyViewPanel;
use core\messages\request\HttpRequestInterface;
use core\secure\authentification\AuthServiceInterface;
use core\view\ViewServiceInterface;
use Latte\Engine;
use Tracy\Debugger;

/**
 * Class LatteView
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class LatteView extends Engine implements ViewServiceInterface
{
    use CacheUpdaterTrait;
    use DiProvider;

    public const HTML_COMPONENT_KEY   = 'HTML_COMPONENTS';
    public const UNIQUE_COMPONENT_KEY = 'UNIQUE_COMPONENTS';


    /**
     * Parameters to assign
     */
    private array $params = [self::HTML_COMPONENT_KEY => []];

    private array $templatesNames = [];

    /**
     * @param AssetsInterface $request
     */
    public function __construct(
        private AuthServiceInterface $auth,
        private HttpRequestInterface $request,
        private AssetsServiceInterface $assets,
        private CacheServiceInterface $cache
    ) {
        $this->_fetchPropsFromCache(['templatesNames']);
        parent::__construct();

        if (!is_dir(BASE_DIR . 'tmp/views')) {
            mkdir(BASE_DIR . 'tmp/views', 777, true);
        }

        $this->setTempDirectory(BASE_DIR . 'tmp/views');
        $this->assign('BASE_URL', Conf::find('PHP.HTTP_PROTOCOL') . '://' . Conf::find('PHP.SERVER_NAME'));

        if ($this->request->isAjaxify()) {
            $this->assign('isAjaxify', true);
        }

        $this->generateCustomFilters();

        if (Conf::find('ENVIRONNEMENT') === 'development' && CONTEXT === 'APP' && !isset($_SERVER['x-requested-with'])) {
            Debugger::getBar()->addPanel(new TracyViewPanel($this));
        }
    }

    public function __destruct()
    {
        $this->_cacheMultiplesProps(['templatesNames']);
    }


    public function getTemplateClass(string $name): string
    {
        if (!isset($this->templatesNames[$name])) {
            $this->templatesNames[$name] = parent::getTemplateClass($name);
        }

        return $this->templatesNames[$name];
    }


    /**
     * Return all affected values
     */
    public function getParameters(): array
    {
        return $this->params;
    }

    /**
     * Assign variables in template
     *
     * @param string $varname
     * @param mixed  $var
     * @param bool   $cache   unused in this implementation
     *
     * @return void
     */
    public function assign($varname, $var = null, $cache = false)
    {
        $this->params[$varname] = $var;
    }

    /**
     * Assign widget to view
     *
     * @return void
     */
    public function assignWidget(HtmlElementInterface $widget)
    {
        if ($widget->hasParent()) {
            $this->assignWidget($widget->getParent());
            $this->removeWidget($widget);
        } else {
            $container = $widget->getViewReference() . 's';
            if (!isset($this->params[self::HTML_COMPONENT_KEY][$container])) {
                $this->params[self::HTML_COMPONENT_KEY][$container] = [];
            }

            $this->params[self::HTML_COMPONENT_KEY][$container][$widget->getIdAttribute()] = $widget;
        }
    }

    /**
     * Assign widget as child of specific element
     *
     * @return void
     */
    public function disposeWidget(HtmlElementInterface $widget, string $idParent)
    {
        if ($widget->hasParent()) {
            $widget->removeFrom($widget->getParent());
        }

        $this->removeWidget($widget);

        if (!isset($this->params[self::UNIQUE_COMPONENT_KEY]["#$idParent"])) {
            $this->params[self::UNIQUE_COMPONENT_KEY]["#$idParent"] = [];
        }

        $this->params[self::UNIQUE_COMPONENT_KEY]["#$idParent"][] = $widget;
    }

    /**
     * Get child widgets for specific element
     */
    public function getChildsFor(string $idParent): ?array
    {
        return $this->params[self::UNIQUE_COMPONENT_KEY]["#$idParent"] ?? null;
    }

    /**
     * Return template compiled
     *
     * @param string|null $idcache unused in this implementation
     */
    public function render(
        string $template,
        $params = [],
        ?string $block = null
    ): void {
        parent::render($template, (!empty($params)) ? $params : $this->params, $block);
    }

    /**
     * Get template compiled as string
     *
     * @param string      $template
     * @param array       $params
     * @param string|null $block    render specific block
     *
     * @return string
     */
    public function fetch(string $template, array $params = [], string $block = null): string
    {
        return $this->renderToString($template, (!empty($params)) ? $params : $this->params, $block);
    }

    public function removeWidget(HtmlElementInterface $widget)
    {
        $container = $widget->getViewReference() . 's';
        if (isset($this->params[self::HTML_COMPONENT_KEY][$container], $this->params[$container][$widget->getIdAttribute()])) {
            unset($this->params[self::HTML_COMPONENT_KEY][$container][$widget->getIdAttribute()]);
        }
    }

    /**
     * Unused in this implementation
     *
     * @todo remove && change ViewServiceInterface
     */
    public function setTemplateDir(string $dir)
    {
    }

    /**
     * @return mixed
     */
    private function generateCustomFilters()
    {
        $this->addFilter(
            'attributize',
            function (array $attributes): string {
                $return = '';
                foreach ($attributes as $key => $value) {
                    $return .= "data-$key=$value ";
                }

                return $return;
            }
        );

        $this->addFunction(
            'count',
            fn ($args) => is_countable($args) ? count($args) : 0
        );
    }
}
