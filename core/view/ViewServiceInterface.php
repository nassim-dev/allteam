<?php

namespace core\view;

use core\html\HtmlElementInterface;

/**
 * Interface ViewServiceInterface
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
interface ViewServiceInterface
{
    /**
     * Assign variables in template
     *
     * @param string $varname
     * @param mixed  $var
     * @param bool   $cache
     *
     * @return void
     */
    public function assign($varname, $var = null, $cache = false);

    /**
     * Assign widget to view
     *
     * @return void
     */
    public function assignWidget(HtmlElementInterface $widget);

    /**
     * Assign widget as child of specific element
     *
     * @return void
     */
    public function disposeWidget(HtmlElementInterface $widget, string $idParent);

    /**
     * Get child widgets for specific element
     */
    public function getChildsFor(string $idParent): ?array;

    public function render(
        string $template,
        $params = [],
        ?string $block = null
    ): void;

    /**
     * Renders template to string.
     *
     * @param object|array $params
     */
    public function fetch(string $template, array $params = [], string $block = null): string;

    /**
     * Set tyemplate directory
     *
     * @return void
     */
    public function setTemplateDir(string $dir);

    /**
     * Return all affected values
     */
    public function getParameters(): array;
}
