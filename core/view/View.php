<?php namespace core\view;

/**
 * Class View
 *
 *  @method static string assign(string $varname, string $var, string $cache)
 *  @method static string assignWidget(core\html\HtmlElementInterface $widget)
 *  @method static string disposeWidget(core\html\HtmlElementInterface $widget, string $idParent)
 *  @method static array getChildsFor(string $idParent)
 *  @method static void render(string $template, string $params, string $block)
 *  @method static string fetch(string $template, array $params, string $block)
 *  @method static string setTemplateDir(string $dir)
 *  @method static array getParameters()
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class View extends \core\facade\HelperFacade
{
    protected static ?string $serviceInterface = 'core\view\ViewServiceInterface';
}
