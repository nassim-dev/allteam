<?php

namespace core\view;

use core\DI\DiProvider;

/**
 * Trait ViewProvider
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait ViewProvider
{
    use DiProvider;

    /**
     * ViewServiceInterface implémentation
     *
     * @var ViewServiceInterface
     */
    private $view;

    /**
     * Return ViewServiceInterface implémetation
     */
    public function getView(): ViewServiceInterface
    {
        return $this->view ??= $this->getDi()->singleton(ViewServiceInterface::class);
    }
}
