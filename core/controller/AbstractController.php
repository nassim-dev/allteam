<?php

namespace core\controller;

use core\DI\DiProvider;
use core\messages\request\HttpRequestInterface;
use core\messages\request\RequestParameterTrait;
use core\messages\response\HttpResponseInterface;
use core\secure\authentification\AuthServiceInterface;
use core\view\ViewServiceInterface;

/**
 * Class AbstractController
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
abstract class AbstractController implements ControllerInterface
{
    use RequestParameterTrait;
    use DiProvider;

    /**
     * Is that a public controller
     *
     * @var boolean
     */
    protected $_public = false;

    /**
     * Page html template
     */
    private ?string $_pageTemplate = null;

    /**
     * Is a popup Page?
     */
    private bool $_popup = false;

    public function __construct(
        protected AuthServiceInterface $auth,
        protected HttpRequestInterface $request,
        protected HttpResponseInterface $response,
        protected ViewServiceInterface $view
    ) {
        $this->auth->setControllerMode($this->isPublic());
    }

    /**
     * Get page html template
     *
     * @PhpUnitGen\get("_pageTemplate")
     */
    public function getPageTemplate(): ?string
    {
        return $this->_pageTemplate;
    }

    /**
     * Get is a popup Page?
     *
     * @PhpUnitGen\get("_popup")
     */
    public function isInPopup(): bool
    {
        return $this->_popup;
    }

    /**
     * Get is that a public controller
     *
     * @PhpUnitGen\get("public")
     */
    public function isPublic(): bool
    {
        return $this->_public;
    }

    /**
     * Set page html template
     *
     * @PhpUnitGen\set("_pageTemplate")
     *
     * @param string|null $pageTemplate Page html template
     */
    public function setPageTemplate(?string $pageTemplate): self
    {
        $this->_pageTemplate = $pageTemplate;

        return $this;
    }

    /**
     * Set is a popup Page?
     *
     * @PhpUnitGen\set("_popup")
     *
     * @param bool $popup Is a popup Page?
     */
    public function setPopup(bool $popup): self
    {
        $this->_popup = $popup;

        return $this;
    }

    /**
     * Set is that a public controller
     *
     * @PhpUnitGen\set("public")
     *
     * @param bool $public Is that a public controller
     */
    public function setPublic(bool $public): self
    {
        $this->_public = $public;

        return $this;
    }

    /**
     * Get the value of response
     *
     * @PhpUnitGen\get("response")
     *
     * @return HttpResponseInterface
     */
    public function getResponse(): HttpResponseInterface
    {
        return $this->response;
    }
}
