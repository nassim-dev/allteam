<?php namespace core\controller;

/**
 * Class Controller
 *
 *  @method static string getClass(string $identifier, string $contextPriority)
 *  @method static string registerMenuActions(string $name, string $icon)
 *  @method static string create(string $identifier, array $parameters)
 *  @method static string get(string $identifier)
 *  @method static core\containers\ContainerInterface getInstances(callable $filter)
 *  @method static string remove(string $identifier)
 *  @method static string registerClass(string $class)
 *  @method static string addNamespace(string $namespace)
 *  @method static string getNamespaces()
 *  @method static string register(string $object, string $identifier)
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class Controller extends \core\facade\HelperFacade
{
    protected static ?string $serviceInterface = 'core\controller\ControllerServiceInterface';
}
