<?php

namespace core\controller;

use core\factory\FactoryInterface;

/**
 * Interface ControllerServiceInterface
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
interface ControllerServiceInterface extends FactoryInterface
{
    /**
     * Get class name if exist ot throw error
     *
     * @throws FactoryException
     */
    public function getClass(string $identifier, ?string $contextPriority = null): ?string;

    /**
     * Register all menu action for app
     *
     * @return void
     */
    public function registerMenuActions(string $name, string $icon);
}
