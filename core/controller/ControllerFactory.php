<?php

namespace core\controller;

use core\cache\CacheServiceInterface;
use core\cache\CacheUpdaterTrait;
use core\containers\ContainerInterface;
use core\containers\DefaultContainer;
use core\factory\AbstractFactory;
use core\factory\FactoryException;
use core\factory\FactoryInterface;
use core\routing\MenuRegisterTrait;
use core\utils\ClassFinder;

/**
 * Class ControllerFactory
 *
 * Controller factory
 *
 * @category  Controller factory
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class ControllerFactory extends AbstractFactory implements ControllerServiceInterface, FactoryInterface
{
    use CacheUpdaterTrait;
    use MenuRegisterTrait;

    public const DEFAULT_CONTROLLER = 'Default';

    /**
     * Main app controller
     */
    private ?array $_controllers = null;

    public function __construct(protected CacheServiceInterface $cache)
    {
        //$this->cache->invalidate(md5(static::class) . '._routes');
        //$this->cache->invalidate(md5(static::class) . '._controllers');
        $this->_fetchPropsFromCache(['_routes', '_controllers']);
        $this->addNamespace('app\controller');
        $this->addNamespace('app\api\controller');
    }

    public function __destruct()
    {
        $this->_cacheMultiplesProps(['_routes', '_controllers']);
    }

    /**
     * Get associated controllers
     *
     * @PhpUnitGen\get("controllers")
     */
    final public function getControllers(): ?array
    {
        if (is_array($this->_controllers)) {
            return $this->_controllers;
        }

        /** @var ClassFinder $finder */
        $finder = $this->getDi()->singleton(ClassFinder::class);

        $this->_controllers = $finder->findClasses('app\controller', ClassFinder::RECURSIVE_MODE) ?? [];

        return $this->_controllers = array_merge($this->_controllers, $finder->findClasses('app\api\controller', ClassFinder::RECURSIVE_MODE) ?? []);
    }

    /**
     * Request unregistred controller
     *
     * @param mixed[] $params
     */
    public function create(string $controller, array $params = []): ?ControllerInterface
    {
        $controller = $this->getClass($controller);
        if ($controller === null) {
            return null;
        }

        return $this->getInstance($controller) ?? $this->register($this->getDi()->singleton($controller, $params), $controller);
    }

    /**
     * Get all instances or filtered by callback
     *
     * @PhpUnitGen\get("instances")
     */
    public function getInstances(?callable $filter = null): ContainerInterface
    {
        $this->instances ??= new DefaultContainer();
        if (!null === $filter) {
            $entries = array_filter($this->instances->toArray(), $filter);

            return new DefaultContainer($entries);
        }

        return $this->instances;
    }

    /**
     * Get registred elements
     */
    public function get(string $identifier): ?ControllerInterface
    {
        return $this->getInstance($identifier) ?? $this->create($identifier);
    }

    /**
     * Get class name if exist ot throw error
     *
     * @throws FactoryException
     */
    public function getClass(string $identifier, ?string $contextPriority = 'app'): ?string
    {
        if (class_exists($identifier)) {
            return $identifier;
        }

        if (str_ends_with($identifier, '_parent')) {
            $identifier = str_replace('_parent', '', $identifier);
        }

        $passedNamespaces = [];
        foreach ($this->getNamespaces() as $namespace) {
            if (!str_contains($namespace, $contextPriority)) {
                $passedNamespaces[] = $namespace;

                continue;
            }

            $defaultIdentifier = $namespace . '\\' . ucfirst($identifier) . 'Controller';
            if (class_exists($defaultIdentifier)) {
                return $defaultIdentifier;
            }
        }

        foreach ($passedNamespaces as $namespace) {
            $defaultIdentifier = $namespace . '\\' . ucfirst($identifier) . 'Controller';
            if (class_exists($defaultIdentifier)) {
                return $defaultIdentifier;
            }
        }

        return null;

        //throw new FactoryException("$identifier can not be instanciate, because it's not a class");
    }
}
