<?php

namespace core\controller;

use core\html\AssetsServiceInterface;
use core\messages\response\HttpResponseInterface;
use core\transport\TransportServiceInterface;

/**
 * Interface ControllerInterface
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
interface ControllerInterface
{
    /**
     * Get is that a public controller
     */
    public function isPublic(): bool;

    /**
     *  Render the view
     *
     * @param AssetsServiceInterface    $javascript
     * @param TransportServiceInterface $transport
     *
     * @return void
     */
    public function render(
        AssetsServiceInterface $javascript,
        TransportServiceInterface $transport
    ): self;

    /**
     * Get the value of response
     *
     * @PhpUnitGen\get("response")
     *
     * @return HttpResponseInterface
     */
    public function getResponse(): HttpResponseInterface;


    /**
     * Undocumented function
     *
     * @return HttpResponseInterface
     */
    public function display(): HttpResponseInterface;
}
