<?php

namespace core\utils;

use core\cache\CacheServiceInterface;
use core\cache\CacheUpdaterTrait;
use HaydenPierce\ClassFinder\ClassFinder as ClassFinderClassFinder;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class ClassFinder extends ClassFinderClassFinder
{
    use CacheUpdaterTrait;

    private $_classes;

    private $_traits;

    private $_methods;

    public function __construct(private CacheServiceInterface $cache)
    {
        $this->cache->setCachePrefix('DEFAULT');
        $this->_fetchPropsFromCache(['_classes', '_traits', '_methods']);
        $this->cache->setCachePrefix(CONTEXT);

        if (!is_array($this->_classes)) {
            $this->_classes = [];
        }

        if (!is_array($this->_traits)) {
            $this->_traits = [];
        }

        if (!is_array($this->_methods)) {
            $this->_methods = [];
        }
    }

    public function __destruct()
    {
        $this->cache->setCachePrefix('DEFAULT');
        $this->_cacheMultiplesProps(['_classes', '_traits', '_methods']);
        $this->cache->setCachePrefix(CONTEXT);
    }

    /**
     * Identify classes in a given namespace.
     *
     * @param  string   $namespace
     * @param  int      $options
     * @return string[]
     *
     * @throws \Exception
     */
    public function findClasses($namespace, $options = self::STANDARD_MODE)
    {
        if (isset($this->_classes[$namespace][$options])) {
            return $this->_classes[$namespace][$options];
        }

        return $this->_classes[$namespace][$options] = self::getClassesInNamespace($namespace, $options);
    }

    /**
     * Return all methods for object trait
     *
     * @return array
     */
    public function getTraitsMethods(object $object, string $filter = '__ALL__')
    {
        if (isset($this->_methods[$object::class][$filter])) {
            return $this->_methods[$object::class][$filter];
        }

        $traits = $this->getTraits($object);
        $return = [];
        foreach ($traits as $trait) {
            if ($filter !== '__ALL__') {
                $methods = get_class_methods($trait);
                foreach ($methods as $method) {
                    if (str_contains($method, '_toArray')) {
                        $return[] = $method;
                    }
                }

                continue;
            }

            $return = array_merge($return, get_class_methods($traits));
        }

        return $this->_methods[$object::class][$filter] = $return;
    }

    public function getTraits(object $object)
    {
        $class = $object::class;
        if (isset($this->_traits[$class])) {
            return $this->_traits[$class];
        }

        return $this->_traits[$class] = Utils::class_uses_deep($object);
    }
}
