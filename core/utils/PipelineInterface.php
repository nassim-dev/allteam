<?php

namespace core\utils;

/**
 * Undocumented interface
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
interface PipelineInterface
{
    public function execute(...$parameters);
    public function getResult(): mixed;
    public function getParent(): ?Pipeline;
    public function setParent(?Pipeline $parent): static;
    public function getPrevious(): ?PipelineInterface;
    public function setPrevious(?PipelineInterface $pipeline): static;
}
