<?php

namespace core\utils;

use core\DI\ProxyInterface;
use core\utils\traits\UtilsArrayTrait;
use core\utils\traits\UtilsCallableTrait;
use core\utils\traits\UtilsDateTrait;
use core\utils\traits\UtilsEncryptionTrait;
use core\utils\traits\UtilsFileTrait;
use core\utils\traits\UtilsHashTrait;
use core\utils\traits\UtilsTextTrait;

/**
 * Class Utils
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class Utils
{
    use UtilsArrayTrait;
    use UtilsTextTrait;
    use UtilsEncryptionTrait;
    use UtilsDateTrait;
    use UtilsFileTrait;
    use UtilsCallableTrait;
    use UtilsHashTrait;

    public static function getClass(object $class)
    {
        if (in_array(ProxyInterface::class, class_implements($class))) {
            $class::_getParentClass();
        }

        return $class::class;
    }
}
