<?php

namespace core\utils\traits;

/**
 * Class UtilsArrayTrait
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait UtilsArrayTrait
{
    public static function array_key_exists_recursive($key, $array)
    {
        if (array_key_exists($key, $array)) {
            return true;
        } else {
            foreach ($array as $value) {
                if (is_array($value) && self::array_key_exists_recursive($key, $value)) {
                    return true;
                }
            }
        }

        return false;
    }

    public static function arrayRecursiveDiff($aArray1, $aArray2)
    {
        $aReturn = [];

        foreach ($aArray1 as $mKey => $mValue) {
            if (array_key_exists($mKey, $aArray2)) {
                if (is_array($mValue)) {
                    $aRecursiveDiff = self::arrayRecursiveDiff($mValue, $aArray2[$mKey]);
                    if (count($aRecursiveDiff)) {
                        $aReturn[$mKey] = $aRecursiveDiff;
                    }
                } else {
                    if ($mValue != $aArray2[$mKey]) {
                        $aReturn[$mKey] = $mValue;
                    }
                }
            } else {
                $aReturn[$mKey] = $mValue;
            }
        }

        return $aReturn;
    }

    /**
     * Execute function on array
     *
     * @param callable $function
     *
     * @return array
     */
    public static function array_map_recursive($function, array $array): array
    {
        $out = [];
        foreach ($array as $key => $val) {
            $out[$key] = $val;
            if (!is_object($val)) {
                $out[$key] = (is_array($val)) ? self::array_map_recursive($function, $val) : $function($val);
            }
        }

        return $out;
    }

    /**
     * Recursive htmlspecialchars_decode
     *
     * @return array
     */
    public static function htmlspecialchars_decode_recursive(array $array): array
    {
        $out = [];
        foreach ($array as $key => $val) {
            if (null === $val) {
                $val = '';
            }

            $out[$key] = $val;
            if (!is_object($val)) {
                $out[$key] = (is_array($val)) ? self::htmlspecialchars_decode_recursive($val) : htmlspecialchars_decode($val);
            }
        }

        return $out;
    }

    /**
     * Recursive html_entity_decode
     *
     * @return array
     */
    public static function html_entity_decode_recursive(array $array): array
    {
        $out = [];
        foreach ($array as $key => $val) {
            if (null === $val) {
                $val = '';
            }

            $out[$key] = $val;
            if (!is_object($val)) {
                $out[$key] = (is_array($val)) ? self::html_entity_decode_recursive($val) : html_entity_decode($val);
            }
        }

        return $out;
    }

    /**
     * Sort array by column
     *
     * @param array  $arr
     * @param string $col
     *
     * @return void
     */
    public static function array_sort_by_column(&$arr, $col)
    {
        $sortedColumns = [];
        foreach ($arr as $key => $row) {
            $sortedColumns[$key] = $row[$col];
        }
    }

    /**
     * @param $class
     * @param $autoload
     */
    public static function class_uses_deep($class, $autoload = true): array
    {
        $traits = [];
        do {
            $traits = array_merge(class_uses($class, $autoload), $traits);
        } while ($class = get_parent_class($class));

        foreach ($traits as $trait => $same) {
            $traits = array_merge(class_uses($trait, $autoload), $traits);
        }

        return array_unique($traits);
    }

    /**
     * Sort array based on time value
     *
     * @param mixed $timeA
     * @param mixed $timeB
     *
     * @return void
     */
    public static function sortFunction($timeA, $timeB)
    {
        return strtotime(key($timeA)) - strtotime(key($timeB));
    }

    public static function arrayPrinter(mixed $object, array $filtered = []): mixed
    {
        $ret = [];
        if (is_array($object) && empty($object)) {
            return $object;
        } elseif (is_object($object)) {
            $object = get_object_vars($object);
        } elseif (!is_iterable($object)) {
            return $object;
        }

        foreach ($object as $k => $v) {
            $k = (!is_int($k) && '_' === $k[0]) ? substr($k, 1) : $k;
            if (in_array($k, $filtered)) {
                continue;
            }

            $ret[$k] = match (gettype($v)) {
                'NULL'      => null,
                'string'    => htmlspecialchars_decode($v, ENT_QUOTES),
                'object'    => self::arrayPrinter($v),
                'ressource' => null,
                default     => $v,
            };
        }

        return $ret;
    }
}
