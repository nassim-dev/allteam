<?php

namespace core\utils\traits;

use core\config\Conf;

/**
 * Class UtilsEncryptionTrait
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait UtilsEncryptionTrait
{
    /**
     * Decode raw url with parmeters
     *
     * @return void
     */
    public static function decodeRawVar(string $str, bool $encode = false)
    {
        if (!$encode) {
            return $str;
        }

        $str = self::decrypt($str);

        $vars   = explode('&', $str);
        $return = [];
        foreach ($vars as $var) {
            $var = explode('=', $var);
            if (isset($var[1])) {
                $return[$var[0]] = $var[1];
            }
        }

        return $return;
    }

    /**
     * Decrypt method with ssl
     *
     * @todo enforce security
     *
     * @param $string
     */
    public static function decrypt($string): bool|string
    {
        $encrypt_method = Conf::find('SECURITY.ENCRYPT.METHOD');
        $secret_key     = Conf::find('SECURITY.ENCRYPT.KEY');
        $secret_iv      = Conf::find('SECURITY.ENCRYPT.IV');

        // hash
        $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        $base64 = base64_decode($string);
        $output = urldecode(openssl_decrypt($base64, $encrypt_method, $key, 0, $iv));

        return str_replace('|TOO_SHORT|', '', $output);
    }

    /**
     * Encrypt method with ssl
     *
     * @todo enforce security
     *
     * @param $string
     */
    public static function encrypt($string): bool|string
    {
        $encrypt_method = Conf::find('SECURITY.ENCRYPT.METHOD');
        $secret_key     = Conf::find('SECURITY.ENCRYPT.KEY');
        $secret_iv      = Conf::find('SECURITY.ENCRYPT.IV');

        // hash
        $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);

        return urlencode(base64_encode($output));
    }

    /**
     * @deprecated implements __sleep && __wakeup methods on specific objects
     *
     * @param $values
     *
     * @return mixed
     */
    public static function removeCallbackBeforeSerialize($values)
    {
        if (is_array($values)) {
            return array_map(function ($value) {
                if (is_array($value)) {
                    return self::removeCallbackBeforeSerialize($value);
                }

                if (is_callable($value) || is_resource($value)) {
                    return null;
                }

                if (is_object($value)) {
                    return self::cleanObjectBeforeSerialize($value);
                }

                return $value;
            }, $values);
        }

        if (is_object($values)) {
            return self::cleanObjectBeforeSerialize($values);
        }

        if (is_callable($values) || is_resource($values)) {
            return null;
        }

        return $values;
    }

    /**
     * @param $val
     */
    public static function secure($val): array|string
    {
        if (is_array($val)) {
            return array_map('core\utils\Utils::secure', $val);
        }

        return (is_object($val)) ? $val : htmlspecialchars($val, ENT_QUOTES, 'UTF-8');
    }

    /**
     * Better serialize function
     *
     * @return void
     */
    public static function serialize(mixed $value): string
    {
        $tmp = \igbinary_serialize($value);

        return base64_encode($tmp);
    }

    /**
     * Better unserialize function
     *
     * @return mixed
     */
    public static function unserialize(string $value)
    {
        $tmp = base64_decode($value);

        return \igbinary_unserialize($tmp);
    }

    /**
     * Serialize object
     *
     * @deprecated implements __sleep && __wakeup methods on specific objects
     *
     * @return mixed
     */
    private static function cleanObjectBeforeSerialize($object)
    {
        $tmp = get_object_vars($object);

        foreach ($tmp as $property => $value) {
            if (is_callable($value) || is_resource($value)) {
                $object->{$property} = null;

                continue;
            }

            if ($property === 'di' || $property === 'diWrapper') {
                $object->{$property} = null;
            }

            if (!property_exists($object, $property)) {
                $object->{$property} = null;
            }
        }

        return $object;
    }
}
