<?php

namespace core\utils\traits;

use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

/**
 * Class UtilsFileTrait
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait UtilsFileTrait
{
    /**
     * Convert Bytes to readable unit
     *
     * @param int $bytes
     *
     * @return void
     */
    public static function fileSizeConvert($bytes)
    {
        $result  = null;
        $bytes   = floatval($bytes);
        $arBytes = [
            0 => [
                'UNIT'  => 'TB',
                'VALUE' => 1024 ** 4
            ],
            1 => [
                'UNIT'  => 'GB',
                'VALUE' => 1024 ** 3
            ],
            2 => [
                'UNIT'  => 'MB',
                'VALUE' => 1024 ** 2
            ],
            3 => [
                'UNIT'  => 'KB',
                'VALUE' => 1024
            ],
            4 => [
                'UNIT'  => 'B',
                'VALUE' => 1
            ]
        ];

        foreach ($arBytes as $arItem) {
            if ($bytes >= $arItem['VALUE']) {
                $result = $bytes / $arItem['VALUE'];
                $result = str_replace('.', ',', strval(round($result, 2))) . ' ' . $arItem['UNIT'];

                break;
            }
        }

        return $result;
    }

    /**
     * Get Size of directory
     *
     * @return void
     */
    public static function dirSize(string $directory)
    {
        $size = 0;
        foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($directory)) as $file) {
            $size += $file->getSize();
        }

        return $size;
    }

    /**
     * Create directory if innexisting
     *
     * @return void
     */
    public static function mkdir(string $dir, int $permissions = 0755)
    {
        if (!file_exists($dir)) {
            mkdir($dir, $permissions, true);
        }
    }

    /**
     * Delete directory and sub-directory
     */
    public static function rmFiles(string $dossier)
    {
        $dir_iterator = new RecursiveDirectoryIterator($dossier);
        $iterator     = new RecursiveIteratorIterator($dir_iterator, RecursiveIteratorIterator::CHILD_FIRST);

        $fichierTrouve = 0;
        if (is_dir($dossier) && ($dh = opendir($dossier))) {
            while (($file = readdir($dh)) !== false && 0 === $fichierTrouve) {
                if ('.' !== $file && '..' !== $file) {
                    $fichierTrouve = 1;
                }
            }

            closedir($dh);
        }

        if ($fichierTrouve > 0) {
            foreach ($iterator as $fichier) {
                $fichier->isDir() ? rmdir($fichier) : unlink($fichier);
            }
        }
    }
}
