<?php

namespace core\utils\traits;

/**
 * Trait UtilsHashTrait
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
trait UtilsHashTrait
{
    public static function hash(mixed $var): ?string
    {
        $hashArray = match (gettype($var)) {
            'boolean'      => $var,
            'integer'      => $var,
            'double'       => $var,
            'string'       => $var,
            'array'        => self::_hashArray($var),
            'object'       => self::_hashObject($var),
            'resource'     => self::_hashRessource($var),
            'NULL'         => $var,
            'unknown type' => $var,
        };

        $hashArray = (is_array($hashArray)) ? implode('.', $hashArray) : $hashArray;

        return null === $hashArray ? null : md5($hashArray);
    }

    /**
     * Hassh array element
     */
    private static function _hashArray(array $var): array
    {
        array_multisort($var);
        $return = [];
        foreach ($var as $element) {
            $return = self::_selectHasher($return, $element);
        }

        return $return;
    }

    /**
     * Hash object
     */
    private static function _hashObject(object $var): array
    {
        $return = [];
        $vars   = get_object_vars($var);
        foreach ($vars as $val) {
            $return = self::_selectHasher($return, $val);
        }

        return $return;
    }


    /**
     * Hash ressource
     *
     * @param ressource $var
     */
    private static function _hashRessource($var): int
    {
        return 'resource:' . get_resource_id($var);
    }

    private static function _selectHasher(array $return, $element): array
    {
        if (is_array($element)) {
            return array_merge($return, self::_hashArray($element));
        }

        if (is_object($element)) {
            return array_merge($return, self::_hashObject($element));
        }

        if (is_resource($element)) {
            $return[] = self::_hashRessource($element);

            return $return;
        }

        $return[] = $element;

        return $return;
    }
}
