<?php

namespace core\utils\traits;

use core\session\Session;
use DateInterval;
use DateTimeInterface;
use Nette\Utils\DateTime;

/**
 * Class UtilsDateTrait
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
trait UtilsDateTrait
{
    /**
     * Return Datetime object
     */
    public static function createDateTime(string|DateTimeInterface|null $elementValue): ?DateTimeInterface
    {
        if (is_object($elementValue)) {
            return $elementValue;
        }

        if ($elementValue === null) {
            return $elementValue;
        }

        $date = DateTime::createFromFormat('d/m/Y H:i', $elementValue);
        if (is_bool($date)) {
            $date = DateTime::createFromFormat('his', $elementValue);
        }

        if (is_bool($date)) {
            $date = DateTime::createFromFormat('d/m/Y', $elementValue);
        }

        if (is_bool($date)) {
            $date = DateTime::createFromFormat('d/m/Y H:i:s', $elementValue);
        }

        if (is_bool($date)) {
            $date = DateTime::createFromFormat('d/m/Y - H:i', $elementValue);
        }

        if (is_bool($date)) {
            $date = DateTime::createFromFormat('d-m-Y H:i', $elementValue);
        }

        if (is_bool($date)) {
            $date = DateTime::createFromFormat('d-m-Y', $elementValue);
        }

        if (is_bool($date)) {
            $date = DateTime::createFromFormat('d-m-Y H:i:s', $elementValue);
        }

        if (is_bool($date)) {
            $date = DateTime::createFromFormat('H:i', $elementValue);
        }

        if (is_bool($date)) {
            $date = DateTime::createFromFormat('H:i:s', $elementValue);
        }

        if (is_bool($date)) {
            $date = DateTime::createFromFormat('Y-m-d H:i:s', $elementValue);
        }

        if (is_bool($date)) {
            $date = DateTime::createFromFormat('Y-m-d', $elementValue);
        }

        if (is_bool($date)) {
            $date = DateTime::createFromFormat('Y-m-d H:i', $elementValue);
        }

        if (is_bool($date)) {
            $date = null;
        }

        return $date;
    }

    public static function dateDiff(string|DateTimeInterface $date1, string|DateTimeInterface $date2): bool|DateInterval
    {
        $datetime1 = (is_string($date1)) ? new DateTime($date1) : $date1;
        $datetime2 = (is_string($date2)) ? new DateTime($date2) : $date2;

        return $datetime1->diff($datetime2);
    }

    /**
     * @param $time
     *
     * @return string
     */
    public static function dateToCal($time)
    {
        return date('Ymd\This', $time) . 'Z';
    }

    public static function formatDate(string|DateTimeInterface $date, ?string $format = null): ?string
    {
        $date = (is_object($date)) ? $date : self::createDateTime($date);

        if ($format === null) {
            $format = self::getDateFormat();
        }

        return (null === $date) ? $date : $date->format($format);
    }

    public static function getDateFormat(): string
    {
        return match (Session::get('CONTEXT.LANGUAGE')) {
            'fr'    => 'd/m/Y H:i',
            'en'    => 'm/d/Y H:i',
            'es'    => 'd/m/Y H:i',
            'ge'    => 'd/m/Y H:i',
            default => 'm/d/Y H:i'
        };
    }


    /**
     * Test difference of interval
     */
    public static function getIntervalDifference(DateTimeInterface $start, DateTimeInterface $end, DateTimeInterface $testedStart, DateTimeInterface $testedEnd): bool|DateInterval
    {
        if ($testedEnd >= $start && $testedStart < $start) {
            return $testedStart->diff($start);
        } elseif ($testedStart <= $end && $testedEnd > $end) {
            return $end->diff($testedEnd);
        }

        return false;
    }
}
