<?php

namespace core\utils\traits;

use Misd\Linkify\Linkify;

/**
 * Class UtilsTextTrait
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait UtilsTextTrait
{
    /**
     * Return bold value
     *
     * @param string $value
     *
     * @return string|mixed
     */
    public static function bold($value)
    {
        return (is_string($value)) ? "<p class='h6'><u>" . $value . '</u></p>' : $value;
    }

    /**
     * Generate a slug
     *
     * @param sting  $string
     * @param string $separator
     *
     * @return void
     */
    public static function generateClassifiedSlug(string $string, $separator = '-')
    {
        $accents_regex = '~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i';
        $special_cases = ['&' => 'and', "'" => ''];
        $string        = mb_strtolower(trim($string), 'UTF-8');
        $string        = str_replace(array_keys($special_cases), array_values($special_cases), $string);
        $string        = preg_replace($accents_regex, '$1', htmlentities($string, ENT_QUOTES, 'UTF-8'));
        $string        = preg_replace('/[^a-z0-9]/u', "$separator", $string);
        $string        = preg_replace("/[$separator]+/u", "$separator", $string);

        return $string;
    }

    /**
     * Hide Id inb html
     *
     * @todo merge with hide()
     */
    public static function hiddenId(null|int|string $ref): string
    {
        return "<span class='d-none' data-id='" . $ref . "'></span>";
    }

    /**
     * @todo merge with hiddenId()
     */
    public static function hide(int|string $element)
    {
        return "<div class='hide'>" . $element . ';</div>';
    }

    /**
     * @param $s
     *
     * @return string
     */
    public static function pad($s)
    {
        if ('x' === $s[0]) {
            return $s;
        }

        $t = explode(':', $s);
        $i = '';
        if (isset($t[1])) {
            $i = ':' . $t[1];
        }

        return str_pad($t[0], 3, '0', STR_PAD_LEFT) . $i;
    }

    /**
     * Format text
     */
    public static function sanitize(string $text): string
    {
        $text    = htmlspecialchars($text, ENT_QUOTES);
        $text    = str_replace("\n\r", "\n", $text);
        $text    = str_replace("\r\n", "\n", $text);
        $text    = str_replace("\n", '<br>', $text);
        $linkify = new Linkify();

        return $linkify->process($text);
    }

    /**
     * @param $text
     *
     * @return mixed
     */
    public static function slugify($text)
    {
        // replace å, ä with a
        $text = str_replace(['å', 'ä'], 'a', $text);
        // replace ö with o
        $text = str_replace('ö', 'o', $text);
        // replace non letter or digits by -
        $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
        // trim
        $text = trim($text, '-');
        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        // lowercase
        $text = strtolower($text);
        // remove unwanted characters
        $text = preg_replace('~[^\-\w]+~', '', $text);
        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

    /**
     * @param $a
     * @param $b
     *
     * @return int
     */
    public static function sort_accent($a, $b): int
    {
        return strcmp(strtolower(self::wd_remove_accents(utf8_encode($a))), strtolower(self::wd_remove_accents(utf8_encode($b))));
    }

    /**
     * @param $s
     *
     * @return mixed
     */
    public static function super_trim($s)
    {
        return preg_replace('/[\n\t\r ]/', '', $s);
    }

    /**
     * @param $values
     *
     * @return mixed
     */
    public static function toArray($values)
    {
        if (!is_array($values)) {
            $values = explode(';', $values);
        }

        if (!is_array($values)) {
            $values = explode(',', $values);
        }

        return $values;
    }

    /**
     * @param        $str
     * @param string $charset
     *
     * @return mixed|string
     */
    public static function wd_remove_accents($str, $charset = 'utf-8')
    {
        $str = htmlentities($str, ENT_NOQUOTES, $charset);

        $str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
        $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
        $str = preg_replace('#&[^;]+;#', '', $str);                  // supprime les autres caractères

        return $str;
    }
}
