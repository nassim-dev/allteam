<?php

namespace core\utils;

/**
 * Undocumented trait
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
trait GeneratorProvider
{
    /**
     * Create a generator
     *
     * @param iterable      $datas
     * @param callable|null $callback function($key, $element){} If false, then break
     *
     * @return \Generator
     */
    private function getGenerator(iterable $datas, ?callable $callback = null): \Generator
    {
        foreach ($datas as $key => $element) {
            yield [$key, $element];

            if (is_callable($callback) && !$callback($key, $element)) {
                break;
            }
        }
    }
}
