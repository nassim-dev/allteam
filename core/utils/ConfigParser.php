<?php

namespace core\utils;

use core\config\Conf;
use core\database\RepositoryFactoryInterface;
use core\entities\EntitieFactoryInterface;
use Nette\Neon\Neon;
use Nette\Utils\FileSystem;

/**
 * Class ConfigParser
 *
 * Init config files in database
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class ConfigParser
{
    public const TEMPLATE_DIR = BASE_DIR . 'templates/img/';
    /**
     * Decoded Neon contents
     *
     * @var array
     */
    public $contentDecoded = null;

    /**
     * Objects to record
     *
     * @var array
     */
    public $objects = [];

    /**
     * SubObjects to record
     *
     * @var array
     */
    public $subObjects = [];

    /**
     * Constructor
     */
    public function __construct(string $file, private string $ressource, private EntitieFactoryInterface $entitieFactory, private RepositoryFactoryInterface $repositoryFactory)
    {
        $this->contentsDecoded = Neon::decode(file_get_contents($file, 'r'));
    }

    /**
     * Record objects to database with no relations
     *
     * @param integer $indcontext
     *
     * @return void
     */
    public function execute(int $idcontext, $oneByOne = false)
    {
        $objects     = [];
        $class       = ucfirst($this->ressource);
        $index       = $this->repositoryFactory->table($class)->getIndexKey();
        $destination = BASE_DIR . Conf::find('PHP.ROOT_PUBLIC_FILES') . 'context_' . $idcontext . '/';


        foreach ($this->objects as $i => $element) {
            if (isset($element['img']) && file_exists(self::TEMPLATE_DIR . $element['img'])) {
                FileSystem::copy(self::TEMPLATE_DIR . $element['img'], $destination . $element['img'], overwrite: true);
            }

            if (isset($element['file']) && file_exists(self::TEMPLATE_DIR . $element['file'])) {
                FileSystem::copy(self::TEMPLATE_DIR . $element['file'], $destination . $element['file'], overwrite: true);
            }

            $object = $this->entitieFactory->create($this->ressource, $element);
            if ($oneByOne) {
                $id                        = $this->repositoryFactory->table($class)->add($object);
                $this->objects[$i][$index] = $id;
            }

            $objects[] = $object;
        }

        if (!$oneByOne) {
            $this->repositoryFactory->table($class)->addMultiple($objects);
        }
    }

    /**
     * Record elements to database with relations
     *
     * @return void
     */
    public function executeWithRelations(int $idcontext)
    {
        if (!empty($this->subObjects)) {
            foreach ($this->subObjects as $i => $element) {
                $element['values']['idcontext'] = $idcontext;
                $class                          = ucfirst($element['ressource']);
                $object                         = $this->entitieFactory->create($element['ressource'], $element['values']);
                $id                             = $this->repositoryFactory->table($class)->add($object);
                if (isset($element['setter'])) {
                    $this->objects[$i][$element['setter']] = $id;
                }

                if (isset($element['setRelation'])) {
                    $this->objects[$i]['relation'] = ['ressource' => $element['ressource'], 'values' => [$id]];
                }
            }

            $class       = ucfirst($this->ressource);
            $index       = $this->repositoryFactory->table($class)->getIndexKey();
            $destination = BASE_DIR . Conf::find('PHP.ROOT_PUBLIC_FILES') . 'context_' . $idcontext . '/';

            foreach ($this->objects as $i => $element) {
                if (isset($element['img']) && file_exists(self::TEMPLATE_DIR . $element['img'])) {
                    FileSystem::copy(self::TEMPLATE_DIR . $element['img'], BASE_DIR . $destination . $element['img'], overwrite: true);
                }

                if (isset($element['file']) && file_exists(self::TEMPLATE_DIR . $element['file'])) {
                    FileSystem::copy(self::TEMPLATE_DIR . $element['file'], BASE_DIR . $destination . $element['file'], overwrite: true);
                }

                $object                    = $this->entitieFactory->create($this->ressource, $element);
                $id                        = $this->repositoryFactory->table($class)->add($object);
                $this->objects[$i][$index] = $id;
                $object->{$index}          = $id;
                if (isset($element['relation'])) {
                    $object->updateRelations('id' . $element['relation']['ressource'], $element['relation']['values']);
                }
            }
        }
    }

    /**
     * Generate objects to record
     *
     * @return void
     */
    public function generateObjects(?ConfigParser $replacedValues = null, $values = [])
    {
        if (!null === $replacedValues) {
            $replace = [];
            foreach ($replacedValues->objects as $element) {
                $replace[$element[$values['key']]] = $element[$values['value']];
            }
        }

        $i = 0;
        foreach ($this->contentsDecoded as $object) {
            $this->subObjects[$i] = null;
            $this->objects[$i]    = $object;
            if (isset($replace)) {
                $this->objects[$i][$values['value']] = $replace[$this->objects[$i][$values['value']]];
            }

            if (isset($this->objects[$i]['preAdd'])) {
                $this->subObjects[$i] = $this->objects[$i]['preAdd'][0];
                unset($this->objects[$i]['preAdd']);
            }

            $i++;
        }
    }
}
