<?php

namespace core\utils;

use core\DI\DiProvider;

/**
 * Undocumented class
 *
 * Execute multiple tasks
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Pipeline
{
    use DiProvider;

    private array $tasks = [];

    private bool $allowed = true;

    private mixed $result = null;

    public function __construct(array $tasks = [])
    {
        foreach ($tasks as $task) {
            $this->pipe($task);
        }
    }

    public function pipe(callable|PipelineInterface|string $task, ...$parameters): self
    {
        $this->tasks[] = ['task' => $task, 'parameters' => $parameters];

        return $this;
    }

    public function execute(): mixed
    {
        $previous = null;
        foreach ($this->tasks as $config) {
            extract($config, EXTR_OVERWRITE);

            if (!$this->allowed) {
                return $this->result;
            }

            if (is_callable($task)) {
                $parameters['_parent'] = $this;
                $this->result          = call_user_func($task, ...$parameters);

                continue;
            }
            if (is_string($task)) {
                $task = $this->getDi()->singleton($task);
            }
            if (is_subclass_of($task, PipelineInterface::class)) {
                /** @var PipelineInterface $task */
                $task->setParent($this)
                    ->setPrevious($previous)
                    ->execute(...$parameters);

                $this->result = $task->getResult();

                $previous = $task;
            }
        }

        return $this->result;
    }

    /**
     * Set the value of allowed
     *
     * @param mixed $allowed
     *
     * @PhpUnitGen\set("allowed")
     *
     * @return self
     */
    public function stop()
    {
        $this->allowed = false;

        return $this;
    }

    /**
     * Set the value of allowed
     *
     * @param mixed $allowed
     *
     * @PhpUnitGen\set("allowed")
     *
     * @return self
     */
    public function play()
    {
        $this->allowed = true;

        return $this;
    }

    /**
     * Get the value of result
     *
     * @PhpUnitGen\get("result")
     *
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }
}
