<?php

namespace core\utils;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
abstract class AbstractPipeline implements PipelineInterface
{
    protected mixed $result = null;

    private ?Pipeline $parent = null;

    private ?PipelineInterface $previous = null;

    /**
     * Get the value of parent
     *
     * @PhpUnitGen\get("parent")
     *
     * @return Pipeline|null
     */
    public function getParent(): ?Pipeline
    {
        return $this->parent;
    }

    /**
     * Set the value of parent
     *
     * @param Pipeline|null $parent
     *
     * @PhpUnitGen\set("parent")
     *
     * @return self
     */
    public function setParent(?Pipeline $parent): static
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get the value of previous
     *
     * @PhpUnitGen\get("previous")
     *
     * @return PipelineInterface|null
     */
    public function getPrevious(): ?PipelineInterface
    {
        return $this->previous;
    }

    /**
     * Set the value of previous
     *
     * @param PipelineInterface|null $previous
     *
     * @PhpUnitGen\set("previous")
     *
     * @return self
     */
    public function setPrevious(?PipelineInterface $previous): static
    {
        $this->previous = $previous;

        return $this;
    }

    /**
     * Get the value of return
     *
     * @PhpUnitGen\get("result")
     *
     * @return mixed
     */
    public function getResult(): mixed
    {
        return $this->result;
    }
}
