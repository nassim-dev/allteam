<?php

namespace core\cache;

use core\entities\EntitieFactoryInterface;
use core\entities\EntitieInterface;
use core\logger\LogServiceInterface;

/**
 * Class EntitiesCache
 *
 * Save object in cache
 *
 * @category  Save object in cache
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class EntitiesCache
{
    /**
     * Array of
     * @var EntitieInterface[]|CustomNetteSelection[]
     */
    private array $entitiesCache = [];


    public function __construct(private EntitieFactoryInterface $entitieFactory)
    {
    }

    /**
     * Find specific object
     */
    public function find(int|null|string $indexKey, string $table): ?EntitieInterface
    {
        if (null === $indexKey) {
            return null;
        }

        if (!isset($this->entitiesCache[$table])) {
            $this->entitiesCache[$table] = [];

            return null;
        }

        if (!isset($this->entitiesCache[$table][$indexKey])) {
            return null;
        }

        return $this->entitieFactory->create($table, $this->entitiesCache[$table][$indexKey]);
    }

    /**
     * Find collection
     *
     * @return EntitieInterface[]|null
     */
    public function findAll(string $table): ?array
    {
        if (!isset($this->entitiesCache[$table])) {
            $this->entitiesCache[$table] = [];

            return null;
        }

        if (empty($this->entitiesCache[$table])) {
            return null;
        }

        $return = [];
        foreach ($this->entitiesCache[$table] as $properties) {
            $return = $this->entitieFactory->create($table, $properties);
        }

        return $return;
    }

    /**
     * Save object
     */
    public function flush(EntitieInterface $entitie, string $table): ?EntitieInterface
    {
        if (is_bool($entitie)) {
            return null;
        }

        if (!isset($this->entitiesCache[$table])) {
            $this->entitiesCache[$table] = [];
        }

        if ($entitie->getPrimary() !== '') {
            $this->entitiesCache[$table][$entitie->getPrimary()] = $entitie->getVarsKeys();
        }

        return $entitie;
    }

    public function remove(EntitieInterface $entitie, string $table): bool
    {
        if (!isset($this->entitiesCache[$table])) {
            $this->entitiesCache[$table] = [];

            return false;
        }

        $indexKey = (is_int($entitie)) ? $entitie : $entitie->getPrimary();
        if (isset($this->entitiesCache[$table][$indexKey])) {
            unset($this->entitiesCache[$table][$indexKey]);

            return true;
        }

        return false;
    }

    public function removeAll()
    {
        $this->entitiesCache = [];
    }

    /**
     * Show
     *
     * @return void
     */
    public function showCache(LogServiceInterface $logService)
    {
        $logService->dump($this->entitiesCache);
    }
}
