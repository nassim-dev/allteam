<?php

namespace core\cache;

/**
 * Undocumented trait
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
trait ExportableTrait
{
    public static function __set_state($properties)
    {
        $obj = new static();
        foreach ($properties as $propertie => $value) {
            $obj->$propertie = $value;
        }

        return $obj;
    }
}
