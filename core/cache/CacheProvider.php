<?php

namespace core\cache;

use core\DI\DiProvider;

/**
 * Trait CacheProvider
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait CacheProvider
{
    use DiProvider;

    /**
     * Apc Cache implémentation
     *
     * @var ApcCache
     */
    private $apc;

    /**
     * Cache implémentation
     *
     * @var CacheServiceInterface
     */
    private $cache;

    /**
     * Fake Cache implémentation
     *
     * @var FakeCache
     */
    private $fake;

    /**
     * Memcached Cache implémentation
     *
     * @var MemcachedCache
     */
    private $memcached;

    /**
     * Redis Cache implémentation
     *
     * @var RedisCache
     */
    private $redis;

    /**
     * Return apc cache implémetation
     */
    public function getApc(): ApcCache
    {
        return $this->apc ??= $this->getDi()->singleton(ApcCache::class);
    }

    /**
     * Return cache implémetation
     */
    public function getCache(): CacheServiceInterface
    {
        return $this->cache ??= $this->getDi()->singleton(CacheServiceInterface::class);
    }

    /**
     * Return fake cache implémetation
     */
    public function getFakeCache(): FakeCache
    {
        return $this->fake ??= $this->getDi()->singleton(FakeCache::class);
    }

    /**
     * Return file cache implémetation
     */
    public function getFileCache(): FileCache
    {
        return $this->file ??= $this->getDi()->singleton(FileCache::class);
    }

    /**
     * Return memcached cache implémetation
     */
    public function getMemcached(): MemcachedCache
    {
        return $this->memcached ??= $this->getDi()->singleton(MemcachedCache::class);
    }

    /**
     * Return redis cache implémetation
     */
    public function getRedis(): RedisCache
    {
        return $this->redis ??= $this->getDi()->singleton(RedisCache::class);
    }
}
