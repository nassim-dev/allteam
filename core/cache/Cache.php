<?php namespace core\cache;

/**
 * Class Cache
 *
 *  @method static string clearAll()
 *  @method static string close()
 *  @method static string get(array|string|int $key)
 *  @method static bool hasKey(array|string|int $key)
 *  @method static string remove(array|string|int $key)
 *  @method static string set(array|string|int $key, string $values)
 *  @method static string replace(string $key, string $value)
 *  @method static string queued(string $key, string $values)
 *  @method static bool setMulti(array $properties)
 *  @method static string getCachePrefix()
 *  @method static string invalidate(string $key)
 *  @method static string setCachePrefix(string $cachePrefix)
 *  @method static string getMulti(array $keys)
 *  @method static array defaultConfig()
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class Cache extends \core\facade\HelperFacade
{
    protected static ?string $serviceInterface = 'core\cache\CacheServiceInterface';
}
