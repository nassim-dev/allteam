<?php

namespace core\cache;

/**
 * Save sql results to prevent mutliple same requests
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class SqlSaver implements RequestCacheServiceInterface
{
    private array $_count = [];

    private array $_objects = [];

    /**
     * Get Sql result Saved
     *
     * @return mixed
     */
    public function get(string $key, callable $callback, string $type = 'list', bool $useCache = true)
    {
        if (!isset($this->_objects[$key][$type]) || !$useCache) {
            $this->_objects[$key][$type]      = $callback();
            $this->_count[$key . '.' . $type] = 0;
        }

        ++$this->_count[$key . '.' . $type];

        return $this->_objects[$key][$type];
    }

    /**
     * Get the value of _count
     */
    public function getCount(): array
    {
        return $this->_count;
    }
}
