<?php

namespace core\cache;

trait CacheUpdaterTrait
{
    private $__objectsCached = [];

    private $__prefix = null;

    public function getPrefix(): string
    {
        if ($this->__prefix === null) {
            $this->__prefix = str_replace('\\', '-', static::class);
        }

        return $this->__prefix ;
    }

    /**
     * Save properties to cache
     *
     * @return void
     */
    protected function _cacheMultiplesProps(array $properties)
    {
        //For Proxy classes
        if (property_exists($this, '__isInitialized__') && $this->__isInitialized__ !== true) {
            return;
        }
        if (null === $this->cache) {
            return;
        }

        foreach ($properties as $property) {
            if (property_exists($this, $property) &&
                    (!array_key_exists($property, $this->__objectsCached) || $this->_isModified($property))
            ) {
                $this->cache->queued($this->getPrefix() . '.' . $property, $this->$property);
            }
        }
    }

    /**
     * Check if property has been modified
     */
    private function _isModified(string $property): bool
    {
        if (is_array($this->$property)) {
            return (count($this->$property) !== count($this->__objectsCached[$property]));
        }

        return $this->__objectsCached[$property] !== $this->$property;
    }



    /**
     * Find properties in cache
     *
     * @return bool
     */
    protected function _fetchPropsFromCache(array $properties): bool
    {
        if (null === $this->cache) {
            return false;
        }

        $keys = array_map(
            fn ($element): string => $this->getPrefix() . '.' . $element,
            $properties
        );

        $cachedComponents = $this->cache->getMulti($keys);

        if (is_bool($cachedComponents) || null === $cachedComponents) {
            return false;
        }

        foreach ($cachedComponents as $key => $value) {
            $property = explode('.', $key);
            $property = $property[1];

            if ($value !== NoValueResponse::get() && $value !== null && $value !== false) {
                $this->$property                  = $value;
                $this->__objectsCached[$property] = $value;
            }
        }

        return true;
    }

    /**
     * Set the value of __prefix
     *
     * @param mixed $__prefix
     *
     * @PhpUnitGen\set("__prefix")
     *
     * @return static
     */
    public function setPrefix(?string $__prefix): static
    {
        $this->__prefix = $__prefix;

        return $this;
    }
}
