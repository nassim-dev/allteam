<?php

namespace core\cache;

use core\DI\DI;
use core\DI\DiProvider;
use core\tasks\AsyncJob;
use core\tasks\SaveCacheTask;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
abstract class AbstractCache
{
    use DiProvider;

    /**
     * Chache prefix key
     */
    private ?string $cachePrefix = 'DEFAULT';

    /**
     * Generate key
     *
     * @return string
     */
    public function generateKey(string | array | int $key)
    {
        if (is_string($key) || is_int($key)) {
            return $this->getCachePrefix() . '.' . $key;
        }

        if (is_array($key)) {
            return $this->getCachePrefix() . '.' . implode('_', $key);
        }

        throw new CacheException('Invalid type for $key : ' . gettype($key));
    }


    protected function defferedCache(array $queue)
    {
        /** @var AsyncJob $task */
        $task = DI::singleton(
            AsyncJob::class,
            [
                'id' => SaveCacheTask::class,
            ]
        );
        $task->setCallable(SaveCacheTask::class);
        $task->setContext(
            [
                'context' => CONTEXT,
                'queue'   => $queue
            ]
        );
        $task->save();
    }

    /**
     * Get the value of cachePrefix
     *
     * @PhpUnitGen\get("cachePrefix")
     */
    public function getCachePrefix(): ?string
    {
        return $this->cachePrefix;
    }

    /**
     * Set chache prefix key
     *
     * @param string|null $cachePrefix Chache prefix key
     *
     * @PhpUnitGen\set("cachePrefix")
     */
    public function setCachePrefix(?string $cachePrefix): self
    {
        $this->cachePrefix = $cachePrefix;

        return $this;
    }
}
