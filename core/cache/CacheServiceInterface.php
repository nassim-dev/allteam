<?php

namespace core\cache;

/**
 * Interface CacheServiceInterface
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
interface CacheServiceInterface
{
    /**
     * Clear all cached values
     *
     * @return void
     */
    public function clearAll();

    /**
     * Close connection
     */
    public function close();

    /**
     * Get a value
     *
     * @return mixed
     */
    public function get(array|int|string $key);

    /**
     * Test if a key exist
     */
    public function hasKey(array|int|string $key): bool;

    /**
     * Remove a key
     */
    public function remove(array|int|string $key);

    /**
     * Set a value
     *
     * @param mixed $value
     */
    public function set(array|int|string $key, $values);

    /**
     * Replace a key
     *
     * @param  mixed $value
     * @return mixed
     */
    public function replace(string $key, $value);

    /**
     * Push element to queue in order to save on destruct
     *
     * @param  mixed $values
     * @return void
     */
    public function queued(string $key, $values);

    /**
     * Set multiples values
     */
    public function setMulti(array $properties): bool;

    /**
     * Get the value of cachePrefix
     *
     * @PhpUnitGen\get("cachePrefix")
     */
    public function getCachePrefix(): ?string;

    /**
     * Invalidate specific key
     *
     * @return void
     */
    public function invalidate(string $key);

    /**
     * Set chache prefix key
     *
     * @param string|null $cachePrefix Chache prefix key
     *
     * @PhpUnitGen\set("cachePrefix")
     *
     * @return self
     */
    public function setCachePrefix(?string $cachePrefix);

    /**
     * Get multiples keys
     *
     * @return mixed
     */
    public function getMulti(array $keys);

    public static function defaultConfig(): array;
}
