<?php

namespace core\cache;

/**
 * Class CacheBuffer
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class CacheBuffer
{
    /**
     * @param $key
     */
    public function __construct(private CacheServiceInterface $cache)
    {
        ob_start();
    }

    /**
     * Save cache
     */
    public function close(string $key)
    {
        if (null !== $this->cache) {
            $this->cache->set($key, $this->compress(ob_get_flush()));
            $this->cache = null;
        }
    }

    /**
     * @param $code
     *
     * @return mixed
     */
    private function compress($code)
    {
        $search = [
            '/\>[^\S ]+/s', // remove whitespaces after tags
            '/[^\S ]+\</s', // remove whitespaces before tags
            '/(\s)+/s'     // remove multiple whitespace sequences
        ];

        $replace = ['>', '<', '\\1'];
        $code    = preg_replace($search, $replace, $code);

        return $code;
    }
}
