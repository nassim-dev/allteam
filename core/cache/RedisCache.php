<?php

namespace core\cache;

use core\services\serializer\DefaultSerializer;
use core\services\serializer\Serializer;
use core\services\serializer\SerializerServiceInterface;
use Predis\Client;

/**
 * Class RedisCache
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class RedisCache extends AbstractCache implements CacheServiceInterface
{
    private \Predis\Client $driver;

    private array $queue = [];

    public static function defaultConfig(): array
    {
        return [new Serializer(new DefaultSerializer()), 'unix:/tmp/redis/redis.sock'];
    }

    /**
     * Constructor
     */
    public function __construct(
        private SerializerServiceInterface $serializer,
        string $endpoint = 'unix:/tmp/redis/redis.sock',
    ) {
        $this->driver = new Client($endpoint);
    }

    public function __destruct()
    {
        if (!empty($this->queue)) {
            $this->getDriver()->mset($this->queue);
        }

        $this->close();
    }

    /**
     * Get multiples keys
     *
     * @return mixed
     */
    public function getMulti(array $keys): array
    {
        $realKeys = [];
        foreach ($keys as $k => $key) {
            $realKeys[] = $this->generateKey($key);
        }

        $datas = $this->getDriver()->mget($realKeys);

        foreach ($datas as $k => $value) {
            $datas[$k] = ($value === null) ? $value : $this->serializer->unserialize($value);
        }

        return array_combine($keys, $datas);
    }

    /**
     * Push element to queue in order to save on destruct
     *
     * @param  mixed $value
     * @return void
     */
    public function queued(string $key, $value)
    {
        $this->queue[$this->generateKey($key)] = $this->serializer->serialize($value);
    }

    public function setMulti(array $properties): bool
    {
        $datas = [];
        foreach ($properties as $key => $value) {
            $datas[$this->generateKey($key)] = $this->serializer->serialize($value);
        }

        if (!empty($datas)) {
            $this->getDriver()->mset($datas);
        }

        return true;
    }


    public function invalidate(string $key)
    {
        $this->driver->del($this->generateKey($key));
    }

    /**
     * Clear all cached values
     *
     * @return void
     */
    public function clearAll()
    {
        $this->driver->flushall();
    }

    /**
     * Close connection
     */
    public function close()
    {
        $this->driver->quit();
    }

    /**
     * Get a value
     *
     * @param string|array|int $key
     *
     * @return mixed|null
     */
    public function get($key)
    {
        $value = $this->driver->get($this->generateKey($key));
        if (null === $value) {
            return null;
        }

        return $this->serializer->unserialize($value);
    }

    /**
     * Get the value of driver
     *
     * @PhpUnitGen\get("driver")
     */
    public function getDriver(): Client
    {
        return $this->driver;
    }

    /**
     * Test if a key exist
     *
     * @param string|array|int $key
     */
    public function hasKey($key): bool
    {
        return (null !== $this->driver->get($this->generateKey($key)));
    }

    /**
     * Remove a key
     *
     * @param string|array|int $key
     */
    public function remove($key)
    {
        $this->driver->del([$this->generateKey($key)]);
    }

    /**
     * @param $key
     * @param $value
     */
    public function replace($key, $value)
    {
        $this->set($key, $value);
    }

    /**
     * Set a value
     *
     * @param string|array|int $key
     * @param mixed            $value
     */
    public function set($key, $value)
    {
        $this->driver->set($this->generateKey($key), $this->serializer->serialize($value));
    }
}
