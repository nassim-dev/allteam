<?php

namespace core\cache;

/**
 * Class FakeCache
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class FakeCache extends AbstractCache implements CacheServiceInterface
{
    /**
     * Storage
     */
    private array $store = [];

    private array $queue = [];

    public function __destruct()
    {
        if (!empty($this->queue)) {
            foreach ($this->queue as $key => $value) {
                $this->set($key, $value);
            }
        }

        $this->close();
    }


    public static function defaultConfig(): array
    {
        return [];
    }

    /**
     * Clear all cached values
     *
     * @return void
     */
    public function clearAll()
    {
        $this->store = [];
    }

    /**
     * Close connection
     */
    public function close()
    {
    }

    /**
     * Get multiples keys
     *
     * @return mixed
     */
    public function getMulti(array $keys)
    {
        $return = [];
        foreach ($keys as $key) {
            $return[$key] = $this->get($key);
        }

        return $return;
    }

    /**
     * Set multiples keys
     */
    public function setMulti(array $properties): bool
    {
        foreach ($properties as $key => $value) {
            $this->set($key, $value);
        }

        return true;
    }

    public function invalidate(string $key)
    {
        if (isset($this->store[$this->generateKey($key)])) {
            unset($this->store[$this->generateKey($key)]);
        }
    }

    /**
     * Get a value
     *
     * @param string|array|int $key
     *
     * @return mixed|null
     */
    public function get($key)
    {
        $key = $this->generateKey($key);

        return $this->store[$key] ?? null;
    }

    /**
     * Test if a key exist
     *
     * @param string|array|int $key
     */
    public function hasKey($key): bool
    {
        return (isset($this->store[$this->generateKey($key)]));
    }

    /**
     * Remove a key
     *
     * @param string|array|int $key
     */
    public function remove($key)
    {
        $key = $this->generateKey($key);
        if ($this->hasKey($key)) {
            unset($this->store[$key]);
        }
    }

    /**
     * Set a value
     *
     * @param string|array|int $key
     * @param mixed            $value
     */
    public function set($key, $value)
    {
        $this->store[$this->generateKey($key)] = $value;
    }

    /**
     * Replace a key
     *
     * @param  mixed $value
     * @return mixed
     */
    public function replace(string $key, $value)
    {
        $this->set($key, $value);
    }

    /**
     * Push element to queue in order to save on destruct
     *
     * @param  mixed $values
     * @return void
     */
    public function queued(string $key, $values)
    {
        $this->queue[$key] = $value;
    }
}
