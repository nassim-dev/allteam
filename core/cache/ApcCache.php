<?php

namespace core\cache;

use core\services\serializer\SerializerServiceInterface;

/**
 * Class ApcCache
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class ApcCache extends AbstractCache implements CacheServiceInterface
{
    private $queue;

    public function __construct(
        private SerializerServiceInterface $serializer,
    ) {
        if (!\apcu_enabled()) {
            throw new CacheException('Apc extension not loaded or not enabled', 1);
        }
    }


    public static function defaultConfig(): array
    {
        return [];
    }


    /**
     * Clear all cached values
     *
     * @return void
     */
    public function clearAll()
    {
        return \apcu_clear_cache() && \apcu_clear_cache();
    }

    /**
     * Close connection
     */
    public function close()
    {
    }

    /**
     * Get multiples keys
     *
     * @return mixed
     */
    public function getMulti(array $keys)
    {
        $data = \apcu_fetch($keys);
        if ($data === null) {
            return NoValueResponse::get();
        }

        return array_map(function ($element) {
            return  $this->serializer->unserialize($element);
        }, $data);
    }


    public function invalidate(string $key)
    {
        \apcu_delete($this->generateKey($key));
    }

    /**
     * Get a value
     *
     * @param string|array|int $key
     *
     * @return mixed|null
     */
    public function get($key)
    {
        $data = \apcu_fetch($this->generateKey($key));

        return  $this->serializer->unserialize($data) ?? NoValueResponse::get();
    }

    /**
     * Replace a key
     *
     * @param  mixed $value
     * @return void
     */
    public function replace(string $key, $value): void
    {
        $this->set($key, $value);
    }

    /**
     * Test if a key exist
     *
     * @param string|array|int $key
     */
    public function hasKey($key): bool
    {
        return (\apcu_exists($this->generateKey($key)));
    }

    /**
     * Set multiples keys
     */
    public function setMulti(array $properties): bool
    {
        $datas = [];
        foreach ($properties as $key => $value) {
            $datas[$this->generateKey($key)] = $this->serializer->serialize($value);
        }

        return \apcu_store($datas);
    }

    /**
     * Remove a key
     *
     * @param string|array|int $key
     *
     * @return void
     */
    public function remove($key)
    {
        \apcu_delete($this->generateKey($key));
    }

    /**
     * Set a value
     *
     * @param string|array|int $key
     * @param mixed            $value
     * @param array            $parameters
     *
     * @return void
     */
    public function set($key, $value, $parameters = [])
    {
        \apcu_store($this->generateKey($key), $this->serializer->serialize($value), $parameters['expire'] ?? 0);
    }

    public function __destruct()
    {
        $this->setMulti($this->queue);
    }

    /**
     * Push element to queue in order to save on destruct
     *
     * @param  mixed $values
     * @return void
     */
    public function queued(string $key, $values)
    {
        $this->queue[$key] = $values;
    }
}
