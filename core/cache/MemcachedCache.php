<?php

namespace core\cache;

/**
 * Class MemcachedCache
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class MemcachedCache extends AbstractCache implements CacheServiceInterface
{
    private array $queue = [];

    private \Memcached $driver;

    /**
     * Constructor
     */
    public function __construct(string $dsn = '/tmp/memcached/memcached.sock', ?int $port = null)
    {
        $this->driver = new \Memcached('persistence_id');
        $this->driver->addServer($dsn, $port);
    }

    public function __destruct()
    {
        if (!empty($this->queue)) {
            $this->getDriver()->setMulti($this->queue);
        }

        $this->close();
    }


    public static function defaultConfig(): array
    {
        return [getenv('MEMCACHED_SERVER'), (is_int(getenv('MEMCACHED_PORT'))) ? getenv('MEMCACHED_PORT') : 0];
    }


    public function invalidate(string $key)
    {
        $this->driver->delete($this->generateKey($key));
    }

    /**
     * Push element to queue in order to save on destruct
     *
     * @param  mixed $value
     * @return void
     */
    public function queued(string $key, $value)
    {
        $this->queue[$this->generateKey($key)] = $value;
    }

    /**
     * Get multiples keys
     *
     * @return mixed
     */
    public function getMulti(array $keys)
    {
        foreach ($keys as $k => $key) {
            $keys[$k] = $this->generateKey($key);
        }

        return $this->getDriver()->getMulti($keys);
    }

    /**
     * Clear all cached values
     *
     * @return void
     */
    public function clearAll()
    {
        $this->driver->flush();
    }

    /**
     * Close connection
     */
    public function close()
    {
        $this->driver->quit();
    }

    /**
     * Get a value
     *
     * @param string|array|int $key
     *
     * @return mixed|null
     */
    public function get($key)
    {
        $result = $this->driver->get($this->generateKey($key));

        return  ($this->driver->getResultCode() === \Memcached::RES_NOTFOUND) ? null : $result;
    }

    /**
     * Get the value of driver
     *
     * @PhpUnitGen\get("driver")
     */
    public function getDriver(): \Memcached
    {
        return $this->driver;
    }

    /**
     * Test if a key exist
     *
     * @param string|array|int $key
     */
    public function hasKey($key): bool
    {
        return (null !== $this->driver->get($this->generateKey($key)));
    }

    /**
     * Remove a key
     *
     * @param string|array|int $key
     */
    public function remove($key)
    {
        if ($this->hasKey($key)) {
            $this->driver->delete($this->generateKey($key));
        }
    }

    /**
     * Replace a key
     *
     * @param  mixed $value
     * @return mixed
     */
    public function replace(string $key, $value): bool
    {
        return $this->driver->replace($this->generateKey($key), $value);
    }

    /**
     * Set a value
     *
     * @param string|array|int $key
     * @param mixed            $value
     */
    public function set($key, $value)
    {
        $this->driver->set($this->generateKey($key), $value);
    }

    /**
     * Set multiples values
     */
    public function setMulti(array $properties): bool
    {
        $datas = [];
        foreach ($properties as $key => $value) {
            $datas[$this->generateKey($key)] = $value;
        }

        return $this->getDriver()->setMulti($datas);
    }
}
