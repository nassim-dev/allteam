<?php

namespace core\services\stripe;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Utils
{
    /**
     * Calc price excluded fees (Stripe)
     *
     * @param int     $price
     * @param boolean $cardEU
     * @param float   $tva
     *
     * @return void
     */
    public static function getPriceExcludedFee($price, $cardEU = true, $tva = 1.2)
    {
        $result = [];
        if ($price <= 0) {
            return [
                'fee'        => _('Free'),
                'chargeTTC'  => _('Free'),
                'chargeHT'   => _('Free'),
                'earningTTC' => _('Free'),
                'earningHT'  => _('Free')
            ];
        }

        $staticFee  = 0.25;
        $percentFee = ($cardEU) ? 0.014 : 0.029;

        $charge = $price * $tva;
        $price  = ($price + round($price * $percentFee + $staticFee, 2)) * $tva;
        $fee    = round($price * $percentFee + $staticFee, 2);
        $charge += $fee;

        $result['fee']        = $fee;
        $result['earningTTC'] = ($charge - $result['fee']);
        $result['earningHT']  = $result['earningTTC'] / $tva;
        $result['chargeTTC']  = round($charge, 2);
        $result['chargeHT']   = round($charge / $tva, 2);

        return $result;
    }

    /**
     * Calc price included fees (Stripe)
     *
     * @param [type]  $price
     * @param boolean $cardEU
     * @param float   $tva
     *
     * @return void
     */
    public static function getPriceIncludedFee($price, $cardEU = true, $tva = 1.2)
    {
        if ($price <= 0) {
            return [
                'fee'        => _('Free'),
                'chargeTTC'  => _('Free'),
                'chargeHT'   => _('Free'),
                'earningTTC' => _('Free'),
                'earningHT'  => _('Free')
            ];
        }

        $staticFee  = 0.25;
        $percentFee = ($cardEU) ? 0.014 : 0.029;

        $charge     = $price * $tva;
        $fee        = round($charge * $percentFee + $staticFee, 2);
        $earningTTC = $charge - $fee;

        $result              = ['fee' => $fee, 'earningTTC' => $earningTTC];
        $result['earningHT'] = $result['earningTTC'] / $tva;
        $result['chargeTTC'] = round($charge, 2);
        $result['chargeHT']  = round($charge / $tva, 2);

        return $result;
    }
}
