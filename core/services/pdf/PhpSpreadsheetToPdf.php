<?php

namespace core\services\pdf;

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

/**
 * Class PhpSpreadsheetToPdf
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class PhpSpreadsheetToPdf
{
    public function __construct(public Spreadsheet $spreadsheet)
    {
    }

    /**
     * @param $filename
     */
    public function render($filename)
    {
        $writer = IOFactory::createWriter($this->spreadsheet, 'Mpdf');
        $writer->setSheetIndex(0);
        $writer->save($filename);

        /*   $writer = IOFactory::createWriter($this->spreadsheet, 'Pdf');
    $writer->save($filename); */
    }
}
