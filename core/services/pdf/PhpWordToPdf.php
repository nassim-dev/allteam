<?php

namespace core\services\pdf;

use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Writer\PDF\MPDF;

/**
 * Class PhpWordToPdf
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class PhpWordToPdf
{
    /**
     * @var mixed
     */
    public $mpdf = null;

    public function __construct(PhpWord $phpWord)
    {
        $this->mpdf = new MPDF($phpWord);
    }

    /**
     * @param $filename
     */
    public function render($filename)
    {
        $this->mpdf->save($filename);
    }
}
