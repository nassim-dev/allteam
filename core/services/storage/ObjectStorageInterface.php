<?php

namespace core\services\storage;

/**
 * Interface ObjectStorageInterface
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
interface ObjectStorageInterface
{
    public const _PRIVATE = 'private';

    public const _PUBLIC = 'public';

    public const _RESTRICTED = 'restricted';

    public const TMP_DIR = '/tmp';

    /**
     * Copy a file
     */
    public function copy(string $source, string $destination): ?FileItemInterface;

    /**
     * Delete an object
     *
     * @return void
     */
    public function delete(string $source);

    /**
     * Get a file
     *
     * @return FileItemInterface|null $identifier
     */
    public function get(string $source): ?FileItemInterface;

    /**
     * Check if file exist
     */
    public function has(string $source): bool;

    /**
     * Move a file
     */
    public function move(FileItemInterface|string $source, string $destination): ?FileItemInterface;

    /**
     * Update file visibility
     *
     * @return FileItemInterface|null $identifier
     */
    public function setVisibility(string $source, string $visibility): ?FileItemInterface;

    /**
     * Update an existing file
     *
     * @return FileItemInterface|null $identifier
     */
    public function update(string $source, string $filename, string $visibility = self::_PUBLIC): ?FileItemInterface;

    /**
     * Upload new file
     *
     * @param $object
     *
     * @return FileItemInterface|null $identifier
     */
    public function upload(string $source, string $destination, string $visibility = self::_PUBLIC): ?FileItemInterface;
}
