<?php

namespace core\services\storage;

/**
 * Class ThumbnailDecorator
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class ThumbnailDecorator
{
    /**
     * @todo implements method to return imagfe preview or extenstion's icon
     */
    public function __construct(FileItemInterface $file)
    {
    }
}
