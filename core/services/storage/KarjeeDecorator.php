<?php

namespace core\services\storage;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class KarjeeDecorator
{
    /**
     * @return array{initialPreviewThumbTags: array<int, array{{KEY}: string, {zoomData}: string}>, initialPreview: string[], initialPreviewConfig: array<int, array{caption: string, size: int, previewAsData: true, url: string, key: string, zoomData: string, extra: array{key: string}, deleteExtraData: array{key: string}}>}
     */
    public static function decorate(FileItemInterface $fileItem): array
    {
        $encyptedKey = $fileItem->getEncyptedKey();
        $config      = [
            'caption'         => $fileItem->getDecoratedTitle() . '.' . $fileItem->getMimeType(),
            'size'            => $fileItem->getSize(),
            'previewAsData'   => true,
            'url'             => $fileItem->generateDeleteUrl(),
            'key'             => $encyptedKey,
            'zoomData'        => $fileItem->getLink(),
            'extra'           => ['key' => $encyptedKey],
            'deleteExtraData' => ['key' => $encyptedKey]
        ];

        return [
            'initialPreviewThumbTags' => [
                ['{KEY}' => $encyptedKey, '{zoomData}' => $encyptedKey]
            ],
            'initialPreview'       => [$fileItem->getLink()],
            'initialPreviewConfig' => [
                $config
            ]];
    }
}
