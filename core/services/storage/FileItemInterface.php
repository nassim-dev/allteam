<?php

namespace core\services\storage;

use DateTimeInterface;

/**
 * Interface FileItemInterface
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
interface FileItemInterface
{
    /**
     * File contents
     *
     * @return mixed
     */
    public function getContents();

    /**
     * Return encrypted key
     */
    public function getEncyptedKey(): string;

    /**
     * Last Modified date
     */
    public function getLastModified(): DateTimeInterface;

    /**
     * File link if public
     */
    public function getLink(): string;

    /**
     * Extenstion && mime type
     */
    public function getMimeType(): string;

    /**
     * Return real path
     */
    public function getRealPath(): string;

    /**
     * File size in octed
     */
    public function getSize(): int;

    /**
     * File title
     */
    public function getTitle(): string;

    /**
     * File visibility
     */
    public function getVisibility(): string;

    /**
     * Set the value of visibility
     */
    public function setVisibility(string $visibility);


    /**
     * Get strict path
     */
    public function getPath(): string;

    /**
     * Return delete url
     */
    public function generateDeleteUrl(): string;

    /**
     * Return public url
     */
    public function generatePublicUrl(): string;
}
