<?php

namespace core\services\storage;

use core\config\Conf;
use core\DI\InjectionAttribute;
use core\secure\authentification\AuthServiceInterface;
use core\secure\sanitizers\SanitizerInterface;
use core\secure\sanitizers\SchemeSanitizer;

/**
 * Class FilesystemStorage
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class FilesystemStorage implements ObjectStorageInterface
{
    /**
     * Base directory
     *
     * @var string|null
     */
    protected $baseDir = null;

    #[InjectionAttribute(SanitizerInterface::class, implementation:SchemeSanitizer::class)]
    public function __construct(
        private SanitizerInterface $sanitizer,
        private ?AuthServiceInterface $auth
    ) {
        $this->baseDir = Conf::find('STORAGE.FILESYSTEM.PATH') . '/context_' . $auth?->getContext()?->getIdcontext() ?? 0;
    }

    /**
     * Copy a file
     *
     * @param string $source
     */
    public function copy(FileItemInterface|string $source, string $destination): ?FileItemInterface
    {
        $source = (is_string($source)) ? $source : $source->getRealPath();
        $source = $this->sanitize($source);
        if (!file_exists($source)) {
            throw new FileException('Unable to find file : ' . $source);
        }

        try {
            $this->mkdir($destination);
            copy($source, $this->baseDir . $destination);
        } catch (FileException $e) {
            throw $e;
        }

        return new FileItem($destination, $this->baseDir);
    }

    /**
     * Delete an object
     *
     * @param string $path
     *
     * @return void
     */
    public function delete(FileItemInterface|string $path)
    {
        $path = (is_string($path)) ? $path : $path->getRealPath();
        $path = $this->sanitize($path);
        if (!file_exists($this->baseDir . $path)) {
            throw new FileException('Unable to find file : ' . $this->baseDir . $path);
        }

        if ((is_file($this->baseDir . $path))) {
            unlink($this->baseDir . $path);
        }
    }

    /**
     * Get a file
     *
     * @return FileItemInterface|null $identifier
     */
    public function get(string $path): ?FileItemInterface
    {
        $path = $this->sanitize($path);
        if (!file_exists($this->baseDir . $path)) {
            return null;
        }

        return new FileItem($path, $this->baseDir);
    }

    /**
     * Check if file exist
     */
    public function has(string $path): bool
    {
        $path = $this->sanitize($path);

        return (file_exists($this->baseDir . $path));
    }

    /**
     * Move a file
     *
     * @param string $source
     */
    public function move(FileItemInterface|string $source, string $destination): ?FileItemInterface
    {
        $source = (is_string($source)) ? $source : $source->getRealPath();

        if (!file_exists($source)) {
            throw new FileException('Unable to find file : ' . $source);
        }

        $this->mkdir($destination);

        rename($source, $this->baseDir . $destination);

        return new FileItem($destination, $this->baseDir);
    }

    /**
     * Update file visibility
     *
     * @param string $path
     *
     * @return FileItemInterface|null $identifier
     */
    public function setVisibility(FileItemInterface|string $file, string $visibility): ?FileItemInterface
    {
        if (is_string($file)) {
            $file = $this->get($file);
        }

        return $file?->setVisibility($visibility);
    }

    /**
     * Update an existing file
     *
     * @param string $path
     *
     * @return FileItemInterface|null $identifier
     */
    public function update(FileItemInterface|string $path, string $filename, string $visibility = self::_PUBLIC): ?FileItemInterface
    {
        $path = (is_string($path)) ? $path : $path->getRealPath();
        $path = $this->sanitize($path);
        if (!file_exists($this->baseDir . $path)) {
            throw new FileException('Unable to find file : ' . $this->baseDir . $path);
        }

        if (!file_exists($filename)) {
            throw new FileException('Unable to find file : ' . $filename);
        }

        file_put_contents($this->baseDir . $path, file_get_contents($filename));

        $file = new FileItem($path, $this->baseDir);

        return $file->setVisibility($visibility);
    }

    /**
     * Upload new file
     *
     * @param $object
     *
     * @return FileItemInterface|null $identifier
     */
    public function upload(string $path, string $destination, string $visibility = self::_PUBLIC): ?FileItemInterface
    {
        $file = $this->move($this->sanitize($path), $destination);

        return $file?->setVisibility($visibility);
    }

    private function sanitize(string &$path)
    {
        $return = $this->sanitizer->sanitize(['path' => $path], ['path' => 'file']);

        return $path = $return['path'];
    }

    private function mkdir(string $path)
    {
        $components = explode('/', $path);
        $components = array_slice($components, 0, -1);
        $directory  = $this->baseDir . implode('/', $components);


        if (!is_dir($directory)) {
            mkdir($directory, 0775, true);
        }
    }
}
