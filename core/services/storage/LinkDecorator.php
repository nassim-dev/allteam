<?php

namespace core\services\storage;

/**
 * Class LinkDecorator
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class LinkDecorator
{
    public function __construct(FileItemInterface $file)
    {
        return '<a class="view-attachement" data-toogle="tooltip" title="' . $file->getTitle() . '" href="' . $file->getLink() . '" alt="' . $file->getTitle() . '" ><span class="badge badge-dark"><i class="fas fa-paperclip"></i> - ' . $file->getTitle() . '</span></a><br>';
    }
}
