<?php

namespace core\services\storage;

use core\secure\authentification\AuthServiceInterface;

/**
 * Class FakeStorage
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class FakeStorage extends FilesystemStorage implements ObjectStorageInterface
{
    public function __construct(AuthServiceInterface $auth)
    {
        $this->baseDir = '/tmp/context_' . $auth->getContext()->getIdcontext() . '/';
    }
}
