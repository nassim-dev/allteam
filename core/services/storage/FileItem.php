<?php

namespace core\services\storage;

use core\utils\Utils;
use DateTimeInterface;

/**
 * Class FileItem
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class FileItem implements FileItemInterface
{
    public const VISIBILITY_PRIVATE = 'PRIVATE';

    public const VISIBILITY_PUBLIC = 'PUBLIC';

    private \DateTimeInterface $lastModified;

    private string $link;

    private string $mimeType;

    private string $realPath;

    private int $size;

    private string $title;

    public function __construct(private string $path, ?string $baseDir = null, private string $visibility = self::VISIBILITY_PRIVATE)
    {
        $this->realPath     = $baseDir . $path;
        $this->size         = filesize($this->realPath);
        $this->mimeType     = pathinfo($this->realPath, PATHINFO_EXTENSION);
        $this->lastModified = Utils::createDateTime(date('d/m/Y H:i:s', filemtime($this->realPath)));
        $this->title        = pathinfo($this->realPath, PATHINFO_FILENAME);
        $this->link         = $this->generatePublicUrl();
    }

    public function getDecoratedTitle(): string
    {
        $elements = explode('.', $this->getTitle());
        unset($elements[0]);

        return implode($elements);
    }

    /**
     * Return delete url
     */
    public function generateDeleteUrl(): string
    {
        return '/api/utils/file/delete/' . $this->getEncyptedKey();
    }

    /**
     * Return public url
     */
    public function generatePublicUrl(): string
    {
        return '/api/utils/file/fetch/' . $this->getEncyptedKey();
    }

    /**
     * File contents
     *
     * @return mixed
     */
    public function getContents()
    {
        return file_get_contents($this->realPath);
    }

    /**
     * Return encrypted key
     */
    public function getEncyptedKey(): string
    {
        return Utils::encrypt($this->path);
    }

    /**
     * Last Modified date
     */
    public function getLastModified(): DateTimeInterface
    {
        return $this->lastModified;
    }

    /**
     * File link if public
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * Extenstion && mime type
     */
    public function getMimeType(): string
    {
        return $this->mimeType;
    }

    /**
     * Return real path
     */
    public function getRealPath(): string
    {
        return $this->realPath;
    }

    /**
     * File size in octed
     */
    public function getSize(): int
    {
        return $this->size;
    }

    /**
     * Get strict path
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * File title
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * File visibility
     */
    public function getVisibility(): string
    {
        return $this->visibility;
    }

    /**
     * Set the value of visibility
     *
     * @PhpUnitGen\set("visibility")
     */
    public function setVisibility(string $visibility): self
    {
        $this->visibility = $visibility;

        return $this;
    }
}
