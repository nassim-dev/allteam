<?php

namespace core\services\storage;

use Aws\S3\S3Client;
use core\config\Conf;
use core\secure\authentification\AuthServiceInterface;

/**
 * Class DigitalOceanSpaceStorage
 *
 * Description
 *
 * @category  Description
 * @version 0.1
 * @todo implement methods
 * @author Nassim Ourami <nassim.ourami@mailo.com>
 * @license CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link https://allteam.io
 * @since File available since Release 0.1
 */
class DigitalOceanSpaceStorage implements ObjectStorageInterface
{
    /**
     * Base directory
     *
     * @var string|null
     */
    protected $baseDir = null;

    /**
     * Aws S3 client
     */
    private \Aws\S3\S3Client $client;

    public function __construct(AuthServiceInterface $auth, private string $bucket, ?array $config = null)
    {
        $config ??= Conf::find('STORAGE.AMAZON');
        $this->client  = new S3Client($config);
        $this->baseDir = 'context_' . $auth->getContext()?->getIdcontext() . '/';
    }

    /**
     * Copy a file
     */
    public function copy(string $path, string $destination): ?FileItemInterface
    {
        return null;
    }

    /**
     * Delete an object
     *
     * @return void
     */
    public function delete(string $path)
    {
    }

    /**
     * Get a file
     *
     * @return FileItemInterface|null $identifier
     */
    public function get(string $path): ?FileItemInterface
    {
        $object = $this->client->getObject([
            'Bucket' => $this->bucket,
            'Key'    => $path,
        ]);

        if ($object->count() === 0) {
            return null;
        }

        return new FileItem($path, $this->baseDir);
    }

    /**
     * Check if file exist
     */
    public function has(string $path): bool
    {
        $object = $this->client->getObject([
            'Bucket' => $this->bucket,
            'Key'    => $path,
        ]);

        return ($object->count() != 0);
    }

    /**
     * Move a file
     */
    public function move(FileItemInterface|string $source, string $destination): ?FileItemInterface
    {
        $source = (is_string($source)) ? $source : $source->getRealPath();

        $this->client->putObject([
            'Bucket' => $this->bucket,
            'Key'    => $this->baseDir . $destination,
            'Body'   => file_get_contents($source)
        ]);

        return new FileItem($destination, $this->baseDir);
    }

    /**
     * Update file visibility
     *
     * @return FileItemInterface|null $identifier
     */
    public function setVisibility(string $path, string $visibility): ?FileItemInterface
    {
        return null;
    }

    /**
     * Update an existing file
     *
     * @return FileItemInterface|null $identifier
     */
    public function update(string $source, string $filename, string $visibility = self::_PUBLIC): ?FileItemInterface
    {
        $source = (is_string($source)) ? $source : $source->getRealPath();
        $file   = $this->move($source, $filename);

        return $file?->setVisibility($visibility);
    }

    /**
     * Upload new file
     *
     * @param $object
     *
     * @return FileItemInterface|null $identifier
     */
    public function upload(string $source, string $destination, string $visibility = self::_PUBLIC): ?FileItemInterface
    {
        $file = $this->move($source, $destination);

        return $file?->setVisibility($visibility);
    }
}
