<?php

namespace core\services\phpunitgen;

use PhpUnitGen\Core\Renderers\Renderer;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class CustomRenderer extends Renderer
{
    /**
     * Add a new line.
     *
     * @param string $content
     *
     * @return static
     */
    protected function addLine(string $content = ''): self
    {
        //Fix extensions namespace
        if (str_contains($content, 'namespace ')) {
            if (str_contains($content, 'extensions\extensions')) {
                $parts    = explode(' ', $content);
                $parts[1] = str_replace('extensions\extensions', '', $parts[1]);
                $parts    = explode('\\', $parts[1]);
                $content  = 'namespace extensions\\' . array_shift($parts) . '\\tests\\' . implode('\\', $parts) . ';';
            }
        }

        return parent::addLine($content);
    }
}
