<?php

namespace core\services\phpunitgen;

use core\services\phpunitgen\ClassFactory as PhpunitgenClassFactory;
use PhpUnitGen\Core\Contracts\Generators\Factories\ClassFactory;
use PhpUnitGen\Core\Contracts\Renderers\Renderer;
use PhpUnitGen\Core\Generators\Tests\DelegateTestGenerator;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class CustomTestDelegator extends DelegateTestGenerator
{
    /**
     * {@inheritdoc}
     */
    public static function implementations(): array
    {
        return [
            TestGenerator::class => CustomTestDelegator::class,
            ClassFactory::class  => PhpunitgenClassFactory::class,
            Renderer::class      => CustomRenderer::class
        ];
    }
}
