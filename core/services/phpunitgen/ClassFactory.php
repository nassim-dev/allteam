<?php

namespace core\services\phpunitgen;

use PhpUnitGen\Core\Generators\Factories\ClassFactory as FactoriesClassFactory;
use PhpUnitGen\Core\Models\TestClass;
use Roave\BetterReflection\Reflection\ReflectionClass;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class ClassFactory extends FactoriesClassFactory
{
    /**
     * {@inheritdoc}
     */
    public function make(ReflectionClass $reflectionClass): TestClass
    {
        $class = parent::make($reflectionClass);

        return $class;
    }
}
