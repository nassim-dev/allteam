<?php

namespace core\services\odtPhp;

use Odf;
use XMLReader;

/**
 * Class OdtphpCustom
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class OdtphpCustom extends Odf
{
    public function getHtml()
    {
        $this->save();
        $file = $this->getTmpfile();
        $xml  = new XMLReader();

        $xml->open("zip://$file#content.xml", 'r');

        $text = [];
        $i    = 0;
        while ($xml->read()) {
            if (XMLReader::END_ELEMENT === $xml->nodeType) {
                continue;
            }

            if ('text:h' === $xml->name) {
                ++$i;
                // Titre 1 (heading 1) devient <h1>
                // Titre 2 (heading 2) devient <h2>, etc...
                $level             = $xml->getAttribute('text:outline-level');
                $text['title'][$i] = "<h$level>" . $xml->readString() . "</h$level>";
                $text['p'][$i]     = '';
            } elseif ('text:p' === $xml->name) {
                $text['p'][$i] .= '<p>' . $xml->readString() . '</p>';
            }
        }

        //print_r($text);
    }

    /**
     * Set document variables
     *
     * @param string  $key
     * @param string  $value
     * @param boolean $encode
     * @param string  $charset
     */
    public function setVars($key, $value, $encode = false, $charset = 'UTF-8'): self
    {
        $tag = $this->config['DELIMITER_LEFT'] . $key . $this->config['DELIMITER_RIGHT'];
        if (!str_contains($this->contentXml, $tag) && !str_contains($this->stylesXml, $tag)) {
            //throw new OdfException("var $key not found in the document");
        } else {
            $value            = $encode ? $this->recursiveHtmlspecialchars($value) : $value;
            $value            = ('ISO-8859' === $charset) ? utf8_encode($value) : $value;
            $this->vars[$tag] = str_replace("\n", '<text:line-break/>', $value);

            return $this;
        }
    }

    /**
     * Recursive htmlspecialchars
     */
    protected function recursiveHtmlspecialchars($value)
    {
        if (is_array($value)) {
            return array_map([$this, 'recursiveHtmlspecialchars'], $value);
        } else {
            return htmlspecialchars($value);
        }
    }

    protected function save()
    {
        $this->file->open($this->tmpfile);
        $this->parse();
        if (!$this->file->addFromString('content.xml', $this->contentXml) || !$this->file->addFromString('styles.xml', $this->stylesXml)) {
            //throw new OdfException('Error during file export addFromString');
        }

        $lastpos   = strrpos($this->manifestXml, "\n", -15); //find second last newline in the manifest.xml file
        $manifdata = '';

        //Enter all images description in $manifdata variable

        foreach ($this->manif_vars as $val) {
            $ext       = substr(strrchr($val, '.'), 1);
            $manifdata = $manifdata . '<manifest:file-entry manifest:media-type="image/' . $ext . '" manifest:full-path="Pictures/' . $val . '"/>' . "\n";
        }

        //Place content of $manifdata variable in manifest.xml file at appropriate place
        $this->manifestXml = substr_replace($this->manifestXml, "\n" . $manifdata, $lastpos + 1, 0);
        //$this->manifestXml = $this->manifestXml ."\n".$manifdata;

        if (!$this->file->addFromString('META-INF/manifest.xml', $this->manifestXml)) {
            //throw new OdfException('Error during manifest file export');
        }

        foreach ($this->images as $imageKey => $imageValue) {
            $this->file->addFile($imageKey, 'Pictures/' . $imageValue);
        }

        $this->file->close(); // seems to bug on windows CLI sometimes
    }
}
