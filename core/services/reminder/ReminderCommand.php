<?php

namespace core\services\reminder;

use core\config\Conf;
use core\secure\user\UserInterface;
use core\utils\Utils;

/**
 * Class ReminderCommand
 *
 * Represent an action to exectue by a Reminder
 *
 * @category  Represent an action to exectue by a Reminder
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class ReminderCommand
{
    /**
     * Default Url
     */
    private string $_baseUrl;

    /**
     * Constructor
     *
     * @param $user
     */
    public function __construct(public array $options = [])
    {
        $this->_baseUrl        = Conf::find('PHP.HTTP_PROTOCOL') . '://' . Conf::find('PHP.SERVER_NAME');
        $this->options['type'] = array_keys($options)[0];
    }

    /**
     * Execute Action
     *
     * @return void
     */
    public function execute(ReminderManager $manager, UserInterface $user)
    {
        switch ($this->options['type']) {
            case 'performance':
                $this->_performanceAction($manager, $user);

                break;
            case 'task':
                $this->_taskAction($manager, $user);

                break;
            case 'transport':
                $this->_transportAction($manager, $user);

                break;
            case 'invoice':
                $this->_invoiceAction($manager, $user);

                break;
        }
    }

    /**
     * Generate Route document
     *
     * @param string $url
     *
     * @return void
     */
    private function _generateRoute($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, 'admin@trickscenique.com:' . Utils::decrypt('password'));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            [
                'Accept: application/json',
                'Content-Type: application/json'
            ]
        );
        curl_setopt($ch, CURLOPT_URL, '127.0.0.1' . $url);
        curl_exec($ch);
        curl_close($ch);
    }

    /**
     * Generate invoice Contents
     *
     * @return void
     */
    private function _invoiceAction(ReminderManager $manager, UserInterface $user)
    {
        foreach ($this->options['options'] as $key => $state) {
            if ($state) {
                switch ($key) {
                    case 'date_tosend':
                        $content = _('Document(s) to send');

                        break;
                    case 'date_topay':
                        $content = _('Document(s) to pay');

                        break;
                    case 'date_tosign':
                        $content = _('Document(s) to sign');

                        break;
                }

                if ($this->options['deadline']->mailing) {
                    $manager->mailContents[$this->options['type']][] = [
                        'title'   => $content,
                        'content' => $this->options['invoice']->getHistoricalFormat(),
                        'link'    => "$this->_baseUrl/invoice",
                        'img'     => '/ressources/fa-file.png'
                    ];
                }

                if ($this->options['deadline']->alerte) {
                    //$manager->alerteContents[] = new Alerte(
                    //    [
                    //        'iduser'     => $user->iduser,
                    //        'idcontext'  => $this->options['invoice']->idcontext,
                    //        'name'       => _('Notification') . $content,
                    //        'content'    => $this->options['invoice']->getHistoricalFormat(),
                    //        'visualized' => '-1'
                    //    ]
                    //);
                }

                if ($this->options['deadline']->todo) {
                    //$manager->todoContents[] = new Todo(
                    //    [
                    //        'iduser'        => $user->iduser,
                    //        'idcontext'     => $this->options['invoice']->idcontext,
                    //        'title'         => _('Notification') . ' - ' . $content,
                    //        'idperformance' => $this->options['invoice']->idperformance,
                    //        'content'       => $this->options['deadline']->name,
                    //        'due_date'      => $this->options['invoice']->{$key}
                    //    ]
                    //);
                }
            }
        }
    }

    /**
     * Generate performance contents
     *
     * @return void
     */
    private function _performanceAction(ReminderManager $manager, UserInterface $user)
    {
        if ($this->options['deadline']->mailing) {
            $manager->mailContents[$this->options['type']][] = [
                'title'   => _('Performance coming'),
                'content' => $this->options['performance']->getHistoricalFormat(),
                'link'    => "$this->_baseUrl/performanceGloblal/" . $this->options['performance']->idperformance,
                'img'     => '/ressources/fa-star.png'
            ];
        }

        if ($this->options['deadline']->alerte) {
            $manager->alerteContents[] = new Alerte(
                [
                    'iduser'     => $user->iduser,
                    'idcontext'  => $this->options['performance']->idcontext,
                    'name'       => _('Notification') . ' - ' . _('Performance coming'),
                    'content'    => $this->options['performance']->getHistoricalFormat(),
                    'visualized' => '-1'
                ]
            );
        }

        if ($this->options['deadline']->todo) {
            $manager->todoContents[] = new Todo(
                [
                    'iduser'        => $user->iduser,
                    'idcontext'     => $this->options['performance']->idcontext,
                    'title'         => _('Notification') . ' - ' . _('Performance coming'),
                    'idperformance' => $this->options['performance']->idperformance,
                    'content'       => $this->options['deadline']->name,
                    'due_date'      => date('Y-m-d')
                ]
            );
        }
    }

    /**
     * Generate task Contents
     *
     * @return void
     */
    private function _taskAction(ReminderManager $manager, UserInterface $user)
    {
        /*
         * Generate Route key
         */
        $paramsCrypted = $this->options['task']->cryptedParamRoadmap();
        $this->_generateRoute('/routePopup/' . $paramsCrypted);
        $params = Utils::decodeRawVar($paramsCrypted, true);
        $key    = Utils::hash($params);

        if ($this->options['deadline']->mailing) {
            $manager->mailContents[$this->options['type']][] = [
                'title'   => _('Event coming'),
                'content' => $this->options['task']->performance->name . ' (' . $this->options['task']->crew->trade->fr . ')<br>' . $this->options['task']->getEventsFormat(),
                'link'    => "$this->_baseUrl/routePopup/$paramsCrypted",
                'img'     => '/ressources/fa-bell.png',
                'files'   => [BASE_DIR . Conf::find('PHP.ROOT_PUBLIC_FILES') . 'context_' . $this->options['task']->getIdcontext() . '/route/performance.' . $key . '.doc']
            ];
        }

        if ($this->options['deadline']->alerte) {
            $manager->alerteContents[] = new Alerte(
                [
                    'iduser'     => $user->iduser,
                    'idcontext'  => $this->options['task']->idcontext,
                    'name'       => _('Notification') . ' - ' . _('Event coming'),
                    'content'    => $this->options['task']->getEventsFormat(),
                    'visualized' => '-1'
                ]
            );
        }

        if ($this->options['deadline']->todo) {
            $manager->todoContents[] = new Todo(
                [
                    'iduser'        => $user->iduser,
                    'idcontext'     => $this->options['task']->idcontext,
                    'title'         => _('Notification') . ' - ' . _('Event coming'),
                    'idperformance' => $this->options['task']->idperformance,
                    'content'       => $this->options['deadline']->name,
                    'due_date'      => date('Y-m-d')
                ]
            );
        }
    }

    /**
     * Generate transport Contents
     *
     * @return void
     */
    private function _transportAction(ReminderManager $manager, UserInterface $user)
    {
        if ($this->options['deadline']->mailing) {
            $manager->mailContents[$this->options['type']][] = [
                'title'   => _('Delivery coming'),
                'content' => $this->options['transport']->getDispatch()->getHistoricalFormat() . '<br>Livraison : ' . Utils::formatDate($this->options['transport']->start) . ' <br> Reprise : ' . Utils::formatDate($this->options['transport']->end),
                'link'    => "$this->_baseUrl/dispatchPopup/" . $this->options['transport']->getDispatch()->iddispatch,
                'img'     => '/ressources/fa-truck.png'
            ];
        }

        if ($this->options['deadline']->alerte) {
            $manager->alerteContents[] = new Alerte(
                [
                    'iduser'     => $user->iduser,
                    'idcontext'  => $this->options['transport']->idcontext,
                    'name'       => _('Notification') . ' - ' . _('Delivery coming'),
                    'content'    => $this->options['transport']->getHistoricalFormat(),
                    'visualized' => '-1'
                ]
            );
        }

        if ($this->options['deadline']->todo) {
            $manager->todoContents[] = new Todo(
                [
                    'iduser'        => $user->iduser,
                    'idcontext'     => $this->options['transport']->idcontext,
                    'title'         => _('Notification') . ' - ' . _('Delivery coming'),
                    'idperformance' => $this->options['transport']->idperformance,
                    'content'       => $this->options['deadline']->name,
                    'due_date'      => date('Y-m-d')
                ]
            );
        }
    }
}
