<?php

namespace core\services\reminder;

use app\entities\User;
use core\database\RepositoryFactoryInterface;
use core\mail\MailFactory;

/**
 * Class ReminderManager
 *
 * Reminder factory && management
 *
 * @category  Reminder factory && management
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class ReminderManager
{
    /**
     * Content to send as Alerte
     *
     * @var array
     */
    public $alerteContents = [];

    /**
     * Content to send as Mail
     *
     * @var array
     */
    public $mailContents = [
        'performance' => [],
        'task'        => [],
        'transport'   => [],
        'invoice'     => []
    ];

    /**
     * Content to send as Alerte
     *
     * @var array
     */
    public $todoContents = [];

    /**
     * Constructor
     */
    public function __construct(
        private MailFactory $mailFactory,
        private RepositoryFactoryInterface $repositoryFactory,
        public array $options = []
    ) {
    }

    /**
     * Execute actions recorded on Reminder
     *
     * @return void
     */
    public function process(Reminder $reminder)
    {
        foreach ($reminder->actions as $action) {
            $action->execute($this, $reminder->user);
        }

        $this->_generateAlertes();
        $this->_generateMail($reminder->user);
        $this->_generateTodos();
    }

    /**
     * Return formated content to send
     *
     * @return void
     */
    private function _formatMailContents()
    {
        $html = [];
        foreach ($this->mailContents as $elements) {
            foreach ($elements as $element) {
                $html[] = $element;
            }
        }

        $this->mailContents = [
            'performance' => [],
            'task'        => [],
            'transport'   => [],
            'invoice'     => []
        ];

        return $html;
    }

    /**
     * Generate Alert records
     *
     * @return void
     */
    private function _generateAlertes()
    {
        $this->repositoryFactory->table('alerte')
            ->addMultiple($this->alerteContents);
        $this->alerteContents = [];
    }

    /**
     * Generate mail to send
     *
     * @param User $user
     *
     * @return void
     */
    private function _generateMail($user)
    {
        $mail = $this->mailFactory->createFromWidget('MailTemplates', [
            'mailNotification', compact('user', $this->_formatMailContents())
        ]);
        $mail->save();
    }

    /**
     * Generate Todo records
     *
     * @return void
     */
    private function _generateTodos()
    {
        $this->repositoryFactory->table('todo')
            ->addMultiple($this->todoContents);
        $this->todoContents = [];
    }
}
