<?php

namespace core\services\reminder;

use core\database\RepositoryFactoryInterface;
use core\entities\EntitieInterface;
use core\secure\authentification\AuthServiceInterface;
use DateInterval;
use Nette\Utils\DateTime;

/**
 * Class Reminder
 *
 * Generate mails, alerte, notifications for user on configured dates
 *
 * @category Generate mails, alerte, notifications for user on configured dates
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Reminder
{
    /**
     * Array of ReminderCommand
     *
     * @var array
     */
    public $actions = [];

    /**
     * User Deadlines
     *
     * @var array
     */
    public $deadlines = [];

    /**
     * User Contexts
     *
     * @var array
     */
    public $contexts = [];

    /**
     * Ressource to recall
     *
     * @var array
     */
    public $ressourcesRecalled = [];

    /**
     * Elements already in a ReminderCommand
     */
    private array $recordedElements = [
        'performance' => [],
        'task'        => [],
        'transport'   => [],
        'invoice'     => []
    ];

    /**
     * @param $user
     */
    public function __construct(
        public EntitieInterface $user,
        private RepositoryFactoryInterface $repositoryFactory,
        private AuthServiceInterface $auth,
        public array $options = []
    ) {
        $this->options['date'] = $options['date'] ?? new DateTime();
        $this->contexts        = $this->user->findRelated(
            'context',
            EntitieInterface::ARRAY_FORMAT,
            ['active' => true]
        );
        $this->contexts[] = $user->context;
    }

    /**
     * Main Action
     *
     * @return void
     */
    public function check()
    {
        foreach ($this->getDeadlines() as $deadline) {
            $performances = $deadline->getPerformances();
            $this->_checkPerformances($deadline, $performances);
            $this->_checkModules($deadline);
        }
    }

    /**
     * Return User dealines for each context
     */
    public function getDeadlines(): array
    {
        foreach ($this->contexts as $context) {
            if (is_object($context)) {
                $this->auth->setIdcontext($context->idcontext);
                //Get Deadlines
                $deadlines = $this->repositoryFactory->table('deadline')
                    ->getList();
                foreach ($deadlines as $deadline) {
                    $this->deadlines[] = $deadline;
                }
            }
        }

        return $this->deadlines;
    }

    /**
     * Check Invoice For a spectific date
     *
     * @param  Deadline $deadline
     * @param  Invoice  $invoice
     * @return boolean
     */
    private function _checkInvoiceAt(?DateTime $date, $deadline, $invoice)
    {
        if (null === $date) {
            return false;
        }

        $startDate = $deadline->getDateTest($date);
        $endDate   = clone $startDate;
        $endDate->add(new DateInterval('PT1440M'));

        // Check Rights
        if ($this->options['date'] >= $startDate && $this->options['date'] <= $endDate && (in_array($this->user->rights, $deadline->getRights[]) || in_array($this->user->iduser, [$invoice->performance->iduser, $invoice->compagny->user->iduser]))) {
            return true;
        }

        return false;
    }

    /**
     * Check Invoice to recall
     *
     * @param Deadline $deadline
     *
     * @return void
     */
    private function _checkInvoices($deadline)
    {
        $invoices = $this->repositoryFactory->table('invoice')
            ->getList();
        foreach ($invoices as $invoice) {
            //Check if invoice has been saved
            if (!$this->_isSaved($invoice, 'invoice')) {
                $options = [
                    'date_topay'  => $this->_checkInvoiceAt($invoice->date_topay, $deadline, $invoice),
                    'date_tosign' => $this->_checkInvoiceAt($invoice->date_tosign, $deadline, $invoice),
                    'date_tosend' => $this->_checkInvoiceAt($invoice->date_tosend, $deadline, $invoice)
                ];
                if (in_array(true, $options)) {
                    $this->recordedElements['invoice'][] = $invoice->idinvoice;
                    $this->actions[]                     = new ReminderCommand(['invoice' => $invoice, 'deadline' => $deadline, 'options' => $options]);
                }
            }
        }
    }

    /**
     * Return true if Module must be recalled
     *
     * @param Deadline $deadline
     */
    private function _checkModules($deadline): bool
    {
        $module = $deadline->getModule();
        if (!is_bool($module)) {
            switch ($module->page) {
                case 'performance':
                    $performances = $this->repositoryFactory->table('performance')
                        ->getList();
                    $this->_checkPerformances($deadline, $performances);

                    break;
                case 'task':
                    $this->_checkTasks($deadline);

                    break;
                case 'transport':
                    $this->_checkTransports($deadline);

                    break;
                case 'invoice':
                    $this->_checkInvoices($deadline);

                    break;
            }

            return true;
        }

        return false;
    }

    /**
     * Check if Performance must be recalled
     *
     * @param Deadline    $deadline
     * @param Performance $performance
     *
     * @return void
     */
    private function _checkPerformance($deadline, $performance)
    {
        //Check if performance has been saved
        if (!$this->_isSaved($performance, 'performance')) {
            //Check Date
            $date      = ($deadline->days >= 0) ? $performance->end : $performance->start;
            $startDate = $deadline->getDateTest($date);
            $endDate   = clone $startDate;
            $endDate->add(new DateInterval('PT1440M'));

            // Check Rights
            if ($this->options['date'] >= $startDate && $this->options['date'] <= $endDate && (in_array($this->user->rights, $deadline->getRights[]) || in_array($this->user->iduser, [$performance->iduser, $performance->compagny->user->iduser]))) {
                //Create new ReminderCommand
                $this->recordedElements['performance'][] = $performance->idperformance;
                $this->actions[]                         = new ReminderCommand(['performance' => $performance, 'deadline' => $deadline]);
            }
        }
    }

    /**
     * Check all Performances to recall
     *
     * @param Deadline $deadline
     *
     * @return void
     */
    private function _checkPerformances($deadline, $performances)
    {
        foreach ($performances as $performance) {
            $this->_checkPerformance($deadline, $performance);
        }
    }

    /**
     * Check Task to recall
     *
     * @param Deadline $deadline
     *
     * @return void
     */
    private function _checkTasks($deadline)
    {
        $this->repositoryFactory = null;
        //Get all Events for period
        $startDate = $deadline->getDateTest($this->options['date']);
        $endDate   = clone $startDate;
        $endDate->add(new DateInterval('PT1440M'));

        $events = $this->repositoryFactory->table('event')
            ->getByParams(
                [
                    'start <= ?'  => $endDate,
                    'start >= ? ' => $startDate
                ]
            );

        foreach ($events as $event) {
            //Get all crew for user
            $crews = $event->findRelated('crew', OBJECT_FORMAT)->where(['iduser' => $this->user->iduser]);

            foreach ($crews as $crew) {
                //Get Task for each crew
                $tasks = $crew->getTask();
                foreach ($tasks as $task) {
                    // Check Rights
                    if (!$this->_isSaved($task, 'task') && ($task->iduser === $this->user->iduser || in_array($this->user->rights, $deadline->getRights[]))) {
                        //Create new ReminderCommand
                        $this->recordedElements['task'][] = $task->idtask;
                        $this->actions[]                  = new ReminderCommand(['task' => $task, 'deadline' => $deadline]);
                    }
                }
            }
        }
    }

    /**
     * Check Transport to recall
     *
     * @param Deadline $deadline
     *
     * @return void
     */
    private function _checkTransports($deadline)
    {
        $this->repositoryFactory = null;
        $transports              = $this->repositoryFactory->table('transport')->getList();
        foreach ($transports as $transport) {
            //Check if transport has been saved
            if (!$this->_isSaved($transport, 'transport')) {
                //Check Date
                $date      = ($deadline->days >= 0) ? $transport->end : $transport->start;
                $startDate = $deadline->getDateTest($date);
                $endDate   = clone $startDate;
                $endDate->add(new DateInterval('PT1440M'));

                // Check Rights
                if ($this->options['date'] >= $startDate && $this->options['date'] <= $endDate && (in_array($this->user->rights, $deadline->getRights[]) || in_array($this->user->iduser, [$transport->performance->iduser, $transport->compagny->user->iduser]))) {
                    //Create new ReminderCommand
                    $this->recordedElements['transport'][] = $transport->idtransport;
                    $this->actions[]                       = new ReminderCommand(['transport' => $transport, 'deadline' => $deadline]);
                }
            }
        }
    }

    /**
     * Return True if $object is alreay saved
     *
     * @param mixed $object
     */
    private function _isSaved($object, String $type): bool
    {
        $method = 'id' . $type;

        return (in_array($object->{$method}, $this->recordedElements[$type]));
    }
}
