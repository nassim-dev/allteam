<?php

namespace core\services;

/**
 * Class Webhook
 *
 * Stripe webhook
 *
 * @category Stripe webhook
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 *
 * @todo rewrite class
 * @deprecated rewrite class
 */
class Webhook
{
    /**
     * @param $event
     */
    public function __construct(/**
     * Stripe Response
     *
     * @var object
     */
    private $_event
    ) {
        $eventType = explode('.', $this->_event->type);
        $method    = $eventType[0] . ucfirst($eventType[1]);
        $method .= (isset($eventType[2])) ? ucfirst($eventType[2]) : null;
        if (!method_exists($this, $method)) {
            http_response_code(404);
            exit();
        }

        //$this->{$method}();
    }

    /**
     * invoice.created event
     *
     * @return void
     */
    public function invoiceCreated()
    {
        $repositoryFactory = null;
        $context           = $repositoryFactory->table('context')->getByParams(['stripecustomer' => $this->_event->data->object->customer])->fetch();
        if (!is_bool($context)) {
            $context->setMeteredbilled(0);
            $context->setLast_idpaypal($this->_event->data->object->id);
            $repositoryFactory->table('context')->update($context);

            //Send Mail
        }
    }

    /**
     * invoice.payment_failed event
     *
     * @return void
     */
    public function invoicePayment_failed()
    {
        $repositoryFactory = null;
        $context           = $repositoryFactory->table('context')->getByParams(['stripecustomer' => $this->_event->data->object->customer])->fetch();
        if (!is_bool($context)) {
            if ($context->last_idpaypal === $this->_event->data->object->id) {
                $context->active = 0;
                $repositoryFactory->table('context')->update($context);
            }

            //Send Mail
        }
    }

    /**
     * invoice.payment_succeeded event
     *
     * @return void
     */
    public function invoicePayment_succeeded()
    {
        $repositoryFactory = null;
        $context           = $repositoryFactory->table('context')->getByParams(['stripecustomer' => $this->_event->data->object->customer])->fetch();
        if (!is_bool($context)) {
            if ($context->last_idpaypal === $this->_event->data->object->id) {
                $context->active = 1;
                $repositoryFactory->table('context')->update($context);
            }

            //Send Mail
        }
    }

    /**
     * charge.succeded event
     *
     * @return void
     */
    /* public function chargeSucceded()
    {

    $charge = $repositoryFactory->table('context')->getByParams(["stripecustomer" =>  $this->_event->customer);
    if (!is]_->fetch()bool($charge)) {
    $charge->setPaid(\app\entities\Stripe::PAID);
    $repositoryFactory->table('stripe')->update($charge);

    //Send Mail

    }
    }
     */
    /**
     * charge.failed event
     *
     * @return void
     */
    /* public function chargeFailed()
    {
    $charge = $repositoryFactory->table('context')->getByParams(["stripecustomer" =>  $this->_event->customer);
    if (!is]_->fetch()bool($charge)) {
    $charge->setPaid(\app\entities\Stripe::NOTPAID);
    $repositoryFactory->table('stripe')->update($charge);

    //Send Mail
    }
    }
     */
    /**
     * charge.pending event
     *
     * @return void
     */
    /* public function chargePending()
    {
    $charge = $repositoryFactory->table('context')->getByParams(["stripecustomer" =>  $this->_event->customer);
    if (!is]_->fetch()bool($charge)) {
    $charge->setPaid(\app\entities\Stripe::PAIDINPROGRESS);
    $repositoryFactory->table('stripe')->update($charge);

    //Send Mail
    }
    }
     */
    /**
     * charge.refunded event
     *
     * @return void
     */
    /* public function chargeRefunded()
    {
    $charge = $repositoryFactory->table('context')->getByParams(["stripecustomer" =>  $this->_event->customer);
    if (!is]_->fetch()bool($charge)) {
    $charge->setPaid(\app\entities\Stripe::REFUNDED);
    $repositoryFactory->table('stripe')->update($charge);

    //Send Mail
    }
    }
     */
    /**
     * charge.dispute.created event
     *
     * @return void
     */
    /* public function chargeDisputeCreated()
    {
    $charge = $repositoryFactory->table('context')->getByParams(["stripecustomer" =>  $this->_event->customer);
    if (!is]_->fetch()bool($charge)) {
    $charge->setPaid(\app\entities\Stripe::PAIDINPROGRESS);
    $repositoryFactory->table('stripe')->update($charge);

    //Send Mail
    }

    }
     */
    /**
     * charge.dispute.funds_withdrawn event
     *
     * @return void
     */
    /* public function chargeDisputeFunds_withdrawn()
    {
    $charge = $repositoryFactory->table('context')->getByParams(["stripecustomer" =>  $this->_event->customer);
    if (!is]_->fetch()bool($charge)) {
    $charge->setPaid(\app\entities\Stripe::NOTPAID);
    $repositoryFactory->table('stripe')->update($charge);

    //Send Mail
    }

    }
     */
    /**
     * charge.dispute.funds_reinstated event
     *
     * @return void
     */
    /* public function chargeDisputeFunds_reinstated()
    {
    $charge = $repositoryFactory->table('context')->getByParams(["stripecustomer" =>  $this->_event->customer);
    if (!is]_->fetch()bool($charge)) {
    $charge->setPaid(\app\entities\Stripe::PAID);
    $repositoryFactory->table('stripe')->update($charge);

    //Send Mail
    }

    } */
    /* public function accountApplicationAuthorized()
    {

    } */

    /**
     *  invoice.upcoming event
     */
    public function invoiceUpcoming()
    {
        $repositoryFactory = null;
        $context           = $repositoryFactory->table('context')->getByParams(['stripecustomer' => $this->_event->data->object->customer])->fetch();
        if (!is_bool($context)) {
            $context->setMeteredbilled(-1);
            $repositoryFactory->table('context')->update($context);

            //Send Mail
        }
    }
}
