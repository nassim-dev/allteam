<?php

namespace core\services\generators;

use Phinx\Db\Adapter\MysqlAdapter;

class Helper
{
    public const NO_VALUE = 'NO_VALUE';

    public const PHPDOC_RELATIONS = [
        'orderable'    => 'int',
        'one-to-one'   => 'int',
        'one-to-many'  => 'array',
        'many-to-one'  => 'int',
        'many-to-many' => 'array',
    ];

    public const PHPDOC_TYPES = [
        'text'          => 'string',
        'richtext'      => 'string',
        'switch'        => 'bool',
        'checkbox'      => 'bool',
        'date'          => '\Nette\Utils\DateTime',
        'datetime'      => '\Nette\Utils\DateTime',
        'time'          => '\Nette\Utils\DateTime',
        'daterange'     => '\Nette\Utils\DateTime',
        'timerange'     => '\Nette\Utils\DateTime',
        'datetimerange' => '\Nette\Utils\DateTime',
        'dateinterval'  => '\DateInterval',
        'number'        => 'int',
        'order'         => 'int',
        'integer'       => 'int',
        'currency'      => 'int',
        'phone'         => 'string',
        'password'      => 'string',
        'email'         => 'string',
        'file'          => '\core\database\types\FileType',
        'color'         => 'string',
        'tag'           => '\core\database\types\ArrayStringType',
        'status'        => 'string',

    ];

    public const SQL_RELATIONS = [
        'orderable'    => 'integer',
        'one-to-one'   => 'integer',
        'one-to-many'  => 'integer',
        'many-to-many' => 'integer',
        'many-to-one'  => 'integer',
    ];

    public const SQL_TYPES = [
        'text'          => 'string',
        'richtext'      => 'text',
        'switch'        => 'boolean',
        'checkbox'      => 'boolean',
        'date'          => 'date',
        'integer'       => 'integer',
        'datetime'      => 'datetime',
        'time'          => 'time',
        'daterange'     => 'date',
        'timerange'     => 'time',
        'datetimerange' => 'datetime',
        'dateinterval'  => 'string',
        'number'        => 'decimal',
        'currency'      => 'decimal',
        'phone'         => 'string',
        'password'      => 'string',
        'email'         => 'string',
        'file'          => 'string',
        'color'         => 'string',
        'tag'           => 'string',
        'status'        => 'string',
    ];

    public const TABLE_RELATIONS = [
        'orderable'    => 'select',
        'one-to-one'   => 'select',
        'one-to-many'  => 'multiselect',
        'many-to-many' => 'multiselect',
        'many-to-one'  => 'select',
    ];

    public const TABLE_TYPES = [
        'text'          => 'text',
        'richtext'      => 'richtext',
        'switch'        => 'checkbox',
        'checkbox'      => 'checkbox',
        'date'          => 'date',
        'datetime'      => 'datetime',
        'time'          => 'time',
        'daterange'     => 'daterange',
        'timerange'     => 'timerange',
        'datetimerange' => 'datetimerange',
        'dateinterval'  => 'time',
        'number'        => 'number',
        'integer'       => 'number',
        'currency'      => 'number',
        'phone'         => 'phone',
        'password'      => 'password',
        'email'         => 'email',
        'file'          => 'fileinput',
        'color'         => 'color',
        'tag'           => 'autocomplete',
        'status'        => 'select',
    ];

    public const FIELD_RELATIONS = [
        'orderable'    => \core\html\form\elements\SelectElement::class,
        'one-to-one'   => \core\html\form\elements\SelectElement::class,
        'one-to-many'  => \core\html\form\elements\MultiselectElement::class,
        'many-to-many' => \core\html\form\elements\MultiselectElement::class,
        'many-to-one'  => \core\html\form\elements\SelectElement::class,
    ];

    public const FIELD_TYPES = [
        'text'          => \core\html\form\elements\TextElement::class,
        'richtext'      => \core\html\form\elements\RichtextElement::class,
        'switch'        => \core\html\form\elements\SwitchElement::class,
        'checkbox'      => \core\html\form\elements\CheckboxElement::class,
        'date'          => \core\html\form\elements\DateElement::class,
        'datetime'      => \core\html\form\elements\DatetimeElement::class,
        'time'          => \core\html\form\elements\TimeElement::class,
        'daterange'     => \core\html\form\elements\DaterangeElement::class,
        'timerange'     => \core\html\form\elements\TimerangeElement::class,
        'datetimerange' => \core\html\form\elements\DatetimerangeElement::class,
        'dateinterval'  => \core\html\form\elements\TimeElement::class,
        'number'        => \core\html\form\elements\NumberElement::class,
        'integer'       => \core\html\form\elements\NumberElement::class,
        'currency'      => \core\html\form\elements\NumberElement::class,
        'phone'         => \core\html\form\elements\TextElement::class,
        'password'      => \core\html\form\elements\PasswordElement::class,
        'email'         => \core\html\form\elements\EmailElement::class,
        'file'          => \core\html\form\elements\FileinputElement::class,
        'color'         => \core\html\form\elements\ColorpickerElement::class,
        'tag'           => \core\html\form\elements\TagElement::class,
        'status'        => \core\html\form\elements\SelectElement::class,
    ];

    public static function phpdocFormatter(string $type): string
    {
        return self::PHPDOC_TYPES[$type] ?? self::PHPDOC_RELATIONS[$type] ?? self::PHPDOC_TYPES['text'];
    }


    public static function sqlColumnFormatter(string $type): string
    {
        return self::SQL_TYPES[$type] ?? self::SQL_RELATIONS[$type] ?? self::SQL_TYPES['text'];
    }

    public static function tableColumnFormatter(string $type): string
    {
        return self::TABLE_TYPES[$type] ?? self::TABLE_RELATIONS[$type] ?? self::TABLE_TYPES['text'];
    }

    public static function fieldFormatter(string $type): string
    {
        return self::FIELD_TYPES[$type] ?? self::FIELD_RELATIONS[$type] ?? self::FIELD_TYPES['text'];
    }

    public static function getDefaultValue(array $property): mixed
    {
        if (array_key_exists($property['type'], self::PHPDOC_RELATIONS)) {
            return (!$property['isNullable']) ? 1 : self::NO_VALUE;
        }

        return  match ($property['type']) {
            'text'          => (!$property['isNullable']) ? '' : 'NULL',
            'richtext'      => (!$property['isNullable']) ? '' : 'NULL',
            'switch'        => (!$property['isNullable']) ? '1' : '0',
            'checkbox'      => (!$property['isNullable']) ? '1' : '0',
            'date'          => self::NO_VALUE,
            'datetime'      => self::NO_VALUE,
            'time'          => self::NO_VALUE,
            'daterange'     => self::NO_VALUE,
            'timerange'     => self::NO_VALUE,
            'datetimerange' => self::NO_VALUE,
            'dateinterval'  => self::NO_VALUE,
            'number'        => (!$property['isNullable']) ? 1 : 0,
            'currency'      => (!$property['isNullable']) ? 1 : 0,
            'integer'       => (!$property['isNullable']) ? 1 : 0,
            'phone'         => (!$property['isNullable']) ? '0123456789' : 'NULL',
            'password'      => (!$property['isNullable']) ? '' : 'NULL',
            'email'         => (!$property['isNullable']) ? '' : 'NULL',
            'file'          => (!$property['isNullable']) ? '' : 'NULL',
            'color'         => (!$property['isNullable']) ? '#525252' : 'NULL',
            'tag'           => (!$property['isNullable']) ? '' : 'NULL',
            'status'        => (!$property['isNullable']) ? '' : 'NULL',
        };
    }

    public static function getDefaultLength(array $property): mixed
    {
        if (array_key_exists('length', $property)) {
            if ($property['length'] == 'no set' || self::isTimeable($property['type'])) {
                return null;
            }

            return ($property['isNullable'] && $property['length'] < 4) ? 4 : $property['length'];
        }


        return  match ($property['type']) {
            'one-to-one'    => MysqlAdapter::INT_DISPLAY_REGULAR,
            'one-to-many'   => MysqlAdapter::INT_DISPLAY_REGULAR,
            'many-to-many'  => MysqlAdapter::INT_DISPLAY_REGULAR,
            'many-to-one'   => MysqlAdapter::INT_DISPLAY_REGULAR,
            'text'          => MysqlAdapter::TEXT_TINY,
            'richtext'      => MysqlAdapter::TEXT_LONG,
            'switch'        => 1,
            'checkbox'      => 1,
            'date'          => null,
            'integer'       => MysqlAdapter::INT_DISPLAY_REGULAR,
            'datetime'      => null,
            'time'          => null,
            'daterange'     => null,
            'timerange'     => null,
            'datetimerange' => null,
            'dateinterval'  => null,
            'number'        => null,
            'currency'      => null,
            'phone'         => 15,
            'password'      => 32,
            'email'         => 64,
            'file'          => MysqlAdapter::TEXT_LONG,
            'color'         => 7,
            'tag'           => MysqlAdapter::TEXT_REGULAR,
            'status'        => MysqlAdapter::TEXT_TINY,
        };
    }

    public static function isTimeable(string $type): bool
    {
        return (in_array($type, ['date', 'datetime', 'time', 'daterange', 'timerange', 'datetimerange', 'dateinterval']));
    }

    public static function withStatus(string $type): bool
    {
        return (in_array($type, ['status']));
    }

    public static function withTag(string $type): bool
    {
        return (in_array($type, ['tag']));
    }
}
