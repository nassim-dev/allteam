<?php

namespace core\services\generators;

use core\utils\Utils;
use Nette\Neon\Neon;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\Method;
use Nette\PhpGenerator\PhpNamespace;
use Nette\PhpGenerator\PsrPrinter;
use Nette\Utils\FileSystem;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.2
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.2
 * @package Allteam
 */
class EntitieGenerator
{
    private string $directory;
    private string $seed_directory;
    private string $migration_directory;
    private string $template_directory;
    private string $asset_directory;
    private string $domain;
    private static $uuid;
    private static $ref;
    public static array $passedFiles   = [];
    public static array $writed        = [];
    private static array $files        = [];
    private static array $fileContents = [];
    private static array $classTypes   = [];
    public const BASE_DIR              = __DIR__ . '/../../../';
    public static bool $overwrite      = false;
    private array $relations           = [];
    private array $manifest            = [];
    private ?array $dependencies       = null;
    private array $foreignKeys         = [];
    private ?array $isOrderable        = null;

    public function __construct(
        private string $name,
        private string $namespace,
        private array $properties = [],
        private ?string $displayName = null
    ) {
        if (self::$uuid === null) {
            self::$uuid = date('Ymdhis');
        }

        if (self::$ref === null) {
            self::$ref = uniqid();
        }

        if (!file_exists(BASE_DIR . 'extensions/manifest.neon')) {
            Utils::mkdir(BASE_DIR . 'extensions');
            exec('touch ' . BASE_DIR . 'extensions/manifest.neon');
            $this->manifest = [];
        } else {
            $this->manifest = Neon::decode(file_get_contents('nette.safe://' . BASE_DIR . 'extensions/manifest.neon')) ?? [];
        }


        $this->properties['flag_delete'] = [
            'name'          => 'flag_delete',
            'type'          => 'checkbox',
            'isNullable'    => true,
            'isSearcheable' => false
        ];

        $isExtension               = (str_contains($this->namespace, 'extensions'));
        $this->directory           = str_replace('\\', '/', $this->namespace);
        $this->seed_directory      = $isExtension ? self::BASE_DIR . $this->directory . '/config/db/seeds/' : self::BASE_DIR . 'config/db/seeds/';
        $this->migration_directory = $isExtension ? self::BASE_DIR . $this->directory . '/config/db/migrations/' : self::BASE_DIR . 'config/db/migrations/';
        $this->template_directory  = $isExtension ? self::BASE_DIR . $this->directory . '/templates/form/' : self::BASE_DIR . 'app/templates/form/';
        $this->asset_directory     = $isExtension ? self::BASE_DIR . $this->directory . '/assets/js/schema/' : self::BASE_DIR . 'assets/js/schema/';
        $this->domain              = $isExtension ? 'dgettext(\'' . str_replace('extensions\\', '', $this->namespace) . '\',' : '_(';
        $this->dependencies        = $this->properties['__dependencies'] ?? null;

        unset($this->properties['__dependencies']);
        foreach ($this->properties as $name => $property) {
            if ($property['type'] == 'orderable') {
                $newProperty                            = $property;
                $newProperty['type']                    = 'one-to-one';
                $newProperty['name']                    = 'id' . $property['targetTable'] . '_parent';
                $this->isOrderable                      = $newProperty;
                $this->properties[$newProperty['name']] = $newProperty;
                unset($this->properties[$name]);
            }
        }
    }

    private function defineControllerWidgets(Method $method, ClassType $class, PhpNamespace $namespace)
    {
        $modes = [
            'isTimeable'  => false,
            'withStatus'  => false,
            'withTag'     => false,
            'isOrderable' => ($this->isOrderable != null)];

        $hasMultiplesViews = false;
        foreach ($this->properties as $property) {
            if (Helper::isTimeable($property['type'])) {
                $modes['isTimeable'] = true;
                $hasMultiplesViews   = true;
            }

            if (Helper::withStatus($property['type'])) {
                $modes['withStatus'] = true;
                $hasMultiplesViews   = true;
            }

            if (Helper::withTag($property['type'])) {
                $modes['withTag']  = true;
                $hasMultiplesViews = true;
            }
        }

        if (!$hasMultiplesViews) {
            return;
        }

        $methodBody = '
        if (!$this->auth->isAllowed() && $this->getParameter("id") === null) {
            return $this->errorAction(403);
        }

        //Create default edit form && modals
        $this->getDi()->callInside($this, "createMainModalEditWidget");

        //Create default add form && modals
        $this->getDi()->callInside($this, "createMainModalAddWidget");


        //Default table && buttons
        $table = $this->getDi()->callInside($this, "createMainTableWidget");

        /**
         * Generate pills table
         */
        $pillTable = $this->getPillsTable();
        $pillTable->setTitle("");

        $pill = $pillTable->addPill(\'table\', _(\'Table\'));
        $pill->appendChild($table);
        $pill->setDefault(true);

        $pillTable->setDefault($pill);

        ';

        $controllerTemplate = $this->getClassType(\app\controller\SkeletonController::class);
        self::preserveMethods($class, $namespace->getName() . '\\' . $class->getName(), \app\controller\SkeletonController::class);

        foreach ($modes as $key => $activated) {
            if ($activated) {
                switch ($key) {
                    case 'isTimeable':

                        //Timeline
                        $template = $controllerTemplate->getMethod('createTimeline');
                        $body     = $template->getBody();
                        $body     = str_replace('skeleton', $this->name, $body);
                        $body     = str_replace('Skeleton', ucfirst($this->name), $body);
                        $body     = str_replace('SKELETON', strtoupper($this->name), $body);

                        $class->addMethod('createTimeline')
                            ->setReturnType(\core\html\timeline\Timeline::class)
                            ->setBody($body);

                        $methodBody .= '
        $timeline = $this->createTimeline();
        $pill = $pillTable->addPill(\'timeline\', _(\'Timeline\'));
        $pill->appendChild($timeline);
                        ';

                        //Calendar
                        $template = $controllerTemplate->getMethod('createCalendarWidget');
                        $body     = $template->getBody();
                        $body     = str_replace('skeleton', $this->name, $body);
                        $body     = str_replace('Skeleton', ucfirst($this->name), $body);
                        $body     = str_replace('SKELETON', strtoupper($this->name), $body);

                        $newMethod = $class->addMethod('createCalendarWidget')
                            ->setReturnType(\core\html\calendar\Calendar::class)
                            ->setBody($body);

                        $newMethod->addParameter('calendarFactory')
                            ->setType(\core\html\calendar\CalendarFactory::class);

                        $newMethod->addParameter('commandFactory')
                            ->setType(\core\html\CommandFactory::class);

                        $methodBody .= '
        $calendar  = $this->getDi()->call($this, \'createCalendarWidget\');
        $pill = $pillTable->addPill(\'calendar\', _(\'Calendar\'));
        $pill->appendChild($calendar);
                        ';

                        break;
                    case 'withStatus':

                        //Calendar
                        $template = $controllerTemplate->getMethod('createDraglistWidget');
                        $body     = $template->getBody();
                        $body     = str_replace('skeleton', $this->name, $body);
                        $body     = str_replace('Skeleton', ucfirst($this->name), $body);
                        $body     = str_replace('SKELETON', strtoupper($this->name), $body);


                        $newMethod = $class->addMethod('createDraglistWidget')
                            ->setReturnType(\core\html\draglist\Draglist::class)
                            ->setBody($body);

                        $newMethod->addParameter('draglistFactory')
                            ->setType(\core\html\draglist\DraglistFactory::class);

                        $newMethod->addParameter('barFactory')
                            ->setType(\core\html\bar\BarFactory::class);

                        $newMethod->addParameter('items', [])
                            ->setType('\core\database\mysql\lib\nette\CustomNetteSelection|array');

                        $methodBody .= '
        $draglist  = $this->getDi()->call($this, \'createDraglistWidget\');
        $pill = $pillTable->addPill(\'draglist\', _(\'Draglist\'));
        $pill->appendChild($draglist);
                        ';

                        break;
                    case 'withTag':

                        break;
                    case 'isOrderable':

                        //Flow
                        $template = $controllerTemplate->getMethod('createFlowWidget');
                        $body     = $template->getBody();
                        $body     = str_replace('skeleton', $this->name, $body);
                        $body     = str_replace('Skeleton', ucfirst($this->name), $body);
                        $body     = str_replace('SKELETON', strtoupper($this->name), $body);

                        $newMethod = $class->addMethod('createFlowWidget')
                            ->setReturnType(\core\html\flow\Flow::class)
                            ->setBody($body);

                        $newMethod->addParameter('flowFactory')
                            ->setType(\core\html\flow\FlowFactory::class);

                        $newMethod->addParameter('commandFactory')
                            ->setType(\core\html\CommandFactory::class);

                        $methodBody .= '
        $flow  = $this->getDi()->call($this, \'createFlowWidget\');
        $pill = $pillTable->addPill(\'flow\', _(\'Flow\'));
        $pill->appendChild($flow);
                        ';

                        break;

                    default:

                        break;
                }
            }
        }

        $method->setBody($methodBody);
    }

    public function generatePhinxSeeder(string $name): ClassType
    {
        $class = new ClassType(self::formatMigrationName($name) . 'Seeder');

        $class->setExtends(\Phinx\Seed\AbstractSeed::class);

        $array   = [];
        $seeders = [];
        foreach ($this->properties as $key => $property) {
            if ($this->propertyType($property) != 'RELATION') {
                $array[$key] = Helper::getDefaultValue($property);
                $array[$key] = ($array[$key] == Helper::NO_VALUE) ? '' : $array[$key];

                if ($property['type'] === 'one-to-one') {
                    $referenceTable = $property['targetTable'] ?? ((str_starts_with($property['name'], 'id')) ? substr($property['name'], 2) : $property['name']);
                    $seeders[]      = ucfirst($referenceTable) . 'Seeder';
                }
            }
        }

        $method = $class->addMethod('getDependencies');
        $body   = '
        return ' . var_export($seeders, true) . ';';
        $method->addBody($body);


        $method = $class->addMethod('run');
        $method->setReturnType('void');

        $body = '$data = [' . var_export($array, true) . '];';
        $body .= '
        $table = $this->table(\'' . $name . '\');';
        $body .= '
        $table->insert($data)
            ->saveData();';
        $body .= '
        //$table"->truncate();';

        $method->addBody($body);
        self::generateDoc($class);

        return $class;
    }

    private function propertyType(array $property): string
    {
        if (in_array($property['name'], ['created_at', 'id' . $property['name'], 'updated_at'])) {
            return 'PHINX_AUTOCREATE';
        }

        if (array_key_exists($property['type'], Helper::FIELD_TYPES) || $property['type'] === 'one-to-one') {
            return 'SQL_COLUMN';
        }

        return 'RELATION';
    }

    public function generateForeignKeys()
    {
        foreach ($this->foreignKeys as $table => $properties) {
            $class = new ClassType('Update' . self::formatMigrationName($table) . self::$ref . 'Table');

            $class
                ->setFinal()
                ->setExtends(\Phinx\Migration\AbstractMigration::class);

            $method = $class->addMethod('change');

            $method->addComment(' Change Method.\r\n')
                ->addComment('')
                ->addComment(' Write your reversible migrations using this method.')
                ->addComment('')
                ->addComment(' More information on writing migrations is available here:')
                ->addComment(' https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method')
                ->addComment('')
                ->addComment(' Remember to call "create()" or "update()" and NOT "save()" when working')
                ->addComment(' with the Table class.');

            $method->setReturnType('void');

            $body = '$table = $this->table(\'' . $table . '\');';


            foreach ($properties as $property) {
                $config = [
                    'name'   => 'idx_' . $property['name'] . '_' . $table,
                    'unique' => false,
                ];

                $parameters = ['delete' => $property['isNullable'] ? 'SET_NULL' : 'CASCADE', 'update' => 'NO_ACTION'];

                $referenceTable = $property['targetTable'] ?? ((str_starts_with($property['name'], 'id')) ? substr($property['name'], 2) : $property['name']);

                $referenceColumn = (isset($property['targetTable'])) ? 'id' . $property['targetTable'] : $property['name'];
                $body .= '
        if($table->exists() && !$table->hasForeignKey(\'' . $property['name'] . '\')){
            $table->addForeignKey(\'' . $property['name'] . '\', \'' . $referenceTable . '\', \'' . $referenceColumn . '\', ' . var_export($parameters, true) . ');
        }';


                /* $body .= '
        if($table->exists() && !$table->hasIndexByName(\' . $config['name'] . '\')){
            $table->addIndex(\'' . $property['name'] . '\', ' . var_export($config, true) . ');
        }';*/
            }

            $body .= '
        $table->update();';

            $method->addBody($body);

            self::generateDoc($class);
            self::$uuid++;
            $date     = self::$uuid + 1000;
            $filename = $this->migration_directory . $date . '_update_' . $table . '_' . self::$ref . '_table.php';
            self::write($class, $filename);

            return $class;
        }
    }


    public function generatePhinxMigrations(
        string $name,
        array $properties,
        array $indexes = [],
        bool $force = false,
        bool $isCreated = false,
        ?string $append = null
    ): ClassType {
        $class = new ClassType('Create' . self::formatMigrationName($name) . self::$ref . $append . 'Table');

        $class
            ->setFinal()
            ->setExtends(\Phinx\Migration\AbstractMigration::class);

        $method = $class->addMethod('change');

        $method->addComment(' Change Method.\r\n')
            ->addComment('')
            ->addComment(' Write your reversible migrations using this method.')
            ->addComment('')
            ->addComment(' More information on writing migrations is available here:')
            ->addComment(' https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method')
            ->addComment('')
            ->addComment(' Remember to call "create()" or "update()" and NOT "save()" when working')
            ->addComment(' with the Table class.');

        $method->setReturnType('void');
        $keys = ['id' . $name];
        foreach ($properties as $property) {
            if (!$force && $this->propertyType($property) != 'SQL_COLUMN') {
                continue;
            }
            $key[] = $property['name'];
        }

        $keys[] = 'created_at';
        $keys[] = 'updated_at';
        if (!$isCreated) {
            /*$body = 'if($this->hasTable(\'' . $name . '\')){
        $this->query("SET foreign_key_checks = 0;");
        $this->table(\'' . $name . '\')->drop()->save();
        $this->query("SET foreign_key_checks = 1;");
            }
            ';*/

            $body = '
        $table = $this->table(\'' . $name . '\', [\'id\' => \'id' . $name . '\',  \'primary_key\' => \'id' . $name . '\']);';
        } else {
            $body = '
        $table = $this->table(\'' . $name . '\');';
        }

        $body .= '
        $columns = ($table->exists()) ? $table->getColumns() : [];';

        $body .= '
        $properties = ' . var_export($keys, true) . ';';

        $body .= '
        $columnNames = [];
        foreach($columns as $column){
            $columnNames[$column->getName()] = true;
            if (!in_array($column->getName(), $properties)) {
                $table->removeColumn($column->getName());
            }
        }';



        $this->foreignKeys[$name] = $this->foreignKeys[$name] ?? [];
        foreach ($properties as $property) {
            if ($property['type'] == 'one-to-one' && $property['name'] != "id$name") {
                $this->foreignKeys[$name][] = $property;
            }

            if (!$force && $this->propertyType($property) != 'SQL_COLUMN') {
                continue;
            }

            $length = Helper::getDefaultLength($property);
            $length = ($length != null) ? ', \'length\' => ' . $length : '';

            $type     = Helper::sqlColumnFormatter($property['type']);
            $default  = Helper::getDefaultValue($property);
            $nullable = ($property['isNullable']) ? 'true' : 'false';

            if (in_array($property['type'], ['daterange', 'datetimerange', 'timerange'])) {
                $body .= '
        if(!isset($columnNames[\'' . $property['name'] . '\'])){
            $table->addColumn(\'' . $property['name'] . '_start\', \'' . $type . '\', [\'null\' => ' . $nullable . $length . ']);
        }else {
            $table->changeColumn(\'' . $property['name'] . '_start\', \'' . $type . '\', [\'null\' => ' . $nullable . $length . ']);
        }';
                $body .= '
        if(!isset($columnNames[\'' . $property['name'] . '\'])){
            $table->addColumn(\'' . $property['name'] . '_end\', \'' . $type . '\', [\'null\' => ' . $nullable . $length . ']);
        }else{
            $table->changeColumn(\'' . $property['name'] . '_end\', \'' . $type . '\', [\'null\' => ' . $nullable . $length . ']);
        }';

                continue;
            }

            if ($default === Helper::NO_VALUE) {
                $body .= '
        if(!isset($columnNames[\'' . $property['name'] . '\'])){
            $table->addColumn(\'' . $property['name'] . '\', \'' . $type . '\', [\'null\' => ' . $nullable . $length . ']);
        }else{
            $table->changeColumn(\'' . $property['name'] . '\', \'' . $type . '\', [\'null\' => ' . $nullable . $length . ']);
        }';

                continue;
            }

            $default = is_string($default) ? '\'' . $default . '\'' : $default;

            if ($property['type'] === 'tag') {
                $body .= '
        if(!$table->exists() || !$table->hasIndexByName(\'idx_' . $property['name'] . '_' . $name . '\')){
            $table->addIndex(\'' . $property['name'] . '\', [ \'type\' => \'fulltext\',  \'name\' =>\'idx_' . $property['name'] . '_' . $name . '\']);
        }';
            }

            $body .= '
        if(!isset($columnNames[\'' . $property['name'] . '\'])){
            $table->addColumn(\'' . $property['name'] . '\', \'' . $type . '\', [ \'default\' => ' . $default . ', \'null\' => ' . $nullable . $length . ']);
        }else{
            $table->changeColumn(\'' . $property['name'] . '\', \'' . $type . '\', [ \'default\' => ' . $default . ', \'null\' => ' . $nullable . $length . ']);
        }';
        }

        $body .= '
        if(!$table->exists()){
            $table->addTimestamps();
        }';
        //$body .= '
        //->addIndex(\'id' . $name . '\', [\'unique\' => true])';


        foreach ($indexes as $config) {
            $index = is_array($config['key']) ? var_export($config['key'], true) : '[\'' . $config['key'] . '\']';
            unset($config['key']);
            $body .= '
        if(!$table->exists() || !$table->hasIndexByName(' . $config['name'] . ')){
            $table->addIndex(' . $index . ', ' . var_export($config, true) . ');
        }';
        }

        $body .= '
        if($table->exists()) {
            $table->update();
        } else {
            $table->create();
        }' ;

        $method->addBody($body);

        self::generateDoc($class);

        return $class;
    }

    public static function write(ClassType $class, string $filename, ?PhpNamespace $namespace = null): bool
    {
        $printer   = new PsrPrinter();
        $fileExist = file_exists($filename);
        if ($fileExist && !self::$overwrite) {
            self::$passedFiles[] = [str_replace(__DIR__, '', $filename), true, false];

            return false;
        }

        $elements = explode('_', $filename);
        unset($elements[0]);
        $registredFile = implode('', $elements);
        if (isset(self::$files[$registredFile]) && self::$files[$registredFile] == $class->getName()) {
            self::$passedFiles[] = [str_replace(__DIR__, '', $filename), false, true];

            return false;
        }

        self::$files[$registredFile] = $class->getName();

        $elements = explode('/', $filename);
        array_pop($elements);
        $directory = implode('/', $elements);
        if (!is_dir($directory)) {
            mkdir($directory, 0777, true);
        }

        self::$writed[] = [$filename, $fileExist, !$fileExist];
        if ($namespace != null) {
            $namespace->add($class);
            file_put_contents($filename, '<?php ' . $printer->printNamespace($namespace));

            return true;
        }

        file_put_contents($filename, '<?php ' . $printer->printClass($class, $class->getNamespace()));

        return true;
    }

    public function getEntitie(bool $isCreated = false, array $needUpdate = []): ?ClassType
    {
        if (empty($needUpdate)) {
            return null;
        }

        $namespace = new PhpNamespace($this->namespace . '\\entities\\' . $this->name);
        $class     = new ClassType(ucfirst($this->name), $namespace);
        $class->addAttribute(\core\entities\EntitieAttribute::class, ['tableName' => $this->name])
            ->setExtends(\core\entities\EntitieAbstract::class);

        $this->generateProperties($class);
        $this->generateJsFiles();

        $constructor = $class->addMethod('__construct')
            ->setPublic()
            ->setBody('parent::__construct($data, $selection);');

        $constructor->addParameter('data')
            ->setType('array');

        $constructor->addParameter('selection')
            ->setType(\core\database\mysql\lib\nette\CustomNetteSelection::class)
            ->setNullable();

        $class->addMethod('getRequiredField')
            ->setPublic()
            ->setBody(self::getMethodBody('getRequiredField', $namespace->getName() . '\\' . $class->getName()) ?? ' return \'name\';')
            ->setReturnType('string|null|array');

        $doc                = [' Representation of mysql row ' . $this->name];
        $doc[]              = ' ';
        $visibilities       = [];
        $regeneratedMethods = ['getRequiredField', '__construct', 'getSearchFields', 'getPropertyVisibilities'];
        foreach ($class->getProperties() as $property) {
            if ($property->getName() === 'searchFields') {
                continue;
            }



            $setterBody = '$this->' . $property->getName() . ' = $value;

        return $this;';

            $getterBody = 'return $this->' . $property->getName() . ';';

            $realname         = (str_ends_with($property->getName(), '_s')) ? substr($property->getName(), 0, -2) : $property->getName();
            $methodName       = $property->getName();
            $setterType       = $property->getType();
            $getterReturnType = $property->getType();

            if ($property->isNullable()) {
                $setterType       = '?' . $setterType;
                $getterReturnType = '?' . $getterReturnType;
            }

            if ($property->isPrivate()) {
                $class->removeProperty($property->getName());

                $getterBody = 'return $this->findRelated(\'' . $realname . '\');';
                $setterBody = '$this->updateRelations(\'' . $realname . '\', $value);

        return $this;';

                $methodName       = substr($realname . 's', 2);
                $setterType       = '?array';
                $getterReturnType = 'array|\core\database\mysql\lib\nette\CustomNetteSelection';
            } else {
                $doc[]                              = ' @property ' . $getterReturnType . ' $' . $property->getName();
                $visibilities[$property->getName()] = $this->properties[$property->getName()]['visibility'] ?? '@public';
            }

            $setter = $class->addMethod('set' . ucfirst($methodName))
                ->setPublic()
                ->setBody($setterBody);

            $setter->addParameter('value')
                ->setType($setterType);
            $setter->setReturnType('self');


            $setter->addComment(' Set value of propertie ' . $property->getName())
                ->addComment(' @param ' . str_replace('?', 'null|', $setterType) . ' $value')
                ->addComment(' @return self');


            $getter = $class->addMethod('get' . ucfirst($methodName))
                ->setPublic()
                ->setBody($getterBody);
            $getter->setReturnType($getterReturnType);

            $getter->addComment(' Get value of propertie ' . $property->getName())
                ->addComment(' @return ' . str_replace('?', 'null|', $getterReturnType));

            $regeneratedMethods[] = $getter->getName();
            $regeneratedMethods[] = $setter->getName();
        }

        $class->addMethod('getPropertyVisibilities')
            ->setPublic()
            ->setReturnType('array')
            ->setBody('return ' . var_export($visibilities, true) . ';');


        $doc[] = ' ';
        self::generateDoc($class, $doc);
        self::preserveMethods($class, $namespace->getName() . '\\' . $class->getName(), $regeneratedMethods);


        self::write($class, self::BASE_DIR . $this->directory . '/entities/' . $this->name . '/' . $class->getName() . '.php', $namespace);

        //if ($writed) {
        $phinx    = $this->generatePhinxMigrations($this->name, $this->properties, [], false, $isCreated);
        $filename = $this->migration_directory . ++self::$uuid . '_create_' . $this->name . '_' . self::$ref . '_table.php';
        self::write($phinx, $filename);

        $this->generateRelationTables($needUpdate);
        $this->generateExtension();

        if (!$isCreated) {
            $seeder   = $this->generatePhinxSeeder($this->name);
            $filename = $this->seed_directory . self::formatMigrationName($this->name) . 'Seeder.php';
            self::write($seeder, $filename);
        }

        $this->generateForeignKeys();
        //}

        return $class;
    }


    private static function formatMigrationName(string $name): string
    {
        $elements = explode('_', $name);
        $return   = array_map(function ($element): string {
            return ucfirst($element);
        }, $elements);

        return implode('', $return);
    }

    private function generateRelationTables(array $changes = [])
    {
        $indexKey = ['name' => 'id' . $this->name, 'type' => 'integer', 'isNullable' => false];

        foreach ($this->relations as $key => $property) {
            if (!isset($changes[$key])) {
                continue;
            }

            $property['type'] = 'integer';
            $columns          = [
                $key               => $property,
                'id' . $this->name => $indexKey
            ];
            $name = $this->name . '_' . (str_starts_with($key, 'id')) ? substr($key, 2) : $key;

            $this->foreignKeys[$name]   = $this->foreignKeys[$name] ?? [];
            $this->foreignKeys[$name][] = 'id' . $this->name;
            $this->foreignKeys[$name][] = $key;

            $indexes   = [];
            $indexes[] = [
                'key'    => ['id' . $this->name, $key],
                'unique' => true,
                'name'   => 'idx_id' . $this->name . '_' . $key . '_' . $name
            ];

            $phinx    = $this->generatePhinxMigrations($name . '_link', $columns, $indexes, force:true, isCreated:false, append:'Link');
            $filename = $this->migration_directory . ++self::$uuid . '_create_' . $name . '_link_' . self::$ref . '_link_table.php';

            self::write($phinx, $filename);
        }
    }

    private function generateProperties(ClassType $class)
    {
        $class->addMethod('getSearchFields')
            ->setPublic()
            ->setBody('return $this->searchFields;')
            ->setReturnType('array');

        $class->addProperty('id' . $this->name)
            ->setType('int')
            ->setProtected()
            ->setNullable()
            ->setValue(null);
        $searcheable = [];
        foreach ($this->properties as $property) {
            $type = Helper::phpdocFormatter($property['type']);

            if (in_array($property['type'], ['daterange', 'datetimerange', 'timerange'])) {
                $prop_start = $class->addProperty($property['name'] . '_start')
                    ->setType($type)
                    ->setProtected();
                $prop_end = $class->addProperty($property['name'] . '_end')
                    ->setType($type)
                    ->setProtected();

                if ($property['isNullable']) {
                    $prop_start->setNullable()
                        ->setValue(null);
                    $prop_end->setNullable()
                        ->setValue(null);
                }

                if ($property['isSearcheable']) {
                    $searcheable[] = $property['name'] . '_start';
                    $searcheable[] = $property['name'] . '_end';
                }

                continue;
            }



            if ($property['isSearcheable']) {
                $searcheable[] = $property['name'];
            }

            if ($this->propertyType($property) === 'RELATION') {
                $prop = $class->addProperty($property['name'] . '_s');

                $this->relations[$property['name']] = $property;
                $prop->setPrivate()
                    ->setType('array|\core\database\mysql\lib\nette\CustomNetteSelection')
                    ->setNullable()
                    ->setValue(null);
            } else {
                $prop = $class->addProperty($property['name'])
                    ->setType($type)
                    ->setProtected();
            }

            if ($property['isNullable'] && $type != 'bool') {
                $prop->setNullable()
                    ->setValue(null);
            }

            if ($type == 'bool') {
                $prop->setValue(false);
            }

            if ($type == 'string' && !$property['isNullable']) {
                $prop->setValue('');
            }
        }

        $class->addProperty('created_at')
            ->setType("\Nette\Utils\DateTime")
            ->setProtected()
            ->setNullable()
            ->setValue(null);

        $class->addProperty('updated_at')
            ->setType("\Nette\Utils\DateTime")
            ->setProtected()
            ->setNullable()
            ->setValue(null);

        $class->addProperty('searchFields', $searcheable)
            ->setType('array')
            ->setPrivate();
    }

    private function generateJsFiles()
    {
        $filename  = $this->asset_directory . ucfirst($this->name) . '.js';
        $fileExist = file_exists($filename);
        $elements  = explode('/', $filename);
        array_pop($elements);
        $directory = implode('/', $elements);
        if (!is_dir($directory)) {
            mkdir($directory, 0777, true);
        }

        self::$writed[] = [$filename, $fileExist, !$fileExist];


        $getters = '';
        foreach ($this->properties as $property) {
            $prop = $property['name'];

            if ($this->propertyType($property) != 'SQL_COLUMN') {
                continue;
            }

            $getters .= <<<JS
    get {$prop}() {
        return this.{$prop};
    }
JS;
        }

        $name      = ucfirst($this->name);
        $namelower = $this->name;

        $contents = <<<JS
/** This file is autogenerated, don't modify */
export default class {$name} {
    // Prototype method
    save() {
        return db.{$namelower}.put(this); // Will only save own props.
    }
    {$getters}
}
JS;
        file_put_contents($filename, $contents);


        $filename  = $this->asset_directory . ucfirst($this->name) . '.json';
        $fileExist = file_exists($filename);
        $schema    = ['id' . $this->name];
        foreach ($this->properties as $property) {
            if ($this->propertyType($property) != 'SQL_COLUMN') {
                continue;
            }

            $schema[] = $property['name'];
        }

        self::$writed[] = [$filename, $fileExist, !$fileExist];
        file_put_contents($filename, json_encode($schema, JSON_PRETTY_PRINT));
    }

    private static function generateDoc(ClassType $class, ?array $contents = null)
    {
        $class->addComment(' Class ' . $class->getName());
        $class->addComment(' ');
        if ($contents != null) {
            foreach ($contents as $line) {
                $class->addComment($line);
            }
        }

        $class->addComment(' @category  Description');
        $class->addComment(' @version   Release: 0.2');
        $class->addComment(' @author    Nassim Ourami <nassim.ourami@mailo.com>');
        $class->addComment(' @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/');
        $class->addComment(' ');
        $class->addComment(' @link    https://allteam.io');
        $class->addComment(' @since   File available since Release 0.2');
        $class->addComment(' @package Allteam');

        return $class;
    }

    /**
     * Return Repository class
     *
     * @return ClassType
     */
    public function getRepository(): ClassType
    {
        $namespace = new PhpNamespace($this->namespace . '\\entities\\' . $this->name);

        $template = self::getClassType(\core\entities\skeleton\SkeletonRepository::class);

        $body = '<?php ' . (new PsrPrinter())->printClass($template, $namespace);
        $body = str_replace('skeleton', $this->name, $body);
        $body = str_replace('Skeleton', ucfirst($this->name), $body);
        $body = str_replace('SKELETON', strtoupper($this->name), $body);

        $class = ClassType::fromCode($body);

        /**
         * Fix bug : null typehint is anormaly removed
         */
        $method     = $class->getMethod('getHtmlList');
        $parameters = $method->getParameters();
        $parameters['order']->setNullable();
        $parameters['order']->setDefaultValue(null);
        $method->setParameters($parameters);

        self::generateDoc($class);
        self::preserveMethods($class, $namespace->getName() . '\\' . $class->getName(), \core\entities\skeleton\SkeletonRepository::class);

        $directory = str_replace('\\', '/', $namespace->getName());
        self::write($class, self::BASE_DIR . $directory . '/' . $class->getName() . '.php', $namespace);

        return $class;
    }

    /**
     * Return Listener class
     *
     * @return ClassType
     */
    public function getListener(): ClassType
    {
        $namespace = new PhpNamespace($this->namespace . '\\event');
        $class     = new ClassType(ucfirst($this->name) . 'Listener', $namespace);
        $class->setExtends(\app\event\EntitieListenerBase::class);

        $method = $class->addMethod('defaultListener');
        $method->addAttribute(
            \core\events\ListenerAttribute::class,
            [
                'event' => 'DEFAULT_' . strtoupper($this->name) . '_EVENT'
            ]
        )
            ->setReturnType('array')
            ->setBody('return $args;');
        $parameter = $method->addParameter('args');

        $parameter->setType('array')
            ->setReference();

        self::generateDoc($class);
        self::preserveMethods($class, $namespace->getName() . '\\' . $class->getName(), ['defaultListener']);

        $directory = str_replace('\\', '/', $namespace->getName());
        self::write($class, self::BASE_DIR . $directory . '/' . $class->getName() . '.php', $namespace);

        return $class;
    }

    /**
     * Return Decorator class
     *
     * @return ClassType
     */
    public function getDecorator(): ClassType
    {
        $namespace = new PhpNamespace($this->namespace . '\\entities\\' . $this->name . '\\decorator');

        $template = self::getClassType(\core\entities\skeleton\decorator\SkeletonDecorator::class);

        $body = '<?php ' . (new PsrPrinter())->printClass($template, $namespace);
        $body = str_replace('skeleton', $this->name, $body);
        $body = str_replace('Skeleton', ucfirst($this->name), $body);
        $body = str_replace('SKELETON', strtoupper($this->name), $body);

        $class = ClassType::fromCode($body);

        /**
         * Fix bug : null typehint is anormaly removed
         */
        $method     = $class->getMethod('decorate');
        $parameters = $method->getParameters();
        $parameters['args']->setNullable();
        $parameters['args']->setDefaultValue(null);
        $method->setParameters($parameters);

        self::generateDoc($class);
        self::preserveMethods($class, $namespace->getName() . '\\' . $class->getName(), \core\entities\skeleton\decorator\SkeletonDecorator::class);

        $directory = str_replace('\\', '/', $namespace->getName());
        self::write($class, self::BASE_DIR . $directory . '/' . $class->getName() . '.php', $namespace);

        return $class;
    }

    /**
     * Return HistoricalDecorator class
     *
     * @return ClassType
     */
    public function getHistoricalDecorator(): ClassType
    {
        $namespace = new PhpNamespace($this->namespace . '\\entities\\' . $this->name . '\\decorator');
        $template  = self::getClassType(\core\entities\skeleton\decorator\HistoricalDecorator::class);

        $body = '<?php ' . (new PsrPrinter())->printClass($template, $namespace);
        $body = str_replace('skeleton', $this->name, $body);
        $body = str_replace('Skeleton', ucfirst($this->name), $body);
        $body = str_replace('SKELETON', strtoupper($this->name), $body);

        $class = ClassType::fromCode($body);

        /**
         * Fix bug : null typehint is anormaly removed
         */
        $method     = $class->getMethod('decorate');
        $parameters = $method->getParameters();
        $parameters['args']->setNullable();
        $parameters['args']->setDefaultValue(null);
        $method->setParameters($parameters);

        self::generateDoc($class);
        self::preserveMethods($class, $namespace->getName() . '\\' . $class->getName(), \core\entities\skeleton\decorator\HistoricalDecorator::class);

        $directory = str_replace('\\', '/', $namespace->getName());
        self::write($class, self::BASE_DIR . $directory . '/' . $class->getName() . '.php', $namespace);

        return $class;
    }

    /**
     * Return DatatableDecorator class
     *
     * @return ClassType
     */
    public function getDatatableDecorator(): ClassType
    {
        $namespace = new PhpNamespace($this->namespace . '\\entities\\' . $this->name . '\\decorator');

        $template = self::getClassType(\core\entities\skeleton\decorator\DatatableDecorator::class);

        $body = '<?php ' . (new PsrPrinter())->printClass($template, $namespace);
        $body = str_replace('skeleton', $this->name, $body);
        $body = str_replace('Skeleton', ucfirst($this->name), $body);
        $body = str_replace('SKELETON', strtoupper($this->name), $body);

        $class = ClassType::fromCode($body);

        /**
         * Fix bug : null typehint is anormaly removed
         */
        $method     = $class->getMethod('decorate');
        $parameters = $method->getParameters();
        $parameters['args']->setNullable();
        $parameters['args']->setDefaultValue(null);
        $method->setParameters($parameters);


        $methodBody = [];

        foreach ($this->properties as $key => $property) {
            if (in_array($key, ['flag_delete'])) {
                continue;
            }

            $methodBody[] = $this->generateDatatableRow($key, $property);
        }

        $method->setBody(str_replace('/*__CONFIG__*/', implode('', $methodBody), $method->getBody()));

        self::generateDoc($class);
        self::preserveMethods($class, $namespace->getName() . '\\' . $class->getName(), \core\entities\skeleton\decorator\DatatableDecorator::class);


        $directory = str_replace('\\', '/', $namespace->getName());
        self::write($class, self::BASE_DIR . $directory . '/' . $class->getName() . '.php', $namespace);

        return $class;
    }

    /**
     * Return Command class
     *
     * @return ClassType
     */
    public function getCommand(): ClassType
    {
        $namespace = new PhpNamespace($this->namespace . '\\entities\\' . $this->name . '\\command');

        $template = self::getClassType(\core\entities\skeleton\command\SkeletonCommand::class);

        $body = '<?php ' . (new PsrPrinter())->printClass($template, $namespace);
        $body = str_replace('skeleton', $this->name, $body);
        $body = str_replace('Skeleton', ucfirst($this->name), $body);
        $body = str_replace('SKELETON', strtoupper($this->name), $body);

        $class = ClassType::fromCode($body);

        self::generateDoc($class);
        self::preserveMethods($class, $namespace->getName() . '\\' . $class->getName(), \core\entities\skeleton\command\SkeletonCommand::class);

        $directory = str_replace('\\', '/', $namespace->getName());
        self::write($class, self::BASE_DIR . $directory . '/' . $class->getName() . '.php', $namespace);

        return $class;
    }

    private function generateExtension()
    {
        if (str_contains($this->namespace, 'extensions')) {
            $namespace = new PhpNamespace($this->namespace);

            $name = explode('\\', $this->namespace);
            $name = end($name);

            $class = new ClassType(ucfirst($name) . 'Extension', $namespace);
            $class->setExtends(\core\extensions\AbstractExtension::class)
                ->addImplement(\core\extensions\ExtensionInterface::class);

            $class->addAttribute(\core\extensions\ExtensionConfig::class, [
                'name'        => ucfirst($name),
                'description' => 'Describe the extension',
                'depends'     => ($this->dependencies != null) ? $this->dependencies : [],
                'icon'        => 'fa-x-ray'
            ]);

            $className = $namespace->getName() . '\\' . $class->getName();
            $class->addMethod('boot')
                ->setPublic()
                ->setBody(self::getMethodBody('boot', $className) ?? '
        /**
        * PUT SOME PIECE OF CODE
        */');

            $class->addMethod('build')
                ->setPublic()
                ->setBody(self::getMethodBody('build', $className) ?? '
        /**
        * PUT SOME PIECE OF CODE
        */');

            $class->addMethod('destroy')
                ->setPublic()
                ->setBody(self::getMethodBody('destroy', $className) ?? '
        /**
        * PUT SOME PIECE OF CODE
        */');

            $method = $class->addMethod('init')
                ->setPublic()
                ->setBody(self::getMethodBody('init', $className) ?? 'parent::init($app);
        /**
        * PUT SOME PIECE OF CODE
        */');
            $parameter = $method->addParameter('app');
            $parameter->setType(\core\app\ApplicationServiceInterface::class);

            self::generateDoc($class);
            self::preserveMethods($class, $className, ['boot', 'destroy', 'init', 'build']);

            $directory = str_replace('\\', '/', $namespace->getName());
            self::write($class, self::BASE_DIR . $directory . '/' . $class->getName() . '.php', $namespace);


            if (!isset($this->manifest[$className])) {
                $this->manifest[$className] = [
                    'isEnabled'   => false,
                    'isInstalled' => false,
                    'url'         => null,
                ];
                file_put_contents('nette.safe://' . BASE_DIR . 'extensions/manifest.neon', Neon::encode($this->manifest, blockMode:true));
            }

            return $class;
        }
    }

    /**
     * Return AppController class
     *
     * @return ClassType
     */
    public function getAppController(bool $generateModel = true): ClassType
    {
        $namespace = new PhpNamespace($this->namespace . '\\controller');

        $class = new ClassType(ucfirst($this->name) . 'Controller', $namespace);
        $class->setExtends(\app\controller\ControllerBase::class);

        $class->addProperty('name', $this->name)
            ->setProtected();

        $indexAction = $class->addMethod('indexAction')
            ->setPublic()
            ->setBody('parent::indexAction();')
            ->addAttribute(\core\routing\RouteAttribute::class, [
                'url'         => '/' . $this->name,
                'method'      => 'GET', 'permission' => ['read:' . $this->name],
                'encoding'    => false,
                'displayName' => $this->displayName ?? $this->name
            ]);

        $this->defineControllerWidgets($indexAction, $class, $namespace);

        $method = $class->addMethod('byIdAction')
            ->setPublic()
            ->setBody('parent::byIdAction($modelFactory, $barFactory);')
            ->addAttribute(\core\routing\RouteAttribute::class, [
                'url'      => '/' . $this->name . '/:id',
                'method'   => 'GET', 'permission' => ['read:' . $this->name],
                'encoding' => false,
            ]);

        $method->addParameter('modelFactory')
            ->setType(\core\model\ModelServiceInterface::class);

        $method->addParameter('barFactory')
            ->setType(\core\html\bar\BarFactory::class);

        if ($generateModel) {
            $class->addMethod('getModelBase')
                ->setProtected()
                ->setBody('return $this->getModel("' . ucfirst($this->name) . '");')
                ->setReturnType($this->namespace . '\\model\\' . ucfirst($this->name) . 'Model');
        }

        self::generateDoc($class);
        self::preserveMethods($class, $namespace->getName() . '\\' . $class->getName(), \app\controller\SkeletonController::class);

        $directory = str_replace('\\', '/', $namespace->getName());
        self::write($class, self::BASE_DIR . $directory . '/' . $class->getName() . '.php', $namespace);

        return $class;
    }

    /**
     * Generate Model class
     *
     * @return ClassType
     */
    public function getModel(): ClassType
    {
        $namespace = new PhpNamespace($this->namespace . '\\model');
        $class     = new ClassType(ucfirst($this->name) . 'Model', $namespace);
        $class->setExtends(\app\model\ModelBase::class);

        $class->addProperty('tableName', $this->name)
            ->setProtected()
            ->setType('string')
            ->setNullable();

        $class->addProperty('tableNameUcFirst', ucfirst($this->name))
            ->setProtected()
            ->setType('string')
            ->setNullable();

        $class->addProperty('tableNameUpper', strtoupper($this->name))
            ->setProtected()
            ->setType('string')
            ->setNullable();

        self::generateDoc($class);
        self::preserveMethods($class, $namespace->getName() . '\\' . $class->getName(), []);

        $directory = str_replace('\\', '/', $namespace->getName());
        self::write($class, self::BASE_DIR . $directory . '/' . $class->getName() . '.php', $namespace);

        return $class;
    }

    /**
     * Get api controller
     *
     * @return ClassType
     */
    public function getApiController(?string $name = null): ClassType
    {
        $name ??= $this->name;

        $template = self::getClassType(\app\api\controller\SkeletonController::class);

        $namespace = new PhpNamespace($this->namespace . '\\api\\controller');

        $body = '<?php ' . (new PsrPrinter())->printClass($template, $namespace);
        $body = str_replace('skeleton', $name, $body);
        $body = str_replace('Skeleton', ucfirst($name), $body);
        $body = str_replace('_(', $this->domain, $body);
        $body = str_replace('\\_(', $this->domain, $body);

        $class = ClassType::fromCode($body);
        $class->setExtends(\app\api\controller\ApiControllerBase::class);

        self::generateDoc($class);
        self::preserveMethods($class, $namespace->getName() . '\\' . $class->getName(), \app\api\controller\SkeletonController::class);

        $directory = str_replace('\\', '/', $namespace->getName());
        self::write($class, self::BASE_DIR . $directory . '/' . $class->getName() . '.php', $namespace);

        return $class;
    }

    /**
     * Generate Datatable widget
     *
     * @return ClassType
     */
    public function getDatatableWidget(): ClassType
    {
        $nameCleaned = str_replace('_', ' ', ucfirst($this->name));
        $template    = self::getClassType(\app\widgets\datatable\SkeletonDatatableWidget::class);
        $namespace   = new PhpNamespace($this->namespace . '\\widgets\\datatable');

        $body = '<?php ' . (new PsrPrinter())->printClass($template, $namespace);
        $body = str_replace('skeleton', $this->name, $body);
        $body = str_replace('Skeleton', ucfirst($this->name), $body);
        $body = str_replace('SKELETON', strtoupper($this->name), $body);
        $body = str_replace('_(', $this->domain, $body);
        $body = str_replace('\\_(', $this->domain, $body);
        $body = str_replace($this->domain . '\'' . ucfirst($this->name), $this->domain . '\'' . $nameCleaned, $body);
        $body = str_replace('_(\'' . ucfirst($this->name), $this->domain . '\'' . $nameCleaned, $body);

        $class      = ClassType::fromCode($body);
        $method     = $class->getMethod(strtoupper($this->name) . '_LIST');
        $methodBody = [];

        foreach ($this->properties as $key => $property) {
            if (in_array($key, ['flag_delete'])) {
                continue;
            }

            $methodBody[] = $this->generateColumn($key, $property);
        }


        $method->setBody(str_replace("'__CONFIG__'", implode(',
        ', $methodBody), $method->getBody()));


        self::generateDoc($class);
        self::preserveMethods($class, $namespace->getName() . '\\' . $class->getName(), \app\widgets\datatable\SkeletonDatatableWidget::class);
        $directory = str_replace('\\', '/', $namespace->getName());
        self::write($class, self::BASE_DIR . $directory . '/' . $class->getName() . '.php', $namespace);

        return $class;
    }

    /**
     * Generate Form widget
     *
     * @return ClassType
     */
    public function getFormWidget(array $changes = []): ClassType
    {
        $template = self::getClassType(\app\widgets\form\SkeletonFormWidget::class);

        $namespace = new PhpNamespace($this->namespace . '\\widgets\\form');
        $body      = '<?php ' . (new PsrPrinter())->printClass($template, $namespace);
        $body      = str_replace('skeleton', $this->name, $body);
        $body      = str_replace('Skeleton', ucfirst($this->name), $body);
        $body      = str_replace('_(', $this->domain, $body);
        $body      = str_replace('\\_(', $this->domain, $body);

        $class      = ClassType::fromCode($body);
        $method     = $class->getMethod('add');
        $methodBody = str_replace('return $form;', '', $method->getBody());

        foreach ($this->properties as $key => $property) {
            if (in_array($key, ['flag_delete'])) {
                continue;
            }

            $methodBody .= $this->generateField($key, $property, $changes);
        }

        $methodBody .= '
        return $form;';

        $method->setBody($methodBody);
        $method->setBody(str_replace('a ' . $this->name, 'a ' . str_replace('_', ' ', $this->name), $method->getBody()));

        $class = self::generateDoc($class);
        $class = self::preserveMethods($class, $namespace->getName() . '\\' . $class->getName(), \app\widgets\form\SkeletonFormWidget::class);

        $directory = str_replace('\\', '/', $namespace->getName());
        self::write($class, self::BASE_DIR . $directory . '/' . $class->getName() . '.php', $namespace);

        //Html temlate
        $filename  = $this->template_directory . 'form.template.' . $this->name . '.html';
        $template  = self::file_get_contents(self::BASE_DIR . 'app/templates/form/form.template.skeleton.html');
        $fileExist = file_exists($filename);

        if ($fileExist) {
            $old      = self::file_get_contents($filename);
            $template = ($template != $old) ? $old : $template;
        }

        self::$writed[] = [$filename, $fileExist, !$fileExist];
        FileSystem::write($filename, $template);

        return $class;
    }

    private static function file_get_contents(string $filename)
    {
        if (!isset(self::$fileContents[$filename])) {
            self::$fileContents[$filename] = file_get_contents($filename);
        }

        return self::$fileContents[$filename];
    }

    /**
     * Return ClassType object
     *
     * @return ClassType
     */
    private static function getClassType(string $className): ClassType
    {
        if (!isset(self::$classTypes[$className])) {
            self::$classTypes[$className] = ClassType::withBodiesFrom($className, true);
        }

        return self::$classTypes[$className];
    }

    /**
     * Append user custom methods exept $regeneratedMehods
     *
     * @param ClassType $class
     *
     * @return ClassType
     */
    private static function preserveMethods(ClassType $class, string $className, array|string $regeneratedMehods): ClassType
    {
        if (class_exists($className)) {
            $oldClassTemplate = self::getClassType($className);
            $excluded         = (is_array($regeneratedMehods)) ? $regeneratedMehods : get_class_methods($regeneratedMehods);
            $methods          = $class->getMethods();

            foreach ($oldClassTemplate->getMethods() as $oldmethod) {
                if (!in_array($oldmethod->getName(), $excluded)) {
                    $methods[strtolower($oldmethod->getName())] = $oldmethod;

                    continue;
                }

                if (!$class->hasMethod($oldmethod->getName())) {
                    continue;
                }

                $templateMethod = $class->getMethod($oldmethod->getName());
                if ($oldmethod->getBody() != $templateMethod->getBody()) {
                    $methods[strtolower($oldmethod->getName())] = $oldmethod;
                }
            }

            $class->setMethods($methods);
        }

        return $class;
    }

    /**
     * Return method body
     *
     * @param string $methodUndocumented function
     *
     * @return string|null
     */
    private static function getMethodBody(string $method, string $className): ?string
    {
        if (!class_exists($className)) {
            return null;
        }

        $oldClassTemplate = self::getClassType($className);
        if (!$oldClassTemplate->hasMethod($method)) {
            return null;
        }

        return $oldClassTemplate->getMethod($method)->getBody();
    }

    private function generateField(string $name, array $property, array $changes = []): string
    {
        if ($name === 'id' . $this->name) {
            return '';
        }

        $type       = Helper::fieldFormatter($property['type']);
        $columnType = $this->propertyType($property);
        $entity     = (str_starts_with($name, 'id')) ? substr($name, 2) : $name;
        $label      = ucfirst(str_replace('_', ' ', $entity));

        if (!str_starts_with($type, '\\')) {
            $type = '\\' . $type;
        }

        if (method_exists(\core\html\form\Helper::class, $property['type'])) {
            $field = '
        /** @var ' . $type . ' $field */
        $field = \core\html\form\Helper::' . $property['type'] . '($form);
            ';
        } elseif (isset($property['isForm'])) {
            $filter   = (is_string($property['isForm'])) ? var_export(explode(',', $property['isForm']), true) : 'null';
            $callback = str_replace(' ', '', ucwords(str_replace('-', ' ', $property['type'])));

            $field = '
        /** @var \core\html\form\FormFactory $formFactory */
        $formFactory = $this->getDi()->singleton(\core\html\form\FormFactory::class);
        $subForm     = $formFactory->createFromWidget(\'' . $entity . '\', [\'add\']);

        /** @var ' . $type . ' $field */
        $field       = $form->addFormAsField($subForm,\'' . $entity . '\', ' . $filter . ', \'' . $callback . '\');
            ';
        } else {
            $field = '
        /** @var ' . $type . ' $field */
        $field = $form->add(' . $type . '::class);';
        }

        $field .= ($property['required'] ?? false) ? '
        $field->isRequired()' : '';

        if ($columnType === 'RELATION' || $property['type'] === 'one-to-one') {
            return $field .= '
        $field->setName("' . $name . '")
            ->setLabel(' . $this->domain . '\'' . $label . '\'))
            ->btn("' . $entity . '");
            ';
        }

        if ($property['type'] === 'status') {
            if (isset($changes[$name])) {
                $this->createStatusTable($name);
            }

            return $field .= '
        $field->setRessource("' . $this->name . '_' . $name . '_list")
            ->setName("' . $name . '")
            ->setLabel(' . $this->domain . '\'' . $label . '\'))
            ->btn("' . $this->name . '_' . $name . '_list");
            ';
        }

        if ($property['type'] === 'tag') {
            $this->createStatusTable($name);

            return $field .= '
        $field->setRessource("' . $this->name . '_' . $name . '_list")
            ->setName("' . $name . '")
            ->setLabel(' . $this->domain . '\'' . $label . '\'));
            ';
        }


        return $field .= '
        $field->setName("' . $name . '")
                ->setLabel(' . $this->domain . '\'' . $label . '\'));
                ';
    }

    private function createStatusTable(string $name)
    {
        $columns = [
            'key'   => ['name' => 'key', 'type' => 'text', 'isNullable' => true],
            'value' => ['name' => 'value', 'type' => 'text', 'isNullable' => true]
        ];

        $name = $this->name . '_' . $name . '_list';

        $phinx    = $this->generatePhinxMigrations($name, $columns);
        $filename = $this->migration_directory . ++self::$uuid . '_create_' . $name . '_' . self::$ref . '_table.php';
        self::write($phinx, $filename);

        $this->getApiController($name);
    }

    private function generateColumn(string $name, array $property): string
    {
        $entity = (str_starts_with($name, 'id')) ? substr($name, 2) : $name;
        $type   = Helper::tableColumnFormatter($property['type']);
        $column = "'" . $name . "' => [
            'label' => " . $this->domain . '\'' . ucfirst(str_replace('_', ' ', $entity)) . "'),
            'type' => '" . $type . "'";

        $columnType = $this->propertyType($property);

        if ($columnType === 'RELATION' || $property['type'] === 'one-to-one') {
            $column .= ',
            \'url\' => (new \core\routing\Link($this->controllerService->getClass(\'' . $entity . '\', \'api\'), \'htmlListAction\'))->getEndpoint()';
        }

        if (in_array($property['type'], ['tag', 'status'])) {
            $apiController = $this->namespace . '\api\controller\\' . ucfirst($this->name) . 'Controller::class';
            $column .= ',
            \'url\' => (new \core\routing\Link(' . $apiController . ', \'tagListAction\'))->getEndpoint()';
        }

        return $column .= '
        ]';
    }

    private function generateDatatableRow(string $name, array $property): string
    {
        $entity = (str_starts_with($name, 'id')) ? substr($name, 2) : $name;
        $type   = Helper::tableColumnFormatter($property['type']);

        $columnType = $this->propertyType($property);

        if ($property['type'] === 'one-to-one') {
            return '$row[\'' . $name . '\'] = $object->' . $entity . '?->historical();';
        }

        if ($columnType === 'RELATION') {
            $code = '
        $relatedRows = $object->findRelated(\'' . $entity . '\', $object::HTML_FORMAT, $args[\'selector\'] ?? null);
        $rows        = [];
        foreach ($relatedRows as $relatedRow) {
            $rows[] = $relatedRow->historical();
        }

        $row[\'' . $name . '\']  = implode(\'\', $rows);
        ';

            return $code;
        }

        return '$row[\'' . $name . '\'] = $object->' . $name . ';';
    }
}
