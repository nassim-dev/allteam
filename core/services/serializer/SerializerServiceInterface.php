<?php

namespace core\services\serializer;

/**
 * Undocumented interface
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
interface SerializerServiceInterface
{
    public function serialize($object, null|string|SerializerInterface $serializer = null): string;
    public function unserialize(string $object);
    public function rebuild(DescriptionInterface $description);
    public function describe($object): ?DescriptionInterface;
}
