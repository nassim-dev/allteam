<?php

namespace core\services\serializer;

use core\DI\DiProvider;
use core\DI\Implementation;
use core\utils\GeneratorProvider;
use Opis\Closure\SerializableClosure;
use WeakReference;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Description implements DescriptionInterface
{
    use DiProvider;
    use GeneratorProvider;

    private ?string $serializer = null;

    public function __construct(private mixed $datas)
    {
        $this->datas = $this->format($datas);
    }

    /**
     * Check null && empty object
     *
     * @return boolean
     */
    public function isUseless(): bool
    {
        return (!is_array($this->datas) || !isset($this->datas['type']) || empty($this->datas));
    }

    public function initialize(): mixed
    {
        return $this->processValue($this->datas);
    }

    /**
     * Main process method
     */
    private function processValue(mixed $value)
    {
        return match (true) {
            (!is_array($value) || !isset($value['type'])) => $value,
            ($value['type'] === '@internal')              => unserialize($value['value']),
            ($value['type'] === 'array')                  => $this->processArray($value['value']),
            ($value['type'] === 'closure')                => $value['value']->getClosure(),
            ($value['type'] === 'resource')               => 'ressource:' . $value['value'],
            ($value['type'] === 'object')                 => $this->processObject($value['value'], $value['classname'], $value['isSingleton']),
            default                                       => $value
        };
    }

    /**
     * Get serializer object
     *
     * @return string|null
     */
    public function getSerializer(): ?string
    {
        return $this->serializer;
    }

    /**
     * Set object serializer class
     *
     * @param string $serializer
     *
     * @return self
     */
    public function setSerializer(string $serializer): self
    {
        $this->serializer = $serializer;

        return $this;
    }

    /**
     * Get the value of datas
     *
     * @PhpUnitGen\get("datas")
     *
     * @return mixed
     */
    public function getDatas(): mixed
    {
        return $this->datas;
    }

    public function __serialize(): array
    {
        return [
            'datas'      => $this->datas,
            'serializer' => $this->serializer
        ];
    }

    /**
     * @return void
     */
    public function __unserialize(array $data)
    {
        $this->serializer = $data['serializer'];
        $this->datas      = $data['datas'];
    }

    /**
     * Process object element
     *
     * @param array  $value
     * @param string $className
     *
     * @return object
     */
    private function processObject(?array $value, string $className, bool $isSingleton): object
    {
        $method = $isSingleton ? 'singleton' : 'make';

        return $this->getDi()->{$method}($className, $this->processValue($value) ?? []);
    }

    /**
     * Process an array element
     *
     * @param array $value
     *
     * @return array
     */
    private function processArray(array $value): array
    {
        $return = [];
        foreach ($this->getGenerator($value) as [$key, $child]) {
            $return[$key ?? $child['name']] = $this->processValue($child);
        }

        return $return;
    }

    /**
     * Main format method
     *
     * @param mixed       $object
     * @param string|null $name
     *
     * @return mixed
     */
    private function format(mixed $object, ?string $name = null): mixed
    {
        $di = DependencieInjectorInterface::class;

        return match (true) {
            (is_array($object))                                  => $this->formatArray($object, $name),
            (is_callable($object))                               => $this->formatCallable($object, $name),
            (is_resource($object))                               => $this->formatRessource($object, $name),
            (is_object($object) && is_subclass_of($object, $di)) => null,
            (is_object($object))                                 => $this->formatObject($object, $name),
            default                                              => $object,
        };
    }


    /**
     * Format array element
     *
     * @param array       $object
     * @param string|null $property
     *
     * @return array
     */
    private function formatArray(array $object, ?string $property = null): array
    {
        $values = [];

        foreach ($this->getGenerator($object) as [$name,$value]) {
            $values[$name] = $this->format($value, $name);
        }

        return [
            'name'  => $property,
            'type'  => gettype($object),
            'value' => $values
        ];
    }

    /**
     * Format callable element
     *
     * @param callable    $object
     * @param string|null $property
     *
     * @return array
     */
    private function formatCallable(callable $object, ?string $property = null): array
    {
        return [
            'name'  => $property,
            'type'  => 'closure',
            'value' => new SerializableClosure(\Closure::fromCallable($object))
        ];
    }

    /**
     * Format object element
     *
     * @param object      $object
     * @param string|null $property
     *
     * @return array
     */
    private function formatObject(object $object, ?string $property = null): array
    {
        $type = 'object';
        if (method_exists($object, 'setDi')) {
            $object->setDi(null);
        }
        if (method_exists($object, '__serialize')) {
            $properties = serialize($object);
            $type       = '@internal';
        }
        if ($object instanceof WeakReference) {
            $properties = $this->format($object->get());
        }

        if (!isset($properties)) {
            $implementation = $this->getDi()->getImplementations($object::class);
            if ($implementation instanceof Implementation) {
                $properties = [];
                $signature  = $implementation->getConstructor();
                $vars       = get_object_vars($object);

                if (null !== $signature) {
                    foreach ($signature as $parameter => $typehint) {
                        $getter                 = 'get' . ucfirst($parameter);
                        $properties[$parameter] = match (true) {
                            (isset($vars[$parameter]))                                  => $this->format($vars[$parameter], $parameter),
                            (method_exists($object, $getter))                           => $this->format(call_user_func_array([$object, $getter], []), $parameter),
                            ($parameter === 'data' && str_contains($typehint, 'array')) => $vars,
                            default                                                     => '__unmatched__',
                        };

                        if ($properties[$parameter] == '__unmatched__') {
                            unset($properties[$parameter]);
                        }
                    }
                }
            }
        }

        return  [
            'name'        => $property,
            'type'        => $type,
            'classname'   => $object::class,
            'value'       => $properties,
            'isSingleton' => $this->getDi()->isSingleton($object)
        ];
    }

    /**
     * Format ressource element
     *
     * @param resource    $object
     * @param string|null $property
     *
     * @return array
     */
    private function formatRessource($object, ?string $property = null): array
    {
        return [
            'name'  => $property,
            'type'  => 'resource',
            'value' => get_resource_id($object)
        ];
    }
}
