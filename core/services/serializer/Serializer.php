<?php

namespace core\services\serializer;

use core\DI\DiProvider;
use core\utils\GeneratorProvider;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Serializer implements SerializerServiceInterface
{
    use DiProvider;
    use GeneratorProvider;

    public const SEPARATOR = '<|>';

    public function __construct(private SerializerInterface $defaultSerializer)
    {
    }

    public function serialize($object, null|string|SerializerInterface $serializer = null): string
    {
        if (is_object($serializer)) {
            $usedSerializer = $serializer;
        } elseif ($serializer === null || !class_exists($serializer)) {
            $usedSerializer = $this->defaultSerializer;
        } else {
            /** @var SerializerInterface $usedSerializer  */
            $usedSerializer = $this->getDi()->singleton($serializer);
        }

        return $usedSerializer->serialize(
            $this->describe($object)
        ) . self::SEPARATOR . $usedSerializer::class;
    }

    public function unserialize(string $object)
    {
        list($serializedString, $serializerClass) = explode(self::SEPARATOR, $object);

        if (!class_exists($serializerClass)) {
            throw new SerializerException("Invalid serializer : $serializerClass");
        }
        /** @var SerializerInterface $serializer  */
        $serializer  = $this->getDi()->singleton($serializerClass);
        $description = $serializer->unserialize($serializedString);

        return $this->rebuild($description);
    }

    public function rebuild(DescriptionInterface $description)
    {
        return $description->initialize();
    }

    public function describe($object): DescriptionInterface
    {
        return $this->getDi()->make(DescriptionInterface::class, ['datas' => $object]);
    }
}
