<?php

namespace core\services\parser;

/**
 * Abstract class AbstractCsvParser
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
abstract class AbstractCsvParser
{
    /**
     * Liste des colonnes du fichier csv
     */
    protected array $_columns = [];

    /**
     * Est-ce que l'on doit ignorer la première ligne (si elle contient le nom des colonnes par exemple)
     */
    protected bool $_ignoreFirstLine = true;

    /**
     * @var int
     */
    protected $_line_number = 0;

    /**
     * Le séparateur pour les colonnes ';' par défaut, mais ça peut être ','
     */
    protected string $_separator = ';';

    /**
     * Méthode pour nettoyer le fichier csv
     */
    public function clean($value): string
    {
        return trim(stripslashes($value));
    }

    /**
     * @param $filename
     */
    public function parse($filename)
    {
        $notifier = null;
        if (!file_exists($filename)) {
            $notifier->notify(_('You do not have to select any file, please select a file in CSV format.'), $notifier->DANGER);
        }

        $this->preParse();
        $f = fopen($filename, 'rb');
        if (null === $f) {
            $notifier->notify('Cound not open ' . $filename . ' for read', $notifier->DANGER);
        }

        $this->_line_number = 0;
        if ($this->_ignoreFirstLine) {
            fgets($f, 1024);
            ++$this->_line_number;
        }

        while ($line = fgets($f, 1024)) {
            $line = $this->clean($line);
            $line = str_getcsv($line, $this->_separator);
            if (count($line) !== count($this->_columns)) {
                continue;
            }

            $line = array_combine($this->_columns, $line);
            array_walk($line, [$this, 'clean']);
            ++$this->_line_number;
            $this->parseLine((object) $line);
        }

        $this->postParse();
    }

    /**
     * Méthode qui est executer pour chaque ligne du fichier csv
     *
     * @param $line stdClass La ligne a traiter
     * @param $i    Le       numéro de la ligne
     */
    abstract public function parseLine($line);

    /**
     * Méthode executer avant que le fichier ne soit parser
     *
     * @param $input
     * @param $delimiter
     * @param $enclosure
     * @param $escape
     *
     * @return mixed
     */
    public function postParse()
    {
    }

    /**
     * @param $input
     * @param $delimiter
     * @param $enclosure
     * @param $escape
     *
     * @return mixed
     */
    public function preParse()
    {
    }
}

if (!function_exists('str_getcsv')) {
    function str_getcsv($input, $delimiter = ',', $enclosure = '"', $escape = '\\')
    {
        $tmp = tmpfile();
        fwrite($tmp, $input);
        rewind($tmp);
        $data = fgetcsv($tmp, null, $delimiter, $enclosure);
        fclose($tmp);

        return $data;
    }
}
