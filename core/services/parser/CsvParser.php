<?php

namespace core\services\parser;

use stdClass;

/**
 * Class CsvParser
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class CsvParser extends AbstractCsvParser
{
    private array $_arrayLines = [];

    private array $_expectedColumns = [];

    /**
     * CsvParser constructor.
     *
     * @param $file
     * @param string $separator
     * @param bool   $ignoreFirstLine
     */
    public function __construct($file, $separator = ';', $ignoreFirstLine = true)
    {
        $this->_separator = $separator;
        $this->setColumns($file);
        $this->_ignoreFirstLine = $ignoreFirstLine;
    }

    /**
     * @param $column
     */
    public function addColumn($column)
    {
        $this->_columns[] = $column;
    }

    /**
     * @return array
     */
    public function getArrayLines()
    {
        return $this->_arrayLines;
    }

    // traitement d'une ligne

    /**
     * @return array
     */
    public function getColumns()
    {
        return $this->_columns;
    }

    /**
     * @return array
     */
    public function getExpectedcolumns()
    {
        return $this->_expectedColumns;
    }

    /**
     * @param stdClass $line
     */
    public function parseLine($line)
    {
        $this->setArrayLines($line);
    }

    /**
     * Méthode pour nettoyer le fichier csv
     */
    public function postParse()
    {
    }

    /**
     * Méthode executer après que tous le fichier ai été parsé
     */
    public function preParse()
    {
    }

    /**
     * @param $var
     */
    public function setArrayLines($var)
    {
        $this->_arrayLines[] = $var;
    }

    /**
     * @param mixed[] $var
     */
    public function setExpectedcolumns(array $var)
    {
        $this->_expectedColumns = $var;
    }

    // traitement d'une ligne

    /**
     * @param $file
     */
    private function setColumns($file)
    {
        $aData = [];
        if (($handle = fopen($file, 'r')) !== false) {
            $filesize = filesize($file);
            $firstRow = true;

            while (($data = fgetcsv($handle, $filesize, $this->_separator)) !== false) {
                if ($firstRow) {
                    $aData    = $data;
                    $firstRow = false;
                }
            }

            fclose($handle);
        }

        $this->_columns = $aData;
    }
}
