<?php

namespace core\model;

/**
 * Interface ModelInterface
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
interface ModelInterface
{
    /**
     * Get the value of tableName
     *
     * @PhpUnitGen\get("tableName")
     */
    public function getTableName(): ?string;

    /**
     * Get the value of tableNameUcFirst
     *
     * @PhpUnitGen\get("tableNameUcFirst")
     */
    public function getTableNameUcFirst(): ?string;

    /**
     * Get the value of tableNameUpper
     *
     * @PhpUnitGen\get("tableNameUpper")
     */
    public function getTableNameUpper(): ?string;
}
