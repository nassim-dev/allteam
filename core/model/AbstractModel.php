<?php

namespace core\model;

use core\database\RepositoryFactoryInterface;
use core\DI\DiProvider;
use core\messages\request\HttpRequestInterface;
use core\messages\request\RequestParameterTrait;
use core\secure\authentification\AuthServiceInterface;

/**
 * Class AbstractModel
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class AbstractModel implements ModelInterface
{
    use RequestParameterTrait;
    use DiProvider;

    /**
     * @var string|null
     */
    protected ?string $tableName = null;

    /**
     * @var string|null
     */
    protected ?string $tableNameUcFirst = null;

    /**
     * @var string|null
     */
    protected ?string $tableNameUpper = null;

    /**
     * Constructor
     */
    public function __construct(
        protected AuthServiceInterface $auth,
        protected HttpRequestInterface $request,
        protected RepositoryFactoryInterface $repositoryFactory,
        ?string $name = null,
    ) {
        $this->setTableName($name ?? $this->tableName);
    }

    /**
     * Get the value of tableName
     *
     * @PhpUnitGen\get("tableName")
     */
    public function getTableName(): ?string
    {
        return $this->tableName;
    }

    /**
     * Get the value of tableNameUcFirst
     *
     * @PhpUnitGen\get("tableNameUcFirst")
     */
    public function getTableNameUcFirst(): ?string
    {
        return $this->tableNameUcFirst;
    }

    /**
     * Get the value of tableNameUpper
     *
     * @PhpUnitGen\get("tableNameUpper")
     */
    public function getTableNameUpper(): ?string
    {
        return $this->tableNameUpper;
    }

    /**
     * Set the value of tableName
     */
    public function setTableName(?string $tableName): self
    {
        if ($tableName !== null) {
            $this->tableName        = strtolower($tableName);
            $this->tableNameUcFirst = ucfirst($tableName);
            $this->tableNameUpper   = strtoupper($tableName);
        }

        return $this;
    }
}
