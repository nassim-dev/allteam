<?php

namespace core\model;

use core\containers\ContainerInterface;
use core\containers\DefaultContainer;
use core\factory\AbstractFactory;
use core\factory\FactoryException;
use core\factory\FactoryInterface;

/**
 * Class ModelFactory
 *
 * Model factory
 *
 * @category  Model factory
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class ModelFactory extends AbstractFactory implements ModelServiceInterface, FactoryInterface
{
    public function __construct()
    {
        $this->addNamespace('app\model');
    }

    /**
     * Get a model
     *
     * @PhpUnitGen\assertInstanceOf("ModelInterface::class")
     */
    public function create(string $model, array $params = []): ModelInterface
    {
        $class = $this->_getClass($model);

        return $this->getInstance($class) ?? $this->register($this->getDi()->singleton($class, $params), $class);
    }

    /**
     * Get all instances or filtered by callback
     *
     * @PhpUnitGen\get("instances")
     */
    public function getInstances(?callable $filter = null): ContainerInterface
    {
        $this->instances ??= new DefaultContainer();
        if (!null === $filter) {
            $entries = array_filter($this->instances->toArray(), $filter);

            return new DefaultContainer($entries);
        }

        return $this->instances;
    }

    /**
     * Get registred elements
     */
    public function get(string $identifier): ModelInterface
    {
        return $this->getInstance($identifier) ?? $this->create($identifier);
    }

    /**
     * Get class name if exist ot throw error
     *
     * @throws FactoryException
     */
    private function _getClass(string $identifier): string
    {
        if (class_exists($identifier)) {
            return $identifier;
        }

        foreach ($this->getNamespaces() as $namespace) {
            $defaultIdentifier = $namespace . '\\' . ucfirst($identifier) . 'Model';
            if (class_exists($defaultIdentifier)) {
                return $defaultIdentifier;
            }
        }

        throw new FactoryException("$identifier can not be instanciate, because it's not a class");
    }
}
