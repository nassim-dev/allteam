<?php

namespace core\facade;

use core\DI\DI;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
abstract class HelperFacade implements FacadeInterface
{
    protected static ?string $serviceInterface = null;

    /**
     * Get object
     *
     * @return mixed
     */
    protected static function __getSingleton(): mixed
    {
        return DI::singleton(static::$serviceInterface);
    }

    public static function __callStatic(string $method, array $arguments): mixed
    {
        $singleton = static::__getSingleton();

        if (null === $singleton) {
            throw new FacadeException('You must provide a class to construct with setObject()');
        }

        return call_user_func_array([$singleton, $method], $arguments);
    }

    /**
     * Get the value of serviceInterface
     *
     * @PhpUnitGen\get("serviceInterface")
     *
     * @return null|string
     */
    public static function getServiceInterface(): ?string
    {
        return self::$serviceInterface;
    }
}
