<?php

namespace core\transport;

/**
 * Undocumented interface
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
interface TransportServiceInterface
{
    public const ERROR            = 'ERROR';
    public const ACTIVITY         = 'ACTIVITY';
    public const CHAT             = 'CHAT';
    public const JSCONFIG         = 'JSCONFIG';
    public const DATASOURCE       = 'DATASOURCE';
    public const RELOADCOMPONENTS = 'RELOADCOMPONENTS';
    public const RECONNECT        = 'RECONNECT';

    /**
     * Send a message
     *
     * @return void
     */
    public function send(array $message, array $parameters);

    /**
     * Receive a message
     *
     * @return void
     */
    public function receive(array $message, array $parameters);
}
