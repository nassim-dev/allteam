<?php

namespace core\transport\websocket;

use Thruway\AbstractSession;
use Thruway\Message\ErrorMessage;
use Thruway\Message\SubscribedMessage;
use Thruway\Message\SubscribeMessage;
use Thruway\Message\UnsubscribedMessage;
use Thruway\Message\UnsubscribeMessage;

/**
 * Undocumented interface
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
interface SubscriberInterface
{
    public function subscribe(string $topic, ?array $options, AbstractSession $session, ?callable $callback): SubscribeMessage;

    public function unsubscribe(string $topic, AbstractSession $session, ?callable $callback): ?UnsubscribeMessage;

    public function process(SubscribedMessage|UnsubscribedMessage $message, ?AbstractSession $session);

    public function handleError(ErrorMessage $errorMessage, ?AbstractSession $session);
}
