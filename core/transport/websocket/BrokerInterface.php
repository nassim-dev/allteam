<?php

namespace core\transport\websocket;

use Ratchet\MessageComponentInterface;
use Ratchet\WebSocket\WsServerInterface;
use Thruway\AbstractSession;
use Thruway\Message\HelloMessage;
use Thruway\Message\PublishedMessage;
use Thruway\Message\PublishMessage;
use Thruway\Message\SubscribedMessage;
use Thruway\Message\SubscribeMessage;
use Thruway\Message\UnsubscribedMessage;
use Thruway\Message\UnsubscribeMessage;
use Thruway\Message\WelcomeMessage;

/**
 * Undocumented interface
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
interface BrokerInterface extends MessageComponentInterface, WsServerInterface
{
    public function handleSubscribe(SubscribeMessage $message, ?AbstractSession $session): SubscribedMessage;

    public function handleHello(HelloMessage $message, ?AbstractSession $session): WelcomeMessage;

    public function handleUnsubscribe(UnsubscribeMessage $message, ?AbstractSession $session): UnsubscribedMessage;

    public function handlePublish(PublishMessage $message, ?AbstractSession $session): PublishedMessage;
}
