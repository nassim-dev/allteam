<?php

namespace core\transport\websocket;

use core\logger\LogServiceInterface;
use Thruway\AbstractSession;
use Thruway\Common\Utils;
use Thruway\Message\ErrorMessage;
use Thruway\Message\Message;
use Thruway\Message\SubscribedMessage;
use Thruway\Message\SubscribeMessage;
use Thruway\Message\UnsubscribedMessage;
use Thruway\Message\UnsubscribeMessage;
use Thruway\Role\AbstractRole;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Subscriber extends AbstractRole implements SubscriberInterface
{
    private array $subscriptions;

    private array $unsubscriptions;

    /**
     * Constructor
     */
    public function __construct(private LogServiceInterface $logger)
    {
        $this->subscriptions   = [];
        $this->unsubscriptions = [];
    }

    public function handleError(ErrorMessage $errorMessage, ?AbstractSession $session)
    {
        $this->logger->dump(
            [
                'data'    => 'Error : ' . $errorMessage,
                'logfile' => WEBSOCKET_LOGFILE
            ]
        );

        if (null !== $session) {
            $session->sendMessage($errorMessage);
        }
    }


    public function onMessage(AbstractSession $session, Message $message)
    {
        if ($this->handlesMessage($message)) {
            $this->process($message, $session);
        }
    }

    public function process(SubscribedMessage|UnsubscribedMessage $message, ?AbstractSession $session)
    {
        if ($message instanceof SubscribedMessage) {
            $this->processSubscribe($message, $session);
        }

        if ($message instanceof UnsubscribedMessage) {
            $this->processUnsubscribe($message, $session);
        }
    }

    private function processSubscribe(SubscribedMessage $message, ?AbstractSession $session)
    {
        if (isset($this->subscriptions[$message->getRequestId()])) {
            $this->subscriptions[$message->getRequestId()]['subscription_id'] = $message->getSubscriptionId();

            if (!is_null($this->subscriptions[$message->getRequestId()]['callback'])) {
                call_user_func($this->subscriptions[$message->getRequestId()]['callback'], [$message, $session]);
            }

            $this->logger->dump(
                [
                    'data'    => 'User [' . $session->getSessionId() . '] has subscribed to topic [' . $this->subscriptions[$message->getRequestId()]['topic'] . ']',
                    'logfile' => WEBSOCKET_LOGFILE
                ]
            );
        }
    }

    private function processUnsubscribe(UnsubscribedMessage $message, ?AbstractSession $session)
    {
        if (isset($this->unsubscriptions[$message->getRequestId()]) && !is_null($this->unsubscriptions[$message->getRequestId()]['callback'])) {
            call_user_func($this->unsubscriptions[$message->getRequestId()]['callback'], [$message, $session]);
            unset($this->unsubscriptions[$message->getRequestId()]);
        }

        if (isset($this->subscriptions[$message->getRequestId()])) {
            $this->logger->dump(
                [
                    'data'    => 'User [' . $session->getSessionId() . '] has unsubscribed to topic [' . $this->subscriptions[$message->getRequestId()]['topic'] . ']',
                    'logfile' => WEBSOCKET_LOGFILE
                ]
            );

            unset($this->subscriptions[$message->getRequestId()]);
        }
    }

    public function subscribe(string $topic, ?array $options, AbstractSession $session, ?callable $callback): SubscribeMessage
    {
        $requestId = Utils::getUniqueId();
        $message   = new SubscribeMessage($requestId, $options ?? [], $topic);

        $this->subscriptions[$requestId] = [
            'topic'      => $message->getTopicName(),
            'callback'   => $callback,
            'session_id' => $session->getSessionId(),
            'request_id' => $requestId
        ];

        $session->sendMessage($message);

        return $message;
    }

    public function unsubscribe(string $topic, AbstractSession $session, ?callable $callback): ?UnsubscribeMessage
    {
        $subscription = $this->findSubscription($topic, $session);
        if (null === $subscription) {
            return null;
        }

        $message = new UnsubscribeMessage($subscription['request_id'], $subscription['subscription_id']);

        $this->unsubscriptions[$subscription['request_id']] = [
            'subscription_id' => $subscription['subscription_id'],
            'request_id'      => $subscription['request_id'],
            'callback'        => $callback,
            'session_id'      => $session->getSessionId()
        ];

        $session->sendMessage($message);

        return $message;
    }

    /**
     * Handle message
     */
    public function handlesMessage(Message $message): bool
    {
        $handledMsgCodes = [
            Message::MSG_SUBSCRIBED,
            Message::MSG_UNSUBSCRIBED
        ];

        return in_array($message->getMsgCode(), $handledMsgCodes, true);
    }

    private function findSubscription(string $topic, AbstractSession $session): ?array
    {
        foreach ($this->subscriptions as $subscription) {
            if ($subscription['topic'] === $topic && $subscription['session_id'] === $session->getSessionId()) {
                return $subscription;
            }
        }

        return null;
    }
}
