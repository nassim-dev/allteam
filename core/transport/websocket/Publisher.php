<?php

namespace core\transport\websocket;

use core\logger\LogServiceInterface;
use Thruway\AbstractSession;
use Thruway\Common\Utils;
use Thruway\Message\ErrorMessage;
use Thruway\Message\Message;
use Thruway\Message\PublishedMessage;
use Thruway\Message\PublishMessage;
use Thruway\Role\AbstractRole;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Publisher extends AbstractRole implements PublisherInterface
{
    private array $messages;

    /**
     * Constructor
     */
    public function __construct(private LogServiceInterface $logger)
    {
        $this->messages = [];
    }

    public function handleError(ErrorMessage $errorMessage, ?AbstractSession $session)
    {
        $this->logger->dump(
            [
                'data'    => 'Error : ' . $errorMessage,
                'logfile' => WEBSOCKET_LOGFILE
            ]
        );


        if (null !== $session) {
            $session->sendMessage($errorMessage);
        }
    }

    public function onMessage(AbstractSession $session, Message $message)
    {
        if ($this->handlesMessage($message)) {
            $this->process($message, $session);
        }
    }

    public function process(PublishedMessage $message, ?AbstractSession $session)
    {
        if (isset($this->messages[$message->getRequestId()]) && !is_null($this->messages[$message->getRequestId()]['callback'])) {
            $this->logger->dump(
                [
                    'data'    => 'Publish to topic [' . $this->messages[$message->getRequestId()]['topic'] . ']',
                    'logfile' => WEBSOCKET_LOGFILE
                ]
            );
            call_user_func($this->messages[$message->getRequestId()]['callback'], [$message, $session]);
            unset($this->messages[$message->getRequestId()]);
        }
    }


    public function publish(string $topic, ?array $options, ?array $arguments, ?array $argumentsKw, ?callable $callback): PublishMessage
    {
        $requestId = Utils::getUniqueId();
        $message   = new PublishMessage($requestId, $options, $topic, $arguments, $argumentsKw);

        $this->messages[$requestId] = [
            'publication_id' => $message->getPublicationId(),
            'callback'       => $callback,
            'topic'          => $message->getTopicName()
        ];

        return $message;
    }


    /**
     * Handle message
     */
    public function handlesMessage(Message $message): bool
    {
        $handledMsgCodes = [
            Message::MSG_PUBLISHED,
        ];

        return in_array($message->getMsgCode(), $handledMsgCodes, true);
    }
}
