<?php

namespace core\transport\websocket;

use core\DI\DiProvider;

/**
 * Trait WebsocketProvider
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait WebsocketProvider
{
    use DiProvider;

    /**
     * WebsocketServiceInterface implémentation
     *
     * @var WebsocketServiceInterface
     */
    private $websocket;

    /**
     * Return WebsocketServiceInterface implémetation
     */
    public function getWebsocket(): WebsocketServiceInterface
    {
        return $this->websocket ??= $this->getDi()->singleton(WebsocketServiceInterface::class);
    }
}
