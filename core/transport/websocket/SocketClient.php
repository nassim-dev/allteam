<?php

namespace core\transport\websocket;

use core\html\javascript\JavascriptServiceInterface;
use core\messages\queue\MessageQueueInterface;
use core\messages\request\HttpRequestInterface;
use core\secure\authentification\AuthServiceInterface;
use core\utils\Utils;
use Thruway\Common\Utils as UtilsThruway;
use Thruway\Message\PublishMessage;

/**
 * Class SocketClient
 *
 * Utilities to use websockets (generate chanels...)
 *
 * @category Utilities to use websockets (generate chanels...)
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class SocketClient implements WebsocketServiceInterface
{
    public const PREFIX = 'com.app.';

    /**
     * Constructor
     */
    public function __construct(
        private HttpRequestInterface $request,
        private AuthServiceInterface $auth,
        private MessageQueueInterface $queue
    ) {
    }

    public function send(array $message, array $parameters)
    {
        if (isset($parameters['iduser'])) {
            $this->sendToUser($message, $parameters['type'] ?? 'ERROR', $parameters['iduser']);
        }

        if (isset($parameters['idcontext'])) {
            $this->sendToContext($message, $parameters['type'] ?? 'ERROR', $parameters['idcontext']);
        }

        if (isset($parameters['page'])) {
            $this->sendToPage($message, $parameters['type'] ?? 'ERROR', $parameters['page']);
        }
    }

    public function receive(array $message, array $parameters)
    {
        //Not implemented
    }

    /**
     * Return Id decoded
     */
    public function getIdFromChannel(string $idcrypted): int
    {
        return (int) str_replace('USR', '', Utils::decrypt($idcrypted));
    }

    /**
     * Get context channel
     */
    public function getContextChannel(?int $idcontext = null): string
    {
        return self::PREFIX . strtolower(Utils::encrypt('CTX' . $idcontext ?? $this->auth->getContext()?->getIdcontext() ?? 'default'));
    }

    /**
     * Get page channel
     */
    public function getPageChannel(?string $page = null): string
    {
        return self::PREFIX . strtolower(Utils::encrypt('PGE' . ($this->auth->getContext()?->getIdcontext() ?? 'default') . $page ?? $this->request->getPage() ?? 'default'));
    }

    /**
     * Get User channel name
     *
     * @param int|null $user
     */
    public function getUserChannel(?int $iduser = null): string
    {
        return self::PREFIX . strtolower(Utils::encrypt('USR' . $iduser ?? $this->auth->getUser()?->getIduser() ?? 'default'));
    }

    /**
     * Send message to context channel
     */
    public function sendToContext(array $message, string $type, ?int $idcontext = null): bool
    {
        if (!null === $idcontext || !null === $this->auth->getContext()?->getIdcontext()) {
            $channel = $this->getContextChannel($idcontext ?? $this->auth->getContext()?->getIdcontext());
            $msg     = new PublishMessage(
                UtilsThruway::getUniqueId(),
                [],
                $channel,
                null,
                [
                    'type'    => strtoupper($type),
                    'content' => $message
                ]
            );

            $msg->acknowledge();

            $this->queue->push($msg, self::class);

            return true;
        }

        return false;
    }

    /**
     * Send message to page channel
     *
     * @return void
     */
    public function sendToPage(array $message, string $type, ?string $page = null)
    {
        $channel = $this->getPageChannel($page ?? $this->request->getPage());
        $msg     = new PublishMessage(
            UtilsThruway::getUniqueId(),
            [],
            $channel,
            null,
            [
                'type'    => strtoupper($type),
                'content' => $message
            ]
        );

        $msg->acknowledge();

        $this->queue->push($msg, self::class);
    }

    /**
     * Send message to user channel
     */
    public function sendToUser(array $message, string $type, ?int $iduser = null): bool
    {
        if (!null === $iduser || is_object($this->auth->getUser())) {
            $channel = $this->getUserChannel($iduser ?? $this->auth->getUser()?->getIduser());
            $msg     = new PublishMessage(
                UtilsThruway::getUniqueId(),
                [],
                $channel,
                null,
                [
                    'type'    => strtoupper($type),
                    'content' => $message
                ]
            );

            $msg->acknowledge();


            $this->queue->push($msg, self::class);

            return true;
        }

        return false;
    }


    /**
     * Update jsConfig with websocket
     */
    public function updateJavascriptConfig(JavascriptServiceInterface $jsConfig, ?array $config = null): bool
    {
        return $this->sendToUser((!null === $config) ? $config : $jsConfig->getConfig(true), 'JSCONFIG');
    }
}
