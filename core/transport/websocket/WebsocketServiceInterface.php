<?php

namespace core\transport\websocket;

use core\html\javascript\JavascriptServiceInterface;
use core\transport\TransportServiceInterface;

/**
 * Class WebsocketServiceInterface
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
interface WebsocketServiceInterface extends TransportServiceInterface
{
    /**
     * Return Id decoded
     */
    public function getIdFromChannel(string $idcrypted): int;

    /**
     * Get context channel
     */
    public function getContextChannel(?int $idcontext = null): string;

    /**
     * Get page channel
     */
    public function getPageChannel(?string $page = null): string;

    /**
     * Get User channel name
     *
     * @param null|int $user
     */
    public function getUserChannel(?int $iduser = null): string;


    /**
     * Send message to context channel
     */
    public function sendToContext(array $message, string $type, ?int $idcontext = null): bool;

    /**
     * Send message to user channel
     *
     * @return void
     */
    public function sendToPage(array $message, string $type, ?string $page = null);

    /**
     * Send message to user channel
     */
    public function sendToUser(array $message, string $type, ?int $iduser = null): bool;

    /**
     * Update jsConfig with websocket
     */
    public function updateJavascriptConfig(JavascriptServiceInterface $jsConfig, ?array $config = null): bool;
}
