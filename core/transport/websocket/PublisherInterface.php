<?php

namespace core\transport\websocket;

use Thruway\AbstractSession;
use Thruway\Message\ErrorMessage;
use Thruway\Message\Message;
use Thruway\Message\PublishedMessage;
use Thruway\Message\PublishMessage;

/**
 * Undocumented interface
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
interface PublisherInterface
{
    public function publish(string $topic, ?array $options, ?array $arguments, ?array $argumentsKw, ?callable $callback): PublishMessage;

    public function process(PublishedMessage $message, ?AbstractSession $session);

    public function handleError(ErrorMessage $errorMessage, ?AbstractSession $session);

    public function handlesMessage(Message $message): bool;
}
