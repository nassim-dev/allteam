<?php

namespace core\transport\websocket;

use core\logger\LogServiceInterface;
use Exception;
use Ratchet\ConnectionInterface;
use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;
use React\EventLoop\LoopInterface;
use React\Socket\SocketServer;
use Thruway\AbstractSession;
use Thruway\Common\Utils;
use Thruway\Event\ConnectionCloseEvent;
use Thruway\Event\ConnectionOpenEvent;
use Thruway\Event\RouterStartEvent;
use Thruway\Event\RouterStopEvent;
use Thruway\Message\EventMessage;
use Thruway\Message\HelloMessage;
use Thruway\Message\PublishedMessage;
use Thruway\Message\PublishMessage;
use Thruway\Message\SubscribedMessage;
use Thruway\Message\SubscribeMessage;
use Thruway\Message\UnsubscribedMessage;
use Thruway\Message\UnsubscribeMessage;
use Thruway\Message\WelcomeMessage;
use Thruway\Serializer\JsonSerializer;
use Thruway\Session;
use Thruway\Transport\AbstractRouterTransportProvider;
use Thruway\Transport\RatchetTransport;

/**
 * Class Broker
 *
 * Represent the main websocket server
 *
 * @category Represent the main websocket server
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class Broker extends AbstractRouterTransportProvider implements BrokerInterface
{
    private ?\Ratchet\Server\IoServer $server = null;

    private \SplObjectStorage $sessions;


    private array $connections = [];

    private \Ratchet\WebSocket\WsServer $ws;

    private $subscriptions;

    public function __construct(
        private LogServiceInterface $logger,
        private WebsocketServiceInterface $socketClient,
        private $address = '127.0.0.1',
        private $port = 8080
    ) {
        $this->sessions      = new \SplObjectStorage();
        $this->ws            = new WsServer($this);
        $this->subscriptions = [];
    }


    public function getSubProtocols()
    {
        return ['ocpp1.6', 'wamp.2.json', 'wamp.2.msgpack'];
    }

    public function onOpen(ConnectionInterface $conn)
    {
        $this->logger->dump(
            [
                'data'    => 'Open connection ' . $conn->resourceId,
                'logfile' => WEBSOCKET_LOGFILE
            ]
        );

        $transport = new RatchetTransport($conn, $this->loop);
        $transport->setSerializer(new JsonSerializer());
        $transport->setTrusted($this->trusted);

        $session = $this->router->createNewSession($transport);
        $this->sessions->attach($conn, $session);
        $this->connections[$session->getSessionId()] = $conn;

        $this->router->getEventDispatcher()->dispatch('connection_open', new ConnectionOpenEvent($session));
    }

    public function onClose(ConnectionInterface $conn)
    {
        $this->logger->dump(
            [
                'data'    => 'Connection ' . $conn->resourceId . ' is closed',
                'logfile' => WEBSOCKET_LOGFILE
            ]
        );

        /** @var Session $session */
        $session = $this->sessions[$conn];
        if (isset($this->connections[$session->getSessionId()])) {
            unset($this->connections[$session->getSessionId()]);
        }

        $this->sessions->detach($conn);

        $this->router->getEventDispatcher()->dispatch('connection_close', new ConnectionCloseEvent($session));
    }

    /** @inheritdoc */
    public function onMessage(ConnectionInterface $from, $json)
    {
        /** @var Session $session */
        $session = $this->sessions[$from];

        try {
            $message = $session->getTransport()->getSerializer()->deserialize($json);
            $this->logger->dump(
                [
                    'data'    => 'Event occur : ' . $message::class . ' from ' . $session->getSessionId(),
                    'logfile' => WEBSOCKET_LOGFILE
                ]
            );
            // $message = match (get_class($message)) {
            //     HelloMessage::class       => $this->handleHello($message, $session),
            //     SubscribeMessage::class   => $this->handleSubscribe($message, $session),
            //     UnsubscribeMessage::class => $this->handleUnsubscribe($message, $session),
            //     PublishMessage::class     => $this->handlePublish($message, $session),
            // };
            $this->logger->dump(
                [
                    'data'    => 'Event sended : ' . $message::class . ' to ' . $session->getSessionId(),
                    'logfile' => WEBSOCKET_LOGFILE
                ]
            );

            $session->dispatchMessage($message);
        } catch (SocketException $e) {
            $this->logger->dump(
                [
                    'data'    => 'Exception occurred during onMessage: ' . $e->getMessage(),
                    'logfile' => WEBSOCKET_LOGFILE
                ]
            );
        }
    }


    public function handleSubscribe(SubscribeMessage $message, ?AbstractSession $session): SubscribedMessage
    {
        $subscriptionId = Utils::getUniqueId();
        $response       = new SubscribedMessage($message->getRequestId(), $subscriptionId);

        $this->subscriptions[$message->getRequestId()] = [
            'topic'           => $message->getTopicName(),
            'session_id'      => $session->getSessionId(),
            'request_id'      => $message->getRequestId(),
            'subscription_id' => $subscriptionId
        ];


        $this->logger->dump(
            [
                'data'    => 'New subscription to topic : ' . $message->getTopicName(),
                'logfile' => WEBSOCKET_LOGFILE
            ]
        );

        return $response;
    }

    public function handlePublish(PublishMessage $message, ?AbstractSession $session): PublishedMessage
    {
        $publicationId = Utils::getUniqueId();
        $response      = new PublishedMessage($message->getRequestId(), $publicationId);


        foreach ($this->subscriptions as $subscription) {
            if ($subscription['session_id'] !== $session->getSessionId() && $subscription['topic'] === $message->getTopicName()) {
                /** @var ConnectionInterface $subscriberConnection */
                $subscriberConnection = $this->connections[$subscription['session_id']] ?? null;
                if ($subscriberConnection === null) {
                    continue;
                }

                $subscriberSession = $this->sessions[$subscriberConnection];
                if ($subscriberSession instanceof Session) {
                    $event = EventMessage::createFromPublishMessage($message, $subscription['subscription_id']);
                    $subscriberSession->dispatchMessage($event);
                    $this->logger->dump(
                        [
                            'data'    => 'Send message to : ' . $subscriberSession->getSessionId(),
                            'logfile' => WEBSOCKET_LOGFILE
                        ]
                    );
                }
            }
        }

        return $response;
    }

    public function handleUnsubscribe(UnsubscribeMessage $message, ?AbstractSession $session): UnsubscribedMessage
    {
        $response = new UnsubscribedMessage($message->getRequestId());

        if (isset($this->subscriptions[$message->getRequestId()])) {
            $this->logger->dump(
                [
                    'data'    => 'Unsubscribe to : ' . $this->subscriptions[$message->getRequestId()]['topic'],
                    'logfile' => WEBSOCKET_LOGFILE
                ]
            );

            unset($this->subscriptions[$message->getRequestId()]);
        }

        return $response;
    }


    public function handleHello(HelloMessage $message, ?AbstractSession $session): WelcomeMessage
    {
        /** @var HelloMessage $message */
        $details            = $message->getDetails();
        $details->transport = (object) $session->getTransport()->getTransportDetails();
        $message->setDetails($details);

        $session->dispatchMessage($message);

        $response = new WelcomeMessage($session->getSessionId(), $details);

        return $response;
    }


    public function onError(ConnectionInterface $conn, Exception $error)
    {
        $this->logger->dump(
            [
                'data'    => 'Error : ' . $error->getMessage(),
                'logfile' => WEBSOCKET_LOGFILE
            ]
        );
    }


    public function handleRouterStart(RouterStartEvent $event)
    {
        $socket = new SocketServer($this->address . ':' . $this->port, [], $this->loop);

        $this->logger->dump(
            [
                'data'    => 'Socket Server start listening on ' . $this->address . ':' . $this->port,
                'logfile' => WEBSOCKET_LOGFILE
            ]
        );

        $this->server = new IoServer(new HttpServer($this->ws), $socket, $this->loop);
    }

    public function enableKeepAlive(LoopInterface $loop, $interval = 30)
    {
        $this->ws->enableKeepAlive($loop, $interval);
    }

    public function handleRouterStop(RouterStopEvent $event)
    {
        if ($this->server) {
            $this->server->socket->close();
        }

        foreach ($this->sessions as $k) {
            $this->sessions[$k]->shutdown();
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            'router.start' => ['handleRouterStart', 10],
            'router.stop'  => ['handleRouterStop', 10]
        ];
    }
}
