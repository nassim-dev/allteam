<?php

namespace core\messages\queue;

use core\cache\RedisCache;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class RedisQueue implements MessageQueueInterface
{
    public function __construct(private RedisCache $driver)
    {
    }

    /**
     * Fetch next result
     *
     * @return mixed|null
     */
    public function pull(array $targets)
    {
        $result = $this->driver->getDriver()->blpop($targets, 10);

        return (!is_null($result)) ? unserialize($result[1]) : null;
    }

    public function push(mixed $message, string $target)
    {
        $this->driver->getDriver()->rpush($target, serialize($message));
    }

    public function readThen(array $targets, callable | array $listener)
    {
        $result = $this->pull($targets);
        if (is_array($listener)) {
            call_user_func_array($listener, [$result]);
        } else {
            call_user_func($listener, [$result]);
        }
    }

    public function getLoopCondition(): bool
    {
        return $this->driver->getDriver()->isConnected();
    }

    /**
     * Get stream ressource
     *
     * @return ressource
     */
    public function getStream($type = null): \Predis\Client
    {
        return $this->driver->getDriver();
    }
}
