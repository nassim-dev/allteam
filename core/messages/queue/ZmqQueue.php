<?php

namespace core\messages\queue;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class ZmqQueue implements MessageQueueInterface
{
    public function __construct(protected \ZMQContext $driver, protected ?string $endpoint)
    {
    }

    private function getSocket(int $type)
    {
        try {
            $socket = $this->driver->getSocket($type);
            $socket->connect($this->endpoint);

            return $socket;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Fetch next result
     *
     * @return mixed|null
     */
    public function pull(array $targets)
    {
        $socket = $this->getSocket(\ZMQ::SOCKET_PULL);
        $result = $socket->recv();

        return (!is_null($result)) ? unserialize($result) : null;
    }

    public function push(mixed $message, string $target)
    {
        $socket = $this->getSocket(\ZMQ::PUSH);
        $socket->send(serialize($message));
    }

    /**
     * Register reader listener
     *
     * @return void
     */
    public function readThen(array $targets, callable | array $listener, array $parameters = [])
    {
        $result = $this->pull($targets);
        if (is_array($listener)) {
            call_user_func_array($listener, [$result]);
        } else {
            call_user_func($listener, [$result]);
        }
    }

    public function getLoopCondition(): bool
    {
        return ($this->getSocket(\ZMQ::SOCKET_PULL) !== null);
    }

    /**
     * Get stream ressource
     *
     * @return ressource
     */
    public function getStream($type = null)
    {
        return $this->getSocket($type ?? \ZMQ::SOCKET_PULL);
    }
}
