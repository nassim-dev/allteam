<?php

namespace core\messages\queue;

use core\cache\FileCache;
use core\services\serializer\SerializerServiceInterface;
use core\utils\GeneratorProvider;
use Nette\Utils\Finder;
use SplFileInfo;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class FilesystemQueue implements MessageQueueInterface
{
    use GeneratorProvider;

    public const PATH = BASE_DIR . 'tmp/cache';

    public function __construct(
        private FileCache $driver,
        private SerializerServiceInterface $serializer,
    ) {
    }

    /**
     * Fetch next result
     *
     * @return mixed|null
     */
    public function pull(array $targets)
    {
        $files = Finder::find('*')->in(self::PATH);

        /** @var SplFileInfo $file */
        foreach ($files as $file) {
            $filename = self::PATH . '/' . $file->getBasename();
            if (!str_contains('TASK', $file->getBasename())) {
                continue;
            }
            $description = require $filename;
            unlink($filename);

            return $this->serializer->rebuild($description);
        }

        return null;
    }

    public function push(mixed $message, string $target)
    {
        $this->driver->set('TASK.' . $target, $message);
    }

    public function readThen(array $targets, callable | array $listener)
    {
        $result = $this->pull($targets);
        if (is_array($listener)) {
            call_user_func_array($listener, [$result]);
        } else {
            call_user_func($listener, [$result]);
        }
    }

    public function getLoopCondition(): bool
    {
        return true;
    }

    /**
     * Get stream ressource
     *
     * @return ressource
     */
    public function getStream($type = null)
    {
        return null;
    }
}
