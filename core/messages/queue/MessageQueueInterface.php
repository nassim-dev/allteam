<?php

namespace core\messages\queue;

/**
 * Undocumented interface
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
interface MessageQueueInterface
{
    /**
     * Push message to queue
     *
     * @return void
     */
    public function push(mixed $message, string $target);

    /**
     * Pull next mesage to queue
     *
     * @return void
     */
    public function pull(array $target);

    /**
     * Get stream ressource
     *
     * @param mixed|null $type
     *
     * @return ressource
     */
    public function getStream($type = null);

    /**
     * Register reader listener
     *
     * @return void
     */
    public function readThen(array $targets, callable | array $listener);

    /**
     * Condition for execute loop
     */
    public function getLoopCondition(): bool;
}
