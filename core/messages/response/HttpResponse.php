<?php

namespace core\messages\response;

/**
 * Class HttpResponse
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class HttpResponse extends ReponseAbstract
{
    public function __construct()
    {
        $this->setContentType('text/html', 'utf-8');
    }
}
