<?php

namespace core\messages\response;

use core\services\storage\FileItemInterface;

/**
 * Interface HttpResponseInterface
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
interface HttpResponseInterface
{
    /**
     * Add Http Header
     *
     * @return static
     */
    public function addHeader(string $name, string $value);

    /**
     * Return Data
     *
     * @param boolean $json
     */
    public function getData(bool $encryptedDatas = false): array;

    /**
     * Send response as json
     *
     * @return string
     */
    public function json(): string;

    /**
     * Send response as html
     *
     * @return string|null
     */
    public function html(): ?string;

    /**
     * Set Http Code
     *
     * @return static
     */
    public function setCode(?int $code = null);

    /**
     * Set Header Content type
     *
     * @return static
     */
    public function setContentType(string $type, ?string $charset = null);

    /**
     * Add data to object
     *
     * @param mixed $data
     *
     * @return static
     */
    public function setData(string $key, $data, bool $updateExisting = false);


    /**
     * Get headers to send
     *
     * @PhpUnitGen\get("headers")
     *
     * @return array
     */
    public function getHeaders(): array;

    /**
     * Get content Type
     *
     * @PhpUnitGen\get("content_type")
     *
     * @return string
     */
    public function getContent_type(): string;

    /**
     * Get page Encoding
     *
     * @PhpUnitGen\get("encoding")
     *
     * @return string
     */
    public function getEncoding(): string;

    /**
     * Get hTTP code
     *
     * @PhpUnitGen\get("code")
     *
     * @return int
     */
    public function getCode(): int;

    /**
     * Get the value of file
     *
     * @PhpUnitGen\get("file")
     *
     * @return null|FileItemInterface
     */
    public function getFile(): ?FileItemInterface;

    /**
     * Define a file to return
     *
     * @param string|FileItemInterface $file
     *
     * @return void
     */
    public function setFile(string|FileItemInterface $file);

    /**
     * Send the file defined in response
     *
     * @return void
     */
    public function sendFile();
}
