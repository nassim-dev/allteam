<?php

namespace core\messages\response;

use core\DI\DiProvider;
use core\messages\HttpUtils;
use core\services\storage\FileItemInterface;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class ReponseAbstract implements HttpResponseInterface
{
    use DiProvider;

    /**
     * Headers to send
     *
     * @var array
     */
    protected array $headers = [];

    /**
     * Page charset
     *
     * @var string
     */
    protected ?string $charset = null;

    /**
     * Page Encoding
     *
     * @var string
     */
    protected ?string $encoding = null;

    /**
     * HTTP code
     *
     * @var integer
     */
    protected int $code = 200;

    /**
     * Content Type
     *
     * @var string
     */
    protected ?string $content_type = null;

    /**
     * Decode return Html
     *
     * @var boolean
     */
    protected bool $htmlDecode = true;

    /**
     * File to return
     *
     * @var FileItemInterface|null
     */
    protected ?FileItemInterface $file = null;

    /**
     * Returned data
     * error => True to Keep modal open
     * html => Append to selecticker list
     * data => New data or Updated data in JSON
     * replaceHtml => Replace some element in HTML
     *
     * @var array
     */
    protected $data = [
        'error'       => false,
        'html'        => '',
        'data'        => false,
        'replaceHtml' => false
    ];


    public function __construct()
    {
        $this->encoding = 'gzip, deflate';
        $this->addHeader('X-Powered-By', 'Various language');
        $this->addHeader('Server', 'Various webserver');
    }

    public function __destruct()
    {
        foreach ($this->headers as $header) {
            header($header);
        }
        header(HttpUtils::HTTP_ERROR($this->code));
    }

    /**
     *  Add Http Header
     *
     * @return static
     */
    public function addHeader(string $name, string $value)
    {
        $this->headers[$name] = $name . ': ' . $value;

        return $this;
    }

    /**
     * Return Data
     */
    public function getData(bool $encryptedDatas = false): array
    {
        return $this->data;
    }

    public function json(): string
    {
        return json_encode([...$this->data, 'status' => HttpUtils::HTTP_ERROR($this->code)], JSON_THROW_ON_ERROR);
    }

    public function html(): ?string
    {
        return $this->data['html'] ?? null;
    }

    public function setFile(string|FileItemInterface $file)
    {
        $this->file = (is_string($file)) ? $this->getDi()->make(FileItemInterface::class, ['path' => $file]) : $file;

        return $this;
    }

    public function sendFile()
    {
        if ($this->file === null) {
            throw new ResponseException('No file was defined, please use setFile() before');
        }

        switch (strtolower($this->file->getMimeType())) {
            case 'odt':
                header('Content-Type: application/vnd.oasis.opendocument.text');

                break;
            case 'odp':
                header('Content-Type: application/vnd.oasis.opendocument.presentation');

                break;
            case 'ods':
                header('Content-Type: application/vnd.oasis.opendocument.spreadsheet');

                break;
            case 'pdf':
                header('Content-Type: application/pdf');

                break;
            case 'png':
                header('Content-Type: image/png');

                break;
            case 'jpg':
                header('Content-Type: image/jpeg');

                break;
            case 'jpeg':
                header('Content-Type: image/jpeg');

                break;
            case 'doc':
                header('Content-Type: application/msword');

                break;
            case 'docx':
                header('Content-Type: application/msword');

                break;
            case 'xls':
                header('Content-Type: application/vnd.ms-excel');

                break;
            case 'xlsx':
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

                break;
            case 'ppt':
                header('Content-Type: application/vnd.ms-powerpoint');

                break;
            case 'pptx':
                header('application/vnd.openxmlformats-officedocument.presentationml.presentation');

                break;
            default:
                header('application/vnd.api+json');
        }

        header('Content-Description: File Transfer');
        header('Content-Disposition: attachment; filename="' . $this->file->getTitle() . '"');
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . $this->file->getSize());

        readfile($this->file->getRealPath());
    }

    /**
     * Set Http Code
     *
     * @return static
     */
    public function setCode(?int $code = null)
    {
        $this->code ??= $code;

        return $this;
    }

    /**
     * Set Header Content type
     *
     * @return static
     */
    public function setContentType(string $type, ?string $charset = 'utf-8')
    {
        $this->content_type = $type;
        $this->charset      = $charset;
        $this->addHeader('Content-Type', $type . ($charset ? '; charset=' . $charset : ''));

        return $this;
    }

    /**
     * Add data to object
     *
     * @return static
     */
    public function setData(string $key, mixed $data, bool $updateExisting = false)
    {
        if (!$updateExisting) {
            $this->data[$key] = $data;

            return $this;
        }

        if (array_key_exists($key, $this->data)) {
            $this->data[$key][$updateExisting] = $data;

            return $this;
        }

        $this->data[$key] = $data;

        return $this;
    }

    /**
     * Get headers to send
     *
     * @PhpUnitGen\get("headers")
     *
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * Get content Type
     *
     * @PhpUnitGen\get("content_type")
     *
     * @return string
     */
    public function getContent_type(): string
    {
        return $this->content_type;
    }

    /**
     * Get page Encoding
     *
     * @PhpUnitGen\get("encoding")
     *
     * @return string
     */
    public function getEncoding(): string
    {
        return $this->encoding;
    }

    /**
     * Get hTTP code
     *
     * @PhpUnitGen\get("code")
     *
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * Get the value of file
     *
     * @PhpUnitGen\get("file")
     *
     * @return null|FileItemInterface
     */
    public function getFile(): ?FileItemInterface
    {
        return $this->file;
    }
}
