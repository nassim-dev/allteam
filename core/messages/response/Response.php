<?php namespace core\messages\response;

/**
 * Class Response
 *
 *  @method static string addHeader(string $name, string $value)
 *  @method static array getData(bool $encryptedDatas)
 *  @method static string json()
 *  @method static string html()
 *  @method static string setCode(int $code)
 *  @method static string setContentType(string $type, string $charset)
 *  @method static string setData(string $key, string $data, bool $updateExisting)
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class Response extends \core\facade\HelperFacade
{
    protected static ?string $serviceInterface = 'core\messages\response\HttpResponseInterface';
}
