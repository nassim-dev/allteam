<?php

namespace core\messages\response;

use core\DI\DiProvider;

/**
 * Trait ResponseProvider
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait ResponseProvider
{
    use DiProvider;

    /**
     * HttpResponseInterface implémentation
     *
     * @var HttpResponseInterface
     */
    private $response;

    /**
     * Return HttpResponseInterface implémetation
     */
    public function getResponse(): HttpResponseInterface
    {
        return $this->response ??= $this->getDi()->singleton(HttpResponseInterface::class);
    }
}
