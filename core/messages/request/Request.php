<?php namespace core\messages\request;

/**
 * Class Request
 *
 *  @method static bool isExtension()
 *  @method static bool isAjaxify()
 *  @method static string getPage()
 *  @method static string addContent(string $key, mixed $value)
 *  @method static array getArgc()
 *  @method static array getArgv()
 *  @method static string getContents(string $key)
 *  @method static string getCookie(string $key)
 *  @method static string getFiles(string $key)
 *  @method static string getGet(string $key)
 *  @method static string getHost()
 *  @method static string getMethod()
 *  @method static string getPost(string $key)
 *  @method static string getServer(string $key)
 *  @method static bool isSecure()
 *  @method static string mergeContents(array $params)
 *  @method static string setContents(array $contents)
 *  @method static string setParameter(string $key, mixed $val)
 *  @method static self setParameters(array $parameters)
 *  @method static string getParameters(string $key)
 *  @method static array getQuery()
 *  @method static string getUrl()
 *  @method static self populate(Swoole\Http2\Request|Swoole\Http\Request|core\messages\request\HttpRequestInterface $request)
 *  @method static array getHeaders()
 *  @category  Description
 *  @version   Release: 0.2
 *  @author    Nassim Ourami <nassim.ourami@mailo.com>
 *  @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 *  @link    https://allteam.io
 *  @since   File available since Release 0.2
 *  @package Allteam
 */
class Request extends \core\facade\HelperFacade
{
    protected static ?string $serviceInterface = 'core\messages\request\HttpRequestInterface';
}
