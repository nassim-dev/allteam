<?php

namespace core\messages\request;

use core\config\Conf;
use core\logger\request\TracyRequestPanel;
use core\secure\sanitizers\SanitizerServiceInterface;
use core\secure\sanitizers\SchemeSanitizer;
use core\secure\sanitizers\TrimSanitizer;
use core\utils\Utils;
use Tracy\Debugger;

/**
 * Class HttpRequest
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
class HttpRequest extends RequestAbstract
{
    /**
     * True if page is load by swup
     *
     * @var bool|null
     */
    private $ajaxify = null;

    /**
     * @return mixed
     */
    public function __construct(
        protected SanitizerServiceInterface $sanitizer,
        SchemeSanitizer $schemeSanitizer,
        TrimSanitizer $trimSanitizer,
        ?string $url = null
    ) {
        parent::__construct($sanitizer, $schemeSanitizer, $trimSanitizer);

        $requestUri = (null == $this->getServer('REQUEST_URI')) ? '' : str_replace('public/', '', $this->getServer('REQUEST_URI') ?? '');

        $this->query  = (null === $url) ? explode('/', trim($requestUri, '/')) : explode('/', $url);
        $this->url    = $url ?? $requestUri;
        $this->host   = $this->getServer('HTTP_HOST');
        $this->method = $this->getServer('REQUEST_METHOD');

        if (!isset($this->headers['REQUEST_URI'])) {
            $this->headers['REQUEST_URI'] = $requestUri;
        }

        $this->initContents();

        if (Conf::find('ENVIRONNEMENT') === 'development' && CONTEXT === 'APP' && $this->getServer('x-requested-with')) {
            Debugger::getBar()->addPanel(new TracyRequestPanel($this));
        }
    }


    /**
     * Get Page from URL
     */
    public function getPage(): ?string
    {
        $key = ($this->isExtension()) ? 2 : 0;

        return (isset($this->query[$key]) && !empty($this->query[$key])) ? $this->query[$key] : null;
    }

    /**
     * Return true if is an extension request
     */
    public function isExtension(): bool
    {
        return ($this->query[0] === 'e');
    }

    /**
     * Get Headers and check if is ajax
     */
    public function isAjaxify(): bool
    {
        if (null === $this->ajaxify) {
            $this->ajaxify = (isset($this->headers['x-requested-with']) && 'Highway' === $this->headers['x-requested-with']);

            //Prevent Highway cache, is that Working ?
            $lastModifiedTime = filemtime(__FILE__);
            $requestUri       = $this->getUrl();
            $etag             = (!$this->ajaxify) ? 'W/"' . Utils::hash($requestUri . $lastModifiedTime) . '"' : 'W/"' . Utils::hash($requestUri . $lastModifiedTime . 'AJAX') . '"';

            header("Etag: $etag");
        }

        return $this->ajaxify;
    }
}
