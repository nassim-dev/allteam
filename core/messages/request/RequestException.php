<?php

namespace core\messages\request;

use Exception;

/**
 * Class RequestException
 *
 * Provide Request exception
 *
 * @category Provide Request exception
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class RequestException extends Exception
{
}
