<?php

namespace core\messages\request;

use core\DI\DiProvider;

/**
 * Trait RequestProvider
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait RequestProvider
{
    use DiProvider;

    /**
     * HttpRequestInterface implémentation
     *
     * @var HttpRequestInterface
     */
    private $request;

    /**
     * Return HttpRequestInterface implémetation
     */
    public function getRequest(): HttpRequestInterface
    {
        return $this->request ??= $this->getDi()->singleton(HttpRequestInterface::class);
    }
}
