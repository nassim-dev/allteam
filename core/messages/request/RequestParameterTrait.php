<?php

namespace core\messages\request;

/**
 * Trait RequestParameterTrait
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
trait RequestParameterTrait
{
    /**
     * Fetch parameter from requestService
     *
     * @return bool|string|int|array|null
     */
    public function getParameter(string $parameter)
    {
        return $this->request->getParameters($parameter) ?? $this->request->getContents($parameter) ?? null;
    }
}
