<?php

namespace core\messages\request;

use core\config\Conf;
use core\logger\request\TracyRequestPanel;
use core\secure\sanitizers\SanitizerServiceInterface;
use core\secure\sanitizers\SchemeSanitizer;
use core\secure\sanitizers\TrimSanitizer;
use Tracy\Debugger;

/**
 * HttpRequestApi
 *
 * Request for api context
 *
 * @category Request for api context
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class HttpRequestApi extends RequestAbstract
{
    public function __construct(
        protected SanitizerServiceInterface $sanitizer,
        SchemeSanitizer $schemeSanitizer,
        TrimSanitizer $trimSanitizer,
        ?string $url = null,
        ?array $existingData = null
    ) {
        parent::__construct($sanitizer, $schemeSanitizer, $trimSanitizer);

        (null !== $existingData && null !== $url) ? $this->constructFromExistingDatas($existingData, $url) : $this->defaultConstructor($url);

        if (Conf::find('ENVIRONNEMENT') === 'development' && CONTEXT === 'APP' && $this->getServer('x-requested-with')) {
            Debugger::getBar()->addPanel(new TracyRequestPanel($this));
        }
    }

    /**
     * Get Called Ressources Mysql
     */
    public function getCalledTable(): string
    {
        return strtolower($this->query[2]);
    }

    /**
     * Return actual page
     */
    public function getPage(): ?string
    {
        $key = ($this->isExtension()) ? 4 : 2;

        return (isset($this->query[$key]) && !empty($this->query[$key])) ? $this->query[$key] : null;
    }

    /**
     * Return true if is an extension request
     */
    public function isExtension(): bool
    {
        return  ($this->query[2] === 'e');
    }


    /**
     * Get Real Ressources Mysql
     */
    public function getTable(array $exceptions = []): string
    {
        return (array_key_exists($this->query[2], $exceptions)) ? ucfirst($exceptions[$this->query[2]]) : ucfirst($this->query[2]);
    }

    /**
     * Get Headers and check if is ajax
     */
    public function isAjaxify(): bool
    {
        return true;
    }

    /**
     * Construct with provided datas
     *
     * @return void
     */
    private function constructFromExistingDatas(array $datas, string $url)
    {
        $this->url     = $url;
        $this->query   = explode('/', trim($this->url));
        $this->host    = Conf::find('PHP.SERVER_NAME') ?? null;
        $this->method  = $datas['method'] ?? 'GET';
        $this->headers = getallheaders() ?? [];

        if (!isset($this->headers['HTTP_REFERER'])) {
            $this->headers['HTTP_REFERER'] = $this->getServer('HTTP_REFERER') ?? $url;
        }

        $this->setContents($datas['datas'] ?? []);
    }

    /**
     * Default constructor
     *
     * @return void
     */
    private function defaultConstructor(?string $url)
    {
        $requestUri    = str_replace('public/', '', $this->getServer('REQUEST_URI') ?? '');
        $urlElements   = explode('?', $requestUri);
        $this->url     = $url ?? $urlElements[0];
        $this->query   = explode('/', $this->url);
        $this->host    = $this->getServer('HTTP_HOST');
        $this->method  = $this->getServer('REQUEST_METHOD');
        $this->headers = getallheaders() ?? [];

        if (!isset($this->headers['HTTP_REFERER'])) {
            $this->headers['HTTP_REFERER'] = $this->getServer('HTTP_REFERER') ?? $requestUri;
        }

        $this->initContents();
    }
}
