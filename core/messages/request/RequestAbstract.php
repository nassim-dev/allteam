<?php

namespace core\messages\request;

use core\config\Conf;
use core\secure\sanitizers\SanitizerServiceInterface;
use core\secure\sanitizers\SchemeSanitizer;
use core\secure\sanitizers\TrimSanitizer;
use Swoole\Http2\Request as RequestH2;
use Swoole\Http\Request;

/**
 * Undocumented class
 *
 * Description
 *
 * @category  Description
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC 4.0 https://creativecommons.org/licenses/by-nc/4.0/
 * @version   Release: 0.1
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
abstract class RequestAbstract implements HttpRequestInterface
{
    /**
     * HTTP HOST
     *
     * @var string
     */
    protected $host = null;

    /**
     * Method
     *
     * @var string
     */
    protected $method = null;

    /**
     * HTTP parameters
     *
     * @var array
     */
    protected $parameters = [];

    /**
     * GET query
     *
     * @var array
     */
    protected $query = null;

    /**
     * Url called
     *
     * @var string
     */
    protected $url = null;

    /**
     * php: //input
     *
     * @var array
     */
    protected $contents = [];

    /**
     * Headers sent
     *
     * @var array
     */
    protected $headers = [];

    private ?array $post = null;

    private ?array $get = null;

    private ?array $server = null;

    private ?array $files = null;

    private ?array $cookie = null;

    public function __construct(
        protected SanitizerServiceInterface $sanitizer,
        SchemeSanitizer $schemeSanitizer,
        TrimSanitizer $trimSanitizer
    ) {
        $this->sanitizer->register($schemeSanitizer)
            ->register($trimSanitizer);

        $this->headers = getallheaders() ?? [];
        $this->files   = (!isset($_FILES)) ? [] : null;
        $this->post    = (!isset($_POST)) ? [] : null;
        $this->get     = (!isset($_GET)) ? [] : null;
        $this->cookie  = (!isset($_COOKIE)) ? [] : null;
        $this->server  = (!isset($_SERVER)) ? [] : null;
    }

    /**
     * Add content to request
     *
     * @return self
     */
    public function addContent(string $key, mixed $value)
    {
        $this->contents[$key] = $value;

        return $this;
    }

    /**
     * Return argc
     *
     * @return array|null
     */
    public function getArgc(): ?array
    {
        return $this->getServer('argc');
    }

    /**
     * Return argv
     *
     * @return array|null
     */
    public function getArgv(): ?array
    {
        return $this->getServer('argv');
    }

    /**
     * Get php://input
     *
     * @return array|mixed
     */
    public function getContents(?string $key = null)
    {
        $this->initContents();

        if (null === $key) {
            return $this->contents;
        }

        if (array_key_exists($key, $this->contents)) {
            return $this->contents[$key];
        }

        return null;
    }

    /**
     * Return $_COOKIE
     *
     * @return array|null|mixed
     */
    public function getCookie(?string $key = null)
    {
        if ($this->cookie === null) {
            $this->cookie = $this->sanitizer->sanitize($_COOKIE);
        }

        if (null === $key) {
            return $this->cookie;
        }

        return $this->cookie[$key] ?? null;
    }

    /**
     * Return $_FILES
     *
     * @return array|null|mixed
     */
    public function getFiles(?string $key = null)
    {
        if ($this->files === null) {
            $options = [
                'name'      => 'string',
                'type'      => 'string',
                'size'      => 'int',
                'tmp_name'  => 'file',
                'error'     => 'int',
                'full_path' => 'file'
            ];
            foreach ($_FILES as $element) {
                $this->files[] = $this->sanitizer->sanitize($element, $options);
            }
        }

        if (null === $key) {
            return $this->files;
        }

        return $this->files[$key] ?? null;
    }

    /**
     * Return $_GET
     *
     * @return array|null|mixed
     */
    public function getGet(?string $key = null)
    {
        if ($this->get === null) {
            $this->get = $this->sanitizer->sanitize($_GET);
        }

        if (null === $key) {
            return $this->get;
        }

        return $this->get[$key] ?? null;
    }

    /**
     * Get HTTP_HOST
     */
    public function getHost(): string
    {
        return $this->host ?? '';
    }

    /**
     * Get HTTP Method GET PUT PATCH DELETE...
     */
    public function getMethod(): string
    {
        return $this->method ?? '';
    }

    /**
     * Return $_POST
     *
     * @return array|null|mixed
     */
    public function getPost(?string $key = null)
    {
        if ($this->post === null) {
            $this->post = $this->sanitizer->sanitize($_POST);
        }

        if (null === $key) {
            return $this->post;
        }

        return $this->post[$key] ?? null;
    }

    /**
     * Return $_SERVER
     *
     * @return array|null|mixed
     */
    public function getServer(?string $key = null)
    {
        if ($this->server === null) {
            $this->server = $this->sanitizer->sanitize($_SERVER);
        }

        if (null === $key) {
            return $this->server;
        }

        return $this->server[$key] ?? null;
    }

    /**
     * Return true if HTTPS
     */
    public function isSecure(): bool
    {
        return (Conf::find('PHP.HTTP_PROTOCOL') === 'https' && ($this->getServer('HTTPS') ?? false));
    }

    /**
     * Add multiple content as array to request
     *
     * @return void
     */
    public function mergeContents(array $contents)
    {
        $this->setContents(array_merge($this->getContents(), $contents));
    }

    protected function initContents()
    {
        if (empty($this->contents)) {
            $data    = [];
            $datatmp = trim(file_get_contents('php://input'), '"');
            parse_str($datatmp, $data);
            $this->contents = $this->sanitizer->sanitize($data);
        }

        return (is_array($this->contents)) ? $this->contents : [];
    }

    /**
     * Format Inputs from php://input
     *
     * @return static
     */
    public function setContents(array $contents)
    {
        $this->contents = $this->sanitizer->sanitize($contents);

        return $this;
    }

    /**
     * Set Parameters in Url
     *
     * @return static
     */
    public function setParameter(string $key, mixed $val)
    {
        $this->parameters[$key] = $val;

        return $this;
    }

    /**
     * Set HTTP parameters
     *
     * @PhpUnitGen\set("parameters")
     *
     * @param array $parameters HTTP parameters
     */
    public function setParameters(array $parameters): self
    {
        $this->parameters = $this->sanitizer->sanitize($parameters);

        return $this;
    }

    /**
     * Get Query Parameters
     *
     * @return array|mixed|null
     */
    public function getParameters(?string $key = null)
    {
        //Return all parameterts
        if (null === $key) {
            return $this->parameters;
        }

        //Return One Key
        if (array_key_exists($key, $this->parameters)) {
            return $this->parameters[$key];
        }

        return null;
    }

    /**
     * Get gET query
     *
     * @PhpUnitGen\get("query")
     */
    public function getQuery(): array
    {
        return $this->query;
    }

    /**
     * Get url called
     *
     * @PhpUnitGen\get("url")
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * Populate form an external request
     *
     * @param Request|RequestH2|HttpRequestInterface $request
     *
     * @return HttpRequestInterface
     */
    public function populate(Request|RequestH2|HttpRequestInterface $request): HttpRequestInterface
    {
        if ($request instanceof HttpRequestInterface) {
            foreach (get_class_vars(self::class) as $name => $value) {
                $getter = 'get' . ucfirst($name);
                if (method_exists($request, $getter)) {
                    $this->{$name} = call_user_func_array([$request, $getter], []);
                }
            }

            return $this;
        }

        $this->contents   = $request->getContent();
        $this->cookie     = $request->cookies ?? $_COOKIE ?? [];
        $this->files      = $request->files;
        $this->get        = $request->get;
        $this->post       = $request->post;
        $this->server     = $request->server;
        $this->headers    = $request->headers ?? getallheaders() ?? [];
        $this->method     = $request->method ?? '';
        $this->url        = $request->server['request_uri'];
        $this->query      = explode('/', trim(str_replace('public/', '', $this->url)));
        $this->host       = $request->server['http_host'] ?? $_SERVER['http_host'] ?? '';
        $this->contents   = $request->getContent();
        $this->parameters = $request->getData();

        return $this;
    }

    /**
     * Get headers sent
     *
     * @PhpUnitGen\get("headers")
     *
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }
}
