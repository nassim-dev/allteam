<?php

namespace core\messages\request;

use Swoole\Http2\Request as RequestH2;
use Swoole\Http\Request;

/**
 * Interface HttpRequestInterface
 *
 * Description
 *
 * @category  Description
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link  https://allteam.io
 * @since File available since Release 0.1
 */
interface HttpRequestInterface
{
    /**
     * Return true if is an extension request
     */
    public function isExtension(): bool;

    /**
     * Get Headers and check if is ajax
     */
    public function isAjaxify(): bool;

    /**
     * Get Page from URL
     */
    public function getPage(): ?string;

    /**
     * Add content to request
     *
     * @return self
     */
    public function addContent(string $key, mixed $value);

    /**
     * Return argc
     *
     * @return array|null
     */
    public function getArgc(): ?array;

    /**
     * Return argv
     *
     * @return array|null
     */
    public function getArgv(): ?array;

    /**
     * Get php://input
     *
     * @return array|mixed
     */
    public function getContents(?string $key = null);

    /**
     * Return $_COOKIE
     *
     * @return array|null|mixed
     */
    public function getCookie(?string $key = null);

    /**
     * Return $_FILES
     *
     * @return array|null|mixed
     */
    public function getFiles(?string $key = null);

    /**
     * Return $_GET
     *
     * @return array|null|mixed
     */
    public function getGet(?string $key = null);

    /**
     * Get HTTP_HOST
     */
    public function getHost(): string;

    /**
     * Get HTTP Method GET PUT PATCH DELETE...
     */
    public function getMethod(): string;

    /**
     * Return $_POST
     *
     * @return array|null|mixed
     */
    public function getPost(?string $key = null);

    /**
     * Return $_SERVER
     *
     * @return array|null|mixed
     */
    public function getServer(?string $key = null);

    /**
     * Return true if HTTPS
     */
    public function isSecure(): bool;

    /**
     * Add multiple content as array to request
     *
     * @return void
     */
    public function mergeContents(array $params);

    /**
     * Format Inputs from php://input
     *
     * @return static
     */
    public function setContents(array $contents);

    /**
     * Set Parameters in Url
     *
     * @return static
     */
    public function setParameter(string $key, mixed $val);

    /**
     * Set HTTP parameters
     *
     * @PhpUnitGen\set("parameters")
     *
     * @param array $parameters HTTP parameters
     */
    public function setParameters(array $parameters): self;

    /**
     * Get Query Parameters
     *
     * @return array|mixed|null
     */
    public function getParameters(?string $key = null);

    /**
     * Get gET query
     *
     * @PhpUnitGen\get("query")
     */
    public function getQuery(): array;

    /**
     * Get url called
     *
     * @PhpUnitGen\get("url")
     */
    public function getUrl(): string;

    /**
     * Set from external object
     *
     * @param RequestH2|Request|HttpRequestInterface $request
     *
     * @return self
     */
    public function populate(RequestH2|Request|HttpRequestInterface $request): self;

    /**
     * Get headers sent
     *
     * @PhpUnitGen\get("headers")
     *
     * @return array
     */
    public function getHeaders(): array;
}
