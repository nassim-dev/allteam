<?php

namespace core\middleware;

use core\messages\request\HttpRequestInterface;
use core\messages\response\HttpResponseInterface;

/**
 * Class Dispatcher
 *
 * Middleware dispatcher
 *
 * @category Middleware dispatcher
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
class MiddlewareDispatcher
{
    /**
     * Actual Index
     */
    private int $_index = 0;

    /**
     * Middlewares
     */
    private array $_middlewares = [];

    /**
     * Object Transmitted
     */
    private array $_savedObjects = [];


    /**
     * @param $response
     */
    public function __construct(
        private HttpResponseInterface $response,
        private HttpRequestInterface $request
    ) {
    }


    /**
     * Add new middleware
     *
     * @return static
     */
    public function pipe(MiddlewareInterface $middleware): static
    {
        $this->_middlewares[] = $middleware;

        return $this;
    }

    /**
     * Executes Middlewares
     *
     * @return HttpResponseInterface
     */
    public function process(): HttpResponseInterface
    {
        $middleware = $this->getMiddleware();
        if (null === $middleware) {
            return $this->response;
        }

        return $middleware->process($this->request, $this->response, $this);
    }

    /**
     * Set object Transmitted
     *
     * @param array $_savedObjects Object Transmitted
     */
    public function setSavedObjects(string $key, mixed $object): self
    {
        $this->_savedObjects[$key] = $object;

        return $this;
    }


    /**
     * Get object Transmitted
     *
     * @return array
     */
    public function getSavedObjects()
    {
        return $this->_savedObjects;
    }

    private function getMiddleware(): ?MiddlewareInterface
    {
        return $this->_middlewares[$this->_index++] ?? null;
    }
}
