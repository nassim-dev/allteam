<?php

namespace core\middleware;

use core\messages\request\HttpRequestInterface;
use core\messages\response\HttpResponseInterface;

/**
 * Interface MiddlewareInterface
 *
 * Middleware interface
 *
 * @category Middleware interface
 * @version   Release: 0.1
 * @author    Nassim Ourami <nassim.ourami@mailo.com>
 * @license   CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * @link    https://allteam.io
 * @since   File available since Release 0.1
 * @package Allteam
 */
interface MiddlewareInterface
{
    public function process(HttpRequestInterface $request, HttpResponseInterface $response, MiddlewareDispatcher $delegate);
}
