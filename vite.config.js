import { defineConfig, splitVendorChunkPlugin } from 'vite';
import { resolve } from 'path';
import reactRefresh from '@vitejs/plugin-react';
import dynamicImportVariables from 'rollup-plugin-dynamic-import-variables';
import legacy from '@vitejs/plugin-legacy';
import path from 'path';
import nodeResolver from '@rollup/plugin-node-resolve';
import vue from '@vitejs/plugin-vue';
import basicSsl from '@vitejs/plugin-basic-ssl';
//import dns from 'dns';
//
//dns.setDefaultResultOrder('verbatim');

export default defineConfig({
  resolve: {
    preserveSymlinks: true,
    alias: [
      {
        find: 'vue',
        replacement: 'vue/dist/vue.esm-bundler.js',
      },
      { find: '@', replacement: resolve(__dirname, '/assets') },
    ],
  },
  plugins: [
    //nodeResolver(),
    basicSsl(),
    vue({
      template: {
        compilerOptions: {
          // treat all tags with a dash as custom elements
          isCustomElement: (tag) => tag.includes('-'),
        },
      },
    }),
    reactRefresh(),
    splitVendorChunkPlugin(),
    legacy({
      targets: ['defaults', 'not IE 11'],
    }),
    {
      name: 'htmlViewsRefresh',
      configureServer: ({ watcher, ws }) => {
        watcher.add(resolve('./**/*.html'));
        watcher.on('change', function (file) {
          if (file.endsWith('.html')) {
            ws.send({
              type: 'full-reload',
            });
          }
        });
      },
    },
  ],
  root: path.resolve(__dirname, './assets'),
  base: '/assets/',
  server: {
    https: true,
    base: 'localhost',
    host: 'localhost',
    watch: {
      disableGlobbing: false,
    },
    port: 3000,
    hmr: {
      host: 'localhost',
      path: '',
      port: 3000,
      protocol: 'wss', //todo Find a way to use wss://
    },
    cors: {
      origin: '*',
      methods: ['GET', 'PUT', 'POST'],
    },
  },

  build: {
    assetsDir: '',
    outDir: '../public/assets',
    //cssCodeSplit: false,
    manifest: true,
    //brotliSize: false,
    sourcemap: true,
    minify: 'terser',
    //terserOptions: {},
    rollupOptions: {
      // overwrite default .html entry
      input: ['./assets/index.js'],
      plugins: [
        //hoistImportDeps(),
        dynamicImportVariables({
          exclude: ['**/*.css', '**/*.scss'],
        }),
      ],
    },
  },
});
